/*
 *    video.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors
     */
    public errordomain ErrVideo
    {
        /**
         * GStreamer plugin we are needed doesn't exist
         */
        NOT_FOUND_PLUGIN,

        /**
         * Pipeline general error
         */
        PIPELINE_FAIL,

        /**
         * Can't get list of video devices from GStreamer
         */
        HW_INFO_ERR,

        /**
         * No video device with specified name in GStreamer list is
         * found
         */
        HW_NOT_FOUND_ERR,

        /**
         * Video device is possibly related to another video system
         */
        VIDEO_SYS_AND_HW_MISMATCH,
    }

    /**
     * GStreamer video pipeline 1 errors
     */
    public errordomain ErrVideo1
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * Buddy turned off video receiving
         */
        TURNED_OFF_VIDEO,
    }

    /**
     * Video call start errors
     */
    public errordomain ErrVideoCallStart
    {
        /**
         * A resource allocation error occurred while trying to create
         * the structures required for the call
         */
        MEM_ALLOC,

        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * The buddy was valid, but not currently connected
         */
        BUDDY_NOT_CONNECTED_ERR,
    }

    /**
     * Video call stop errors
     */
    public errordomain ErrVideoCallStop
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,
    }

    /*
     * Video call answer errors
     */
    public errordomain ErrVideoCallAnswer
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * Failed to initialize codecs for call session. Note that codec
         * initiation will fail if there is no receive callback
         * registered for either video video.
         */
        CODEC_FAIL,

        /**
         * The buddy was valid, but he is not currently trying to
         * initiate a call.
         * This is also returned if this client is already in a call
         * with the buddy.
         */
        NOT_CALLING_BUDDY,
    }

    /**
     * This is the video class.
     *
     * Webcam input, speaker output are handled there.
     * GStreamer is the multimedia framework which does all of this.
     */
    [SingleInstance]
    public class Video : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // See doc/GSTREAMER_PIPELINES for details how the video
        // pipelines are built

        // GStreamer video pipeline 1.
        // (Webcam Input - P2P-network Output.)
        private Gst.Pipeline pipeline1;

        // GStreamer video pipeline 1 elements.
        // (Webcam Input - P2P-network Output.)
        private struct pipeline1_elems_t
        {
            Gst.Element  src;     // autovideosrc
            Gst.Element  filter1; // videoscale
            Gst.Element  filter2; // videoconvert
            Gst.App.Sink sink;    // appsink
        }

        private pipeline1_elems_t pipeline1_elems;

        // GStreamer video pipeline 2.
        // (P2P-network Input - Display Output, full-screen.)
        private Gst.Pipeline pipeline2;

        // GStreamer video pipeline 2 elements.
        // (P2P-network Input - Display Output, full-screen.)
        private struct pipeline2_elems_t
        {
            Gst.App.Src src;    // appsrc
            Gst.Element filter; // videoscale
            Gst.Element sink;   // autovideosink
        }

        private pipeline2_elems_t pipeline2_elems;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Callback performing the handling video frames from GStreamer
        // video pipeline 1.
        // (Webcam Input - P2P-network Output)
        private Gst.FlowReturn pipeline1_new_sample_handle()
        {
            // Perform the GStreamer video pipeline 1 flowing until all
            // samples have been sent
            for (;;)
            {
                Gst.MapInfo map;
                unowned uint8[] data;
                size_t len;

                // Check for a new video frame from a webcam
                Gst.Sample? frm = this.pipeline1_elems.sink.try_pull_sample(0 // No timeout, no block
                                                                           );
                if (frm == null)
                    break;

                // Get the frame's buffer
                Gst.Buffer? buf = frm.get_buffer();
                GLib.assert(buf != null);

                // Map buffer
                bool res = buf.map(out map, Gst.MapFlags.READ);
                GLib.assert(res);

                // Number of bytes to sent.
                // Data to sent.
                len  = map.size;
                data = map.data;

                // Lock mutex
                model->mutex.lock();

                // Should have at least one peer
                if (model->peer_buddies_video.size == 0)
                {
                    // Unlock mutex.
                    // Unmap buffer.
                    model->mutex.unlock();
                    buf.unmap(map);
                    return Gst.FlowReturn.OK;
                }

                // Each peer should receive the frame
                foreach (Buddy peer in model->peer_buddies_video.values)
                {
                    // Send video from webcam to peer
                    string dbg;

                    try
                    {
                        // Prepare the debug print
                        dbg  = _("Outcoming video frame to buddy: %s: ").printf(peer.display_name_get());
                        dbg += GLib.dngettext(Cfg.DOMAIN, "%u byte", "%u bytes", len).printf((uint) len);
                        dbg += ": ";

                        // Plane offsets
                        uint32 total  = 640 * 480; // TODO: tune it wisely
                        uint32 total4 = total / 4;

                        uint32 len_y = total;
                        uint32 len_u = total4;
                        //uint32 len_v = total4;

                        uint32 offset_y = 0;
                        uint32 offset_u = offset_y + len_y;
                        uint32 offset_v = offset_u + len_u;

                        // Send video frame
                        ToxAV.ERR_SEND_FRAME err = ToxAV.ERR_SEND_FRAME.OK;

                        res = model->tox_av.video_send_frame(peer.uid,
                                                             640, // TODO: tune it wisely
                                                             480, // TODO: tune it wisely
                                                             (uint8[]) &data[offset_y],
                                                             (uint8[]) &data[offset_u],
                                                             (uint8[]) &data[offset_v],
                                                             out err);

                        // Handle error
                        switch (err)
                        {
                        // The function returned successfully
                        case ToxAV.ERR_SEND_FRAME.OK:
                            GLib.assert(res);
                            break;

                        // The samples data pointer was null
                        case ToxAV.ERR_SEND_FRAME.NULL:
                            GLib.assert(!res);
                            GLib.assert(false);
                            break;

                        // The buddy number passed did not designate a valid buddy
                        case ToxAV.ERR_SEND_FRAME.FRIEND_NOT_FOUND:
                            GLib.assert(!res);
                            GLib.assert(false);
                            break;

                        // This client is currently not in a call with the buddy
                        case ToxAV.ERR_SEND_FRAME.FRIEND_NOT_IN_CALL:
                            GLib.assert(!res);
                            GLib.assert(false);
                            break;

                        // Synchronization error occurred
                        case ToxAV.ERR_SEND_FRAME.SYNC:
                            GLib.assert(!res);
                            throw new ErrVideo1.SYNC_FAIL("Synchronization error occurred");
                            //break;

                        // One of the frame parameters was invalid. E. g., the video
                        // sampling rate may be unsupported.
                        case ToxAV.ERR_SEND_FRAME.INVALID:
                            GLib.assert(!res);
                            GLib.assert(false);
                            break;

                        // Buddy turned off video receiving
                        case ToxAV.ERR_SEND_FRAME.PAYLOAD_TYPE_DISABLED:
                            GLib.assert(!res);
                            throw new ErrVideo1.TURNED_OFF_VIDEO("Buddy turned off video receiving");
                            //break;

                        // Failed to push frame through RTP interface
                        case ToxAV.ERR_SEND_FRAME.RTP_FAILED:
                            GLib.assert(!res);
                            GLib.assert(false);
                            break;

                        // Another error
                        default:
                            GLib.assert(false);
                            break;
                        }

                        GLib.debug(dbg + _("OK"));
                    }
                    catch (ErrVideo1 ex)
                    {
                        GLib.debug(dbg + Model.first_ch_lowercase_make(ex.message));
                    }
                }

                // Unlock mutex
                model->mutex.unlock();

                // Unmap buffer
                buf.unmap(map);
            }

            return Gst.FlowReturn.OK;
        }

        /*----------------------------------------------------------------------------*/

        // Return a pipeline state.
        //
        // Notice. This method should be private. I changed it to public
        // only to suppress compiler warning that method never used.
        //private bool pipeline_en_get(Model        *model,
        //                             Gst.Pipeline  pipeline) throws ErrVideo
        public bool pipeline_en_get(Model        *model,
                                    Gst.Pipeline  pipeline) throws ErrVideo
        {
            Gst.StateChangeReturn res;
            Gst.State state;

            // Get the state
            res = pipeline.get_state(out state,
                                     null,
                                     0);

            // Ensure there are no errors
            if (res == Gst.StateChangeReturn.FAILURE)
            {
                throw new ErrVideo.PIPELINE_FAIL(_("Video subsystem fails while trying to get its state"));
            }

            GLib.assert(state == Gst.State.NULL    ||
                        state == Gst.State.PLAYING ||
                        state == Gst.State.READY);

            return state != Gst.State.NULL;
        }

        /*----------------------------------------------------------------------------*/

        // Start/stop a pipeline
        private void pipeline_en_set(Model        *model,
                                     Gst.Pipeline  pipeline,
                                     bool          flag) throws ErrVideo
        {
            Gst.StateChangeReturn res;
            Gst.State state = flag ? Gst.State.PLAYING :
                                     Gst.State.NULL;

            // Set the state
            res = pipeline.set_state(state);

            // Ensure there are no errors
            if (res == Gst.StateChangeReturn.FAILURE)
            {
                throw new ErrVideo.PIPELINE_FAIL(_("Video subsystem fails while trying to start/stop"));
            }
        }

        /*----------------------------------------------------------------------------*/

        // Initialize video pipeline 1.
        // (Webcam Input - P2P-network Output.)
        private void video1_init(Model *model) throws ErrVideo
        {
            // Create the elements
            this.pipeline1_elems.src     =                Gst.ElementFactory.make("autovideosrc", "src");
            this.pipeline1_elems.filter1 =                Gst.ElementFactory.make("videoscale",   "filter1");
            this.pipeline1_elems.filter2 =                Gst.ElementFactory.make("videoconvert", "filter2");
            this.pipeline1_elems.sink    = (Gst.App.Sink) Gst.ElementFactory.make("appsink",      "sink");

            GLib.assert(this.pipeline1_elems.src     != null);
            GLib.assert(this.pipeline1_elems.filter1 != null);
            GLib.assert(this.pipeline1_elems.filter2 != null);
            GLib.assert(this.pipeline1_elems.sink    != null);

            // Set caps
            var caps = new Gst.Caps.simple("video/x-raw",
                                           "width",  GLib.Type.INT, 640, // TODO: tune it wisely
                                           "height", GLib.Type.INT, 480  // TODO: tune it wisely
                                          );
            this.pipeline1_elems.sink.caps = new Gst.Caps.simple("video/x-raw",
                                                                 "format", GLib.Type.STRING, "I420");

            // Set signals
            this.pipeline1_elems.sink.set_emit_signals(true);
            this.pipeline1_elems.sink.new_sample.connect(pipeline1_new_sample_handle);

            // Create the empty pipeline
            this.pipeline1 = new Gst.Pipeline("pipeline1");
            GLib.assert(this.pipeline1 != null);

            // Build the pipeline
            this.pipeline1.add_many(this.pipeline1_elems.src,
                                    this.pipeline1_elems.filter1,
                                    this.pipeline1_elems.filter2,
                                    this.pipeline1_elems.sink);

            bool res = this.pipeline1_elems.src.link(this.pipeline1_elems.filter1);
            GLib.assert(res);
            res = this.pipeline1_elems.filter1.link_filtered(this.pipeline1_elems.filter2, caps);
            GLib.assert(res);
            res = this.pipeline1_elems.filter2.link(this.pipeline1_elems.sink);
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize video pipeline 1.
        // (Webcam Input - P2P-network Output.)
        private void video1_deinit(Model *model) throws ErrVideo
        {
            // Stop the pipeline
            pipeline_en_set(model,
                            this.pipeline1,
                            false // Stop
                           );
        }

        /*----------------------------------------------------------------------------*/

        // Initialize video pipeline 2.
        // (P2P-network Input - Display Output, full-screen.)
        private void video2_init(Model *model) throws ErrVideo
        {
            // Create the elements
            this.pipeline2_elems.src    = (Gst.App.Src) Gst.ElementFactory.make("appsrc",        "src");
            this.pipeline2_elems.filter =               Gst.ElementFactory.make("videoscale",    "filter");
            this.pipeline2_elems.sink   =               Gst.ElementFactory.make("autovideosink", "sink");

            GLib.assert(this.pipeline2_elems.src    != null);
            GLib.assert(this.pipeline2_elems.filter != null);
            GLib.assert(this.pipeline2_elems.sink   != null);

            // Caps.
            // (Desired video scale.)
            var caps = new Gst.Caps.simple("video/x-raw",
                                           "format", GLib.Type.STRING, "I420",
                                           "width",  GLib.Type.INT, 640,
                                           "height", GLib.Type.INT, 480);

            // Create the empty pipeline
            this.pipeline2 = new Gst.Pipeline("pipeline2");
            GLib.assert(this.pipeline2 != null);

            // Build the pipeline
            this.pipeline2.add_many(this.pipeline2_elems.src,
                                    this.pipeline2_elems.filter,
                                    this.pipeline2_elems.sink);

            bool res = this.pipeline2_elems.src.link(this.pipeline2_elems.filter);
            GLib.assert(res);
            res = this.pipeline2_elems.filter.link_filtered(this.pipeline2_elems.sink, caps);
            GLib.assert(res);

            // Start the pipeline
            pipeline_en_set(model,
                            this.pipeline2,
                            true // Start
                           );
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize video pipeline 2.
        // (P2P-network Input - Display Output, full-screen.)
        private void video2_deinit(Model *model) throws ErrVideo
        {
            // Stop the pipeline
            pipeline_en_set(model,
                            this.pipeline2,
                            false // Stop
                           );
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Video constructor
         *
         * @param model Model
         */
        public Video(Model *model)
        {
#if GSTREAMER_INITIALIZED_BY_VIDEO
            // Initialize GStreamer
            try
            {
                unowned string[] args = model->args;
                bool res = Gst.init_check(ref args);
                GLib.assert(res);
                Gst.init(ref args);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                GLib.assert(false);
            }
#endif

            // TODO: eliminate Librem 5 quirk
#if PORT_LIBREM5
            return;
#endif

            // Initialize video pipelines.
            //
            // Initialize pipeline 1.
            try
            {
                video1_init(model);
            }
            catch (ErrVideo ex)
            {
                GLib.debug(ex.message);
            }

            // Initialize pipeline 2
            try
            {
                video2_init(model);
            }
            catch (ErrVideo ex)
            {
                GLib.debug(ex.message);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Video destructor
         */
        ~Video()
        {
            // Deinitialize video pipelines.
            //
            // Deinitialize pipeline 1.
            try
            {
                video1_deinit(model);
            }
            catch (ErrVideo ex)
            {
                //GLib.assert(false);
            }

            // Deinitialize pipeline 2.
            try
            {
                video2_deinit(model);
            }
            catch (ErrVideo ex)
            {
                //GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Enable webcam
         *
         * @param model Model
         */
        public void webcam_en(Model *model) throws ErrVideo
        {
            // Start pipeline 1.
            // (Webcam Input - P2P-network Output.)
            pipeline_en_set(model,
                            pipeline1,
                            true // Start
                           );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disable webcam
         *
         * @param model Model
         */
        public void webcam_dis(Model *model) throws ErrVideo
        {
            // Stop pipeline 1.
            // (Webcam Input - P2P-network Output.)
            pipeline_en_set(model,
                            pipeline1,
                            false // Stop
                           );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Play video frame
         *
         * @param model  Model
         * @param buf    Video frame in YUV I420 format
         * @param width  Width
         * @param height Height
         */
        public void play(Model   *model,
                         uint8[]  buf,
                         size_t   width,
                         size_t   height)
        {
            // Set caps.
            // (Source video scale.)
            this.pipeline2_elems.src.caps = new Gst.Caps.simple("video/x-raw",
                                                                "format", GLib.Type.STRING, "I420",
                                                                "width",  GLib.Type.INT, (int) width,
                                                                "height", GLib.Type.INT, (int) height);

            // Push the incoming buffer into the video pipeline
            Gst.Memory mem = Gst.Memory.new_wrapped<void *>(Gst.MemoryFlags.READONLY,
                                                            buf,
                                                            0,   // No offset
                                                            buf.length,
                                                            null // Not used
                                                           );
            var buf_ = new Gst.Buffer();

            buf_.append_memory(mem);
            this.pipeline2_elems.src.push_buffer(buf_);
        }
    }
}

