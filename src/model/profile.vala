/*
 *    profile.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors.
     *
     * Something goes bad while the profile files creating/deleting.
     */
    public errordomain ErrProfileGeneral
    {
        /**
         * Directory creation error
         */
        CANNOT_CREATE_DIR,

        /**
         * Directory renaming error
         */
        CANNOT_RENAME_DIR,

        /**
         * File renaming error
         */
        CANNOT_RENAME_FILE,

        /**
         * Packing archive error
         */
        CANNOT_PACK_ARCHIVE,

        /**
         * Unpacking archive error
         */
        CANNOT_UNPACK_ARCHIVE,

        /**
         * Directory deletion error
         */
        CANNOT_DEL_DIR,

        /**
         * File deletion error
         */
        CANNOT_DEL_FILE,
    }

    /*
     * Profile encryption errors
     */
    public errordomain ErrProfileEncryption
    {
        /**
         * The crypto lib was unable to derive a key from the given
         * passphrase, which is usually a lack of memory issue
         */
        FAILED_KEY_DERIVATION,

        /**
         * The encryption itself failed
         */
        FAILED_ERR,
    }

    /*
     * Profile decryption errors
     */
    public errordomain ErrProfileDecryption
    {
        /**
         * The input data was shorter than
         * ToxEncrypt.PASS_ENCRYPTION_EXTRA_LENGTH bytes
         */
        INVALID_LENGTH,

        /**
         * The input data is missing the magic number (i.e. wasn't
         * created by this module, or is corrupted)
         */
        BAD_FORMAT,

        /**
         * The crypto lib was unable to derive a key from the given
         * passphrase, which is usually a lack of memory issue
         */
        FAILED_KEY_DERIVATION,

        /**
         * The encrypted byte array could not be decrypted. Either the
         * data was corrupt or the password/key was incorrect.
         */
        FAILED_ERR,
    }

    /**
     * Database error
     */
    public errordomain ErrProfileDB
    {
        /**
         * Can't open database
         */
        OPEN_ERR,

        /**
         * Can't execute PRAGMA statement
         */
        PRAGMA_ERR,

        /**
         * Can't execute CREATE statement
         */
        CREATE_ERR,

        /**
         * Can't prepare SELECT statement
         */
        PREPARE_SEL_ERR,

        /**
         * Can't execute SELECT statement
         */
        SEL_ERR,

        /**
         * Can't prepare INSERT statement
         */
        PREPARE_INSERT_ERR,

        /**
         * Can't execute INSERT statement
         */
        INSERT_ERR,

        /**
         * Can't prepare UPDATE statement
         */
        PREPARE_UPD_ERR,

        /**
         * Can't execute UPDATE statement
         */
        UPD_ERR,

        /**
         * Can't prepare DELETE statement
         */
        PREPARE_DEL_ERR,

        /**
         * Can't execute DELETE statement
         */
        DEL_ERR,
    }

    /**
     * This is the profile class
     */
    public class Profile : Person
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Database
        private Sqlite.Database? db;

        // Block size used for unpacking archives
        private const size_t UNPACK_BLOCK_SIZE = 10000;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Profile name
         */
        public string? profile_name { public get; public set construct; }

        /**
         * false: password is not used for profile encryption or it is
         * unknown,
         * true:  password is used
         */
        public bool use_pwd { public get; public set construct; }

        /**
         * Password.
         * null means either the password is not used or it is unknown.
         */
        public string? pwd { public get; public set construct; }

        /**
         * Profile-dependent application settings
         */
        public GLib.KeyFile? cfg { public get; private set; }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Configuration key.
         * show_buddies key in [main] group.
         */
        public enum cfg_main_show_buddies_t
        {
            SHOW_ALL         = 0,
            HIDE_OFFLINE_BUT = 1,
            HIDE_OFFLINE     = 2,
        }

        /**
         * Configuration key.
         * sort_buddies key in [main] group.
         */
        public enum cfg_main_sort_buddies_t
        {
            ADD_LST      = 0,
            DISPLAY_NAME = 1,
            LAST_MSG     = 2,
            STATE        = 3,
        }

        /**
         * Configuration key.
         * sort_buddies_order key in [main] group.
         */
        public enum cfg_main_sort_buddies_order_t
        {
            ASC  = 0,
            DESC = 1,
        }

        /**
         * Configuration key.
         * sort_conferences key in [main] group.
         */
        public enum cfg_main_sort_conferences_t
        {
            ADD_LST      = 0,
            DISPLAY_NAME = 1,
            LAST_MSG     = 2,
        }

        /**
         * Configuration key.
         * sort_conferences_order key in [main] group.
         */
        public enum cfg_main_sort_conferences_order_t
        {
            ASC  = 0,
            DESC = 1,
        }

        /**
         * Configuration key.
         * sort_members key in [main] group.
         */
        public enum cfg_main_sort_members_t
        {
            ADD_LST      = 0,
            DISPLAY_NAME = 1,
        }

        /**
         * Configuration key.
         * sort_members_order key in [main] group.
         */
        public enum cfg_main_sort_members_order_t
        {
            ASC  = 0,
            DESC = 1,
        }

        /**
         * Configuration key.
         * avatar_size key in [appearance] group.
         */
        public enum cfg_appearance_avatar_size_t
        {
            SMALL = 0,
            BIG   = 1,
        }

        /**
         * Configuration key.
         * proxy_type key in [network] group.
         */
        public enum cfg_net_proxy_type_t
        {
            NONE   = 0,
            HTTP   = 1,
            SOCKS5 = 2,
        }

        /**
         * Configuration key.
         * sys key in [audio] group.
         */
        public enum cfg_audio_sys_t
        {
            AUTO       = 0,
#if __WXGTK__
            JACK       = 1,
            PIPEWIRE   = 2,
            PULSEAUDIO = 3,
            OSS        = 4,
#elif __WXMSW__
#endif

            CNT,
        }

        /**
         * Conference invitation type
         */
        public enum conference_invitation_t
        {
            TEXT        = 0,
            AUDIO_VIDEO = 1,

            NONE,
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Filepath to profile
        private string filepath_get(Model *model)
        {
            return GLib.Path.build_filename(model->path_data,
                                            this.profile_name,
                                            this.profile_name + ".tox");
        }

        /*----------------------------------------------------------------------------*/

        // Filepath to configuration file
        private string cfg_filepath_get(Model *model)
        {
            return GLib.Path.build_filename(model->path_data,
                                            this.profile_name,
                                            this.use_pwd ? this.profile_name + ".cfg.bin" :
                                                           this.profile_name + ".cfg");
        }

        /*----------------------------------------------------------------------------*/

        // Filepath to database
        private string db_filepath_get(Model *model)
        {
            return GLib.Path.build_filename(model->path_data,
                                            this.profile_name,
                                            this.use_pwd ? this.profile_name + ".sqlite3.bin" :
                                                           this.profile_name + ".sqlite3");
        }

        /*----------------------------------------------------------------------------*/

        // Encrypt file (e. g. profile, config or database)
        private uint8[] encrypt(uint8[] plain) throws ErrProfileEncryption
        {
            // Encrypt the given data with the given passphrase
            size_t encrypted_len = plain.length + ToxEncrypt.PASS_ENCRYPTION_EXTRA_LENGTH;
            var    encrypted_buf = new uint8[encrypted_len];

            ToxEncrypt.ERR_ENCRYPTION err;
            bool res = ToxEncrypt.pass_encrypt(plain,
                                               pwd.data,
                                               encrypted_buf,
                                               out err);

            switch (err)
            {
                case ToxEncrypt.ERR_ENCRYPTION.OK:   // Everything is fine
                    GLib.assert(res);
                    break;
                case ToxEncrypt.ERR_ENCRYPTION.NULL: // Some input data, or maybe the output pointer, was null
                    GLib.assert(false);
                    break;
                case ToxEncrypt.ERR_ENCRYPTION.KEY_DERIVATION_FAILED:
                    GLib.assert(!res);
                    throw new ErrProfileEncryption.FAILED_KEY_DERIVATION(_("The crypto lib was unable to derive a key from the given passphrase, " +
                                                                           "which is usually a lack of memory issue"));
                    //break;
                case ToxEncrypt.ERR_ENCRYPTION.FAILED:
                    GLib.assert(!res);
                    throw new ErrProfileEncryption.FAILED_ERR(_("The encryption itself failed"));
                    //break;
                default:
                    GLib.assert(false);
                    break;
            }

            return encrypted_buf;
        }

        /*----------------------------------------------------------------------------*/

        // Decrypt file (e. g. profile, config or database)
        private uint8[] decrypt(uint8[] encrypted_buf) throws ErrProfileDecryption
        {
            // Decrypt the given data with the given passphrase
            if (encrypted_buf.length < ToxEncrypt.PASS_ENCRYPTION_EXTRA_LENGTH)
                throw new ErrProfileDecryption.INVALID_LENGTH(_("The input data was shorter than %d bytes"), ToxEncrypt.PASS_ENCRYPTION_EXTRA_LENGTH);

            size_t plain_len = encrypted_buf.length - ToxEncrypt.PASS_ENCRYPTION_EXTRA_LENGTH;
            var    plain     = new uint8[plain_len];

            ToxEncrypt.ERR_DECRYPTION err;
            bool res = ToxEncrypt.pass_decrypt(encrypted_buf,
                                               pwd.data,
                                               plain,
                                               out err);

            switch (err)
            {
                case ToxEncrypt.ERR_DECRYPTION.OK:   // Everything is fine
                    GLib.assert(res);
                    break;
                case ToxEncrypt.ERR_DECRYPTION.NULL: // Some input data, or maybe the output pointer, was null
                    GLib.assert(false);
                    break;
                case ToxEncrypt.ERR_DECRYPTION.INVALID_LENGTH:
                    GLib.assert(false);
                    break;
                case ToxEncrypt.ERR_DECRYPTION.BAD_FORMAT:
                    GLib.assert(!res);
                    throw new ErrProfileDecryption.BAD_FORMAT(_("The input data is missing the magic number"));
                    //break;
                case ToxEncrypt.ERR_DECRYPTION.KEY_DERIVATION_FAILED:
                    GLib.assert(!res);
                    throw new ErrProfileDecryption.FAILED_KEY_DERIVATION(_("The crypto lib was unable to derive a key from the given passphrase, " +
                                                                           "which is usually a lack of memory issue"));
                    //break;
                case ToxEncrypt.ERR_DECRYPTION.FAILED:
                    GLib.assert(!res);
                    throw new ErrProfileDecryption.FAILED_ERR(_("The encrypted profile could not be decrypted. " +
                                                                "Either the data was corrupt or the password was incorrect."));
                    //break;
                default:
                    GLib.assert(false);
                    break;
            }

            return plain;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Profile constructor
         *
         * @param profile_name Profile name
         * @param use_pwd      false: password is not used for profile
         *                            encryption or it is unknown,
         *                     true:  password is used
         * @param pwd          Password.
         *                     null means either the password is not used or
         *                     it is unknown.
         */
        public Profile(string? profile_name,
                       bool    use_pwd = false,
                       string? pwd     = null)
            requires ((!use_pwd && pwd == null) ||
                        use_pwd)
        {
            Object(//key          :  null, // Array properties are not supported by Vala 0.48
                   addr           : null,
                   name           : null,
                   state          : Person.state_t.NONE,
                   state_user     : Person.state_user_t.NONE,
                   status         : null,
                   changed_avatar : false,
                   //avatar         : null,
                   //avatar_16x16   : null,
                   //avatar_32x32   : null,
                   //avatar_normal  : null,

                   profile_name : profile_name,
                   use_pwd      : use_pwd,
                   pwd          : pwd);

            this.key           = null;
            this.avatar        = null;
            this.avatar_16x16  = null;
            this.avatar_32x32  = null;
            this.avatar_normal = null;

            this.db  = null;
            this.cfg = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Calculate password strength
          *
          * @param pwd Password
          *
          * @return Password strength in percent
          */
        public static uint pwd_strength_calc(string pwd)
            ensures (0 <= result <= 100)
        {
            if (pwd.length < 8)
                return 0;

            var map = new Gee.HashMap<unichar, uint>();
            unichar ch;
            double score = 0;

            for (int pos = 0; pwd.get_next_char(ref pos, out ch);)
            {
                map[ch] = map[ch] + 1;
                score += 5.0f / map[ch];
            }

            int variations = -1;

            variations += GLib.Regex.match_simple("[0-9]",        pwd) ? 1 : 0;
            variations += GLib.Regex.match_simple("[a-z]",        pwd) ? 1 : 0;
            variations += GLib.Regex.match_simple("[A-Z]",        pwd) ? 1 : 0;
            variations += GLib.Regex.match_simple("[^0-9a-zA-Z]", pwd) ? 1 : 0;

            int score_percent = (int) score;

            score_percent += variations * 10;
            score_percent -= 20;

            if (score_percent < 0)   score_percent = 0;
            if (score_percent > 100) score_percent = 100;

            return score_percent;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Load profile from disk to memory and check whether it is
         * encrypted
         *
         * @param model Model
         *
         * @return false Profile is not encrypted,
         *         true  Profile is encrypted
         */
        public bool is_encrypted(Model *model) throws GLib.FileError
        {
            // Load encryped profile from disk to memory
            uint8[] buf;
            string filepath = GLib.Path.build_filename(model->path_data, this.profile_name, this.profile_name + ".tox");
            GLib.FileUtils.get_data(filepath, out buf);

            // Return whether it is encrypted
            return ToxEncrypt.is_data_encrypted(buf);
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Import profile from memory.
         *
         * Does nothing by now.
         *
         * @param model Model
         *
         * @param plain Tox ID and other saved data
         */
        public void import(Model *model, uint8[] plain)
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Export profile to memory.
         *
         * Stores all information associated with the Tox instance to a
         * byte array.
         *
         * @param model Model
         *
         * @return Tox ID and other saved data
         */
        public uint8[] export(Model *model)
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Store all information associated with the Tox instance to a
            // byte array
            uint8[] plain = new uint8[model->tox.get_savedata_size()];
            model->tox.get_savedata(plain);
            GLib.assert(!ToxEncrypt.is_data_encrypted(plain));

            return plain;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Load profile from disk to memory and decrypt
         *
         * @param model Model
         *
         * @return Tox ID and other saved data
         */
        public uint8[] load(Model *model) throws GLib.FileError, ErrProfileDecryption
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Load encryped profile from disk to memory
            uint8[] buf;
            string filepath = filepath_get(model);
            GLib.FileUtils.get_data(filepath, out buf);

            // Decrypt the data.
            //
            // Note: Vala 0.48 bug here if using ternary operator.
            uint8[] plain;
            if (this.use_pwd) plain = decrypt(buf);
            else              plain = (owned) buf;
            GLib.assert(!ToxEncrypt.is_data_encrypted(plain));

            // Import profile from memory
            import(model, plain);

            return plain;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Encrypt and save profile from memory to disk
         *
         * @param model Model
         */
        public void save(Model *model) throws GLib.FileError, ErrProfileEncryption, ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Export profile to memory
            uint8[] plain = export(model);

            // Encrypt the data.
            //
            // Note: Vala 0.48 bug here if using ternary operator.
            uint8[] buf;
            if (this.use_pwd) buf = encrypt(plain);
            else              buf = (owned) plain;
            GLib.assert( !this.use_pwd ||
                        ( this.use_pwd && ToxEncrypt.is_data_encrypted(buf)));

            // Create data directory
            string dir = GLib.Path.build_filename(model->path_data, this.profile_name);

            if (!GLib.FileUtils.test(dir, GLib.FileTest.EXISTS))
            {
                int res = GLib.DirUtils.create(dir, 0755);
                if (res == -1)
                    throw new ErrProfileGeneral.CANNOT_CREATE_DIR(_("Can't create directory %s: %s"), dir, Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
            }

            // Write encrypted profile from memory to disk
            string filepath = GLib.Path.build_filename(dir, this.profile_name + ".tox");
            GLib.FileUtils.set_data(filepath, buf);
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Rename profile on disk.
         *
         * Also renames a profile directory, so rename config first with
         * cfg_rename() and database with db_del() methods.
         *
         * @param model        Model
         * @param profile_name New profile name
         */
        public void rename(Model *model, string profile_name) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Profile filepaths
            string dir_old      = GLib.Path.build_filename(model->path_data, this.profile_name);
            string filepath_old = GLib.Path.build_filename(dir_old, this.profile_name + ".tox");

            string dir_new      = GLib.Path.build_filename(model->path_data, profile_name);
            string filepath_new = GLib.Path.build_filename(dir_old, profile_name + ".tox");

            // Rename profile in memory
            this.profile_name = profile_name;

            // Rename profile on disk
            if (GLib.FileUtils.rename(filepath_old, filepath_new) == -1)
                throw new ErrProfileGeneral.CANNOT_RENAME_DIR(_("Can't rename file %s to %s. %s."), filepath_old,
                                                                                                    filepath_new,
                                                                                                    Posix.strerror(GLib.errno));

            // Rename data directory
            if (GLib.FileUtils.rename(dir_old, dir_new) == -1)
                throw new ErrProfileGeneral.CANNOT_RENAME_FILE(_("Can't rename directory %s to %s. %s."), dir_old,
                                                                                                          dir_new,
                                                                                                          Posix.strerror(GLib.errno));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Copy profile from data directory to specific destination on
         * disk
         *
         * @param model    Model
         * @param filepath Filepath to where copy to do
         */
        public void copy_to(Model *model, string filepath) throws GLib.Error
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Profile filepaths
            string dir_src      = GLib.Path.build_filename(model->path_data, this.profile_name);
            string filepath_src = GLib.Path.build_filename(dir_src, this.profile_name + ".tox");

            GLib.File src = GLib.File.new_for_path(filepath_src);
            GLib.File dst = GLib.File.new_for_path(filepath);

            // Copy profile to
            src.copy(dst,
                     GLib.FileCopyFlags.OVERWRITE | GLib.FileCopyFlags.TARGET_DEFAULT_PERMS,
                     null, // Can't cancel the operation
                     null  // No operation progress monitoring
                    );
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Copy profile from specific source on disk to data directory
         *
         * @param model    Model
         * @param filepath Filepath from where copy to do
         */
        public void copy_from(Model *model, string filepath) throws ErrProfileGeneral, GLib.Error
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Profile filepaths
            string dir_dst      = GLib.Path.build_filename(model->path_data, this.profile_name);
            string filepath_dst = GLib.Path.build_filename(dir_dst, this.profile_name + ".tox");

            GLib.File src = GLib.File.new_for_path(filepath);
            GLib.File dst = GLib.File.new_for_path(filepath_dst);

            // Create data directory
            if (!GLib.FileUtils.test(dir_dst, GLib.FileTest.EXISTS))
            {
                int res = GLib.DirUtils.create(dir_dst, 0755);
                if (res == -1)
                    throw new ErrProfileGeneral.CANNOT_CREATE_DIR(_("Can't create directory %s. %s."), dir_dst, Posix.strerror(GLib.errno));
            }

            // Copy profile from
            src.copy(dst,
                     GLib.FileCopyFlags.OVERWRITE | GLib.FileCopyFlags.TARGET_DEFAULT_PERMS,
                     null, // Can't cancel the operation
                     null  // No operation progress monitoring
                    );
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Archive profile directory on disk.
         *
         * Profile directory contains profile, config, database.
         * gzip and tar file formats are used.
         *
         * @param model        Model
         * @param profile_name New profile name
         * @param filepath     Archive filepath
         */
        public void archive(Model *model, string profile_name, string filepath) throws ErrProfileGeneral, GLib.FileError
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Profile filepaths
            string dir = GLib.Path.build_filename(model->path_data, this.profile_name);

            string filepath_profile = GLib.Path.build_filename(dir, this.profile_name + ".tox");
            string filepath_cfg     = GLib.Path.build_filename(dir, this.use_pwd ? this.profile_name + ".sqlite3.bin" :
                                                                                   this.profile_name + ".sqlite3");
            string filepath_db      = GLib.Path.build_filename(dir, this.use_pwd ? this.profile_name + ".cfg.bin" :
                                                                                   this.profile_name + ".cfg");

            unowned string err = _("Can't pack archive %s. %s.");
            
            // Add gzip filter
            var archive = new Archive.Write();
            Archive.Result res;

            res = archive.add_filter_gzip();
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't add gzip filter"));

            // Set format tar
            res = archive.set_format_gnutar();
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't set tar format"));

            // Open archive
            res = archive.open_filename(filepath);
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't open file"));

            // Add profile into archive
            uint8[] buf;
            var entry = new Archive.Entry();

            GLib.FileUtils.get_data(filepath_profile, out buf);

            entry.set_pathname(profile_name + ".tox");
            entry.set_size(buf.length);
            entry.set_filetype(Archive.FileType.IFREG);
            entry.set_perm(0644);

            res = archive.write_header(entry);
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't write profile header"));
            archive.write_data(buf);

            // Add config into archive
            GLib.FileUtils.get_data(filepath_cfg, out buf);

            entry.set_pathname(this.use_pwd ? profile_name + ".sqlite3.bin" :
                                              profile_name + ".sqlite3");
            entry.set_size(buf.length);
            //entry.set_filetype(Archive.FileType.IFREG);
            //entry.set_perm(0644);

            res = archive.write_header(entry);
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't write config header"));
            archive.write_data(buf);

            // Add database into archive
            GLib.FileUtils.get_data(filepath_db, out buf);

            entry.set_pathname(this.use_pwd ? profile_name + ".cfg.bin" :
                                              profile_name + ".cfg");
            entry.set_size(buf.length);
            //entry.set_filetype(Archive.FileType.IFREG);
            //entry.set_perm(0644);

            res = archive.write_header(entry);
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't write database header"));
            archive.write_data(buf);

            // Close archive
            res = archive.close();
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_PACK_ARCHIVE(err, filepath, _("Can't close file"));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Unarchive from disk to profile directory.
         *
         * Profile directory contains profile, config, database.
         * gzip and tar file formats are used.
         *
         * @param model    Model
         * @param filepath Archive filepath
         */
        public void unarchive(Model *model, string filepath) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            unowned string err = _("Can't unpack archive %s. %s.");

            // Profile filepaths
            string dir = GLib.Path.build_filename(model->path_data, this.profile_name);

            // Create data directory
            if (!GLib.FileUtils.test(dir, GLib.FileTest.EXISTS))
            {
                int res = GLib.DirUtils.create(dir, 0755);
                if (res == -1)
                    throw new ErrProfileGeneral.CANNOT_CREATE_DIR(_("Can't create directory %s. %s."), dir, Posix.strerror(GLib.errno));
            }

            // Add support for gzip filter
            var archive = new Archive.Read();
            Archive.Result res;

            res = archive.support_filter_gzip();
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_UNPACK_ARCHIVE(err, filepath, _("Can't add support for gzip filter"));

            // Add support for tar format
            res = archive.support_format_tar();
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_UNPACK_ARCHIVE(err, filepath, _("Can't add support for tar format"));

            // Open archive
            res = archive.open_filename(filepath, UNPACK_BLOCK_SIZE);
            if (res != Archive.Result.OK)
                throw new ErrProfileGeneral.CANNOT_UNPACK_ARCHIVE(err, filepath, _("Can't open file"));

            // Select which attributes we want to restore
            Archive.ExtractFlags flags;

            flags  = Archive.ExtractFlags.ACL;
            flags |= Archive.ExtractFlags.FFLAGS;
            flags |= Archive.ExtractFlags.OWNER;
            flags |= Archive.ExtractFlags.PERM;
            flags |= Archive.ExtractFlags.TIME;

            var extractor = new Archive.WriteDisk();

            extractor.set_options(flags);
            extractor.set_standard_lookup();

            // Extract archive
            try
            {
                for (;;)
                {
                    unowned Archive.Entry entry;

                    res = archive.next_header(out entry);
                    if (res == Archive.Result.EOF)
                        break;
                    if (res != Archive.Result.OK)
                        throw new ErrProfileGeneral.CANNOT_UNPACK_ARCHIVE(err, filepath, _("Input/output error"));

                    unowned string filename  = entry.pathname();
                            string filepath_ = GLib.Path.build_filename(dir, filename);
                    entry.set_pathname(filepath_); // Specify destination directory for extraction

                    res = extractor.write_header(entry);
                    if (res != Archive.Result.OK)
                        continue;

                    unowned uint8[] buf;
                    Archive.int64_t offset;

                    for (;;)
                    {
                        res = archive.read_data_block(out buf, out offset);
                        if (res == Archive.Result.EOF)
                            break;

                        res = (Archive.Result) extractor.write_data_block(buf, offset);
                        if (res != Archive.Result.OK)
                            throw new ErrProfileGeneral.CANNOT_UNPACK_ARCHIVE(err, filepath, _("Input/output error"));
                    }
                }
            }
            catch (ErrProfileGeneral ex)
            {
                GLib.DirUtils.remove(dir);
                throw ex;
            }
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Delete profile from disk.
         *
         * Also deletes a profile directory, which should be empty, so
         * delete config first with cfg_del() and database with db_del()
         * methods.
         *
         * @param model Model
         */
        public void del(Model *model) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Profile filepaths
            string dir      = GLib.Path.build_filename(model->path_data, this.profile_name);
            string filepath = GLib.Path.build_filename(dir, this.profile_name + ".tox");

            // Delete profile from disk
            if (GLib.FileUtils.remove(filepath) == -1)
                throw new ErrProfileGeneral.CANNOT_DEL_DIR(_("Can't delete file %s: %s"), filepath, Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));

            // Delete data directory
            if (GLib.DirUtils.remove(dir) == -1)
                throw new ErrProfileGeneral.CANNOT_DEL_FILE(_("Can't delete directory %s: %s"), dir, Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Initialize profile-dependent application settings
         *
         * @param model Model
         */
        public void cfg_init(Model *model)
        {
            // Initialize config
            if (this.cfg == null)
                this.cfg = new GLib.KeyFile();

            // Set default values for all keys
            this.cfg.set_boolean     ("main",       "use_win_pos_x_y",             false);
            this.cfg.set_integer     ("main",       "x",                           0);
            this.cfg.set_integer     ("main",       "y",                           0);
            this.cfg.set_boolean     ("main",       "use_win_size",                false);
            this.cfg.set_integer     ("main",       "width",                       (int) View.FrmMain.WIDTH);
            this.cfg.set_integer     ("main",       "height",                      (int) View.FrmMain.HEIGHT);
            this.cfg.set_integer     ("main",       "sash_pos",                    (int) View.FrmMain.SPLITTER_VERTICAL_SASH_POS);
            this.cfg.set_integer     ("main",       "sash_tab1_pos",               (int) View.FrmMain.SPLITTER_VERTICAL_TAB1_SASH_POS);
            this.cfg.set_integer     ("main",       "show_buddies",                cfg_main_show_buddies_t.SHOW_ALL);
            this.cfg.set_integer     ("main",       "sort_buddies",                cfg_main_sort_buddies_t.ADD_LST);
            this.cfg.set_integer     ("main",       "sort_buddies_order",          cfg_main_sort_buddies_order_t.ASC);
            this.cfg.set_boolean     ("main",       "sort_buddies_unread_top",     true);
            this.cfg.set_integer     ("main",       "sort_conferences",            cfg_main_sort_conferences_t.ADD_LST);
            this.cfg.set_integer     ("main",       "sort_conferences_order",      cfg_main_sort_conferences_order_t.ASC);
            this.cfg.set_boolean     ("main",       "sort_conferences_unread_top", true);
            this.cfg.set_integer     ("main",       "sort_members",                cfg_main_sort_members_t.DISPLAY_NAME);
            this.cfg.set_integer     ("main",       "sort_members_order",          cfg_main_sort_members_order_t.ASC);
            this.cfg.set_integer     ("appearance", "avatar_size",                 cfg_appearance_avatar_size_t.BIG);
            this.cfg.set_boolean     ("privacy",    "send_typing_notifications",   true);
            this.cfg.set_string_list ("network",    "nodes",                       { "URL: " + Model.NODES_URL, "URL: " + Model.NODES_URL_ALT, "File: " + Model.NODES_FILE });
            this.cfg.set_boolean_list("network",    "en_nodes",                    { false, true, true });
            this.cfg.set_boolean     ("network",    "allow_udp",                   true);
            this.cfg.set_integer     ("network",    "proxy_type",                  cfg_net_proxy_type_t.NONE);
            this.cfg.set_string      ("network",    "proxy_host",                  "");
            this.cfg.set_integer     ("network",    "proxy_port",                  0);
            this.cfg.set_integer     ("audio",      "sys",                         cfg_audio_sys_t.AUTO);
            this.cfg.set_string      ("audio",      "cap_dev",                     "");
            this.cfg.set_integer     ("audio",      "vol",                         50);
            this.cfg.set_string      ("audio",      "playback_dev",                "");
            this.cfg.set_integer     ("audio",      "gain",                        50);
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Load profile-dependent application settings from
         * configuration file on disk to memory and decrypt
         *
         * @param model Model
         */
        public void cfg_load(Model *model) throws GLib.Error
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion
            GLib.assert(this.cfg != null);

            // Load encryped config from disk to memory
            uint8[] buf;
            string filepath = cfg_filepath_get(model);
            FileUtils.get_data(filepath, out buf);

            // Decrypt the data.
            //
            // Note: Vala 0.48 bug here if using ternary operator.
            uint8[] plain;
            if (this.use_pwd) plain = decrypt(buf);
            else              plain = (owned) buf;
            GLib.assert(!ToxEncrypt.is_data_encrypted(plain));

            // Load config
            var bytes = new GLib.Bytes(plain);

            try
            {
                // Load config from memory
                this.cfg.load_from_bytes(bytes, GLib.KeyFileFlags.KEEP_COMMENTS);

                // Set default values for keys which are not set
                try { this.cfg.get_boolean("main", "use_win_pos_x_y"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("main", "use_win_pos_x_y", false); };

                try { this.cfg.get_integer("main", "x"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "x", 0); };

                try { this.cfg.get_integer("main", "y"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "y", 0); };

                try { this.cfg.get_boolean("main", "use_win_size"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("main", "use_win_size", false); };

                try { this.cfg.get_integer("main", "width"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "width", (int) View.FrmMain.WIDTH); };

                try { this.cfg.get_integer("main", "height"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "height", (int) View.FrmMain.HEIGHT); };

                try { this.cfg.get_integer("main", "sash_pos"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sash_pos", (int) View.FrmMain.SPLITTER_VERTICAL_SASH_POS); };

                try { this.cfg.get_integer("main", "sash_tab1_pos"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sash_tab1_pos", (int) View.FrmMain.SPLITTER_VERTICAL_TAB1_SASH_POS); };

                try { this.cfg.get_integer("main", "show_buddies"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "show_buddies", cfg_main_show_buddies_t.SHOW_ALL); };

                try { this.cfg.get_integer("main", "sort_buddies"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_buddies", cfg_main_sort_buddies_t.ADD_LST); };

                try { this.cfg.get_integer("main", "sort_buddies_order"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_buddies_order", cfg_main_sort_buddies_order_t.ASC); };

                try { this.cfg.get_boolean("main", "sort_buddies_unread_top"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("main", "sort_buddies_unread_top", true); };

                try { this.cfg.get_integer("main", "sort_conferences"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_conferences", cfg_main_sort_conferences_t.ADD_LST); };

                try { this.cfg.get_integer("main", "sort_conferences_order"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_conferences_order", cfg_main_sort_conferences_order_t.ASC); };

                try { this.cfg.get_boolean("main", "sort_conferences_unread_top"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("main", "sort_conferences_unread_top", true); };

                try { this.cfg.get_integer("main", "sort_members"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_members", cfg_main_sort_members_t.DISPLAY_NAME); };

                try { this.cfg.get_integer("main", "sort_members_order"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("main", "sort_members_order", cfg_main_sort_members_order_t.ASC); };

                try { this.cfg.get_integer("appearance", "avatar_size"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("appearance", "avatar_size", cfg_appearance_avatar_size_t.BIG); };

                try { this.cfg.get_string("privacy", "send_typing_notifications"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("privacy", "send_typing_notifications", true); };

                try { this.cfg.get_string_list("network", "nodes"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_string_list("network", "nodes", { "URL: " + Model.NODES_URL, "URL: " + Model.NODES_URL_ALT, "File: " + Model.NODES_FILE }); };

                try { this.cfg.get_boolean_list("network", "en_nodes"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean_list("network", "en_nodes", { false, true, true } ); };

                try { this.cfg.get_boolean("network", "allow_udp"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("network", "allow_udp", true); };

                try { this.cfg.get_integer("network", "proxy_type"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("network", "proxy_type", cfg_net_proxy_type_t.NONE); };

                try { this.cfg.get_string("network", "proxy_host"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_string("network", "proxy_host", ""); };

                try { this.cfg.get_integer("network", "proxy_port"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("network", "proxy_port", 0); };

                try { this.cfg.get_integer("audio", "sys"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("audio", "sys", cfg_audio_sys_t.AUTO); };

                try { this.cfg.get_string("audio", "cap_dev"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_string("audio", "cap_dev", ""); };

                try { this.cfg.get_integer("audio", "vol"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("audio", "vol", 50); };

                try { this.cfg.get_string("audio", "playback_dev"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_string("audio", "playback_dev", ""); };

                try { this.cfg.get_integer("audio", "gain"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("audio", "gain", 50); };
            }
            catch (GLib.KeyFileError ex)
            {
                // Set default values for all keys
                cfg_init(model);

                // Pass exception выще
                throw ex;
            }
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Encrypt and save profile-dependent application settings to
         * configuration file from memory to disk
         *
         * @param model Model
         */
        public void cfg_save(Model *model) throws ErrProfileEncryption, GLib.FileError
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion
            GLib.assert(this.cfg != null);

            // Save config to memory
            string plain = this.cfg.to_data();

            // Encrypt the data
            uint8[] buf = this.use_pwd ?
                encrypt(plain.data) :
                plain.data;
            GLib.assert( !this.use_pwd ||
                        ( this.use_pwd && ToxEncrypt.is_data_encrypted(buf)));

            // Write encrypted database from memory to disk
            string filepath = cfg_filepath_get(model);
            GLib.FileUtils.set_data(filepath, buf);
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Rename configuration file on disk
         *
         * @param model        Model
         * @param profile_name New profile name
         */
        public void cfg_rename(Model *model, string profile_name) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Configuration file paths
            string dir = GLib.Path.build_filename(model->path_data, this.profile_name);

            string filename_old;
            string filename_new;

            if (this.use_pwd)
            {
                filename_old = this.profile_name + ".cfg.bin";
                filename_new = profile_name      + ".cfg.bin";
            }
            else
            {
                filename_old = this.profile_name + ".cfg";
                filename_new = profile_name      + ".cfg";
            }

            string filepath_old = GLib.Path.build_filename(dir, filename_old);
            string filepath_new = GLib.Path.build_filename(dir, filename_new);

            // Rename configuration file on disk
            if (GLib.FileUtils.rename(filepath_old, filepath_new) == -1)
                throw new ErrProfileGeneral.CANNOT_RENAME_FILE(_("Can't rename file %s to %s: %s"), filepath_old,
                                                                                                    filepath_new,
                                                                                                    Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Delete configuration file from disk
         *
         * @param model Model
         */
        public void cfg_del(Model *model) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should no be a one-time seSSion

            // Delete configuration file from disk
            string filepath = cfg_filepath_get(model);
            if (GLib.FileUtils.remove(filepath) == -1)
                throw new ErrProfileGeneral.CANNOT_DEL_FILE(_("Can't delete file %s: %s"), filepath, Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Initialize database
         *
         * @param model Model
         */
        public void db_init(Model *model) throws ErrProfileDB
        {
            // Open in-memory database
            int res = Sqlite.Database.open(":memory:", out this.db);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.OPEN_ERR(_("Database error: %s.\nError code: [%d]."), this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Load database from disk to memory and decrypt
          *
          * @param model Model
          */
        public void db_load(Model *model) throws GLib.FileError, ErrProfileDecryption
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Load encrypted database from disk to memory
            uint8[] buf;
            string filepath = db_filepath_get(model);
            GLib.FileUtils.get_data(filepath, out buf);

            // Decrypt the data.
            //
            // Note: Vala 0.48 bug here if using ternary operator.
            uint8[] plain;
            if (this.use_pwd) plain = decrypt(buf);
            else              plain = (owned) buf;
            GLib.assert(!ToxEncrypt.is_data_encrypted(plain));

            // Copy plain data to database pages
            Sqlite.SSE.raw_write(this.db, 0, plain);
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Encrypt and save database from memory to disk
          *
          * @param model Model
          */
        public void db_save(Model *model) throws ErrProfileEncryption, GLib.FileError
        {
            GLib.assert(this.profile_name != null); // Should no be a one-time seSSion

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Copy database pages to plain data
            uint8[] plain;
            Sqlite.SSE.raw_read(this.db, 0, out plain);

            // Encrypt the data.
            //
            // Note: Vala 0.48 bug here if using ternary operator.
            uint8[] buf;
            if (this.use_pwd) buf = encrypt(plain);
            else              buf = (owned) plain;
            GLib.assert( !this.use_pwd ||
                        ( this.use_pwd && ToxEncrypt.is_data_encrypted(buf)));

            // Write encrypted database from memory to disk
            string filepath = db_filepath_get(model);
            FileUtils.set_data(filepath, buf);
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Rename database on disk
         *
         * @param model        Model
         * @param profile_name New profile name
         */
        public void db_rename(Model *model, string profile_name) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should not be an one-time seSSion

            // Configuration file paths
            string dir = GLib.Path.build_filename(model->path_data, this.profile_name);

            string filename_old;
            string filename_new;

            if (this.use_pwd)
            {
                filename_old = this.profile_name + ".sqlite3.bin";
                filename_new = profile_name      + ".sqlite3.bin";
            }
            else
            {
                filename_old = this.profile_name + ".sqlite3";
                filename_new = profile_name      + ".sqlite3";
            }

            string filepath_old = GLib.Path.build_filename(dir, filename_old);
            string filepath_new = GLib.Path.build_filename(dir, filename_new);

            // Rename database on disk
            if (GLib.FileUtils.rename(filepath_old, filepath_new) == -1)
                throw new ErrProfileGeneral.CANNOT_RENAME_FILE(_("Can't rename file %s to %s: %s"), filepath_old,
                                                                                                     filepath_new,
                                                                                                     Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Delete database from disk
         *
         * @param model Model
         */
        public void db_del(Model *model) throws ErrProfileGeneral
        {
            GLib.assert(this.profile_name != null); // Should no be a one-time seSSion

            // Delete database from disk
            string filepath = db_filepath_get(model);
            if (GLib.FileUtils.remove(filepath) == -1)
                throw new ErrProfileGeneral.CANNOT_DEL_FILE(_("Can't delete file %s: %s"), filepath, Model.first_ch_lowercase_make(Posix.strerror(GLib.errno)));
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Setup database pragmas
          *
          * @param model Model
          */
        public void db_pragma_setup(Model *model) throws ErrProfileDB
        {
            unowned string sql;
                    string err;
                    int    res;

            unowned string err_fmt = _("Database error: %s");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Setup search with LIKE operator to be case-sensitive
            sql = """
PRAGMA case_sensitive_like = 1""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.PRAGMA_ERR(err_fmt, err);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Create database tables
          *
          * @param model Model
          */
        public void db_create(Model *model) throws ErrProfileDB
        {
            unowned string sql;
                    string err;
                    int    res;

            unowned string err_fmt = _("Database error: %s");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Create profile table
            sql = """
CREATE TABLE IF NOT EXISTS profile (avatar_16x16  BLOB,
                                    avatar_32x32  BLOB,
                                    avatar_normal BLOB)""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create profile table trigger
            sql = """
CREATE TRIGGER IF NOT EXISTS profile_insert_integrity
BEFORE INSERT ON profile
WHEN (SELECT COUNT(*) FROM profile) = 1
BEGIN
    SELECT RAISE(IGNORE);
END""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create buddies table
            sql = """
CREATE TABLE IF NOT EXISTS buddies (buddy_id           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                    uid                INTEGER NOT NULL UNIQUE, -- unique identifier used by libtoxcore
                                    addr               TEXT,
                                    Alias              TEXT,
                                    have_unread_msg    INTEGER NOT NULL, -- 0 means all messages from buddy were read,
                                                                         -- 1 means have unread messages from buddy
                                    datetime_add_lst   TEXT NOT NULL,
                                    datetime_seen_last TEXT,
                                    avatar_16x16       BLOB,
                                    avatar_32x32       BLOB,
                                    avatar_normal      BLOB)""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create conferences table
            sql = """
CREATE TABLE IF NOT EXISTS conferences (conference_id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                        uid              INTEGER NOT NULL UNIQUE, -- unique identifier used by libtoxcore
                                        Alias            TEXT,
                                        have_unread_msg  INTEGER NOT NULL, -- 0 means all messages from conference were read,
                                                                           -- 1 means have unread messages from conference
                                        datetime_add_lst TEXT NOT NULL)""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create conference members table
            sql = """
CREATE TABLE IF NOT EXISTS members (member_id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                    key           BLOB NOT NULL,
                                    fg            INTEGER NOT NULL,
                                    bg            INTEGER NOT NULL,
                                    conference_id INTEGER NOT NULL,

                                    UNIQUE(key, conference_id)
                                    FOREIGN KEY(conference_id) REFERENCES conferences (conference_id))""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create conversations with buddies table
            sql = """
CREATE TABLE IF NOT EXISTS conv_buddies (msg_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                         name     TEXT NOT NULL,
                                         am_i     INTEGER NOT NULL, -- 0 means message is issued from peer side,
                                                                    -- 1 means message is issued from our side
                                         msg      TEXT NOT NULL,
                                         datetime TEXT NOT NULL,
                                         buddy_id INTEGER NOT NULL,

                                         FOREIGN KEY(buddy_id) REFERENCES buddies (buddy_id))""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create conversations in conferences table
            sql = """
CREATE TABLE IF NOT EXISTS conv_conferences (msg_id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                             name          TEXT NOT NULL,
                                             fg            INTEGER NOT NULL,
                                             bg            INTEGER NOT NULL,
                                             msg           TEXT NOT NULL,
                                             datetime      TEXT NOT NULL,
                                             conference_id INTEGER NOT NULL,

                                             FOREIGN KEY(conference_id) REFERENCES conferences (conference_id))""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create buddy requests table
            sql = """
CREATE TABLE IF NOT EXISTS buddy_reqs (req_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       key      BLOB NOT NULL,
                                       msg      TEXT NOT NULL,
                                       datetime TEXT NOT NULL,
                                       
                                       UNIQUE(key))""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }

            // Create conference invitations table
            sql = """
CREATE TABLE IF NOT EXISTS conference_invitations (invitation_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                                   type          INTEGER NOT NULL, -- 0 means the conference is text only, 1 means it is AV-conference
                                                   cookie        BLOB NOT NULL,
                                                   datetime      TEXT NOT NULL,
                                                   buddy_id      INTEGER NOT NULL,

                                                   UNIQUE(cookie)
                                                   FOREIGN KEY(buddy_id) REFERENCES buddies (buddy_id))""";
            res = this.db.exec(sql, null, out err);

            if (res != Sqlite.OK)
            {
                this.db = null;
                throw new ErrProfileDB.CREATE_ERR(err_fmt, err);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select profile from database
         *
         * @param avatar_16x16  Avatar, small 16x16 icon in PNG
         * @param avatar_32x32  Avatar, large 32x32 icon in PNG
         * @param avatar_normal Avatar, 200x200 (or smaller) image in
         *                      PNG
         */
        public void db_profile_sel(out uint8[]? avatar_16x16,
                                   out uint8[]? avatar_32x32,
                                   out uint8[]? avatar_normal) throws ErrProfileDB
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            avatar_16x16  = null;
            avatar_32x32  = null;
            avatar_normal = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Select from "profile" table:
            //  - prepare SQL query,
            sql = """
SELECT avatar_16x16, avatar_32x32, avatar_normal
FROM   profile""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.ROW)
                throw new ErrProfileDB.SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            if (stmt.column_type(0) != Sqlite.NULL)
            {
                int len = stmt.column_bytes(0);
                avatar_16x16 = new uint8[len];
                if (len > 0)
                    Posix.memcpy(avatar_16x16, stmt.column_blob(0), len);
            }

            if (stmt.column_type(1) != Sqlite.NULL)
            {
                int len = stmt.column_bytes(1);
                avatar_32x32 = new uint8[len];
                if (len > 0)
                    Posix.memcpy(avatar_32x32, stmt.column_blob(1), len);
            }

            if (stmt.column_type(2) != Sqlite.NULL)
            {
                int len = stmt.column_bytes(2);
                avatar_normal = new uint8[len];
                if (len > 0)
                    Posix.memcpy(avatar_normal, stmt.column_blob(2), len);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new buddy into database table "profile"
         *
         * @param avatar_16x16  Avatar, small 16x16 icon in PNG
         * @param avatar_32x32  Avatar, large 32x32 icon in PNG
         * @param avatar_normal Avatar, 200x200 (or smaller) image in
         *                      PNG
         */
        public void db_profile_insert(uint8[]? avatar_16x16,
                                      uint8[]? avatar_32x32,
                                      uint8[]? avatar_normal) throws ErrProfileDB
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;
            int pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Insert into "profile" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO profile
VALUES                ($AVATAR_16X16, $AVATAR_32X32, $AVATAR_NORMAL)""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$AVATAR_16X16");
            GLib.assert(pos > 0);
            if (avatar_16x16 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_16x16, avatar_16x16.length);

            pos = stmt.bind_parameter_index("$AVATAR_32X32");
            GLib.assert(pos > 0);
            if (avatar_32x32 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_32x32, avatar_32x32.length);

            pos = stmt.bind_parameter_index("$AVATAR_NORMAL");
            GLib.assert(pos > 0);
            if (avatar_normal == null) stmt.bind_null(pos);
            else                       stmt.bind_blob(pos, avatar_normal, avatar_normal.length);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update profile in database table "profile"
         *
         * @param avatar_16x16  Avatar, small 16x16 icon in PNG
         * @param avatar_32x32  Avatar, large 32x32 icon in PNG
         * @param avatar_normal Avatar, 200x200 (or smaller) image in
         *                      PNG
         */
        public void db_profile_upd(uint8[]? avatar_16x16,
                                   uint8[]? avatar_32x32,
                                   uint8[]? avatar_normal) throws ErrProfileDB
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;
            int pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Update "profile" table:
            //  - prepare SQL query,
            sql = """
UPDATE profile
SET    avatar_16x16  = $AVATAR_16X16,
       avatar_32x32  = $AVATAR_32X32,
       avatar_normal = $AVATAR_NORMAL""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$AVATAR_16X16");
            GLib.assert(pos > 0);
            if (avatar_16x16 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_16x16, avatar_16x16.length);

            pos = stmt.bind_parameter_index("$AVATAR_32X32");
            GLib.assert(pos > 0);
            if (avatar_32x32 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_32x32, avatar_32x32.length);

            pos = stmt.bind_parameter_index("$AVATAR_NORMAL");
            GLib.assert(pos > 0);
            if (avatar_normal == null) stmt.bind_null(pos);
            else                       stmt.bind_blob(pos, avatar_normal, avatar_normal.length);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select buddy from database table "buddies"
         *
         * @param stmt               Database statement. Pass null for
         *                           the first time you call this
         *                           method.
         * @param add_lst            Added to buddy list number
         * @param uid                Buddy unique identifier
         * @param addr               Tox ID
         * @param Alias              Local Alias
         * @param have_unread_msg    Buddy.unread_t.FALSE: all messages
         *                               from buddy were read,
         *                           Buddy.unread_t.TRUE:  have unread
         *                               message from buddy
         * @param datetime_add_lst   Date and time of the adding to
         *                           buddy list
         * @param datetime_seen_last Last seen date and time
         * @param avatar_16x16       Avatar, small 16x16 icon in PNG
         * @param avatar_32x32       Avatar, large 32x32 icon in PNG
         * @param avatar_normal      Avatar, 200x200 (or smaller) image
         *                           in PNG
         * @param last_msg           Last message from/to the buddy
         *                           number
         * @param datetime_last_msg  Date and time of the last message
         *                           from/to the buddy number
         *
         * @return false: no more buddies are available,
         *         true:  there are more buddies,
         *                call this method again
         */
        public bool db_buddies_sel(ref Sqlite.Statement? stmt,

                                   out int64          add_lst,
                                   out uint32         uid,
                                   out string?        addr,
                                   out string?        Alias,
                                   out Buddy.unread_t have_unread_msg,
                                   out GLib.DateTime? datetime_add_lst,
                                   out GLib.DateTime? datetime_seen_last,
                                   out uint8[]?       avatar_16x16,
                                   out uint8[]?       avatar_32x32,
                                   out uint8[]?       avatar_normal,
                                   out int64          last_msg,
                                   out GLib.DateTime? datetime_last_msg) throws ErrProfileDB
        {
            unowned string sql;
            int res;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            add_lst            = -1;
            uid                = 0;
            addr               = null;
            Alias              = null;
            have_unread_msg    = Buddy.unread_t.NONE;
            avatar_16x16       = null;
            avatar_32x32       = null;
            avatar_normal      = null;
            datetime_add_lst   = null;
            datetime_seen_last = null;
            last_msg           = -1;
            datetime_last_msg  = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Select from "buddies" table:
            //  - prepare SQL query,
            if (stmt == null)
            {
                sql = """
SELECT    buddies.buddy_id, buddies.uid, buddies.addr, buddies.Alias,
              buddies.have_unread_msg, buddies.datetime_add_lst, buddies.datetime_seen_last, buddies.avatar_16x16,
              buddies.avatar_32x32, buddies.avatar_normal, MAX(conv_buddies.msg_id), conv_buddies.datetime
FROM      buddies
LEFT JOIN conv_buddies
USING     (buddy_id)
GROUP BY  buddy_id
ORDER BY  buddy_id""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_add_lst_str;
                unowned string? datetime_seen_last_str;
                unowned string? datetime_last_msg_str;

                add_lst              = stmt.column_int64(0);
                uid                  = (uint32) stmt.column_int64(1);
                have_unread_msg      = stmt.column_int(4) == 0 ? Buddy.unread_t.FALSE : Buddy.unread_t.TRUE;
                datetime_add_lst_str = stmt.column_text(5);

                if (stmt.column_type(2) != Sqlite.NULL)
                    addr = stmt.column_text(2);
                if (stmt.column_type(3) != Sqlite.NULL)
                    Alias = stmt.column_text(3);
                if (stmt.column_type(6) == Sqlite.NULL)
                    datetime_seen_last_str = null;
                else
                    datetime_seen_last_str = stmt.column_text(6);

                if (stmt.column_type(7) != Sqlite.NULL)
                {
                    int len = stmt.column_bytes(7);
                    avatar_16x16 = new uint8[len];
                    if (len > 0)
                        Posix.memcpy(avatar_16x16, stmt.column_blob(7), len);
                }

                if (stmt.column_type(8) != Sqlite.NULL)
                {
                    int len = stmt.column_bytes(8);
                    avatar_32x32 = new uint8[len];
                    if (len > 0)
                        Posix.memcpy(avatar_32x32, stmt.column_blob(8), len);
                }

                if (stmt.column_type(9) != Sqlite.NULL)
                {
                    int len = stmt.column_bytes(9);
                    avatar_normal = new uint8[len];
                    if (len > 0)
                        Posix.memcpy(avatar_normal, stmt.column_blob(9), len);
                }

                if (stmt.column_type(10) == Sqlite.NULL)
                {
                    last_msg              = -1;
                    datetime_last_msg_str = null;
                }
                else
                {
                    last_msg              = stmt.column_int64(10);
                    datetime_last_msg_str = stmt.column_text(11);
                }

                var timezone = new GLib.TimeZone.local();
                datetime_add_lst = new GLib.DateTime.from_iso8601(datetime_add_lst_str, timezone);
                if (datetime_add_lst == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                if (datetime_seen_last_str != null)
                {
                    datetime_seen_last = new GLib.DateTime.from_iso8601(datetime_seen_last_str, timezone);
                    if (datetime_seen_last == null)
                        throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));
                }

                if (datetime_last_msg_str != null)
                {
                    datetime_last_msg = new GLib.DateTime.from_iso8601(datetime_last_msg_str, timezone);
                    if (datetime_last_msg == null)
                        throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));
                }

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new buddy into database table "buddies"
         *
         * @param uid                Buddy unique identifier
         * @param addr               Tox ID
         * @param Alias              Local Alias
         * @param have_unread_msg    Buddy.unread_t.FALSE: all messages
         *                               from buddy were read,
         *                           Buddy.unread_t.TRUE:  have unread
         *                               message from buddy
         * @param datetime_add_lst   Date and time of the adding to
         *                           buddy list
         * @param datetime_seen_last Last seen date and time
         * @param avatar_16x16       Avatar, small 16x16 icon in PNG
         * @param avatar_32x32       Avatar, large 32x32 icon in PNG
         * @param avatar_normal      Avatar, 200x200 (or smaller) image
         *                           in PNG
         *
         * @return Position in buddy list when it sorted by date and
         *         time
         */
        public int64 db_buddies_insert(uint32         uid,
                                       string?        addr,
                                       string?        Alias,
                                       Buddy.unread_t have_unread_msg,
                                       GLib.DateTime  datetime_add_lst,
                                       GLib.DateTime? datetime_seen_last,
                                       uint8[]?       avatar_16x16,
                                       uint8[]?       avatar_32x32,
                                       uint8[]?       avatar_normal) throws ErrProfileDB

            requires (Alias == null || Alias.char_count() > 0)
            requires (have_unread_msg == Buddy.unread_t.FALSE ||
                      have_unread_msg == Buddy.unread_t.TRUE)
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return -1;

            // Insert into "buddies" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO buddies
VALUES                (NULL, $UID, $ADDR, $ALIAS,
                          $HAVE_UNREAD_MSG, $DATETIME_ADD_LST, $DATETIME_SEEN_LAST, $AVATAR_16X16,
                          $AVATAR_32X32, $AVATAR_NORMAL)""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$ADDR");
            GLib.assert(pos > 0);
            if (addr == null) stmt.bind_null(pos);
            else              stmt.bind_text(pos, addr);

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            pos = stmt.bind_parameter_index("$ALIAS");
            GLib.assert(pos > 0);
            if (Alias == null) stmt.bind_null(pos);
            else               stmt.bind_text(pos, Alias);

            pos = stmt.bind_parameter_index("$HAVE_UNREAD_MSG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, have_unread_msg == Buddy.unread_t.TRUE ? 1 : 0);

            pos = stmt.bind_parameter_index("$DATETIME_ADD_LST");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime_add_lst.format("%Y-%m-%d %H:%M:%S"));

            pos = stmt.bind_parameter_index("$DATETIME_SEEN_LAST");
            GLib.assert(pos > 0);
            if (datetime_seen_last == null) stmt.bind_null(pos);
            else                            stmt.bind_text(pos, datetime_seen_last.format("%Y-%m-%d %H:%M:%S"));

            pos = stmt.bind_parameter_index("$AVATAR_16X16");
            GLib.assert(pos > 0);
            if (avatar_16x16 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_16x16, avatar_16x16.length);

            pos = stmt.bind_parameter_index("$AVATAR_32X32");
            GLib.assert(pos > 0);
            if (avatar_32x32 == null) stmt.bind_null(pos);
            else                      stmt.bind_blob(pos, avatar_32x32, avatar_32x32.length);

            pos = stmt.bind_parameter_index("$AVATAR_NORMAL");
            GLib.assert(pos > 0);
            if (avatar_normal == null) stmt.bind_null(pos);
            else                       stmt.bind_blob(pos, avatar_normal, avatar_normal.length);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            return this.db.last_insert_rowid();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update buddy in database table "buddies"
         *
         * @param uid_old             Buddy old unique identifier
         * @param uid_new             Buddy new unique identifier
         * @param Alias               Local Alias
         * @param addr                Tox ID
         * @param have_unread_msg     Buddy.unread_t.FALSE:
         *                                all messages from buddy were
         *                                read
         *                            Buddy.unread_t.TRUE: have unread
         *                                message from buddy
         * @param datetime_seen_last  Last seen date and time
         * @param set_avatar          false: do not change avatar cells,
         *                            true:  change avatar cells,
         * @param avatar_16x16        Avatar, small 16x16 icon in PNG
         * @param avatar_32x32        Avatar, large 32x32 icon in PNG
         * @param avatar_normal       Avatar, 200x200 (or smaller) image
         *                            in PNG
         */
        public void db_buddies_upd(uint32         uid_old,
                                   uint32         uid_new,
                                   string?        addr,
                                   string?        Alias,
                                   Buddy.unread_t have_unread_msg,
                                   GLib.DateTime? datetime_seen_last,
                                   bool           set_avatar,
                                   uint8[]?       avatar_16x16,
                                   uint8[]?       avatar_32x32,
                                   uint8[]?       avatar_normal) throws ErrProfileDB

            requires (have_unread_msg == Buddy.unread_t.FALSE ||
                      have_unread_msg == Buddy.unread_t.TRUE)
            requires ((!set_avatar && avatar_16x16 == null && avatar_32x32 == null && avatar_normal == null) ||
                        set_avatar)
        {
            string           sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Update "buddies" table:
            //  - prepare SQL query,
            sql = """
UPDATE buddies
SET    uid                = $UID_NEW,
       addr               = $ADDR,
       Alias              = $ALIAS,
       have_unread_msg    = $HAVE_UNREAD_MSG,
       """;

            if (set_avatar)
            {
                sql += """
       datetime_seen_last = $DATETIME_SEEN_LAST,
       avatar_16x16       = $AVATAR_16X16,
       avatar_32x32       = $AVATAR_32X32,
       avatar_normal      = $AVATAR_NORMAL""";
            }
            else
            {
                sql += """
       datetime_seen_last = $DATETIME_SEEN_LAST""";
            }

            sql += """
WHERE  uid = $UID_OLD""";

            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$UID_NEW");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid_new);

            pos = stmt.bind_parameter_index("$ADDR");
            GLib.assert(pos > 0);
            if (addr == null) stmt.bind_null(pos);
            else              stmt.bind_text(pos, addr);

            pos = stmt.bind_parameter_index("$ALIAS");
            GLib.assert(pos > 0);
            if (Alias == null) stmt.bind_null(pos);
            else               stmt.bind_text(pos, Alias);

            pos = stmt.bind_parameter_index("$HAVE_UNREAD_MSG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, have_unread_msg == Buddy.unread_t.TRUE ? 1 : 0);

            pos = stmt.bind_parameter_index("$DATETIME_SEEN_LAST");
            GLib.assert(pos > 0);
            if (datetime_seen_last == null) stmt.bind_null(pos);
            else                            stmt.bind_text(pos, datetime_seen_last.format("%Y-%m-%d %H:%M:%S"));

            if (set_avatar)
            {
                pos = stmt.bind_parameter_index("$AVATAR_16X16");
                GLib.assert(pos > 0);
                if (avatar_16x16 == null) stmt.bind_null(pos);
                else                      stmt.bind_blob(pos, avatar_16x16, avatar_16x16.length);

                pos = stmt.bind_parameter_index("$AVATAR_32X32");
                GLib.assert(pos > 0);
                if (avatar_32x32 == null) stmt.bind_null(pos);
                else                      stmt.bind_blob(pos, avatar_32x32, avatar_32x32.length);

                pos = stmt.bind_parameter_index("$AVATAR_NORMAL");
                GLib.assert(pos > 0);
                if (avatar_normal == null) stmt.bind_null(pos);
                else                       stmt.bind_blob(pos, avatar_normal, avatar_normal.length);
            }

            pos = stmt.bind_parameter_index("$UID_OLD");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid_old);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete buddy from database table "buddies"
         *
         * @param uid Buddy unique identifier
         */
        public void db_buddies_del(uint32 uid) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "buddies" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM buddies
WHERE       uid = $UID""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select conference from database table "conferences"
         *
         * @param stmt               Database statement. Pass null for
         *                           the first time you call this
         *                           method.
         * @param add_lst            Added to conference list number
         * @param uid                Conference unique identifier
         * @param Alias              Local Alias
         * @param have_unread_msg    Conference.unread_t.FALSE: all messages
         *                               from conference were read,
         *                           Conference.unread_t.TRUE:  have unread
         *                               message from conference
         * @param datetime_add_lst   Date and time of the adding to
         *                           conference list
         * @param last_msg           Last message from/to the conference
         *                           number
         * @param datetime_last_msg  Date and time of the last message
         *                           from/to the conference number
         *
         * @return false: no more conferences are available,
         *         true:  there are more conferences,
         *                call this method again
         */
        public bool db_conferences_sel(ref Sqlite.Statement? stmt,

                                       out int64               add_lst,
                                       out uint32              uid,
                                       out string?             Alias,
                                       out Conference.unread_t have_unread_msg,
                                       out GLib.DateTime?      datetime_add_lst,
                                       out int64               last_msg,
                                       out GLib.DateTime?      datetime_last_msg) throws ErrProfileDB
        {
            unowned string sql;
            int res;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            add_lst           = -1;
            uid               = 0;
            Alias             = null;
            have_unread_msg   = Conference.unread_t.NONE;
            datetime_add_lst  = null;
            last_msg          = -1;
            datetime_last_msg = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Select from "conferences" table:
            //  - prepare SQL query,
            if (stmt == null)
            {
                sql = """
SELECT    conferences.conference_id, conferences.uid, conferences.Alias, conferences.have_unread_msg,
              conferences.datetime_add_lst, MAX(conv_conferences.msg_id), conv_conferences.datetime
FROM      conferences
LEFT JOIN conv_conferences
USING     (conference_id)
GROUP BY  conference_id
ORDER BY  conference_id""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_add_lst_str;
                unowned string? datetime_last_msg_str;

                add_lst              = stmt.column_int64(0);
                uid                  = (uint32) stmt.column_int64(1);
                have_unread_msg      = stmt.column_int(3) == 0 ? Conference.unread_t.FALSE : Conference.unread_t.TRUE;
                datetime_add_lst_str = stmt.column_text(4);

                if (stmt.column_type(2) != Sqlite.NULL)
                    Alias = stmt.column_text(2);

                if (stmt.column_type(5) == Sqlite.NULL)
                {
                    last_msg              = -1;
                    datetime_last_msg_str = null;
                }
                else
                {
                    last_msg              = stmt.column_int64(5);
                    datetime_last_msg_str = stmt.column_text(6);
                }

                var timezone = new GLib.TimeZone.local();
                datetime_add_lst = new GLib.DateTime.from_iso8601(datetime_add_lst_str, timezone);
                if (datetime_add_lst == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                if (datetime_last_msg_str != null)
                {
                    datetime_last_msg = new GLib.DateTime.from_iso8601(datetime_last_msg_str, timezone);
                    if (datetime_last_msg == null)
                        throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));
                }

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update conference in database table "conferences"
         *
         * @param uid_old            Old conference unique identifier
         * @param uid_new            New conference unique identifier
         * @param Alias              Local Alias
         * @param have_unread_msg    Conference.unread_t.FALSE: all
         *                               messages from conference were
         *                               read
         *                           Conference.unread_t.TRUE: have
         *                               unread message from conference
         */
        public void db_conferences_upd(uint32  uid_old,
                                       uint32  uid_new,
                                       string? Alias,
                                       Conference.unread_t have_unread_msg) throws ErrProfileDB

            requires (have_unread_msg == Conference.unread_t.FALSE ||
                      have_unread_msg == Conference.unread_t.TRUE)
        {
            string           sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Update "conferences" table:
            //  - prepare SQL query,
            sql = """
UPDATE conferences
SET    uid             = $UID_NEW,
       Alias           = $ALIAS,
       have_unread_msg = $HAVE_UNREAD_MSG
WHERE  uid = $UID_OLD""";

            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$UID_NEW");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid_new);

            pos = stmt.bind_parameter_index("$ALIAS");
            GLib.assert(pos > 0);
            if (Alias == null) stmt.bind_null(pos);
            else               stmt.bind_text(pos, Alias);

            pos = stmt.bind_parameter_index("$HAVE_UNREAD_MSG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, have_unread_msg == Conference.unread_t.TRUE ? 1 : 0);

            pos = stmt.bind_parameter_index("$UID_OLD");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid_old);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.UPD_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new conference into database table "conferences"
         *
         * @param uid              Conference unique identifier
         * @param Alias            Local Alias
         * @param have_unread_msg  Conference.unread_t.FALSE: all
         *                             messages from conference were
         *                             read,
         *                         Conference.unread_t.TRUE:  have
         *                             unread message from conference
         * @param datetime_add_lst Date and time of the adding to
         *                         conference list
         *
         * @return Position in conference list when it sorted by date and
         *         time
         */
        public int64 db_conferences_insert(uint32              uid,
                                           string?             Alias,
                                           Conference.unread_t have_unread_msg,
                                           GLib.DateTime       datetime_add_lst) throws ErrProfileDB

            requires (Alias == null || Alias.char_count() > 0)
            requires (have_unread_msg == Conference.unread_t.FALSE ||
                      have_unread_msg == Conference.unread_t.TRUE)
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return -1;

            // Insert into "conferences" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO conferences
VALUES                (NULL, $UID, $ALIAS, $HAVE_UNREAD_MSG,
                          $DATETIME_ADD_LST)""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            pos = stmt.bind_parameter_index("$ALIAS");
            GLib.assert(pos > 0);
            if (Alias == null) stmt.bind_null(pos);
            else               stmt.bind_text(pos, Alias);

            pos = stmt.bind_parameter_index("$HAVE_UNREAD_MSG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, have_unread_msg == Conference.unread_t.TRUE ? 1 : 0);

            pos = stmt.bind_parameter_index("$DATETIME_ADD_LST");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime_add_lst.format("%Y-%m-%d %H:%M:%S"));

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            return this.db.last_insert_rowid();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete conference from database table "conferences"
         *
         * @param uid Conference unique identifier
         */
        public void db_conferences_del(uint32 uid) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "conferences" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM conferences
WHERE       uid = $UID""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select member from database table "members"
         *
         * @param stmt Database statement. Pass null for the first time
         *             you call this method.
         * @param key  Public key
         * @param fg   Foreground colour
         * @param bg   Background colour
         * @param uid  Conference unique identifier
         */
        public bool db_members_sel(ref Sqlite.Statement? stmt,

                                   ref uint8[]? key,
                                   out uint     fg,
                                   out uint     bg,
                                       uint32   uid) throws ErrProfileDB
        {
            string sql;
            int    res;
            int    pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            fg = 0;
            bg = 0;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "conferences" table:
                //  - prepare SQL query,
                sql = """
SELECT members.key, members.fg, members.bg
FROM   members
JOIN   conferences
USING  (conference_id)
WHERE  conferences.uid = $UID""";
                if (key != null)
                    sql += """
AND    members.key     = $KEY""";

                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

                //  - bind parameters to SQL query
                pos = stmt.bind_parameter_index("$UID");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                if (key != null)
                {
                    pos = stmt.bind_parameter_index("$KEY");
                    GLib.assert(pos > 0);
                    stmt.bind_blob(pos, key, key.length);
                }
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                if (key == null)
                {
                    int len = stmt.column_bytes(0);
                    key = new uint8[len];
                    if (len > 0)
                        Posix.memcpy(key, stmt.column_blob(0), len);
                }

                fg = (uint) stmt.column_int64(1);
                bg = (uint) stmt.column_int64(2);

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new member into database table "members"
         *
         * @param key Public key
         * @param fg  Foreground colour
         * @param bg  Background colour
         * @param uid Conference unique identifier
         */
        public void db_members_insert(uint8[] key,
                                      uint    fg,
                                      uint    bg,
                                      uint32  uid) throws ErrProfileDB
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;
            int pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Insert into "members" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO members
VALUES                (-- member_id
                       NULL,

                       -- key
                       $KEY,

                       -- fg
                       $FG,

                       -- bg,
                       $BG,

                       -- conference_id
                       (SELECT conference_id
                        FROM   conferences
                        WHERE  uid = $UID))""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$KEY");
            GLib.assert(pos > 0);
            stmt.bind_blob(pos, key, key.length);

            pos = stmt.bind_parameter_index("$FG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, (int) fg);

            pos = stmt.bind_parameter_index("$BG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, (int) bg);

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete members of specified conference from database table
         * "members"
         *
         * @param uid Conference unique identifier
         */
        public void db_members_del(uint32 uid) throws ErrProfileDB
        {
            string           sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "members" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM members
WHERE       conference_id = (SELECT conference_id
                             FROM   conferences
                             WHERE  uid = $UID)""";

            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select next message from database table "conv_buddies"
         * written by/for peer with specific uid
         *
         * @param stmt          Database statement. Pass null for the
         *                              first time you call this method.
         * @param name          Message author
         * @param uid           Buddy unique identifier
         * @param am_i          false: message is issued from peer side,
         *                      true:  message is issued from our side
         * @param msg           Message
         * @param datetime      Date and time
         * @param datetime_from Date and time from or null if no limit
         * @param datetime_to   Date and time to or null if no limt
         * @param limit         Maximum number of messages. Use -1 if no
         *                      limit should be used.
         *
         * @return false: no more messages are available,
         *         true:  there are more messages,
         *                call this method again
         */
        public bool db_conv_buddies_sel(ref Sqlite.Statement? stmt,

                                        out string? name,
                                        uint32      uid,
                                        out bool    am_i,
                                        out string? msg,
                                        out GLib.DateTime? datetime,

                                        GLib.DateTime? datetime_from,
                                        GLib.DateTime? datetime_to,

                                        int limit) throws ErrProfileDB
        {
            string sql;
            int    res;
            int    pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            name     = null;
            am_i     = false;
            msg      = null;
            datetime = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "conv_buddies" table:
                //  - prepare SQL query,
                sql = """
SELECT   conv_buddies.name, conv_buddies.am_i, conv_buddies.msg, conv_buddies.datetime
FROM     conv_buddies
JOIN     buddies
USING    (buddy_id)
WHERE    buddies.uid = $UID""";
                if (datetime_from != null)
                    sql += """
AND      conv_buddies.datetime >= $DATETIME_FROM""";
                if (datetime_to != null)
                    sql += """
AND      conv_buddies.datetime <= $DATETIME_TO""";
                sql += """
ORDER BY conv_buddies.msg_id ASC
LIMIT    """ + limit.to_string() + """ OFFSET 0""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

                //  - bind parameters to SQL query
                pos = stmt.bind_parameter_index("$UID");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                if (datetime_from != null)
                {
                    pos = stmt.bind_parameter_index("$DATETIME_FROM");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, datetime_from.format("%Y-%m-%d %H:%M:%S"));
                }

                if (datetime_to != null)
                {
                    pos = stmt.bind_parameter_index("$DATETIME_TO");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, datetime_to.format("%Y-%m-%d %H:%M:%S"));
                }
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_str;

                name         = stmt.column_text(0);
                msg          = stmt.column_text(2);
                datetime_str = stmt.column_text(3);

                if (name         == null ||
                    msg          == null ||
                    datetime_str == null)
                {
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                }

                am_i     = stmt.column_int(1) == 0 ? false : true;
                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());

                if (datetime == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new message into database table "conv_buddies"
         *
         * @param name     Message author
         * @param uid      Buddy unique identifier
         * @param am_i     false: message is issued from peer side,
         *                 true:  message is issued from our side
         * @param msg      Message
         * @param datetime Date and time
         *
         * @return Last message from/to the buddy number
         */
        public int64 db_conv_buddies_insert(string        name,
                                            uint32        uid,
                                            bool          am_i,
                                            owned string  msg,
                                            GLib.DateTime datetime) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return -1;

            // Insert into "conv_buddies" table:
            //  - prepare SQL query,
            sql = """
INSERT INTO conv_buddies
VALUES      (NULL, $NAME, $AM_I, $MSG, $DATETIME, (SELECT buddy_id
                                                   FROM   buddies
                                                   WHERE  uid = $UID))""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$NAME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, name);

            pos = stmt.bind_parameter_index("$AM_I");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, am_i ? 1 : 0);

            pos = stmt.bind_parameter_index("$MSG");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, msg);

            pos = stmt.bind_parameter_index("$DATETIME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime.format("%Y-%m-%d %H:%M:%S"));

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            return this.db.last_insert_rowid();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete all messages to/from specified buddy from database
         * table "conv_buddies"
         *
         * @param uid Buddy unique identifier
         * @param datetime_from Date and time from or null if no limit
         * @param datetime_to   Date and time to or null if no limt
         */
        public void db_conv_buddies_del(uint32 uid,

                                        GLib.DateTime? datetime_from,
                                        GLib.DateTime? datetime_to) throws ErrProfileDB
        {
            string           sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "conv_buddies" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM conv_buddies
WHERE       buddy_id = (SELECT buddy_id
                        FROM   buddies
                        WHERE  uid = $UID)""";
            if (datetime_from != null)
                sql += """
AND         datetime >= $DATETIME_FROM""";
            if (datetime_to != null)
                sql += """
AND         datetime <= $DATETIME_TO""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            if (datetime_from != null)
            {
                pos = stmt.bind_parameter_index("$DATETIME_FROM");
                GLib.assert(pos > 0);
                stmt.bind_text(pos, datetime_from.format("%Y-%m-%d %H:%M:%S"));
            }

            if (datetime_to != null)
            {
                pos = stmt.bind_parameter_index("$DATETIME_TO");
                GLib.assert(pos > 0);
                stmt.bind_text(pos, datetime_to.format("%Y-%m-%d %H:%M:%S"));
            }

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select next date and time from database table "conv_buddies"
         * at which there have been written messages by/for peer with
         * specific uid
         *
         * @param stmt     Database statement. Pass null for the first
         *                 time you call this method.
         * @param uid      Buddy unique identifier
         * @param find_str Restrict messages by this string or null to
         *                 select all messages from database
         * @param datetime Date and time
         *
         * @return false: no more dates are available,
         *         true:  there are more dates,
         *                call this method again
         */
        public bool db_conv_buddies_datetime_sel(ref Sqlite.Statement? stmt,
                                                 uint32 uid,
                                                 string? find_str,
                                                 out GLib.DateTime? datetime) throws ErrProfileDB

            requires (find_str == null || find_str.char_count() > 0)
        {
            string sql;
            int    res;
            int    pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            datetime = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "conv_buddies" table:
                //  - prepare SQL query,
                sql = """
SELECT tbl2.datetime
FROM

(
    SELECT   strftime('%Y-%m-%d', conv_buddies.datetime) as date
    FROM     conv_buddies
    JOIN     buddies
    USING    (buddy_id)
    WHERE    buddies.uid = $UID_TBL1""";
                if (find_str != null)
                    sql += """
    AND      '(' || conv_buddies.datetime || ') ' || conv_buddies.name || ': ' || conv_buddies.msg LIKE $FIND_STR ESCAPE '\'""";
                    sql += """
    GROUP BY date
    ORDER BY conv_buddies.datetime DESC
) AS tbl1

LEFT JOIN

(
    SELECT   strftime('%Y-%m-%d', conv_buddies.datetime) AS date, conv_buddies.datetime AS datetime
    FROM     conv_buddies   
    JOIN     buddies
    USING    (buddy_id)
    WHERE    buddies.uid = $UID_TBL2
    GROUP BY date
    ORDER BY conv_buddies.datetime DESC
) AS tbl2

USING (date)""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

                //  - bind parameters to SQL query
                pos = stmt.bind_parameter_index("$UID_TBL1");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                pos = stmt.bind_parameter_index("$UID_TBL2");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                string escaped_find_str;

                if (find_str != null)
                {
                    try
                    {
                        var pattern      = new GLib.Regex("(%|_)");
                        escaped_find_str = pattern.replace(find_str,
                                                           -1, // String is NULL-terminated
                                                           0,  // Start from zero position
                                                           "\\\\\\1");
                    }
                    catch (GLib.RegexError ex)
                    {
                        escaped_find_str = "";
                        GLib.assert(false);
                    }

                    pos = stmt.bind_parameter_index("$FIND_STR");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, "%" + escaped_find_str + "%");
                }
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_str = stmt.column_text(0);
                if (datetime_str == null)
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());
                if (datetime == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select next message from database table "conv_conferences"
         * written by/for peer with specific uid
         *
         * @param stmt          Database statement. Pass null for the
         *                              first time you call this method.
         * @param name          Message author
         * @param uid           Conference unique identifier
         * @param fg            Author foreground colour
         * @param bg            Author background colour
         * @param msg           Message
         * @param datetime      Date and time
         * @param datetime_from Date and time from or null if no limit
         * @param datetime_to   Date and time to or null if no limt
         * @param limit         Maximum number of messages. Use -1 if no
         *                      limit should be used.
         *
         * @return false: no more messages are available,
         *         true:  there are more messages,
         *                call this method again
         */
        public bool db_conv_conferences_sel(ref Sqlite.Statement? stmt,

                                            out string? name,
                                            uint32      uid,
                                            out uint    fg,
                                            out uint    bg,
                                            out string? msg,
                                            out GLib.DateTime? datetime,

                                            GLib.DateTime? datetime_from,
                                            GLib.DateTime? datetime_to,

                                            int limit) throws ErrProfileDB
        {
            string sql;
            int    res;
            int    pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            name     = null;
            fg       = 0;
            bg       = 0;
            msg      = null;
            datetime = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "conv_conferences" table:
                //  - prepare SQL query,
                sql = """
SELECT   conv_conferences.name, conv_conferences.fg, conv_conferences.bg, conv_conferences.msg,
             conv_conferences.datetime
FROM     conv_conferences
JOIN     conferences
USING    (conference_id)
WHERE    conferences.uid = $UID""";
                if (datetime_from != null)
                    sql += """
AND      conv_conferences.datetime >= $DATETIME_FROM""";
                if (datetime_to != null)
                    sql += """
AND      conv_conferences.datetime <= $DATETIME_TO""";
                sql += """
ORDER BY conv_conferences.msg_id ASC
LIMIT    """ + limit.to_string() + """ OFFSET 0""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

                //  - bind parameters to SQL query
                pos = stmt.bind_parameter_index("$UID");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                if (datetime_from != null)
                {
                    pos = stmt.bind_parameter_index("$DATETIME_FROM");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, datetime_from.format("%Y-%m-%d %H:%M:%S"));
                }

                if (datetime_to != null)
                {
                    pos = stmt.bind_parameter_index("$DATETIME_TO");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, datetime_to.format("%Y-%m-%d %H:%M:%S"));
                }
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_str;

                name         = stmt.column_text(0);
                msg          = stmt.column_text(3);
                datetime_str = stmt.column_text(4);

                if (name         == null ||
                    msg          == null ||
                    datetime_str == null)
                {
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                }

                fg = stmt.column_int(1);
                bg = stmt.column_int(2);

                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());

                if (datetime == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new message into database table "conv_conferences"
         *
         * @param name     Message author
         * @param uid      Conference unique identifier
         * @param fg       Author foreground colour
         * @param bg       Author background colour
         * @param msg      Message
         * @param datetime Date and time
         *
         * @return Last message from/to the conference number
         */
        public int64 db_conv_conferences_insert(string        name,
                                                uint32        uid,
                                                uint          fg,
                                                uint          bg,
                                                owned string  msg,
                                                GLib.DateTime datetime) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return -1;

            // Insert into "conv_conferences" table:
            //  - prepare SQL query,
            sql = """
INSERT INTO conv_conferences
VALUES      (NULL, $NAME, $FG, $BG,
                $MSG, $DATETIME, (SELECT conference_id
                                  FROM   conferences
                                  WHERE  uid = $UID))""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$NAME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, name);

            pos = stmt.bind_parameter_index("$FG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, (int) fg);

            pos = stmt.bind_parameter_index("$BG");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, (int) bg);

            pos = stmt.bind_parameter_index("$MSG");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, msg);

            pos = stmt.bind_parameter_index("$DATETIME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime.format("%Y-%m-%d %H:%M:%S"));

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            return this.db.last_insert_rowid();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete all messages to/from specified conference from
         * database table "conv_conferences"
         *
         * @param uid Conference unique identifier
         * @param datetime_from Date and time from or null if no limit
         * @param datetime_to   Date and time to or null if no limt
         */
        public void db_conv_conferences_del(uint32 uid,

                                            GLib.DateTime? datetime_from,
                                            GLib.DateTime? datetime_to) throws ErrProfileDB
        {
            string           sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "conv_conferences" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM conv_conferences
WHERE       conference_id = (SELECT conference_id
                             FROM   conferences
                             WHERE  uid = $UID)""";
            if (datetime_from != null)
                sql += """
AND         datetime >= $DATETIME_FROM""";
            if (datetime_to != null)
                sql += """
AND         datetime <= $DATETIME_TO""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            if (datetime_from != null)
            {
                pos = stmt.bind_parameter_index("$DATETIME_FROM");
                GLib.assert(pos > 0);
                stmt.bind_text(pos, datetime_from.format("%Y-%m-%d %H:%M:%S"));
            }

            if (datetime_to != null)
            {
                pos = stmt.bind_parameter_index("$DATETIME_TO");
                GLib.assert(pos > 0);
                stmt.bind_text(pos, datetime_to.format("%Y-%m-%d %H:%M:%S"));
            }

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select next date and time from database table
         * "conv_conferences" at which there have been written messages
         * by/for peer with specific uid
         *
         * @param stmt     Database statement. Pass null for the first
         *                 time you call this method.
         * @param uid      Conference unique identifier
         * @param find_str Restrict messages by this string or null to
         *                 select all messages from database
         * @param datetime Date and time
         *
         * @return false: no more dates are available,
         *         true:  there are more dates,
         *                call this method again
         */
        public bool db_conv_conferences_datetime_sel(ref Sqlite.Statement? stmt,
                                                     uint32 uid,
                                                     string? find_str,
                                                     out GLib.DateTime? datetime) throws ErrProfileDB
            requires (find_str == null || find_str.char_count() > 0)
        {
            string sql;
            int    res;
            int    pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            datetime = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "conv_conferences" table:
                //  - prepare SQL query,
                sql = """
SELECT tbl2.datetime
FROM

(
    SELECT   strftime('%Y-%m-%d', conv_conferences.datetime) as date
    FROM     conv_conferences
    JOIN     conferences
    USING    (conference_id)
    WHERE    conferences.uid = $UID_TBL1""";
                if (find_str != null)
                    sql += """
    AND      '(' || conv_conferences.datetime || ') ' || conv_conferences.name || ': ' || conv_conferences.msg LIKE $FIND_STR ESCAPE '\'""";
                    sql += """
    GROUP BY date
    ORDER BY conv_conferences.datetime DESC
) AS tbl1

LEFT JOIN

(
    SELECT   strftime('%Y-%m-%d', conv_conferences.datetime) AS date, conv_conferences.datetime AS datetime
    FROM     conv_conferences
    JOIN     conferences
    USING    (conference_id)
    WHERE    conferences.uid = $UID_TBL2
    GROUP BY date
    ORDER BY conv_conferences.datetime DESC
) AS tbl2

USING (date)""";
                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

                //  - bind parameters to SQL query
                pos = stmt.bind_parameter_index("$UID_TBL1");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                pos = stmt.bind_parameter_index("$UID_TBL2");
                GLib.assert(pos > 0);
                stmt.bind_int64(pos, uid);

                string escaped_find_str;

                if (find_str != null)
                {
                    try
                    {
                        var pattern      = new GLib.Regex("(%|_)");
                        escaped_find_str = pattern.replace(find_str,
                                                           -1, // String is NULL-terminated
                                                           0,  // Start from zero position
                                                           "\\\\\\1");
                    }
                    catch (GLib.RegexError ex)
                    {
                        escaped_find_str = "";
                        GLib.assert(false);
                    }

                    pos = stmt.bind_parameter_index("$FIND_STR");
                    GLib.assert(pos > 0);
                    stmt.bind_text(pos, "%" + escaped_find_str + "%");
                }
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                unowned string? datetime_str = stmt.column_text(0);
                if (datetime_str == null)
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());
                if (datetime == null)
                    throw new ErrProfileDB.SEL_ERR(_("Wrong datetime"));

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select buddy request from database table "buddy_reqs"
         *
         * @param stmt     Database statement. Pass null for the first
         *                 time you call this method.
         * @param key      Public key
         * @param msg      Message
         * @param datetime Date and time
         */
        public bool db_buddy_reqs_sel(ref Sqlite.Statement? stmt,

                                      out uint8[]? key,
                                      out string?  msg,
                                      out GLib.DateTime? datetime) throws ErrProfileDB
        {
            unowned string sql;
            int res;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            key      = null;
            msg      = null;
            datetime = null;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "buddy_reqs" table:
                //  - prepare SQL query,
                sql = """
SELECT   key, msg, datetime
FROM     buddy_reqs
ORDER BY datetime ASC""";

                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                int len = stmt.column_bytes(0);
                key = new uint8[len];
                if (len > 0)
                    Posix.memcpy(key, stmt.column_blob(0), len);

                msg = stmt.column_text(1);

                unowned string? datetime_str;
                datetime_str = stmt.column_text(2);
                if (datetime_str == null)
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new buddy request into database table "buddy_reqs"
         *
         * @param key      Public key
         * @param msg      Message
         * @param datetime Date and time
         */
        public void db_buddy_reqs_insert(uint8[]       key,
                                         string        msg,
                                         GLib.DateTime datetime) throws ErrProfileDB
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;
            int pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Insert into "buddy_reqs" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO buddy_reqs
VALUES                (NULL, $KEY, $MSG, $DATETIME)""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$KEY");
            GLib.assert(pos > 0);
            stmt.bind_blob(pos, key, key.length);

            pos = stmt.bind_parameter_index("$MSG");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, msg);

            pos = stmt.bind_parameter_index("$DATETIME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime.format("%Y-%m-%d %H:%M:%S"));

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete buddy request with specified public key from database
         * table "buddy_reqs"
         *
         * @param key Public key
         */
        public void db_buddy_reqs_del(uint8[] key) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "buddy_reqs" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM buddy_reqs
WHERE       key = $KEY""";

            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$KEY");
            GLib.assert(pos > 0);
            stmt.bind_blob(pos, key, key.length);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select conference invitation from database table
         * "conference_invitations"
         *
         * @param stmt     Database statement. Pass null for the first
         *                 time you call this method.
         * @param type     The conference type (text only or
         *                 audio/video)
         * @param cookie   Cookie
         * @param datetime Date and time
         * @param uid      Buddy unique identifier
         */
        public bool db_conference_invitations_sel(ref Sqlite.Statement? stmt,

                                                  out conference_invitation_t type,
                                                  out uint8[]?       cookie,
                                                  out GLib.DateTime? datetime,
                                                  out uint32         uid) throws ErrProfileDB

            ensures (type == conference_invitation_t.NONE ||
                     type == conference_invitation_t.TEXT ||
                     type == conference_invitation_t.AUDIO_VIDEO)
        {
            unowned string sql;
            int res;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // Default values of output parameters
            type     = conference_invitation_t.NONE;
            cookie   = null;
            datetime = null;
            uid      = 0;

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return false;

            // Initialize statement
            if (stmt == null)
            {
                // Select from "buddy_reqs" table:
                //  - prepare SQL query,
                sql = """
SELECT   conference_invitations.type,
             conference_invitations.cookie,
             conference_invitations.datetime,
             buddies.uid
FROM     conference_invitations
JOIN     buddies
USING    (buddy_id)
ORDER BY conference_invitations.datetime ASC""";

                res = this.db.prepare(sql, sql.length, out stmt);
                if (res != Sqlite.OK)
                    throw new ErrProfileDB.PREPARE_SEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
            }

            //  - execute SQL query
            res = stmt.step();

            if (res == Sqlite.ROW)
            {
                type = (conference_invitation_t) stmt.column_int(0);
                
                switch (type)
                {
                case conference_invitation_t.TEXT:        break;
                case conference_invitation_t.AUDIO_VIDEO: break;
                default:
                    throw new ErrProfileDB.SEL_ERR(_("Wrong conference type"));
                    //break;
                }

                int len = stmt.column_bytes(1);
                cookie = new uint8[len];
                if (len > 0)
                    Posix.memcpy(cookie, stmt.column_blob(1), len);

                unowned string? datetime_str;
                datetime_str = stmt.column_text(2);
                if (datetime_str == null)
                    throw new ErrProfileDB.SEL_ERR(_("Null fields"));
                datetime = new GLib.DateTime.from_iso8601(datetime_str, new GLib.TimeZone.local());

                uid = (uint32) stmt.column_int64(3);

                return true;
            }

            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert new conference invitation into database table
         * "conference_invitations"
         *
         * @param type     The conference type (text only or
         *                 audio/video)
         * @param cookie   Cookie
         * @param datetime Date and time
         * @param uid      Buddy unique identifier
         */
        public void db_conference_invitations_insert(conference_invitation_t type,
                                                     uint8[]       cookie,
                                                     GLib.DateTime datetime,
                                                     uint32        uid) throws ErrProfileDB

            requires (type == conference_invitation_t.TEXT ||
                      type == conference_invitation_t.AUDIO_VIDEO)
        {
            unowned string sql;
            int res;
            Sqlite.Statement stmt;
            int pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Insert into "conference_invitations" table:
            //  - prepare SQL query,
            sql = """
INSERT OR IGNORE INTO conference_invitations
VALUES                (NULL, $TYPE, $COOKIE, $DATETIME,
                          (SELECT buddy_id
                           FROM   buddies
                           WHERE  uid = $UID))""";
            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            //  - bind parameters to SQL query,
            pos = stmt.bind_parameter_index("$TYPE");
            GLib.assert(pos > 0);
            stmt.bind_int(pos, (int) type);

            pos = stmt.bind_parameter_index("$COOKIE");
            GLib.assert(pos > 0);
            stmt.bind_blob(pos, cookie, cookie.length);

            pos = stmt.bind_parameter_index("$DATETIME");
            GLib.assert(pos > 0);
            stmt.bind_text(pos, datetime.format("%Y-%m-%d %H:%M:%S"));

            pos = stmt.bind_parameter_index("$UID");
            GLib.assert(pos > 0);
            stmt.bind_int64(pos, uid);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.INSERT_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Delete conference invitation with specified cookie from
         * database table "conference_invitations"
         *
         * @param cookie Cookie
         */
        public void db_conference_invitations_del(uint8[] cookie) throws ErrProfileDB
        {
            unowned string   sql;
            int              res;
            Sqlite.Statement stmt;
            int              pos;

            unowned string err_fmt = _("Database error: %s.\nError code: [%d].");

            // No actions if database is not initialized (corrupted?)
            if (this.db == null)
                return;

            // Delete from "conference_invitations" table:
            //  - prepare SQL query,
            sql = """
DELETE FROM conference_invitations
WHERE       cookie = $COOKIE""";

            res = this.db.prepare(sql, sql.length, out stmt);
            if (res != Sqlite.OK)
                throw new ErrProfileDB.PREPARE_DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());

            pos = stmt.bind_parameter_index("$COOKIE");
            GLib.assert(pos > 0);
            stmt.bind_blob(pos, cookie, cookie.length);

            //  - execute SQL query
            res = stmt.step();
            if (res != Sqlite.DONE)
                throw new ErrProfileDB.DEL_ERR(err_fmt, this.db.errmsg(), this.db.errcode());
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display profile name
          *
          * @return Display profile name
          */
        public string display_profile_name_get()
        {
            string res = this.profile_name == null ? _("None (one-time seSSion)") :
                         this.profile_name;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display name
          *
          * @return Display name
          */
        public override string display_name_get()
        {
            string res;

            if (this.name == null)
                res = this.addr == null ? _("Unknown") :
                                          this.addr;
            else
            {
                if (this.name == "")
                    res = this.addr == null ? this.name :
                                              this.addr;
                else
                    res = this.name;
            }

            return res;
        }
    }
}

