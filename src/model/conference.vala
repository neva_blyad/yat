/*
 *    conference.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * Message send errors in conferences
     */
    public errordomain ErrConferenceInviteBuddy
    {
        /**
         * The client is not connected to the conference
         */
        NO_CONNECTION,

        /**
         * The message packet failed to send
         */
        SEND_FAIL,
    }

    /**
     * This is the conference class
     */
    public class Conference : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Properties
        private string?  title_;
        private string?  Alias_;
        private unread_t have_unread_msg_;
        private int      offline_;
        private int      online_;
        private int64    add_lst_;
        private int64    last_msg_;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Name maximum size.
         *
         * Constants from libtoxcore.
         */
        public static size_t title_max_len = ToxCore.max_name_length();

        /**
         * Unique identifier used by libtoxcore
         */
        public uint32 uid { public get; public set construct; }

        /**
         * Title
         */
        public string? title
        {
            public get
            {
                return this.title_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() <= title_max_len);
                this.title_ = value;
            }
        }

        /**
         * Optional local Alias
         */
        public string? Alias
        {
            public get
            {
                return this.Alias_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() > 0);
                this.Alias_ = value;
            }
        }

        /**
         * Are there unread messages from the conference or not?
         */
        public unread_t have_unread_msg
        {
            public get
            {
                return this.have_unread_msg_;
            }

            public set construct
            {
                GLib.assert(value == unread_t.NONE  ||
                            value == unread_t.FALSE ||
                            value == unread_t.TRUE);
                this.have_unread_msg_ = value;
            }
        }

        /**
         * Number of the currently offline members
         */
        public int offline
        {
            public get
            {
                return this.offline_;
            }

            public set construct
            {
                GLib.assert(offline >= -1);
                this.offline_ = value;
            }
        }

        /**
         * Number of the currently online members
         */
        public int online
        {
            public get
            {
                return this.online_;
            }

            public set construct
            {
                GLib.assert(online >= -1);
                this.online_ = value;
            }
        }

        /**
         * Added to conference list number.
         *
         * Using this member instead of datetime member below is faster
         * and more accurate, because two conferences with the same
         * datetime can exist.
         *
         * There are can be holes in this numeration.
         */
        public int64 add_lst
        {
            public get
            {
                return this.add_lst_;
            }

            public set construct
            {
                GLib.assert(value >= -1);
                this.add_lst_ = value;
            }
        }

        /**
         * Last added to conference list number
         */
        public static uint64 last_add_lst = 1;

        /**
         * Date and time of the adding to conference list
         */
        public GLib.DateTime datetime_add_lst { public get; public set construct; }

        /**
         * Last message from/to the conference number.
         *
         * Using this member instead of datetime member below is faster
         * and more accurate, because two conferences with the same datetime
         * can exist.
         *
         * There are can be holes in this numeration.
         */
        public int64 last_msg
        {
            public get
            {
                return this.last_msg_;
            }

            public set construct
            {
                GLib.assert(value >= -1);
                this.last_msg_ = value;
            }
        }

        /**
         * Date and time of the last message from/to the conference number
         */
        public GLib.DateTime? datetime_last_msg { public get; public set construct; }

        /**
         * Member list with all members.
         * All members are presented here, not only those, which showed
         * in GUI.
         *
         * Represented as hash map.
         */
        public Gee.HashMap<uint32, Member> members_hash { public get; private set; }

        /**
         * Next added to member list number
         */
        public uint64 member_add_lst { public get; public set construct; }

        /*
         * Foreground colour of the next joined member 
         */
        public uint member_fg { public get; public set construct; }

        /*
         * Background colour of the next joined member 
         */
        public uint member_bg { public get; public set construct; }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Are there unread messages from the conference or not?
         */
        public enum unread_t
        {
            /**
             * Read/unread is not set yet
             */
            NONE,

            /**
             * All messages from conference were read
             */
            FALSE,

            /**
             * Have unread message from conference
             */
            TRUE,
        }

        /**
         * Parameter for method recalc()
         */
        public enum recalc_kind_t
        {
            /**
             * The method recalculates the number of offline members in the
             * conference
             */
            OFFLINE,

            /**
             * The method recalculates the number of online members in the
             * conference
             */
            ONLINE,
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Conference constructor
         *
         * @param uid   Unique identifier used by libtoxcore
         * @param Alias Alias
         */
        public Conference(uint32  uid,
                          string? Alias)
        {
            // GObject-style construction
            Object(uid               : uid,
                   title             : null,
                   Alias             : Alias,
                   have_unread_msg   : unread_t.NONE,
                   offline           : -1,
                   online            : -1,
                   add_lst           : -1,
                   last_msg          : -1,
                   datetime_last_msg : null,
                   member_add_lst    : 1,
                   member_fg         : 0,
                   member_bg         : 0);

            this.datetime_add_lst = new GLib.DateTime.now_local();
            this.members_hash     = new Gee.HashMap<uint32, Member>();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to default avatar icon
          *
          * @param is_small false: get large 32x32 icon,
          *                 true:  get small 16x16 icon
          *
          * @return Filepath to default icon
          */
        public static string icon_filepath_default_avatar_get(bool is_small)
        {
            unowned string filename = is_small ? "no_avatar_16x16.gif" : "no_avatar_32x32.gif";
                    string filepath = GLib.Path.build_filename("img", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to unread icon
          *
          * @param is_small false: get large 32x32 icon,
          *                 true:  get small 16x16 icon
          *
          * @return Filepath to icon
          */
        public static string icon_filepath_unread_get(bool is_small)
        {
            unowned string filename = is_small ? "unread_16x16.gif" : "unread_32x32.gif";
                    string filepath = GLib.Path.build_filename("img", "conference", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * How to compare two conferences?
          *
          * This method returns function for using with sorting
          * algorithms. Returned functions can be different according to
          * profile configuration (sorting by, sorting order). Comparing
          * two conferences can be used for placing them in GUI controls.
          *
          * @param model Model
          *
          * @return Compare function
          */
        public static GLib.CompareDataFunc<Conference> cmp_fn_get(Model *model)
        {
            GLib.CompareDataFunc<Conference> res;

            Profile.cfg_main_sort_conferences_t       sort_conferences;
            Profile.cfg_main_sort_conferences_order_t sort_conferences_order;
            bool sort_conferences_unread_top;

            try
            {
                sort_conferences       = (Profile.cfg_main_sort_conferences_t)
                    model->self.cfg.get_integer("main", "sort_conferences");
                sort_conferences_order = (Profile.cfg_main_sort_conferences_order_t)
                    model->self.cfg.get_integer("main", "sort_conferences_order");
                sort_conferences_unread_top =
                    model->self.cfg.get_boolean("main", "sort_conferences_unread_top");
            }
            catch (GLib.KeyFileError ex)
            {
                sort_conferences            = Profile.cfg_main_sort_conferences_t.ADD_LST;
                sort_conferences_order      = Profile.cfg_main_sort_conferences_order_t.ASC;
                sort_conferences_unread_top = false;
                GLib.assert(false);
            }

            switch (sort_conferences)
            {
            case Profile.cfg_main_sort_conferences_t.ADD_LST:

                if (sort_conferences_order != Profile.cfg_main_sort_conferences_order_t.DESC)
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst <  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst <  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }
                else
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst >  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst >  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }

                break;

            case Profile.cfg_main_sort_conferences_t.DISPLAY_NAME:

                if (sort_conferences_order != Profile.cfg_main_sort_conferences_order_t.DESC)
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name >  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                string a_display_name = a.display_name_get();
                                string b_display_name = b.display_name_get();

                                if      (a_display_name >  b_display_name) return  1;
                                else if (a_display_name == b_display_name) return  0;
                                else                                       return -1;
                            };
                    }
                }
                else
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name <  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                string a_display_name = a.display_name_get();
                                string b_display_name = b.display_name_get();

                                if      (a_display_name <  b_display_name) return  1;
                                else if (a_display_name == b_display_name) return  0;
                                else                                       return -1;
                            };
                    }
                }

                break;

            case Profile.cfg_main_sort_conferences_t.LAST_MSG:

                if (sort_conferences_order != Profile.cfg_main_sort_conferences_order_t.DESC)
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if (a.last_msg < b.last_msg)
                                    {
                                        return  1;
                                    }
                                    else if (a.last_msg > b.last_msg)
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name >  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if (a.last_msg < b.last_msg)
                                {
                                    return  1;
                                }
                                else if (a.last_msg > b.last_msg)
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name >  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }
                else
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if (a.last_msg > b.last_msg)
                                    {
                                        return  1;
                                    }
                                    else if (a.last_msg < b.last_msg)
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name <  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if (a.last_msg > b.last_msg)
                                {
                                    return  1;
                                }
                                else if (a.last_msg < b.last_msg)
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name <  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }

                break;

            default:

                if (sort_conferences_order != Profile.cfg_main_sort_conferences_order_t.DESC)
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst <  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst <  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }
                else
                {
                    if (sort_conferences_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst >  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst >  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }

                break;
            }

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display name
          *
          * @return Display name
          */
        public string display_name_get()
        {
            string res;

            if (this.Alias == null)
                res = this.title == null ? _("Unknown") :
                                           this.title;
            else
                res = this.Alias;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display member counters.
          *
          * Contains the currently online and total members.
          *
          * @return Display member counters
          */
        public string display_cnt_get()
        {
            int online = this.online;
            int all    = online + this.offline;

            string online_str = online == -1 ? "?" : online.to_string();
            string all_str    = all    == -1 ? "?" : all.to_string();

            return online_str + "/" + all_str;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Should conference be showed in GUI?
          *
          * @param model      Model
          * @param use_filter false: don't use filtering,
          *                   true:  use filtering,
          * @param pattern    Filter pattern
          */
        public bool should_be_displayed(Model *model, bool use_filter, GLib.Regex? pattern)
        {
            // Use filter?
            if (use_filter)
            {
                // Filter conference.
                // Scans for a display name.
                if (!pattern.match(this.display_name_get()))
                    return false;
            }

            return true;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Invite a buddy
          *
          * @param model Model
          * @param buddy Buddy
          */
        public void invite(Model *model, Buddy buddy) throws ErrConferenceInviteBuddy
        {
            ToxCore.ERR_CONFERENCE_INVITE err;
            bool res = model->tox.conference_invite(buddy.uid,
                                                    this.uid,
                                                    out err);

            switch (err)
            {
            // The function returned successfully
            case ToxCore.ERR_CONFERENCE_INVITE.OK:
                GLib.assert(res);
                break;

            // The conference number passed did not designate a valid
            // conference
            case ToxCore.ERR_CONFERENCE_INVITE.CONFERENCE_NOT_FOUND:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // The invite packet failed to send
            case ToxCore.ERR_CONFERENCE_INVITE.FAIL_SEND:
                GLib.assert(!res);
                throw new ErrConferenceInviteBuddy.SEND_FAIL(_("The invite packet failed to send"));
                //break;

            // The client is not connected to the conference
            case ToxCore.ERR_CONFERENCE_INVITE.NO_CONNECTION:
                GLib.assert(!res);
                throw new ErrConferenceInviteBuddy.NO_CONNECTION(_("The client is not connected to the conference"));
                //break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Recalculate the number of online or offline members
          *
          * @param model Model
          * @param kind  What kind of data the method should recalculate
          *              offline, online or all members
          */
        public void recalc(Model *model, recalc_kind_t kind)
            requires (kind == recalc_kind_t.OFFLINE ||
                      kind == recalc_kind_t.ONLINE)
        {
            ToxCore.ERR_CONFERENCE_PEER_QUERY err = ToxCore.ERR_CONFERENCE_PEER_QUERY.OK;

            switch (kind)
            {
            case recalc_kind_t.OFFLINE:

                this.offline = (int) model->tox.conference_offline_peer_count(this.uid, out err);
                break;

            case recalc_kind_t.ONLINE:

                this.online = (int) model->tox.conference_peer_count(this.uid, out err);
                break;

            default:

                GLib.assert(false);
                break;
            }

            switch (err)
            {
            // The function returned successfully
            case ToxCore.ERR_CONFERENCE_PEER_QUERY.OK:
                break;

            // The conference number passed did not designate a valid
            // conference
            case ToxCore.ERR_CONFERENCE_PEER_QUERY.CONFERENCE_NOT_FOUND:
                GLib.assert(false);
                break;

            // The peer number passed did not designate a valid peer
            case ToxCore.ERR_CONFERENCE_PEER_QUERY.PEER_NOT_FOUND:
                GLib.assert(false);
                break;

            // The client is not connected to the conference
            case ToxCore.ERR_CONFERENCE_PEER_QUERY.NO_CONNECTION:
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }
        }
    }
}

