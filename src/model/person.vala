/*
 *    person.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * This is the person class
     */
    public abstract class Person : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Properties
        private string?      addr_;
        private string?      name_;
        private state_t      state_;
        private state_user_t state_user_;
        private string?      status_;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Public key size.
         *
         * Constants from libtoxcore.
         */
        public static size_t key_size       = ToxCore.public_key_size();

        /**
         * Tox ID size.
         *
         * Constants from libtoxcore.
         */
        public static size_t addr_size      = 2 * ToxCore.address_size();

        /**
         * Name maximum size.
         *
         * Constants from libtoxcore.
         */
        public static size_t name_max_len   = ToxCore.max_name_length();

        /**
         * Status maximum size.
         *
         * Constants from libtoxcore.
         */
        public static size_t status_max_len = ToxCore.max_status_message_length();

        /**
         * Public key.
         *
         * Warning! It is forbidden to make array a property of Object
         * class. So there are wrapper method allowing to control it
         * manually: key_normal_get(). You may need it to use in script
         * GIR-based plugins.
         */
        public uint8[]? key;

        /**
         * Tox ID.
         *
         * ASCII hexadecimal format.
         */
        public string? addr
        {
            public get
            {
                return this.addr_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() == addr_size);
                this.addr_ = value;
            }
        }

        /**
         * Name
         */
        public string? name
        {
            public get
            {
                return this.name_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() <= name_max_len);
                this.name_ = value;
            }
        }

        /**
         * Network state
         */
        public state_t state
        {
            public get
            {
                return this.state_;
            }

            public set construct
            {
                GLib.assert(value == state_t.NONE       ||
                            value == state_t.OFFLINE    ||
                            value == state_t.ONLINE_TCP ||
                            value == state_t.ONLINE_UDP);
                this.state_ = value;
            }
        }

        /**
         * User state
         */
        public state_user_t state_user
        {
            public get
            {
                return this.state_user_;
            }

            public set construct
            {
                GLib.assert(value == state_user_t.NONE  ||
                            value == state_user_t.AVAIL ||
                            value == state_user_t.AWAY  ||
                            value == state_user_t.BUSY);
                this.state_user_ = value;
            }
        }

        /**
         * Status
         */
        public string? status
        {
            public get
            {
                return this.status_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() <= status_max_len);
                this.status_ = value;
            }
        }

        /**
         * false: avatar has been changed at least one time during
         * session,
         * true:  avatar hasn't been changed
         */
        public bool changed_avatar { public get; public set construct; }

        /**
         * Avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         */
        public Gdk.Pixbuf? avatar { public get; public set; }

        /**
          * No avatar
          */
        public static Gdk.Pixbuf? no_avatar = null;

        /**
         * Avatar.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make an array a property of
         * Object class. So there are wrapper method allowing to control
         * it manually: dl_get(), dl_set(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_16x16;

        /**
         * Avatar.
         *
         * Large 32x32 icon.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make an array a property of
         * Object class. So there are wrapper method allowing to control
         * it manually: dl_get(), dl_set(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_32x32;

        /**
         * Avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make an array a property of
         * Object class. So there are wrapper method allowing to control
         * it manually: dl_get(), dl_set(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_normal;

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Network state
         */
        public enum state_t
        {
            /**
             * State is not set yet
             */
            NONE,

            /**
             * User is offline
             */
            OFFLINE,

            /**
             * User is online and available (TCP connection)
             */
            ONLINE_TCP,

            /**
             * User is online and available (UDP connection)
             */
            ONLINE_UDP,
        }

        /**
         * User state
         */
        public enum state_user_t
        {
            /**
             * State is not set yet
             */
            NONE,

            /**
             * User is online and available
             */
            AVAIL,

            /**
             * User is away. Clients can set this e.g. after a user defined
             * inactivity time.
             */
            AWAY,

            /**
             * User is busy. Signals to other clients that this client does
             * not currently wish to communicate.
             */
            BUSY,
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Get display name
         *
         * @return Display name
         */
        public abstract string display_name_get();

        /*----------------------------------------------------------------------------*/

        /**
          * Get network state for display
          *
          * @param first_lowercase First char is lowercase
          *
          * @return Network state for display
          */
        public string display_state_get(bool first_lowercase)
        {
            unowned string str;

            switch (this.state)
            {
            case state_t.NONE:
                str = _("Unknown");
                break;
            case state_t.OFFLINE:
                str = _("Offline");
                break;
            case state_t.ONLINE_TCP:
                str = _("Online (TCP)");
                break;
            case state_t.ONLINE_UDP:
                str = _("Online (UDP)");
                break;
            default:
                str = "";
                GLib.assert(false);
                break;
            }

            string res = first_lowercase ? Model.first_ch_lowercase_make(str) :
                                           str;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to default avatar image
          *
          * @return Filepath to default image
          */
        public static string img_filepath_default_avatar_get()
        {
            unowned string filename = "no_avatar_200x200.gif";
                    string filepath = GLib.Path.build_filename("img", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get user state for display
          *
          * @param first_lowercase First char is lowercase
          *
          * @return User state for display
          */
        public string display_state_user_get(bool first_lowercase)
        {
            unowned string str;

            switch (this.state_user)
            {
            case state_user_t.NONE:
                str = _("Unknown");
                break;
            case state_user_t.AVAIL:
                str = _("Available");
                break;
            case state_user_t.AWAY:
                str = _("Away");
                break;
            case state_user_t.BUSY:
                str = _("Busy");
                break;
            default:
                str = "";
                GLib.assert(false);
                break;
            }

            string res = (first_lowercase) ? Model.first_ch_lowercase_make(str) :
                                             str;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to default avatar icon
          *
          * @param is_small false: get large 32x32 icon,
          *                 true:  get small 16x16 icon
          *
          * @return Filepath to default icon
          */
        public static string icon_filepath_default_avatar_get(bool is_small)
        {
            unowned string filename = is_small ? "no_avatar_16x16.gif" : "no_avatar_32x32.gif";
                    string filepath = GLib.Path.build_filename("img", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to network state icon
          *
          * @param is_small false: get large 32x32 icon,
          *                 true:  get small 16x16 icon
          *
          * @return Filepath to icon
          */
        public string icon_filepath_state_get(bool is_small)
        {
            unowned string filename;

            switch (this.state)
            {
            case state_t.NONE:
                filename = is_small ? "offline_16x16.gif" : "offline_32x32.gif";
                break;
            case state_t.OFFLINE:
                filename = is_small ? "offline_16x16.gif" : "offline_32x32.gif";
                break;
            case state_t.ONLINE_TCP:
                filename = is_small ? "online_16x16.gif" : "online_32x32.gif";
                break;
            case state_t.ONLINE_UDP:
                filename = is_small ? "online_16x16.gif" : "online_32x32.gif";
                break;
            default:
                filename = "";
                GLib.assert(false);
                break;
            }

            string filepath = GLib.Path.build_filename("img", "state", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get filepath to user state icon
          *
          * @param is_small    false: get large 32x32 icon,
          *                    true:  get small 16x16 icon
          * @param unread_flag false: have not unread messages from a
          *                           buddy,
          *                    true:  otherwise
          *
          * @return Filepath to icon
          */
        public string icon_filepath_state_user_get(bool is_small, bool unread_flag = false)
        {
            unowned string filename;

            switch (this.state_user)
            {
            case state_user_t.NONE:
                filename = unread_flag ? is_small ? "avail_unread_16x16.gif" : "avail_unread_32x32.gif" :
                                         is_small ? "avail_16x16.gif"        : "avail_32x32.gif";
                break;
            case state_user_t.AVAIL:
                filename = unread_flag ? is_small ? "avail_unread_16x16.gif" : "avail_unread_32x32.gif" :
                                         is_small ? "avail_16x16.gif"        : "avail_32x32.gif";
                break;
            case state_user_t.AWAY:
                filename = unread_flag ? is_small ? "away_unread_16x16.gif"  : "away_unread_32x32.gif" :
                                         is_small ? "away_16x16.gif"         : "away_32x32.gif";
                break;
            case state_user_t.BUSY:
                filename = unread_flag ? is_small ? "busy_unread_16x16.gif"  : "busy_unread_32x32.gif" :
                                         is_small ? "busy_16x16.gif"         : "busy_32x32.gif";
                break;
            default:
                filename = "";
                GLib.assert(false);
                break;
            }

            string filepath = GLib.Path.build_filename("img", "state_user", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_16x16_get()
        {
            return this.avatar_16x16;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_16x16_set(uint8[]? avatar_16x16)
        {
            this.avatar_16x16 = avatar_16x16;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Small 32x32 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_32x32_get()
        {
            return this.avatar_32x32;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_32x32_set(uint8[]? avatar_32x32)
        {
            this.avatar_32x32 = avatar_32x32;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_normal_get()
        {
            return this.avatar_normal;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_normal_set(uint8[]? avatar_normal)
        {
            this.avatar_normal = avatar_normal;
        }
    }
}

