/*
 *    thread_ui.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model.Threads
{
    /**
     * UI event loop thread.
     *
     * Manages UI callbacks.
     */
    public class ThreadUI : Thread<void>
    {
        /*----------------------------------------------------------------------------*/
        /*                             Protected Methods                              */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread body
         */
        protected override void run()
        {
            // Execute GUI loop.
            // This method blocks.
            view->loop();
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * UI thread constructor
         *
         * @param name Thread name
         */
        public ThreadUI(string name)
        {
            // Base class constructor
            base(name);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * UI thread destructor
         */
        ~ThreadUI()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Stop thread
         */
        public override void stop()
        {
            // Execute from GUI loop
            view->exit();
        }
    }
}

