/*
 *    thread_bootstrap.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model.Threads
{
    /**
     * Bootstrap thread.
     *
     * Downloads node list, connects to DHTs, reconnect if needed.
     */
    public class ThreadBootstrap : Thread<void>
    {
        /*----------------------------------------------------------------------------*/
        /*                               Private Types                                */
        /*----------------------------------------------------------------------------*/

        // State of connection thread
        [Flags]
        private enum state_t
        {
            NORMAL           = 0,
            COLLECT_NODES    = 1 << 0,
            CONNECT_TO_NODES = 1 << 1,
            ONLINE           = 1 << 2,
        }

        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Bootstrap nodes
        private const string NODES_FILENAME        = "tox-nodes.json"; // Filename of list with (currently online?) bootstrap nodes
        private const ulong  RETRY_DELAY_US        = 5000000;          // Delay before next attemption of receiving and parsing the node list
        private const uint   MAX_NODES             = 8;                // Maximum number of nodes to bootstrap
        private const uint64 CONNECTION_TOUT_US    = 5000000;          // After bootstrap request wait this time for changing DHT state to be connected. If not connected, try to connect again.
        private const uint64 DISCONNECTION_TOUT_US = 10000000;         // After receiving the disconnected status wait this time for DHT state not to be changed. Then we can try to connect again.

        /*----------------------------------------------------------------------------*/
        /*                               Private Types                                */
        /*----------------------------------------------------------------------------*/

        // Bootstrap node
        private class Node : Object
        {
            public string ipv4       { public get; public set; }
            public uint16 port       { public get; public set; }
            public string public_key { public get; public set; }

            private uint8[] public_key_bin;

            public void public_key_bin_set() throws ErrModelHex
            {
                size_t desired_len = 2 * Person.key_size;
                if (public_key.length != desired_len)
                    throw new ErrModelHex.HEX_WRONG_LEN(_("The length of public key is wrong: %s: %u characters (should be %u characters)"),
                                                        public_key,
                                                        (uint) public_key.length,
                                                        (uint) desired_len);
                public_key_bin = Model.hex2bin_convert(public_key);
            }

            public uint8[] public_key_bin_get()
            {
                return public_key_bin;
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Content of one cell of asynchronous queue.
         *
         * Contains command to connection thread.
         */
        public class Cmd : Object
        {
            public enum cmd_t
            {
                HANDLE_NEW_STATE,
                WAS_REINIT_HANDLE_NEW_STATE,
                WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG,
                EXIT_THREAD,
            } 

            public cmd_t          cmd   { public get; public construct; }
            public Person.state_t state { public get; public construct; }

            public Cmd(cmd_t cmd,
                       Person.state_t state)

                requires ((cmd == cmd_t.HANDLE_NEW_STATE                   && (state == Person.state_t.OFFLINE    ||
                                                                               state == Person.state_t.ONLINE_TCP ||
                                                                               state == Person.state_t.ONLINE_UDP))
                          ||
                          (cmd == cmd_t.WAS_REINIT_HANDLE_NEW_STATE        &&  state == Person.state_t.OFFLINE)
                          ||
                          (cmd == cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG &&  state == Person.state_t.NONE)
                          ||
                          (cmd == cmd_t.EXIT_THREAD                        &&  state == Person.state_t.NONE))

            {
                Object(cmd   : cmd,
                       state : state);
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Asynchronous queue, primitive to communicate with the thread.
         * Consists of command cells.
         */
        public AsyncQueue<Cmd> queue_cmd;

        /**
         * Cancellation state, used to unblock the thread if it is
         * blocked
         */
        public GLib.Cancellable cancel;

        /*----------------------------------------------------------------------------*/
        /*                             Protected Methods                              */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread body
         */
        protected override void run()
        {
            state_t state = state_t.NORMAL;

            // Perform the bootstraping.
            //
            // This loop consists of two parts, two sub-loops:
            //  - collecting the nodes,
            //  - attempting to connect to them.
            for (;;)
            {
                string[]   src;    // Node sources
                bool[]     en_src; // State of node sources (enabled/disabled)
                size_t     cnt;    // Number of node sources

                bool       first_iteration = true;
                uint       idx             = 0;
                bool       have_en_src     = false;
                Node[]     nodes           = {};
                GLib.File? file            = null;

                // Get node sources from config
                try
                {
                    src    = model->self.cfg.get_string_list ("network", "nodes");
                    en_src = model->self.cfg.get_boolean_list("network", "en_nodes");
                }
                catch (GLib.KeyFileError ex)
                {
                    src    = {};
                    en_src = {};

                    GLib.assert(false);
                }

                cnt = src.length <= en_src.length ? src.length : en_src.length;

                // This task is useless if there are no node sources at all
                if (cnt == 0)
                {
                    GLib.debug(_("There are no node sources at all, do nothing"));
                    return;
                }

                // Collect list of bootstrap nodes
                for (;;)
                {
                    // (Do post-increment.)
                    //
                    // No actions right now if it is the first iteration.
                    if (first_iteration)
                        first_iteration = false;
                    // It is not the first iteration.
                    // Do we have more node sources?
                    else if (idx < cnt - 1)
                    {
                        // We have more node sources.
                        // Get next node source from config.
                        idx++;
                    }
                    else
                    {
                        // No node sources anymore
                        GLib.debug(_("I've checked all node sources"));

                        // Do we already have a node list?
                        if (nodes.length > 0)
                        {
                            // Everything is fine.
                            // Now we have a node list.
                            // Exit from loop.
                            GLib.debug(_("Now we have a list with %u nodes"), nodes.length);
                            break;
                        }

                        // Continue with first node source from config again
                        GLib.debug(_("We don't have a node list"));
                        idx = 0;

                        // Wait some time and try again
                        if (have_en_src) GLib.debug(_("I'll wait a little and start from the beginning of list"));
                        else             GLib.debug(_("All node sources are disabled, so do nothing"));

                        for (;;)
                        {
                            // Wait for some time.
                            // Or wait for a command (reinitialization / exit thread).
                            Cmd? cmd;

                            if (have_en_src)
                            {
                                cmd = this.queue_cmd.timeout_pop(RETRY_DELAY_US);
                                if (cmd == null)
                                    break;
                            }
                            else
                                cmd = this.queue_cmd.pop();

                            // Handle the own network state changing
                            if (cmd.cmd == Cmd.cmd_t.HANDLE_NEW_STATE ||
                                cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_STATE)
                            {
                                state = cmd.state == Person.state_t.OFFLINE ?
                                    state & ~state_t.ONLINE :
                                    state |  state_t.ONLINE;
                            }
                            // Collect nodes again on reinitialization
                            else if (cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG)
                            {
                                state |= state_t.COLLECT_NODES;
                                break;
                            }

                            // Shutdown thread if exit command has been received
                            if (cmd.cmd == Cmd.cmd_t.EXIT_THREAD)
                                return;
                        }

                        if ((state & state_t.COLLECT_NODES) > 0)
                            break;
                    }

                    // Parse a node source from config.
                    // What it is: file, URL or buddy?
                    try
                    {
                        // Ignore node source if it is disabled in config
                        if (!en_src[idx])
                        {
                            GLib.debug(_("Node source is disabled, ignoring: [%s]"), src[idx]);
                            continue;
                        }

                        have_en_src = true;

                        // Parse node source
                        var regex = new GLib.Regex("^(.+?): (.+)\\z");
                        GLib.MatchInfo info;
                        bool res = regex.match(src[idx], 0, out info);
                        if (!res)
                            throw new ErrGeneral.BAD_NODE_FMT(_("Bad node format: %s").printf(src[idx]));

                        string type = info.fetch(1);
                        string data = info.fetch(2);

                        GLib.debug(_("%s: using %s as node source..."), _(type), data);

                        // Determine its type
                        switch (type)
                        {
                        case "URL":

                            // URL
                            file = GLib.File.new_for_uri(data);

                            break;

                        case "File":

                            // File
                            file = GLib.File.new_for_path(data);

                            break;

                        case "Buddy":

                            // Buddy
                            file = null;

                            // Parse buddy field
                            regex = new GLib.Regex("^(.+) \\((.+):(\\d+)\\)\\z");
                            res = regex.match(data, 0, out info);
                            if (!res)
                                throw new ErrGeneral.BAD_BUDDY_FMT(_("Bad buddy format: %s").printf(data));

                            // Find the buddy in buddy list
                            string display_name = info.fetch(1);
                            Buddy  buddy        = null;

                            foreach (Buddy tmp in model->buddies_hash.values)
                                if (tmp.display_name_get() == display_name)
                                {
                                    buddy = tmp;
                                    break;
                                }

                            if (buddy == null)
                                throw new ErrGeneral.BUDDY_NOT_FOUND_ERR(_("Buddy not found: %s").printf(display_name));
     
                            // Get buddy's public key
                            ToxCore.ERR_FRIEND_GET_PUBLIC_KEY err;

                            size_t len = Person.key_size;
                            var    key  = new uint8[len];

                            res = model->tox.friend_get_public_key(buddy.uid, key, out err);
                            GLib.assert(res);
                            GLib.assert(err == ToxCore.ERR_FRIEND_GET_PUBLIC_KEY.OK);

                            // Prepare the new node with the buddy
                            var tmp = new Node();

                            tmp.ipv4 = info.fetch(2);
                            tmp.port = (uint16) int.parse(info.fetch(3));

                            tmp.public_key = "";
                            for (size_t byte = 0; byte < len; byte++)
                                tmp.public_key += key[byte].to_string("%02X");
                            tmp.public_key_bin_set();

                            nodes += tmp;
                            GLib.debug(_("Added new DHT node %s:%u"), tmp.ipv4, tmp.port);

                            break;

                        default:

                            // Another type
                            file = null;

                            throw new ErrGeneral.BAD_NODE_TYPE(_("Bad node type: %s").printf(type));
                            //break;
                        }
                    }
                    catch (RegexError ex)
                    {
                        GLib.assert(false);
                    }
                    catch (ErrGeneral ex)
                    {
                        GLib.debug(ex.message);
                    }
                    catch (ErrModelHex ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // Do we need to read a URL/file with nodes?
                    if (file != null)
                    {
                        // Read and parse JSON URL/file with nodes
                        try
                        {
                            // Lock mutex
                            model->mutex.lock();

                            // Reset cancellation state if there are no more commands which
                            // should interrupt the reading URL or file.
                            //
                            // How to know if there are such commands in the command queue?
                            //
                            // We can't peek queue — GLib has no API methods to do this. We
                            // should pop a message from it and then push it back. Do it for
                            // all messages in the queue.
                            size_t len          = this.queue_cmd.length();
                            bool   reset_cancel = true;

                            for (size_t num = 0; num < len; num++)
                            {
                                // Get a command
                                Cmd? cmd = this.queue_cmd.try_pop();
                                GLib.assert(cmd != null);

                                // Should this command interrupt the reading URL or file??
                                if (cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG ||
                                    cmd.cmd == Cmd.cmd_t.EXIT_THREAD)
                                {
                                    reset_cancel = false;
                                }

                                // Give the command back
                                this.queue_cmd.push(cmd);
                            }

                            // Reset the thread to its uncancelled state
                            if (reset_cancel)
                                this.cancel.reset();

                            // Unlock mutex
                            model->mutex.unlock();

                            // Get JSON from remote data server
                            GLib.FileInputStream stream_file = file.read(this.cancel);
                            GLib.DataInputStream stream_data = new GLib.DataInputStream(stream_file);

                            var data = new GLib.StringBuilder();

                            for (;;)
                            {
                                string? line = stream_data.read_line(null, this.cancel);
                                if (line == null)
                                    break;
                                data.append(line);
                            }

                            // Parse JSON
                            var json = new Json.Parser();
                            json.load_from_data(data.str);
                            unowned Json.Node root = json.get_root();
                            if (root == null)
                                throw new ErrModelJSON.JSON_INVALID(_("Can't get root node"));
                            unowned Json.Object obj = root.get_object();
                            if (obj == null)
                                throw new ErrModelJSON.JSON_INVALID(_("Can't get object of the root node"));
                            if (!obj.has_member("nodes"))
                                throw new ErrModelJSON.JSON_INVALID(_("No member nodes it the root node"));
                            unowned Json.Array arr = obj.get_array_member("nodes");

                            arr.foreach_element((arr, index, node) =>
                            {
                                Node tmp = Json.gobject_deserialize(typeof(Node), node) as Node;

                                try
                                {
                                    tmp.public_key_bin_set();
                                    nodes += tmp;
                                    GLib.debug(_("Added new DHT node %s:%u"), tmp.ipv4, tmp.port);
                                }
                                catch (ErrModelHex ex)
                                {
                                    GLib.debug(ex.message);
                                }
                            });

                            // Node list is still empty?
                            if (nodes.length == 0)
                                throw new ErrModelJSON.JSON_INVALID(_("Empty node list"));
                        }
                        catch (GLib.Error ex)
                        {
                            GLib.debug(ex.message);

                            // Handle all incoming commands
                            for (;;)
                            {
                                // Get a command
                                Cmd? cmd = this.queue_cmd.try_pop();
                                if (cmd == null)
                                    break;

                                // Handle the own network state changing
                                if (cmd.cmd == Cmd.cmd_t.HANDLE_NEW_STATE ||
                                    cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_STATE)
                                {
                                    state = cmd.state == Person.state_t.OFFLINE ?
                                        state & ~state_t.ONLINE :
                                        state |  state_t.ONLINE;
                                }
                                // Collect nodes again on reinitialization
                                else if (cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG)
                                {
                                    state |= state_t.COLLECT_NODES;
                                    break;
                                }

                                // Shutdown thread if exit command has been received
                                if (cmd.cmd == Cmd.cmd_t.EXIT_THREAD)
                                    return;
                            }

                            if ((state & state_t.COLLECT_NODES) > 0)
                                break;
                        }
                    }
                }

                if ((state & state_t.COLLECT_NODES) > 0)
                {
                    state &= ~state_t.COLLECT_NODES;
                    continue;
                }

                // Now we have node list.
                //
                // Determine the maximum number of nodes to bootstrap.
                uint max_nodes = nodes.length <= MAX_NODES ? nodes.length :
                                                             MAX_NODES;

                // Get first node from list
                uint cur  = 0;
                Node node = nodes[cur];

                // Attempt to connect to bootstrap nodes
                for (;;)
                {
                    // Do connections only if offline
                    Cmd? cmd;

                    if ((state & state_t.ONLINE) == 0)
                    {
                        // Lock mutex
                        model->mutex.lock();

                        // Send get nodes requests
                        for (uint num = 0; num < max_nodes; num++)
                        {
                            // Setup connection with current node
                            try
                            {
                                // Send get nodes request
                                ToxCore.ERR_BOOTSTRAP err;
                                bool res = model->tox.bootstrap(node.ipv4,
                                                                node.port,
                                                                node.public_key_bin_get(),
                                                                out err);

                                switch (err)
                                {
                                // The function returned successfully
                                case ToxCore.ERR_BOOTSTRAP.OK:
                                    GLib.assert(res);
                                    break;

                                // One of the arguments to the function was NULL when it was not
                                // expected
                                case ToxCore.ERR_BOOTSTRAP.NULL:
                                    GLib.assert(false);
                                    break;

                                // The address could not be resolved to an IP address, or the IP
                                // address passed was invalid
                                case ToxCore.ERR_BOOTSTRAP.BAD_HOST:
                                    GLib.assert(!res);
                                    throw new ErrGeneral.FAILED_BOOTSTRAP(_("Bad host"));
                                    //break;

                                // The port passed was invalid. The valid port range is
                                // (1, 65535).
                                case ToxCore.ERR_BOOTSTRAP.BAD_PORT:
                                    GLib.assert(!res);
                                    throw new ErrGeneral.FAILED_BOOTSTRAP(_("Bad port"));
                                    //break;

                                // Another error
                                default:
                                    GLib.assert(false);
                                    break;
                                }

                                // Unlock mutex
                                model->mutex.unlock();

                                // Add TCP relay.
                                //
                                // Warning! This method may block for several seconds sometimes.
                                // So I should unlock mutex here. It is dangerous.
                                //
                                // This method also may return false, so I disable assert below.
                                // It occurs very rarely.
                                //
                                // TODO.
                                // Try to fix it with new version of libtoxcore.
                                res = model->tox.add_tcp_relay(node.ipv4,
                                                               node.port,
                                                               node.public_key_bin_get(),
                                                               out err);
                                //GLib.assert(res);
                                //GLib.assert(err == ToxCore.ERR_BOOTSTRAP.OK);

                                // Lock mutex
                                model->mutex.lock();

                                GLib.debug(_("Trying to connect to DHT node %s:%u..."), node.ipv4, node.port);
                            }
                            catch (ErrGeneral ex)
                            {
                                GLib.debug(_("Can't connect to DHT node %s:%u, %s"), node.ipv4, node.port, Model.first_ch_lowercase_make(ex.message));
                            }

                            // Get next node from list
                            cur  = (cur == nodes.length - 1) ? 0 : cur + 1;
                            node = nodes[cur];
                        }

                        // Unlock mutex
                        model->mutex.unlock();

                        // Wait for changing the connection to online for timetout
                        cmd = this.queue_cmd.timeout_pop(CONNECTION_TOUT_US);
                    }
                    else
                        cmd = null;

                    // Handle incoming commands
                    while ((state & state_t.ONLINE) > 0 ||
                           cmd != null)
                    {
                        // Online or offline?
                        if ((state & state_t.ONLINE) > 0 &&
                            cmd == null)
                        {
                            // Do nothing while connected
                            do
                            {
                                cmd = this.queue_cmd.pop();
                            } while (cmd.state == Person.state_t.ONLINE_TCP ||
                                     cmd.state == Person.state_t.ONLINE_UDP);
                        }
                        else
                        {
                            // Reconnect to nodes on reinitialization
                            if (cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_STATE)
                            {
                                state |= state_t.CONNECT_TO_NODES;
                                state = cmd.state == Person.state_t.OFFLINE ?
                                    state & ~state_t.ONLINE :
                                    state |  state_t.ONLINE;
                                break;
                            }

                            // Collect nodes again on reinitialization
                            if (cmd.cmd == Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG)
                            {
                                state |= state_t.COLLECT_NODES;
                                break;
                            }

                            // Shutdown thread if exit command has been received
                            if (cmd.cmd == Cmd.cmd_t.EXIT_THREAD)
                                return;

                            // Do nothing while connected
                            while (cmd.state == Person.state_t.ONLINE_TCP ||
                                   cmd.state == Person.state_t.ONLINE_UDP)
                            {
                                cmd    = this.queue_cmd.pop();
                                state |= state_t.ONLINE;
                            }
                        }

                        // After disconnection do not reconnect to DHT immediately.
                        //
                        // libtoxcore documentation recommends wait some time before
                        // reconnection, because network state may frequently changes
                        // for short amounts of time.
                        //
                        // Reconnect immediately only after the Tox the reinitialization
                        // (e. g. new proxy settings were applied).
                        while (cmd != null &&
                               cmd.cmd   == Cmd.cmd_t.HANDLE_NEW_STATE &&
                               cmd.state == Person.state_t.OFFLINE)
                        {
                            cmd    = this.queue_cmd.timeout_pop(DISCONNECTION_TOUT_US);
                            state &= ~state_t.ONLINE;
                        }

                        // We are disconnected enough time ago. No we can try to
                        // bootstrap again.
                    }

                    if ((state & state_t.COLLECT_NODES) > 0)
                    {
                        state &= ~state_t.COLLECT_NODES;
                        break;
                    }

                    if ((state & state_t.CONNECT_TO_NODES) > 0)
                    {
                        state &= ~state_t.CONNECT_TO_NODES;
                        cur    = 0;
                        node   = nodes[cur];
                    }
                }
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Bootstrap thread constructor
         *
         * @param name Thread name
         */
        public ThreadBootstrap(string name)
        {
            // Base class constructor
            base(name);

            // Initialize asynchronous queue to communicate with this
            // thread.
            // Initialize its cancellation state.
            this.queue_cmd = new AsyncQueue<Cmd>();
            this.cancel    = new GLib.Cancellable();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Bootstrap thread destructor
         */
        ~ThreadBootstrap()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Stop thread
         */
        public override void stop()
        {
            // Ask the thread to exit immidiately.
            // Asynchronous queue is used for communication with this
            // thread.
            var cmd = new Cmd(Cmd.cmd_t.EXIT_THREAD,
                              Person.state_t.NONE // Not used
                             );
            this.queue_cmd.push(cmd);

            // Unblock the thread if it is blocked
            this.cancel.cancel();
        }
    }
}

