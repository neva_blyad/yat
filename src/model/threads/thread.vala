/*
 *    thread.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model.Threads
{
    /**
     * This is the thread class
     */
    public abstract class Thread<T> : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread name
         */
        public string name { public get; protected set construct; }

        /**
         * Thread reference
         */
        public GLib.Thread<T>? thread { public get; protected set construct; }

        /*----------------------------------------------------------------------------*/
        /*                             Protected Methods                              */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread body
         */
        protected abstract T run();

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread constructor
         *
         * @param name Thread name
         */
        protected Thread(string name)
        {
            // GObject-style construction
            Object(name   : name,
                   thread : null);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Thread destructor
         */
        ~Thread()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Start thread
         */
        public void start()
        {
            // This creates and starts a new thread
            GLib.assert(this.thread == null);
            GLib.ThreadFunc<T> body = () =>
            {
                return run();
            };

            this.thread = new GLib.Thread<T>(this.name, body);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Stop thread
         */
        public abstract void stop();

        /*----------------------------------------------------------------------------*/

        /**
         * Is thread started?
         *
         * @return false: thread is not started,
         *         true:  thread is started
         */
        public bool is_started()
        {
            return this.thread != null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Wait until thread finish
         */
        public void wait()
        {
            GLib.assert(this.thread != null);
            this.thread.join();
        }
    }
}

