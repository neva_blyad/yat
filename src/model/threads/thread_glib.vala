/*
 *    thread_glib.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model.Threads
{
    /**
     * GLib event loop thread.
     *
     * Manages async methods, software timers, watchers, etc.
     */
    public class ThreadGLib : Thread<void>
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // GLib event loop.
        //
        // E. g. all async methods and timers used by our application
        // are handled by him.
        private GLib.MainLoop glib_loop;

        /*----------------------------------------------------------------------------*/
        /*                             Protected Methods                              */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread body
         */
        protected override void run()
        {
            // Run GLib event loop.
            // This method blocks.
            this.glib_loop.run();
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * GLib thread constructor
         *
         * @param name Thread name
         */
        public ThreadGLib(string name)
        {
            // Base class constructor
            base(name);

            // Initialize GLib event loop
            this.glib_loop = new GLib.MainLoop(null, false);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * GLib thread destructor
         */
        ~ThreadGLib()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Stop thread
         */
        public override void stop()
        {
            // Quit GLib event loop
            GLib.assert(this.glib_loop.is_running());
            this.glib_loop.quit();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * GLib event loop state
         *
         * @return false: not running,
         *         true:  running
         */
        public bool is_running()
        {
            // Check the event loop
            return this.glib_loop.is_running();
        }
    }
}

