/*
 *    buddy.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * A/V call errors
     */
    public errordomain ErrCall
    {
        /**
         * A resource allocation error occurred while trying to create
         * the structures required for the call
         */
        MEM_ALLOC,

        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * The buddy was valid, but not currently connected
         */
        BUDDY_NOT_CONNECTED_ERR,
    }

    /**
     * A/V call cancel errors
     */
    public errordomain ErrCallCancel
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,
    }

    /*
     * Audio call answer errors
     */
    public errordomain ErrCallAnswer
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * Failed to initialize codecs for call session. Note that codec
         * initiation will fail if there is no receive callback
         * registered for either audio video.
         */
        CODEC_FAIL,

        /**
         * The buddy was valid, but he is not currently trying to
         * initiate a call.
         * This is also returned if this client is already in a call
         * with the buddy.
         */
        NOT_CALLING_BUDDY,
    }

    /**
     * This is the buddy class
     */
    public class Buddy : Person
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Properties
        private string?  Alias_;
        private state_t  action_from_us_;
        private state_t  action_to_us_;
        private unread_t have_unread_msg_;
        private int64    add_lst_;
        private int64    last_msg_;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Unique identifier used by libtoxcore
         */
        public uint32 uid { public get; public set construct; }

        /**
         * Optional local Alias
         */
        public string? Alias
        {
            public get
            {
                return this.Alias_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() > 0);
                this.Alias_ = value;
            }
        }

        /**
         * Whether we are typing or doing a call to the buddy
         */
        public state_t action_from_us
        {
            public get
            {
                return this.action_from_us_;
            }

            public set construct
            {
                GLib.assert((value & ~state_t.MASK_ALL) == 0);
                GLib.assert((value & state_t.TYPING_KNOWN) != 0 || (value & state_t.TYPING_TXT) == 0);

                state_t tmp = value & MASK_AV_CALL;
                GLib.assert((tmp == 0) ||
                            (tmp != 0 && (tmp & (tmp - 1)) == 0));
                this.action_from_us_ = value;
            }
        }

        /**
         * Whether the buddy is typing or doing a call
         */
        public state_t action_to_us
        {
            public get
            {
                return this.action_to_us_;
            }

            public set construct
            {
                GLib.assert((value & ~state_t.MASK_ALL) == 0);
                GLib.assert((value & state_t.TYPING_KNOWN) != 0 || (value & state_t.TYPING_TXT) == 0);

                state_t tmp = value & MASK_AV_CALL;
                GLib.assert((tmp == 0) ||
                            (tmp != 0 && (tmp & (tmp - 1)) == 0));
                this.action_to_us_ = value;
            }
        }

        /**
          * Source ID of timer that should turn off the typing
          * notification
          */
        public uint timer_typing_off { public get; public set construct; }

        /**
         * Are there unread messages from the buddy or not?
         */
        public unread_t have_unread_msg
        {
            public get
            {
                return this.have_unread_msg_;
            }

            public set construct
            {
                GLib.assert(value == unread_t.NONE  ||
                            value == unread_t.FALSE ||
                            value == unread_t.TRUE);
                this.have_unread_msg_ = value;
            }
        }

        /**
         * Added to buddy list number.
         *
         * Using this member instead of datetime member below is faster
         * and more accurate, because two buddies with the same datetime
         * can exist.
         *
         * There are can be holes in this numeration.
         */
        public int64 add_lst
        {
            public get
            {
                return this.add_lst_;
            }

            public set construct
            {
                GLib.assert(value >= -1);
                this.add_lst_ = value;
            }
        }

        /**
         * Last added to buddy list number
         */
        public static uint64 last_add_lst = 1;

        /**
         * Date and time of the adding to buddy list
         */
        public GLib.DateTime datetime_add_lst { public get; public set construct; }

        /**
         * Last seen date and time
         */
        public GLib.DateTime? datetime_seen_last { public get; public set construct; }

        /**
         * Last message from/to the buddy number.
         *
         * Using this member instead of datetime member below is faster
         * and more accurate, because two buddies with the same datetime
         * can exist.
         *
         * There are can be holes in this numeration.
         */
        public int64 last_msg
        {
            public get
            {
                return this.last_msg_;
            }

            public set construct
            {
                GLib.assert(value >= -1);
                this.last_msg_ = value;
            }
        }

        /**
         * Date and time of the last message from/to the buddy number
         */
        public GLib.DateTime? datetime_last_msg { public get; public set construct; }

        /**
         * Time when somebody answered to A/V call
         * (we or peer)
         */
        public GLib.DateTime? datetime_av_answer { public get; public set construct; }

        /**
         * Temporary buffer for download.
         *
         * Warning! It is forbidden to make an array a property of
         * Object class. So there are wrapper method allowing to control
         * it manually: dl_get(), dl_set(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? dl;

        /**
          * Number of the buddies without state
          */
        public static size_t none       = 0;

        /**
          * Number of the currently offline buddies
          */
        public static size_t offline    = 0;

        /**
          * Number of the currently online buddies (TCP connection)
          */
        public static size_t online_tcp = 0;

        /**
          * Number of the currently online buddies (UDP connection)
          */
        public static size_t online_udp = 0;

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Whether we are typing or doing a call to the buddy.
         * Whether the buddy is typing or doing a call.
         */
        [Flags]
        public enum state_t
        {
            /*
             * No action
             */
            NONE = 0,

            /**
             * Typing state is known (see below)
             */
            TYPING_KNOWN = 1 << 0,

            /**
             * We are currently typing a message / or the buddy is typing to
             * us (or at least it was the last notification from him)
             */
            TYPING_TXT = 1 << 1,

            /**
             * We are currently calling to the buddy / or the buddy called
             * from us
             */
            DOING_AUDIO_CALL = 1 << 2,

            /**
             * We are currently doing a video call to the buddy / or the
             * buddy called from us
             */
            DOING_VIDEO_CALL = 1 << 3,

            /**
             * Audio call is active
             */
            ACTIVE_AUDIO_CALL = 1 << 4,

            /**
             * Video call is active
             */
            ACTIVE_VIDEO_CALL = 1 << 5,

            /**
             * Bitmask of doing A/V call flags
             */
            MASK_DOING_AV_CALL = DOING_AUDIO_CALL | DOING_VIDEO_CALL,

            /**
             * Bitmask of active A/V call flags
             */
            MASK_ACTIVE_AV_CALL = ACTIVE_AUDIO_CALL | ACTIVE_VIDEO_CALL,

            /**
             * Bitmask of doing or active audiocall flags
             */
            MASK_AUDIO_CALL = DOING_AUDIO_CALL | ACTIVE_AUDIO_CALL,

            /**
             * Bitmask of doing or active videocall flags
             */
            MASK_VIDEO_CALL = DOING_VIDEO_CALL | ACTIVE_VIDEO_CALL,

            /**
             * Bitmask of doing or active A/V flags
             */
            MASK_AV_CALL = MASK_AUDIO_CALL | MASK_VIDEO_CALL,

            /**
             * Bitmask of all flags
             */
            MASK_ALL = 0x3F,
        }

        /**
         * Are there unread messages from the buddy or not?
         */
        public enum unread_t
        {
            /**
             * Read/unread is not set yet
             */
            NONE,

            /**
             * All messages from buddy were read
             */
            FALSE,

            /**
             * Have unread message from buddy
             */
            TRUE,
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Buddy constructor
         *
         * @param uid   Unique identifier used by libtoxcore
         * @param addr  Tox ID
         * @param Alias Alias
         */
        public Buddy(uint32  uid,
                     string? addr,
                     string? Alias)
        {
            // GObject-style construction
            Object(//key          :  null, // Array properties are not supported by Vala 0.48
                   addr           : addr,
                   name           : null,
                   state          : Person.state_t.NONE,
                   state_user     : Person.state_user_t.NONE,
                   status         : null,
                   changed_avatar : false,
                   //avatar         : null,
                   //avatar_16x16   : null,
                   //avatar_32x32   : null,
                   //avatar_normal  : null,

                   uid                : uid,
                   Alias              : Alias,
                   //dl                 : null,
                   action_from_us     : state_t.NONE,
                   action_to_us       : state_t.NONE,
                   timer_typing_off   : 0,
                   have_unread_msg    : unread_t.NONE,
                   add_lst            : -1,
                   datetime_seen_last : null,
                   last_msg           : -1,
                   datetime_last_msg  : null,
                   datetime_av_answer : null);

            this.key           = null;
            this.avatar        = null;
            this.avatar_16x16  = null;
            this.avatar_32x32  = null;
            this.avatar_normal = null;

            this.dl               = null;
            this.datetime_add_lst = new GLib.DateTime.now_local();

            none++;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * How to compare two buddies?
          *
          * This method returns function for using with sorting
          * algorithms. Returned functions can be different according to
          * profile configuration (sorting by, sorting order). Comparing
          * two buddies can be used for placing them in GUI controls.
          *
          * @param model Model
          *
          * @return Compare function
          */
        public static GLib.CompareDataFunc<Buddy> cmp_fn_get(Model *model)
        {
            GLib.CompareDataFunc<Buddy> res;

            Profile.cfg_main_sort_buddies_t       sort_buddies;
            Profile.cfg_main_sort_buddies_order_t sort_buddies_order;
            bool sort_buddies_unread_top;

            try
            {
                sort_buddies       = (Profile.cfg_main_sort_buddies_t)
                    model->self.cfg.get_integer("main", "sort_buddies");
                sort_buddies_order = (Profile.cfg_main_sort_buddies_order_t)
                    model->self.cfg.get_integer("main", "sort_buddies_order");
                sort_buddies_unread_top =
                    model->self.cfg.get_boolean("main", "sort_buddies_unread_top");
            }
            catch (GLib.KeyFileError ex)
            {
                sort_buddies            = Profile.cfg_main_sort_buddies_t.ADD_LST;
                sort_buddies_order      = Profile.cfg_main_sort_buddies_order_t.ASC;
                sort_buddies_unread_top = false;
                GLib.assert(false);
            }

            switch (sort_buddies)
            {
            case Profile.cfg_main_sort_buddies_t.ADD_LST:

                if (sort_buddies_order != Profile.cfg_main_sort_buddies_order_t.DESC)
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst <  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst <  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }
                else
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst >  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst >  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }

                break;

            case Profile.cfg_main_sort_buddies_t.DISPLAY_NAME:

                if (sort_buddies_order != Profile.cfg_main_sort_buddies_order_t.DESC)
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name >  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                string a_display_name = a.display_name_get();
                                string b_display_name = b.display_name_get();

                                if      (a_display_name >  b_display_name) return  1;
                                else if (a_display_name == b_display_name) return  0;
                                else                                       return -1;
                            };
                    }
                }
                else
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name <  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                string a_display_name = a.display_name_get();
                                string b_display_name = b.display_name_get();

                                if      (a_display_name <  b_display_name) return  1;
                                else if (a_display_name == b_display_name) return  0;
                                else                                       return -1;
                            };
                    }
                }

                break;

            case Profile.cfg_main_sort_buddies_t.LAST_MSG:

                if (sort_buddies_order != Profile.cfg_main_sort_buddies_order_t.DESC)
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if (a.last_msg < b.last_msg)
                                    {
                                        return  1;
                                    }
                                    else if (a.last_msg > b.last_msg)
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name >  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if (a.last_msg < b.last_msg)
                                {
                                    return  1;
                                }
                                else if (a.last_msg > b.last_msg)
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name >  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }
                else
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if (a.last_msg > b.last_msg)
                                    {
                                        return  1;
                                    }
                                    else if (a.last_msg < b.last_msg)
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name <  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if (a.last_msg > b.last_msg)
                                {
                                    return  1;
                                }
                                else if (a.last_msg < b.last_msg)
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name <  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }

                break;

            case Profile.cfg_main_sort_buddies_t.STATE:

                if (sort_buddies_order != Profile.cfg_main_sort_buddies_order_t.DESC)
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if ((a.state == Person.state_t.NONE       || a.state == Person.state_t.OFFLINE) &&
                                        (b.state == Person.state_t.ONLINE_TCP || b.state == Person.state_t.ONLINE_UDP))
                                    {
                                        return 1;
                                    }
                                    else if ((a.state == Person.state_t.ONLINE_TCP || a.state == Person.state_t.ONLINE_UDP) &&
                                             (b.state == Person.state_t.NONE       || b.state == Person.state_t.OFFLINE))
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name >  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if ((a.state == Person.state_t.NONE       || a.state == Person.state_t.OFFLINE) &&
                                    (b.state == Person.state_t.ONLINE_TCP || b.state == Person.state_t.ONLINE_UDP))
                                {
                                    return 1;
                                }
                                else if ((a.state == Person.state_t.ONLINE_TCP || a.state == Person.state_t.ONLINE_UDP) &&
                                         (b.state == Person.state_t.NONE       || b.state == Person.state_t.OFFLINE))
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name >  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }
                else
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if ((a.state == Person.state_t.ONLINE_TCP || a.state == Person.state_t.ONLINE_UDP) &&
                                        (b.state == Person.state_t.NONE       || b.state == Person.state_t.OFFLINE))
                                    {
                                        return 1;
                                    }
                                    else if ((a.state == Person.state_t.NONE       || a.state == Person.state_t.OFFLINE) &&
                                             (b.state == Person.state_t.ONLINE_TCP || b.state == Person.state_t.ONLINE_UDP))
                                    {
                                        return -1;
                                    }
                                    else
                                    {
                                        string a_display_name = a.display_name_get();
                                        string b_display_name = b.display_name_get();

                                        if      (a_display_name <  b_display_name) return  1;
                                        else if (a_display_name == b_display_name) return  0;
                                        else                                       return -1;
                                    }
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if ((a.state == Person.state_t.ONLINE_TCP || a.state == Person.state_t.ONLINE_UDP) &&
                                    (b.state == Person.state_t.NONE       || b.state == Person.state_t.OFFLINE))
                                {
                                    return 1;
                                }
                                else if ((a.state == Person.state_t.NONE       || a.state == Person.state_t.OFFLINE) &&
                                         (b.state == Person.state_t.ONLINE_TCP || b.state == Person.state_t.ONLINE_UDP))
                                {
                                    return -1;
                                }
                                else
                                {
                                    string a_display_name = a.display_name_get();
                                    string b_display_name = b.display_name_get();

                                    if      (a_display_name <  b_display_name) return  1;
                                    else if (a_display_name == b_display_name) return  0;
                                    else                                       return -1;
                                }
                            };
                    }
                }

                break;

            default:

                if (sort_buddies_order != Profile.cfg_main_sort_buddies_order_t.DESC)
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst <  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst <  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }
                else
                {
                    if (sort_buddies_unread_top)
                    {
                        res = (a, b) =>
                            {
                                if      (a.have_unread_msg == unread_t.FALSE && b.have_unread_msg == unread_t.TRUE)  return  1;
                                else if (a.have_unread_msg == unread_t.TRUE  && b.have_unread_msg == unread_t.FALSE) return -1;
                                else
                                {
                                    if      (a.add_lst >  b.add_lst) return  1;
                                    else if (a.add_lst == b.add_lst) return  0;
                                    else                             return -1;
                                }
                            };
                    }
                    else
                    {
                        res = (a, b) =>
                            {
                                if      (a.add_lst >  b.add_lst) return  1;
                                else if (a.add_lst == b.add_lst) return  0;
                                else                             return -1;
                            };
                    }
                }

                break;
            }

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display name
          *
          * @return Display name
          */
        public override string display_name_get()
        {
            string res;

            if (this.Alias == null)
            {
                if (this.name == null)
                    res = this.addr == null ? _("Unknown") :
                                              this.addr;
                else
                {
                    if (this.name == "")
                        res = this.addr == null ? this.name :
                                                  this.addr;
                    else
                        res = this.name;
                }
            }
            else
                res = this.Alias;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Should buddy be showed in GUI?
          *
          * @param model      Model
          * @param use_filter false: don't use filtering,
          *                   true:  use filtering,
          * @param pattern    Filter pattern
          */
        public bool should_be_displayed(Model *model, bool use_filter, GLib.Regex? pattern)
        {
            // Get show/hide buddies config setting
            Profile.cfg_main_show_buddies_t show_buddies;

            try
            {
                show_buddies = (Profile.cfg_main_show_buddies_t)
                    model->self.cfg.get_integer("main", "show_buddies");
            }
            catch (GLib.KeyFileError ex)
            {
                show_buddies = Profile.cfg_main_show_buddies_t.SHOW_ALL;
                GLib.assert(false);
            }

            // Use show/hide buddies config setting?
            switch (show_buddies)
            {
            case Profile.cfg_main_show_buddies_t.SHOW_ALL:
                break;
            case Profile.cfg_main_show_buddies_t.HIDE_OFFLINE_BUT:
                if (this.have_unread_msg == Buddy.unread_t.FALSE &&
                        (this.state == Person.state_t.NONE ||
                         this.state == Person.state_t.OFFLINE))
                    return false;
                break;
            case Profile.cfg_main_show_buddies_t.HIDE_OFFLINE:
                if (this.state == Person.state_t.NONE ||
                        this.state == Person.state_t.OFFLINE)
                    return false;
                break;
            default:
                break;
            }

            // Use filter?
            if (use_filter)
            {
                // Filter buddy.
                // Scans for a display name and status match.
                if (!pattern.match(this.display_name_get()) &&
                        !pattern.match(this.status))
                    return false;
            }

            return true;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Start/stop typing a message
         *
         * @param model Model
         * @param flag  false: stop,
         *              true:  start
         */
        public void typing_change(Model *model, bool flag)
        {
            bool changed_typing;

            if (flag && (this.action_from_us & state_t.TYPING_TXT) == 0)
            {
                changed_typing = true;
                this.action_from_us |= state_t.TYPING_KNOWN | state_t.TYPING_TXT;
                GLib.debug(_("I've started typing message: %s").printf(display_name_get()));
            }
            else if (!flag && (this.action_from_us & state_t.TYPING_TXT) > 0)
            {
                changed_typing = true;
                this.action_from_us |=  state_t.TYPING_KNOWN;
                this.action_from_us &= ~state_t.TYPING_TXT;
                GLib.debug(_("I've stopped typing message: %s").printf(display_name_get()));
            }
            else
                changed_typing = false;

            // Set self typing to Tox profile
            try
            {
                // Continue only if typing is changed and sending typing
                // notifications is turned on
                if (model->self.cfg.get_boolean("privacy", "send_typing_notifications") &&
                    changed_typing)
                {
                    ToxCore.ERR_FRIEND_DELETE err;
                    bool res = model->tox.self_set_typing(this.uid, flag, out err);
                    GLib.assert(res);
                    GLib.assert(err == ToxCore.ERR_FRIEND_DELETE.OK);
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Send avatar
         *
         * @param model Model
         */
        public void avatar_send(Model *model)
        {
            const string filename = "noname";

            GLib.assert(model->self.avatar_normal != null);

            // Start to send avatar
            ToxCore.ERR_FILE_SEND err;
            uint32 file_num = model->tox.file_send(this.uid,
                                                   ToxCore.FileKind.AVATAR,
                                                   model->self.avatar_normal.length,
                                                   null, // Generate file identifier
                                                   filename.data,
                                                   out err);
            GLib.assert(file_num != uint32.MAX);
            GLib.assert(err == ToxCore.ERR_FILE_SEND.OK);

            string dbg;

            dbg  = _("Outcoming file: %s: #%lu, %s, ").printf(display_name_get(), file_num, filename);
            dbg += GLib.dngettext(Cfg.DOMAIN, ", %llu byte: ", ", %llu bytes: ", this.avatar_normal.length).printf(this.avatar_normal.length);
            dbg += _("OK");

            GLib.debug(dbg);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get temporary buffer for download.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? dl_get()
        {
            return this.dl;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set temporary buffer for download.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void dl_set(uint8[]? dl)
        {
            this.dl = dl;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Do the A/V call.
         *
         * Mutex should be locked.
         *
         * @param model Model
         * @param video false: no video,
         *              true:  do video call
         */
        public void av_call(Model *model, bool video) throws ErrCall
        {
            GLib.assert((this.action_from_us & state_t.MASK_AV_CALL) == 0 &&
                        (this.action_to_us   & state_t.MASK_AV_CALL) == 0);
            GLib.assert(this.datetime_av_answer == null);

            // Call the buddy. This will start ringing the buddy.
            ToxAV.ERR_CALL err;
            bool res = model->tox_av.call(this.uid,
                                          128,              // Should be enough. TODO: accurately calculate this value.
                                          video ? 5000 : 0, // Should be enough. TODO: accurately calculate this value.
                                          out err);

            switch (err)
            {
            // The function returned successfully
            case ToxAV.ERR_CALL.OK:
                GLib.assert(res);
                break;

            // A resource allocation error occurred while trying to create
            // the structures required for the call
            case ToxAV.ERR_CALL.MALLOC:
                throw new ErrCall.MEM_ALLOC(_("Unable to allocate enough memory"));
                //break;

            // Synchronization error occurred
            case ToxAV.ERR_CALL.SYNC:
                GLib.assert(!res);
                throw new ErrCall.SYNC_FAIL(_("Synchronization error occurred"));
                //break;

            // The buddy number did not designate a valid buddy
            case ToxAV.ERR_CALL.FRIEND_NOT_FOUND:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // The buddy was valid, but not currently connected
            case ToxAV.ERR_CALL.FRIEND_NOT_CONNECTED:
                GLib.assert(!res);
                throw new ErrCall.BUDDY_NOT_CONNECTED_ERR(_("This client is currently not connected to the buddy"));
                //break;

            // Attempted to call a buddy while already in an audio or video
            // call with them
            case ToxAV.ERR_CALL.FRIEND_ALREADY_IN_CALL:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // Audio or video bit rate is invalid
            case ToxAV.ERR_CALL.INVALID_BIT_RATE:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Cancel the A/V call.
         *
         * Mutex should be locked.
         *
         * @param model Model
         */
        public void av_call_cancel(Model *model) throws ErrCallCancel
        {
            // Reject the call if it was not answered, yet. Cancel the call
            // after it was answered.
            ToxAV.ERR_CALL_CONTROL err;
            bool res = model->tox_av.call_control(this.uid,
                                                  ToxAV.CallControl.CANCEL,
                                                  out err);

            switch (err)
            {
            // The function returned successfully
            case ToxAV.ERR_CALL_CONTROL.OK:
                GLib.assert(res);
                break;

            // Synchronization error occurred
            case ToxAV.ERR_CALL_CONTROL.SYNC:
                GLib.assert(!res);
                throw new ErrCallCancel.SYNC_FAIL(_("Synchronization error occurred"));
                //break;

            // The buddy uid passed did not designate a valid buddy
            case ToxAV.ERR_CALL_CONTROL.FRIEND_NOT_FOUND:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // This client is currently not in a call with the buddy. Before
            // the call is answered, only CANCEL is a valid control.
            case ToxAV.ERR_CALL_CONTROL.FRIEND_NOT_IN_CALL:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // Happens if user tried to pause an already paused call or if
            // trying to resume a call that is not paused
            case ToxAV.ERR_CALL_CONTROL.INVALID_TRANSITION:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Answer the A/V call.
         *
         * Mutex should be locked.
         *
         * @param model Model
         * @param video false: no video,
         *              true:  do video call
         */
        public void av_answer(Model *model, bool video) throws ErrCallAnswer
        {
            GLib.assert((this.action_from_us & state_t.MASK_DOING_AV_CALL)  == 0 &&
                        (this.action_from_us & state_t.MASK_ACTIVE_AV_CALL) == 0 &&
                        (this.action_to_us   & state_t.MASK_ACTIVE_AV_CALL) == 0 &&

                        ((!video && (this.action_to_us & state_t.DOING_AUDIO_CALL) >  0  &&
                                    (this.action_to_us & state_t.DOING_VIDEO_CALL) == 0) ||
                         ( video && (this.action_to_us & state_t.DOING_AUDIO_CALL) == 0  &&
                                    (this.action_to_us & state_t.DOING_VIDEO_CALL) >  0)));

            GLib.assert(this.datetime_av_answer == null);

            // Accept an incoming call
            ToxAV.ERR_ANSWER err;
            bool res = model->tox_av.answer(this.uid,
                                            128,              // Should be enough. TODO: accurately calculate this value.
                                            video ? 5000 : 0, // Should be enough. TODO: accurately calculate this value.
                                            out err);

            switch (err)
            {
            // The function returned successfully
            case ToxAV.ERR_ANSWER.OK:
                GLib.assert(res);
                break;

            // Synchronization error occurred
            case ToxAV.ERR_ANSWER.SYNC:
                GLib.assert(!res);
                throw new ErrCallAnswer.SYNC_FAIL(_("Synchronization error occurred"));
                //break;

            // Failed to initialize codecs for call session. Note that codec
            // initiation will fail if there is no receive callback
            // registered for either audio video.
            case ToxAV.ERR_ANSWER.CODEC_INITIALIZATION:
                GLib.assert(!res);
                throw new ErrCallAnswer.CODEC_FAIL(_("Failed to initialize codecs for call session"));
                //break;

             // The buddy number did not designate a valid buddy
            case ToxAV.ERR_ANSWER.FRIEND_NOT_FOUND:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // The buddy was valid, but they are not currently trying to
            // initiate a call.
            // This is also returned if this client is already in a call
            // with the buddy.
            case ToxAV.ERR_ANSWER.FRIEND_NOT_CALLING:
                GLib.assert(!res);
                throw new ErrCallAnswer.NOT_CALLING_BUDDY(_("Buddy is not currently calling you"));
                //break;

            // Audio or video bitrate is invalid
            case ToxAV.ERR_ANSWER.INVALID_BIT_RATE:
                GLib.assert(!res);
                GLib.assert(false);
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }
        }
    }
}

