/*
 *    plugin.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * Plugin errors.
     *
     * There are errors related to plugin executable, i. e. dynamic
     * library or interpreter script.
     *
     * Something goes bad while the plugin's dynamic library
     * opening/closing, loading symbols, etc.
     */
    public errordomain ErrPluginGeneral
    {
        /**
         * Can't load plugin
         */
        LOAD_ERR,

        /**
         * Can't unload plugin
         */
        UNLOAD_ERR,

        /**
         * Icon doesn't exist
         */
        ICON_DOESNT_EXIST_ERR,
    }

    /**
     * This is the plugin interface.
     *
     * Read a few notes if you are a plugin writer.
     *
     * Each plugin is GObject class that should be derived from this
     * interface. It implements the abstract methods listed below.
     *
     * Don't bother trying to override non-abstract methods.
     */
    public interface Plugin : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Model.
         *
         * Note. This property is useless for compiled plugins.
         *       Simpler using model reference directly.
         *       But for scripting this property may be better or even the only
         *       option.
         */
        public abstract unowned Model     model_ { public get; public construct; } 

        /**
         * View.
         *
         * Note. This property is useless for compiled plugins.
         *       Simpler using model reference directly.
         *       But for scripting this property may be better or even the only
         *       option.
         */
        public abstract unowned View.View view_  { public get; public construct; } 

        /**
         * Controller.
         *
         * Note. This property is useless for compiled plugins.
         *       Simpler using model reference directly.
         *       But for scripting this property may be better or even the only
         *       option.
         */
        public abstract unowned Ctrl.Ctrl ctrl_  { public get; public construct; } 

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Get filepath to plugin icon.
        //
        // Note. Usually you don't want to reimplement this method.
        private string _icon_filepath_get(Model *model, Peas.PluginInfo info) throws ErrPluginGeneral
        {
            // Plugin description fields.
            // Prepare filepath of icon.
            unowned string filename = info.get_icon_name();
                    string domain   = "yat-plugins-" + filename;
            unowned string name     = GLib.dgettext(domain, info.get_name());
                    string filepath = GLib.Path.build_filename(info.get_module_dir(), filename);

            // Ensure there is icon here
            if (!GLib.FileUtils.test(filepath, GLib.FileTest.IS_REGULAR))
                throw new ErrPluginGeneral.ICON_DOESNT_EXIST_ERR(_("Plugin %s: icon %s doesn't exist"), name, filepath);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Plugin entry point
         */
        public abstract void plugin_load();

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin exit point
         */
        public abstract void plugin_unload();

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration availability
         *
         * @return false: the plugin isn't configurable,
         *         true:  the plugin is configurable
         */
        public abstract bool plugin_cfg_check();

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration entry point
         */
        public abstract void plugin_cfg_exec();

        /*----------------------------------------------------------------------------*/

        /**
         * Get translated plugin name
         *
         * @param model Model
         * @param info  The plugin information
         *
         * @return Name
         */
        public unowned string name_get(Model *model, Peas.PluginInfo info)
        {
            unowned string filename = info.get_module_name();
                    string domain   = "yat-plugins-" + filename;
            unowned string name     = GLib.dgettext(domain, info.get_name());

            return name;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get filepath to plugin icon
         *
         * @param model Model
         * @param info  The plugin information
         *
         * @return Filepath to icon
         */
        public string? icon_filepath_get(Model *model, Peas.PluginInfo info)
        {
            string? filepath;

            try
            {
                filepath = _icon_filepath_get(model, info);
            }
            catch (ErrPluginGeneral ex)
            {
                GLib.debug(ex.message);
                filepath = null;
            }

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Load plugin
         *
         * @param model Model
         * @param info  The plugin information
         */
        public void load(Model *model, Peas.PluginInfo info) throws GLib.Error, ErrPluginGeneral
        {
            unowned string name = name_get(model, info);

            GLib.debug(_("Loading plugin %s..."), name);
            bool res  = info.is_available();
                 res &= model->plugins_ext.engine.try_load_plugin(info);
            if (!res)
                throw new ErrPluginGeneral.LOAD_ERR(_("Can't load %s plugin"), name);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Unload plugin
         *
         * @param model Model
         * @param info  The plugin information
         */
        public void unload(Model *model, Peas.PluginInfo info) throws GLib.Error, ErrPluginGeneral
        {
            unowned string name = name_get(model, info);

            GLib.debug(_("Unloading plugin %s..."), name);
            bool res  = info.is_available();
                 res &= model->plugins_ext.engine.try_unload_plugin(info);
            if (!res)
                throw new ErrPluginGeneral.UNLOAD_ERR(_("Can't unload %s plugin"), name);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration availability
         *
         * @param model Model
         * @param info  The plugin information
         *
         * @return false: the plugin isn't configurable,
         *         true:  the plugin is configurable
         */
        public bool cfg_check(Model *model, Peas.PluginInfo info)
        {
            GLib.debug(_("Checking the configuration availability for plugin %s..."), name_get(model, info));
            bool avail = plugin_cfg_check();

            return avail;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Execute plugin configuration
         *
         * @param model Model
         * @param info  The plugin information
         */
        public void cfg_exec(Model *model, Peas.PluginInfo info)
        {
            GLib.debug(_("Executing configuration for plugin %s..."), name_get(model, info));
            plugin_cfg_exec();
        }
    }
}

