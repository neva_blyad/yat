/*
 *    member.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * This is the member class.
     *
     * Member is somebody's Tox client who has connected to one
     * of the conferences which we have in our conference list.
     *
     * So it is the member of some conference.
     *
     * Member is not the same as buddy, because buddy should be
     * joined to the buddy list, but a member may be joined to the
     * buddy list and may not. Member can also be ourself.
     */
    public class Member : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Properties
        private string? name_;
        private int64   add_lst_;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Name maximum size.
         *
         * Constants from libtoxcore.
         */
        public static size_t name_max_len = ToxCore.max_name_length();

        /**
         * Unique identifier used by libtoxcore
         */
        public uint32 uid { public get; public set construct; }

        /*
         * This member may be myself.
         *
         * The member also may be on our buddy list.
         * In this case this variable is reference to corresponding buddy.
         *
         * And this member may be not myself and not on our buddy list.
         * In this case this variable should be null.
         */
        public Person? person { public get; public set construct; }

        /*
         * The member is joined to this conference
         */
        public Conference conference { public get; public set construct; }

        /**
         * Name
         */
        public string? name
        {
            public get
            {
                return this.name_;
            }

            public set construct
            {
                GLib.assert(value == null || value.char_count() <= name_max_len);
                this.name_ = value;
            }
        }

        /*
         * Public key
         */
        public uint8[]? key;

        /**
         * Added to member list number.
         *
         * Using this member instead of datetime member below is faster
         * and more accurate, because two members with the same datetime
         * can exist.
         *
         * There are can be holes in this numeration.
         */
        public int64 add_lst
        {
            public get
            {
                return this.add_lst_;
            }

            public set construct
            {
                GLib.assert(value >= -1);
                this.add_lst_ = value;
            }
        }

        /*
         * Foreground colour
         */
        public uint fg { public get; public set construct; }

        /*
         * Background colour
         */
        public uint bg { public get; public set construct; }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Member constructor
         *
         * @param uid        Unique identifier used by libtoxcore
         * @param person     Reference to myself / corresponding buddy /
         *                   null.
         * @param conference The member is joined to this conference
         * @param fg         Foreground colour
         * @param bg         Background colour
         */
        public Member(uint32      uid,
                      Person?     person,
                      Conference? conference,
                      uint        fg,
                      uint        bg)
        {
            // GObject-style construction
            Object(uid        : uid,
                   person     : person,
                   conference : conference,
                   name       : null,
                   add_lst    : -1,
                   fg         : fg,
                   bg         : bg);

            this.key = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * How to compare two members?
          *
          * This method returns function for using with sorting
          * algorithms. Returned functions can be different according to
          * profile configuration (sorting by, sorting order). Comparing
          * two members can be used for placing them in GUI controls.
          *
          * @param model Model
          *
          * @return Compare function
          */
        public static GLib.CompareDataFunc<Member> cmp_fn_get(Model *model)
        {
            GLib.CompareDataFunc<Member> res;

            Profile.cfg_main_sort_members_t       sort_members;
            Profile.cfg_main_sort_members_order_t sort_members_order;

            try
            {
                sort_members       = (Profile.cfg_main_sort_members_t)
                    model->self.cfg.get_integer("main", "sort_members");
                sort_members_order = (Profile.cfg_main_sort_members_order_t)
                    model->self.cfg.get_integer("main", "sort_members_order");
            }
            catch (GLib.KeyFileError ex)
            {
                sort_members       = Profile.cfg_main_sort_members_t.ADD_LST;
                sort_members_order = Profile.cfg_main_sort_members_order_t.ASC;
                GLib.assert(false);
            }

            switch (sort_members)
            {
            case Profile.cfg_main_sort_members_t.ADD_LST:

                if (sort_members_order != Profile.cfg_main_sort_members_order_t.DESC)
                {
                    res = (a, b) =>
                        {
                            if      (a.add_lst <  b.add_lst) return  1;
                            else if (a.add_lst == b.add_lst) return  0;
                            else                             return -1;
                        };
                }
                else
                {
                    res = (a, b) =>
                        {
                            if      (a.add_lst >  b.add_lst) return  1;
                            else if (a.add_lst == b.add_lst) return  0;
                            else                             return -1;
                        };
                }

                break;

            case Profile.cfg_main_sort_members_t.DISPLAY_NAME:

                if (sort_members_order != Profile.cfg_main_sort_members_order_t.DESC)
                {
                    res = (a, b) =>
                        {
                            string a_display_name = a.display_name_get();
                            string b_display_name = b.display_name_get();

                            if      (a_display_name >  b_display_name) return  1;
                            else if (a_display_name == b_display_name) return  0;
                            else                                       return -1;
                        };
                }
                else
                {
                    res = (a, b) =>
                        {
                            string a_display_name = a.display_name_get();
                            string b_display_name = b.display_name_get();

                            if      (a_display_name <  b_display_name) return  1;
                            else if (a_display_name == b_display_name) return  0;
                            else                                       return -1;
                        };
                }

                break;

            default:

                if (sort_members_order != Profile.cfg_main_sort_members_order_t.DESC)
                {
                    res = (a, b) =>
                        {
                            if      (a.add_lst <  b.add_lst) return  1;
                            else if (a.add_lst == b.add_lst) return  0;
                            else                             return -1;
                        };
                }
                else
                {
                    res = (a, b) =>
                        {
                            if      (a.add_lst >  b.add_lst) return  1;
                            else if (a.add_lst == b.add_lst) return  0;
                            else                             return -1;
                        };
                }

                break;
            }

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get display name
          *
          * @return Display name
          */
        public string display_name_get()
        {
            string res;

            if (this.person == null)
                res = this.name == null ? _("Unknown") :
                                          this.name;
            else
                res = this.person.display_name_get();

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Get public key
          *
          * @return Public key
          */
        public uint8[]? key_get()
        {
            uint8[] res;

            if (this.person == null)
                res = this.key;
            else
                res = this.person.key;

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Should member be showed in GUI?
          *
          * @param model   Model
          * @param pattern Filter pattern
          */
        public bool should_be_displayed(Model *model, GLib.Regex? pattern)
        {
            // Filter member.
            // Scans for a display name.
            if (!pattern.match(this.display_name_get()))
                return false;

            return true;
        }
    }
}

