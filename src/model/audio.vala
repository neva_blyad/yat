/*
 *    audio.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors
     */
    public errordomain ErrAudio
    {
        /**
         * GStreamer plugin we are needed doesn't exist
         */
        NOT_FOUND_PLUGIN,

        /**
         * Pipeline general error
         */
        PIPELINE_FAIL,

        /**
         * Can't get list of audio devices from GStreamer
         */
        HW_INFO_ERR,

        /**
         * No audio device with specified name in GStreamer list is
         * found
         */
        HW_NOT_FOUND_ERR,

        /**
         * Audio device is possibly related to another audio system
         */
        AUDIO_SYS_AND_HW_MISMATCH,
    }

    /**
     * GStreamer audio pipeline 1 errors
     */
    public errordomain ErrAudio1
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * Buddy turned off audio receiving
         */
        TURNED_OFF_AUDIO,
    }

    /**
     * Audio call start errors
     */
    public errordomain ErrAudioCallStart
    {
        /**
         * A resource allocation error occurred while trying to create
         * the structures required for the call
         */
        MEM_ALLOC,

        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * The buddy was valid, but not currently connected
         */
        BUDDY_NOT_CONNECTED_ERR,
    }

    /**
     * Audio call stop errors
     */
    public errordomain ErrAudioCallStop
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,
    }

    /*
     * Audio call answer errors
     */
    public errordomain ErrAudioCallAnswer
    {
        /**
         * Synchronization error occurred
         */
        SYNC_FAIL,

        /**
         * Failed to initialize codecs for call session. Note that codec
         * initiation will fail if there is no receive callback
         * registered for either audio video.
         */
        CODEC_FAIL,

        /**
         * The buddy was valid, but he is not currently trying to
         * initiate a call.
         * This is also returned if this client is already in a call
         * with the buddy.
         */
        NOT_CALLING_BUDDY,
    }

    /**
     * This is the audio class.
     *
     * Microphone input, speaker output are handled there.
     * GStreamer is the multimedia framework which does all of this.
     */
    [SingleInstance]
    public class Audio : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // See doc/GSTREAMER_PIPELINES for details how the audio
        // pipelines are built

        // Audio parameters
        private const uint  CNT_CHANNELS = 2; // Stereo
        private const ulong BITRATE_HZ   = 48000;

        // GStreamer device monitor
        private Gst.DeviceMonitor mon;

        // GStreamer audio pipeline 1.
        // (Microphone Input - P2P-network Output.)
        private Gst.Pipeline pipeline1;

        // GStreamer audio pipeline 1 elements.
        // (Microphone Input - P2P-network Output.)
        private struct pipeline1_elems_t
        {
            Gst.Element  src;    // autoaudiosrc
            Gst.Element  filter; // rawaudioparse
            Gst.App.Sink sink;   // appsink
        }

        private pipeline1_elems_t pipeline1_elems;

        // Remaining bytes from pipeline 1.
        // We've got them from source and should send it to sink.
        // Can't send them right now, because we have not them enough:
        // Tox allows sending only fixed-length audio frames.
        private unowned uint8[]? pipeline1_buf;

        // GStreamer audio pipeline 2.
        // (P2P-network Input - Speaker Output.)
        private Gst.Pipeline pipeline2;

        // GStreamer audio pipeline 2 elements.
        // (P2P-network Input - Speaker Output.)
        private struct pipeline2_elems_t
        {
            Gst.App.Src src;  // appsrc
            Gst.Element sink; // autoaudiosink 
        }

        private pipeline2_elems_t pipeline2_elems;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/


        // Callback performing the handling audio frames from GStreamer
        // audio pipeline 1.
        // (Microphone Input - P2P-network Output)
        private Gst.FlowReturn pipeline1_new_sample_handle()
        {
            // Perform the GStreamer audio pipeline 1 flowing until all
            // samples have been sent
            for (;;)
            {
                Gst.MapInfo map;
                unowned uint8[] data;
                size_t len;
                size_t cnt_samples;

                // Tox allows send only fixed-length audio frames.
                //
                // TODO: tune this constant.
                //const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = (size_t) ((double) BITRATE_HZ * 2.5 / 1000);
                //const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = BITRATE_HZ *  5 / 1000;
                //const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = BITRATE_HZ * 10 / 1000;
                //const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = BITRATE_HZ * 20 / 1000;
                //const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = BITRATE_HZ * 40 / 1000;
                const size_t MAX_SAMPLES_SENT_AT_ONE_TIME = BITRATE_HZ * 60 / 1000; // Tox doesn't allow send more at one time

                // Check for a new audio frame from a microphone
                Gst.Sample? frm = this.pipeline1_elems.sink.try_pull_sample(0 // No timeout, no block
                                                                           );
                if (frm == null)
                    break;

                // Get the frame's buffer
                Gst.Buffer? buf = frm.get_buffer();
                GLib.assert(buf != null);

                // Map buffer
                bool res = buf.map(out map, Gst.MapFlags.READ);
                GLib.assert(res);

                // Do we have unsent bytes from the previous callback call?
                if (this.pipeline1_buf == null)
                {
                    // No unsent bytes.
                    // Number of bytes to sent.
                    // Data to sent.
                    len  = map.size;
                    data = map.data;
                }
                else
                {
                    // Have unsent bytes.
                    // We'll add new buffer to them.
                    size_t cur_len = this.pipeline1_buf.length;
                    size_t add_len = map.size;

                    // Number of bytes to sent
                    len = cur_len + add_len;

                    // Ask system for a new memory block
                    this.pipeline1_buf        = (uint8 []) GLib.realloc(this.pipeline1_buf, len);
                    this.pipeline1_buf.length = (int) len;

                    GLib.assert(this.pipeline1_buf != null);

                    // Now add the current frame
                    Posix.memcpy(&this.pipeline1_buf[cur_len],
                                 map.data,
                                 add_len);

                    // Can't use Gst.Buffer.extract() method here instead of
                    // Posix.memcpy(), it's buggy, it has wrong VAPI
                    //buf.extract();

                    // Data to sent
                    data = this.pipeline1_buf;
                }

                // Number of samples to send.
                // Not all of the samples will be sent this time.
                cnt_samples = len / (sizeof(int16) * CNT_CHANNELS);
                cnt_samples = cnt_samples - cnt_samples % MAX_SAMPLES_SENT_AT_ONE_TIME;

                // Send audio from microphone to peers
                for (uint sample =  0;
                          sample <  (uint) cnt_samples;
                          sample += (uint) MAX_SAMPLES_SENT_AT_ONE_TIME)
                {
                    // Lock mutex
                    model->mutex.lock();

                    // Prepare temporary hash with all peers
                    var peers = new Gee.HashMap<uint32, Buddy>();
                    foreach (Buddy peer in model->peer_buddies_audio.values)
                        peers[peer.uid] = peer;
                    foreach (Buddy peer in model->peer_buddies_video.values)
                        peers[peer.uid] = peer;

                    // Should have at least one peer
                    if (peers.size == 0)
                    {
                        // Unlock mutex.
                        // Unmap buffer.
                        model->mutex.unlock();
                        buf.unmap(map);
                        return Gst.FlowReturn.OK;
                    }

                    // Each peer should receive the frame
                    foreach (Buddy peer in peers.values)
                    {
                        // Send audio from microphone to peer
                        string dbg;

                        try
                        {
                            // Prepare the debug print
                            dbg  = _("Outcoming audio frame to buddy: %s: ").printf(peer.display_name_get());
                            dbg += GLib.dngettext(Cfg.DOMAIN, "%u sample", "%u samples", MAX_SAMPLES_SENT_AT_ONE_TIME).printf((uint) MAX_SAMPLES_SENT_AT_ONE_TIME);
                            dbg += ", ";
                            dbg += GLib.dngettext(Cfg.DOMAIN, "%u channel", "%u channels", CNT_CHANNELS).printf(CNT_CHANNELS);
                            dbg += ", ";
                            dbg += _("%lu Hz bitrate").printf(BITRATE_HZ);
                            dbg += ": ";

                            // Send audio frame
                            ToxAV.ERR_SEND_FRAME err;
                            res = model->tox_av.audio_send_frame(peer.uid,
                                                                 (int16 []) &((int16 []) data)[CNT_CHANNELS * sample],
                                                                 MAX_SAMPLES_SENT_AT_ONE_TIME,
                                                                 (uint8) CNT_CHANNELS,
                                                                 (uint32) BITRATE_HZ,
                                                                 out err);

                            // Handle error
                            switch (err)
                            {
                            // The function returned successfully
                            case ToxAV.ERR_SEND_FRAME.OK:
                                GLib.assert(res);
                                break;

                            // The samples data pointer was null
                            case ToxAV.ERR_SEND_FRAME.NULL:
                                GLib.assert(!res);
                                GLib.assert(false);
                                break;

                            // The buddy number passed did not designate a valid buddy
                            case ToxAV.ERR_SEND_FRAME.FRIEND_NOT_FOUND:
                                GLib.assert(!res);
                                GLib.assert(false);
                                break;

                            // This client is currently not in a call with the buddy
                            case ToxAV.ERR_SEND_FRAME.FRIEND_NOT_IN_CALL:
                                GLib.assert(!res);
                                GLib.assert(false);
                                break;

                            // Synchronization error occurred
                            case ToxAV.ERR_SEND_FRAME.SYNC:
                                GLib.assert(!res);
                                throw new ErrAudio1.SYNC_FAIL("Synchronization error occurred");
                                //break;

                            // One of the frame parameters was invalid. E. g., the audio
                            // sampling rate may be unsupported.
                            case ToxAV.ERR_SEND_FRAME.INVALID:
                                GLib.assert(!res);
                                GLib.assert(false);
                                break;

                            // Buddy turned off audio receiving
                            case ToxAV.ERR_SEND_FRAME.PAYLOAD_TYPE_DISABLED:
                                GLib.assert(!res);
                                throw new ErrAudio1.TURNED_OFF_AUDIO("Buddy turned off audio receiving");
                                //break;

                            // Failed to push frame through RTP interface
                            case ToxAV.ERR_SEND_FRAME.RTP_FAILED:
                                GLib.assert(!res);
                                GLib.assert(false);
                                break;

                            // Another error
                            default:
                                GLib.assert(false);
                                break;
                            }

                            GLib.debug(dbg + _("OK"));
                        }
                        catch (ErrAudio1 ex)
                        {
                            GLib.debug(dbg + Model.first_ch_lowercase_make(ex.message));
                        }
                    }

                    // Unlock mutex
                    model->mutex.unlock();
                }

                // Had we sent all the PCM buffer entirely?
                size_t remaining = len % (MAX_SAMPLES_SENT_AT_ONE_TIME * (sizeof(int16) * CNT_CHANNELS));

                if (remaining > 0)
                {
                    // Save remaining bytes for the next time this callback will be
                    // called. We can't send them this time.
                    uint8 *tmp = new uint8[remaining];

                    Posix.memcpy(tmp,
                                 &data[len - remaining],
                                 remaining);

                    this.pipeline1_buf        = (uint8[]) tmp;
                    this.pipeline1_buf.length = (int) remaining;
                }
                else
                {
                    // All the buffer had been sent entirely.
                    // Nothing to save for the next callback calling.
                    this.pipeline1_buf = null;
                }

                // Unmap buffer
                buf.unmap(map);
            }

            return Gst.FlowReturn.OK;
        }

        /*----------------------------------------------------------------------------*/

        // Get name of GStreamer plugin to use with capture devices.
        // Source element of the pipeline 1 should use this plugin.
        private unowned string plugin_cap_get(Profile.cfg_audio_sys_t audio_sys, out unowned string factory_name) throws ErrAudio
        {
            // Determine the GStreamer factory to use for pipeline 1 source
            // element.
            // (Microphone Input - P2P-network Output.)
            switch (audio_sys)
            {
            case Profile.cfg_audio_sys_t.AUTO:
                factory_name = "autoaudiosrc";
                break;
#if __WXGTK__
            case Profile.cfg_audio_sys_t.JACK:
                factory_name = "jackaudiosrc";
                break;
            case Profile.cfg_audio_sys_t.PIPEWIRE:
                factory_name = "pipewiresrc";
                break;
            case Profile.cfg_audio_sys_t.PULSEAUDIO:
                factory_name = "pulsesrc";
                break;
            case Profile.cfg_audio_sys_t.OSS:
                factory_name = "osssrc";
                break;
#elif __WXMSW__
#endif
            default:
                factory_name = "autoaudiosrc";
                break;
            }

            // Determine the GStreamer plugin to use for pipeline 1 source
            Gst.ElementFactory? factory = Gst.ElementFactory.find(factory_name);
            //GLib.assert(factory != null);
            if (factory == null)
                throw new ErrAudio.NOT_FOUND_PLUGIN(_("System multimedia plugin we are needed for capture doesn't exist"));

            unowned string? plugin = factory.get_plugin_name();
            GLib.assert(plugin != null);

            return plugin;
        }

        /*----------------------------------------------------------------------------*/

        // Get name of GStreamer plugin to use with playback devices.
        // Sink element of the pipeline 2 should use this plugin.
        private unowned string plugin_playback_get(Profile.cfg_audio_sys_t audio_sys, out unowned string factory_name) throws ErrAudio
        {
            // Determine the GStreamer factory to use for pipeline 2 sink
            // element.
            // (P2P-network Input - Speaker Output.)
            switch (audio_sys)
            {
            case Profile.cfg_audio_sys_t.AUTO:
                factory_name = "autoaudiosink";
                break;
#if __WXGTK__
            case Profile.cfg_audio_sys_t.JACK:
                factory_name = "jackaudiosink";
                break;
            case Profile.cfg_audio_sys_t.PIPEWIRE:
                factory_name = "pipewiresink";
                break;
            case Profile.cfg_audio_sys_t.PULSEAUDIO:
                factory_name = "pulsesink";
                break;
            case Profile.cfg_audio_sys_t.OSS:
                factory_name = "osssink";
                break;
#elif __WXMSW__
#endif
            default:
                factory_name = "autoaudiosink";
                break;
            }

            // Determine the GStreamer plugin to use for pipeline 2 sink
            Gst.ElementFactory? factory = Gst.ElementFactory.find(factory_name);
            //GLib.assert(factory != null);
            if (factory == null)
                throw new ErrAudio.NOT_FOUND_PLUGIN(_("System multimedia plugin we are needed for playback doesn't exist"));

            unowned string? plugin = factory.get_plugin_name();
            GLib.assert(plugin != null);

            return plugin;
        }

        /*----------------------------------------------------------------------------*/

        // Return a pipeline state.
        //
        // Notice. This method should be private. I changed it to public
        // only to suppress compiler warning that method never used.
        //private bool pipeline_en_get(Model        *model,
        //                             Gst.Pipeline  pipeline) throws ErrAudio
        public bool pipeline_en_get(Model        *model,
                                    Gst.Pipeline  pipeline) throws ErrAudio
        {
            Gst.StateChangeReturn res;
            Gst.State state;

            // Get the state
            res = pipeline.get_state(out state,
                                     null,
                                     0);

            // Ensure there are no errors
            if (res == Gst.StateChangeReturn.FAILURE)
            {
                throw new ErrAudio.PIPELINE_FAIL(_("Audio subsystem fails while trying to get its state"));
            }

            GLib.assert(state == Gst.State.NULL    ||
                        state == Gst.State.PLAYING ||
                        state == Gst.State.READY);

            return state != Gst.State.NULL;
        }

        /*----------------------------------------------------------------------------*/

        // Start/stop a pipeline
        private void pipeline_en_set(Model        *model,
                                     Gst.Pipeline  pipeline,
                                     bool          flag) throws ErrAudio
        {
            Gst.StateChangeReturn res;
            Gst.State state = flag ? Gst.State.PLAYING :
                                     Gst.State.NULL;

            // Set the state
            res = pipeline.set_state(state);

            // Ensure there are no errors
            if (res == Gst.StateChangeReturn.FAILURE)
            {
                throw new ErrAudio.PIPELINE_FAIL(_("Audio subsystem fails while trying to start/stop"));
            }
        }

        /*----------------------------------------------------------------------------*/

        // Get name of GStreamer plugin to use with audio device.
        // This plugin would be used in specified pipeline.
        private unowned string plugin_get(Gst.Pipeline pipeline,
                                          Profile.cfg_audio_sys_t audio_sys,
                                          out unowned string factory_name) throws ErrAudio
        {
            // Determine the pipeline 1 plugin.
            // (Microphone Input - P2P-network Output.)
            // Determine the pipeline 2 plugin.
            // (P2P-network Input - Speaker Output.)
            unowned string plugin = pipeline == pipeline1 ?
                plugin_cap_get     (audio_sys, out factory_name) :
                plugin_playback_get(audio_sys, out factory_name);

            return plugin;
        }

        /*----------------------------------------------------------------------------*/

        // Set gain or volume to source/sink element of pipeline
        private void vol_set(Gst.Element elem,
                             Profile.cfg_audio_sys_t audio_sys,
                             double vol)
        {
            // Configure the element
            switch (audio_sys)
            {
            case Profile.cfg_audio_sys_t.AUTO:
                GLib.assert(false);
                break;
#if __WXGTK__
            case Profile.cfg_audio_sys_t.JACK:
                // TODO
                break;
            case Profile.cfg_audio_sys_t.PIPEWIRE:
                // TODO
                break;
            case Profile.cfg_audio_sys_t.PULSEAUDIO:
                elem.set("volume", vol); // Volume
                break;
            case Profile.cfg_audio_sys_t.OSS:
                // TODO
                break;
#elif __WXMSW__
#endif
            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        // Initialize audio pipeline 1.
        // (Microphone Input - P2P-network Output.)
        private void audio1_init(Model *model) throws ErrAudio
        {
            // Create the elements
            this.pipeline1_elems.src    =                Gst.ElementFactory.make("autoaudiosrc",  "src");
            this.pipeline1_elems.filter =                Gst.ElementFactory.make("rawaudioparse", "filter");
            this.pipeline1_elems.sink   = (Gst.App.Sink) Gst.ElementFactory.make("appsink",       "sink");

            GLib.assert(this.pipeline1_elems.src    != null);
            GLib.assert(this.pipeline1_elems.filter != null);
            GLib.assert(this.pipeline1_elems.sink   != null);

            // Configure the elements
            this.pipeline1_elems.filter.set("format",       0); // PCM
            this.pipeline1_elems.filter.set("interleaved",  "true");
            this.pipeline1_elems.filter.set("num-channels", CNT_CHANNELS);
            this.pipeline1_elems.filter.set("pcm-format",   Gst.Audio.Format.S16LE);
            this.pipeline1_elems.filter.set("sample-rate",  BITRATE_HZ);

            // Set signals
            this.pipeline1_elems.sink.set_emit_signals(true);
            this.pipeline1_elems.sink.new_sample.connect(pipeline1_new_sample_handle);

            // Create the empty pipeline
            this.pipeline1 = new Gst.Pipeline("pipeline1");
            GLib.assert(this.pipeline1 != null);

            // Build the pipeline
            this.pipeline1.add_many(this.pipeline1_elems.src,
                                    this.pipeline1_elems.filter,
                                    this.pipeline1_elems.sink);
            bool res = this.pipeline1_elems.src.link_many(this.pipeline1_elems.filter,
                                                          this.pipeline1_elems.sink);
            GLib.assert(res);

            // Buffer for sink is empty
            this.pipeline1_buf = null;
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize audio pipeline 1.
        // (Microphone Input - P2P-network Output.)
        private void audio1_deinit(Model *model) throws ErrAudio
        {
            // Stop the pipeline
            pipeline_en_set(model,
                            this.pipeline1,
                            false // Stop
                           );

            // Free the buffer for sink
            if (this.pipeline1_buf != null)
                GLib.free(this.pipeline1_buf);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize audio pipeline 2.
        // (P2P-network Input - Speaker Output.)
        private void audio2_init(Model *model) throws ErrAudio
        {
            // Create the elements
            this.pipeline2_elems.src  = (Gst.App.Src) Gst.ElementFactory.make("appsrc",        "src");
            this.pipeline2_elems.sink =               Gst.ElementFactory.make("autoaudiosink", "sink");

            GLib.assert(this.pipeline2_elems.src  != null);
            GLib.assert(this.pipeline2_elems.sink != null);

            // Create the empty pipeline
            this.pipeline2 = new Gst.Pipeline("pipeline2");
            GLib.assert(this.pipeline2 != null);

            // Build the pipeline
            this.pipeline2.add_many(this.pipeline2_elems.src, this.pipeline2_elems.sink);
            bool res = this.pipeline2_elems.src.link(this.pipeline2_elems.sink);
            GLib.assert(res);

            // Start the pipeline
            pipeline_en_set(model,
                            this.pipeline2,
                            true // Start
                           );
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize audio pipeline 2.
        // (P2P-network Input - Speaker Output.)
        private void audio2_deinit(Model *model) throws ErrAudio
        {
            // Stop the pipeline
            pipeline_en_set(model,
                            this.pipeline2,
                            false // Stop
                           );
        }

        /*----------------------------------------------------------------------------*/

        // Get list of audio devices available in system.
        // Only devices of specified audio system are interested.
        // Filtered sound cards and digital input/output devices are
        // present.
        private Gee.ArrayList<Gst.Device> devs_get(Profile.cfg_audio_sys_t audio_sys, string filter) throws ErrAudio
        {
            // Check whether audio system autodetection is on
            var devs = new Gee.ArrayList<Gst.Device>();
            bool autodetection_sys;

            switch (audio_sys)
            {
            case Profile.cfg_audio_sys_t.AUTO:
                autodetection_sys = true;
                break;
#if __WXGTK__
            case Profile.cfg_audio_sys_t.JACK:
            case Profile.cfg_audio_sys_t.PIPEWIRE:
            case Profile.cfg_audio_sys_t.PULSEAUDIO:
            case Profile.cfg_audio_sys_t.OSS:
                autodetection_sys = false;
                break;
#elif __WXMSW__
#endif
            default:
                autodetection_sys = true;
                break;
            }

            if (!autodetection_sys)
            {
                // Audio system is specified.
                //
                // First, get list of all audio devices available in system
                // using GStreamer monitor.
                // Use filter to cut non-input (or non-output) devices.
                uint id = this.mon.add_filter(filter, null);
                this.mon.start();
                GLib.List<Gst.Device>? devs_all = this.mon.get_devices();
                this.mon.stop();
                this.mon.remove_filter(id);
                if (devs_all == null)
                    throw new ErrAudio.HW_INFO_ERR(_("Can't get list of audio devices"));

                // Now we should drop all devices which have no relation to
                // specified audio system
                // Go through device list
                devs_all.foreach((dev) =>
                {
                    // Create new temporary element and fill it with data from the
                    // current audio device
                    Gst.Element elem = dev.create_element("tmp");
                    GLib.assert(elem != null); // TODO: replace by exception?

                    // Get name of plugin that GStreamer proposes to use with this
                    // element
                    Gst.ElementFactory? factory = elem.get_factory();
                    GLib.assert(factory != null);

                    unowned string? plugin = factory.get_plugin_name();
                    GLib.assert(plugin != null);

                    // Now we can remain only devices of specified audio system
                    bool flag;

                    switch (audio_sys)
                    {
                    case Profile.cfg_audio_sys_t.AUTO:
                        flag = false;
                        break;
#if __WXGTK__
                    case Profile.cfg_audio_sys_t.JACK:
                        flag = plugin == "jack";
                        break;
                    case Profile.cfg_audio_sys_t.PIPEWIRE:
                        flag = plugin == "pipewire";
                        break;
                    case Profile.cfg_audio_sys_t.PULSEAUDIO:
                        flag = plugin == "pulseaudio";
                        break;
                    case Profile.cfg_audio_sys_t.OSS:
                        flag = plugin == "ossaudio";
                        break;
#elif __WXMSW__
#endif
                    default:
                        flag = false;
                        break;
                    }

                    if (flag)
                        devs.add(dev);
                });
            }

            return devs;
        }

        /*----------------------------------------------------------------------------*/

        // This method configures a pipeline.
        //
        // Set an audio subsystem.
        // (E. g. PipeWire, PulseAudio, JACK, OSS, etc.)
        //
        // It also changes input & output sound devices and their
        // parameters.
        // Settings from the profile's config [audio] section are used.
        private void pipeline_cfg(    Model        *model,
                                      Gst.Pipeline  pipeline,
                                  ref Gst.Element   elem,
                                      string        elem_name,
                                      string        filter,
                                      string        key_dev,
                                      string        key_vol) throws ErrAudio
        {
            // Determine the GStreamer plugin we have to use as source or as
            // sink element of the pipeline
            Profile.cfg_audio_sys_t audio_sys;

            try
            {
                audio_sys = (Profile.cfg_audio_sys_t)
                    model->self.cfg.get_integer("audio", "sys");
            }
            catch (GLib.KeyFileError ex)
            {
                audio_sys = Profile.cfg_audio_sys_t.AUTO;
                GLib.assert(false);
            }

            unowned string factory_name_new;
            unowned string plugin_new = plugin_get(pipeline,
                                                   audio_sys,
                                                   out factory_name_new);
            bool autodetection_sys    = plugin_new == "autodetect";

            // Check whether audio system autodetection is on
            bool replace_elem; Gst.Device? dev = null;
            bool set_vol;      double vol = 0;

            if (autodetection_sys)
            {
                // Audio system is not specified and should be autodetected.
                //
                // Now determine the GStreamer plugin we had used before.
                Gst.ElementFactory? factory = elem.get_factory();
                GLib.assert(factory != null);

                unowned string? plugin_old = factory.get_plugin_name();
                GLib.assert(plugin_old != null);

                // Continue if GStreamer plugin for the element is to be changed
                if (plugin_new == plugin_old)
                {
                    // Save the old element
                    replace_elem = false;
                    set_vol      = false;
                }
                else
                {
                    // Replace the old element by the new one
                    replace_elem = true;
                    set_vol      = false;
                }
            }
            else
            {
                // Audio system is specified.
                //
                // Determine the name of input/output device we should use.
                // We'll use it to find through GStreamer hardware list later.
                // Also prepare its volume setting.
                string dev_name;

                try
                {
                    dev_name = model->self.cfg.get_string("audio", key_dev);

                    vol = model->self.cfg.get_integer("audio", key_vol);
                    vol = vol > 100.0 ? 1.0 : vol / 100.0;
                }
                catch (GLib.KeyFileError ex)
                {
                    dev_name = "";
                    vol      = 0;

                    GLib.assert(false);
                }

                // Check whether card autodetection is on
                bool autodetection_card = dev_name == "";

                if (autodetection_card)
                {
                    // Audio device is not specified and should be autodetected
                }
                else
                {
                    // Audio device is specified.
                    //
                    // Find it in the list we'll get from GStreamer.
                    // We'll use it as source/sink element in pipeline.
                    Gee.ArrayList<Gst.Device> devs = devs_get(audio_sys, filter);

                    foreach (Gst.Device tmp in devs)
                    {
                        string tmp_name = tmp.get_display_name();

                        if (dev_name == tmp_name)
                        {
                            dev = tmp;
                            break;
                        }
                    }

                    if (dev == null)
                        throw new ErrAudio.HW_NOT_FOUND_ERR(_("There is no audio device with specified name"));
                }

                // Replace the old element by the new one
                replace_elem = true;
                set_vol      = true;
            }

            // No we are going to replace the old element by new one.
            // Configure it.
            // Rebuild the pipeline.
            if (replace_elem)
            {
                // Stop the pipeline
                try
                {
                    pipeline_en_set(model,
                                    pipeline,
                                    false // Stop
                                   );
                }
                catch (ErrAudio ex)
                {
                    GLib.debug(ex.message);
                }

                // Drop the old element
                Gst.Element **elem1;
                Gst.Element **elem2;

                if (pipeline == this.pipeline1)
                {
                    elem1 = &this.pipeline1_elems.src;
                    elem2 = &this.pipeline1_elems.filter;
                }
                //else if (pipeline == this.pipeline2)
                else
                {
                    elem1 = &this.pipeline2_elems.src;
                    elem2 = &this.pipeline2_elems.sink;
                }

                (*elem1)->unlink(*elem2);
                bool res = pipeline.remove(elem);
                //GLib.assert(res);

                // Recreate the element
                if (dev == null)
                {
                    // Create new empty source/sink element
                    elem = Gst.ElementFactory.make(factory_name_new, elem_name);
                    GLib.assert(elem != null);
                }
                else
                {
                    // Create new source/sink element and fill it with data from the
                    // found audio device
                    elem = dev.create_element(elem_name);
                    GLib.assert(elem != null); // TODO: replace by exception?

                    // Ensure the GStreamer plugin from the hardware list matches
                    // the specified audio subsystem
                    Gst.ElementFactory? factory = elem.get_factory();
                    GLib.assert(factory != null);

                    unowned string? plugin_new_real = factory.get_plugin_name();
                    GLib.assert(plugin_new_real != null);

                    if (plugin_new_real != plugin_new)
                        throw new ErrAudio.AUDIO_SYS_AND_HW_MISMATCH(_("Audio device is possibly related to another audio system"));
                }

                // Rebuild the pipeline
                res = pipeline.add_element(elem);
                if (res)
                    res &= (*elem1)->link(*elem2);

                if (!res)
                    throw new ErrAudio.PIPELINE_FAIL(_("Audio subsystem fails while configuring the capture/playback"));

                // Start the pipeline
                if ((pipeline == pipeline1 && (model->peer_buddies_audio.size > 0 ||
                                               model->peer_buddies_video.size > 0)) ||
                    (pipeline == pipeline2))
                {
                    pipeline_en_set(model,
                                    pipeline,
                                    true // Start
                                   );
                }
            }

            // Configure the element.
            // Volume is to be set.
            if (set_vol)
            {
                vol_set(elem,
                        audio_sys,
                        vol);
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Audio constructor
         *
         * @param model Model
         */
        public Audio(Model *model)
        {
            // GStreamer device monitor
            this.mon = new Gst.DeviceMonitor();
#if SHOW_HIDDEN_AUDIO_HW
            bool flag = true;
#else
            bool flag = false;
#endif
            this.mon.set_show_all_devices(flag);
            bool flag_real = this.mon.get_show_all_devices();
            GLib.assert(flag == flag_real);

#if GSTREAMER_INITIALIZED_BY_AUDIO
            // Initialize GStreamer
            try
            {
                unowned string[] args = model->args;
                bool res = Gst.init_check(ref args);
                GLib.assert(res);
                Gst.init(ref args);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                GLib.assert(false);
            }
#endif

            // Initialize audio pipelines.
            //
            // Initialize pipeline 1.
            try
            {
                audio1_init(model);
            }
            catch (ErrAudio ex)
            {
                GLib.debug(ex.message);
            }

            // Initialize pipeline 2
            try
            {
                audio2_init(model);
            }
            catch (ErrAudio ex)
            {
                GLib.debug(ex.message);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Audio destructor
         */
        ~Audio()
        {
            // Deinitialize audio pipelines.
            //
            // Deinitialize pipeline 1.
            try
            {
                audio1_deinit(model);
            }
            catch (ErrAudio ex)
            {
                //GLib.assert(false);
            }

            // Deinitialize pipeline 2.
            try
            {
                audio2_deinit(model);
            }
            catch (ErrAudio ex)
            {
                //GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get list of capture devices available in system.
         * All sound cards and digital input devices are present.
         *
         * @param audio_sys Only devices of specified audio system are
         *                  interested
         */
        public Gee.ArrayList<Gst.Device> devs_cap_get(Profile.cfg_audio_sys_t audio_sys) throws ErrAudio
        {
            // Get device list using GStreamer monitor and its ability to
            // filter only capture devices
            return devs_get(audio_sys, "Audio/Source");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get list of playback devices available in system.
         * All sound cards and digital output devices are present.
         *
         * @param audio_sys Only devices of specified audio system are
         *                  interested
         */
        public Gee.ArrayList<Gst.Device> devs_playback_get(Profile.cfg_audio_sys_t audio_sys) throws ErrAudio
        {
            // Get device list using GStreamer monitor and its ability to
            // filter only playback devices
            return devs_get(audio_sys, "Audio/Sink");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set an audio subsystem.
         * (E. g. PipeWire, PulseAudio, JACK, OSS, etc.)
         *
         * It also changes input & output sound devices and their
         * parameters.
         * Settings from the profile's config [audio] section are used.
         *
         * @param model Model
         */
        public void sys_cfg(Model *model) throws ErrAudio
        {
            // This method should pass to the end.
            // So we won't return immediately if exception raises.
            // Instead we remember this exception and throw it just before
            // return.
            // (The informing only about the first exception is enough.)
            ErrAudio? sys_cfg_ex = null;

            // Configure the pipeline 1.
            // (Microphone Input - P2P-network Output.)
            try
            {
                pipeline_cfg(model,
                             pipeline1,
                             ref this.pipeline1_elems.src,
                             "src",
                             "Audio/Source",
                             "cap_dev",
                             "gain");
            }
            catch (ErrAudio ex)
            {
                GLib.debug(ex.message);
                sys_cfg_ex = ex;
            }

            // Configure the pipeline 2.
            // (P2P-network Input - Speaker Output.)
            try
            {
                pipeline_cfg(model,
                             pipeline2,
                             ref this.pipeline2_elems.sink,
                             "sink",
                             "Audio/Sink",
                             "playback_dev",
                             "vol");
            }
            catch (ErrAudio ex)
            {
                GLib.debug(ex.message);
                if (sys_cfg_ex == null)
                    sys_cfg_ex = ex;
            }

            // Throw an exception if any
            if (sys_cfg_ex != null)
                throw sys_cfg_ex;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Enable microphone
         *
         * @param model Model
         */
        public void mic_en(Model *model) throws ErrAudio
        {
            // Start pipeline 1.
            // (Microphone Input - P2P-network Output.)
            pipeline_en_set(model,
                            pipeline1,
                            true // Start
                           );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disable microphone
         *
         * @param model Model
         */
        public void mic_dis(Model *model) throws ErrAudio
        {
            // Stop pipeline 1.
            // (Microphone Input - P2P-network Output.)
            pipeline_en_set(model,
                            pipeline1,
                            false // Stop
                           );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Play audio frame
         *
         * @param model        Model
         * @param pcm          An array of audio samples in S16LE format
         *                     (number of samples * cnt_channels elements).
         *                     Expected format is LRLRLR, i. e. interleaved.
         * @param cnt_channels Number of audio channels
         * @param bitrate      Sampling rate used in this frame
         */
        public void play(Model   *model,
                         int16[]  pcm,
                         uint     cnt_channels,
                         uint     bitrate)

            requires (pcm.length   > 0)
            requires (cnt_channels > 0)
            requires (bitrate      > 0)
        {
            // Set caps
            this.pipeline2_elems.src.caps = new Gst.Caps.simple("audio/x-raw",
                                                                "format",   GLib.Type.STRING, "S16LE",
                                                                "channels", GLib.Type.INT,    cnt_channels,
                                                                "rate",     GLib.Type.INT,    bitrate,
                                                                "layout",   GLib.Type.STRING, "interleaved");

            // Push the incoming buffer into the audio pipeline
            Gst.Memory mem = Gst.Memory.new_wrapped<void *>(Gst.MemoryFlags.READONLY,
                                                            (uint8[]) pcm,
                                                            0,   // No offset
                                                            sizeof(int16) * pcm.length,
                                                            null // Not used
                                                           );
            var buf = new Gst.Buffer();

            buf.append_memory(mem);
            this.pipeline2_elems.src.push_buffer(buf);
        }
    }
}

