/*
 *    model.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Model object
 */
Model.Model *model;

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors
     */
    public errordomain ErrGeneral
    {
        /**
         * Make directory error
         */
        CANNOT_CREATE_DIR,

        /**
         * Can't read directory
         */
        FAILED_READ_DIR,

        /**
         * Can't bootstrap
         */
        FAILED_BOOTSTRAP,

        /**
         * Bad node format
         */
        BAD_NODE_FMT,

        /**
         * Bad node type
         */
        BAD_NODE_TYPE,

        /**
         * Bad buddy format
         */
        BAD_BUDDY_FMT,

        /**
         * Unknown buddy
         */
        BUDDY_NOT_FOUND_ERR,

        /**
         * User wants a language but has no required locale installed
         */
        LOCALE_IS_NOT_INSTALLED_ERR,
    }

    /**
     * JSON parsing
     */
    public errordomain ErrModelJSON
    {
        /**
         * JSON format error
         */
        JSON_INVALID,
    }

    /**
     * Hexadecimal converting with public_key_bin_set(),
     * hex2bin_convert() methods
     */
    public errordomain ErrModelHex
    {
        /**
         * The length of a hexadecimal string is wrong
         */
        HEX_WRONG_LEN,

        /**
         * Bad hexadecimal string
         */
        HEX_INVALID,
    }

    /**
     * Tox initialization
     */
    public errordomain ErrModelInitTox
    {
        /**
         * The function was unable to allocate enough memory to store
         * the internal structures for the Tox object
         */
        MEM_ALLOC,

        /**
         * The function was unable to bind to a port. This may mean that
         * all ports have already been bound, e. g. by other Tox
         * instances, or it may mean a permission error. You may be able
         * to gather more information from errno.
         */
        PORT_ALLOC,

        /**
         * Proxy type was valid but the proxy host passed had an invalid
         * format or was NULL
         */
        PROXY_BAD_HOST,

        /**
         * Proxy type was valid, but the proxy port was invalid
         */
        PROXY_BAD_PORT,

        /**
         * The proxy address passed could not be resolved
         */
        PROXY_NOT_FOUND_ERR,

        /**
         * The data format was invalid. This can happen when loading
         * data that was saved by an older version of Tox, or when the
         * data has been corrupted. When loading from badly formatted
         * data, some data may have been loaded, and the rest is
         * discarded. Passing an invalid length parameter also causes
         * this error.
         */
        LOAD_BAD_FORMAT,
    }

    /**
     * Buddy adding errors
     */
    public errordomain ErrModelAddBuddy
    {
        /**
         * The buddy Tox ID belongs to the sending client
         */
        ADDR_OWN_KEY,

        /**
         * A buddy request has already been sent, or the Tox ID
         * belongs to a buddy that is already on the buddy list
         */
        SENT_ALREADY,

        /**
         * The buddy Tox ID checksum failed
         */
        BAD_CHECKSUM,

        /**
         * The buddy was already there, but the nospam value was
         * different
         */
        SET_NEW_NOSPAM,

        /**
         * A memory allocation failed when trying to increase the buddy
         * list size
         */
        MEM_ALLOC,
    }

    /**
     * This is the Model class
     */
    [SingleInstance]
    public class Model : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Path to currently running executable, i. e. /usr/local/bin
        private string path;

        // i18n constants
        private const string CHARSET = "UTF-8"; // Character set of the translation domain

        // Configuration filename. This file contains
        // profile-independent application settings.
        private const string CFG_FILENAME = "yat.cfg";

        // Properties.
        //
        // Note that Tox A/V instance should be freed before Tox
        // instance.
        // So it is declared before.
        // It is important, because program crashes otherwise.
        private ToxAV.ToxAV tox_av_;
        private ToxCore.Tox tox_;

        // Timers
        private uint timer_tox_iteration;    // Timer performing Tox iteration
        private uint timer_tox_av_iteration; // Timer performing Tox A/V iteration

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        // See doc/FILE_HIERARCHY for details

        /**
         * URL of list with currently online bootstrap nodes
         */
        public const string NODES_URL = "http://www.lovecry.pt/dl/tox-nodes.json";

        /**
         * URL of list with currently online bootstrap nodes. (Alternative.)
         */
        public const string NODES_URL_ALT = "https://nodes.tox.chat/json";

        /**
         * File with currently online bootstrap nodes
         */
        public const string NODES_FILE = "tox-nodes.json";

        /**
         * CLI arguments
         */
        public string[] args { public get; public set; }

        /**
         * Program installation prefix, i. e. /usr/local
         */
        public string prefix { public get; private set; }

        /**
         * false: program is not installed (runned from build directory),
         * true:  program is installed to system directory
         */
        public bool use_prefix { public get; private set; }

        /**
         * Directory with translations, i. e. ../locale
         */
        public string locale { public get; private set; }

        /**
         * Directory with plugin translations, i. e. ../locale/plugins
         */
        public string locale_plugins { public get; private set; }

        /**
         * Directory for user configuration,
         * i. e. /home/user/.config/yat
         */
        public string path_cfg { public get; private set; }

        /**
         * Directory for user data,
         * i. e. /home/user/.local/yat
         */
        public string path_data { public get; private set; }

        /**
         * Directory with plugins, i. e. plugins
         */
        public string path_plugins { public get; private set; }

        /**
         * Directory with libraries, i. e. /usr/lib/x86_64-linux-gnu/yat
         */
        public string? path_lib { public get; private set; }

        /**
         * Profile-independent application settings.
         *
         * They are read from CFG_FILENAME file.
         */
        public GLib.KeyFile cfg { public get; private set; }

        /**
         * Tox A/V instance.
         *
         * It should be freed before Tox instance.
         */
        public ToxAV.ToxAV tox_av
        {
            public get
            {
                return this.tox_av_;
            }

            public owned set // valac 0.56.3 warning if no explicit setter for this property
            {
                this.tox_av_ = (owned) value;
            }
        }

        /**
         * Tox instance.
         *
         * It should be freed after Tox A/V instance.
         * So it is defined after.
         */
        public ToxCore.Tox tox
        {
            public get
            {
                return this.tox_;
            }

            private owned set // valac 0.56.3 warning if no explicit setter for this property
            {
                this.tox_ = (owned) value;
            }
        }

        /**
         * false: no Tox iteration is needed,
         * true:  no Tox iteration is needed,
         */
        public bool tox_need_iteration { public get; public set; }

        /**
         * false: no Tox A/V iteration is needed,
         * true:  no Tox A/V iteration is needed,
         */
        public bool tox_av_need_iteration { public get; public set; }

        /**
         * Bootstrap thread
         */
        public Threads.ThreadBootstrap? thread_bootstrap { public get; public set; } // Thread performing boostrapping

        /**
         * GLib event loop thread
         */
        public Threads.ThreadGLib?      thread_glib      { public get; public set; } // Thread rotating the GLib event loop

        /**
         * UI event loop thread
         */
        public Threads.ThreadUI?        thread_ui        { public get; public set; } // Thread rotating the UI event loop

        /**
         * Threads
         */
        public Gee.HashMap<GLib.Thread<void>, Threads.Thread<void>> threads;

        /**
         * Mutex used to protect shared data between threads.
         *
         * Also libtoxcore methods are not thread-safe.
         * GUI library is usually not thread-safe.
         * This mutex solve their protection.
         *
         * Warning! It is forbidden to make Mutex a property of Object
         * class. So there are wrapper methods allowing to control it
         * manually: mutex_lock(), mutex_unlock(). You may need it to
         * use in script GIR-based plugins.
         */
        public GLib.Mutex mutex;

        /**
         * Profiles available on this PC
         */
        public Gee.ArrayList<Profile> profiles
        {
            public get;
            private set;
        }

        /**
         * Self profile
         */
        public unowned Profile? self
        {
            public get;
            public set;
        }

        /**
         * Buddy list in order as it is shown in GUI.
         * Not all buddies are present here, only filtered.
         *
         * Represented as array list.
         */
        public Gee.ArrayList<Buddy> buddies_lst
        {
            public get;
            private set;
        }

        /**
         * Buddy list with all buddies.
         * All buddies are presented here, not only those, which showed
         * in GUI.
         *
         * Represented as hash map.
         */
        public Gee.HashMap<uint32, Buddy> buddies_hash
        {
            public get;
            private set;
        }

        /**
         * Current peer in buddy list
         */
        public unowned Buddy? peer_buddy
        {
            public get;
            public set;
        }

        /**
         * Current A/V peers in buddy list (audio)
         */
        public Gee.HashMap<uint32, Buddy> peer_buddies_audio
        {
            public get;
            public set;
        }

        /**
         * Current A/V peers in buddy list (video)
         */
        public Gee.HashMap<uint32, Buddy> peer_buddies_video
        {
            public get;
            public set;
        }

        /**
         * Conference list in order as it is shown in GUI.
         * Not all conferences are present here, only filtered.
         *
         * Represented as array list.
         */
        public Gee.ArrayList<Conference> conferences_lst
        {
            public get;
            private set;
        }

        /**
         * Conference list with all conferences.
         * All conferences are presented here, not only those, which
         * showed in GUI.
         *
         * Represented as hash map.
         */
        public Gee.HashMap<uint32, Conference> conferences_hash
        {
            public get;
            private set;
        }

        /**
         * Current peer in conference list
         */
        public unowned Conference? peer_conference
        {
            public get;
            public set;
        }

        /**
         * Member list in order as it is shown in GUI.
         * Not all members are present here, only filtered.
         *
         * Represented as array list.
         */
        public Gee.ArrayList<Member> members_lst
        {
            public get;
            private set;
        }

        /**
         * Buddy requests
         */
        public Gee.ArrayList<BuddyReq> buddy_reqs
        {
            public get;
            private set;
        }

        /**
         * Conference invitations
         */
        public Gee.ArrayList<ConferenceInvitation> conference_invitations
        {
            public get;
            private set;
        }

        /**
         * Width of the program icon
         */
        public const size_t ICON_WIDTH  = 200;

        /**
         * Height of the program icon
         */
        public const size_t ICON_HEIGHT = 200;

        /**
         * Audio
         */
        public Audio audio { public get; public set; }

        /**
         * Video
         */
        public Video video { public get; public set; }

        /**
         * Plugins available on this PC.
         *
         * Represented as libpeas data collection.
         * Sorted by dependencies, Peas internal logic.
         */
        public Peas.ExtensionSet? plugins_ext
        {
            public get;
            public set;
        }

        /**
         * Plugins available on this PC.
         *
         * Represented as array list.
         * Sorted by name.
         */
        public Gee.ArrayList<Peas.PluginInfo> plugins_lst
        {
            public  get;
            private set;
        }

        /**
         * false: plugins should not be loaded,
         * true:  plugins should be loaded
         */
        public bool should_load_plugins { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Thread
         */
        [Flags]
        public enum thread_t
        {
            BOOTSTRAP,
            GLIB,
            UI,

            ALL = BOOTSTRAP | GLIB | UI,
        }

        /**
         * Configuration key.
         * language key in [appearance] group.
         */
        public enum cfg_appearance_lang_t
        {
            DEF   = 0,
            EN_UK = 1,
            EN_US = 2,
            RU    = 3,

            SIZE,
        }

        /**
         * Buddy request
         */
        public class BuddyReq
        {
            /**
             * Public key as binary
             */
            public uint8[] key;

            /**
             * Public key as hexadecimal ASCII
             */
            public string key_str;

            /**
             * Buddy request
             */
            public string msg { public get; public set; }

            /**
             * false: buddy request was added in previous Tox session,
             * true:  buddy request was added in this Tox session
             */
            public bool added_in_this_tox_session;

            /*
             * Date and time of the adding to buddy request list
             */
            public GLib.DateTime datetime;
        }

        /**
         * Conference invitation
         */
        public class ConferenceInvitation
        {
            /**
             * Buddy.
             *
             * Who has invited us?
             */
            public Buddy buddy;

            /**
             * Type
             */
            public ToxCore.CONFERENCE_TYPE type;

            /**
             * Cookie as binary
             */
            public uint8[] cookie;

            /**
             * Cookie as hexadecimal ASCII
             */
            public string cookie_str;

            /*
             * Date and time of the adding to conference invitation list
             */
            public GLib.DateTime datetime;
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize model
        private void init()
        {
            this.cfg                    = new GLib.KeyFile();
            this.thread_bootstrap       = null;
            this.thread_glib            = null;
            this.thread_ui              = null;
            this.threads                = new Gee.HashMap<GLib.Thread<void>, Threads.Thread<void>>();
            this.mutex                  = GLib.Mutex();
            this.profiles               = new Gee.ArrayList<Profile>();
            this.self                   = null;
            this.buddies_lst            = new Gee.ArrayList<Buddy>();
            this.buddies_hash           = new Gee.HashMap<uint32, Buddy>();
            this.peer_buddy             = null;
            this.peer_buddies_audio     = new Gee.HashMap<uint32, Buddy>();
            this.peer_buddies_video     = new Gee.HashMap<uint32, Buddy>();
            this.conferences_lst        = new Gee.ArrayList<Conference>();
            this.conferences_hash       = new Gee.HashMap<uint32, Conference>();
            this.peer_conference        = null;
            this.members_lst            = new Gee.ArrayList<Member>();
            this.buddy_reqs             = new Gee.ArrayList<BuddyReq>();
            this.conference_invitations = new Gee.ArrayList<ConferenceInvitation>();
            this.plugins_ext            = null;
            this.plugins_lst            = new Gee.ArrayList<Peas.PluginInfo>();
            this.should_load_plugins    = false;
        }

        /*----------------------------------------------------------------------------*/

        // Initialize prefix.
        //
        // Finds the program installation prefix if it is installed.
        private void prefix_init()
        {
            // Find path to currently running executable
            int len = WhereAmI.exec_path_get(null, null);
            var filepath = new char[len];
            int len_dir;
            WhereAmI.exec_path_get(filepath, out len_dir);

            // Cut the directory name
            this.path = "";
            for (int byte = 0; byte < len_dir; byte++)
                this.path += filepath[byte].to_string();

            // Find the program installation prefix if any
            try
            {
                var regex = new GLib.Regex(Path.DIR_SEPARATOR_S + "bin\\z");

                this.prefix = regex.replace_literal(this.path, -1, 0, "");
                this.use_prefix  = this.path != this.prefix;
            }
            catch (RegexError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // Initialize working directory.
        //
        // Change current working directory to where is the program located.
        private void dir_init()
        {
            // Change directory to the base path
            string working_dir = this.use_prefix ?
                GLib.Path.build_filename(prefix, "share", "yat") :
                this.path;

            int res = GLib.Environment.set_current_dir(working_dir);
            GLib.assert(res == 0);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize locale
        private void locale_init()
        {
            // Directory with translations.
            // Directory with plugin translations.
            if (this.use_prefix)
            {
                this.locale         = GLib.Path.build_filename(prefix, "share", "locale");
                this.locale_plugins = this.locale;
            }
            else
            {
                this.locale         = "locale";
                this.locale_plugins = GLib.Path.build_filename("plugins", "locale");
            }

            // Install the specified system locale
            GLib.Intl.setlocale(GLib.LocaleCategory.ALL, "");
            GLib.Intl.setlocale(GLib.LocaleCategory.COLLATE, "C");

            // Bind domain to the directory with translations
            domain_add(this.locale, Cfg.DOMAIN);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize configuration and data directories.
        //
        // Create them if they don't exist.
        // Read data directory to find out what the profiles exist.
        private void cfg_and_data_init()
        {
            // Initialize configuration directory
            this.path_cfg = GLib.Environment.get_user_config_dir();
            this.path_cfg = GLib.Path.build_filename(this.path_cfg, "yat");

            // Initialize data directory
            this.path_data = GLib.Environment.get_user_data_dir();
            this.path_data = GLib.Path.build_filename(this.path_data, "yat");

            // Create configuration and data directories
            try
            {
                int res = GLib.DirUtils.create_with_parents(this.path_cfg, 0755);
                if (res == -1)
                    throw new ErrGeneral.CANNOT_CREATE_DIR(this.path_cfg);

                res = GLib.DirUtils.create_with_parents(this.path_data, 0755);
                if (res == -1)
                    throw new ErrGeneral.CANNOT_CREATE_DIR(this.path_data);
            }
            catch (ErrGeneral ex)
            {
                GLib.error(_("Can't create directory %s: %s"), ex.message, first_ch_lowercase_make(GLib.strerror(GLib.errno)));
            }

            // Open and read data directory.
            // Directories founded here are profile names.
            try
            {
                // Open data directory
                GLib.Dir cur = GLib.Dir.open(this.path_data, 0);

                // Read it file by file
                for (;;)
                {
                    // Read next file
                    GLib.errno = 0;
                    unowned string? filename = cur.read_name();

                    // Exit if no files are remaining.
                    // Go to next file if error appears.
                    if (filename == null)
                    {
                        if (GLib.errno != 0)
                            throw new ErrGeneral.FAILED_READ_DIR(this.path_data);
                        break;
                    }

                    // We are interested only in directories
                    string filepath = GLib.Path.build_filename(this.path_data, filename);
                    if (!GLib.FileUtils.test(filepath, GLib.FileTest.IS_DIR))
                        continue;

                    // Ensure there is Tox main profile file here
                    string filepath_tox = GLib.Path.build_filename(filepath, filename + ".tox");
                    if (!GLib.FileUtils.test(filepath_tox, GLib.FileTest.IS_REGULAR))
                        continue;

                    // Add profile
                    try
                    {
                        // Initialize profile.
                        // Add profile.
                        var profile = new Profile(filename);
                        this.profiles.add(profile);

                        // Set that profile uses password (and is encrypted) or doesn't
                        // use password (and is not encrypted)
                        profile.use_pwd = profile.is_encrypted(this);
                    }
                    catch (GLib.FileError ex)
                    {
                        GLib.error(ex.message);
                    }
                }

                // Sort profiles
                profiles_sort();

                // Read no avatar icon from filesystem
                var file = GLib.File.new_for_path(Person.img_filepath_default_avatar_get());
                GLib.FileInputStream stream = file.read();
                Person.no_avatar = new Gdk.Pixbuf.from_stream(stream);
            }
            catch (ErrGeneral ex)
            {
                GLib.debug(_("Can't read directory %s: %s"), ex.message, first_ch_lowercase_make(GLib.strerror(GLib.errno)));
            }
            catch (GLib.FileError ex)
            {
                GLib.error(ex.message);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
            }
        }

        /*----------------------------------------------------------------------------*/

        // Initialize POSIX signals
        private void posix_sigs_init()
        {
            //Posix.@signal(Posix.Signal.INT,  Ctrl.Ctrl.sig_handle);
            //Posix.@signal(Posix.Signal.KILL, Ctrl.Ctrl.sig_handle);
            //Posix.@signal(Posix.Signal.TERM, Ctrl.Ctrl.sig_handle);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize audio
        private void audio_init()
        {
            this.audio = new Audio(this);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize video
        private void video_init()
        {
            this.video = new Video(this);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize plugin directory.
        //
        // Reads data directory to find out what the plugins exist.
        private void plugins_init()
        {
            // Get the default engine
            var engine = Peas.Engine.get_default();

            // Enable loaders
            engine.enable_loader("lua5.1");
            engine.enable_loader("perl");
            engine.enable_loader("python3");

            // Set directory with plugins.
            // Append a search path to the list of paths where to look for
            // plugins.
            this.path_plugins = this.use_prefix ? "." : "plugins";
            engine.add_search_path(this.path_plugins,
                                   null // Data directory is the same
                                  );
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize plugins
        private void plugins_deinit()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Sort plugins
          */
        public void plugins_sort()
        {
            this.plugins_lst.sort((a, b) =>
            {
                unowned string filename_a = a.get_module_name();
                unowned string filename_b = b.get_module_name();

                string domain_a = "yat-plugins-" + filename_a;
                string domain_b = "yat-plugins-" + filename_b;

                unowned string a_name = dgettext(domain_a, a.get_name());
                unowned string b_name = dgettext(domain_b, b.get_name());

                if (a_name > b_name)
                    return 1;
                else if (a_name == b_name)
                    return 0;
                else
                    return -1;
            });
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Load plugins.
         *
         * Discovers plugins.
         * Sorts them alphabetically.
         * Loads those plugins which are enabled in configuration for
         * autostart.
         *
         * @return The first error message appeared if any
         */
        public string? plugins_load()
        {
            string? err = null;

            // Get the default engine
            var engine = Peas.Engine.get_default();

            // Initialize extension set.
            //
            // Allocates object for each plugin of Plugin type.
            // Sets properties.
            this.plugins_ext = new Peas.ExtensionSet(engine, typeof(Plugin),
                "model_", model,
                "view_",  view,
                "ctrl_",  ctrl,
                null
            );

            // Connect load signal
            this.plugins_ext.extension_added.connect((info, plugin) =>
            {
                // Plugin description fields
                unowned string filename = info.get_module_name();
                        string domain   = "yat-plugins-" + filename;
                unowned string name     = GLib.dgettext(domain, info.get_name());

                // Plugin entry point
                Plugin plugin_ = plugin as Plugin;
                plugin_.plugin_load();
                GLib.debug(_("Plugin %s has been started"), name);
            });

            // Connect unload signal
            this.plugins_ext.extension_removed.connect((info, plugin) =>
            {
                // Plugin description fields
                unowned string filename = info.get_module_name();
                        string domain   = "yat-plugins-" + filename;
                unowned string name     = GLib.dgettext(domain, info.get_name());

                // Plugin exit point
                Plugin plugin_ = plugin as Plugin;
                plugin_.plugin_unload();
                GLib.debug(_("Plugin %s has been stopped"), name);
            });

            // Add discovered plugins to array to be sorted.
            // Sort plugins.
            foreach (unowned Peas.PluginInfo info in this.plugins_ext.engine.get_plugin_list())
                this.plugins_lst.add(info);
            plugins_sort();

            // Initialize plugins that are enabled
            foreach (unowned Peas.PluginInfo info in this.plugins_ext.engine.get_plugin_list())
            {
                // Plugin description fields
                unowned string filename = info.get_module_name();
                        string domain   = "yat-plugins-" + filename;

                // Do the plugin l14n & i18n.
                //
                // Bind domain to the directory with plugin translations.
                domain_add(this.locale_plugins, domain); // gettext strings are to be translatable
                view->domain_add(domain);                // XRC strings are to be translatable

                unowned string name = GLib.dgettext(domain, info.get_name());
                GLib.debug(_("Discovered new plugin %s"), name);

                // Is this plugin enabled?
                bool en_plugin;

                try
                {
                    en_plugin = this.self.cfg.get_boolean("plugins", filename);
                }
                catch (GLib.KeyFileError ex)
                {
                    this.self.cfg.set_boolean("plugins", filename, false);
                    en_plugin = false;
                };

                // Load plugin
                if (en_plugin)
                {
                    // The plugin is enabled
                    try
                    {
                        // Load it
                        Plugin plugin = this.plugins_ext.get_extension(info) as Plugin;
                        plugin.load(model, info);
                    }
                    catch (GLib.Error ex)
                    {
                        GLib.debug(ex.message);
                        if (err == null)
                            err = ex.message;

                        // Something has gone wrong.
                        // The plugin has not been loaded.
                        // Assume that this plugin is disabled.
                        // Update per-profile config with the plugin disabled.
                        en_plugin = false;
                        this.self.cfg.set_boolean("plugins", filename, en_plugin);
                    }
                }
            }

            return err;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Unload plugins
         *
         * @return The first error message appeared if any
         */
        public string? plugins_unload()
        {
            string? err = null;

            // Deinitialize plugins that are enabled
            foreach (unowned Peas.PluginInfo info in this.plugins_ext.engine.get_plugin_list())
            {
                // Is this plugin enabled?
                unowned string filename = info.get_module_name();
                bool en_plugin;

                try                          { en_plugin = this.self.cfg.get_boolean("plugins", filename); }
                catch (GLib.KeyFileError ex) { en_plugin = false; }

                if (en_plugin)
                {
                    // This plugin is enabled.
                    // Now we have to unload it.
                    try
                    {
                        // Unload it
                        Plugin plugin = this.plugins_ext.get_extension(info) as Plugin;
                        plugin.unload(model, info);
                    }
                    catch (GLib.Error ex)
                    {
                        GLib.debug(ex.message);
                        if (err == null)
                            err = ex.message;
                    }
                }
            }

            return err;
        }

        /*----------------------------------------------------------------------------*/

        // Some debug
        private void dbg()
        {
            GLib.debug(_("Path to program executable: %s"), this.path);
            GLib.debug(_("Program prefix: %s"), use_prefix ? prefix : _("not used"));
            GLib.debug(_("Working directory: %s"), GLib.Environment.get_current_dir());
            GLib.debug(_("User configuration directory: %s"), this.path_cfg);
            GLib.debug(_("User data directory: %s"), this.path_data);
            GLib.debug(_("Translation domain: %s"), Cfg.DOMAIN);
            GLib.debug(_("Locale directory: %s"), this.locale);
            GLib.debug(_("Plugin locale directory: %s"), this.locale_plugins);
            GLib.debug(_("Supported locales are:"));
            foreach (unowned string lang in GLib.Intl.get_language_names())
                GLib.debug(lang);
            GLib.debug(_("Plugin directory: %s"), this.path_plugins);
            GLib.debug(_("Library directory: %s"), this.path_lib == null ? _("not used") : this.path_lib);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Model constructor.
         *
         * Initializes prefix.
         * Initializes working directory.
         * Initializes locale.
         * Initializes configuration and data directory.
         * Initializes POSIX signals.
         * Initializes audio.
         * Initializes video.
         * Initializes plugins.
         *
         * Loads profile-independent application settings from
         * configuration file on disk to memory.
         * Reinitializes locale using the parameter from the loaded
         * config.
         *
         * Outputs some debug messages.
         *
         * @param args CLI arguments
         */
        public Model(string[] args)
        {
            // Remember the program arguments
            this.args = args;

            // Initialize model.
            // Initialize prefix.
            // Initialize working directory.
            // Initialize locale.
            // Initialize configuration and data directory.
            // Initialize POSIX signals.
            // Initialize audio.
            // Initialize video.
            // Initialize plugins.
            init();
            prefix_init();
            dir_init();
            locale_init();
            cfg_and_data_init();
            posix_sigs_init();
            audio_init();
            video_init();
            plugins_init();

            // Load profile-independent application settings from
            // configuration file on disk to memory.
            // Reinitialize locale using the parameter from the loaded
            // config.
            try
            {
                // Apply new locale settings
                cfg_load();
                locale_reinit();
            }
            catch (ErrGeneral ex)
            {
                // Invalid locale settings.
                // Reset them.
                GLib.debug(ex.message);

                this.cfg.set_integer("appearance", "lang", cfg_appearance_lang_t.DEF);
            }

            // Output some debug messages
            dbg();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Model constructor.
         */
        ~Model()
        {
            // Deinitialize plugins
            plugins_deinit();
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Get locale, e. g. ru_RU
         *
         * @return Current locale
         */
        public string locale_get()
        {
            string? locale;

            try
            {
                cfg_appearance_lang_t lang =
                    (Model.cfg_appearance_lang_t) this.cfg.get_integer("appearance", "lang");

                switch (lang)
                {
                case cfg_appearance_lang_t.DEF:
                default:

                                   locale = GLib.Intl.setlocale(GLib.LocaleCategory.ALL, null);
                    var            regex  = new GLib.Regex("^(.+)\\.(.+)\\z");
                    GLib.MatchInfo info;
                    bool           res    = regex.match(locale, 0, out info);

                    locale = res ? info.fetch(1) : "";
                    //string? encoding = info.fetch(2);

                    break;

                case cfg_appearance_lang_t.EN_UK:
                    locale = "en_GB";
                    break;
                case cfg_appearance_lang_t.EN_US:
                    locale = "en_US";
                    break;
                case cfg_appearance_lang_t.RU:
                    locale = "ru_RU";
                    break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                locale = "";
                GLib.assert(false);
            }
            catch (GLib.RegexError ex)
            {
                locale = "";
                GLib.assert(false);
            }

            return locale;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Get locale with encoding suffix, e. g. ru_RU.UTF-8
         *
         * @return Current locale
         */
        public string locale_with_encoding_get()
        {
            string locale;

            try
            {
                cfg_appearance_lang_t lang =
                    (Model.cfg_appearance_lang_t) this.cfg.get_integer("appearance", "lang");

                switch (lang)
                {
                case cfg_appearance_lang_t.DEF:
                default:
                    locale = "";
                    break;
                case cfg_appearance_lang_t.EN_UK:
                    locale = "en_GB.UTF-8";
                    break;
                case cfg_appearance_lang_t.EN_US:
                    locale = "en_US.UTF-8";
                    break;
                case cfg_appearance_lang_t.RU:
                    locale = "ru_RU.UTF-8";
                    break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                locale = "";
                GLib.assert(false);
            }

            return locale;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Reinitialize locale
          *
          * @return false: can't set locale,
          *         true:  locale has been set
          */
        public bool locale_reinit() throws ErrGeneral
        {
            // Get locale
            string locale = locale_with_encoding_get();

            // Set locale
            bool res = GLib.Intl.setlocale(GLib.LocaleCategory.ALL, locale) != null;

            if (!res)
            {
                throw new ErrGeneral.LOCALE_IS_NOT_INSTALLED_ERR(
                    _("No required locale installed: %s.\n" +
                      "Try to install it:\n" +
                      "sudo dpkg-reconfigure locales\n" +
                      "(For Debian GNU/Linux and derivatives.)").printf(locale));
            }

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Bind domain to the directory with translations
          *
          * @param dir    Directory with translations
          * @param domain Localization domain
          */
        public void domain_add(string dir, string domain)
        {
            GLib.Intl.textdomain(domain);
            GLib.Intl.bindtextdomain(domain, dir);
            GLib.Intl.bind_textdomain_codeset(domain, CHARSET);
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Load profile-independent application settings from
          * configuration file on disk to memory
          */
        public void cfg_load()
        {
            // Prepare filepath of config
            string filepath = GLib.Path.build_filename(this.path_cfg, CFG_FILENAME);

            // Load config
            try
            {
                // Load config from disk to memory
                this.cfg.load_from_file(filepath, GLib.KeyFileFlags.KEEP_COMMENTS);

                // Set default values for keys which are not set
                try { this.cfg.get_boolean("general", "enable_autoload"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_boolean("general", "enable_autoload", false); };

                try { this.cfg.get_string("general", "autoload_profile_name"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_string("general", "autoload_profile_name", ""); };

                try { this.cfg.get_integer("appearance", "lang"); }
                catch (GLib.KeyFileError ex) { this.cfg.set_integer("appearance", "lang", cfg_appearance_lang_t.DEF); };
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);

                // Set default values for all keys
                this.cfg.set_boolean("general",    "enable_autoload",       false);
                this.cfg.set_string ("general",    "autoload_profile_name", "");
                this.cfg.set_integer("appearance", "lang",                  cfg_appearance_lang_t.DEF);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Save profile-independent application settings to
          * configuration file from memory on disk
          */
        public void cfg_save() throws GLib.FileError
        {
            // Prepare filepath of config
            string filepath = GLib.Path.build_filename(this.path_cfg, CFG_FILENAME);

            // Save config from memory to disk
            this.cfg.save_to_file(filepath);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize Tox.
         *
         * Creates a new Tox instance.
         *
         * @param save_data Previously saved data or null if there is no
         *                  such data. In later case new Tox ID will be
         *                  generated.
         */
        public void tox_init(uint8[]? save_data) throws ErrModelInitTox
        {
            // Startup options for Tox
            ToxCore.ERR_OPTIONS_NEW err_opts;
            var opts = new ToxCore.Options(out err_opts);
            GLib.assert(err_opts == OK);

            // Fill it
            try
            {
                opts.ipv6_enabled = false; // IPv4 socket is created, which subsequently only allows IPv4 communication
                opts.udp_enabled  = this.self.cfg.get_boolean("network", "allow_udp"); // Enable the use of UDP communication when available?
                opts.start_port   = 0;     // The default port range will be used: [33445-33545]
                opts.end_port     = 0;     // The default port range will be used: [33445-33545]
                opts.tcp_port     = 0;     // TCP server is disabled
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }

            // Fill proxy
            Profile.cfg_net_proxy_type_t proxy_type;
            string proxy_host;
            uint16 proxy_port;

            try
            {
                proxy_type = (Profile.cfg_net_proxy_type_t) this.self.cfg.get_integer("network", "proxy_type");
                proxy_host = this.self.cfg.get_string("network", "proxy_host");
                proxy_port = (uint16) this.self.cfg.get_integer("network", "proxy_port");
            }
            catch (GLib.KeyFileError ex)
            {
                proxy_type = 0;
                proxy_host = "";
                proxy_port = 0;

                GLib.assert(false);
            }

            switch (proxy_type)
            {
            case Profile.cfg_net_proxy_type_t.NONE:
                opts.proxy_type = ToxCore.ProxyType.NONE;   // Don't use a proxy
                opts.proxy_host = null;                     // Ignored
                opts.proxy_port = 0;                        // Ignored
                break;
            case Profile.cfg_net_proxy_type_t.HTTP:
                opts.proxy_type = ToxCore.ProxyType.HTTP;   // HTTP proxy using connect
                opts.proxy_host = proxy_host;               // The IP address or DNS name of the proxy to be used
                opts.proxy_port = proxy_port;               // The port to use to connect to the proxy server
                break;
            case Profile.cfg_net_proxy_type_t.SOCKS5:
                opts.proxy_type = ToxCore.ProxyType.SOCKS5; // SOCKS proxy for simple socket pipes
                opts.proxy_host = proxy_host;               // The IP address or DNS name of the proxy to be used
                opts.proxy_port = proxy_port;               // The port to use to connect to the proxy server
                break;
            default:
                opts.proxy_type = ToxCore.ProxyType.NONE;   // Don't use a proxy
                opts.proxy_host = null;                     // Ignored
                opts.proxy_port = 0;                        // Ignored
                break;
            }

            // Usage of previously saved data
            if (save_data == null)
            {
                // No save data
                opts.savedata_type = ToxCore.SaveDataType.NONE;
                opts.savedata_data = null;
            }
            else
            {
                // Use save data
                opts.savedata_type = ToxCore.SaveDataType.TOX_SAVE;
                opts.savedata_data = save_data;
            }

            // Tox instance
            ToxCore.ERR_NEW err;
            var tmp = new ToxCore.Tox(opts, out err);

            switch (err)
            {
            // The function returned successfully
            case ToxCore.ERR_NEW.OK:
                GLib.assert(tmp != null);
                break;

            // One of the arguments to the function was NULL when it was not
            // expected
            case ToxCore.ERR_NEW.NULL:
                GLib.assert(false);
                break;

            // The function was unable to allocate enough memory to store
            // the internal structures for the Tox object
            case ToxCore.ERR_NEW.MALLOC:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.MEM_ALLOC(_("Unable to allocate enough memory"));
                //break;

            // The function was unable to bind to a port. This may mean that
            // all ports have already been bound, e. g. by other Tox
            // instances, or it may mean a permission error. You may be able
            // to gather more information from errno.
            case ToxCore.ERR_NEW.PORT_ALLOC:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.PORT_ALLOC(_("Unable to bind to a port. This may mean that all ports have already been bound, " +
                                                       "e. g. by other Tox instances, or it may mean a permission error."));
                //break;

            // Proxy type was invalid
            case ToxCore.ERR_NEW.PROXY_BAD_TYPE:
                GLib.assert(false);
                break;

            // Proxy type was valid but the proxy host passed had an invalid
            // format or was NULL
            case ToxCore.ERR_NEW.PROXY_BAD_HOST:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.PROXY_BAD_HOST(_("Bad proxy host %s").printf(proxy_host));
                //break;

            // Proxy type was valid, but the proxy port was invalid
            case ToxCore.ERR_NEW.PROXY_BAD_PORT:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.PROXY_BAD_PORT(_("Bad proxy port %u").printf(proxy_port));
                //break;

            // The proxy address passed could not be resolved
            case ToxCore.ERR_NEW.PROXY_NOT_FOUND:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.PROXY_NOT_FOUND_ERR(_("The proxy address %s could not be resolved".printf(proxy_host)));
                //break;

            // The byte array to be loaded contained an encrypted save
            case ToxCore.ERR_NEW.LOAD_ENCRYPTED:
                GLib.assert(false);
                break;

            // The data format was invalid. This can happen when loading
            // data that was saved by an older version of Tox, or when the
            // data has been corrupted. When loading from badly formatted
            // data, some data may have been loaded, and the rest is
            // discarded. Passing an invalid length parameter also causes
            // this error.
            case ToxCore.ERR_NEW.LOAD_BAD_FORMAT:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.LOAD_BAD_FORMAT(_("The profile format was invalid. This can happen when loading "     +
                                                            "data that was saved by an older version of Tox, or when the "      +
                                                            "data has been corrupted. When loading from badly formatted data, " +
                                                            "some data may have been loaded, and the rest is discarded."));
                //break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }

            // No errors.
            // We can assign Tox instance now.
            // This also deletes old object if it exists.
            this.tox = (owned) tmp;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Deinitialize Tox.
         *
         * Destroys the old Tox instance.
         */
        public void tox_deinit()
        {
            // Deletes old object if it exists
            this.tox = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize Tox callbacks
         */
        public void tox_cb_init()
        {
            // Tox callbacks
            this.tox.callback_self_connection_status((tox, state, param) => { ctrl->tox_core.self_state_changed_sig(tox, state, param); });                                                        // This callback is triggered whenever there is a change in the DHT connection state

            this.tox.callback_friend_message((tox, uid, type, msg, param) => { ctrl->tox_core.buddy_rx_sig(tox, uid, type, msg, param); });                                                        // This callback is triggered when a message from a buddy is received
            this.tox.callback_friend_name((tox, uid, name, param) => { ctrl->tox_core.buddy_name_changed_sig(tox, uid, name, param); });                                                           // This callback is triggered when a buddy changes their name
            this.tox.callback_friend_connection_status((tox, uid, name, param) => { ctrl->tox_core.buddy_state_changed_sig(tox, uid, name, param); });                                             // This callback is triggered when a buddy goes offline after having been online, or when a buddy goes online
            this.tox.callback_friend_status((tox, uid, name, param) => { ctrl->tox_core.buddy_state_user_changed_sig(tox, uid, name, param); });                                                   // This callback is triggered when a buddy changes their user state
            this.tox.callback_friend_status_message((tox, uid, name, param) => { ctrl->tox_core.buddy_status_changed_sig(tox, uid, name, param); });                                               // This callback is triggered when a buddy changes their status
            this.tox.callback_friend_request((tox, key, msg, param) => { ctrl->tox_core.buddy_req_rx_sig(tox, key, msg, param); });                                                                // This callback is triggered when a buddy request is received
            this.tox.callback_friend_typing((tox, uid, flag, param) => { ctrl->tox_core.buddy_typing_sig(tox, uid, flag, param); });                                                               // This callback is triggered when a buddy starts or stops typing

            this.tox.callback_file_recv((tox, uid, file_num, kind, len, filename, param) => { ctrl->tox_core.file_req_rx_sig(tox, uid, file_num, kind, len, filename, param); });                  // This callback is triggered when a file transfer request is received
            this.tox.callback_file_recv_chunk((tox, uid, file_num, offset, buf, param) => { ctrl->tox_core.file_chunk_rx_sig(tox, uid, file_num, offset, buf, param); });                          // This callback is triggered when a file chunk is received
            this.tox.callback_file_chunk_request((tox, uid, file_num, offset, len, param) => { ctrl->tox_core.file_chunk_tx_sig(tox, uid, file_num, offset, len, param); });                       // This callback is triggered when a Core is ready to send more file data

            this.tox.callback_conference_message((tox, uid_conference, uid_member, type, msg, param) => { ctrl->tox_core.conference_rx_sig(tox, uid_conference, uid_member, type, msg, param); }); // This callback is triggered when the client receives a conference message
            this.tox.callback_conference_peer_name((tox, uid_conference, uid_member, name, param) => { ctrl->tox_core.member_name_changed_sig(tox, uid_conference, uid_member, name, param); });   // This callback is triggered when when a peer changes their name
            this.tox.callback_conference_title((tox, uid_conference, uid_member, title, param) => { ctrl->tox_core.title_changed_sig(tox, uid_conference, uid_member, title, param); });           // This callback is triggered when a peer changes the conference title.
            this.tox.callback_conference_invite((tox, uid, type, cookie, param) => { ctrl->tox_core.conference_invited_sig(tox, uid, type, cookie, param); });                                     // This callback is triggered when the client is invited to join a conference
            this.tox.callback_conference_connected((tox, uid, param) => { ctrl->tox_core.conference_connected_sig(tox, uid, param); });                                                            // This callback is triggered when the client successfully connects to a conference after joining it
            this.tox.callback_conference_peer_list_change((tox, uid, param) => { ctrl->tox_core.conference_members_changed_sig(tox, uid, param); });                                               // This callback is triggered when a peer joins or leaves the conference
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Do Tox iteration
         */
        public void tox_iterate()
        {
            //GLib.debug(_("Tox tick")); // Too many information in STDOUT, so it is commented
            this.tox.iterate(null);
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Schedule next Tox iteration
          */
        public void tox_next_iteration_schedule()
        {
            // Schedule next Tox iteration
            this.timer_tox_iteration = GLib.Timeout.add(this.tox.iteration_interval(), () =>
            {
                // Lock mutex
                this.mutex.lock();

                // Tox iteration is needed.
                //
                // We don't do iteration right here, but wake up idle thread
                // which do it for us. It is needed because all GUI methods
                // should be called from single thread. Tox callbacks may call
                // such GUI methods so they go to idle.
                view->idle_wake_up();
                this.tox_need_iteration = true;

                // Unlock mutex
                this.mutex.unlock();

                // Stop timer
                return GLib.Source.REMOVE;
            });
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize Tox A/V.
         *
         * Creates a new Tox A/V instance.
         */
        public void tox_av_init() throws ErrModelInitTox
        {
            // Tox A/V instance
            ToxAV.ERR_NEW err;
            var tmp = new ToxAV.ToxAV(this.tox, out err);

            switch (err)
            {
            // The function returned successfully
            case ToxAV.ERR_NEW.OK:
                GLib.assert(tmp != null);
                break;

            // One of the arguments to the function was NULL when it was not
            // expected
            case ToxAV.ERR_NEW.NULL:
                GLib.assert(false);
                break;

            // The function was unable to allocate enough memory to store
            // the internal structures for the Tox object
            case ToxAV.ERR_NEW.MALLOC:
                GLib.assert(tmp == null);
                throw new ErrModelInitTox.MEM_ALLOC(_("Unable to allocate enough memory"));
                //break;

            // Attempted to create a second session for the same Tox
            // instance
            case ToxAV.ERR_NEW.MULTIPLE:
                GLib.assert(false);
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }

            // No errors.
            // We can assign Tox A/V instance now.
            // This also deletes old object if it exists.
            this.tox_av = (owned) tmp;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Deinitialize Tox A/V.
         *
         * Destroys the old Tox A/V instance.
         */
        public void tox_av_deinit()
        {
            // Deletes old object if it exists
            this.tox_av = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize Tox A/V callbacks
         */
        public void tox_av_cb_init()
        {
            // Tox A/V callbacks
            this.tox_av.callback_call((tox_av, uid, audio, video) => { ctrl->tox_core_av.called_sig(tox_av, uid, audio, video); });                                                                        // This callback is triggered when a peer calls us
            this.tox_av.callback_audio_bit_rate((uid, audio_bitrate, video_bitrate) => { ctrl->tox_core_av.bitrate_suggest_sig(uid, audio_bitrate, video_bitrate); });                                     // This callback is triggered when the network becomes too saturated for current bit rates at which point core suggests new bitrates
            this.tox_av.callback_audio_receive_frame((tox_av, uid, pcm, cnt_samples, cnt_channels, bitrate) => { ctrl->tox_core_av.audio_rx_sig(tox_av, uid, pcm, cnt_samples, cnt_channels, bitrate); }); // This callback is triggered when an audio frame is coming
            this.tox_av.callback_video_receive_frame((tox_av, uid, width, height, y, u, v, ystride, ustride, vstride) => { ctrl->tox_core_av.video_rx_sig(tox_av, uid, width, height, y, u, v, ystride, ustride, vstride); }); // This callback is triggered when an video frame is coming
            this.tox_av.callback_call_state((tox_av, uid, state) => { ctrl->tox_core_av.buddy_av_state_changed_sig(tox_av, uid, state); });
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Do Tox A/V iteration
         */
        public void tox_av_iterate()
        {
            //GLib.debug(_("Tox A/V tick")); // Too many information in STDOUT, so it is commented
            this.tox_av.iterate();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Schedule next Tox iteration
          */
        public void tox_av_next_iteration_schedule()
        {
            // Schedule next Tox iteration
            this.timer_tox_av_iteration = GLib.Timeout.add(this.tox_av.iteration_interval(), () =>
            {
                // Lock mutex
                this.mutex.lock();

                // Tox A/V iteration is needed.
                //
                // We don't do iteration right here, but wake up idle thread
                // which do it for us. It is needed because all GUI methods
                // should be called from single thread. Tox callbacks may call
                // such GUI methods so they go to idle.
                view->idle_wake_up();
                this.tox_av_need_iteration = true;

                // Unlock mutex
                this.mutex.unlock();

                // Stop timer
                return GLib.Source.REMOVE;
            });
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Sort profiles.
          *
          * TODO: move sort function to Profile class.
          */
        public void profiles_sort()
        {
            this.profiles.sort((a, b) =>
                {
                    if (a.profile_name > b.profile_name)
                        return 1;
                    else if (a.profile_name == b.profile_name)
                        return 0;
                    else
                        return -1;
                });
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Initialize self.
          *
          * Copies self data from current Tox profile to Model memory.
          *
          * @param is_new_profile false: already created profile is used,
          *                       true:  new profile is to be created
          */
        public void self_init(bool is_new_profile) throws GLib.Error
        {
            // Allocate public key
            this.self.key = new uint8[Person.key_size];

            // Get self public key from Tox profile
            this.tox.self_get_public_key(self.key);

            // Get self Tox ID from Tox profile
            uint32 len_addr = ToxCore.address_size();
            var    addr     = new uint8[len_addr];

            this.tox.self_get_address(addr);
            this.self.addr = bin2hex_convert(addr);

            // Get self name from Tox profile
            size_t len_name = this.tox.self_get_name_size();
            var    name     = new uint8[len_name];

            this.tox.self_get_name(name);
            this.self.name = arr2str_convert(name);

            // Get self network state from Tox profile
            ToxCore.ConnectionStatus state = this.tox.self_get_connection_status();

            switch (state)
            {
            // There is no connection
            case ToxCore.ConnectionStatus.NONE:
                this.self.state = Person.state_t.OFFLINE;
                break;

            // Another state
            default:
                GLib.assert(false);
                break;
            }

            // Get self user state from Tox profile
            switch (this.tox.status)
            {
            // User is online and available
            case ToxCore.UserStatus.NONE:
                this.self.state_user = Person.state_user_t.AVAIL;
                break;

            // User is away
            case ToxCore.UserStatus.AWAY:
                this.self.state_user = Person.state_user_t.AWAY;
                break;

            // User is busy
            case ToxCore.UserStatus.BUSY:
                this.self.state_user = Person.state_user_t.BUSY;
                break;

            // Another state
            default:
                GLib.assert(false);
                break;
            }

            // Set default status if profile is to be created.
            // Get self last saved status otherwise.
            if (is_new_profile)
            {
                // Set self status to Tox profile
                this.self.status = _("Toxing on yat");

                ToxCore.ERR_SET_INFO err;
                this.tox.self_set_status_message(this.self.status.data, out err);
                GLib.assert(err == ToxCore.ERR_SET_INFO.OK);
            }
            else
            {
                // Get self status from Tox profile
                size_t len    = this.tox.self_get_status_message_size();
                var    status = new uint8[len];

                this.tox.self_get_status_message(status);
                this.self.status = arr2str_convert(status);
            }

            // Insert new profile into database table "profile".
            //
            // (It simply does nothing if there is already profile in
            // database. Only one row should present in this table.)
            try
            {
                this.self.db_profile_insert(null, // No avatar
                                            null, // No avatar
                                            null  // No avatar
                                           );
            }
            catch (ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Select profile from database table "profile".
            // Set avatar.
            try
            {
                this.self.db_profile_sel(out this.self.avatar_16x16,
                                         out this.self.avatar_32x32,
                                         out this.self.avatar_normal);
            }
            catch (ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            if (this.self.avatar_normal != null)
            {
                var tmp          = new uint8[this.self.avatar_normal.length];
                Posix.memcpy(tmp, this.self.avatar_normal, tmp.length);
                var stream       = new GLib.MemoryInputStream.from_data(tmp);
                this.self.avatar = new Gdk.Pixbuf.from_stream(stream);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize buddies.
         *
         * Copies buddy list from current Tox profile to Model
         * memory.
         *
         * @return The first error message appeared if any
         */
        public string? buddies_init()
        {
            string? err = null;

            // Allocate buddy uids.
            // Copy buddy uids from current Tox profile to this array.
            size_t len  = this.tox.self_get_friend_list_size();
            var    uids = new uint32[len];

            this.tox.self_get_friend_list(uids);

            // Handle each uid in array
            foreach (uint32 uid in uids)
            {
                // Add buddy
                var buddy = new Buddy(uid,
                                      null, // Tox ID is unknown
                                      null  // Alias is unknown
                                     );
                buddy_init(buddy);
                ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.Ctrl.action_t.ADD_ELEM, buddy);
            }

            // Select buddy from database table "buddies".
            // Set there are unread messages from the buddy or not.
            // Set date and time of the adding to buddy list.
            var buddies_hash = new Gee.HashMap<uint32, Buddy>();
            foreach (Buddy buddy in this.buddies_hash.values)
                buddies_hash[buddy.uid] = buddy;

            try
            {
                int64          add_lst;
                uint32         uid;
                string?        addr;
                string?        Alias;
                Buddy.unread_t have_unread_msg;
                GLib.DateTime? datetime_add_lst;
                GLib.DateTime? datetime_seen_last;
                uint8[]?       avatar_16x16;
                uint8[]?       avatar_32x32;
                uint8[]?       avatar_normal;
                int64          last_msg;
                GLib.DateTime? datetime_last_msg;

                Sqlite.Statement? stmt = null;

                while (this.self.db_buddies_sel(ref stmt,

                                                out add_lst,
                                                out uid,
                                                out addr,
                                                out Alias,
                                                out have_unread_msg,
                                                out datetime_add_lst,
                                                out datetime_seen_last,
                                                out avatar_16x16,
                                                out avatar_32x32,
                                                out avatar_normal,
                                                out last_msg,
                                                out datetime_last_msg))
                {
                    // Check whether the buddy from database exists in Tox profile
                    if (this.buddies_hash.has_key(uid))
                    {
                        // Buddy from Tox profile exists in database
                        Buddy buddy = this.buddies_hash[uid];

                        buddy.add_lst            = add_lst;
                        buddy.addr               = addr;
                        buddy.Alias              = Alias;
                        buddy.have_unread_msg    = have_unread_msg;
                        buddy.datetime_add_lst   = datetime_add_lst;
                        buddy.datetime_seen_last = datetime_seen_last;
                        buddy.avatar_16x16       = (owned) avatar_16x16;
                        buddy.avatar_32x32       = (owned) avatar_32x32;
                        buddy.avatar_normal      = (owned) avatar_normal;
                        buddy.last_msg           = last_msg;
                        buddy.datetime_last_msg  = datetime_last_msg;

                        if (buddy.avatar_normal != null)
                        {
                            try
                            {
                                var tmp      = new uint8[buddy.avatar_normal.length];
                                Posix.memcpy(tmp, buddy.avatar_normal, tmp.length);
                                var stream   = new GLib.MemoryInputStream.from_data(tmp);
                                buddy.avatar = new Gdk.Pixbuf.from_stream(stream);
                            }
                            catch (GLib.Error ex)
                            {
                                GLib.debug(ex.message);
                                if (err == null)
                                    err = ex.message;
                            }
                        }

                        buddies_hash.unset(buddy.uid);
                    }
                    else
                    {
                        // Buddy from Tox profile doesn't exist in database.
                        //
                        // Delete member from database table "members".
                        // Delete all messages to/from specified buddy from database
                        // table "conv_buddies".
                        // Delete buddy from database table "buddies".
                        try
                        {
                            this.self.db_conv_buddies_del(uid,
                                                          null, // No date & time limit, delete all messages
                                                          null  // No date & time limit, delete all messages
                                                         );
                            this.self.db_buddies_del(uid);
                        }
                        catch (ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }
                    }
                }

                // Have some buddies exist in Tox profile, but don't exist in
                // database? Add them to database.
                foreach (Buddy buddy in buddies_hash.values)
                {
                    // Insert new buddy into database table "buddies"
                    add_lst = this.self.db_buddies_insert(buddy.uid,
                                                          buddy.addr,
                                                          buddy.Alias,
                                                          buddy.have_unread_msg,
                                                          buddy.datetime_add_lst,
                                                          buddy.datetime_seen_last,
                                                          buddy.avatar_16x16,
                                                          buddy.avatar_32x32,
                                                          buddy.avatar_normal);

                    if (add_lst != -1 &&
                            add_lst != 0)
                    {
                        buddy.add_lst = add_lst;
                    }
                }
            }
            catch (ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Update buddy list according to the database selection above
            // (i. e. sort buddy list, apply Aliases if any, etc.)
            ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.Ctrl.action_t.UPD_LST);

            return err;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Initialize conferences.
          *
          * Copies conference list from current Tox profile to Model
          * memory.
          */
        public void conferences_init()
        {
            // Allocate conference uids.
            // Copy conference uids from current Tox profile to this array.
            size_t len  = this.tox.conference_get_chatlist_size();
            var    uids = new uint32[len];

            this.tox.conference_get_chatlist(uids);

            // Handle each uid in array
            foreach (uint32 uid in uids)
            {
                // Add conference
                var conference = new Conference(uid,
                                                null // Alias is unknown
                                               );
                conference_init(conference);
                ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.Ctrl.action_t.ADD_ELEM, conference);
            }

            // Select conference from database table "conferences".
            // Set there are unread messages from the conference or not.
            // Set date and time of the adding to conference list.
            var conferences_hash = new Gee.HashMap<uint32, Conference>();
            foreach (Conference conference in this.conferences_hash.values)
                conferences_hash[conference.uid] = conference;

            try
            {
                int64               add_lst;
                uint32              uid;
                string?             Alias;
                Conference.unread_t have_unread_msg;
                GLib.DateTime?      datetime_add_lst;
                int64               last_msg;
                GLib.DateTime?      datetime_last_msg;

                Sqlite.Statement? stmt = null;

                while (this.self.db_conferences_sel(ref stmt,

                                                    out add_lst,
                                                    out uid,
                                                    out Alias,
                                                    out have_unread_msg,
                                                    out datetime_add_lst,
                                                    out last_msg,
                                                    out datetime_last_msg))
                {
                    // Check whether the conference from database exists in Tox
                    // profile
                    if (this.conferences_hash.has_key(uid))
                    {
                        // Conference from Tox profile exists in database
                        Conference conference = this.conferences_hash[uid];

                        conference.add_lst           = add_lst;
                        conference.Alias             = Alias;
                        conference.have_unread_msg   = have_unread_msg;
                        conference.datetime_add_lst  = datetime_add_lst;
                        conference.last_msg          = last_msg;
                        conference.datetime_last_msg = datetime_last_msg;

                        conferences_hash.unset(conference.uid);
                    }
                    else
                    {
                        // Conference from Tox profile doesn't exist in database.
                        //
                        // Delete members of specified conference from database table
                        // "members".
                        // Delete all messages to/from specified conference from
                        // database table "conv_conferences".
                        // Delete conference from database table "conferences".
                        try
                        {
                            this.self.db_members_del(uid);
                            this.self.db_conv_conferences_del(uid,
                                                              null, // No date & time limit, delete all messages
                                                              null  // No date & time limit, delete all messages
                                                             );
                            this.self.db_conferences_del(uid);
                        }
                        catch (ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }
                    }
                }

                // Have some conferences exist in Tox profile, but don't exist
                // in database? Add them to database.
                foreach (Conference conference in conferences_hash.values)
                {
                    // Insert new conference into database table "conferences"
                    add_lst = this.self.db_conferences_insert(conference.uid,
                                                              conference.Alias,
                                                              conference.have_unread_msg,
                                                              conference.datetime_add_lst);

                    if (add_lst != -1 &&
                            add_lst != 0)
                    {
                        conference.add_lst = add_lst;
                    }
                }
            }
            catch (ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Initialize members of all conferences
            foreach (Conference conference in this.conferences_hash.values)
                members_init(conference);

            // Update conference list according to the database selection
            // above (i. e. sort conference list, apply Aliases if any,
            // etc.)
            ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.Ctrl.action_t.UPD_LST);
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Initialize members.
          *
          * Copies member list of specified conference from current Tox
          * profile to Model memory.
          *
          * @param conference Conference
          */
        public void members_init(Conference conference)
        {
            // Get the number of online members
            int    online = conference.online;
            uint32 cnt    = online == -1 ? 0 : online;

            // Handle all members of this conference
            size_t  len = Person.key_size;
            uint8[] key = new uint8[len];

            for (uint32 uid = 0; uid < cnt; uid++)
            {
                // Find out if the member is ourself
                Person? person = null;

                ToxCore.ERR_CONFERENCE_PEER_QUERY err;
                bool res = this.tox.conference_peer_number_is_ours(conference.uid,
                                                                   uid,
                                                                   out err);
                GLib.assert(err == ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);

                if (res)
                {
                    // The member is ourself
                    person = this.self;
                }
                else
                {
                    // The member is not ourself.
                    //
                    // Find out if he is buddy.
                    // Compare the public keys to know it.
                    res = this.tox.conference_peer_get_public_key(conference.uid,
                                                                  uid,
                                                                  key,
                                                                  out err);
                    GLib.assert(res);
                    GLib.assert(err == ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);

                    foreach (Buddy buddy in this.buddies_hash.values)
                    {
                        GLib.assert(buddy.key != null);

                        if (Posix.memcmp(buddy.key,
                                         key,
                                         buddy.key.length) == 0)
                        {
                            // The member is our buddy
                            person = buddy;
                            break;
                        }
                    }
                }

                // Add member to the member list
                var member = new Member(uid,
                                        person,
                                        conference,
                                        conference.member_fg++,
                                        conference.member_bg++);
                member_init(member,
                            true // Set added to member list number
                           );
                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.Ctrl.action_t.ADD_ELEM, member);
            }

            // Select member from database table "members".
            // Set foreground and background colours.
            var members_hash = new Gee.HashMap<uint32, Member>();
            foreach (Member member in conference.members_hash.values)
                members_hash[member.uid] = member;

            try
            {
                uint fg;
                uint bg;

                Sqlite.Statement? stmt = null;
                uint8[]?          key_ = null;

                while (this.self.db_members_sel(ref stmt,

                                                ref key_,
                                                out fg,
                                                out bg,
                                                    conference.uid))
                {
                    // Check whether the member from database exists in Tox profile
                    foreach (Member member in conference.members_hash.values)
                    {
                        // Compare public keys
                        uint8[]? key__ = member.key_get();
                        GLib.assert(key__ != null);

                        if (Posix.memcmp(key__,
                                         key_,
                                         key__.length) == 0)
                        {
                            // Member from Tox profile exists in database
                            member.fg = fg;
                            member.bg = bg;

                            members_hash.unset(member.uid);
                            break;
                        }
                    }

                    if (conference.member_fg <= fg)
                        conference.member_fg = fg + 1;
                    if (conference.member_bg <= bg)
                        conference.member_bg = bg + 1;

                    key_ = null;
                }

                // Have some members exist in Tox profile, but don't exist in
                // database? Add them to database.
                foreach (Member member in members_hash.values)
                {
                    // Insert new member into database table "members"
                    this.self.db_members_insert(member.key,
                                                member.fg,
                                                member.bg,
                                                member.conference.uid);
                }
            }
            catch (ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Recolourize names in member list with new foreground and
            // background colours settings taken from a database
            ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.Ctrl.action_t.UPD_LST);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize threads and their resources
         *
         * @param thread ORed threads you wish to start
         */
        public void threads_init(thread_t thread)

            requires ( (thread & ~thread_t.ALL) == 0 &&

                      ((thread & thread_t.BOOTSTRAP) > 0 ||
                       (thread & thread_t.GLIB)      > 0 ||
                       (thread & thread_t.UI)        > 0))
        {
            // Cannot run without threads
            //GLib.assert(GLib.Thread.supported()); // It produces warning by GCC, so I disabled this assert

            // Initialize bootstrap thread
            if ((thread & thread_t.BOOTSTRAP) > 0)
            {
                GLib.assert(this.thread_bootstrap == null);
                this.thread_bootstrap = new Threads.ThreadBootstrap("thread_bootstrap"); // Thread performing boostrapping
            }

            // Initialize GLib event loop thread
            if ((thread & thread_t.GLIB) > 0)
            {
                GLib.assert(this.thread_glib == null);
                this.thread_glib = new Threads.ThreadGLib("thread_glib");                // Thread rotating the UI event loop
            }

            // Initialize UI event loop thread
            if ((thread & thread_t.UI) > 0)
            {
                GLib.assert(this.thread_ui == null);
                this.thread_ui = new Threads.ThreadUI("thread_ui");                      // Thread rotating the GLib event loop
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Start threads
         *
         * @param thread ORed threads you wish to start
         */
        public void threads_start(thread_t thread)

            requires ( (thread & ~thread_t.ALL) == 0 &&

                      ((thread & thread_t.BOOTSTRAP) > 0 ||
                       (thread & thread_t.GLIB)      > 0 ||
                       (thread & thread_t.UI)        > 0))
        {
            // Cannot run without threads
            //GLib.assert(GLib.Thread.supported()); // It produces warning by GCC, so I disabled this assert

            // Start bootstrap thread
            if ((thread & thread_t.BOOTSTRAP) > 0)
            {
                GLib.assert(this.thread_bootstrap != null);
                this.thread_bootstrap.start();
                this.threads[this.thread_bootstrap.thread] = this.thread_bootstrap;
            }

            // Start GLib event loop thread
            if ((thread & thread_t.GLIB) > 0)
            {
                GLib.assert(this.thread_glib != null);
                this.thread_glib.start();
                this.threads[this.thread_glib.thread] = this.thread_glib;
            }

            // Start UI event loop thread
            if ((thread & thread_t.UI) > 0)
            {
                GLib.assert(this.thread_ui != null);
                this.thread_ui.start();
                this.threads[this.thread_ui.thread] = this.thread_ui;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Stop threads
         *
         * @param thread ORed threads you wish to start
         */
        public void threads_stop(thread_t thread)

            requires ( (thread & ~thread_t.ALL) == 0 &&

                      ((thread & thread_t.BOOTSTRAP) > 0 ||
                       (thread & thread_t.GLIB)      > 0 ||
                       (thread & thread_t.UI)        > 0))
        {
            // Cannot run without threads
            //GLib.assert(GLib.Thread.supported()); // It produces warning by GCC, so I disabled this assert

            // Stop bootstrap thread
            if ((thread & thread_t.BOOTSTRAP) > 0)
            {
                GLib.assert(this.thread_bootstrap != null);
                GLib.assert(this.thread_bootstrap.is_started());
                this.thread_bootstrap.stop();
            }

            // Stop GLib event loop thread
            if ((thread & thread_t.GLIB) > 0)
            {
                GLib.assert(this.thread_glib != null);
                GLib.assert(this.thread_glib.is_started());
                this.thread_glib.stop();
            }

            // Stop UI event loop thread
            if ((thread & thread_t.UI) > 0)
            {
                GLib.assert(this.thread_ui != null);
                GLib.assert(this.thread_ui.is_started());
                this.thread_ui.stop();
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Wait until threads finish
         *
         * @param thread ORed threads you wish to start
         */
        public void threads_wait(thread_t thread)

            requires ( (thread & ~thread_t.ALL) == 0 &&

                      ((thread & thread_t.BOOTSTRAP) > 0 ||
                       (thread & thread_t.GLIB)      > 0 ||
                       (thread & thread_t.UI)        > 0))
        {
            // Cannot run without threads
            //GLib.assert(GLib.Thread.supported()); // It produces warning by GCC, so I disabled this assert

            // Wait until UI event loop thread finish
            if ((thread & thread_t.UI) > 0 &&
                this.thread_ui != null)
            {
                GLib.assert(this.thread_ui.is_started());
                this.thread_ui.wait();
            }

            // Wait until bootstrap thread finish
            if ((thread & thread_t.BOOTSTRAP) > 0 &&
                this.thread_bootstrap != null)
            {
                GLib.assert(this.thread_bootstrap.is_started());
                this.thread_bootstrap.wait();
            }

            // Wait until GLib event loop thread finish
            if ((thread & thread_t.GLIB) > 0 &&
                this.thread_glib != null)
            {
                GLib.assert(this.thread_glib.is_started());
                this.thread_glib.wait();
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Send buddy request.
         * Add buddy.
         *
         * @param addr Tox ID
         * @param msg  The message that will be sent along with the buddy request
         *
         * @return Buddy unique identifier
         */
        public uint32 buddy_add(uint8[] addr, uint8[] msg) throws ErrModelAddBuddy
        {
            // Add a buddy to Tox profile
            ToxCore.ERR_FRIEND_ADD err;
            uint32 uid = this.tox.friend_add(addr,
                                             msg,
                                             out err);

            switch (err)
            {
            // The function returned successfully
            case ToxCore.ERR_FRIEND_ADD.OK:
                break;

            // One of the arguments to the function was NULL
            case ToxCore.ERR_FRIEND_ADD.NULL:
                GLib.assert(false);
                break;

            // The length of the buddy request message exceeded
            // ToxCore.TOX_MAX_FRIEND_REQUEST_LENGTH
            case ToxCore.ERR_FRIEND_ADD.TOO_LONG:
                GLib.assert(false);
                break;

            // The buddy request message was empty
            case ToxCore.ERR_FRIEND_ADD.NO_MESSAGE:
                GLib.assert(false);
                break;

            // The buddy Tox ID belongs to the sending client
            case ToxCore.ERR_FRIEND_ADD.OWN_KEY:
                throw new ErrModelAddBuddy.ADDR_OWN_KEY(_("The buddy Tox ID belongs to the sending client"));
                //break;

            // A buddy request has already been sent, or the address
            // belongs to a buddy that is already on the buddy list
            case ToxCore.ERR_FRIEND_ADD.ALREADY_SENT:
                throw new ErrModelAddBuddy.SENT_ALREADY(_("This Tox ID belongs to a buddy that is already on the buddy list"));
                //break;

            // The buddy address checksum failed
            case ToxCore.ERR_FRIEND_ADD.BAD_CHECKSUM:
                throw new ErrModelAddBuddy.BAD_CHECKSUM(_("The buddy Tox ID checksum failed"));
                //break;

            // The buddy was already there, but the nospam value was
            // different
            case ToxCore.ERR_FRIEND_ADD.SET_NEW_NOSPAM:
                throw new ErrModelAddBuddy.SET_NEW_NOSPAM(_("The buddy was already there, but the nospam value was different"));
                //break;

            // A memory allocation failed when trying to increase the buddy
            // list size
            case ToxCore.ERR_FRIEND_ADD.MALLOC:
                throw new ErrModelAddBuddy.MEM_ALLOC(_("Unable to allocate enough memory"));
                //break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }

            return uid;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Lock mutex used to protect shared data between threads.
         *
         * It is impossible to access the mutex directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void mutex_lock()
        {
            // Lock mutex
            this.mutex.lock();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Unlock mutex used to protect shared data between threads.
         *
         * It is impossible to access the mutex directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void mutex_unlock()
        {
            // Unlock mutex
            this.mutex.unlock();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Add buddy without sending a buddy request
         *
         * @param key Public key
         *
         * @return Buddy unique identifier
         */
        public uint32 buddy_no_req_add(uint8[] key) throws ErrModelAddBuddy
        {
            // Add a buddy to Tox profile without sending a buddy request
            ToxCore.ERR_FRIEND_ADD err;
            uint32 uid = this.tox.friend_add_norequest(key, out err);

            switch (err)
            {
            // The function returned successfully
            case ToxCore.ERR_FRIEND_ADD.OK:
                break;

            // One of the arguments to the function was NULL
            case ToxCore.ERR_FRIEND_ADD.NULL:
                GLib.assert(false);
                break;

            // The length of the buddy request message exceeded
            // ToxCore.TOX_MAX_FRIEND_REQUEST_LENGTH
            case ToxCore.ERR_FRIEND_ADD.TOO_LONG:
                GLib.assert(false);
                break;

            // The buddy request message was empty
            case ToxCore.ERR_FRIEND_ADD.NO_MESSAGE:
                GLib.assert(false);
                break;

            // The buddy Tox ID belongs to the sending client
            case ToxCore.ERR_FRIEND_ADD.OWN_KEY:
                GLib.assert(false);
                break;

            // A buddy request has already been sent, or the address
            // belongs to a buddy that is already on the buddy list
            case ToxCore.ERR_FRIEND_ADD.ALREADY_SENT:
                throw new ErrModelAddBuddy.SENT_ALREADY(_("This Tox ID belongs to a buddy that is already on the buddy list"));
                //break;

            // The buddy address checksum failed
            case ToxCore.ERR_FRIEND_ADD.BAD_CHECKSUM:
                throw new ErrModelAddBuddy.BAD_CHECKSUM(_("The buddy Tox ID checksum failed"));
                //break;

            // The buddy was already there, but the nospam value was
            // different
            case ToxCore.ERR_FRIEND_ADD.SET_NEW_NOSPAM:
                throw new ErrModelAddBuddy.SET_NEW_NOSPAM(_("The buddy was already there, but the nospam value was different"));
                //break;

            // A memory allocation failed when trying to increase the buddy
            // list size
            case ToxCore.ERR_FRIEND_ADD.MALLOC:
                throw new ErrModelAddBuddy.MEM_ALLOC(_("Unable to allocate enough memory"));
                //break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }

            return uid;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize buddy.
         *
         * Copies buddy data from current Tox profile to Model memory.
         *
         * @param buddy Buddy
         */
        public void buddy_init(Buddy buddy)
        {
            // Allocate public key
            buddy.key = new uint8[Person.key_size];

            // Get public key from Tox profile
            ToxCore.ERR_FRIEND_GET_PUBLIC_KEY err;
            bool res = this.tox.friend_get_public_key(buddy.uid, buddy.key, out err);
            GLib.assert(res);
            GLib.assert(err == ToxCore.ERR_FRIEND_GET_PUBLIC_KEY.OK);

            // Allocate name
            ToxCore.ERR_FRIEND_QUERY err_ = ToxCore.ERR_FRIEND_QUERY.FRIEND_NOT_FOUND;    // Should initialize this variable with error to avoid compiler warning about possibly unassigned local variable
            var name = new uint8[this.tox.friend_get_name_size(buddy.uid, out err_) + 1]; // Add 1 byte for storing zero-terminating character
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);

            // Get buddy name from Tox profile
            res = this.tox.friend_get_name(buddy.uid,
                                           name,
                                           out err_);
            GLib.assert(res);
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);
            name[name.length - 1] = '\0';
            buddy.name = (string) name;

            // Get buddy network state from Tox profile
            ToxCore.ConnectionStatus state;
            state = this.tox.friend_get_connection_status(buddy.uid, out err_);
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);

            switch (state)
            {
            // There is no connection
            case ToxCore.ConnectionStatus.NONE:
                buddy.state = Person.state_t.OFFLINE;
                Buddy.none--;
                Buddy.offline++;
                break;

            // Another state
            default:
                GLib.assert(false);
                break;
            }

            // Get buddy user state from Tox profile
            ToxCore.UserStatus state_user;
            state_user = this.tox.friend_get_status(buddy.uid, out err_);
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);

            switch (state_user)
            {
            // User is online and available
            case ToxCore.UserStatus.NONE:
                buddy.state_user = Person.state_user_t.AVAIL;
                break;

            // User is away
            case ToxCore.UserStatus.AWAY:
                buddy.state_user = Person.state_user_t.AWAY;
                break;

            // User is busy
            case ToxCore.UserStatus.BUSY:
                buddy.state_user = Person.state_user_t.BUSY;
                break;

            // Another state
            default:
                buddy.state_user = Person.state_user_t.NONE;
                GLib.assert(false);
                break;
            }

            // Allocate status
            var status = new uint8[this.tox.friend_get_status_message_size(buddy.uid, out err_) + 1]; // Add 1 byte for storing zero-terminating character
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);

            // Get buddy status from Tox profile
            res = this.tox.friend_get_status_message(buddy.uid,
                                                     status,
                                                     out err_);
            GLib.assert(res);
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);
            status[status.length - 1] = '\0';
            buddy.status = (string) status;

            // Set self typing to Tox profile.
            // Set that we are not typing a message.
            buddy.typing_change(this,
                                false // Stop typing
                               );

            // Get buddy typing from Tox profile
            res = this.tox.friend_get_typing(buddy.uid, out err_);
            GLib.assert(err_ == ToxCore.ERR_FRIEND_QUERY.OK);

            if (res)
                buddy.action_to_us |= Buddy.state_t.TYPING_KNOWN | Buddy.state_t.TYPING_TXT;
            else
            {
                buddy.action_to_us |=  Buddy.state_t.TYPING_KNOWN;
                buddy.action_to_us &= ~Buddy.state_t.TYPING_TXT;
            }

            // Set there are no unread messages from the buddy
            buddy.have_unread_msg = Buddy.unread_t.FALSE;

            // Set added to buddy list number
            buddy.add_lst = (int64) Buddy.last_add_lst++;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize conference.
         *
         * Copies conference data from current Tox profile to Model memory.
         *
         * @param conference Conference
         */
        public void conference_init(Conference conference)
        {
            // Title
            ToxCore.ERR_CONFERENCE_TITLE err = ToxCore.ERR_CONFERENCE_TITLE.CONFERENCE_NOT_FOUND; // Should initialize this variable with error to avoid compiler warning about possibly unassigned local variable
            size_t len = this.tox.conference_get_title_size(conference.uid, out err);

            if (err == ToxCore.ERR_CONFERENCE_TITLE.INVALID_LENGTH)
            {
                // Title is empty
                conference.title = "";
            }
            else
            {
                // Allocate title
                GLib.assert(err == ToxCore.ERR_CONFERENCE_TITLE.OK);
                var title = new uint8[len + 1]; // Add 1 byte for storing zero-terminating character

                // Get conference title from Tox profile
                bool res = this.tox.conference_get_title(conference.uid,
                                                         title,
                                                         out err);
                GLib.assert(res);
                GLib.assert(err == ToxCore.ERR_CONFERENCE_TITLE.OK);
                title[title.length - 1] = '\0';
                conference.title = (string) title;
            }

            // Set there are no unread messages from the conference
            conference.have_unread_msg = Conference.unread_t.FALSE;

            // Recalculate the number of online members.
            // Recalculate the number of offline members.
            conference.recalc(this, Conference.recalc_kind_t.OFFLINE);
            conference.recalc(this, Conference.recalc_kind_t.ONLINE);

            // Set added to conference list number
            conference.add_lst = (int64) Conference.last_add_lst++;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize member.
         *
         * Copies member data from current Tox profile to Model memory.
         *
         * @param member      Member
         * @param set_add_lst false: don't set added to member list
         *                           number,
         *                    true:  set added to member list number
         */
        public void member_init(Member member, bool set_add_lst = false)
        {
            // Fill name and public key of member only if he is not ourself
            // and he is not our buddy
            if (member.person == null)
            {
                // Allocate name
                ToxCore.ERR_CONFERENCE_PEER_QUERY err;

                size_t len  = this.tox.conference_peer_get_name_size(member.conference.uid, member.uid, out err);
                var    name = new uint8[len];

                GLib.assert(err == ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);

                // Get name from Tox profile
                bool res = this.tox.conference_peer_get_name(member.conference.uid,
                                                             member.uid,
                                                             name,
                                                             out err);
                GLib.assert(res);
                GLib.assert(err == ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);

                member.name = arr2str_convert(name);

                // Allocate public key
                len        = Person.key_size;
                member.key = new uint8[len];

                // Get public key from Tox profile
                res = this.tox.conference_peer_get_public_key(member.conference.uid,
                                                              member.uid,
                                                              member.key,
                                                              out err);
                GLib.assert(res);
                GLib.assert(err == ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);
            }

            // Set added to member list number
            if (set_add_lst)
                member.add_lst = (int64) member.conference.member_add_lst++;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Image rescale
         *
         * @param src        Source image
         * @param width      Desired width
         * @param height     Desired height
         * @param exact_size false New image can be smaller,
         *                   true  New image should has exactly required
         *                         width and height
         *
         * @return Destination image
         */
        public Gdk.Pixbuf image_rescale(Gdk.Pixbuf src,
                                        size_t     width,
                                        size_t     height,
                                        bool       exact_size)
        {
            Gdk.Pixbuf dst;
            Gdk.Pixbuf tmp;

            // Rescale image
            if (src.width > width || src.height > height)
            {
                size_t width_tmp;
                size_t height_tmp;

                if (src.width >= src.height)
                {
                    width_tmp  = width;
                    height_tmp = (ulong) src.height * width / src.width;
                }
                else
                {
                    width_tmp  = (ulong) src.width * height / src.height;
                    height_tmp = height;
                }

                tmp = src.scale_simple((int) width_tmp, (int) height_tmp, Gdk.InterpType.BILINEAR);
            }
            else
                tmp = src;

            // New image should has exactly required width and height?
            // Or it can be smaller?
            if (exact_size)
            {
                if (tmp.width == width && tmp.height == height)
                    dst = tmp;
                else
                {
                    dst = new Gdk.Pixbuf(Gdk.Colorspace.RGB,
                                         true,        // Have alpha channel
                                         8,           // Bits per sample
                                         (int) width, // Width
                                         (int) height // Height
                                        );
                    dst.fill((uint32) 0xFFFFFF00UL);       // Fill white
                    dst.add_alpha(true, 0xFF, 0xFF, 0xFF); // Replace white by alpha channel

                    tmp.copy_area(0, // Source x
                                  0, // Source y
                                  tmp.width,
                                  tmp.height,
                                  dst,
                                  (int) (width  - tmp.width)  / 2, // Destination x
                                  (int) (height - tmp.height) / 2  // Destination y
                                 );
                }
            }
            else
                dst = tmp;

            return dst;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get filepath to program icon
         *
         * @return Filepath to icon
         */
        public string icon_filepath_program_get()
        {
            unowned string filename = "yat_200x200.png";
                    string filepath = GLib.Path.build_filename("img", filename);

            return filepath;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Convert byte array to string
         *
         * @param arr Byte array
         *
         * @return String
         */
        public static string arr2str_convert(uint8[] arr)
        {
            string str = "";
            foreach (uint8 byte in arr)
                str += byte.to_string("%c");

            return str;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Convert hexadecimal string to binary array
         *
         * @param hex Hexadecimal string
         */
        public static uint8[] hex2bin_convert(string hex) throws ErrModelHex
        {
            if (hex.length == 0 ||
                    hex.length % 2 > 0)
                throw new ErrModelHex.HEX_WRONG_LEN(_("The length of a hexadecimal string is wrong: %s: %u characters"), hex, hex.length);
            var bin = new uint8[hex.length / 2];

            for (size_t byte = 0; byte < bin.length; byte++)
            {
                int tmp;
                int res = hex.substring(2 * (long) byte, 2).scanf("%02X", out tmp);
                if (res != 1)
                    throw new ErrModelHex.HEX_INVALID(_("Bad hexadecimal string: %s"), hex);
                bin[byte] = (uint8) tmp;
            }

            return bin;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Convert binary array to hexadecimal string
         *
         * @param bin Binary array
         *
         * @return Hexadecimal string
         */
        public static string bin2hex_convert(uint8[] bin)
        {
            string hex = "";
            foreach (uint8 byte in bin)
                hex += byte.to_string("%02X");

            return hex;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Make first character of string lowercase
         *
         * @param str String
         *
         * @return The string starting lowercase character
         */
        public static string first_ch_lowercase_make(string str)
        {
            string res;

            if (str.char_count() > 0)
            {
                int idx = str.index_of_nth_char(1);
                res = str.substring(0, idx).down() +
                      str.substring(idx, -1);
            }
            else
                res = "";

            return res;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Escape special characters to use string as markup
         *
         * @param str String
         *
         * @return Escaped string
         */
        public static string markup_escape(string str)
        {
            string escaped_str;

            try
            {
                GLib.RegexEvalCallback replace_fn = (info, res) =>
                {
                    string match = info.fetch(0);

                    switch (match)
                    {
                    case "_":  res.append("__");
                               break;
                    case "&":  res.append("&amp;");
                               break;
                    case "'":  res.append("&apos;");
                               break;
                    case "\"": res.append("&quot;");
                               break;
                    case "<":  res.append("&lt;");
                               break;
                    case ">":  res.append("&gt;");
                               break;

                    default:
                        GLib.assert(false);
                        break;
                    }

                    return false;
                };

                GLib.Regex pattern = new GLib.Regex("_|&|'|\"|<|>");
                escaped_str = pattern.replace_eval(str,
                                                   -1, // String is NULL-terminated
                                                   0,  // Start from zero position
                                                   0,  // No match options
                                                   replace_fn
                                                  );
            }
            catch (GLib.RegexError ex)
            {
                escaped_str = "";
                GLib.assert(false);
            }

            return escaped_str;
        }
    }
}

