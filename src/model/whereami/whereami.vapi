/*
 *    whereami.vapi
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

[CCode (cheader_filename = "whereami.h")]
namespace WhereAmI
{
    [CCode (cname = "wai_getExecutablePath", has_target = false)]
    public int exec_path_get([CCode (array_length_type="int")] char[]? path, out int len_dir);

    [CCode (cname = "wai_getModulePath", has_target = false)]
    public int module_path_get([CCode (array_length_type="int")] char[]? path, out int len_dir);
}

