/*
** This routines are called to read/write data directly into the database image
** (presumably taken from a database file created externally).
**
** The database from which the supplied data is taken must have a page-size
** equal to the value of the SQLITE_DEFAULT_PAGE_SIZE macro this file is
** compiled with.
*/
int sqlite3_raw_read(
  sqlite3 *db, 
  int *nData, 
  int iOffset, 
  unsigned char **zData
){
  Pager *pPager;
  int pageSize;              /* Copy of pPager->pageSize */
  int n;                     /* Remaining bytes to copy from database pages */
  unsigned const char *z;    /* Bytes to copy from database pages */
  int iPage;                 /* Page number to copy from */
  int iOff;                  /* Offset on iPage to read from */
  int rc = SQLITE_OK;        /* Offset on iPage to read from */
  unsigned char *p1 = 0;     /* First page of database */

  assert( db );
  assert( nData );
  assert( iOffset>=0 );
  assert( db->aDb[0].pBt );
  assert( zData );

  pPager = sqlite3BtreePager(db->aDb[0].pBt);
  pageSize = pPager->pageSize;

  rc = sqlite3PagerGet(pPager, 1, (void **)&p1, 0);
  sqlite3PagerPagecount(pPager, &n);
  n *= pageSize;
  *nData = n;
  *zData = malloc(n);
  z = *zData;

  iPage = (iOffset / pageSize) + 1;
  iOff = (iOffset % pageSize);
  while( n>0 && rc==SQLITE_OK ){
    int bytes;                       /* Number of bytes to write */
    unsigned char *p = 0;            /* Page iPage */
    bytes = pageSize-iOff;
    bytes = (bytes<n)?bytes:n;

    /* Retrieve the page, copy the data from it
    */
    if( SQLITE_OK==(rc=sqlite3PagerGet(pPager, iPage, (void **)&p, 0)) ){
      memcpy(z, &((unsigned char *) sqlite3PagerGetData(p))[iOff], bytes);
    }
    if( p ){
      sqlite3PagerUnref(p);
    }

    n -= bytes;
    z += bytes;
    iOff = 0;
    iPage++;
  }
  if( p1 ){
    sqlite3PagerUnref(p1);
  }
  sqlite3Error(db, rc);
  return sqlite3ApiExit(db, rc);
}

int sqlite3_raw_write(
  sqlite3 *db, 
  int nData, 
  int iOffset, 
  unsigned const char *zData
){
  Pager *pPager;
  int pageSize;              /* Copy of pPager->pageSize */
  int n;                     /* Remaining bytes to copy into database pages */
  unsigned const char *z;    /* Bytes to copy into database pages */
  int iPage;                 /* Page number to copy into */
  int iOff;                  /* Offset on iPage to write to */
  int rc = SQLITE_OK;        /* Offset on iPage to write to */
  unsigned char *p1 = 0;     /* First page of database */

  assert( db );
  assert( nData>=0 );
  assert( iOffset>=0 );
  assert( db->aDb[0].pBt );
  assert( zData || nData==0 );

  pPager = sqlite3BtreePager(db->aDb[0].pBt);
  pageSize = pPager->pageSize;
  z = zData;
  n = nData;

  rc = sqlite3PagerGet(pPager, 1, (void **)&p1, 0);

  iPage = (iOffset / pageSize) + 1;
  iOff = (iOffset % pageSize);
  while( n>0 && rc==SQLITE_OK ){
    int bytes;                       /* Number of bytes to write */
    unsigned char *p = 0;            /* Page iPage */
    bytes = pageSize-iOff;
    bytes = (bytes<n)?bytes:n;

    /* Retrieve the page, set it to writable and copy the data to it. The
    ** first sqlite3PagerWrite() call starts a new transaction, which is
    ** committed at the end of this routine.
    */
    if( SQLITE_OK==(rc=sqlite3PagerGet(pPager, iPage, (void **)&p, 0)) &&
        SQLITE_OK==(rc=sqlite3PagerWrite(p))
    ){
      memcpy(&((unsigned char *) sqlite3PagerGetData(p))[iOff], z, bytes);
    }
    if( p ){
      sqlite3PagerUnref(p);
    }

    n -= bytes;
    z += bytes;
    iOff = 0;
    iPage++;
  }
  if( rc==SQLITE_OK ){
    rc = sqlite3PagerCommitPhaseOne(pPager, 0, 0);
    if( SQLITE_OK==rc ){
      rc = sqlite3PagerCommitPhaseTwo(pPager);
    }
  }
  if( p1 ){
    sqlite3PagerUnref(p1);
  }
  sqlite3Error(db, rc);
  return sqlite3ApiExit(db, rc);
}

