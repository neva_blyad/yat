/*
 *    SSE.vapi
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

[CCode (lower_case_cprefix = "sqlite3_", cheader_filename = "sqlite3.h")]
namespace Sqlite
{
    namespace SSE
    {
        [CCode (cname = "sqlite3_raw_read", has_target = false)]
        public int raw_read (Sqlite.Database db, int offset, [CCode (array_length_pos = 1.0, array_length_type = "int")] out uint8[] data);

        [CCode (cname = "sqlite3_raw_write", has_target = false)]
        public int raw_write(Sqlite.Database db, int offset, [CCode (array_length_pos = 1.0, array_length_type = "int")] uint8[] data);
    }
}

