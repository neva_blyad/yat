/*
 *    dlg_edit_conference.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Edit Conference" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgEditConference : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Title input text control changed signal
         */
        public signal void *txt_title_changed_sig(void *fn, void *param, void *event);

        /**
         * Alias input text control changed signal
         */
        public signal void *txt_Alias_changed_sig(void *fn, void *param, void *event);

        /**
         * "Edit" button click signal
         */
        public signal void *btn_edit_conference_clicked_sig(void *fn, void *param, void *event);

        /**
         * List box with buddies select signal
         */
        public signal void *lst_box_buddies_selected_sig(void *fn, void *param, void *event);

        /**
         * Right button click signal
         */
        public signal void *btn_right_clicked_sig(void *fn, void *param, void *event);

        /**
         * Left button click signal
         */
        public signal void *btn_left_clicked_sig(void *fn, void *param, void *event);

        /**
         * List box with invited buddies select signal
         */
        public signal void *lst_box_buddies_invites_selected_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Enable/disable "Edit" button
        private void btn_edit_conference_en_set()
        {
            Model.Conference conference = ctrl->handled_contacts.dlg_edit_conference.conference;

            string title   = view->dlg_edit_conference.txt_title.val_get();
            string Alias   = view->dlg_edit_conference.txt_Alias.val_get();
            size_t invites = view->dlg_edit_conference.lst_box_buddies_invites.cnt_get();

            view->dlg_edit_conference.btn_edit_conference.en((( conference.title == null                 ||
                                                                conference.title != title                ||
                                                               (conference.Alias == null && Alias != "") ||
                                                               (conference.Alias != null && Alias != conference.Alias))
                                                              &&
                                                              1 <= title.length <= Model.Conference.title_max_len) || invites > 0);
        }

        /*----------------------------------------------------------------------------*/

        // Title input text control changed callback
        private void *txt_title_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Enable/disable "Edit" button.
            // Unlock mutex.
            model->mutex.lock();
            btn_edit_conference_en_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Alias input text control changed callback
        private void *txt_Alias_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Enable/disable "Edit" button.
            // Unlock mutex.
            model->mutex.lock();
            btn_edit_conference_en_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Edit" button click callback
        private void *btn_edit_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get entered data from text input controls
            string title = view->dlg_edit_conference.txt_title.val_get();
            string Alias = view->dlg_edit_conference.txt_Alias.val_get();

            // Update an Alias
            Model.Conference conference = ctrl->handled_contacts.dlg_edit_conference.conference;

            conference.Alias = (Alias == "") ? null :
                                               Alias;

            // Set title
            if (conference.title == null ||
                conference.title != title)
            {
                // Set new conference title
                conference.title = title;

                // Set new conference title to Tox profile
                global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE err;
                model->tox.conference_set_title(conference.uid,
                                                conference.title.data,
                                                out err);
                GLib.assert(err == global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.OK);
            }

            // For each invited buddy...
            size_t cnt = view->dlg_edit_conference.lst_box_buddies_invites.cnt_get();
            string? dbg_gui = null;

            for (uint idx = 0; idx < cnt; idx++)
            {
                // Find him and send invite
                string display_name = view->dlg_edit_conference.lst_box_buddies_invites.val_get(idx);
                string dbg_console  = _("Invitating buddy: %s to %s").printf(display_name,
                                                                             conference.display_name_get());
                try
                {
                    // Find the buddy in buddy list
                    Model.Buddy buddy = null;

                    foreach (Model.Buddy tmp in model->buddies_hash.values)
                        if (tmp.display_name_get() == display_name)
                        {
                            buddy = tmp;
                            break;
                        }

                    if (buddy == null)
                        throw new ErrGeneral.BUDDY_NOT_FOUND_ERR(_("Buddy not found: %s").printf(display_name));

                    // Invite the buddy to the conference
                    conference.invite(model, buddy);

                    GLib.debug("%s: OK", dbg_console);
                }
                catch (ErrGeneral ex)
                {
                    GLib.debug(ex.message);
                }
                catch (Model.ErrConferenceInviteBuddy ex)
                {
                    GLib.debug("%s: %s", dbg_console, Model.Model.first_ch_lowercase_make(ex.message));
                    if (dbg_gui == null)
                        dbg_gui = dbg_console;
                }
            }

            // Update conference list
            ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                                       conference,

                                                       true,  // Set display name column
                                                       false, // Don't set count column
                                                       false  // Don't set unread state column
                                                      );

            // Close "Edit Conference" dialog
            view->dlg_edit_conference.show(false);

            // Unlock mutex
            model->mutex.unlock();

            // Show a buddy invitation error if it occurs
            if (dbg_gui != null)
            {
                var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_edit_conference, dbg_gui, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List box with buddies select callback
        private void *lst_box_buddies_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable right button
            size_t cnt      = view->dlg_edit_conference.lst_box_buddies.cnt_get();
            bool   have_sel = false;

            for (uint idx = 0; idx < cnt; idx++)
            {
                if (view->dlg_edit_conference.lst_box_buddies.sel_get(idx))
                {
                    have_sel = true;
                    break;
                }
            }

            view->dlg_edit_conference.btn_right.en(have_sel);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Right button click callback
        private void *btn_right_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get selections from the right list box
            size_t cnt     = view->dlg_edit_conference.lst_box_buddies.cnt_get();
            var    sel     = new uint[cnt];
            size_t cnt_sel = view->dlg_edit_conference.lst_box_buddies.selections_elem_get(sel);

            GLib.assert(0 < cnt_sel <= cnt);

            // Edit selected buddies to the right list box
            for (uint idx = 0; idx < cnt_sel; idx++)
            {
                string display_name = view->dlg_edit_conference.lst_box_buddies.val_get(sel[idx]);
                view->dlg_edit_conference.lst_box_buddies_invites.append(display_name);
            }

            // Remove selected buddies from the left list box
            ctrl->ctrl_changed_by_user = false;
            for (int idx = (int) cnt_sel - 1; idx >= 0; idx--)
                view->dlg_edit_conference.lst_box_buddies.remove(sel[idx]);
            ctrl->ctrl_changed_by_user = true;

            // Disable right button if needed
            cnt_sel = view->dlg_edit_conference.lst_box_buddies.selections_elem_get(sel);
            if (cnt_sel == 0)
                view->dlg_edit_conference.btn_right.en(false);

            // Enable/disable "Edit" button
            btn_edit_conference_en_set();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Left button click callback
        private void *btn_left_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get selections from the left list box
            size_t cnt     = view->dlg_edit_conference.lst_box_buddies_invites.cnt_get();
            var    sel     = new uint[cnt];
            size_t cnt_sel = view->dlg_edit_conference.lst_box_buddies_invites.selections_elem_get(sel);

            GLib.assert(0 < cnt_sel <= cnt);

            // Edit selected buddies to the left list box
            for (uint idx = 0; idx < cnt_sel; idx++)
            {
                string display_name = view->dlg_edit_conference.lst_box_buddies_invites.val_get(sel[idx]);
                view->dlg_edit_conference.lst_box_buddies.append(display_name);
            }

            // Remove selected buddies from the right list box
            ctrl->ctrl_changed_by_user = false;
            for (int idx = (int) cnt_sel - 1; idx >= 0; idx--)
                view->dlg_edit_conference.lst_box_buddies_invites.remove(sel[idx]);
            ctrl->ctrl_changed_by_user = true;

            // Disable left button if needed
            cnt_sel = view->dlg_edit_conference.lst_box_buddies_invites.selections_elem_get(sel);
            if (cnt_sel == 0)
                view->dlg_edit_conference.btn_left.en(false);

            // Enable/disable "Edit" button
            btn_edit_conference_en_set();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List box with invited buddies select callback
        private void *lst_box_buddies_invites_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable left button
            size_t cnt      = view->dlg_edit_conference.lst_box_buddies_invites.cnt_get();
            bool   have_sel = false;

            for (uint idx = 0; idx < cnt; idx++)
            {
                if (view->dlg_edit_conference.lst_box_buddies_invites.sel_get(idx))
                {
                    have_sel = true;
                    break;
                }
            }

            view->dlg_edit_conference.btn_left.en(have_sel);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Edit Conference" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgEditConference()
        {
            // Connect signals to callback methods
            this.txt_title_changed_sig.connect(txt_title_changed);
            this.txt_Alias_changed_sig.connect(txt_Alias_changed);
            this.btn_edit_conference_clicked_sig.connect(btn_edit_conference_clicked);
            this.lst_box_buddies_selected_sig.connect(lst_box_buddies_selected);
            this.btn_right_clicked_sig.connect(btn_right_clicked);
            this.btn_left_clicked_sig.connect(btn_left_clicked);
            this.lst_box_buddies_invites_selected_sig.connect(lst_box_buddies_invites_selected);
        }
    }
}

