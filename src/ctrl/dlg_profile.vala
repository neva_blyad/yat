/*
 *    dlg_profile.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors
     */
    public errordomain ErrDlgProfileGeneral
    {
        /**
         * Cannot rename profile, name already exists
         */
        ALREADY_EXISTS_PROFILE_NAME,

        /**
         * Bad file format
         */
        BAD_FILE_FMT,
    }

    /**
     * This is the "Choose Profile" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgProfile : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * "Choose Profile" dialog close signal
         */
        public signal void *closed_sig(void *fn, void *param, void *event);

        /**
         * Profile name input text control changed signal
         */
        public signal void *txt_name_tab1_changed_sig(void *fn, void *param, void *event);

        /**
         * Password input text control changed signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *txt_pwd_tab1_changed_sig(void *fn, void *param, void *event);

        /**
         * Password confirmation input text control changed signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *txt_confirm_tab1_changed_sig(void *fn, void *param, void *event);

        /**
         * "Use password" check box clicked signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *check_pwd_tab1_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Don't save on disk" check box clicked signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *check_mem_only_tab1_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Create" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_create_tab1_clicked_sig(void *fn, void *param, void *event);

        /**
         * Choice of profile name select signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *choice_name_tab2_selected_sig(void *fn, void *param, void *event);

        /**
         * "Action" button click signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *btn_action_tab2_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Load" button click signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *btn_load_tab2_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Load automatically" check box click signal
         */
        public signal void *check_autoload_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Rename..." menu item click signal.
         *
         * It is placed in context menu for "Action" button.
         */
        public signal void *menu_rename_profile_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Delete" menu item click signal.
         *
         * It is placed in context menu for "Action" button.
         */
        public signal void *menu_del_profile_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Import" menu item click signal.
         *
         * It is placed in context menu for "Action" button.
         */
        public signal void *menu_import_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Export..." -> "Export for yat..." menu item click signal.
         *
         * It is placed in context menu for "Action" button.
         */
        public signal void *menu_export_yat_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Export..." -> "Export for another Tox client..." menu item
         * click signal.
         *
         * It is placed in context menu for "Action" button.
         */
        public signal void *menu_export_another_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Enable/disable "Create" button
        private void btn_create_tab1_en()
        {
            // Get values of tab 1 controls
            ulong  len      = view->dlg_profile.txt_name_tab1.len_get();
            string pwd      = view->dlg_profile.txt_pwd_tab1.val_get();
            string pwd2     = view->dlg_profile.txt_confirm_tab1.val_get();
            bool   use_pwd  = view->dlg_profile.check_pwd_tab1.val_get();
            bool   mem_only = view->dlg_profile.check_mem_only_tab1.val_get();

            // Enable the button if profile name is set and passwords are
            // the same
            view->dlg_profile.btn_create_tab1.en((!use_pwd && !mem_only && len > 0)                ||
                                                 (!use_pwd &&  mem_only)                           ||
                                                 ( use_pwd && !mem_only && len > 0 && pwd == pwd2) ||
                                                 ( use_pwd &&  mem_only));
        }

        /*----------------------------------------------------------------------------*/

        // "Choose Profile" dialog close callback
        private void *closed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Properly shutdown saving all data
            ctrl->exit();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Profile name input text control changed callback
        private void *txt_name_tab1_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Enable/disable "Create" button
            btn_create_tab1_en();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Password input text control changed callback.
        //
        // It is placed on the tab 1 panel.
        private void *txt_pwd_tab1_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Calculate password strength
            string pwd = view->dlg_profile.txt_pwd_tab1.val_get();
            uint score_percent = Model.Profile.pwd_strength_calc(pwd);

            // Update progress controls with that, label and gauge
            view->dlg_profile.pwd_strength_tab1_set(score_percent);

            // Enable/disable "Create" button
            btn_create_tab1_en();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Password confirmation input text control changed callback.
        //
        // It is placed on the tab 1 panel.
        private void *txt_confirm_tab1_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Enable/disable "Create" button
            btn_create_tab1_en();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Use password" check box clicked callback.
        //
        // It is placed on the tab 1 panel.
        private void *check_pwd_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get value of the check box
            bool use_pwd = view->dlg_profile.check_pwd_tab1.val_get();

            // Show/hide tab 1 password controls
            view->dlg_profile.lbl_pwd_tab1.show(use_pwd);
            view->dlg_profile.txt_pwd_tab1.show(use_pwd);
            view->dlg_profile.lbl_confirm_tab1.show(use_pwd);
            view->dlg_profile.txt_confirm_tab1.show(use_pwd);
            view->dlg_profile.pwd_strength_tab1_show(use_pwd);

            // Continue for checked state
            if (use_pwd)
            {
                // Clear tab 1 password controls
                view->dlg_profile.txt_pwd_tab1.clr();
                view->dlg_profile.txt_confirm_tab1.clr();
                view->dlg_profile.pwd_strength_tab1_set(0);
            }
            else
            {
                // Enable "Create" button possibly
                btn_create_tab1_en();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Don't save on disk" check box clicked callback.
        //
        // It is placed on the tab 1 panel.
        private void *check_mem_only_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get value of the check box
            bool mem_only     = view->dlg_profile.check_mem_only_tab1.val_get();
            bool save_on_disk = !mem_only;

            // Show/hide tab 1 controls
            view->dlg_profile.lbl_name_tab1.show(save_on_disk);
            view->dlg_profile.txt_name_tab1.show(save_on_disk);
            view->dlg_profile.lbl_pwd_tab1.show(save_on_disk);
            view->dlg_profile.txt_pwd_tab1.show(save_on_disk);
            view->dlg_profile.lbl_confirm_tab1.show(save_on_disk);
            view->dlg_profile.txt_confirm_tab1.show(save_on_disk);
            view->dlg_profile.pwd_strength_tab1_show(save_on_disk);
            view->dlg_profile.check_pwd_tab1.show(save_on_disk);

            // Continue for checked state
            if (save_on_disk)
            {
                // Clear tab 1 password controls
                view->dlg_profile.txt_name_tab1.clr();
                view->dlg_profile.txt_pwd_tab1.clr();
                view->dlg_profile.txt_confirm_tab1.clr();
                view->dlg_profile.pwd_strength_tab1_set(0);
                view->dlg_profile.check_pwd_tab1.val_set(true);
            }
            else
            {
                // Enable "Create" button possibly
                btn_create_tab1_en();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Create" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_create_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get values of tab 1 controls
            string profile_name = view->dlg_profile.txt_name_tab1.val_get();
            string pwd          = view->dlg_profile.txt_pwd_tab1.val_get();
            bool   use_pwd      = view->dlg_profile.check_pwd_tab1.val_get();
            bool   mem_only     = view->dlg_profile.check_mem_only_tab1.val_get();

            // Check whether profile with this profile name already exists
            if (!mem_only)
            {
                foreach (Model.Profile profile in model->profiles)
                {
                    // Compare entered profile name with the next one of existing
                    if (profile.profile_name == profile_name)
                    {
                        // Ask for confirmation
                        var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main,
                                                              _("Profile with name %s already exists. Are you sure you want to overwrite it?").printf(profile_name),
                                                              _("New Profile"),
                                                              View.Wrapper.MsgDlg.style_t.CENTR | View.Wrapper.MsgDlg.style_t.OK | View.Wrapper.MsgDlg.style_t.CANCEL);
                        View.Wrapper.Dlg.answer_t answer = msg_dlg.modal_show();

                        switch (answer)
                        {
                        case View.Wrapper.Dlg.answer_t.OK:
                            break;
                        case View.Wrapper.Dlg.answer_t.CANCEL:
                            return null;
                            //break;
                        default:
                            GLib.assert(false);
                            break;
                        }

                        break;
                    }
                }
            }

            // Create profile.
            // Initialize Tox.
            var profile = new Model.Profile(mem_only             ? null  : profile_name,
                                            mem_only             ? false : use_pwd,
                                            mem_only || !use_pwd ? null  : pwd
                                           );

            try
            {
                // Initialize profile
                ctrl->profile_init(profile,
                                   true // Creating new profile
                                  );

                // Add new profile
                model->profiles.add(profile);
            }
            catch (GLib.Error ex)
            {
                // Nothing to do
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Choice of profile name select callback.
        //
        // It is placed on the tab 2 panel.
        private void *choice_name_tab2_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Determine whether selected profile is encrypted
            uint idx     = view->dlg_profile.choice_name_tab2.sel_get();
            bool use_pwd = model->profiles[(int) idx].use_pwd;

            // Update tab 2 password controls
            view->dlg_profile.lbl_pwd_tab2.show(use_pwd);
            view->dlg_profile.txt_pwd_tab2.show(use_pwd);
            if (use_pwd)
                view->dlg_profile.txt_pwd_tab2.clr();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Action" button click callback.
        //
        // It is placed on the tab 2 panel.
        private void *btn_action_tab2_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Pops up the context menu
            view->dlg_profile.btn_action_tab2.menu_popup(view->dlg_profile.menu_action);

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Load" button click callback.
        //
        // It is placed on the tab 2 panel.
        private void *btn_load_tab2_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get tab 2 input controls
            uint   idx = view->dlg_profile.choice_name_tab2.sel_get();
            string pwd = view->dlg_profile.txt_pwd_tab2.val_get();

            // Load existing profile.
            // Initialize Tox.
            Model.Profile profile = model->profiles[(int) idx];

            try
            {
                // Remember a password
                if (profile.use_pwd)
                    profile.pwd = pwd;

                // Initialize profile
                ctrl->profile_init(profile,
                                   false // Loading existing profile
                                  );
            }
            catch (GLib.Error ex)
            {
                // Clear password
                profile.pwd = null;
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Load automatically" check box click callback
        private void *check_autoload_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get the check box value.
            // Update config with it.
            bool flag = view->dlg_profile.check_autoload.val_get();
            model->cfg.set_boolean("general", "enable_autoload", flag);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Rename..." menu item click callback.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_rename_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get old profile name
            uint idx = view->dlg_profile.choice_name_tab2.sel_get();
            Model.Profile profile = model->profiles[(int) idx];

            // Ask user for new profile name using input dialog
            string profile_name = View.Wrapper.InputDlg.txt_inp(view->dlg_profile,
                                                                _("Enter new profile name:"),
                                                                _("Enter profile name"),
                                                                profile.profile_name);
            if (profile_name == "" ||
                    profile_name == profile.profile_name)
                return null;

            // Rename it
            try
            {
                // Check this profile name is available
                foreach (Model.Profile profile_ in model->profiles)
                    if (profile_ != profile &&
                            profile_.profile_name == profile_name)
                        throw new ErrDlgProfileGeneral.ALREADY_EXISTS_PROFILE_NAME(_("Profile with name %s already exists"), profile_name);

                // Rename configuration file, database, profile on disk
                profile.cfg_rename(model, profile_name);
                profile.db_rename (model, profile_name);
                profile.rename    (model, profile_name);

                // Sort profiles
                model->profiles_sort();

                // Rename in GUI
                view->dlg_profile.choice_name_tab2.clr();

                for (uint idx_ = 0; idx_ < model->profiles.size; idx_++)
                {
                    Model.Profile profile_ = model->profiles[(int) idx_];
                    view->dlg_profile.choice_name_tab2.append(profile_.profile_name);
                    if (profile_.profile_name == profile.profile_name)
                        idx = idx_; // Found new index of renamed profile in choice control
                }

                view->dlg_profile.choice_name_tab2.sel_set(idx);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Delete" menu item click callback.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_del_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get selected profile name
            uint idx              = view->dlg_profile.choice_name_tab2.sel_get();
            Model.Profile profile = model->profiles[(int) idx];

            // Ask for confirmation
            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main,
                                                  _("Are you sure you want to delete profile %s?\n" +
                                                        "Your Tox ID, buddy list, conversations, settings are to be deleted permanently.\n" +
                                                        "This action cannot be undone.").printf(profile.profile_name),
                                                  _("Delete Profile"),
                                                  View.Wrapper.MsgDlg.style_t.CENTR | View.Wrapper.MsgDlg.style_t.OK | View.Wrapper.MsgDlg.style_t.CANCEL);
            View.Wrapper.Dlg.answer_t answer = msg_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Delete configuration file
            try
            {
                // Delete configuration file from disk
                profile.cfg_del(model);
            }
            catch (Model.ErrProfileGeneral ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_ = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                msg_dlg_.modal_show();
            }

            // Delete database
            try
            {
                // Delete database from disk
                profile.db_del(model);
            }
            catch (Model.ErrProfileGeneral ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_ = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                msg_dlg_.modal_show();
            }

            // Delete profile
            try
            {
                // Delete profile from disk
                profile.del(model);

                // Delete from memory
                model->profiles.remove_at((int) idx);

                // Delete from GUI
                view->dlg_profile.choice_name_tab2.remove(idx);
                size_t cnt = view->dlg_profile.choice_name_tab2.cnt_get();

                if (cnt > 0)
                {
                    // Select new profile
                    if (idx == cnt)
                        idx--;
                    view->dlg_profile.choice_name_tab2.sel_set(idx);

                    // Determine whether selected profile is encrypted
                    bool use_pwd = model->profiles[(int) idx].use_pwd;

                    // Update tab 2 password controls
                    view->dlg_profile.lbl_pwd_tab2.show(use_pwd);
                    view->dlg_profile.txt_pwd_tab2.show(use_pwd);
                    view->dlg_profile.txt_pwd_tab2.clr();
                }
                else
                {
                    // No profiles exist.
                    // Update tab 2 controls.
                    view->dlg_profile.choice_name_tab2.en(false);
                    view->dlg_profile.menu_rename_profile.en(false);
                    view->dlg_profile.menu_del_profile.en(false);
                    view->dlg_profile.menu_export_yat.en(false);
                    view->dlg_profile.menu_export_another.en(false);
                    view->dlg_profile.btn_load_tab2.en(false);
                    view->dlg_profile.lbl_pwd_tab2.show(false);
                    view->dlg_profile.txt_pwd_tab2.show(false);
                    view->dlg_profile.txt_pwd_tab2.clr();
                }
            }
            catch (Model.ErrProfileGeneral ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_ = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                msg_dlg_.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Import" menu item click callback.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_import_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Ask user for a Tox profile filepath
            var file_dlg = new View.Wrapper.FileDlg(view->frm_main,
                                                    _("Import an Tox profile"),
                                                    GLib.Environment.get_home_dir(),
                                                    "",
                                                    _("All files (*)|*|GNU archives with Tox profiles (*.tgz)|*.tgz|Tox profiles (*.tox)|*.tox"),
                                                    View.Wrapper.FileDlg.style_t.OPEN_FILE | View.Wrapper.FileDlg.style_t.FILE_EXIST_MUST | View.Wrapper.FileDlg.style_t.PREVIEW);
            View.Wrapper.Dlg.answer_t answer = file_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Copy it to data directory and unarchive if needed
            try
            {
                // Determine profile name and file format from filepath,
                //
                // e. g. ~/Downloads/user.tox => profile name is user,
                //                               file format is tox
                string         filepath = file_dlg.path_get();
                string         filename = GLib.Path.get_basename(filepath);
                var            regex    = new GLib.Regex("^(.+)\\.(.+)\\z");
                GLib.MatchInfo info;
                bool           res      = regex.match(filename, 0, out info);

                if (!res)
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tox or .tgz extension."));

                string? profile_name = info.fetch(1);
                string? ext          = info.fetch(2);

                GLib.assert(profile_name != null);
                GLib.assert(ext          != null);

                // Check this profile name is available
                foreach (Model.Profile profile in model->profiles)
                    if (profile.profile_name == profile_name)
                        throw new ErrDlgProfileGeneral.ALREADY_EXISTS_PROFILE_NAME(_("Profile with name %s already exists"), profile_name);

                // Initialize profile
                var profile = new Model.Profile(profile_name);

                // Selected by user file format should be *.tox or *.tgz
                switch (ext)
                {
                // *.tox is general multi-client Tox format.
                // It is supported by yat, uTox, qTox and other clients.
                case "tox":

                    // Add profile.
                    // Sort profiles.
                    model->profiles.add(profile);
                    model->profiles_sort();

                    // Copy profile from specific source on disk to data directory
                    profile.copy_from(model, filepath);

                    break;

                // *.tgz archives are yat-specific
                case "tgz":

                    // Add profile.
                    // Sort profiles.
                    model->profiles.add(profile);
                    model->profiles_sort();

                    // Unarchive from disk to profile directory
                    profile.unarchive(model, filepath);

                    break;

                // Another format
                default:

                    // Throw error
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tox or .tgz extension."));
                    //break;
                }

                // Set that profile uses password (and is encrypted) or doesn't
                // use password (and is not encrypted)
                profile.use_pwd = profile.is_encrypted(model);

                // Add imported profile in GUI
                int idx = -1;
                view->dlg_profile.choice_name_tab2.clr();

                for (uint idx_ = 0; idx_ < model->profiles.size; idx_++)
                {
                    Model.Profile profile_ = model->profiles[(int) idx_];
                    view->dlg_profile.choice_name_tab2.append(profile_.profile_name);
                    if (profile_.profile_name == profile.profile_name)
                        idx = (int) idx_; // Found new index of renamed profile in choice control
                }

                GLib.assert(idx != -1);
                view->dlg_profile.choice_name_tab2.sel_set(idx);

                // Update tab 2 password controls
                view->dlg_profile.choice_name_tab2.en(true);
                view->dlg_profile.menu_rename_profile.en(true);
                view->dlg_profile.menu_del_profile.en(true);
                view->dlg_profile.menu_export_yat.en(true);
                view->dlg_profile.menu_export_another.en(true);
                view->dlg_profile.btn_load_tab2.en(true);
                view->dlg_profile.lbl_pwd_tab2.show(profile.use_pwd);
                view->dlg_profile.txt_pwd_tab2.show(profile.use_pwd);
                if (profile.use_pwd)
                    view->dlg_profile.txt_pwd_tab2.clr();
            }
            catch (RegexError ex)
            {
                GLib.assert(false);
            }
            catch (ErrDlgProfileGeneral ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();
            }
            catch (GLib.FileError ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();

                model->profiles.remove_at(model->profiles.size - 1);
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Export..." -> "Export for yat..." menu item.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_export_yat_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get selected profile name
            uint idx = view->dlg_profile.choice_name_tab2.sel_get();
            Model.Profile profile = model->profiles[(int) idx];

            // Ask user for a Tox profile filepath
            var file_dlg = new View.Wrapper.FileDlg(view->frm_main,
                                                    _("Export the Tox profile"),
                                                    GLib.Environment.get_home_dir(),
                                                    profile.profile_name + ".tgz",
                                                    _("All files (*)|*|GNU archives with Tox profiles (*.tgz)|*.tgz"),
                                                    View.Wrapper.FileDlg.style_t.SAVE_FILE | View.Wrapper.FileDlg.style_t.OVERWRITE_PROMPT);
            View.Wrapper.Dlg.answer_t answer = file_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Archive it
            try
            {
                // Determine profile name and file format from filepath,
                //
                // e. g. ~/Downloads/user.tox => profile name is user,
                //                               file format is tox
                string         filepath = file_dlg.path_get();
                string         filename = GLib.Path.get_basename(filepath);
                var            regex    = new GLib.Regex("^(.+)\\.(.+)\\z");
                GLib.MatchInfo info;
                bool           res      = regex.match(filename, 0, out info);

                if (!res)
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tgz extension."));

                string? profile_name = info.fetch(1);
                string? ext          = info.fetch(2);

                GLib.assert(profile_name != null);
                GLib.assert(ext          != null);

                if (ext != "tgz")
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tgz extension."));

                // Arhive profile directory on disk
                profile.archive(model, profile_name, filepath);
            }
            catch (RegexError ex)
            {
                GLib.assert(false);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Export..." -> "Export for another Tox client..." menu item.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_export_another_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Get selected profile name
            uint idx = view->dlg_profile.choice_name_tab2.sel_get();
            Model.Profile profile = model->profiles[(int) idx];

            // Ask user for a Tox profile filepath
            var file_dlg = new View.Wrapper.FileDlg(view->frm_main,
                                                    _("Export the Tox profile"),
                                                    GLib.Environment.get_home_dir(),
                                                    profile.profile_name + ".tox",
                                                    _("All files (*)|*|Tox profiles (*.tox)|*.tox"),
                                                    View.Wrapper.FileDlg.style_t.SAVE_FILE | View.Wrapper.FileDlg.style_t.OVERWRITE_PROMPT);
            View.Wrapper.Dlg.answer_t answer = file_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Copy it
            try
            {
                // Determine profile name and file format from filepath,
                //
                // e. g. ~/Downloads/user.tox => profile name is user,
                //                               file format is tox
                string         filepath = file_dlg.path_get();
                string         filename = GLib.Path.get_basename(filepath);
                var            regex    = new GLib.Regex("^(.+)\\.(.+)\\z");
                GLib.MatchInfo info;
                bool           res      = regex.match(filename, 0, out info);

                if (!res)
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tox extension."));

                string? profile_name = info.fetch(1);
                string? ext          = info.fetch(2);

                GLib.assert(profile_name != null);
                GLib.assert(ext          != null);

                if (ext != "tox")
                    throw new ErrDlgProfileGeneral.BAD_FILE_FMT(_("Bad file format. Choose file with .tox extension."));

                // Copy profile from data directory to specific destination on
                // disk
                profile.copy_to(model, filepath);
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                var msg_dlg_err = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg_err.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Choose Profile" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgProfile()
        {
            // Connect signals to callback methods
            this.closed_sig.connect(closed);
            this.txt_name_tab1_changed_sig.connect(txt_name_tab1_changed);
            this.txt_pwd_tab1_changed_sig.connect(txt_pwd_tab1_changed);
            this.txt_confirm_tab1_changed_sig.connect(txt_confirm_tab1_changed);
            this.check_pwd_tab1_clicked_sig.connect(check_pwd_tab1_clicked);
            this.check_mem_only_tab1_clicked_sig.connect(check_mem_only_tab1_clicked);
            this.btn_create_tab1_clicked_sig.connect(btn_create_tab1_clicked);
            this.choice_name_tab2_selected_sig.connect(choice_name_tab2_selected);
            this.btn_action_tab2_clicked_sig.connect(btn_action_tab2_clicked);
            this.btn_load_tab2_clicked_sig.connect(btn_load_tab2_clicked);
            this.check_autoload_clicked_sig.connect(check_autoload_clicked);
            this.menu_rename_profile_clicked_sig.connect(menu_rename_profile_clicked);
            this.menu_del_profile_clicked_sig.connect(menu_del_profile_clicked);
            this.menu_import_clicked_sig.connect(menu_import_clicked);
            this.menu_export_yat_clicked_sig.connect(menu_export_yat_clicked);
            this.menu_export_another_clicked_sig.connect(menu_export_another_clicked);
        }
    }
}

