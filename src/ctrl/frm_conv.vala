/*
 *    frm_conv.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Conversation History" frame class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class FrmConv : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * "Conversation History" frame catches focus signal
         */
        public signal void *focus_set_sig(void *fn, void *param, void *event);

        /**
         * "Conversation History" frame close signal
         */
        public signal void *closed_sig(void *fn, void *param, void *event);

        /**
         * Tree control with conversation date & times select signal
         */
        public signal void *tree_datetime_selected_sig(void *fn, void *param, void *event);

        /**
         * Conversation search input text control button cancel signal
         */
        public signal void *srch_conv_btn_cancel_sig(void *fn, void *param, void *event);

        /**
         * Conversation search input text control button search signal
         */
        public signal void *srch_conv_btn_srch_sig(void *fn, void *param, void *event);

        /**
         * Conversation search input text control button changed signal
         */
        public signal void *srch_conv_changed_sig(void *fn, void *param, void *event);

        /**
         * Find previous button click signal
         */
        public signal void *btn_find_prev_clicked_sig(void *fn, void *param, void *event);

        /**
         * Find next button click signal
         */
        public signal void *btn_find_next_clicked_sig(void *fn, void *param, void *event);

        /**
         *"Delete" button click signal
         */
        public signal void *btn_del_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Delete All" button click signal
         */
        public signal void *btn_del_all_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Close" button click signal
         */
        public signal void *btn_close_conv_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Conversation history date & time.
        // Each element of this hash maps represents one item in tree
        // control of "Conversation History" frame.
        private Gee.HashMap<string, Datetime> datetime;

        // Array list with found positions of substring in conversation
        // history
        private ulong find_cur;
        private Gee.ArrayList<FindPos> find_pos;

        /*----------------------------------------------------------------------------*/
        /*                               Private Types                                */
        /*----------------------------------------------------------------------------*/

        // Conversation history date & time.
        // Each object of this class represents one item in tree control
        // of "Conversation History" frame.
        private class Datetime
        {
            public GLib.DateTime from;
            public GLib.DateTime to;
        }

        // Found position of substring in conversation history
        private class FindPos
        {
            public ulong from;
            public ulong to;
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // "Conversation History" frame catches focus signal
        private void *focus_set(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Set our own helper tip string to search widget when field is
            // empty.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_conv.srch_conv_val_def_upd();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conversation History" frame close callback
        private void *closed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Close "Conversation History" frame.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_conv.show(false);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Clear and refill date & time tree control.
         * Also enables/disables "Delete" button.
         *
         * @param find_str String to find or null
         */
        public void tree_datetime_fill(string? find_str) throws Model.ErrProfileDB
        {
            // Select next message from database table "conv_buddies"
            // written by/for peer with specific uid,
            //
            // OR
            //
            // select next date and time from database table
            // "conv_conferences" at which there have been written messages
            // by/for peer with specific uid
            Sqlite.Statement? stmt     = null;
            GLib.DateTime?    datetime = null;

            size_t cnt = 0;

            int year = -1;
            int mon  = -1;

            View.Wrapper.TreeCtrl.Item item = null;

            view->frm_conv.tree_datetime.clr();
            this.datetime.clear();
            view->frm_conv.btn_del.en(false);

            while ((ctrl->handled_contacts.frm_conv.conference == null && model->self.db_conv_buddies_datetime_sel(ref stmt,
                                                                                                                   ctrl->handled_contacts.frm_conv.buddy.uid,
                                                                                                                   find_str,
                                                                                                                   out datetime))
                   ||
                   (ctrl->handled_contacts.frm_conv.buddy      == null && model->self.db_conv_conferences_datetime_sel(ref stmt,
                                                                                                                       ctrl->handled_contacts.frm_conv.conference.uid,
                                                                                                                       find_str,
                                                                                                                       out datetime)))
            {
                var tz = new GLib.TimeZone.local();
                int year_tmp = datetime.get_year();
                int mon_tmp  = datetime.get_month();

                GLib.assert((year == -1 && mon == -1) ||
                            (year_tmp <= year && (mon_tmp <= mon || year_tmp < year)));

                // New root item (month and year)
                if ((year == -1 && mon == -1) ||
                    (year_tmp < year || mon_tmp < mon))
                {
                    year = year_tmp;
                    mon  = mon_tmp;

                    string   val       = datetime.format("%OB %Y");
                    Datetime datetime_ = new Datetime();

                    datetime_.from = new GLib.DateTime(tz,
                                                       year,
                                                       mon,
                                                       1, // Day
                                                       0, // Hour
                                                       0, // Minute
                                                       0  // Seconds
                                                      );
                    datetime_.to = datetime_.from.add_months(1);
                    datetime_.to = datetime_.to.add_seconds(-1);

                    item = view->frm_conv.tree_datetime.append(null, // Append to root
                                                               val,
                                                               null  // No meta tag
                                                              );
                    view->frm_conv.tree_datetime.expand(item);
                    this.datetime[val] = datetime_;
                }

                // New regular item (full date & time)
                string   val       = datetime.format("%A, %e %B %Y %H:%M:%S");
                int      day       = datetime.get_day_of_month();
                Datetime datetime_ = new Datetime();

                datetime_.from = new GLib.DateTime(tz,
                                                   year,
                                                   mon,
                                                   day,
                                                   datetime.get_hour(),
                                                   datetime.get_minute(),
                                                   datetime.get_second());
                datetime_.to   = new GLib.DateTime(tz,
                                                   year,
                                                   mon,
                                                   day,
                                                   23, // Hour
                                                   59, // Minute
                                                   59  // Seconds
                                                  );
                view->frm_conv.tree_datetime.append(item,
                                                    val,
                                                    null // No meta tag
                                                   );
                view->frm_conv.tree_datetime.expand(item);

                if (cnt == 0)
                {
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_conv.tree_datetime.sel(item);
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_conv.btn_del.en(true);
                }

                this.datetime[val] = datetime_;

                cnt++;
            }
        }

        /*----------------------------------------------------------------------------*/

        // Tree control with conversation date & times select callback
        private void *tree_datetime_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like value
            // removing.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get string to find
            string find_str = view->frm_conv.srch_conv.val_get();
            unowned string? find_str_non_empty = find_str.char_count() > 0 ? find_str : null;

            // Clear and refill conversation text input.
            // Enable "Delete" button.
            txt_conv_fill(find_str_non_empty);
            view->frm_conv.btn_del.en(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Clear and refill conversation text input.
         *
         * Also enables/disables find previous/next button.
         *
         * @param find_str String to find or null
         */
        public void txt_conv_fill(string? find_str)
        {
            // Clear text control with exchanged messages
            view->frm_conv.txt_conv.clr();

            // Some information based on currently selected item in date &
            // time tree control
            View.Wrapper.TreeCtrl.Item? item = view->frm_conv.tree_datetime.sel_get();
            if (item == null)
                return;
            string val = view->frm_conv.tree_datetime.val_get(item);
            Datetime datetime = this.datetime[val];

            // Select last messages from database and append them to text
            // control
            try
            {
                string? name;
                string? msg;
                GLib.DateTime? datetime_msg;

                Sqlite.Statement? stmt = null;

                if (ctrl->handled_contacts.frm_conv.conference == null)
                {
                    // Select next message from database table "conv_buddies"
                    // written by/for peer with specific uid
                    bool am_i;

                    while (model->self.db_conv_buddies_sel(ref stmt,

                                                           out name,
                                                           ctrl->handled_contacts.frm_conv.buddy.uid,
                                                           out am_i,
                                                           out msg,
                                                           out datetime_msg,

                                                           datetime.from,
                                                           datetime.to,

                                                           -1 // No limit should be used
                                                          ))
                    {
                        // Append next message to text control with last exchanged
                        // messages
                        view->frm_conv.txt_conv_buddies_append(name,
                                                               am_i,
                                                               msg,
                                                               datetime_msg);
                    }
                }
                // else if (ctrl->handled_contacts.frm_conv.buddy == null)
                else
                {
                    // Select next message from database table "conv_conferences"
                    // written by/for peer with specific uid
                    uint fg;
                    uint bg;

                    while (model->self.db_conv_conferences_sel(ref stmt,

                                                               out name,
                                                               ctrl->handled_contacts.frm_conv.conference.uid,
                                                               out fg,
                                                               out bg,
                                                               out msg,
                                                               out datetime_msg,

                                                               datetime.from,
                                                               datetime.to,

                                                               -1 // No limit should be used
                                                              ))
                    {
                        // Append next message to text control with last exchanged
                        // messages
                        view->frm_conv.txt_conv_conferences_append(name,
                                                                   fg,
                                                                   bg,
                                                                   msg,
                                                                   datetime_msg);
                    }
                }
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Find substring if it is required
            if (find_str == null)
            {
                // Disable find previous button.
                // Disable find next button.
                view->frm_conv.btn_find_prev.en(false);
                view->frm_conv.btn_find_next.en(false);

                // Scroll to end of text control with last exchanged messages
                ulong len = view->frm_conv.txt_conv.len_get();
                view->frm_conv.txt_conv.pos_show(len);
            }
            else
            {
                GLib.Regex? pattern;

                // Prepare pattern
                try
                {
                    string escaped_find_str;

                    escaped_find_str = GLib.Regex.escape_string(find_str);
                    pattern          = new GLib.Regex(escaped_find_str);
                }
                catch (GLib.RegexError ex)
                {
                    pattern = null;
                    GLib.assert(false);
                }

                // Find all the messages using pattern
                string conv = view->frm_conv.txt_conv.val_get();
                int cnt = conv.char_count();
                GLib.MatchInfo info;
                bool res = pattern.match(conv, 0, out info);
                GLib.assert(res);

                // Get the first found occurence.
                // Convert byte offsets to character offsets.
                int from, to;
                res = info.fetch_pos(0, out from, out to);
                unowned string conv_from = conv.offset((long) from);
                unowned string conv_to   = conv.offset((long) to);
                from = cnt - conv_from.char_count();
                to   = cnt - conv_to.char_count();
                GLib.assert(res);

                // Scroll to it
                view->frm_conv.txt_conv.sel_set((long) from, (long) to);
                view->frm_conv.txt_conv.pos_show((ulong) from);

                // Remember found offsets.
                // They would be needed by find previous/next button callbacks.
                this.find_pos.clear();
                var find_pos = new FindPos();
                find_pos.from = from;
                find_pos.to   = to;
                this.find_pos.add(find_pos);
                this.find_cur = 0;

                // Find next occurences if any
                bool have_more_than_one_occurences = false;

                try
                {
                    // Find next occurence
                    while (info.next())
                    {
                        // Get the next occurence.
                        // Convert byte offsets to character offsets.
                        res = info.fetch_pos(0, out from, out to);
                        GLib.assert(res);
                        conv_from = conv.offset((long) from);
                        conv_to   = conv.offset((long) to);
                        from = cnt - conv_from.char_count();
                        to   = cnt - conv_to.char_count();

                        // Remember found offsets.
                        // They would be needed by find previous/next button callbacks.
                        find_pos = new FindPos();
                        find_pos.from = from;
                        find_pos.to   = to;
                        this.find_pos.add(find_pos);

                        have_more_than_one_occurences = true;
                    }
                }
                catch (GLib.RegexError ex)
                {
                    GLib.assert(false);
                }

                // Disable find previous button.
                // Enable/disable find next button.
                view->frm_conv.btn_find_prev.en(false);
                view->frm_conv.btn_find_next.en(have_more_than_one_occurences);
            }
        }

        /*----------------------------------------------------------------------------*/

        // Conversation search input text control button cancel signal
        private void *srch_conv_btn_cancel(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Clear search text control.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_conv.srch_conv.clr();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Conversation search input text control button search signal
        private void *srch_conv_btn_srch(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get string to find
            string find_str = view->frm_conv.srch_conv.val_get();
            unowned string? find_str_non_empty = find_str.char_count() > 0 ? find_str : null;

            // Update date & time tree control and conversation text
            // control
            try
            {
                // Clear and refill date & time tree control.
                // It also enables/disables "Delete" button.
                //
                // Clear and refill conversation text input.
                // It also enables/disables find previous/next button.
                tree_datetime_fill(find_str_non_empty);
                txt_conv_fill(find_str_non_empty);
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Conversation search input text control button changed signal
        private void *srch_conv_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Do nothing.
            // (Someday, it is possible to implement live search here.)

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Find previous button click callback
        private void *btn_find_prev_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Scroll to previous occurence
            this.find_cur--;

            FindPos find_pos = this.find_pos[(int) this.find_cur];

            view->frm_conv.txt_conv.sel_set((long) find_pos.from, (long) find_pos.to);
            view->frm_conv.txt_conv.pos_show(find_pos.from);

            // Disable find previous button.
            // Enable find next button.
            if (this.find_cur == 0)
                view->frm_conv.btn_find_prev.en(false);
            view->frm_conv.btn_find_next.en(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Find next button click callback
        private void *btn_find_next_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Scroll to next occurence
            this.find_cur++;

            FindPos find_pos = this.find_pos[(int) this.find_cur];

            view->frm_conv.txt_conv.sel_set((long) find_pos.from, (long) find_pos.to);
            view->frm_conv.txt_conv.pos_show(find_pos.from);

            // Enable find previous button.
            // Disable find next button.
            view->frm_conv.btn_find_prev.en(true);
            if (this.find_cur == this.find_pos.size - 1)
                view->frm_conv.btn_find_next.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Delete" button click callback
        private void *btn_del_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Some information based on currently selected item in date &
            // time tree control
            Model.Buddy? buddy              = ctrl->handled_contacts.frm_conv.buddy;
            Model.Conference? conference    = ctrl->handled_contacts.frm_conv.conference;
            View.Wrapper.TreeCtrl.Item item = view->frm_conv.tree_datetime.sel_get();
            string val                      = view->frm_conv.tree_datetime.val_get(item);
            Datetime datetime               = this.datetime[val];

            // Delete messages to/from specified buddy/conference from database
            // tables "conv_buddies"/"conv_conferences".
            // Update date & time tree control and conversation text
            // control.
            try
            {
                // Delete from database...
                if (conference == null)
                {
                    // ... table "conv_buddies"
                    model->self.db_conv_buddies_del(buddy.uid,
                                                    datetime.from,
                                                    datetime.to);
                }
                else
                {
                    // ... table "conv_conferences"
                    model->self.db_conv_conferences_del(conference.uid,
                                                        datetime.from,
                                                        datetime.to);
                }

                // Get string to find
                string find_str = view->frm_conv.srch_conv.val_get();
                unowned string? find_str_non_empty = find_str.char_count() > 0 ? find_str : null;

                // Clear and refill date & time tree control.
                // It also enables/disables "Delete" button.
                //
                // Clear and refill conversation text input.
                // It also enables/disables find previous/next button.
                tree_datetime_fill(find_str_non_empty);
                txt_conv_fill(find_str_non_empty);

                // Select last messages from database and append them to text
                // control.
                string? name;
                string? msg;
                GLib.DateTime? datetime_;

                Sqlite.Statement? stmt = null;

                if (buddy != null &&
                    model->peer_buddy == buddy)
                {
                    // Clear previous conversations
                    view->frm_main.txt_conv_buddy_tab1.clr();
                    ctrl->frm_main.txt_conv_buddy_tab1_typing_len = 0;

                    // Select next message from database table "conv_buddies"
                    // written by/for peer with specific uid
                    bool am_i;

                    while (model->self.db_conv_buddies_sel(ref stmt,

                                                           out name,
                                                           buddy.uid,
                                                           out am_i,
                                                           out msg,
                                                           out datetime_,

                                                           null, // No date & time limit, just limit to number of outputted messages below
                                                           null, // No date & time limit, just limit to number of outputted messages below

                                                           View.FrmMain.TXT_CONV_TAB1_LIMIT))
                    {
                        // Append next message to text control with last exchanged
                        // messages from/to the buddy
                        view->frm_main.txt_conv_buddy_tab1_append(name,
                                                                  am_i,
                                                                  msg,
                                                                  datetime_);
                    }

                    // Append typing information
                    if ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                        ctrl->frm_main.txt_conv_buddy_tab1_typing_len = view->frm_main.txt_conv_buddy_tab1_append_typing(buddy.display_name_get());
                }
                else if (conference != null &&
                         model->peer_conference == conference)
                {
                    // Clear previous conversations
                    view->frm_main.txt_conv_conference_tab1.clr();

                    // Select next message from database table "conv_conferences"
                    // written by/for peer with specific uid
                    uint fg;
                    uint bg;

                    while (model->self.db_conv_conferences_sel(ref stmt,

                                                               out name,
                                                               conference.uid,
                                                               out fg,
                                                               out bg,
                                                               out msg,
                                                               out datetime_,

                                                               null, // No date & time limit, just limit to number of outputted messages below
                                                               null, // No date & time limit, just limit to number of outputted messages below

                                                               View.FrmMain.TXT_CONV_TAB1_LIMIT))
                    {
                        // Append next message to text control with last exchanged
                        // messages from/to the conference
                        view->frm_main.txt_conv_conference_tab1_append(name,
                                                                       fg,
                                                                       bg,
                                                                       msg,
                                                                       datetime_);
                    }
                }
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Delete All" button click callback
        private void *btn_del_all_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Delete all messages to/from specified buddy/conference from
            // database tables "conv_buddies"/"conv_conferences"
            Model.Buddy      buddy      = ctrl->handled_contacts.frm_conv.buddy;
            Model.Conference conference = ctrl->handled_contacts.frm_conv.conference;

            try
            {
                // Delete from database...
                if (conference == null)
                {
                    // ... table "conv_buddies"
                    model->self.db_conv_buddies_del(buddy.uid,
                                                    null, // No date & time limit, delete all messages
                                                    null  // No date & time limit, delete all messages
                                                   );
                }
                else
                {
                    // ... table "conv_conferences"
                    model->self.db_conv_conferences_del(conference.uid,
                                                        null, // No date & time limit, delete all messages
                                                        null  // No date & time limit, delete all messages
                                                       );
                }

                // Update controls:
                //  - main window,
                if (buddy != null &&
                    model->peer_buddy == buddy)
                {
                    view->frm_main.txt_conv_buddy_tab1.clr();
                    ctrl->frm_main.txt_conv_buddy_tab1_typing_len = 0;
                }
                else if (conference != null &&
                         model->peer_conference == conference)
                {
                    view->frm_main.txt_conv_conference_tab1.clr();
                }

                //  - "Conversation History" frame
                view->frm_conv.tree_datetime.clr();
                this.datetime.clear();
                view->frm_conv.txt_conv.clr();
                view->frm_conv.srch_conv.clr();
                view->frm_conv.btn_del.en(false);
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Close" button click callback
        private void *btn_close_conv_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Close "Conversation History" frame.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_conv.show(false);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Conversation History" frame callback constructor.
         *
         * Connects signals to callback methods.
         */
        public FrmConv()
        {
            // GObject-style construction
            Object();

            // Conversation history date & time
            this.datetime = new Gee.HashMap<string, Datetime>();

            // Array list with found positions of substring in conversation
            // history
            this.find_cur = 0;
            this.find_pos = new Gee.ArrayList<FindPos>();

            // Connect signals to callback methods
            this.focus_set_sig.connect(focus_set);
            this.closed_sig.connect(closed);
            this.tree_datetime_selected_sig.connect(tree_datetime_selected);
            this.srch_conv_btn_cancel_sig.connect(srch_conv_btn_cancel);
            this.srch_conv_btn_srch_sig.connect(srch_conv_btn_srch);
            this.srch_conv_changed_sig.connect(srch_conv_changed);
            this.btn_find_prev_clicked_sig.connect(btn_find_prev_clicked);
            this.btn_find_next_clicked_sig.connect(btn_find_next_clicked);
            this.btn_del_clicked_sig.connect(btn_del_clicked);
            this.btn_del_all_clicked_sig.connect(btn_del_all_clicked);
            this.btn_close_conv_clicked_sig.connect(btn_close_conv_clicked);
        }
    }
}

