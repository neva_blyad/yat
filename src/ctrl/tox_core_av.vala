/*
 *    tox_core_av.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * Tox AV class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    [SingleInstance]
    public class ToxCoreAV : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * This signal is emitted when a peer calls us
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param audio  true:  the buddy is sending audio,
         *               false: otherwise,
         * @param video  true:  the buddy is sending video,
         *               false: otherwise
         */
        public signal void called_sig(ToxAV.ToxAV tox_av,
                                      uint32      uid,
                                      bool        audio,
                                      bool        video);

        /**
         * This signal is emitted when the network becomes too saturated
         * for current bit rates at which point core suggests new
         * bitrates
         *
         * @param uid           Conference unique identifier
         * @param audio_bitrate Suggested maximum audio bitrate in
         *                      Kb/s
         * @param video_bitrate Suggested maximum video bitrate in
         *                      Kb/s
         */
        public signal void bitrate_suggest_sig(uint32 uid,
                                               uint32 audio_bitrate,
                                               uint32 video_bitrate);

        /**
         * This signal is emitted when an audio frame is coming
         *
         * @param tox_av       Tox A/V instance
         * @param uid          Conference unique identifier
         * @param pcm          An array of audio samples
         *                     (cnt_samples * cnt_channels elements)
         * @param cnt_samples  The number of audio samples per channel
         *                     in the PCM array
         * @param cnt_channels Number of audio channels
         * @param bitrate      Sampling rate used in this frame
         */
        public signal void audio_rx_sig(ToxAV.ToxAV tox_av,
                                        uint32      uid,
                                        int16[]     pcm,
                                        size_t      cnt_samples,
                                        uint8       cnt_channels,
                                        uint32      bitrate);

        /**
         * This signal is emitted when an video frame is coming
         *
         * @param tox_av  Tox A/V instance
         * @param uid     Conference unique identifier
         * @param wigth   Width
         * @param height  Height
         * @param y       Luminosity plane
         * @param u       U chroma plane
         * @param v       V chroma plane
         * @param ystride Luminosity stride
         * @param ustride U chroma plane stride
         * @param vstride V chroma plane stride
         */
        public signal void video_rx_sig(ToxAV.ToxAV tox_av,

                                        uint32  uid,
                                        uint16  width,
                                        uint16  height,
                                        uint8[] y,
                                        uint8[] u,
                                        uint8[] v,
                                        int32   ystride,
                                        int32   ustride,
                                        int32   vstride);

        /**
         * This signal is emitted when an event is issued
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param state  Bitmask of ToxAV.FriendCallState enums
         */
        public signal void buddy_av_state_changed_sig(ToxAV.ToxAV tox_av,
                                                      uint32      uid,
                                                      uint32      state);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer calls us.
        //
        // Parameters:
        //  — tox_av Tox A/V instance
        //  — uid    Conference unique identifier
        //  — audio  true:  the buddy is sending audio,
        //           false: otherwise,
        //  — video  true:  the buddy is sending video,
        //           false: otherwise
        private void called(ToxAV.ToxAV tox_av,
                            uint32      uid,
                            bool        audio,
                            bool        video)
        {
            Model.Buddy buddy = model->buddies_hash[uid];
            string      dbg;
            string      display_name = buddy.display_name_get();
            string?     err = null;

            // Available multimedia
            if (!audio)
            {
                if (!video)
                {
                    err = _("buddy doesn't provide both A/V, rejecting");
                }
            }

            dbg = _("%s: %s: ").printf(video ?
                _("Incoming video call from buddy") :
                _("Incoming audio call from buddy"), display_name);

            // Have error?
            if (err == null)
            {
                // Are we already talking to this buddy or doing something
                // else with him?
                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                    (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) == 0)
                {
                    // We are not talking to the buddy
                    GLib.debug(dbg + _("waiting for your decision"));

                    // Fill the buddy call information.
                    // Add buddy to A/V peers.
                    if (video)
                    {
                        buddy.action_to_us |= Model.Buddy.state_t.DOING_VIDEO_CALL;
                        model->peer_buddies_video[buddy.uid] = buddy;
                    }
                    else
                    {
                        buddy.action_to_us |= Model.Buddy.state_t.DOING_AUDIO_CALL;
                        model->peer_buddies_audio[buddy.uid] = buddy;
                    }
                }
                else
                {
                    // We are talking to the buddy already
                    GLib.debug(dbg + _("already have a deal with him, ignoring"));
                }
            }
            // Handle error
            else
            {
                // Handle the incoming call rejecting
                GLib.debug(dbg + err);

                // Cancel the A/V call
                ctrl->frm_main.av_call_cancel(buddy,
                                              true, // Hang up
                                              true, // Update GUI
                                              true  // Mutex is locked
                                             );
            }

            // Update buddy list,
            // new row background should be set
            int idx = model->buddies_lst.index_of(buddy);

            if (idx != -1)
            {
                ctrl->frm_main.lst_ctrl_buddies_val_set(idx,
                                                        buddy,

                                                        true,  // Set display name column
                                                        false, // Don't set status column
                                                        false, // Don't state column
                                                        false  // Don't user state column
                                                       );
            }

            // A/V call GUIs elements.
            // Update status bar.
            ctrl->frm_main.av_widgets_upd();
            ctrl->frm_main.status_val_set(View.FrmMain.STATUS_COL_EVT);
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the network becomes too
        // saturated for current bit rates at which point core suggests
        // new bitrates.
        //
        // Parameters:
        //  — uid           Conference unique identifier
        //  — audio_bitrate Suggested maximum audio bitrate in
        //                  Kb/s
        //  — video_bitrate Suggested maximum video bitrate in
        //                  Kb/s
        private void bitrate_suggest(uint32 uid,
                                     uint32 audio_bitrate,
                                     uint32 video_bitrate)
        {
            // TODO: by now this method does nothing.
            //       I will add some behavior in future.
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when an audio frame is coming.
        //
        // Parameters:
        //  — tox_av       Tox A/V instance
        //  — uid          Conference unique identifier
        //  — pcm          An array of audio samples
        //                 (cnt_samples * cnt_channels elements)
        //  — cnt_samples  The number of audio samples per channel in
        //                 the PCM array
        //  — cnt_channels Number of audio channels
        //  — bitrate      Sampling rate used in this frame
        private void audio_rx(ToxAV.ToxAV tox_av,
                              uint32      uid,
                              int16[]     pcm,
                              size_t      cnt_samples,
                              uint8       cnt_channels,
                              uint32      bitrate)
        {
            Model.Buddy buddy = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();

            string dbg  = _("Incoming audio frame from buddy: %s: ").printf(display_name);
                   dbg += GLib.dngettext(Cfg.DOMAIN, "%u sample", "%u samples", cnt_samples).printf((uint) cnt_samples);
                   dbg += ", ";
                   dbg += GLib.dngettext(Cfg.DOMAIN, "%u channel", "%u channels", cnt_channels).printf(cnt_channels);
                   dbg += ", ";
                   dbg += _("%lu Hz bitrate").printf(bitrate);
                   dbg += ": ";

            // Drop foreign frame
            bool drop_frame = false;

            if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) == 0)
            {
                dbg += _("have no A/V call, dropping");
                drop_frame = true;
            }
            else if ((buddy.action_from_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0 ||
                     (buddy.action_to_us   & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0)
            {
                dbg += _("A/V call is not answered yet, dropping");
                drop_frame = true;
            }
            else
                dbg += _("OK");

            GLib.debug(dbg);

            if (drop_frame)
                return;

            // Play the audio frame
            pcm.length = (int) cnt_samples * cnt_channels; // Array size isn't set before this instruction
            model->audio.play(model,
                              pcm,
                              cnt_channels,
                              bitrate);
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when an video frame is coming.
        //
        // Parameters:
        //  - tox_av  Tox A/V instance
        //  - uid     Buddy unique identifier
        //  - wigth   Width
        //  - height  Height
        //  - y       Luminosity plane
        //  - u       U chroma plane
        //  - v       V chroma plane
        //  - ystride Luminosity plane stride
        //  - ustride U chroma plane stride
        //  - vstride V chroma plane stride
        private void video_rx(ToxAV.ToxAV tox_av,

                              uint32  uid,
                              uint16  width,
                              uint16  height,
                              uint8[] y,
                              uint8[] u,
                              uint8[] v,
                              int32   ystride,
                              int32   ustride,
                              int32   vstride)
        {
            Model.Buddy buddy = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      dbg = _("Incoming video frame from buddy: %s: %ux%u: ").printf(display_name, width, height);

            // Always-positive strides.
            // Note. abs() is implemented manually, not every platform has
            // this method.
            //uint32 ystride_abs = ystride.abs();
            //uint32 ustride_abs = ustride.abs();
            //uint32 vstride_abs = vstride.abs();
            uint32 ystride_abs = ystride >= 0 ? ystride : -ystride;
            uint32 ustride_abs = ustride >= 0 ? ustride : -ustride;
            uint32 vstride_abs = vstride >= 0 ? vstride : -vstride;

            // Some temporary variables
            uint32 width2  = width  / 2;
            uint32 height2 = height / 2;

            // Drop wrong frames
            bool drop_frame;

            if (ystride_abs < width  ||
                ustride_abs < width2 ||
                vstride_abs < width2)
            {
                dbg += _("Too small strides, dropping");
                drop_frame = true;
            }
            else
            {
                dbg += _("OK");
                drop_frame = false;
            }

            GLib.debug(dbg);

            if (drop_frame)
                return;

            // Plane lengths without strides
            uint32 len_y = width  * height;
            uint32 len_u = width2 * height2;
            uint32 len_v = len_u;

            // Prepare a new buffer without strides
            uint32 len = len_y + len_u + len_v;
            var    buf = new uint8[len];

            // Copy Y plane.
            // (Go throught each row of the plane with stride.)
            uint32 offset_dst = 0;
            uint32 offset_src = 0;
            uint32 len_copy   = width;

            for (uint row = 0; row < height; row++)
            {
                Posix.memcpy(&buf[offset_dst], &y[offset_src], len_copy);

                offset_dst += len_copy;
                offset_src += ystride_abs;
            }

            // Copy U plane
            offset_src = 0;
            len_copy   = width2;

            for (uint row = 0; row < height2; row++)
            {
                Posix.memcpy(&buf[offset_dst], &u[offset_src], len_copy);

                offset_dst += len_copy;
                offset_src += ustride_abs;
            }

            // Copy V plane
            offset_src = 0;
            //len_copy   = width2;

            for (uint row = 0; row < height2; row++)
            {
                Posix.memcpy(&buf[offset_dst], &v[offset_src], len_copy);

                offset_dst += len_copy;
                offset_src += vstride_abs;
            }

            // Play the video frame
            model->video.play(model,
                              buf,
                              width,
                              height);
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when an event is issued.
        //
        // Parameters:
        //  — tox_av Tox A/V instance
        //  — uid    Conference unique identifier
        //  — state  Bitmask of ToxAV.FriendCallState enums
        private void buddy_av_state_changed(ToxAV.ToxAV tox_av,
                                            uint32      uid,
                                            uint32      state)
        {
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      dbg;

            // Available multimedia
            bool video = (buddy.action_from_us & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0 ||
                         (buddy.action_to_us   & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0;

            // Has the call been rejectred or finished?
            if ((state & ToxAV.FriendCallState.FINISHED) > 0 ||
                (state & ToxAV.FriendCallState.ERROR)    > 0)
            {
                if ((state & ToxAV.FriendCallState.ERROR) > 0)
                {
                    dbg = _("%s: %s: ").printf(video ? _("Buddy reported video call errors") :
                                                       _("Buddy reported audio call errors"), display_name);
                }
                else
                {
                    bool rejected_flag = (buddy.action_from_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0;
                    dbg = _("%s: %s: ").printf(video ?
                        rejected_flag ? _("Buddy rejected video call") : _("Buddy finished video call") :
                        rejected_flag ? _("Buddy rejected audio call") : _("Buddy finished audio call"), display_name);
                }

                // Continue only if we really talk with this buddy
                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) > 0 ||
                    (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) > 0)
                {
                    dbg += _("cleaned up the call");

                    // Cancel the A/V call
                    ctrl->frm_main.av_call_cancel(buddy,
                                                  false, // Do not hang up
                                                  true,  // Update GUI
                                                  true   // Mutex is locked
                                                 );
                }
                else
                {
                    dbg += _("no active call, ignoring");
                }
            }
            // Has the call been answered?
            else if ((state & ToxAV.FriendCallState.SENDING_A)   > 0 ||
                     (state & ToxAV.FriendCallState.ACCEPTING_A) > 0 ||
                     (state & ToxAV.FriendCallState.SENDING_V)   > 0 ||
                     (state & ToxAV.FriendCallState.ACCEPTING_V) > 0)
            {
                dbg = _("%s: %s: ").printf(video ? _("Buddy answered video call") :
                                                   _("Buddy answered audio call"), display_name);

                // Continue only if we are calling to this buddy
                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0)
                {
                    dbg += _("OK");

                    // Suggested multimedia
                    //bool video_ = (state & ToxAV.FriendCallState.SENDING_V)   > 0 ||
                    //              (state & ToxAV.FriendCallState.ACCEPTING_V) > 0;

                    // Our A/V call has been answered 
                    ctrl->frm_main.av_answer_answered(buddy,
                                                      false, // We call, buddy answered
                                                      video,
                                                      true   // Mutex is locked
                                                     );
                }
                else
                {
                    dbg += _("but we don't call him, ignoring");
                }
            }
            else
            {
                dbg = _("%s: %s").printf(video ? _("Buddy reported video call unknown problem") :
                                                 _("Buddy reported audio call unknown problem"), display_name);
            }

            GLib.debug(dbg);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox AV callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreAV()
        {
            // Connect signals to callback methods
            this.called_sig.connect(called);
            this.bitrate_suggest_sig.connect(bitrate_suggest);
            this.audio_rx_sig.connect(audio_rx);
            this.video_rx_sig.connect(video_rx);
            this.buddy_av_state_changed_sig.connect(buddy_av_state_changed);
        }
    }
}

