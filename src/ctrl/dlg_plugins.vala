/*
 *    dlg_plugins.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Plugins" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgPlugins : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Width and height of the program icon
        private const size_t ICON_WIDTH  = 200;
        private const size_t ICON_HEIGHT = 200;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /*
         * "Plugins" dialog close signal
         */
        public signal void *closed_sig(void *fn, void *param, void *event);

        /**
         * Check list box with plugins toggle signal
         */
        public signal void *check_lst_plugins_toggled_sig(void *fn, void *param, void *event);

        /**
         * Check list box with plugins select signal
         */
        public signal void *check_lst_plugins_selected_sig(void *fn, void *param, void *event);

        /**
         * "About" button click signal
         */
        public signal void *btn_about_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Preferences" button click signal
         */
        public signal void *btn_preferences_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Enable/disable "Preferences" button
        private void btn_preferences_en_set(Peas.PluginInfo info)
        {
            // "Preferences" button availability depends on the particular
            // plugin
            Model.Plugin plugin = model->plugins_ext.get_extension(info) as Model.Plugin;
            bool avail = info.is_loaded();

            if (avail)
            {
                // Ask the plugin kindly if it supports any sort of
                // configuration
                avail = plugin.cfg_check(model, info);
            }

            // Plugin description fields
            unowned string filename = info.get_module_name();
                    string domain   = "yat-plugins-" + filename;
            unowned string name     = GLib.dgettext(domain, info.get_name());

            // Enable/disable "Preferences" button
            if (avail) GLib.debug(_("Configuration is available for plugin %s"),     name);
            else       GLib.debug(_("Configuration is not available for plugin %s"), name);

            view->dlg_plugins.btn_preferences.en(avail);
        }

        /*----------------------------------------------------------------------------*/

        // "Plugins" dialog close callback
        private void *closed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Close "Plugins" dialog.
            // Unlock mutex.
            model->mutex.lock();
            view->dlg_plugins.show(false);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Check list box with plugins toggle callback
        private void *check_lst_plugins_toggled(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Iterate through the all plugins
            uint            idx;
            Peas.PluginInfo info         = null;
            size_t          cnt          = view->dlg_plugins.check_lst_plugins.cnt_get();
            bool            changed_flag = false;

            for (idx = 0; idx < cnt; idx++)
            {
                // Plugin description fields
                               info     = model->plugins_lst[(int) idx];
                unowned string filename = info.get_module_name();

                // Get state of the plugin.
                // Is this plugin checked?
                bool checked_flag = view->dlg_plugins.check_lst_plugins.is_checked(idx);
                bool en_plugin;

                // Had been this plugin enabled early?
                try                          { en_plugin = model->self.cfg.get_boolean("plugins", filename); }
                catch (GLib.KeyFileError ex) { en_plugin = false; }

                // Had been this plugin enabled early?
                // User checked the plugin and it had been disabled =>
                if (checked_flag && !en_plugin)
                {
                    // ... enable and load
                    try
                    {
                        // Load plugin
                        Model.Plugin plugin = model->plugins_ext.get_extension(info) as Model.Plugin;
                        plugin.load(model, info);

                        // Update per-profile config with the plugin enabled
                        model->self.cfg.set_boolean("plugins", filename, true);
                    }
                    catch (GLib.Error ex)
                    {
                        // Something has gone wrong.
                        // The plugin has not been loaded.
                        // Assume that this plugin still is disabled.
                        // Uncheck the plugin back.
                        GLib.debug(ex.message);
                        en_plugin = false;
                        view->dlg_plugins.check_lst_plugins.check(idx, en_plugin);

                        // Unlock mutex
                        model->mutex.unlock();

                        var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_plugins, ex.message, _("Error"));
                        msg_dlg.modal_show();

                        // Lock mutex
                        model->mutex.lock();
                    }

                    changed_flag = true;
                    break;
                }

                // User unchecked the plugin and it had been enabled =>
                if (!checked_flag && en_plugin)
                {
                    // ... disable and unload
                    try
                    {
                        // Update per-profile config with the plugin disabled
                        model->self.cfg.set_boolean("plugins", filename, false);

                        // Unload it
                        Model.Plugin plugin = model->plugins_ext.get_extension(info) as Model.Plugin;
                        plugin.unload(model, info);
                    }
                    catch (GLib.Error ex)
                    {
                        // Something has gone wrong.
                        // Check the plugin back.
                        GLib.debug(ex.message);
                        en_plugin = true;
                        view->dlg_plugins.check_lst_plugins.check(idx, en_plugin);

                        // Unlock mutex
                        model->mutex.unlock();

                        var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_plugins, ex.message, _("Error"));
                        msg_dlg.modal_show();

                        // Lock mutex
                        model->mutex.lock();
                    }

                    changed_flag = true;
                    break;
                }
            }

            // Do something only if plugin state has been changed
            if (changed_flag)
            {
                // Get index of selected plugin in list
                int sel = view->dlg_plugins.check_lst_plugins.sel_elem_get();

                // Enable/disable "Preferences" button if needed
                if (sel != View.Wrapper.Win.FOUND_NOT &&
                        idx == (uint) sel)
                    btn_preferences_en_set(info);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Check list box with plugins select callback
        private void *check_lst_plugins_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected plugin in list
            int sel = view->dlg_plugins.check_lst_plugins.sel_elem_get();
            GLib.assert(sel != View.Wrapper.Win.FOUND_NOT);

            Peas.PluginInfo info = model->plugins_lst[sel];

            // Enable "About" button.
            // Enable/disable "Preferences" button.
            view->dlg_plugins.btn_about.en(true);
            btn_preferences_en_set(info);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "About" button click callback
        private void *btn_about_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected plugin in list
            int sel = view->dlg_plugins.check_lst_plugins.sel_elem_get();
            GLib.assert(sel != View.Wrapper.Win.FOUND_NOT);

            Peas.PluginInfo info   = model->plugins_lst[sel];
            Model.Plugin    plugin = model->plugins_ext.get_extension(info) as Model.Plugin;

            // Plugin description fields
            string domain = "yat-plugins-" + info.get_module_name();

                    string?   icon_filepath = plugin.icon_filepath_get(model, info);
            unowned string    name          = GLib.dgettext(domain, info.get_name());
            unowned string    ver           = GLib.dgettext(domain, info.get_version());
            unowned string?   desc          = info.get_external_data("DescriptionLong");
            unowned string?   copying       = info.get_external_data("License");
            unowned string    url           = info.get_website();
            unowned string    copyright     = GLib.dgettext(domain, info.get_copyright());
            unowned string[]? devs          = info.get_authors();
            unowned string?   artists       = info.get_external_data("Artists");
            unowned string?   translators   = info.get_external_data("Translators");

            if (desc        != null) desc        = GLib.dgettext(domain, desc);
            if (copying     != null) copying     = GLib.dgettext(domain, copying);
            if (artists     != null) artists     = GLib.dgettext(domain, artists);
            if (translators != null) translators = GLib.dgettext(domain, translators);

            // Unlock mutex
            model->mutex.unlock();

            // Show about dialog
            var about_dlg = new View.Wrapper.AboutDlg(view->frm_main,

                                                      icon_filepath,
                                                      ICON_WIDTH,
                                                      ICON_HEIGHT,

                                                      name,
                                                      ver,
                                                      desc,
                                                      copying,
                                                      url,
                                                      copyright,
                                                      GLib.dgettext(domain, string.joinv(";", devs)).split(";"), // We should glue authors to find out its translated variant. Then we'll split it back again =(
                                                      artists.split(";"),
                                                      translators.split(";"));
            about_dlg.modal_show();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Preferences" button click callback
        private void *btn_preferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected plugin in list
            int sel = view->dlg_plugins.check_lst_plugins.sel_elem_get();
            GLib.assert(sel != View.Wrapper.Win.FOUND_NOT);

            Peas.PluginInfo info   = model->plugins_lst[sel];
            Model.Plugin    plugin = model->plugins_ext.get_extension(info) as Model.Plugin;

            // Execute plugin configuration
            plugin.cfg_exec(model, info);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Plugin" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgPlugins()
        {
            // Connect signals to callback methods
            this.closed_sig.connect(closed);
            this.check_lst_plugins_toggled_sig.connect(check_lst_plugins_toggled);
            this.check_lst_plugins_selected_sig.connect(check_lst_plugins_selected);
            this.btn_about_clicked_sig.connect(btn_about_clicked);
            this.btn_preferences_clicked_sig.connect(btn_preferences_clicked);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Fill check list box with plugins
         */
        public void check_lst_plugins_fill()
        {
            // Add plugins to the "Plugins" dialog
            uint idx = 0;

            foreach (Peas.PluginInfo info in model->plugins_lst)
            {
                // Plugin description fields
                        string  filename = info.get_module_name();
                        string  domain   = "yat-plugins-" + filename;
                unowned string  name     = GLib.dgettext(domain, info.get_name());
                unowned string  desc     = GLib.dgettext(domain, info.get_description());
                unowned string? loader   = info.get_external_data("Loader");

                // Is the plugin enabled?
                bool en_plugin;

                try                          { en_plugin = model->self.cfg.get_boolean("plugins", filename); }
                catch (GLib.KeyFileError ex) { en_plugin = false; }

                // Add file extension to filepath
                string filepath;

                switch (loader)
                {
                case "perl":
                    filename += ".pm";
                    break;
                case "python":
                case "python3":
                    filename += ".py";
                    break;
                case "lua51":
                    filename += ".lua";
                    break;
                default:
#if __WXMSW__
                    filename = "lib" + filename + ".dll";
#elif __WXGTK__
                    filename = "lib" + filename + ".so";
#else
                    // TODO
#endif
                    break;
                }

                filepath = GLib.Path.build_filename(info.get_module_dir(), filename);

                // Prepare entry value for "Plugins" dialog
                string val = "%s\n%s".printf(name, desc);

                if (GLib.FileUtils.test(filepath, GLib.FileTest.IS_REGULAR))
                {
                    val += "\n%s".printf(filepath);
                }

                // Add the value to plugin list
                view->dlg_plugins.check_lst_plugins.append(val);
                if (en_plugin)
                    view->dlg_plugins.check_lst_plugins.check(idx, true);

                idx++;
            }
        }
    }
}

