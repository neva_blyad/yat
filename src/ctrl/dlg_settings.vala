/*
 *    dlg_settings.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Settings" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgSettings : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Node source choice select callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *choice_node_tab3_selected_sig(void *fn, void *param, void *event);

        /**
         * URL input text control changed callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *txt_url_tab3_changed_sig(void *fn, void *param, void *event);

        /**
         * File picker changed callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *file_tab3_changed_sig(void *fn, void *param, void *event);

        /**
         * Buddy host input text control changed callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *txt_buddy_host_tab3_changed_sig(void *fn, void *param, void *event);

        /**
         * "Add" button click callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_add_tab3_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Remove" button click callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_remove_tab3_clicked_sig(void *fn, void *param, void *event);

        /**
         * Up button click callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_up_tab3_clicked_sig(void *fn, void *param, void *event);

        /**
         * Down button click callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_down_tab3_clicked_sig(void *fn, void *param, void *event);

        /**
         * Check list box with nodes source choice select callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *check_lst_node_tab3_selected_sig(void *fn, void *param, void *event);

        /**
         * Proxy type choice select callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *choice_proxy_tab3_selected_sig(void *fn, void *param, void *event);

        /**
         * Host input text control changed callback.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *txt_proxy_host_tab3_changed_sig(void *fn, void *param, void *event);

        /**
         * Audio subsystem choice select callback.
         *
         * It is placed on the tab 5 panel.
         */
        public signal void *choice_sys_tab5_selected_sig(void *fn, void *param, void *event);

        /**
         * Capture device choice select callback.
         *
         * It is placed on the tab 5 panel.
         */
        public signal void *choice_cap_tab5_selected_sig(void *fn, void *param, void *event);

        /**
         * Playback device choice select callback.
         *
         * It is placed on the tab 5 panel.
         */
        public signal void *choice_playback_tab5_selected_sig(void *fn, void *param, void *event);

        /**
         * "Reset" button click callback
         */
        public signal void *btn_reset_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Reset to last applied settings" menu item click callback.
         *
         * It is placed in context menu for "Reset" button.
         */
        public signal void *menu_reset_last_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Reset to program default settings" menu item click callback.
         *
         * It is placed in context menu for "Reset" button.
         */
        public signal void *menu_reset_def_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Apply" button click callback
         */
        public signal void *btn_apply_settings_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * reset(), audio_widgets_tab5_reset() parameter
         */
        public enum reset_target_t
        {
            /**
             * Reset to last applied settings
             */
            SETTINGS_APPLIED_LAST,

            /**
             * Reset to program initial settings
             */
            SETTINGS_INITIAL,

            /**
             * Using the current settings of some widgets reset other widgets
             */
            SETTINGS_CUR,
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Reset widgets related to audio to last saved or initial
        // state. They are located on tab 5.
        private void audio_widgets_tab5_reset(reset_target_t target) throws GLib.KeyFileError
        {
            // First, reset or update those widgets that look the same in
            // both states, last applied and program initial, no matter
            // which button user has clicked.
            //
            // Tab 5.
            //
            // Prepare lists with all audio devices available in system;
            // capture and playback devices are interested.
            // Fill choice widgets with these lists.
            string? err = null;
            Model.Profile.cfg_audio_sys_t audio_sys;

            switch (target)
            {
            // Reset to last applied settings
            case reset_target_t.SETTINGS_APPLIED_LAST:
                audio_sys = (Model.Profile.cfg_audio_sys_t) model->self.cfg.get_integer("audio", "sys");
                break;
            // Reset to program initial settings
            case reset_target_t.SETTINGS_INITIAL:
                audio_sys = Model.Profile.cfg_audio_sys_t.AUTO;
                break;
            // Using the current settings of some widgets reset other
            // widgets
            case reset_target_t.SETTINGS_CUR:
                audio_sys = (Model.Profile.cfg_audio_sys_t) view->dlg_settings.choice_sys_tab5.sel_get();
                break;
            // Another?
            default:
                audio_sys = Model.Profile.cfg_audio_sys_t.AUTO;
                GLib.assert(false);
                break;
            }
return;

            string cap_dev      = model->self.cfg.get_string("audio", "cap_dev");
            string playback_dev = model->self.cfg.get_string("audio", "playback_dev");

            uint sel_cap        = 0;
            uint sel_playback   = 0;

            // Fill capture device choice
            view->dlg_settings.choice_cap_tab5.clr();
            view->dlg_settings.choice_cap_tab5.append(_("Detect Automatically"));

            try
            {
                Gee.ArrayList<Gst.Device> devs_cap = model->audio.devs_cap_get(audio_sys);

                for (uint idx = 0; idx < devs_cap.size; idx++)
                {
                    string name = devs_cap[(int) idx].get_display_name();
                    view->dlg_settings.choice_cap_tab5.append(name);
                    if (name != "" &&
                            name == cap_dev)
                        sel_cap = idx + 1;
                }
            }
            catch (Model.ErrAudio ex)
            {
                GLib.debug(ex.message);
                err = ex.message;
            }

            // Fill playback device choice
            view->dlg_settings.choice_playback_tab5.clr();
            view->dlg_settings.choice_playback_tab5.append(_("Detect Automatically"));

            try
            {
                Gee.ArrayList<Gst.Device> devs_playback = model->audio.devs_playback_get(audio_sys);

                for (uint idx = 0; idx < devs_playback.size; idx++)
                {
                    string name = devs_playback[(int) idx].get_display_name();
                    view->dlg_settings.choice_playback_tab5.append(name);
                    if (name != "" &&
                            name == playback_dev)
                        sel_playback = idx + 1;
                }
            }
            catch (Model.ErrAudio ex)
            {
                GLib.debug(ex.message);
                if (err == null)
                    err = ex.message;
            }

            // Show only the first audio related error if it occurs
            if (err != null)
            {
                unowned View.Wrapper.TopLvlWin win;
                if (view->dlg_settings.is_shown()) win = view->dlg_settings;
                else                               win = view->frm_main;

                // Unlock mutex
                model->mutex.unlock();

                var msg_dlg = new View.Wrapper.MsgDlg(win, err, _("Error"));
                msg_dlg.modal_show();

                // Lock mutex
                model->mutex.lock();
            }

            // Second, reset remaining widgets.
            // They look different in last applied and initial state.
            //
            // Last applied or initial state?
            switch (target)
            {
            // Reset to last applied settings
            case reset_target_t.SETTINGS_APPLIED_LAST:

                // Tab 5
                uint   sys = (uint) audio_sys;
                size_t cnt = Model.Profile.cfg_audio_sys_t.CNT;
                if (sys >= cnt)
                    sys = cnt > 1 ? (uint) cnt - 1 : 0;
                view->dlg_settings.choice_sys_tab5.sel_set(sys);

                view->dlg_settings.choice_cap_tab5.sel_set(sel_cap);
                view->dlg_settings.choice_playback_tab5.sel_set(sel_playback);

                // TODO: JACK:       add gain & volume support,
                // TODO: PipeWire:   add gain & volume support,
                // TODO: PulseAudio: ok,
                // TODO: OSS:        add gain & volume support
                bool adjust_sliders = audio_sys == Model.Profile.cfg_audio_sys_t.PULSEAUDIO;

                uint gain;
                uint vol;

                if (adjust_sliders)
                {
                    gain = model->self.cfg.get_integer("audio", "gain");
                    if (gain > 100)
                        gain = 100;

                    vol = model->self.cfg.get_integer("audio", "vol");
                    if (vol > 100)
                        vol = 100;
                }
                else
                {
                    gain = 50;
                    vol  = 50;
                }

                view->dlg_settings.slider_gain_tab5.val_set(gain);
                view->dlg_settings.slider_vol_tab5.val_set(vol);

                view->dlg_settings.slider_gain_tab5.en(adjust_sliders);
                view->dlg_settings.slider_vol_tab5.en(adjust_sliders);

                break;

            // Reset to program initial settings
            case reset_target_t.SETTINGS_INITIAL:

                view->dlg_settings.choice_sys_tab5.sel_set((int) Model.Profile.cfg_audio_sys_t.AUTO);
                view->dlg_settings.choice_cap_tab5.sel_set(View.DlgSettings.CHOICE_CAP_IDX_AUTO);
                view->dlg_settings.choice_playback_tab5.sel_set(View.DlgSettings.CHOICE_PLAYBACK_IDX_AUTO);
                view->dlg_settings.slider_gain_tab5.val_set(50);
                view->dlg_settings.slider_vol_tab5.val_set(50);

                view->dlg_settings.slider_gain_tab5.en(false);
                view->dlg_settings.slider_vol_tab5.en(false);

                break;

            // Using the current settings of some widgets reset other
            // widgets
            case reset_target_t.SETTINGS_CUR:

                view->dlg_settings.choice_cap_tab5.sel_set(View.DlgSettings.CHOICE_CAP_IDX_AUTO);
                view->dlg_settings.choice_playback_tab5.sel_set(View.DlgSettings.CHOICE_PLAYBACK_IDX_AUTO);

                bool adjust_sliders = audio_sys == Model.Profile.cfg_audio_sys_t.PULSEAUDIO;

                if (!adjust_sliders)
                {
                    view->dlg_settings.slider_gain_tab5.val_set(50);
                    view->dlg_settings.slider_vol_tab5.val_set(50);
                }

                view->dlg_settings.slider_gain_tab5.en(adjust_sliders);
                view->dlg_settings.slider_vol_tab5.en(adjust_sliders);

                break;

            // Another?
            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        // Node source choice select callback.
        //
        // It is placed on the tab 3 panel.
        private void *choice_node_tab3_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected choice
            uint idx = view->dlg_settings.choice_node_tab3.sel_get();

            // Show/hide bootstrap node controls
            switch (idx)
            {
            // URL
            case View.DlgSettings.CHOICE_NODE_IDX_URL:
                ctrl->ctrl_changed_by_user = false;
                view->dlg_settings.txt_url_tab3.clr();
                ctrl->ctrl_changed_by_user = true;
                view->dlg_settings.txt_url_tab3.show(true);
                view->dlg_settings.file_tab3.show(false);
                view->dlg_settings.choice_buddy_tab3.show(false);
                view->dlg_settings.lbl_buddy_host_tab3.show(false);
                view->dlg_settings.txt_buddy_host_tab3.show(false);
                view->dlg_settings.lbl_buddy_port_tab3.show(false);
                view->dlg_settings.spin_buddy_port_tab3.show(false);
                view->dlg_settings.btn_add_tab3.en(false);
                break;

            // File
            case View.DlgSettings.CHOICE_NODE_IDX_FILE:
                view->dlg_settings.txt_url_tab3.show(false);
                view->dlg_settings.file_tab3.show(true);
                view->dlg_settings.choice_buddy_tab3.show(false);
                view->dlg_settings.lbl_buddy_host_tab3.show(false);
                view->dlg_settings.txt_buddy_host_tab3.show(false);
                view->dlg_settings.lbl_buddy_port_tab3.show(false);
                view->dlg_settings.spin_buddy_port_tab3.show(false);
                size_t len = view->dlg_settings.file_tab3.path_get().length;
                view->dlg_settings.btn_add_tab3.en(len > 0);
                break;

            // Buddy
            case View.DlgSettings.CHOICE_NODE_IDX_BUDDY:
                view->dlg_settings.txt_url_tab3.show(false);
                view->dlg_settings.file_tab3.show(false);
                if (view->dlg_settings.choice_buddy_tab3.cnt_get() > 0)
                    view->dlg_settings.choice_buddy_tab3.sel_set(0);
                view->dlg_settings.choice_buddy_tab3.show(true);
                view->dlg_settings.lbl_buddy_host_tab3.show(true);
                ctrl->ctrl_changed_by_user = false;
                view->dlg_settings.txt_buddy_host_tab3.clr();
                ctrl->ctrl_changed_by_user = true;
                view->dlg_settings.txt_buddy_host_tab3.show(true);
                view->dlg_settings.lbl_buddy_port_tab3.show(true);
                view->dlg_settings.spin_buddy_port_tab3.val_set(0);
                view->dlg_settings.spin_buddy_port_tab3.show(true);
                view->dlg_settings.btn_add_tab3.en(false);
                break;

            // Something else
            default:
                GLib.assert(false);
                break;
            }

            view->dlg_settings.check_lst_node_tab3.show(true, true); // Fit hack

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // URL input text control changed callback.
        //
        // It is placed on the tab 3 panel.
        private void *txt_url_tab3_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Add" button
            ulong len = view->dlg_settings.txt_url_tab3.len_get();
            view->dlg_settings.btn_add_tab3.en(len > 0);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // File picker changed callback.
        //
        // It is placed on the tab 3 panel.
        private void *file_tab3_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Add" button
            size_t len = view->dlg_settings.file_tab3.path_get().length;
            view->dlg_settings.btn_add_tab3.en(len > 0);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Buddy host input text control changed callback.
        //
        // It is placed on the tab 3 panel.
        private void *txt_buddy_host_tab3_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Add" button
            ulong len = view->dlg_settings.txt_buddy_host_tab3.len_get();
            view->dlg_settings.btn_add_tab3.en(len > 0);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Add" button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_add_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected node type
            uint idx = view->dlg_settings.choice_node_tab3.sel_get();

            string  node;
            string *meta;

            // Get node
            switch (idx)
            {
            // URL
            case View.DlgSettings.CHOICE_NODE_IDX_URL:

                string url = view->dlg_settings.txt_url_tab3.val_get();

                node = _("URL") + ": " + url;
                meta =   "URL"  + ": " + url;

                break;

            // File
            case View.DlgSettings.CHOICE_NODE_IDX_FILE:

                string filepath = view->dlg_settings.file_tab3.path_get();

                node = _("File") + ": " + filepath;
                meta =   "File"  + ": " + filepath;

                break;

            // Buddy
            case View.DlgSettings.CHOICE_NODE_IDX_BUDDY:

                int buddy_idx = view->dlg_settings.choice_buddy_tab3.sel_get();
                GLib.assert(buddy_idx != View.Wrapper.Win.FOUND_NOT);

                string buddy = view->dlg_settings.choice_buddy_tab3.val_get((uint) buddy_idx);
                string host  = view->dlg_settings.txt_buddy_host_tab3.val_get();
                uint   port  = (uint) view->dlg_settings.spin_buddy_port_tab3.val_get();

                node = "%s: %s (%s:%u)".printf(_("Buddy"), buddy, host, port);
                meta = "%s: %s (%s:%u)".printf(  "Buddy",  buddy, host, port);

                break;

            // Something else
            default:

                node = "";
                meta = "";
                GLib.assert(false);

                break;
            }

            // Add node to bootstrap list
            view->dlg_settings.check_lst_node_tab3.append(node);
            size_t cnt = view->dlg_settings.check_lst_node_tab3.cnt_get();
            view->dlg_settings.check_lst_node_tab3.meta_set((uint) cnt - 1, meta);

            // Enable Down button
            int  idx_     = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            bool have_sel = idx_ != View.Wrapper.Win.FOUND_NOT;

            if (have_sel)
            {
                size_t cnt_ = view->dlg_settings.check_lst_node_tab3.cnt_get();
                if (idx_ + 1 == cnt_ - 1)
                    view->dlg_settings.btn_down_tab3.en(true);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Remove" button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_remove_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected node in list.
            // Remove it.
            int idx = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            GLib.assert(idx != View.Wrapper.Win.FOUND_NOT);
            string *meta = view->dlg_settings.check_lst_node_tab3.meta_get((uint) idx);
            delete meta;
            view->dlg_settings.check_lst_node_tab3.remove((uint) idx);

            // Disable "Remove" button if no node is selected.
            // Enable/disable Up & Down buttons.
                 idx      = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            bool have_sel = idx != View.Wrapper.Win.FOUND_NOT;

            if (have_sel)
            {
                size_t cnt = view->dlg_settings.check_lst_node_tab3.cnt_get();

                view->dlg_settings.btn_up_tab3.en(idx != 0);
                view->dlg_settings.btn_down_tab3.en(idx < cnt - 1);
            }
            else
            {
                view->dlg_settings.btn_remove_tab3.en(false);
                view->dlg_settings.btn_up_tab3.en(false);
                view->dlg_settings.btn_down_tab3.en(false);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Up button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_up_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected and above nodes in list
            int    idx_sel   = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            int    idx_above = idx_sel - 1;
            size_t cnt       = view->dlg_settings.check_lst_node_tab3.cnt_get();

            GLib.assert(idx_sel   != View.Wrapper.Win.FOUND_NOT);
            GLib.assert(idx_above >= 0);

            // Get their check states.
            // Swap them.
            bool flag_sel   = view->dlg_settings.check_lst_node_tab3.is_checked(idx_sel);
            bool flag_above = view->dlg_settings.check_lst_node_tab3.is_checked(idx_above);

            view->dlg_settings.check_lst_node_tab3.check(idx_sel,   flag_above);
            view->dlg_settings.check_lst_node_tab3.check(idx_above, flag_sel);

            // Get their values.
            // Swap them.
            string val_sel   = view->dlg_settings.check_lst_node_tab3.val_get(idx_sel);
            string val_above = view->dlg_settings.check_lst_node_tab3.val_get(idx_above);

            view->dlg_settings.check_lst_node_tab3.val_set(idx_sel,   val_above);
            view->dlg_settings.check_lst_node_tab3.val_set(idx_above, val_sel);

            // Get their meta.
            // Swap them.
            string *meta_sel   = view->dlg_settings.check_lst_node_tab3.meta_get(idx_sel);
            string *meta_above = view->dlg_settings.check_lst_node_tab3.meta_get(idx_above);

            view->dlg_settings.check_lst_node_tab3.meta_set(idx_sel,   meta_above);
            view->dlg_settings.check_lst_node_tab3.meta_set(idx_above, meta_sel);

            // Select above node
            //view->dlg_settings.check_lst_node_tab3.sel_set(idx_sel,   false);
            view->dlg_settings.check_lst_node_tab3.sel_set(idx_above, true);

            // Enable/disable Up & Down buttons
            if (idx_above == 0)
                view->dlg_settings.btn_up_tab3.en(false);
            if (idx_sel == cnt - 1)
                view->dlg_settings.btn_down_tab3.en(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Down button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_down_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected and below nodes in list
            int    idx_sel   = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            int    idx_below = idx_sel + 1;
            size_t cnt       = view->dlg_settings.check_lst_node_tab3.cnt_get();

            GLib.assert(idx_sel   != View.Wrapper.Win.FOUND_NOT);
            GLib.assert(idx_below <  cnt);

            // Get their check states.
            // Swap them.
            bool flag_sel   = view->dlg_settings.check_lst_node_tab3.is_checked(idx_sel);
            bool flag_below = view->dlg_settings.check_lst_node_tab3.is_checked(idx_below);

            view->dlg_settings.check_lst_node_tab3.check(idx_sel,   flag_below);
            view->dlg_settings.check_lst_node_tab3.check(idx_below, flag_sel);

            // Get their values.
            // Swap them.
            string val_sel   = view->dlg_settings.check_lst_node_tab3.val_get(idx_sel);
            string val_below = view->dlg_settings.check_lst_node_tab3.val_get(idx_below);

            view->dlg_settings.check_lst_node_tab3.val_set(idx_sel,   val_below);
            view->dlg_settings.check_lst_node_tab3.val_set(idx_below, val_sel);

            // Get their meta.
            // Swap them.
            string *meta_sel   = view->dlg_settings.check_lst_node_tab3.meta_get(idx_sel);
            string *meta_below = view->dlg_settings.check_lst_node_tab3.meta_get(idx_below);

            view->dlg_settings.check_lst_node_tab3.meta_set(idx_sel,   meta_below);
            view->dlg_settings.check_lst_node_tab3.meta_set(idx_below, meta_sel);

            // Select below node
            //view->dlg_settings.check_lst_node_tab3.sel_set(idx_sel,   false);
            view->dlg_settings.check_lst_node_tab3.sel_set(idx_below, true);

            // Enable/disable Up & Down buttons
            if (idx_sel == 0)
                view->dlg_settings.btn_up_tab3.en(true);
            if (idx_below == cnt - 1)
                view->dlg_settings.btn_down_tab3.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Check list box with nodes source choice select callback.
        //
        // It is placed on the tab 3 panel.
        private void *check_lst_node_tab3_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable "Remove" button
            view->dlg_settings.btn_remove_tab3.en(true);

            // Enable/disable Up & Down buttons
            int    idx = view->dlg_settings.check_lst_node_tab3.sel_elem_get();
            size_t cnt = view->dlg_settings.check_lst_node_tab3.cnt_get();

            GLib.assert(idx != View.Wrapper.Win.FOUND_NOT);

            view->dlg_settings.btn_up_tab3.en(idx != 0);
            view->dlg_settings.btn_down_tab3.en(idx < cnt - 1);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Proxy type choice select callback.
        //
        // It is placed on the tab 3 panel.
        private void *choice_proxy_tab3_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected choice
            uint idx = view->dlg_settings.choice_proxy_tab3.sel_get();

            // Enable/disable host:port controls
            switch (idx)
            {
            // None
            case View.DlgSettings.CHOICE_PROXY_IDX_NONE:
                ctrl->ctrl_changed_by_user = false;
                view->dlg_settings.txt_proxy_host_tab3.clr();
                ctrl->ctrl_changed_by_user = true;
                view->dlg_settings.txt_proxy_host_tab3.en(false);
                view->dlg_settings.spin_proxy_port_tab3.val_set(0);
                view->dlg_settings.spin_proxy_port_tab3.en(false);
                break;

            // HTTP
            case View.DlgSettings.CHOICE_PROXY_IDX_HTTP:
                view->dlg_settings.txt_proxy_host_tab3.en(true);
                view->dlg_settings.spin_proxy_port_tab3.en(true);
                break;

            // SOCKS5
            case View.DlgSettings.CHOICE_PROXY_IDX_SOCKS5:
                view->dlg_settings.txt_proxy_host_tab3.en(true);
                view->dlg_settings.spin_proxy_port_tab3.en(true);
                break;

            // Something else
            default:
                GLib.assert(false);
                break;
            }

            // Enable/disable "Apply" button
            btn_apply_settings_en_set();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Host input text control changed callback.
        //
        // It is placed on the tab 3 panel.
        private void *txt_proxy_host_tab3_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Enable/disable "Apply" button.
            // Unlock mutex.
            model->mutex.lock();
            btn_apply_settings_en_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Audio subsystem device choice select callback.
        //
        // It is placed on the tab 5 panel.
        private void *choice_sys_tab5_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update widgets related to audio. They are located on tab 5.
            // Changing the sound system assumes that input/output card
            // choices should be refilled.
            try
            {
                audio_widgets_tab5_reset(reset_target_t.SETTINGS_CUR);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Capture device choice select callback.
        //
        // It is placed on the tab 5 panel.
        private void *choice_cap_tab5_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected choice
            //uint idx = view->dlg_settings.choice_cap_tab5.sel_get();

            // Nothing to do

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Playback device choice select callback.
        //
        // It is placed on the tab 5 panel.
        private void *choice_playback_tab5_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected choice
            //uint idx = view->dlg_settings.choice_sys_tab5.sel_get();

            // Nothing to do

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Reset" button click callback
        private void *btn_reset_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Pops up the context menu.
            // Unlock mutex.
            //model->mutex.lock(); // Can't lock here because popup window blocks
            view->dlg_settings.btn_reset.menu_popup(view->dlg_settings.menu_reset);
            //model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Reset to last applied settings" menu item click callback.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_reset_last_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Reset all widgets to last saved state.
            // Unlock mutex.
            model->mutex.lock();
            reset(reset_target_t.SETTINGS_APPLIED_LAST);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Reset to program default settings" menu item click callback.
        //
        // It is placed in context menu for "Action" button.
        private void *menu_reset_def_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Reset all widgets to initial state.
            // Unlock mutex.
            model->mutex.lock();
            reset(reset_target_t.SETTINGS_INITIAL);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Enable/disable "Apply" button
        private void btn_apply_settings_en_set()
        {
            bool flag;

            if (view->dlg_settings.choice_proxy_tab3.sel_get() == View.DlgSettings.CHOICE_PROXY_IDX_NONE)
                flag = true;
            else
            {
                int    len     = view->dlg_settings.txt_proxy_host_tab3.val_get().length;
                uint32 max_len = global::ToxCore.max_hostname_length();

                flag = len >  0 &&
                       len <= max_len;
            }

            view->dlg_settings.btn_apply_settings.en(flag);
        }

        /*----------------------------------------------------------------------------*/

        // "Apply" button click callback
        private void *btn_apply_settings_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update common config with values from GUI
            model->cfg.set_boolean("general", "enable_autoload", !view->dlg_settings.check_show_profile_tab1.val_get());

            // Update per-profile config with values from GUI.
            //
            // Get old config values.
            Model.Model.cfg_appearance_lang_t          lang_old;
            Model.Profile.cfg_appearance_avatar_size_t avatar_size_old;
            string[] nodes_old;
            bool[]   en_nodes_old;
            bool     allow_udp_old;
            Model.Profile.cfg_net_proxy_type_t         proxy_type_old;
            string   proxy_host_old;
            uint16   proxy_port_old;
            Model.Profile.cfg_audio_sys_t              audio_sys_old;
            string   cap_dev_old;
            uint     gain_old;
            string   playback_dev_old;
            uint     vol_old;

            try
            {
                lang_old         = (Model.Model.cfg_appearance_lang_t) model->cfg.get_integer("appearance", "lang");
                avatar_size_old  = (Model.Profile.cfg_appearance_avatar_size_t) model->self.cfg.get_integer("appearance", "avatar_size");
                nodes_old        = model->self.cfg.get_string_list("network", "nodes");
                en_nodes_old     = model->self.cfg.get_boolean_list("network", "en_nodes");
                allow_udp_old    = model->self.cfg.get_boolean("network", "allow_udp");
                proxy_type_old   = (Model.Profile.cfg_net_proxy_type_t) model->self.cfg.get_integer("network", "proxy_type");
                proxy_host_old   = model->self.cfg.get_string("network", "proxy_host");
                proxy_port_old   = (uint16) model->self.cfg.get_integer("network", "proxy_port");
                audio_sys_old    = (Model.Profile.cfg_audio_sys_t) model->self.cfg.get_integer("audio", "sys");
                cap_dev_old      = model->self.cfg.get_string("audio", "cap_dev");
                gain_old         = model->self.cfg.get_integer("audio", "gain");
                playback_dev_old = model->self.cfg.get_string("audio", "playback_dev");
                vol_old          = model->self.cfg.get_integer("audio", "vol");
            }
            catch (GLib.KeyFileError ex)
            {
                lang_old         = Model.Model.cfg_appearance_lang_t.DEF;
                avatar_size_old  = Model.Profile.cfg_appearance_avatar_size_t.SMALL;
                nodes_old        = {};
                en_nodes_old     = {};
                allow_udp_old    = true;
                proxy_type_old   = Model.Profile.cfg_net_proxy_type_t.NONE;
                proxy_host_old   = "";
                proxy_port_old   = 0;
                audio_sys_old    = Model.Profile.cfg_audio_sys_t.AUTO;
                cap_dev_old      = "";
                gain_old         = 0;
                playback_dev_old = "";
                vol_old          = 0;

                GLib.assert(false);
            }

            // Update config with values from GUI controls
            model->cfg.set_integer("appearance", "lang", view->dlg_settings.choice_lang_tab2.sel_get());
            bool is_small = view->dlg_settings.radio_avatar_size_16x16_tab2.sel_get();
            model->self.cfg.set_integer("appearance", "avatar_size", is_small ? Model.Profile.cfg_appearance_avatar_size_t.SMALL :
                                                                                Model.Profile.cfg_appearance_avatar_size_t.BIG);

            size_t   cnt          = view->dlg_settings.check_lst_node_tab3.cnt_get();
            string[] nodes_new    = {};
            bool[]   en_nodes_new = {};

            for (uint idx = 0; idx < cnt; idx++)
            {
                nodes_new    += (string *) view->dlg_settings.check_lst_node_tab3.meta_get(idx);
                en_nodes_new +=            view->dlg_settings.check_lst_node_tab3.is_checked(idx);
            }

            uint sel_cap      = view->dlg_settings.choice_cap_tab5.sel_get();
            uint sel_playback = view->dlg_settings.choice_playback_tab5.sel_get();

            model->self.cfg.set_string_list("network", "nodes", nodes_new);
            if (en_nodes_new.length == 0) // set_boolean_list() can't handle empty arrays
                model->self.cfg.set_string("network", "en_nodes", "");
            else
                model->self.cfg.set_boolean_list("network", "en_nodes", en_nodes_new);
            model->self.cfg.set_boolean("network", "allow_udp",                 view->dlg_settings.check_udp_tab3.val_get());
            model->self.cfg.set_integer("network", "proxy_type",                view->dlg_settings.choice_proxy_tab3.sel_get());
            model->self.cfg.set_string ("network", "proxy_host",                view->dlg_settings.txt_proxy_host_tab3.val_get());
            model->self.cfg.set_integer("network", "proxy_port",                view->dlg_settings.spin_proxy_port_tab3.val_get());
            model->self.cfg.set_boolean("privacy", "send_typing_notifications", view->dlg_settings.check_send_typing_tab4.val_get());
            model->self.cfg.set_integer("audio",   "sys",                       view->dlg_settings.choice_sys_tab5.sel_get());
            model->self.cfg.set_string ("audio",   "cap_dev",                   sel_cap == View.DlgSettings.CHOICE_CAP_IDX_AUTO ? "" : view->dlg_settings.choice_cap_tab5.val_get(sel_cap));
            model->self.cfg.set_integer("audio",   "gain",                      view->dlg_settings.slider_gain_tab5.val_get());
            model->self.cfg.set_string ("audio",   "playback_dev",              sel_playback == View.DlgSettings.CHOICE_PLAYBACK_IDX_AUTO ? "" : view->dlg_settings.choice_playback_tab5.val_get(sel_playback));
            model->self.cfg.set_integer("audio",   "vol",                       view->dlg_settings.slider_vol_tab5.val_get());

            // Get new config values
            Model.Model.cfg_appearance_lang_t          lang_new;
            Model.Profile.cfg_appearance_avatar_size_t avatar_size_new;
            bool   allow_udp_new;
            Model.Profile.cfg_net_proxy_type_t         proxy_type_new;
            string proxy_host_new;
            uint16 proxy_port_new;
            Model.Profile.cfg_audio_sys_t              audio_sys_new;
            string cap_dev_new;
            uint   gain_new;
            string playback_dev_new;
            uint   vol_new;

            try
            {
                lang_new         = (Model.Model.cfg_appearance_lang_t) model->cfg.get_integer("appearance", "lang");
                avatar_size_new  = (Model.Profile.cfg_appearance_avatar_size_t) model->self.cfg.get_integer("appearance", "avatar_size");
                allow_udp_new    = model->self.cfg.get_boolean("network", "allow_udp");
                proxy_type_new   = (Model.Profile.cfg_net_proxy_type_t) model->self.cfg.get_integer("network", "proxy_type");
                proxy_host_new   = model->self.cfg.get_string("network", "proxy_host");
                proxy_port_new   = (uint16) model->self.cfg.get_integer("network", "proxy_port");
                audio_sys_new    = (Model.Profile.cfg_audio_sys_t) model->self.cfg.get_integer("audio", "sys");
                cap_dev_new      = model->self.cfg.get_string("audio", "cap_dev");
                gain_new         = model->self.cfg.get_integer("audio", "gain");
                playback_dev_new = model->self.cfg.get_string("audio", "playback_dev");
                vol_new          = model->self.cfg.get_integer("audio", "vol");
            }
            catch (GLib.KeyFileError ex)
            {
                lang_new         = Model.Model.cfg_appearance_lang_t.DEF;
                avatar_size_new  = Model.Profile.cfg_appearance_avatar_size_t.SMALL;
                allow_udp_new    = true;
                proxy_type_new   = Model.Profile.cfg_net_proxy_type_t.NONE;
                proxy_host_new   = "";
                proxy_port_new   = 0;
                audio_sys_new    = Model.Profile.cfg_audio_sys_t.AUTO;
                cap_dev_new      = "";
                gain_new         = 0;
                playback_dev_new = "";
                vol_new          = 0;

                GLib.assert(false);
            }

            bool upd_bootstrap_nodes = false;

            for (uint idx = 0; idx < nodes_old.length; idx++)
                if (nodes_old[idx] != nodes_new[idx])
                {
                    upd_bootstrap_nodes = true;
                    break;
                }

            if (!upd_bootstrap_nodes)
            {
                for (uint idx = 0; idx < en_nodes_old.length; idx++)
                    if (en_nodes_old[idx] != en_nodes_new[idx])
                    {
                        upd_bootstrap_nodes = true;
                        break;
                    }
            }


            // Reinitialize some parts of the application?
            // (Tox instance, buddy list, etc.)
            Ctrl.reinit_target_t target = Ctrl.reinit_target_t.NONE;

            if (lang_new != lang_old)
                target |= Ctrl.reinit_target_t.NEW_LANG;
            if (avatar_size_new != avatar_size_old)
                target |= Ctrl.reinit_target_t.NEW_AVATAR_SIZE;
            if (upd_bootstrap_nodes)
                target |= Ctrl.reinit_target_t.NEW_BOOTSTRAP_NODES;
            if (allow_udp_new != allow_udp_old)
                target |= Ctrl.reinit_target_t.NEW_COMM_SETTINGS;
            if (proxy_type_new != proxy_type_old ||
                    proxy_host_new != proxy_host_old ||
                    proxy_port_new != proxy_port_old)
                target |= Ctrl.reinit_target_t.NEW_PROXY_SETTINGS;
            if (audio_sys_new != audio_sys_old ||
                    cap_dev_new      != cap_dev_old      ||
                    gain_new         != gain_old         ||
                    playback_dev_new != playback_dev_old ||
                    vol_new          != vol_old)
                target |= Ctrl.reinit_target_t.NEW_AUDIO_SETTINGS;

            // Apply new settings
            try
            {
                // Reinitialize profile
                ctrl->profile_reinit(target);
            }
            catch (GLib.Error ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(ex.message);
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg.modal_show();

                // Lock mutex
                model->mutex.lock();
            }

            // Apply new settings (for plugins)
            foreach (unowned Peas.PluginInfo info in model->plugins_ext.engine.get_plugin_list())
            {
                // Each plugin...
                bool err_ = false;

                try
                {
                    // Plugin description fields
                    unowned string  filename   = info.get_module_name();
                    bool            en_plugin  = model->self.cfg.get_boolean("plugins", filename);
                    if (!en_plugin) // Is this plugin enabled?
                        continue;
                    Model.Plugin     plugin          = model->plugins_ext.get_extension(info) as Model.Plugin;
                    unowned string?  reload_settings = info.get_external_data("ReloadSettings");
                            string[] target_arr      = reload_settings.split(";");

                    // Reinitialize the plugin
                    Ctrl.reinit_target_t target_;

                    foreach (string target_str in target_arr)
                    {
                        unowned string target_str_ = target_str._strip();

                        switch (target_str_)
                        {
                        case "Language":
                            target_ = Ctrl.reinit_target_t.NEW_LANG;
                            break;
                        case "Avatar":
                            target_ = Ctrl.reinit_target_t.NEW_AVATAR_SIZE;
                            break;
                        case "Bootstrap Nodes":
                            target_ = Ctrl.reinit_target_t.NEW_BOOTSTRAP_NODES;
                            break;
                        case "Communication":
                            target_ = Ctrl.reinit_target_t.NEW_COMM_SETTINGS;
                            break;
                        case "Proxy":
                            target_ = Ctrl.reinit_target_t.NEW_PROXY_SETTINGS;
                            break;
                        case "Audio":
                            target_ = Ctrl.reinit_target_t.NEW_AUDIO_SETTINGS;
                            break;
                        default:
                            target_ = Ctrl.reinit_target_t.NONE;
                            break;
                        }

                        if ((target & target_) > 0)
                        {
                            plugin.plugin_unload();
                            plugin.plugin_load();
                        }
                    }
                }
                catch (GLib.Error ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    if (!err_)
                    {
                        GLib.debug(ex.message);
                        var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                        msg_dlg.modal_show();
                        err_ = true;
                    }

                    // Lock mutex
                    model->mutex.lock();
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Settings" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgSettings()
        {
            // Connect signals to callback methods
            this.choice_node_tab3_selected_sig.connect(choice_node_tab3_selected);
            this.txt_url_tab3_changed_sig.connect(txt_url_tab3_changed);
            this.file_tab3_changed_sig.connect(file_tab3_changed);
            this.txt_buddy_host_tab3_changed_sig.connect(txt_buddy_host_tab3_changed);
            this.btn_add_tab3_clicked_sig.connect(btn_add_tab3_clicked);
            this.btn_remove_tab3_clicked_sig.connect(btn_remove_tab3_clicked);
            this.btn_up_tab3_clicked_sig.connect(btn_up_tab3_clicked);
            this.btn_down_tab3_clicked_sig.connect(btn_down_tab3_clicked);
            this.check_lst_node_tab3_selected_sig.connect(check_lst_node_tab3_selected);
            this.choice_proxy_tab3_selected_sig.connect(choice_proxy_tab3_selected);
            this.txt_proxy_host_tab3_changed_sig.connect(txt_proxy_host_tab3_changed);
            this.choice_sys_tab5_selected_sig.connect(choice_sys_tab5_selected);
            this.choice_cap_tab5_selected_sig.connect(choice_cap_tab5_selected);
            this.choice_playback_tab5_selected_sig.connect(choice_playback_tab5_selected);
            this.btn_reset_clicked_sig.connect(btn_reset_clicked);
            this.menu_reset_last_clicked_sig.connect(menu_reset_last_clicked);
            this.menu_reset_def_clicked_sig.connect(menu_reset_def_clicked);
            this.btn_apply_settings_clicked_sig.connect(btn_apply_settings_clicked);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Reset all widgets to last saved or initial state
         *
         * @param target How to reset
         */
        public void reset(reset_target_t target)
        {
            // Reset all widgets
            try
            {
                // First, reset or update those widgets that look the same in
                // both states, last applied and program initial, no matter
                // which button user has clicked.
                //
                // Tab 3.
                //
                // Create temporary buddy list array.
                // Sort it by display name.
                var buddies_lst = new Gee.ArrayList<Model.Buddy>();
                foreach (Model.Buddy buddy in model->buddies_hash.values)
                    buddies_lst.add(buddy);

                buddies_lst.sort((a, b) =>
                    {
                        string a_display_name = a.display_name_get();
                        string b_display_name = b.display_name_get();

                        if      (a_display_name >  b_display_name) return  1;
                        else if (a_display_name == b_display_name) return  0;
                        else                                       return -1;
                    }
                );

                // Fill buddy choice with the prepared above array
                view->dlg_settings.choice_buddy_tab3.clr();
                foreach (Model.Buddy buddy in buddies_lst)
                    view->dlg_settings.choice_buddy_tab3.append(buddy.display_name_get());

                // Enable/disable buddy widgets
                bool have_buddies = view->dlg_settings.choice_buddy_tab3.cnt_get() > 0;

                view->dlg_settings.choice_buddy_tab3.en(have_buddies);
                view->dlg_settings.txt_buddy_host_tab3.en(have_buddies);
                view->dlg_settings.spin_buddy_port_tab3.en(have_buddies);

                // Clear check list box with nodes.
                // (It will filled with new values in this method later.)
                size_t cnt = view->dlg_settings.check_lst_node_tab3.cnt_get();

                for (uint idx = 0; idx < cnt; idx++)
                {
                    string *meta = view->dlg_settings.check_lst_node_tab3.meta_get(idx);
                    delete meta;
                }

                view->dlg_settings.check_lst_node_tab3.clr();

                // Tab 5
                audio_widgets_tab5_reset(target);

                // Second, reset remaining widgets.
                // They look different in last applied and initial state.
                //
                // Last applied or initial state?
                if (target == reset_target_t.SETTINGS_APPLIED_LAST)
                {
                    // Tab 1
                    view->dlg_settings.check_show_profile_tab1.val_set(!model->cfg.get_boolean("general", "enable_autoload"));

                    // Tab 2
                    Model.Model.cfg_appearance_lang_t lang =
                        (Model.Model.cfg_appearance_lang_t) model->cfg.get_integer("appearance", "lang");
                    if (lang >= Model.Model.cfg_appearance_lang_t.SIZE)
                        lang = Model.Model.cfg_appearance_lang_t.DEF;
                    view->dlg_settings.choice_lang_tab2.sel_set((int) lang);

                    bool is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                        Model.Profile.cfg_appearance_avatar_size_t.SMALL;
                    if (is_small) view->dlg_settings.radio_avatar_size_16x16_tab2.sel_set(true);
                    else          view->dlg_settings.radio_avatar_size_32x32_tab2.sel_set(true);

                    // Tab 3
                    view->dlg_settings.choice_node_tab3.sel_set(View.DlgSettings.CHOICE_NODE_IDX_URL);
                    ctrl->ctrl_changed_by_user = false;
                    view->dlg_settings.txt_url_tab3.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->dlg_settings.txt_url_tab3.show(true);
                    view->dlg_settings.file_tab3.show(false);
                    view->dlg_settings.choice_buddy_tab3.show(false);
                    view->dlg_settings.lbl_buddy_host_tab3.show(false);
                    view->dlg_settings.txt_buddy_host_tab3.show(false);
                    view->dlg_settings.lbl_buddy_port_tab3.show(false);
                    view->dlg_settings.spin_buddy_port_tab3.show(false);
                    view->dlg_settings.btn_add_tab3.en(false);
                    view->dlg_settings.btn_remove_tab3.en(false);
                    view->dlg_settings.btn_up_tab3.en(false);
                    view->dlg_settings.btn_down_tab3.en(false);
                    view->dlg_settings.panel_tab3.fit();

                    string[] nodes    = model->self.cfg.get_string_list("network", "nodes");
                    bool[]   en_nodes = model->self.cfg.get_boolean_list("network", "en_nodes");
                    size_t   cnt_     = nodes.length <= en_nodes.length ? nodes.length : en_nodes.length;

                    for (uint idx = 0; idx < cnt_; idx++)
                    {
                        try
                        {
                            var regex = new GLib.Regex("^(.+?): (.+)\\z");
                            GLib.MatchInfo info;
                            bool res = regex.match(nodes[idx], 0, out info);
                            if (!res)
                                throw new Model.ErrGeneral.BAD_NODE_FMT(_("Bad node format: %s").printf(nodes[idx]));
                            string type = info.fetch(1);
                            if (type != "URL" &&
                                    type != "File" &&
                                    type != "Buddy")
                                throw new Model.ErrGeneral.BAD_NODE_TYPE(_("Bad node type: %s").printf(type));
                            string  data = info.fetch(2);
                            string  node = _(type) + ": " + data;
                            string *meta =   type  + ": " + data;

                            view->dlg_settings.check_lst_node_tab3.append(node);
                            view->dlg_settings.check_lst_node_tab3.meta_set(idx, meta);
                            if (en_nodes[idx])
                                view->dlg_settings.check_lst_node_tab3.check(idx, true);
                        }
                        catch (RegexError ex)
                        {
                            GLib.assert(false);
                        }
                        catch (Model.ErrGeneral ex)
                        {
                            GLib.debug(ex.message);
                        }
                    }

                    view->dlg_settings.check_udp_tab3.val_set(model->self.cfg.get_boolean("network", "allow_udp"));

                    uint proxy_type = (uint) model->self.cfg.get_integer("network", "proxy_type");
                    if (proxy_type > View.DlgSettings.CHOICE_PROXY_IDX_SOCKS5)
                        proxy_type = View.DlgSettings.CHOICE_PROXY_IDX_SOCKS5;

                    view->dlg_settings.choice_proxy_tab3.sel_set(proxy_type);
                    bool flag = proxy_type != View.DlgSettings.CHOICE_PROXY_IDX_NONE;
                    view->dlg_settings.txt_proxy_host_tab3.en(flag);
                    view->dlg_settings.spin_proxy_port_tab3.en(flag);

                    if (flag)
                    {
                        ctrl->ctrl_changed_by_user = false;
                        view->dlg_settings.txt_proxy_host_tab3.val_set (model->self.cfg.get_string ("network", "proxy_host"));
                        ctrl->ctrl_changed_by_user = true;
                        view->dlg_settings.spin_proxy_port_tab3.val_set(model->self.cfg.get_integer("network", "proxy_port"));
                    }
                    else
                    {
                        ctrl->ctrl_changed_by_user = false;
                        view->dlg_settings.txt_proxy_host_tab3.clr();
                        ctrl->ctrl_changed_by_user = true;
                        view->dlg_settings.spin_proxy_port_tab3.val_set(0);
                    }

                    btn_apply_settings_en_set(); // Enable/disable "Apply" button

                    // Tab 4
                    view->dlg_settings.check_send_typing_tab4.val_set(model->self.cfg.get_boolean("privacy", "send_typing_notifications"));

                    // Tab 5.
                    //
                    // Do nothing. All related to tab 5 code had been executed
                    // above in audio_widgets_tab5_reset() method.
                }
                else if (target == reset_target_t.SETTINGS_INITIAL)
                {
                    // Tab 1
                    view->dlg_settings.check_show_profile_tab1.val_set(true);

                    // Tab 2
                    view->dlg_settings.choice_lang_tab2.sel_set((int) Model.Model.cfg_appearance_lang_t.DEF);
                    view->dlg_settings.radio_avatar_size_32x32_tab2.sel_set(true);

                    // Tab 3
                    view->dlg_settings.choice_node_tab3.sel_set(View.DlgSettings.CHOICE_NODE_IDX_URL);
                    ctrl->ctrl_changed_by_user = false;
                    view->dlg_settings.txt_url_tab3.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->dlg_settings.txt_url_tab3.show(true);
                    view->dlg_settings.file_tab3.show(false);
                    view->dlg_settings.choice_buddy_tab3.show(false);
                    view->dlg_settings.lbl_buddy_host_tab3.show(false);
                    view->dlg_settings.txt_buddy_host_tab3.show(false);
                    view->dlg_settings.lbl_buddy_port_tab3.show(false);
                    view->dlg_settings.spin_buddy_port_tab3.show(false);
                    view->dlg_settings.btn_add_tab3.en(false);
                    view->dlg_settings.btn_remove_tab3.en(false);
                    view->dlg_settings.btn_up_tab3.en(false);
                    view->dlg_settings.btn_down_tab3.en(false);
                    view->dlg_settings.panel_tab3.fit();

                    string  node0 = _("URL") + ": " + Model.Model.NODES_URL;
                    string *meta0 = "URL: %s".printf(Model.Model.NODES_URL);

                    view->dlg_settings.check_lst_node_tab3.append(node0);
                    view->dlg_settings.check_lst_node_tab3.meta_set(0, meta0);
                    view->dlg_settings.check_lst_node_tab3.check(0, false);

                    string  node1 = _("URL") + ": " + Model.Model.NODES_URL_ALT;
                    string *meta1 = "URL: %s".printf(Model.Model.NODES_URL_ALT);

                    view->dlg_settings.check_lst_node_tab3.append(node1);
                    view->dlg_settings.check_lst_node_tab3.meta_set(1, meta1);
                    view->dlg_settings.check_lst_node_tab3.check(1, true);

                    string  node2 = _("File") + ": " + Model.Model.NODES_FILE;
                    string *meta2 = "File: %s".printf(Model.Model.NODES_FILE);

                    view->dlg_settings.check_lst_node_tab3.append(node2);
                    view->dlg_settings.check_lst_node_tab3.meta_set(2, meta2);
                    view->dlg_settings.check_lst_node_tab3.check(2, true);

                    view->dlg_settings.check_udp_tab3.val_set(true);
                    view->dlg_settings.choice_proxy_tab3.sel_set(View.DlgSettings.CHOICE_PROXY_IDX_NONE);
                    ctrl->ctrl_changed_by_user = false;
                    view->dlg_settings.txt_proxy_host_tab3.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->dlg_settings.txt_proxy_host_tab3.en(false);
                    view->dlg_settings.spin_proxy_port_tab3.val_set(0);
                    view->dlg_settings.spin_proxy_port_tab3.en(false);
                    view->dlg_settings.btn_apply_settings.en(true);

                    // Tab 4
                    view->dlg_settings.check_send_typing_tab4.val_set(true);

                    // Tab 5.
                    //
                    // Do nothing. All related to tab 5 code had been executed
                    // above in audio_widgets_tab5_reset() method.
                }
                //else if (target == reset_target_t.SETTINGS_CUR)
                else
                {
                    GLib.assert(false);
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }
    }
}
