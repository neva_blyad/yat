/*
 *    dlg_edit_buddy.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Edit Buddy" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgEditBuddy : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Alias input text control changed signal
         */
        public signal void *txt_Alias_changed_sig(void *fn, void *param, void *event);

        /**
         * "Edit" button click signal
         */
        public signal void *btn_edit_buddy_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Alias input text control changed callback
        private void *txt_Alias_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Edit" button
            Model.Buddy buddy = ctrl->handled_contacts.dlg_edit_buddy.buddy;

            string Alias = view->dlg_edit_buddy.txt_Alias.val_get();
            view->dlg_edit_buddy.btn_edit_buddy.en((buddy.Alias == null && Alias != "") ||
                                                   (buddy.Alias != null && Alias != buddy.Alias));

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Edit" button click callback
        private void *btn_edit_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update a buddy Alias
            Model.Buddy buddy = ctrl->handled_contacts.dlg_edit_buddy.buddy;

            string Alias = view->dlg_edit_buddy.txt_Alias.val_get();
            buddy.Alias = Alias == "" ? null :
                                        Alias;

            // Update the buddy list
            ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                                   buddy,

                                                   true,  // Set display name column
                                                   false, // Don't set status column
                                                   false, // Don't state column
                                                   false  // Don't set user state column
                                                  );

            // Update member list
            Model.Conference? conference = model->peer_conference;

            if (conference != null)
                foreach (Model.Member member in conference.members_hash.values)
                    if (member.person == buddy)
                    {
                        ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                    member,

                                                                    true, // Set display name column
                                                                    false // Don't set user state column
                                                                   );
                        break;
                    }

            // Close "Edit Buddy" dialog
            view->dlg_edit_buddy.show(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Edit Buddy" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgEditBuddy()
        {
            // Connect signals to callback methods
            this.txt_Alias_changed_sig.connect(txt_Alias_changed);
            this.btn_edit_buddy_clicked_sig.connect(btn_edit_buddy_clicked);
        }
    }
}

