/*
 *    ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Ctrl object
 */
Ctrl.Ctrl *ctrl;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * General errors
     */
    public errordomain ErrGeneral
    {
        /**
         * Autoload profile name from configuration file is not found
         * in user data directory
         */
        DOES_NOT_EXIST_AUTOLOAD_PROFILE_NAME,

        /**
         * Unknown buddy
         */
        BUDDY_NOT_FOUND_ERR,
    }

    /**
     * This is the Controller class
     */
    [SingleInstance]
    public class Ctrl : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
          * CLI arguments
          */
        public string[] args { public get; public set; }

        /**
         * Tox callbacks
         */
        public ToxCore tox_core { public get; public set; }

        /**
         * Tox AV callbacks
         */
        public ToxCoreAV tox_core_av { public get; public set; }

        /**
         * "Choose Profile" dialog callbacks
         */
        public DlgProfile dlg_profile { public get; public set; }

        /**
         * Main window frame callbacks
         */
        public FrmMain frm_main { public get; public set; }

        /**
         * "About Profile" / "About Buddy" / "About Conference" dialog
         * callbacks
         */
        public DlgAbout dlg_about { public get; public set; }

        /**
         * "Edit Profile" dialog callbacks
         */
        public DlgEditProfile dlg_edit_profile { public get; public set; }

        /**
         * "Add Buddy" dialog callbacks
         */
        public DlgAddBuddy dlg_add_buddy { public get; public set; }

        /**
         * "Edit Buddy" dialog callbacks
         */
        public DlgEditBuddy dlg_edit_buddy { public get; public set; }

        /**
         * "Conversation History" dialog callbacks
         */
        public FrmConv frm_conv { public get; public set; }

        /**
         * "Add Conference" dialog callbacks
         */
        public DlgAddConference dlg_add_conference { public get; public set; }

        /**
         * "Edit Conference" dialog callbacks
         */
        public DlgEditConference dlg_edit_conference { public get; public set; }

        /**
         * "Settings" dialog callbacks
         */
        public DlgSettings dlg_settings { public get; public set; }

        /**
         * "In Loving Memory of" dialog callbacks
         */
        public DlgVika dlg_Vika { public get; public set; }

        /**
         * "Plugins" dialog callbacks
         */
        public DlgPlugins dlg_plugins { public get; public set; }

        /**
         * Control is changed by us or by user.
         * (Input text control value changing, list control selection,
         * etc.)
         */
        public bool ctrl_changed_by_user { public get; public set construct; }

        /**
         * Currently handled buddies and conferences. Used in some
         * frames and dialogs, not in all.
         *
         * Note that the currently handled and peers are not the same.
         * For example, user can open "Edit Buddy" dialog with one
         * buddy, then select another peer on the background buddy list
         * in main window frame.
         */
        public HandledContacts handled_contacts { public get; public set construct; }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Currently handled buddies and conferences
         */
        public class HandledContacts
        {
            // "Edit Buddy" dialog
            public struct dlg_edit_buddy_t
            {
                Model.Buddy? buddy;
            }

            // "Edit Conference" dialog
            public struct dlg_edit_conference_t
            {
                Model.Conference? conference;
            }

            // "Conversation History" frame
            public struct frm_conv_t
            {
                Model.Buddy?      buddy;
                Model.Conference? conference;
            }

            public dlg_edit_buddy_t      dlg_edit_buddy;
            public dlg_edit_conference_t dlg_edit_conference;
            public frm_conv_t            frm_conv;
        }

        // profile_reinit() parameter
        [Flags]
        public enum reinit_target_t
        {
            NONE                = 0,
            NEW_LANG            = 1 << 0,
            NEW_AVATAR_SIZE     = 1 << 1,
            NEW_BOOTSTRAP_NODES = 1 << 2,
            NEW_COMM_SETTINGS   = 1 << 3,
            NEW_PROXY_SETTINGS  = 1 << 4,
            NEW_AUDIO_SETTINGS  = 1 << 5,
        }

        /**
         * Action parameter for methods:
         * - buddy_lst_change(),
         * - conference_lst_change(),
         * - invite_lst_change().
         *
         * It changes the buddy/conference/invite list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding objects, hashes and arrays.
         */
        public enum action_t
        {
            /**
              * Add element
              */
            ADD_ELEM,

            /**
              * Edit element
              */
            EDIT_ELEM,

            /**
              * Remove element
              */
            REMOVE_ELEM,

            /**
              * Update list
              */
            UPD_LST,
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize callbacks
        private void cb_init()
        {
            // Tox callbacks
            this.tox_core            = new ToxCore();
            this.tox_core_av         = new ToxCoreAV();

            // Frame and dialog UI callbacks
            this.dlg_profile         = new DlgProfile();
            this.frm_main            = new FrmMain();
            this.dlg_about           = new DlgAbout();
            this.dlg_edit_profile    = new DlgEditProfile();
            this.dlg_add_buddy       = new DlgAddBuddy();
            this.dlg_edit_buddy      = new DlgEditBuddy();
            this.frm_conv            = new FrmConv();
            this.dlg_add_conference  = new DlgAddConference();
            this.dlg_edit_conference = new DlgEditConference();
            this.dlg_settings        = new DlgSettings();
            this.dlg_Vika            = new DlgVika();
            this.dlg_plugins         = new DlgPlugins();
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Controller constructor.
         *
         * Initializes callbacks.
         *
         * @param args CLI arguments
         */
        public Ctrl(string[] args)
        {
            // GObject-style construction
            GLib.Object(ctrl_changed_by_user : true,
                        handled_contacts     : new HandledContacts());

            // Remember the program arguments
            this.args = args;

            // Initialize callbacks
            cb_init();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Controller destructor
         */
        ~Ctrl()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Start Controller logic
         */
        public void execute()
        {
            // Initialize threads.
            // Start threads.
            // Wait until threads finish.
            model->threads_init (Model.Model.thread_t.UI);
            model->threads_start(Model.Model.thread_t.UI);
            model->threads_wait (Model.Model.thread_t.ALL);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Save all data on disk and shutdown
         */
        public void exit()
        {
            // Do some action before we exit.
            // Profile, config, database should be saved on disk.
            //
            // Check whether profile is not initialized yet
            if (model->self == null)
            {
                // Profile is not initialized yet.
                // This means we are in "Choose Profile" dialog.
                //
                // Save configuration settings.
                // Only common config should be saved because no profile is
                // loaded yet.
                try
                {
                    // Save profile-independent application settings to
                    // configuration file
                    model->cfg_save();
                }
                catch (GLib.FileError ex)
                {
                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();
                }

                // Stop threads
                model->threads_stop(Model.Model.thread_t.UI);
            }
            else
            {
                // Profile is already initialized.
                // This means we are in main window frame.
                //
                // Lock mutex.
                model->mutex.lock();

                // Do plugins termination
                string? err = model->plugins_unload();

                // Show only the first unload error if it'd occured
                if (err != null)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, err, _("Error"));
                    msg_dlg.modal_show();

                    // Lock mutex
                    model->mutex.lock();
                }

                // Does we have an A/V call with somebody?
                //
                // Prepare temporary hash with all peers.
                var peers = new Gee.HashMap<uint32, Model.Buddy>();
                foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                    peers[peer.uid] = peer;
                foreach (Model.Buddy peer in model->peer_buddies_video.values)
                    peers[peer.uid] = peer;

                // Each buddy should be notified that we do not type him a
                // message
                foreach (Model.Buddy buddy in model->buddies_hash.values)
                {
                    // Check whether we are typing a message
                    if ((buddy.action_from_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                    {
                        // Typing was started.
                        // Stop timer that should turn it off.
                        GLib.Source.remove(buddy.timer_typing_off);
                    }

                    // Set that we stop typing a message
                    buddy.typing_change(model,
                                        false // Stop typing
                                       );
                }

                // Each A/V session should be shutdowned
                foreach (Model.Buddy peer in peers.values)
                {
                    // Cancel the A/V call
                    ctrl->frm_main.av_call_cancel(peer,
                                                  true, // Hang up
                                                  true, // Update GUI
                                                  true  // Mutex is locked
                                                 );
                }

                // First, profile is gonna to be saved on disk
                try
                {
                    // Encrypt and save profile from memory on disk
                    if (model->self.profile_name != null // Do not save one-time sessions
                       )
                    {
                        model->self.save(model);
                    }
                }
                catch (GLib.Error ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    // Lock mutex
                    model->mutex.lock();
                }

                // Save configuration settings.
                //
                // Save common config.
                try
                {
                    // Save profile-independent application settings to
                    // configuration file
                    model->cfg_save();
                }
                catch (GLib.FileError ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    // Lock mutex
                    model->mutex.lock();
                }

                // Save profile config to disk
                try
                {
                    // Update per-profile config with window size.
                    //
                    // Note: normally, we should use the code below in resize
                    //       method. But that method breaks GUI looking under GTK,
                    //       so I have to disable it =(
                    if (model->self.profile_name != null // Do not save one-time sessions
                       )
                    {
                        size_t width;
                        size_t height;

                        view->frm_main.size_get(out width, out height);

                        if (width  != this.frm_main.width ||
                            height != this.frm_main.height)
                        {
                            model->self.cfg.set_boolean("main", "use_win_size", true);
                            model->self.cfg.set_integer("main", "width", (int) width);
                            model->self.cfg.set_integer("main", "height", (int) height);
                        }

                        // Update per-profile config with sash positions
                        size_t pos = view->frm_main.splitter_vertical.sash_get();
                        model->self.cfg.set_integer("main", "sash_pos", (int) pos);

                        if (!this.frm_main.page_conferences_opened_first_time) // GUI hack. Sash position is wrong without this condition.
                        {
                            pos = view->frm_main.splitter_vertical_tab1.sash_get();
                            model->self.cfg.set_integer("main", "sash_tab1_pos", (int) pos);
                        }

                        // Save profile-dependent application settings to
                        // configuration file
                        model->self.cfg_save(model);
                    }
                }
                catch (GLib.Error ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    // Lock mutex
                    model->mutex.lock();
                }

                // Next, database is gonna to be saved on disk
                try
                {
                    // Update profile in database table "profile"
                    try
                    {
                        if (model->self.changed_avatar)
                            model->self.db_profile_upd(model->self.avatar_16x16,
                                                       model->self.avatar_32x32,
                                                       model->self.avatar_normal);
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // Create temporary buddy list array.
                    // Sort it by unique identifiers used by libtoxcore.
                    var buddies_lst = new Gee.ArrayList<Model.Buddy>();
                    foreach (Model.Buddy buddy in model->buddies_hash.values)
                        buddies_lst.add(buddy);

                    buddies_lst.sort((a, b) =>
                        {
                            if      (a.uid >  b.uid) {                     return  1; }
                            else if (a.uid == b.uid) { GLib.assert(false); return  0; }
                            else                     {                     return -1; }
                        }
                    );

                    // Each buddy should be updated in database: new UIDs, new
                    // Aliases, new avatars, etc.
                    uint32 uid = 0;

                    foreach (Model.Buddy buddy in buddies_lst)
                    {
                        // Update buddy last seen
                        if (buddy.state == Model.Person.state_t.ONLINE_TCP ||
                            buddy.state == Model.Person.state_t.ONLINE_UDP)
                        {
                            buddy.datetime_seen_last = new GLib.DateTime.now_local();
                        }

                        // Update buddy in database table "buddies"
                        try
                        {
                            if (buddy.changed_avatar)
                            {
                                model->self.db_buddies_upd(buddy.uid,
                                                           uid,
                                                           buddy.addr,
                                                           buddy.Alias,
                                                           buddy.have_unread_msg,
                                                           buddy.datetime_seen_last,
                                                           true, // Change avatar cells
                                                           buddy.avatar_16x16,
                                                           buddy.avatar_32x32,
                                                           buddy.avatar_normal);
                            }
                            else
                            {
                                model->self.db_buddies_upd(buddy.uid,
                                                           uid,
                                                           buddy.addr,
                                                           buddy.Alias,
                                                           buddy.have_unread_msg,
                                                           buddy.datetime_seen_last,
                                                           false, // Don't change avatar cells
                                                           null,  // Don't change avatar cells
                                                           null,  // Don't change avatar cells
                                                           null   // Don't change avatar cells
                                                          );
                            }
                        }
                        catch (Model.ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }

                        uid++;
                    }

                    // Create temporary conference list array.
                    // Sort it by unique identifiers used by libtoxcore.
                    var conferences_lst = new Gee.ArrayList<Model.Conference>();
                    foreach (Model.Conference conference in model->conferences_hash.values)
                        conferences_lst.add(conference);

                    conferences_lst.sort((a, b) =>
                        {
                            if      (a.uid >  b.uid) {                     return  1; }
                            else if (a.uid == b.uid) { GLib.assert(false); return  0; }
                            else                     {                     return -1; }
                        }
                    );

                    // Each conference should be updated in database: new UIDs, new
                    // Aliases, etc.
                    uid = 0;

                    foreach (Model.Conference conference in conferences_lst)
                    {
                        // Update conference in database table "conferences"
                        try
                        {
                            model->self.db_conferences_upd(conference.uid,
                                                           uid,
                                                           conference.Alias,
                                                           conference.have_unread_msg);
                        }
                        catch (Model.ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }

                        uid++;
                    }

                    // Encrypt and save database from memory on disk
                    if (model->self.profile_name != null // Do not save one-time sessions
                       )
                    {
                        model->self.db_save(model);
                    }
                }
                catch (GLib.Error ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    // Lock mutex
                    model->mutex.lock();
                }

                // Stop threads
                model->threads_stop(Model.Model.thread_t.ALL);

                // No Tox iteration is needed.
                // No Tox A/V iteration is needed.
                model->tox_need_iteration    = false;
                model->tox_av_need_iteration = false;

                // Unlock mutex
                model->mutex.unlock();
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This method is to be called when all get loaded and starts
         */
        public void gui_setup()
        {
            // Setup the "Choose Profile" dialog widgets
            if (model->profiles.size > 0)
            {
                // At least one profile already exists.
                // Add profiles on tab 2.
                view->dlg_profile.choice_name_tab2.en(true);
                foreach (Model.Profile profile in model->profiles)
                    view->dlg_profile.choice_name_tab2.append(profile.profile_name);
                view->dlg_profile.choice_name_tab2.sel_set(0);
                view->dlg_profile.menu_rename_profile.en(true);
                view->dlg_profile.menu_del_profile.en(true);
                view->dlg_profile.menu_export_yat.en(true);
                view->dlg_profile.menu_export_another.en(true);
                view->dlg_profile.btn_load_tab2.en(true);

                // Hide tab 2 password controls if needed
                if (!model->profiles[0].use_pwd)
                {
                    view->dlg_profile.show(true); // Hack. Input control size is wrong else.
                    view->dlg_profile.lbl_pwd_tab2.show(false);
                    view->dlg_profile.txt_pwd_tab2.show(false);
                    view->dlg_profile.show(false); // Hack
                }
            }
            else
            {
                // No profiles exists yet.
                // Hide tab 2 password controls.
                view->dlg_profile.show(true); // Hack. Input control size is wrong else.
                view->dlg_profile.lbl_pwd_tab2.show(false);
                view->dlg_profile.txt_pwd_tab2.show(false);
                view->dlg_profile.show(false); // Hack
            }

            // Setup the "Add Buddy" dialog widgets
            view->dlg_add_buddy.txt_addr.max_len_set(Model.Person.addr_size);

            // Setup the "In Loving Memory of" dialog widgets
            view->dlg_Vika.show(true); // Ugly hack. Doesn't fit properly without this shit.
            view->dlg_Vika.img_avatar.img_set(
                GLib.Path.build_filename("img", "VikaBeseda.gif"),
                null // Avatar filepath is to be used, do not use avatar blob
            );
            view->dlg_Vika.fit();
            view->dlg_Vika.show(false);

            // Setup the main window widgets
            size_t widths[] = { View.FrmMain.STATUS_WIDTH_DUMMY,       // First column is unused. I set it just to hide helps from menu bar.
                                View.FrmMain.STATUS_WIDTH_BUDDIES,     // Number of all and currently online buddies
                                View.FrmMain.STATUS_WIDTH_CONFERENCES, // Number of conferences
                                View.FrmMain.STATUS_WIDTH_EVT          // Event
                              };
            view->frm_main.status.fields_set(widths);

            // Read autoload settings from config
            bool   en_autoload;
            string autoload_profile_name;

            try
            {
                en_autoload           = model->cfg.get_boolean("general", "enable_autoload");
                autoload_profile_name = model->cfg.get_string ("general", "autoload_profile_name");
            }
            catch (GLib.KeyFileError ex)
            {
                en_autoload           = false;
                autoload_profile_name = "";
                GLib.assert(false);
            }

            // Profile autoloading
            if (en_autoload)
            {
                // Autoloading is enabled
                Model.Profile? profile;

                try
                {
                    // One-time session should be used?
                    bool is_new_profile;

                    if (autoload_profile_name == "")
                    {
                        // Using one-time session.
                        // Create profile.
                        profile = new Model.Profile(null, // No profile name
                                                    false // No password is used (memory-only storage)
                                                   );
                        is_new_profile = true;

                        // Add new profile
                        model->profiles.add(profile);
                    }
                    else
                    {
                        // Using one of the saved on disk profiles.
                        // Find profile that should be loaded automatically.
                        profile        = null;
                        is_new_profile = false;

                        foreach (Model.Profile tmp in model->profiles)
                            if (tmp.profile_name == autoload_profile_name)
                            {
                                profile = tmp;
                                break;
                            }

                        // Not found case handling
                        if (profile == null)
                            throw new ErrGeneral.DOES_NOT_EXIST_AUTOLOAD_PROFILE_NAME(_("Autoload profile %s from configuration file " +
                                                                                        "is not found in user data directory %s").printf(autoload_profile_name, model->path_data));
                    }

                    // Load existing profile.
                    // Initialize Tox.
                    try
                    {
                        // Ask user for his password using input dialog
                        if (profile.use_pwd)
                        {
                            profile.pwd = View.Wrapper.InputDlg.pwd_inp(null,
                                                                        _("Password is used for encrypting your profile %s.\nEnter the password:").printf(autoload_profile_name),
                                                                        _("Enter password"),
                                                                        "");
                        }

                        // Initialize profile
                        profile_init(profile, is_new_profile);
                    }
                    catch (GLib.Error ex)
                    {
                        // Clear password
                        profile.pwd = null;

                        // Show "Choose Profile" dialog
                        view->dlg_profile.show(true);
                    }
                }
                catch (ErrGeneral ex)
                {
                    // Clear password if any
                    if (profile != null && profile.use_pwd)
                        profile.pwd = null;

                    // Do not autoload, it is failed
                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    // Show "Choose Profile" dialog
                    view->dlg_profile.show(true);
                }

                // Check profile autoload check box
                view->dlg_profile.check_autoload.val_set(true);
            }
            else
            {
                // Autoloading is disabled.
                // Show "Choose Profile" dialog.
                view->dlg_profile.show(true);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Handle SIGINT, SIGKILL, SIGTERM POSIX signals
         *
         * @param sig Not used currently
         */
        public void sig_handle(int sig)
        {
            // Properly shutdown saving all data.
            //
            // (I commented this code allowing to terminate application
            // without any savings using signals.)
            //exit();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize profile.
         * Creates or loads existing profile.
         *
         * @param profile        Profile
         * @param is_new_profile false: already created profile is used,
         *                       true:  new profile is to be created
         */
        public void profile_init(Model.Profile profile, bool is_new_profile) throws GLib.Error
        {
            // Create or load existing profile.
            // Initialize Tox.
            try
            {
                string? err;

                // Set self profile
                model->self = profile;

                // New or existing?
                uint8[]? save_data;

                if (is_new_profile)
                {
                    // Do not use save data. We are creating a new profile, so use
                    // new Tox ID.
                    save_data = null;
                }
                else
                {
                    // Load profile from disk to memory and decrypt
                    save_data = model->self.load(model);
                }

                // Initialize database
                err = null;

                try
                {
                    model->self.db_init(model);
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    err = ex.message;
                }

                // Load encrypted database from disk to memory and decrypt
                try
                {
                    if (!is_new_profile)
                        model->self.db_load(model);
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    if (err == null)
                        err = ex.message;
                }

                // Setup database pragmas
                try
                {
                    model->self.db_pragma_setup(model);
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    if (err == null)
                        err = ex.message;
                }

                // Create database tables
                try
                {
                    model->self.db_create(model);
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    if (err == null)
                        err = ex.message;
                }

                // Show database error if it occurs
                if (err != null)
                {
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, err, _("Error"));
                    msg_dlg.modal_show();
                }

                // Initialize profile-dependent application settings
                model->self.cfg_init(model);

                // Load profile-dependent application settings from
                // configuration file disk to memory and decrypt
                try
                {
                    if (!is_new_profile)
                        model->self.cfg_load(model);
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                    msg_dlg.modal_show();
                }

                // Try to initialize Tox
                try
                {
                    // Initialize Tox.
                    // Initialize Tox A/V.
                    model->tox_init(save_data);
                    model->tox_av_init();
                }
                catch (Model.ErrModelInitTox ex)
                {
                    // Proxy errors are recoverable
                    if (ex is Model.ErrModelInitTox.PROXY_BAD_HOST ||
                        ex is Model.ErrModelInitTox.PROXY_BAD_PORT ||
                        ex is Model.ErrModelInitTox.PROXY_NOT_FOUND_ERR)
                    {
                        // Invalid proxy settings
                        GLib.debug(ex.message);
                        var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                        msg_dlg.modal_show();

                        // Reset them
                        model->self.cfg.set_integer("network", "proxy_type", Model.Profile.cfg_net_proxy_type_t.NONE);
                        model->self.cfg.set_string ("network", "proxy_host", "");
                        model->self.cfg.set_integer("network", "proxy_port", 0);

                        // Reinitialize Tox with new proxy settings
                        model->tox_init(save_data);
                        model->tox_av_init();
                    }
                    else
                    {
                        // Pass exception выще
                        throw ex;
                    }
                }

                // Add buddy list columns.
                // Add conference list columns.
                // Add the invite list columns.
                try
                {
                    size_t icon_width;
                    size_t icon_height;

                    if (model->self.cfg.get_integer("appearance", "avatar_size") ==
                            Model.Profile.cfg_appearance_avatar_size_t.SMALL)
                    {
                        icon_width  = 16;
                        icon_height = 16;
                    }
                    else
                    {
                        icon_width  = 32;
                        icon_height = 32;
                    }

                    view->frm_main.lst_ctrl_buddies.icon_size_set(icon_width, icon_height);
                    view->frm_main.lst_ctrl_conferences.icon_size_set(icon_width, icon_height);
                    view->frm_main.lst_ctrl_members_tab1.icon_size_set(icon_width, icon_height);
                    view->frm_main.lst_ctrl_invites_tab3.icon_size_set(icon_width, icon_height);

                    const size_t MAGIC = 8;

                    view->frm_main.lst_ctrl_buddies.col_add(View.FrmMain.LST_CTRL_BUDDIES_COL_DISPLAY_NAME, 0); // The column width will be set later
                    view->frm_main.lst_ctrl_buddies.col_add(View.FrmMain.LST_CTRL_BUDDIES_COL_STATUS,       0); // The column width will be set later
                    view->frm_main.lst_ctrl_buddies.col_add(View.FrmMain.LST_CTRL_BUDDIES_COL_STATE,        icon_width + MAGIC);
                    view->frm_main.lst_ctrl_buddies.col_add(View.FrmMain.LST_CTRL_BUDDIES_COL_STATE_USER,   icon_width + MAGIC);

                    view->frm_main.lst_ctrl_conferences.col_add(View.FrmMain.LST_CTRL_CONFERENCES_COL_DISPLAY_NAME, 0); // The column width will be set later
                    view->frm_main.lst_ctrl_conferences.col_add(View.FrmMain.LST_CTRL_CONFERENCES_COL_CNT,          0); // The column width will be set later
                    view->frm_main.lst_ctrl_conferences.col_add(View.FrmMain.LST_CTRL_CONFERENCES_COL_UNREAD_STATE, icon_width + MAGIC);

                    view->frm_main.lst_ctrl_members_tab1.col_add(View.FrmMain.LST_CTRL_MEMBERS_COL_DISPLAY_NAME, 0); // The column width will be set later
                    view->frm_main.lst_ctrl_members_tab1.col_add(View.FrmMain.LST_CTRL_MEMBERS_COL_STATE_USER,   icon_width + MAGIC);

                    view->frm_main.lst_ctrl_invites_tab3.col_add(View.FrmMain.LST_CTRL_INVITES_COL_DISPLAY_NAME, 0); // The column width will be set later
                    view->frm_main.lst_ctrl_invites_tab3.col_add(View.FrmMain.LST_CTRL_INVITES_COL_COOKIE,       0); // The column width will be set later
                    view->frm_main.lst_ctrl_invites_tab3.col_add(View.FrmMain.LST_CTRL_INVITES_COL_DATETIME,     0); // The column width will be set later
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Initialize Tox callbacks.
                // Initialize Tox A/V callbacks.
                model->tox_cb_init();
                model->tox_av_cb_init();

                // Initialize self
                try
                {
                    model->self_init(is_new_profile);
                }
                catch (GLib.IOError ex)
                {
                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                    msg_dlg.modal_show();
                }

                // Initialize buddies
                err = model->buddies_init();

                // Show only the first buddy error if it'd occured
                if (err != null)
                {
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, err, _("Error"));
                    msg_dlg.modal_show();
                }

                // Initialize conferences
                model->conferences_init();

                // Select buddy requests from database
                try
                {
                    // Select buddy request from database table "buddy_reqs"
                    uint8[]? key;
                    string?  msg;
                    GLib.DateTime? datetime;

                    Sqlite.Statement? stmt = null;

                    while (model->self.db_buddy_reqs_sel(ref stmt,

                                                         out key,
                                                         out msg,
                                                         out datetime))
                    {
                        // Convert public key from array to string
                        size_t len = key.length;

                        string key_str = "";
                        for (size_t byte = 0; byte < len; byte++)
                            key_str += key[byte].to_string("%02X");

                        // Prepare the new buddy request
                        var buddy_req = new Model.Model.BuddyReq();

                        buddy_req.key      = new uint8[len];
                        buddy_req.key_str  = (owned) key_str;
                        buddy_req.msg      = (owned) msg;
                        buddy_req.added_in_this_tox_session = false;
                        buddy_req.datetime = datetime;

                        Posix.memcpy(buddy_req.key,
                                     key,
                                     buddy_req.key.length);

                        // Add the buddy request
                        model->buddy_reqs.insert(0, buddy_req);

                        // Update tab 2 controls
                        view->frm_main.check_lst_pub_keys_tab2_insert(buddy_req.key_str, buddy_req.datetime);
                        this.frm_main.txt_msg_tab2_upd();
                    }
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }

                // Select conference invitations from database
                try
                {
                    // Select conference invitation from database table
                    // "conference_invitations"
                    Model.Profile.conference_invitation_t type;
                    uint8[]?       cookie;
                    GLib.DateTime? datetime;
                    uint32         uid;

                    Sqlite.Statement? stmt = null;

                    while (model->self.db_conference_invitations_sel(ref stmt,

                                                                     out type,
                                                                     out cookie,
                                                                     out datetime,
                                                                     out uid))
                    {
                        GLib.assert(type == Model.Profile.conference_invitation_t.TEXT ||
                                    type == Model.Profile.conference_invitation_t.AUDIO_VIDEO);

                        // Convert cookie from array to string
                        string cookie_str = "";
                        foreach (uint8 byte in cookie)
                            cookie_str += byte.to_string("%02X");

                        // Prepare the new conference invitation
                        var conference_invitation = new Model.Model.ConferenceInvitation();

                        conference_invitation.buddy      = model->buddies_hash[uid];
                        conference_invitation.type       = type == Model.Profile.conference_invitation_t.TEXT ?
                            global::ToxCore.CONFERENCE_TYPE.TEXT :
                            global::ToxCore.CONFERENCE_TYPE.AV;
                        conference_invitation.cookie     = new uint8[cookie.length];
                        conference_invitation.cookie_str = (owned) cookie_str;
                        conference_invitation.datetime   = datetime;

                        Posix.memcpy(conference_invitation.cookie,
                                     cookie,
                                     conference_invitation.cookie.length);

                        // Add it to the invite list
                        this.frm_main.lst_ctrl_invites_tab3_change(action_t.ADD_ELEM, conference_invitation);
                    }
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }

                // Create directory and save newly created empty profile, config
                // and database on disk.
                //
                // It is not necessary to do saving right now. We could save
                // later, when application will be closed, but we want to be
                // sure disk operation is available and inform user if it is not.
                try
                {
                    // Save files
                    if (is_new_profile &&                    // Save only newly created profiles
                            model->self.profile_name != null // Do not save one-time sessions
                       )
                    {
                        model->self.save(model);
                        model->self.db_save(model);
                        model->self.cfg_save(model);
                    }
                }
                catch (GLib.Error ex)
                {
                    GLib.debug(ex.message);
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                    msg_dlg.modal_show();
                }

                // Update config with new autoload name
                model->cfg.set_string("general",
                                      "autoload_profile_name",
                                      model->self.profile_name == null ? "" : model->self.profile_name);

                // Set an audio subsystem.
                // Change input & output sound devices, their gain and volume.
                try
                {
                    model->audio.sys_cfg(model);
                }
                catch (Model.ErrAudio ex)
                {
                    var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                    msg_dlg.modal_show();
                }

                // Move and resize main window
                try
                {
                    bool move_win = model->self.cfg.get_boolean("main", "use_win_pos_x_y");

                    if (move_win)
                    {
                        int x = model->self.cfg.get_integer("main", "x");
                        int y = model->self.cfg.get_integer("main", "y");

                        view->frm_main.pos_set(x, y);
                    }

                    bool resize_win = model->self.cfg.get_boolean("main", "use_win_size");

                    if (resize_win)
                    {
                        size_t width  = model->self.cfg.get_integer("main", "width");
                        size_t height = model->self.cfg.get_integer("main", "height");

                        view->frm_main.size_set(width, height);
                    }

                    size_t width;
                    size_t height;

                    view->frm_main.size_get(out width, out height);

                    this.frm_main.width  = width;
                    this.frm_main.height = height;
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Now open main window
                view->dlg_profile.show(false);
                view->frm_main.show(true);

                // Set sash positions
                try
                {
                    size_t pos = model->self.cfg.get_integer("main", "sash_pos");
                    view->frm_main.splitter_vertical.sash_set(pos);
                    pos = model->self.cfg.get_integer("main", "sash_tab1_pos");
                    view->frm_main.splitter_vertical_tab1.sash_set(pos);
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Set new width of buddy list columns.
                // Set new width of conference list columns.
                // Set new width of the invite list columns.
                view->frm_main.lst_ctrl_buddies_width_set();
                view->frm_main.lst_ctrl_conferences_width_set();
                view->frm_main.lst_ctrl_invites_tab3_width_set();

                // Check menu item of "Buddies" -> "Show" submenu
                Model.Profile.cfg_main_show_buddies_t show_buddies;

                try
                {
                    show_buddies = (Model.Profile.cfg_main_show_buddies_t)
                        model->self.cfg.get_integer("main", "show_buddies");
                }
                catch (GLib.KeyFileError ex)
                {
                    show_buddies = Model.Profile.cfg_main_show_buddies_t.SHOW_ALL;
                    GLib.assert(false);
                }

                switch (show_buddies)
                {
                case Model.Profile.cfg_main_show_buddies_t.SHOW_ALL:
                    view->frm_main.menu_show_all.check(true);
                    view->frm_main.menu_show_all_.check(true);
                    break;
                case Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE_BUT:
                    view->frm_main.menu_hide_offline_but.check(true);
                    view->frm_main.menu_hide_offline_but_.check(true);
                    break;
                case Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE:
                    view->frm_main.menu_hide_offline.check(true);
                    view->frm_main.menu_hide_offline_.check(true);
                    break;
                default:
                    view->frm_main.menu_show_all.check(true);
                    view->frm_main.menu_show_all_.check(true);
                    break;
                }

                // Check menu item of "Buddies" -> "Sort By" submenu (1)
                Model.Profile.cfg_main_sort_buddies_t sort_buddies;

                try
                {
                    sort_buddies = (Model.Profile.cfg_main_sort_buddies_t)
                        model->self.cfg.get_integer("main", "sort_buddies");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_buddies = Model.Profile.cfg_main_sort_buddies_t.ADD_LST;
                    GLib.assert(false);
                }

                switch (sort_buddies)
                {
                case Model.Profile.cfg_main_sort_buddies_t.ADD_LST:
                    view->frm_main.menu_sort_add_lst_buddies.check(true);
                    view->frm_main.menu_sort_add_lst_buddies_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_buddies_t.DISPLAY_NAME:
                    view->frm_main.menu_sort_display_name_buddies.check(true);
                    view->frm_main.menu_sort_display_name_buddies_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_buddies_t.LAST_MSG:
                    view->frm_main.menu_sort_last_msg_buddies.check(true);
                    view->frm_main.menu_sort_last_msg_buddies_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_buddies_t.STATE:
                    view->frm_main.menu_sort_state_buddies.check(true);
                    view->frm_main.menu_sort_state_buddies_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_add_lst_buddies.check(true);
                    view->frm_main.menu_sort_add_lst_buddies_.check(true);
                    break;
                }

                // Check menu item of "Buddies" -> "Sort By" submenu (2)
                Model.Profile.cfg_main_sort_buddies_order_t sort_buddies_order;

                try
                {
                    sort_buddies_order = (Model.Profile.cfg_main_sort_buddies_order_t)
                        model->self.cfg.get_integer("main", "sort_buddies_order");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_buddies_order = Model.Profile.cfg_main_sort_buddies_order_t.DESC;
                    GLib.assert(false);
                }

                switch (sort_buddies_order)
                {
                case Model.Profile.cfg_main_sort_buddies_order_t.ASC:
                    view->frm_main.menu_sort_asc_buddies.check(true);
                    view->frm_main.menu_sort_asc_buddies_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_buddies_order_t.DESC:
                    view->frm_main.menu_sort_desc_buddies.check(true);
                    view->frm_main.menu_sort_desc_buddies_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_asc_buddies.check(true);
                    view->frm_main.menu_sort_asc_buddies_.check(true);
                    break;
                }

                // Check menu item "Buddies" -> "Sort By" -> "Unread are always
                // top"
                try
                {
                    bool flag = model->self.cfg.get_boolean("main", "sort_buddies_unread_top");
                    view->frm_main.menu_sort_unread_top_buddies.check(flag);
                    view->frm_main.menu_sort_unread_top_buddies_.check(flag);
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Check menu item of "Conferences" -> "Sort By" submenu (1)
                Model.Profile.cfg_main_sort_conferences_t sort_conferences;

                try
                {
                    sort_conferences = (Model.Profile.cfg_main_sort_conferences_t)
                        model->self.cfg.get_integer("main", "sort_conferences");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_conferences = Model.Profile.cfg_main_sort_conferences_t.ADD_LST;
                    GLib.assert(false);
                }

                switch (sort_conferences)
                {
                case Model.Profile.cfg_main_sort_conferences_t.ADD_LST:
                    view->frm_main.menu_sort_add_lst_conferences.check(true);
                    view->frm_main.menu_sort_add_lst_conferences_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_conferences_t.DISPLAY_NAME:
                    view->frm_main.menu_sort_display_name_conferences.check(true);
                    view->frm_main.menu_sort_display_name_conferences_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_conferences_t.LAST_MSG:
                    view->frm_main.menu_sort_last_msg_conferences.check(true);
                    view->frm_main.menu_sort_last_msg_conferences_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_add_lst_conferences.check(true);
                    view->frm_main.menu_sort_add_lst_conferences_.check(true);
                    break;
                }

                // Check menu item of "Conferences" -> "Sort By" submenu (2)
                Model.Profile.cfg_main_sort_conferences_order_t sort_conferences_order;

                try
                {
                    sort_conferences_order = (Model.Profile.cfg_main_sort_conferences_order_t)
                        model->self.cfg.get_integer("main", "sort_conferences_order");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_conferences_order = Model.Profile.cfg_main_sort_conferences_order_t.DESC;
                    GLib.assert(false);
                }

                switch (sort_conferences_order)
                {
                case Model.Profile.cfg_main_sort_conferences_order_t.ASC:
                    view->frm_main.menu_sort_asc_conferences.check(true);
                    view->frm_main.menu_sort_asc_conferences_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_conferences_order_t.DESC:
                    view->frm_main.menu_sort_desc_conferences.check(true);
                    view->frm_main.menu_sort_desc_conferences_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_asc_conferences.check(true);
                    view->frm_main.menu_sort_asc_conferences_.check(true);
                    break;
                }

                // Check menu item "Conferences" -> "Sort By" -> "Unread are always
                // top"
                try
                {
                    bool flag = model->self.cfg.get_boolean("main", "sort_conferences_unread_top");
                    view->frm_main.menu_sort_unread_top_conferences.check(flag);
                    view->frm_main.menu_sort_unread_top_conferences_.check(flag);
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Check menu item of "Conferences" -> "Members" -> "Sort By"
                // submenu (1)
                Model.Profile.cfg_main_sort_members_t sort_members;

                try
                {
                    sort_members = (Model.Profile.cfg_main_sort_members_t)
                        model->self.cfg.get_integer("main", "sort_members");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_members = Model.Profile.cfg_main_sort_members_t.ADD_LST;
                    GLib.assert(false);
                }

                switch (sort_members)
                {
                case Model.Profile.cfg_main_sort_members_t.ADD_LST:
                    view->frm_main.menu_sort_add_lst_members.check(true);
                    view->frm_main.menu_sort_add_lst_members_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_members_t.DISPLAY_NAME:
                    view->frm_main.menu_sort_display_name_members.check(true);
                    view->frm_main.menu_sort_display_name_members_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_add_lst_members.check(true);
                    view->frm_main.menu_sort_add_lst_members_.check(true);
                    break;
                }

                // Check menu item of "Conferences" -> "Members" -> "Sort By"
                // submenu (2)
                Model.Profile.cfg_main_sort_members_order_t sort_members_order;

                try
                {
                    sort_members_order = (Model.Profile.cfg_main_sort_members_order_t)
                        model->self.cfg.get_integer("main", "sort_members_order");
                }
                catch (GLib.KeyFileError ex)
                {
                    sort_members_order = Model.Profile.cfg_main_sort_members_order_t.DESC;
                    GLib.assert(false);
                }

                switch (sort_members_order)
                {
                case Model.Profile.cfg_main_sort_members_order_t.ASC:
                    view->frm_main.menu_sort_asc_members.check(true);
                    view->frm_main.menu_sort_asc_members_.check(true);
                    break;
                case Model.Profile.cfg_main_sort_members_order_t.DESC:
                    view->frm_main.menu_sort_desc_members.check(true);
                    view->frm_main.menu_sort_desc_members_.check(true);
                    break;
                default:
                    view->frm_main.menu_sort_asc_members.check(true);
                    view->frm_main.menu_sort_asc_members_.check(true);
                    break;
                }

                // Set name, status
                view->frm_main.lbl_name.lbl_set(model->self.display_name_get());
                view->frm_main.lbl_status.lbl_set(model->self.status);

                // Set self avatar
                this.frm_main.img_avatar_set();

                // Set self network & user state icons
                bool is_small;

                try
                {
                    is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                        Model.Profile.cfg_appearance_avatar_size_t.SMALL;
                }
                catch (GLib.KeyFileError ex)
                {
                    is_small = false;
                    GLib.assert(false);
                }

                view->frm_main.img_state.img_set(
                    model->self.icon_filepath_state_get(is_small),
                    null // Avatar filepath is to be used, do not use avatar blob
                   );
                view->frm_main.btn_bmp_state_user.img_set(
                    model->self.icon_filepath_state_user_get(is_small));

                // Enable/disable "Apply" button.
                // Update status bar.
                this.frm_main.btn_apply_status_en_set();
                this.frm_main.status_val_set(View.FrmMain.STATUS_COL_BUDDIES);
                this.frm_main.status_val_set(View.FrmMain.STATUS_COL_CONFERENCES);
            }
            catch (GLib.Error ex)
            {
                // Unload just created/loaded profile
                model->self = null;

                GLib.debug(ex.message);
                var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_profile, ex.message, _("Error"));
                msg_dlg.modal_show();

                // Pass exception выще
                throw ex;
            }

            // Schedule next Tox iteration.
            // Schedule next Tox A/V iteration.
            model->tox_next_iteration_schedule();
            model->tox_av_next_iteration_schedule();

            // Tox iteration is needed.
            // Tox A/V iteration is needed.
            model->tox_need_iteration    = true;
            model->tox_av_need_iteration = true;

            // Now plugins should be loaded
            model->should_load_plugins = true;

            // Initialize threads.
            // Start threads.
            model->threads_init (Model.Model.thread_t.BOOTSTRAP | Model.Model.thread_t.GLIB);
            model->threads_start(Model.Model.thread_t.BOOTSTRAP | Model.Model.thread_t.GLIB);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Reinitialize profile.
         * Reloads existing profile.
         *
         * @param target What parts of the application have been changed
         */
        public void profile_reinit(reinit_target_t target) throws Model.ErrModelInitTox, Model.ErrGeneral, Model.ErrAudio
        {
            // Redraw controls with new size avatars
            if ((target & reinit_target_t.NEW_AVATAR_SIZE) > 0)
            {
                // Set self avatar
                this.frm_main.img_avatar_set();

                // Set self network & user state icons
                bool   is_small;
                size_t icon_width;
                size_t icon_height;

                try
                {
                    if ((Model.Profile.cfg_appearance_avatar_size_t) model->self.cfg.get_integer("appearance", "avatar_size") ==
                            Model.Profile.cfg_appearance_avatar_size_t.SMALL)
                    {
                        is_small    = true;
                        icon_width  = 16;
                        icon_height = 16;
                    }
                    else
                    {
                        is_small    = false;
                        icon_width  = 32;
                        icon_height = 32;
                    }
                }
                catch (GLib.KeyFileError ex)
                {
                    is_small    = true;
                    icon_width  = 16;
                    icon_height = 16;

                    GLib.assert(false);
                }

                view->frm_main.img_state.img_set(
                    model->self.icon_filepath_state_get(is_small),
                    null // Avatar filepath is to be used, do not use avatar blob
                   );
                view->frm_main.btn_bmp_state_user.img_set(
                    model->self.icon_filepath_state_user_get(is_small));

                // Set new icon size to lists
                view->frm_main.lst_ctrl_buddies.icon_size_set(icon_width, icon_height);
                view->frm_main.lst_ctrl_conferences.icon_size_set(icon_width, icon_height);
                view->frm_main.lst_ctrl_members_tab1.icon_size_set(icon_width, icon_height);
                view->frm_main.lst_ctrl_invites_tab3.icon_size_set(icon_width, icon_height);

                // Set new width of the list columns
                const size_t MAGIC = 8;

                view->frm_main.lst_ctrl_buddies.width_set(View.FrmMain.LST_CTRL_BUDDIES_COL_STATE,      icon_width + MAGIC);
                view->frm_main.lst_ctrl_buddies.width_set(View.FrmMain.LST_CTRL_BUDDIES_COL_STATE_USER, icon_width + MAGIC);
                view->frm_main.lst_ctrl_conferences.width_set(View.FrmMain.LST_CTRL_CONFERENCES_COL_UNREAD_STATE, icon_width + MAGIC);
                view->frm_main.lst_ctrl_members_tab1.width_set(View.FrmMain.LST_CTRL_MEMBERS_COL_STATE_USER, icon_width + MAGIC);

                // Refill buddy list.
                // Refill conference list.
                // Refill members list.
                // Refill the invite list.
                this.frm_main.lst_ctrl_buddies_change(action_t.UPD_LST);
                this.frm_main.lst_ctrl_conferences_change(action_t.UPD_LST);
                this.frm_main.lst_ctrl_members_tab1_change(action_t.UPD_LST);
                this.frm_main.lst_ctrl_invites_tab3_change(action_t.UPD_LST);
            }

            // Have new bootstrap nodes?
            if ((target & reinit_target_t.NEW_BOOTSTRAP_NODES) > 0)
            {
                // Ask the bootstrap thread that bootstrap nodes have been
                // updated.
                // Asynchronous queue is used for communication with that
                // thread.
                var cmd = new Model.Threads.ThreadBootstrap.Cmd(Model.Threads.ThreadBootstrap.Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_NODE_SRC_CFG,
                                                                Model.Person.state_t.NONE // Not used
                                                               );
                model->thread_bootstrap.queue_cmd.push(cmd);

                // Unblock the thread if it is blocked
                model->thread_bootstrap.cancel.cancel();
            }

            // Reinitialize Tox with new network settings if any.
            // Have new communication or proxy settings?
            if ((target & reinit_target_t.NEW_COMM_SETTINGS)  > 0 ||
                (target & reinit_target_t.NEW_PROXY_SETTINGS) > 0)
            {
                // Reinitialize Tox with new network settings
                try
                {
                    // Export profile to memory.
                    // Reinitialize Tox.
                    // Reinitialize Tox callbacks.
                    // Reinitialize Tox A/V.
                    // Reinitialize Tox A/V callbacks.
                    uint8[] save_data = model->self.export(model);
                    model->tox_av_deinit();
                    model->tox_init(save_data);
                    model->tox_cb_init();
                    model->tox_av_init();
                    model->tox_av_cb_init();

                    // Create temporary buddy list array.
                    // Sort it by unique identifiers used by libtoxcore.
                    var buddies_lst = new Gee.ArrayList<Model.Buddy>();
                    foreach (Model.Buddy buddy in model->buddies_hash.values)
                        buddies_lst.add(buddy);

                    buddies_lst.sort((a, b) =>
                        {
                            if      (a.uid >  b.uid) {                     return  1; }
                            else if (a.uid == b.uid) { GLib.assert(false); return  0; }
                            else                     {                     return -1; }
                        }
                    );

                    // Get rid of holes between UIDs
                    uint32 uid = 0;

                    foreach (Model.Buddy buddy in buddies_lst)
                    {
                        model->buddies_hash.unset(buddy.uid);
                        buddy.uid = uid++;
                        model->buddies_hash[buddy.uid] = buddy;
                    }

                    // Get self network state from Tox profile
                    global::ToxCore.ConnectionStatus state = model->tox.self_get_connection_status();

                    switch (state)
                    {
                    // There is no connection
                    case global::ToxCore.ConnectionStatus.NONE:
                        model->self.state = Model.Person.state_t.OFFLINE;
                        break;

                    // Another state
                    default:
                        GLib.assert(false);
                        break;
                    }

                    // Set buddies network states
                    Model.Buddy.none       = 0;
                    Model.Buddy.offline    = model->buddies_lst.size;
                    Model.Buddy.online_tcp = 0;
                    Model.Buddy.online_udp = 0;

                    foreach (Model.Buddy buddy in model->buddies_lst)
                    {
                        // Get buddy network state from Tox profile
                        global::ToxCore.ConnectionStatus state_;
                        global::ToxCore.ERR_FRIEND_QUERY err;
                        state_ = model->tox.friend_get_connection_status(buddy.uid, out err);
                        GLib.assert(err == global::ToxCore.ERR_FRIEND_QUERY.OK);

                        switch (state_)
                        {
                        // There is no connection
                        case global::ToxCore.ConnectionStatus.NONE:
                            buddy.state = Model.Person.state_t.OFFLINE;
                            break;

                        // Another state
                        default:
                            GLib.assert(false);
                            break;
                        }
                    }

                    // Create temporary conference list array.
                    // Sort it by unique identifiers used by libtoxcore.
                    var conferences_lst = new Gee.ArrayList<Model.Conference>();
                    foreach (Model.Conference conference in model->conferences_hash.values)
                        conferences_lst.add(conference);

                    conferences_lst.sort((a, b) =>
                        {
                            if      (a.uid >  b.uid) {                     return  1; }
                            else if (a.uid == b.uid) { GLib.assert(false); return  0; }
                            else                     {                     return -1; }
                        }
                    );

                    // Get rid of holes between UIDs
                    uid = 0;

                    foreach (Model.Conference conference in conferences_lst)
                    {
                        model->conferences_hash.unset(conference.uid);
                        conference.uid = uid++;
                        model->conferences_hash[conference.uid] = conference;
                    }

                    // Recalculate the number of offline members.
                    // Recalculate the number of online members.
                    foreach (Model.Conference conference in model->conferences_hash.values)
                    {
                        conference.recalc(model, Model.Conference.recalc_kind_t.OFFLINE);
                        conference.recalc(model, Model.Conference.recalc_kind_t.ONLINE);
                    }

                    // Inform Model bootstrap thread that Tox network state has
                    // been changed.
                    // Asynchronous queue is used for communication with that
                    // thread.
                    var cmd = new Model.Threads.ThreadBootstrap.Cmd(Model.Threads.ThreadBootstrap.Cmd.cmd_t.WAS_REINIT_HANDLE_NEW_STATE,
                                                                    model->self.state);
                    model->thread_bootstrap.queue_cmd.push(cmd);

                    // Mark all buddy requests they were added in previous Tox
                    // session
                    foreach (Model.Model.BuddyReq buddy_req in model->buddy_reqs)
                        buddy_req.added_in_this_tox_session = false;

                    // Update icon with self network state
                    try
                    {
                        view->frm_main.img_state.img_set(
                            model->self.icon_filepath_state_get(model->self.cfg.get_integer("appearance", "avatar_size") == Model.Profile.cfg_appearance_avatar_size_t.SMALL),
                            null // Avatar filepath is to be used, do not use avatar blob
                        );
                    }
                    catch (GLib.KeyFileError ex)
                    {
                        GLib.assert(false);
                    }

                    // Refill buddy list.
                    // Refill conference list.
                    this.frm_main.lst_ctrl_buddies_change(action_t.UPD_LST);
                    this.frm_main.lst_ctrl_conferences_change(action_t.UPD_LST);

                    // Reinitialize members of all conferences
                    model->members_lst.clear();

                    foreach (Model.Conference conference in model->conferences_hash.values)
                    {
                        conference.members_hash.clear();
                        model->members_init(conference);
                    }

                    // Update status bar
                    this.frm_main.status_val_set(View.FrmMain.STATUS_COL_BUDDIES);
                }
                catch (Model.ErrModelInitTox ex)
                {
                    // Invalid proxy settings.
                    // Reset them.
                    model->self.cfg.set_integer("network", "proxy_type", Model.Profile.cfg_net_proxy_type_t.NONE);
                    model->self.cfg.set_string ("network", "proxy_host", "");
                    model->self.cfg.set_integer("network", "proxy_port", 0);

                    // Reload all widgets with new language if needed
                    if ((target & reinit_target_t.NEW_LANG) > 0)
                    {
                        // Reinitialize locale or reset settings on fail
                        try
                        {
                            // Reinitialize locale
                            model->locale_reinit();

                            // Unlock mutex.
                            // Properly shutdown saving all data.
                            // Restart the application.
                            model->mutex.unlock();
                            exit();
                            Posix.execv(this.args[0], this.args);
                        }
                        catch (Model.ErrGeneral ex)
                        {
                            // Invalid locale settings.
                            // Reset them.
                            model->cfg.set_integer("appearance", "lang", Model.Model.cfg_appearance_lang_t.DEF);
                        }
                    }

                    // Pass exception выще
                    throw ex;
                }
            }

            // Reload all widgets with new language if needed
            if ((target & reinit_target_t.NEW_LANG) > 0)
            {
                // Reinitialize locale or reset settings on fail
                try
                {
                    // Reinitialize locale
                    model->locale_reinit();

                    // Unlock mutex.
                    // Properly shutdown saving all data.
                    // Restart the application.
                    model->mutex.unlock();
                    exit();
                    Posix.execv(this.args[0], this.args);
                }
                catch (Model.ErrGeneral ex)
                {
                    // Invalid locale settings.
                    // Reset them.
                    model->cfg.set_integer("appearance", "lang", Model.Model.cfg_appearance_lang_t.DEF);

                    // Pass exception выще
                    throw ex;
                }
            }

            // Set an audio subsystem.
            // Change input & output sound devices, their gain and volume.
            if ((target & reinit_target_t.NEW_AUDIO_SETTINGS) > 0)
            {
                model->audio.sys_cfg(model);
            }
        }
    }
}

