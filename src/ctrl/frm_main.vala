/*
 *    frm_main.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /*----------------------------------------------------------------------------*/
    /*                               Error Domains                                */
    /*----------------------------------------------------------------------------*/

    /**
     * Message send errors in 1v1 buddy chats
     */
    public errordomain ErrFrmMainSendMsgBuddy
    {
        /**
         * This client is currently not connected to the buddy
         */
        BUDDY_NOT_CONNECTED_ERR,

        /**
         * An allocation error occurred while increasing the send queue
         * size
         */
        MEM_ALLOC,

        /**
         * Message length exceeded global::ToxCore.MAX_MESSAGE_LENGTH
         */
        TOO_LONG,
    }

    /**
     * Message send errors in conferences
     */
    public errordomain ErrFrmMainSendMsgConference
    {
        /**
         * The client is not connected to the conference
         */
        NO_CONNECTION,

        /**
         * The message packet failed to send
         */
        SEND_FAIL,
    }

    /**
     * Conference joining errors
     */
    public errordomain ErrFrmMainJoinConference
    {
        /**
         * The cookie passed has an invalid length
         */
        INVALID_LEN,

        /**
         * The conference is not the expected type. This indicates an
         * invalid cookie.
         */
        WRONG_TYPE,

        /**
         * Client is already in this conference
         */
        DUPLICATE,

        /**
         * The join packet failed to send
         */
        SEND_FAIL,
    }

    /**
     * This is the main window frame class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class FrmMain : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /*
         * Main window idle handler signal
         */
        public signal void *idle_handle_sig(void *fn, void *param, void *event);

        /*
         * Main window close signal
         */
        public signal void *closed_sig(void *fn, void *param, void *event);

        /*
         * Main window move signal
         */
        public signal void *move_sig(void *fn, void *param, void *event);

        /*
         * "Profile" -> "Edit Profile..." menu item click signal
         */
        public signal void *menu_edit_profile_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Profile" -> "About Profile..." menu item click signal
         */
        public signal void *menu_about_profile_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Profile" -> "Change Profile..." menu item click signal
         */
        public signal void *menu_change_profile_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Profile" -> "Quit" menu item click signal
         */
        public signal void *menu_quit_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Add Buddy..." menu item click signal
         */
        public signal void *menu_add_buddy_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Edit Buddy..." menu item click signal
         */
        public signal void *menu_edit_buddy_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Remove Buddy" menu item click signal
         */
        public signal void *menu_remove_buddy_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "About Buddy..." menu item click signal
         */
        public signal void *menu_about_buddy_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Conversation History..." menu item click signal
         */
        public signal void *menu_conv_buddy_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Show" -> "Show all" menu item click signal
         */
        public signal void *menu_show_all_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Show" -> "Hide offline but show unread" menu
         * item click signal
         */
        public signal void *menu_hide_offline_but_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Show" -> "Hide offline" menu item click
         * signal
         */
        public signal void *menu_hide_offline_clicked_sig(void *fn, void *param, void *event);

        /*
         * Buddies" -> "Sort By" -> "Adding to buddy list" menu item
         * click signal
         */
        public signal void *menu_sort_add_lst_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Display name" menu item click
         * signal
         */
        public signal void *menu_sort_display_name_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Last message" menu item click
         * signal
         */
        public signal void *menu_sort_last_msg_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Network state" menu item click
         * signal
         */
        public signal void *menu_sort_state_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Ascending" menu item click
         * signal
         */
        public signal void *menu_sort_asc_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Descending" menu item click
         * signal
         */
        public signal void *menu_sort_desc_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Buddies" -> "Sort By" -> "Unread are always top" menu item
         * click signal
         */
        public signal void *menu_sort_unread_top_buddies_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Add Conference..." menu item click signal
         */
        public signal void *menu_add_conference_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Edit Conference..." menu item click
         * signal
         */
        public signal void *menu_edit_conference_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Remove Conference" menu item click signal
         */
        public signal void *menu_remove_conference_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "About Conference..." menu item click
         * signal
         */
        public signal void *menu_about_conference_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Conversation History..." menu item click
         * signal
         */
        public signal void *menu_conv_conference_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Adding to conference list"
         * menu item click signal
         */
        public signal void *menu_sort_add_lst_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Display name" menu item click
         * signal
         */
        public signal void *menu_sort_display_name_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Last message" menu item click
         * signal
         */
        public signal void *menu_sort_last_msg_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Ascending" menu item click
         * signal
         */
        public signal void *menu_sort_asc_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Descending" menu item click
         * signal
         */
        public signal void *menu_sort_desc_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Sort By" -> "Unread are always top" menu
         * item click signal
         */
        public signal void *menu_sort_unread_top_conferences_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "Add to Buddy List" / "Find in
         * Buddy List" menu item click signal
         */
        public signal void *menu_members_action_in_buddy_list_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "About Member..." menu item
         * click signal
         */
        public signal void *menu_about_member_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "Sort By" -> "Adding to member
         * List" menu item click signal
         */
        public signal void *menu_sort_add_lst_members_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "Sort By" -> "Display name"
         * menu item click signal
         */
        public signal void *menu_sort_display_name_members_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "Sort By" -> "Ascending" menu
         * item click signal
         */
        public signal void *menu_sort_asc_members_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Conferences" -> "Members" -> "Sort By" -> "Descending" menu
         * item click signal
         */
        public signal void *menu_sort_desc_members_clicked_sig(void *fn, void *param, void *event);

        /*
         * List control with conferences right click signal.
         *
         * It is placed on the left panel.
         */
        public signal void *lst_ctrl_conferences_right_clicked_sig(void *fn, void *param, void *event);

        /*
         * List control with conferences select signal.
         *
         * It is placed on the conference list panel.
         */
        public signal void *lst_ctrl_conferences_selected_sig(void *fn, void *param, void *event);

        /*
         * List control with conferences deselect signal.
         *
         * It is placed on the conference list panel.
         */
        public signal void *lst_ctrl_conferences_deselected_sig(void *fn, void *param, void *event);

        /*
         * "Tools" -> "Plugins" menu item click signal
         */
        public signal void *menu_plugins_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Tools" -> "Settings" menu item click signal
         */
        public signal void *menu_settings_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Help" -> "In Loving Memory of..." menu item click signal
         */
        public signal void *menu_Vika_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Help" -> "About..." menu item click signal
         */
        public signal void *menu_about_clicked_sig(void *fn, void *param, void *event);

        /*
         * Button for changing self user state click signal
         */
        public signal void *btn_bmp_state_user_clicked_sig(void *fn, void *param, void *event);

        /*
         * Vertical splitter sash position changing
         */
        public signal void *splitter_vertical_sash_changing_sig(void *fn, void *param, void *event);

        /*
         * Do the buddy selection:
         * - update GUI controls,
         * - select last messages from database and append them to text
         *   control
         * List control with buddies right click signal.
         *
         * It is placed on the left panel.
         */
        public signal void *lst_ctrl_buddies_right_clicked_sig(void *fn, void *param, void *event);

        /*
         * List control with buddies select signal.
         *
         * It is placed on the buddy list panel.
         */
        public signal void *lst_ctrl_buddies_selected_sig(void *fn, void *param, void *event);

        /*
         * List control with buddies deselect signal.
         *
         * It is placed on the buddy list panel.
         */
        public signal void *lst_ctrl_buddies_deselected_sig(void *fn, void *param, void *event);

        /*
         * Status & buddy filter choice select signal.
         *
         * It is placed on the left panel.
         */
        public signal void *choice_status_and_filter_selected_sig(void *fn, void *param, void *event);

        /*
         * Status & buddy filter input text control changed signal.
         *
         * It is placed on the left panel.
         */
        public signal void *txt_status_and_filter_changed_sig(void *fn, void *param, void *event);

        /*
         * "Apply" button click signal.
         *
         * It is placed on the left panel.
         */
        public signal void *btn_apply_status_clicked_sig(void *fn, void *param, void *event);

        /*
         * Vertical splitter sash position changing.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *splitter_vertical_tab1_sash_changing_sig(void *fn, void *param, void *event);

        /*
         * List control with members right click signal.
         *
         * It is placed on the right panel.
         */
        public signal void *lst_ctrl_members_tab1_right_clicked_sig(void *fn, void *param, void *event);

        /*
         * List control with members select signal.
         *
         * It is placed on the right panel.
         */
        public signal void *lst_ctrl_members_tab1_selected_sig(void *fn, void *param, void *event);

        /*
         * List control with members deselect signal.
         *
         * It is placed on the right panel.
         */
        public signal void *lst_ctrl_members_tab1_deselected_sig(void *fn, void *param, void *event);

        /*
         * "Available" menu item click signal.
         *
         * It is placed in context menu for changing self user state.
         */
        public signal void *menu_avail_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Away" menu item click signal.
         *
         * It is placed in context menu for changing self user state.
         */
        public signal void *menu_away_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Busy" menu item click signal.
         *
         * It is placed in context menu for changing self user state.
         */
        public signal void *menu_busy_clicked_sig(void *fn, void *param, void *event);

        /*
         * Contact list notebook page changed signal.
         *
         * It is placed on the left panel.
         */
        public signal void *notebook_contacts_page_changed_sig(void *fn, void *param, void *event);

        /*
         * Contact list notebook page changing signal.
         *
         * It is placed on the left panel.
         */
        public signal void *notebook_contacts_page_changing_sig(void *fn, void *param, void *event);

        /*
         * Members filter input text control changed signal.
         *
         * It is placed on the right panel.
         */
        public signal void *txt_filter_members_changed_sig(void *fn, void *param, void *event);

        /*
         * Input text control with current typing message changed
         * signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *txt_msg_tab1_changed_sig(void *fn, void *param, void *event);

        /*
         * "Send" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_send_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Audio Call" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_av_audio_call_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Video Call" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_av_video_call_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * A/V Call "Finish" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_av_finish_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * A/V Call "Accept" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_av_accept_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * A/V Call "Reject" button click signal.
         *
         * It is placed on the tab 1 panel.
         */
        public signal void *btn_av_reject_tab1_clicked_sig(void *fn, void *param, void *event);

        /*
         * Check list box with public signal keys of buddy requests select
         * allback.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *check_lst_pub_keys_tab2_selected_sig(void *fn, void *param, void *event);

        /*
         * List box with public signal keys of buddy requests toggle signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *check_lst_pub_keys_tab2_toggled_sig(void *fn, void *param, void *event);

        /*
         * "Accept" button click signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *btn_accept_tab2_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Reject" button click signal.
         *
         * It is placed on the tab 2 panel.
         */
        public signal void *btn_reject_tab2_clicked_sig(void *fn, void *param, void *event);

        /*
         * List control with invites select signal.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *lst_ctrl_invites_tab3_selected_sig(void *fn, void *param, void *event);

        /*
         * List control with invites deselect signal.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *lst_ctrl_invites_tab3_deselected_sig(void *fn, void *param, void *event);

        /*
         * "Accept" button click signal.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_accept_tab3_clicked_sig(void *fn, void *param, void *event);

        /*
         * "Reject" button click signal.
         *
         * It is placed on the tab 3 panel.
         */
        public signal void *btn_reject_tab3_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Typing timeout.
        // After this time notification is sent that we have stopped
        // typing.
        private const uint typing_tout_ms = 1488;

        // Timer starting when answered an A/V call
        private uint timer_status;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Main window width
         */
        public size_t width { public get; public set; }

        /**
         * Main window height
         */
        public size_t height { public get; public set; }

        /**
         * Length of appended to text control with last exchanged
         * messages typing information in characters.
         * Used only if the buddy list tab is opened.
         */
        public ulong txt_conv_buddy_tab1_typing_len { public get; public set construct; }

        /**
         * Is page with conferences opened for the first time?
         */
        public bool page_conferences_opened_first_time { public get; public set construct; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Main window idle handler callback
        private void *idle_handle(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            string? err = null;

            // Lock mutex
            model->mutex.lock();

            // Is Tox iteration needed?
            //
            // We do Tox iteration here in idle handler, because all GUI
            // methods should be called from single thread. Tox callbacks
            // may call such GUI methods.
            // The same for Tox A/V below.
            if (model->tox_need_iteration)
            {
                // Do Tox iteration.
                // Schedule next Tox iteration.
                model->tox_iterate();
                model->tox_next_iteration_schedule();

                // No Tox iteration is needed
                model->tox_need_iteration = false;
            }

            // Is Tox A/V iteration needed?
            if (model->tox_av_need_iteration)
            {
                // Do Tox A/V iteration.
                // Schedule next Tox A/V iteration.
                model->tox_av_iterate();
                model->tox_av_next_iteration_schedule();

                // No Tox A/V iteration is needed
                model->tox_av_need_iteration = false;
            }

            // Initialize plugins if it hadn't been done already
            if (model->should_load_plugins)
            {
                // Set ready flag
                model->should_load_plugins = false;

                // Load plugins.
                //
                // Discovers plugins.
                // Sorts them alphabetically.
                // Loads those plugins which are enabled in per-user
                // configuration for autostart.
                err = model->plugins_load();

                // Fill check list box in "Plugins" dialog with discovered
                // plugins
                ctrl->dlg_plugins.check_lst_plugins_fill();
            }

            // Unlock mutex
            model->mutex.unlock();

            // Show only the first load error if it'd occured
            if (err != null)
            {
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, err, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Main window close callback
        private void *closed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Properly shutdown saving all data
            ctrl->exit();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Main window move callback
        private void *move(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update per-profile config with window position
            int x;
            int y;

            view->frm_main.pos_get(out x, out y);

            model->self.cfg.set_boolean("main", "use_win_pos_x_y", true);
            model->self.cfg.set_integer("main", "x", x);
            model->self.cfg.set_integer("main", "y", y);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Profile" -> "Edit Profile..." menu item click callback
        private void *menu_edit_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Deallocate preview avatar
            ctrl->dlg_edit_profile.changed_avatar = false;

            ctrl->dlg_edit_profile.avatar_16x16  = null;
            ctrl->dlg_edit_profile.avatar_32x32  = null;
            ctrl->dlg_edit_profile.avatar_normal = null;

            // Lock mutex
            model->mutex.lock();

            // Initialize "Edit Profile" dialog widgets.
            // Show the dialog.
            view->dlg_edit_profile.txt_name.focus_set();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_edit_profile.txt_name.val_set(model->self.name);
            ctrl->ctrl_changed_by_user = true;
            bool have_avatar = model->self.avatar_normal != null;
            view->dlg_edit_profile.img_avatar.img_set(!have_avatar ? Model.Person.img_filepath_default_avatar_get() :
                                                                     null,
                                                      model->self.avatar_normal);
            view->dlg_edit_profile.btn_del.en(have_avatar);
            view->dlg_edit_profile.btn_edit_profile.en(false);
            view->dlg_edit_profile.show(true);
            view->dlg_edit_profile.fit();
            //view->dlg_edit_profile.refresh();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Profile" -> "About Profile..." menu item click callback
        private void *menu_about_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Initialize "About Profile" dialog widgets.
            // Show the dialog.
            view->dlg_about.img_avatar.show(true, // Show
                                            true  // Update layout
                                           );
            view->dlg_about.img_avatar.img_set(model->self.avatar_normal == null ? Model.Person.img_filepath_default_avatar_get() :
                                                                                   null,
                                               model->self.avatar_normal);
            view->dlg_about.txt_about_val_person_set(model->self.display_profile_name_get(),
                                                     model->self.name,
                                                     null, // No Alias for self is used
                                                     model->self.addr,
                                                     model->self.display_state_get(true),
                                                     model->self.display_state_user_get(true),
                                                     model->self.status,
                                                     null, // No date and time of the adding to buddy list for self is used
                                                     null  // No last seen date and time for self is used
                                                    );
            view->dlg_about.lbl_set(_("About Profile"));
            view->dlg_about.btn_close_about.focus_set();
            view->dlg_about.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Profile" -> "Change Profile..." menu item click callback
        private void *menu_change_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Disable the profile autoloading.
            // Unlock mutex.
            model->mutex.lock();
            model->cfg.set_boolean("general", "enable_autoload", false);
            model->mutex.unlock();

            // Properly shutdown saving all data.
            // Restart the application.
            ctrl->exit();
            Posix.execvp(ctrl->args[0], ctrl->args);

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Profile" -> "Quit" menu item click callback
        private void *menu_quit_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Properly shutdown saving all data
            ctrl->exit();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Add Buddy..." menu item click callback
        private void *menu_add_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Initialize "Add Buddy" dialog controls.
            // Show the dialog.
            view->dlg_add_buddy.txt_addr.focus_set();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_add_buddy.txt_addr.clr();
            view->dlg_add_buddy.txt_msg.clr();
            ctrl->ctrl_changed_by_user = true;
            view->dlg_add_buddy.txt_Alias.clr();
            view->dlg_add_buddy.btn_add_buddy.en(false);
            view->dlg_add_buddy.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Edit Buddy..." menu item click callback
        private void *menu_edit_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remember which buddy is handled now
            ctrl->handled_contacts.dlg_edit_buddy.buddy = model->peer_buddy;

            Model.Buddy buddy = ctrl->handled_contacts.dlg_edit_buddy.buddy;

            // Initialize "Edit Buddy" dialog controls.
            // Show the dialog.
            view->dlg_edit_buddy.lbl_name_.lbl_set(buddy.name  == null ? _("unknown") : buddy.name);
            view->dlg_edit_buddy.txt_Alias.focus_set();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_edit_buddy.txt_Alias.val_set(buddy.Alias == null ? ""           : buddy.Alias);
            ctrl->ctrl_changed_by_user = true;
            view->dlg_edit_buddy.btn_edit_buddy.en(false);
            view->dlg_edit_buddy.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Remove Buddy" menu item click callback
        private void *menu_remove_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Ask for confirmation
            Model.Buddy buddy = model->peer_buddy;
            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main,
                                                  _("Are you sure you want to remove %s from your buddy list?").printf(buddy.display_name_get()),
                                                  _("Remove Buddy"),
                                                  View.Wrapper.MsgDlg.style_t.CENTR | View.Wrapper.MsgDlg.style_t.OK | View.Wrapper.MsgDlg.style_t.CANCEL);
            View.Wrapper.Dlg.answer_t answer = msg_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Lock mutex
            model->mutex.lock();

            // Remove buddy from the buddy list
            int idx = model->buddies_lst.index_of(buddy);
            GLib.assert(idx != -1);

            lst_ctrl_buddies_change(Ctrl.action_t.REMOVE_ELEM, buddy);

            // Does we have an A/V call with the removed buddy?
            if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) > 0 ||
                (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) > 0)
            {
                // Cancel the A/V call
                av_call_cancel(model->peer_buddy,
                               true,  // Hang up
                               false, // Do not update GUI (we'll update it below by ourselves)
                               true   // Mutex is locked
                              );
            }

            // Remove a buddy from Tox profile
            global::ToxCore.ERR_FRIEND_DELETE err;
            model->tox.friend_delete(buddy.uid, out err);

            switch (err)
            {
            // The function returned successfully
            case global::ToxCore.ERR_FRIEND_DELETE.OK:
                break;

            // There was no buddy with the given buddy number
            case global::ToxCore.ERR_FRIEND_DELETE.FRIEND_NOT_FOUND:
                GLib.assert(false);
                break;

            // Another error
            default:
                GLib.assert(false);
                break;
            }

            // Remove a buddy from buddy list
            switch (buddy.state)
            {
            case Model.Person.state_t.NONE:
                //Model.Buddy.none--;
                GLib.assert(false);
                break;
            case Model.Person.state_t.OFFLINE:
                Model.Buddy.offline--;
                break;
            case Model.Person.state_t.ONLINE_TCP:
                Model.Buddy.online_tcp--;
                break;
            case Model.Person.state_t.ONLINE_UDP:
                Model.Buddy.online_udp--;
                break;
            default:
                GLib.assert(false);
                break;
            }

            // Check whether we are typing a message
            if ((buddy.action_from_us & Model.Buddy.state_t.TYPING_TXT) > 0)
            {
                // Typing was started.
                // Stop timer that should turn it off.
                GLib.Source.remove(buddy.timer_typing_off);
            }

            // Set that we stop typing a message
            buddy.typing_change(model,
                                false // Stop typing
                               );

            // Edit members in member lists.
            //
            // Find the buddy which has been removed.
            // His state icon and avatar now should be disabled, because the
            // found member is not our buddy now.
            foreach (Model.Conference conference in model->conferences_hash.values)
                foreach (Model.Member member in conference.members_hash.values)
                    if (member.person == buddy)
                    {
                        member.person = null;
                        model->member_init(member);

                        lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                     member,

                                                     true, // Set display name column
                                                     true  // Set user state column
                                                    );

                        break;
                    }

            // Remove conference invitations from the removed buddy.
            // Iterate through the all invites.
            size_t cnt = view->frm_main.lst_ctrl_invites_tab3.row_cnt_get();

            if (cnt > 0)
            {
                for (int row = (int) cnt - 1; row >= 0; row--)
                {
                    // Continue if this invite is from the buddy which was removed
                    Model.Model.ConferenceInvitation conference_invitation = model->conference_invitations[row];

                    if (conference_invitation.buddy == buddy)
                    {
                        // Remove conference from the invite list
                        lst_ctrl_invites_tab3_change(Ctrl.action_t.REMOVE_ELEM, conference_invitation);

                        // Delete conference invitation with specified cookie from
                        // database table "conference_invitations"
                        try
                        {
                            model->self.db_conference_invitations_del(conference_invitation.cookie);
                        }
                        catch (Model.ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }
                    }
                }
            }

            // Change menus
            unowned string val = _("&Add to Buddy List");

            view->frm_main.menu_members_action_in_buddy_list.val_set(val + "\tAlt-L");
            view->frm_main.menu_members_action_in_buddy_list_.val_set(val);

            // Delete all messages to/from specified buddy from database
            // table "conv_buddies".
            // Delete buddy from database table "buddies".
            try
            {
                model->self.db_conv_buddies_del(buddy.uid,
                                                null, // No date & time limit, delete all messages
                                                null  // No date & time limit, delete all messages
                                               );
                model->self.db_buddies_del(buddy.uid);

            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Update status bar
            status_val_set(View.FrmMain.STATUS_COL_BUDDIES);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "About Buddy..." menu item click callback
        private void *menu_about_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update buddy last seen
            Model.Buddy buddy = model->peer_buddy;

            if (buddy.state == Model.Person.state_t.ONLINE_TCP ||
                buddy.state == Model.Person.state_t.ONLINE_UDP)
            {
                buddy.datetime_seen_last = new GLib.DateTime.now_local();
            }

            // Initialize "About Buddy" dialog widgets.
            // Show the dialog.
            view->dlg_about.img_avatar.show(true, // Show
                                            true  // Update layout
                                           );
            view->dlg_about.img_avatar.img_set(buddy.avatar_normal == null ? Model.Person.img_filepath_default_avatar_get() :
                                                                             null,
                                               buddy.avatar_normal);
            view->dlg_about.txt_about_val_person_set(null, // No profile name for buddies is used
                                                     buddy.name               == null ? _("unknown") : buddy.name,
                                                     buddy.Alias              == null ? _("unused")  : buddy.Alias,
                                                     buddy.addr               == null ? _("unknown") : buddy.addr,
                                                     buddy.display_state_get(true),
                                                     buddy.display_state_user_get(true),
                                                     buddy.status             == null ? _("unknown") : buddy.status,
                                                     Model.Model.first_ch_lowercase_make(buddy.datetime_add_lst.format("%A, %e %B %Y %H:%M:%S")),
                                                     buddy.datetime_seen_last == null ?
                                                         _("never") :
                                                         Model.Model.first_ch_lowercase_make(buddy.datetime_seen_last.format("%A, %e %B %Y %H:%M:%S")));
            view->dlg_about.lbl_set(_("About Buddy"));
            view->dlg_about.btn_close_about.focus_set();
            view->dlg_about.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Conversation History..." menu item click callback
        private void *menu_conv_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remember which buddy is handled now
            ctrl->handled_contacts.frm_conv.buddy      = model->peer_buddy;
            ctrl->handled_contacts.frm_conv.conference = null;

            // Update date & time tree control and conversation text
            // control
            try
            {
                // Clear and refill date & time tree control.
                // It also enables/disables "Delete" button.
                //
                // Clear and refill conversation text input.
                // It also enables/disables find previous/next button.
                ctrl->frm_conv.tree_datetime_fill(null // Select all messages from database, no restriction by message content
                                                 );
                ctrl->frm_conv.txt_conv_fill(null // Do not highlight any substring in messages
                                            );
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Update other controls.
            // Show the "Conversation History" frame.
            view->frm_conv.srch_conv_val_def_set();
            view->frm_conv.btn_close_conv.focus_set();
            view->frm_conv.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Show" -> "Show all" menu item click callback
        private void *menu_show_all_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_show_buddies_t show_buddies = (Model.Profile.cfg_main_show_buddies_t)
                    model->self.cfg.get_integer("main", "show_buddies");

                switch (show_buddies)
                {
                case Model.Profile.cfg_main_show_buddies_t.SHOW_ALL:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE_BUT:
                    break;
                case Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_show_all.is_checked();
                view->frm_main.menu_show_all_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_show_all_.is_checked();
                view->frm_main.menu_show_all.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "show_buddies", Model.Profile.cfg_main_show_buddies_t.SHOW_ALL);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Show" -> "Hide offline but show unread" menu
        // item click callback
        private void *menu_hide_offline_but_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_show_buddies_t show_buddies = (Model.Profile.cfg_main_show_buddies_t)
                    model->self.cfg.get_integer("main", "show_buddies");

                if (show_buddies == Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE_BUT)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_hide_offline_but.is_checked();
                view->frm_main.menu_hide_offline_but_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_hide_offline_but_.is_checked();
                view->frm_main.menu_hide_offline_but.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "show_buddies", Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE_BUT);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Show" -> "Hide offline" menu item click
        // callback
        private void *menu_hide_offline_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_show_buddies_t show_buddies = (Model.Profile.cfg_main_show_buddies_t)
                    model->self.cfg.get_integer("main", "show_buddies");

                if (show_buddies == Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_hide_offline.is_checked();
                view->frm_main.menu_hide_offline_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_hide_offline_.is_checked();
                view->frm_main.menu_hide_offline.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "show_buddies", Model.Profile.cfg_main_show_buddies_t.HIDE_OFFLINE);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Adding to buddy list" menu item
        // click * callback
        private void *menu_sort_add_lst_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_t sort_buddies = (Model.Profile.cfg_main_sort_buddies_t)
                    model->self.cfg.get_integer("main", "sort_buddies");

                switch (sort_buddies)
                {
                case Model.Profile.cfg_main_sort_buddies_t.ADD_LST:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_sort_buddies_t.DISPLAY_NAME:
                    break;
                case Model.Profile.cfg_main_sort_buddies_t.LAST_MSG:
                    break;
                case Model.Profile.cfg_main_sort_buddies_t.STATE:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_add_lst_buddies.is_checked();
                view->frm_main.menu_sort_add_lst_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_add_lst_buddies_.is_checked();
                view->frm_main.menu_sort_add_lst_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies", Model.Profile.cfg_main_sort_buddies_t.ADD_LST);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Display name" menu item click
        // callback
        private void *menu_sort_display_name_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_t sort_buddies = (Model.Profile.cfg_main_sort_buddies_t)
                    model->self.cfg.get_integer("main", "sort_buddies");

                if (sort_buddies == Model.Profile.cfg_main_sort_buddies_t.DISPLAY_NAME)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_display_name_buddies.is_checked();
                view->frm_main.menu_sort_display_name_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_display_name_buddies_.is_checked();
                view->frm_main.menu_sort_display_name_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies", Model.Profile.cfg_main_sort_buddies_t.DISPLAY_NAME);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Last message" menu item click
        // callback
        private void *menu_sort_last_msg_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_t sort_buddies = (Model.Profile.cfg_main_sort_buddies_t)
                    model->self.cfg.get_integer("main", "sort_buddies");

                if (sort_buddies == Model.Profile.cfg_main_sort_buddies_t.LAST_MSG)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_last_msg_buddies.is_checked();
                view->frm_main.menu_sort_last_msg_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_last_msg_buddies_.is_checked();
                view->frm_main.menu_sort_last_msg_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies", Model.Profile.cfg_main_sort_buddies_t.LAST_MSG);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Network state" menu item click
        // callback
        private void *menu_sort_state_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_t sort_buddies = (Model.Profile.cfg_main_sort_buddies_t)
                    model->self.cfg.get_integer("main", "sort_buddies");

                if (sort_buddies == Model.Profile.cfg_main_sort_buddies_t.STATE)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_state_buddies.is_checked();
                view->frm_main.menu_sort_state_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_state_buddies_.is_checked();
                view->frm_main.menu_sort_state_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies", Model.Profile.cfg_main_sort_buddies_t.STATE);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Ascending" menu item click
        // callback
        private void *menu_sort_asc_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_order_t sort_buddies_order = (Model.Profile.cfg_main_sort_buddies_order_t)
                    model->self.cfg.get_integer("main", "sort_buddies_order");

                if (sort_buddies_order == Model.Profile.cfg_main_sort_buddies_order_t.ASC)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_asc_buddies.is_checked();
                view->frm_main.menu_sort_asc_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_asc_buddies_.is_checked();
                view->frm_main.menu_sort_asc_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies_order", Model.Profile.cfg_main_sort_buddies_order_t.ASC);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Descending" menu item click
        // callback
        private void *menu_sort_desc_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_buddies_order_t sort_buddies_order = (Model.Profile.cfg_main_sort_buddies_order_t)
                    model->self.cfg.get_integer("main", "sort_buddies_order");

                switch (sort_buddies_order)
                {
                case Model.Profile.cfg_main_sort_buddies_order_t.ASC:
                    break;
                case Model.Profile.cfg_main_sort_buddies_order_t.DESC:
                    model->mutex.unlock();
                    return null;
                    //break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_desc_buddies.is_checked();
                view->frm_main.menu_sort_desc_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_desc_buddies_.is_checked();
                view->frm_main.menu_sort_desc_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_buddies_order", Model.Profile.cfg_main_sort_buddies_order_t.DESC);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Buddies" -> "Sort By" -> "Unread are always top" menu item
        // click callback
        private void *menu_sort_unread_top_buddies_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_unread_top_buddies.is_checked();
                view->frm_main.menu_sort_unread_top_buddies_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_unread_top_buddies_.is_checked();
                view->frm_main.menu_sort_unread_top_buddies.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_boolean("main", "sort_buddies_unread_top", flag);

            // Refill buddy list
            lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Add Conference..." menu item click callback
        private void *menu_add_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Create temporary buddy list array.
            // Sort it by display name.
            var buddies_lst = new Gee.ArrayList<Model.Buddy>();
            foreach (Model.Buddy buddy in model->buddies_hash.values)
                buddies_lst.add(buddy);

            buddies_lst.sort((a, b) =>
                {
                    string a_display_name = a.display_name_get();
                    string b_display_name = b.display_name_get();

                    if      (a_display_name >  b_display_name) return  1;
                    else if (a_display_name == b_display_name) return  0;
                    else                                       return -1;
                }
            );

            // Fill list box with buddies with the prepared above array
            view->dlg_add_conference.lst_box_buddies.clr();
            foreach (Model.Buddy buddy in buddies_lst)
                view->dlg_add_conference.lst_box_buddies.append(buddy.display_name_get());

            // Initialize other "Add Conference" dialog controls.
            // Show the dialog.
            view->dlg_add_conference.txt_title.focus_set();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_add_conference.txt_title.clr();
            ctrl->ctrl_changed_by_user = true;
            view->dlg_add_conference.txt_Alias.clr();
            view->dlg_add_conference.lst_box_buddies_invites.clr();
            view->dlg_add_conference.btn_left.en(false);
            view->dlg_add_conference.btn_right.en(false);
            view->dlg_add_conference.btn_add_conference.en(false);
            view->dlg_add_conference.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Edit Conference..." menu item click
        // callback
        private void *menu_edit_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remember which conference is handled now
            ctrl->handled_contacts.dlg_edit_conference.conference = model->peer_conference;

            Model.Conference conference = ctrl->handled_contacts.dlg_edit_conference.conference;

            // Create temporary buddy list array.
            // Sort it by display name.
            var buddies_lst = new Gee.ArrayList<Model.Buddy>();
            foreach (Model.Buddy buddy_ in model->buddies_hash.values)
                buddies_lst.add(buddy_);

            buddies_lst.sort((a, b) =>
                {
                    string a_display_name = a.display_name_get();
                    string b_display_name = b.display_name_get();

                    if      (a_display_name >  b_display_name) return  1;
                    else if (a_display_name == b_display_name) return  0;
                    else                                       return -1;
                }
            );

            // Fill list box with buddies with the prepared above array
            view->dlg_edit_conference.lst_box_buddies.clr();
            foreach (Model.Buddy buddy_ in buddies_lst)
                view->dlg_edit_conference.lst_box_buddies.append(buddy_.display_name_get());

            // Initialize "Edit Conference" dialog controls.
            // Show the dialog.
            view->dlg_edit_conference.txt_title.focus_set();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_edit_conference.txt_title.val_set(conference.title == null ? "" : conference.title);
            view->dlg_edit_conference.txt_Alias.val_set(conference.Alias == null ? "" : conference.Alias);
            ctrl->ctrl_changed_by_user = true;
            view->dlg_edit_conference.lst_box_buddies_invites.clr();
            view->dlg_edit_conference.btn_left.en(false);
            view->dlg_edit_conference.btn_right.en(false);
            view->dlg_edit_conference.btn_edit_conference.en(false);
            view->dlg_edit_conference.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Remove Conference" menu item click callback
        private void *menu_remove_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Ask for confirmation
            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main,
                                                  _("Are you sure you want to remove %s from your conference list?").printf(model->peer_conference.display_name_get()),
                                                  _("Remove Conference"),
                                                  View.Wrapper.MsgDlg.style_t.CENTR | View.Wrapper.MsgDlg.style_t.OK | View.Wrapper.MsgDlg.style_t.CANCEL);
            View.Wrapper.Dlg.answer_t answer = msg_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;
            case View.Wrapper.Dlg.answer_t.CANCEL:
                return null;
                //break;
            default:
                GLib.assert(false);
                break;
            }

            // Lock mutex
            model->mutex.lock();

            // Remove a conference from Tox profile
            Model.Conference conference = model->peer_conference;
            int idx = model->conferences_lst.index_of(conference);
            GLib.assert(idx != -1);

            global::ToxCore.ERR_CONFERENCE_DELETE err;
            model->tox.conference_delete(conference.uid, out err);
            GLib.assert(err == global::ToxCore.ERR_CONFERENCE_DELETE.OK);

            // Remove conference from the conference list.
            // Refill members list.
            // Update status bar.
            lst_ctrl_conferences_change(Ctrl.action_t.REMOVE_ELEM, conference);
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);
            status_val_set(View.FrmMain.STATUS_COL_CONFERENCES);

            // Delete members of specified conference from database table
            // "members".
            // Delete conference from database table "conferences".
            // Delete all messages to/from specified conference from database
            // table "conv_conferences".
            try
            {
                model->self.db_members_del(conference.uid);
                model->self.db_conv_conferences_del(conference.uid,
                                                    null, // No date & time limit, delete all messages
                                                    null  // No date & time limit, delete all messages
                                                   );
                model->self.db_conferences_del(conference.uid);
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "About Conference..." menu item click
        // callback
        private void *menu_about_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Initialize "About Conference" dialog widgets.
            // Show the dialog.
            Model.Conference conference = model->peer_conference;

            view->dlg_about.img_avatar.show(true, // Show
                                            true  // Update layout
                                           );
            view->dlg_about.img_avatar.img_set(Model.Person.img_filepath_default_avatar_get(),
                                               null // Avatar filepath is to be used, do not use avatar blob
                                              );
            view->dlg_about.txt_about_val_conference_set(conference.title == null ? _("unknown") : conference.title,
                                                         conference.Alias == null ? _("unused")  : conference.Alias,
                                                         conference.display_cnt_get(),
                                                         (conference.offline != -1 &&
                                                             conference.online != -1) ? conference.offline + conference.online : 0,
                                                         Model.Model.first_ch_lowercase_make(conference.datetime_add_lst.format("%A, %e %B %Y %H:%M:%S")));
            view->dlg_about.lbl_set(_("About Conference"));
            view->dlg_about.btn_close_about.focus_set();
            view->dlg_about.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Conversation History..." menu item click
        // callback
        private void *menu_conv_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remember which conference is handled now
            ctrl->handled_contacts.frm_conv.buddy      = null;
            ctrl->handled_contacts.frm_conv.conference = model->peer_conference;

            // Update date & time tree control and conversation text
            // control
            try
            {
                // Clear and refill date & time tree control.
                // It also enables/disables "Delete" button.
                //
                // Clear and refill conversation text input.
                // It also enables/disables find previous/next button.
                ctrl->frm_conv.tree_datetime_fill(null // Select all messages from database, no restriction by message content
                                                 );
                ctrl->frm_conv.txt_conv_fill(null // Do not highlight any substring in messages
                                            );
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Update other controls.
            // Show the "Conversation History" frame.
            view->frm_conv.srch_conv_val_def_set();
            view->frm_conv.btn_close_conv.focus_set();
            view->frm_conv.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Adding to conference list"
        // menu item click callback
        private void *menu_sort_add_lst_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_conferences_t sort_conferences = (Model.Profile.cfg_main_sort_conferences_t)
                    model->self.cfg.get_integer("main", "sort_conferences");

                switch (sort_conferences)
                {
                case Model.Profile.cfg_main_sort_conferences_t.ADD_LST:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_sort_conferences_t.DISPLAY_NAME:
                    break;
                case Model.Profile.cfg_main_sort_conferences_t.LAST_MSG:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_add_lst_conferences.is_checked();
                view->frm_main.menu_sort_add_lst_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_add_lst_conferences_.is_checked();
                view->frm_main.menu_sort_add_lst_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_conferences", Model.Profile.cfg_main_sort_conferences_t.ADD_LST);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Display name" menu item click
        // callback
        private void *menu_sort_display_name_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_conferences_t sort_conferences = (Model.Profile.cfg_main_sort_conferences_t)
                    model->self.cfg.get_integer("main", "sort_conferences");

                if (sort_conferences == Model.Profile.cfg_main_sort_conferences_t.DISPLAY_NAME)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_display_name_conferences.is_checked();
                view->frm_main.menu_sort_display_name_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_display_name_conferences_.is_checked();
                view->frm_main.menu_sort_display_name_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_conferences", Model.Profile.cfg_main_sort_conferences_t.DISPLAY_NAME);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Last message" menu item click
        // callback
        private void *menu_sort_last_msg_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_conferences_t sort_conferences = (Model.Profile.cfg_main_sort_conferences_t)
                    model->self.cfg.get_integer("main", "sort_conferences");

                if (sort_conferences == Model.Profile.cfg_main_sort_conferences_t.LAST_MSG)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_last_msg_conferences.is_checked();
                view->frm_main.menu_sort_last_msg_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_last_msg_conferences_.is_checked();
                view->frm_main.menu_sort_last_msg_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_conferences", Model.Profile.cfg_main_sort_conferences_t.LAST_MSG);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Ascending" menu item click
        // callback
        private void *menu_sort_asc_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_conferences_order_t sort_conferences_order = (Model.Profile.cfg_main_sort_conferences_order_t)
                    model->self.cfg.get_integer("main", "sort_conferences_order");

                switch (sort_conferences_order)
                {
                case Model.Profile.cfg_main_sort_conferences_order_t.ASC:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_sort_conferences_order_t.DESC:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_asc_conferences.is_checked();
                view->frm_main.menu_sort_asc_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_asc_conferences_.is_checked();
                view->frm_main.menu_sort_asc_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_conferences_order", Model.Profile.cfg_main_sort_conferences_order_t.ASC);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Descending" menu item click
        // callback
        private void *menu_sort_desc_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_conferences_order_t sort_conferences_order = (Model.Profile.cfg_main_sort_conferences_order_t)
                    model->self.cfg.get_integer("main", "sort_conferences_order");

                if (sort_conferences_order == Model.Profile.cfg_main_sort_conferences_order_t.DESC)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_desc_conferences.is_checked();
                view->frm_main.menu_sort_desc_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_desc_conferences_.is_checked();
                view->frm_main.menu_sort_desc_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_conferences_order", Model.Profile.cfg_main_sort_conferences_order_t.DESC);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Sort By" -> "Unread are always top" menu
        // item click callback
        private void *menu_sort_unread_top_conferences_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_unread_top_conferences.is_checked();
                view->frm_main.menu_sort_unread_top_conferences_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_unread_top_conferences_.is_checked();
                view->frm_main.menu_sort_unread_top_conferences.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_boolean("main", "sort_conferences_unread_top", flag);

            // Refill conference list
            lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "Add to Buddy List" / "Find in
        // Buddy List" menu item click callback
        private void *menu_members_action_in_buddy_list_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Find selected member
            uint idx;
            size_t cnt = view->frm_main.lst_ctrl_members_tab1.row_cnt_get();
            Model.Member? member = null;

            for (idx = 0; idx < cnt; idx++)
            {
                if (view->frm_main.lst_ctrl_members_tab1.sel_get(idx))
                {
                    member = model->members_lst[(int) idx];
                    break;
                }
            }

            GLib.assert(member != null);

            // Do action with member
            if (member.person == null)
            {
                // Member is not our buddy and is not ourself.
                // Add him to buddy list.
                GLib.assert(member.name != null);
                string dbg = _("Adding buddy: %s: ").printf(member.name);

                try
                {
                    // Add buddy without sending a buddy request
                    uint8[]? key = member.key_get();
                    GLib.assert(key != null);

                    uint32 uid = model->buddy_no_req_add(key);

                    // Add buddy to the buddy list
                    var buddy = new Model.Buddy(uid,
                                                null, // Tox ID is unknown
                                                null  // Alias is unused
                                               );
                    model->buddy_init(buddy);
                    lst_ctrl_buddies_change(Ctrl.action_t.ADD_ELEM, buddy);

                    // His state icon and avatar now should be enabled, because the
                    // added member is our buddy now.
                    member.person = buddy;
                    member.name   = null;
                    member.key    = null;

                    lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                 member,

                                                 true, // Set display name column
                                                 true  // Set user state column
                                                );

                    // Change menus
                    unowned string val = _("&Open in Buddy List");

                    view->frm_main.menu_members_action_in_buddy_list.val_set(val + "\tAlt-L");
                    view->frm_main.menu_members_action_in_buddy_list_.val_set(val);

                    // Insert new buddy into database table "buddies"
                    try
                    {
                        int64 add_lst = model->self.db_buddies_insert(uid,
                                                                      null, // Tox ID is unknown
                                                                      null, // Alias is unused
                                                                      Model.Buddy.unread_t.FALSE,
                                                                      buddy.datetime_add_lst,
                                                                      null, // No last seen date and time
                                                                      null, // No avatar
                                                                      null, // No avatar
                                                                      null  // No avatar
                                                                     );

                        if (add_lst != -1 &&
                                add_lst != 0)
                        {
                            buddy.add_lst = add_lst;
                        }
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // Update status bar
                    status_val_set(View.FrmMain.STATUS_COL_BUDDIES);

                    GLib.debug(dbg + _("OK"));
                }
                catch (Model.ErrModelAddBuddy ex)
                {
                    // Unlock mutex
                    model->mutex.unlock();

                    GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                    var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                    msg_dlg.modal_show();

                    return null;
                }
            }
            else if (member.person is Model.Profile)
            {
                // Member is ourself.
                // Can't go here.
                GLib.assert(false);
            }
            //else if (member.person is Model.Buddy)
            else
            {
                // Member is our buddy.
                // Open him in buddy list.
                GLib.assert(member.person is Model.Buddy);
                var buddy = member.person as Model.Buddy;

                // Do the buddy selection:
                //  - update GUI controls,
                //  - select last messages from database and append them to text
                //    control
                lst_ctrl_buddies_sel(buddy);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "About Member..." menu item
        // click callback
        private void *menu_about_member_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Find selected member
            uint idx;
            size_t cnt = view->frm_main.lst_ctrl_members_tab1.row_cnt_get();
            Model.Member? member = null;

            for (idx = 0; idx < cnt; idx++)
            {
                if (view->frm_main.lst_ctrl_members_tab1.sel_get(idx))
                {
                    member = model->members_lst[(int) idx];
                    break;
                }
            }

            GLib.assert(member != null);

            // Initialize "About Member" dialog widgets.
            // Show the dialog.
            string is_buddy;

            if (member.person == null)
            {
                is_buddy = _("no");

                view->dlg_about.img_avatar.show(false, // Hide
                                                true   // Update layout
                                               );
            }
            else
            {
                if (member.person is Model.Profile)
                {
                    is_buddy = _("no, the member is myself");
                }
                else
                {
                    GLib.assert(member.person is Model.Buddy);
                    is_buddy = _("yes");
                }

                Model.Person person = member.person;

                view->dlg_about.img_avatar.show(true, // Show
                                                true  // Update layout
                                               );
                view->dlg_about.img_avatar.img_set(person.avatar_normal == null ? Model.Person.img_filepath_default_avatar_get() :
                                                                                  null,
                                                   person.avatar_normal);
            }

            view->dlg_about.txt_about_val_member_set(member.display_name_get(),
                                                     is_buddy);
            view->dlg_about.lbl_set(_("About Member"));
            view->dlg_about.btn_close_about.focus_set();
            view->dlg_about.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "Sort By" -> "Adding to member
        // list" menu item click callback
        private void *menu_sort_add_lst_members_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_members_t sort_members = (Model.Profile.cfg_main_sort_members_t)
                    model->self.cfg.get_integer("main", "sort_members");

                switch (sort_members)
                {
                case Model.Profile.cfg_main_sort_members_t.ADD_LST:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_sort_members_t.DISPLAY_NAME:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_add_lst_members.is_checked();
                view->frm_main.menu_sort_add_lst_members_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_add_lst_members_.is_checked();
                view->frm_main.menu_sort_add_lst_members.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_members", Model.Profile.cfg_main_sort_members_t.ADD_LST);

            // Refill member list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "Sort By" -> "Display name"
        // menu item click callback
        private void *menu_sort_display_name_members_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_members_t sort_members = (Model.Profile.cfg_main_sort_members_t)
                    model->self.cfg.get_integer("main", "sort_members");

                if (sort_members == Model.Profile.cfg_main_sort_members_t.DISPLAY_NAME)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_display_name_members.is_checked();
                view->frm_main.menu_sort_display_name_members_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_display_name_members_.is_checked();
                view->frm_main.menu_sort_display_name_members.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_members", Model.Profile.cfg_main_sort_members_t.DISPLAY_NAME);

            // Refill member list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "Sort By" -> "Ascending" menu
        // item click callback
        private void *menu_sort_asc_members_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_members_order_t sort_members_order = (Model.Profile.cfg_main_sort_members_order_t)
                    model->self.cfg.get_integer("main", "sort_members_order");

                switch (sort_members_order)
                {
                case Model.Profile.cfg_main_sort_members_order_t.ASC:
                    model->mutex.unlock();
                    return null;
                    //break;
                case Model.Profile.cfg_main_sort_members_order_t.DESC:
                    break;
                default:
                    model->mutex.unlock();
                    return null;
                    //break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_asc_members.is_checked();
                view->frm_main.menu_sort_asc_members_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_asc_members_.is_checked();
                view->frm_main.menu_sort_asc_members.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_members_order", Model.Profile.cfg_main_sort_members_order_t.ASC);

            // Refill member list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Conferences" -> "Members" -> "Sort By" -> "Descending" menu
        // item click callback
        private void *menu_sort_desc_members_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

#if __WXMSW__
            // Under M$ Windows this radiobutton callback will be called
            // even if it is already checked. Do nothing in this case.
            try
            {
                // Already checked?
                // Unlock mutex and exit.
                Model.Profile.cfg_main_sort_members_order_t sort_members_order = (Model.Profile.cfg_main_sort_members_order_t)
                    model->self.cfg.get_integer("main", "sort_members_order");

                if (sort_members_order == Model.Profile.cfg_main_sort_members_order_t.DESC)
                {
                    model->mutex.unlock();
                    return null;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
#endif

            // Get/set state of menu item
            bool flag;

            if (param == null)
            {
                flag = view->frm_main.menu_sort_desc_members.is_checked();
                view->frm_main.menu_sort_desc_members_.check(flag);
            }
            else
            {
                flag = view->frm_main.menu_sort_desc_members_.is_checked();
                view->frm_main.menu_sort_desc_members.check(flag);
            }

            // Update per-profile config with new flag
            model->self.cfg.set_integer("main", "sort_members_order", Model.Profile.cfg_main_sort_members_order_t.DESC);

            // Refill member list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with conferences right click callback.
        //
        // It is placed on the left panel.
        private void *lst_ctrl_conferences_right_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Pops up the context menu.
            // Unlock mutex.
            //model->mutex.lock(); // Can't lock here because popup window blocks
            view->frm_main.lst_ctrl_conferences.menu_popup(view->frm_main.menu_conferences_,
                                                           true // Use current mouse position
                                                          );
            //model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with conferences select callback.
        //
        // It is placed on the conference list panel.
        private void *lst_ctrl_conferences_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Find selected peer
            uint idx;
            size_t cnt = view->frm_main.lst_ctrl_conferences.row_cnt_get();
            Model.Conference? conference = null;

            for (idx = 0; idx < cnt; idx++)
            {
                if (view->frm_main.lst_ctrl_conferences.sel_get(idx))
                {
                    conference = model->conferences_lst[(int) idx];
                    break;
                }
            }

            GLib.assert(conference != null);

            // Open tab 1 in main notebook
            view->frm_main.notebook_main.page_sel_set(View.FrmMain.NOTEBOOK_MAIN_PAGE_CONVERSATIONS);

            // Determine new peer
            model->peer_conference = conference;

            // Check if there are unread messages from the conference
            if (conference.have_unread_msg == Model.Conference.unread_t.TRUE)
            {
                // Have unread messages from him was before.
                // Set there are no unread messages.
                conference.have_unread_msg = Model.Conference.unread_t.FALSE;

                // Update conference list,
                // set normal icon in conference list (not unread)
                lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                            conference,

                                            false, // Don't set display name column
                                            false, // Don't set count column
                                            true   // Set unread state column
                                           );
            }

            // Refill members list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Update other controls
            view->frm_main.menu_edit_conference.en(true);
            view->frm_main.menu_remove_conference.en(true);
            view->frm_main.menu_about_conference.en(true);
            view->frm_main.menu_conv_conference.en(true);
            view->frm_main.menu_edit_conference_.en(true);
            view->frm_main.menu_remove_conference_.en(true);
            view->frm_main.menu_about_conference_.en(true);
            view->frm_main.menu_conv_conference_.en(true);
            view->frm_main.txt_conv_conference_tab1.clr();
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_msg_tab1.focus_set();
            view->frm_main.txt_msg_tab1.clr();
            ctrl->ctrl_changed_by_user = true;
            view->frm_main.txt_msg_tab1.en(true);
            view->frm_main.btn_send_tab1.en(false);

            // Select last messages from database and append them to text
            // control
            try
            {
                // Select next message from database table "conv_conferences"
                // written by/for peer with specific uid
                string? name;
                uint    fg;
                uint    bg;
                string? msg;
                GLib.DateTime? datetime;

                Sqlite.Statement? stmt = null;

                while (model->self.db_conv_conferences_sel(ref stmt,

                                                           out name,
                                                           conference.uid,
                                                           out fg,
                                                           out bg,
                                                           out msg,
                                                           out datetime,

                                                           null, // No date & time limit, just limit to number of outputted messages below
                                                           null, // No date & time limit, just limit to number of outputted messages below

                                                           View.FrmMain.TXT_CONV_TAB1_LIMIT))
                {
                    // Append next message to text control with last exchanged
                    // messages from/to the conference
                    view->frm_main.txt_conv_conference_tab1_append(name,
                                                                   fg,
                                                                   bg,
                                                                   msg,
                                                                   datetime);
                }
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Warning!
            // I disabled code below because it's buggy.
            // It may freeze the app.

            // Scroll to end of text control with last exchanged messages
            // from/to the conference
            //ulong len = view->frm_main.txt_conv_conference_tab1.len_get();
            //view->frm_main.txt_conv_conference_tab1.pos_show(len);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with conferences deselect callback.
        //
        // It is placed on the conference list panel.
        private void *lst_ctrl_conferences_deselected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like row removing.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // No peer
            model->peer_conference = null;

            // Refill members list
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

            // Update controls
            view->frm_main.menu_edit_conference.en(false);
            view->frm_main.menu_remove_conference.en(false);
            view->frm_main.menu_about_conference.en(false);
            view->frm_main.menu_conv_conference.en(false);
            view->frm_main.menu_edit_conference_.en(false);
            view->frm_main.menu_remove_conference_.en(false);
            view->frm_main.menu_about_conference_.en(false);
            view->frm_main.menu_conv_conference_.en(false);
            view->frm_main.txt_conv_conference_tab1.clr();
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_msg_tab1.clr();
            ctrl->ctrl_changed_by_user = true;
            view->frm_main.txt_msg_tab1.en(false);
            view->frm_main.btn_send_tab1.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Tools" -> "Plugins" menu item click callback
        private void *menu_plugins_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Initialize "Plugins" dialog controls.
            // Show the dialog.
            // Unlock mutex.
            model->mutex.lock();
            ctrl->ctrl_changed_by_user = false;
            view->dlg_plugins.check_lst_plugins.focus_set();
            view->dlg_plugins.show(true);
            ctrl->ctrl_changed_by_user = true;
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Tools" -> "Settings" menu item click callback
        private void *menu_settings_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Reset tab selection.
            // Initialize "Settings" dialog controls.
            // Show the dialog.
            view->dlg_settings.notebook.page_sel_set(View.DlgSettings.NOTEBOOK_PAGE_GENERAL);
            view->dlg_settings.check_show_profile_tab1.focus_set();
            ctrl->dlg_settings.reset(DlgSettings.reset_target_t.SETTINGS_APPLIED_LAST);
            view->dlg_settings.show(true);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Help" -> "In Loving Memory of..." menu item click callback
        private void *menu_Vika_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Initialize "In Loving Memory of" dialog controls.
            // Show the dialog.
            // Unlock mutex.
            model->mutex.lock();
            view->dlg_Vika.btn_close_Vika.focus_set();
            view->dlg_Vika.show(true);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Help" -> "About..." menu item click callback
        private void *menu_about_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Copying
            unowned string copying = _("""This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.""");

            // Show about dialog
            var about_dlg = new View.Wrapper.AboutDlg(view->frm_main,

                                                      model->icon_filepath_program_get(),
                                                      Model.Model.ICON_WIDTH,
                                                      Model.Model.ICON_HEIGHT,

                                                      Cfg.NAME,
                                                      "%u.%u.%u%s".printf(Cfg.Ver.MAJOR, Cfg.Ver.MINOR, Cfg.Ver.MICRO, Cfg.Ver.NAME == "" ? "" : " \"" + Cfg.Ver.NAME + "\""),
                                                      _("Internet messenger using decentralized (P2P) Tox protocol"),
                                                      copying,
                                                      "https://gitlab.com/neva_blyad/yat",
                                                      _("Copyright © 2021-2023 НЕВСКИЙ БЛЯДИНА\n<neva_blyad@lovecry.pt>\n<neva_blyad@lovecri.es>"),
                                                      {_("НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>"), _("Invisible Light"), _("I wish there was somebody else...")},
                                                      {_("I wish there was somebody...")},
                                                      {_("I wish there was somebody...")});
            about_dlg.modal_show();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Button for changing self user state click callback
        private void *btn_bmp_state_user_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Pops up the context menu.
            // Unlock mutex.
            //model->mutex.lock(); // Can't lock here because popup window blocks
            view->frm_main.btn_bmp_state_user.menu_popup(view->frm_main.menu_state_user);
            //model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Vertical splitter sash position changing
        private void *splitter_vertical_sash_changing(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Set new width of buddy list columns.
            // Set new width of conference list columns.
            // Set new width of member list columns.
            // Set new width of the invite list columns.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_main.lst_ctrl_buddies_width_set();
            view->frm_main.lst_ctrl_conferences_width_set();
            view->frm_main.lst_ctrl_members_tab1_width_set();
            view->frm_main.lst_ctrl_invites_tab3_width_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with buddies right click callback.
        //
        // It is placed on the left panel.
        private void *lst_ctrl_buddies_right_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Pops up the context menu.
            // Unlock mutex.
            //model->mutex.lock(); // Can't lock here because popup window blocks
            view->frm_main.lst_ctrl_buddies.menu_popup(view->frm_main.menu_buddies_,
                                                       true // Use current mouse position
                                                      );
            //model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with buddies select callback.
        //
        // It is placed on the buddy list panel.
        private void *lst_ctrl_buddies_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Find selected peer
            uint         idx;
            size_t       cnt   = view->frm_main.lst_ctrl_buddies.row_cnt_get();
            Model.Buddy? buddy = null;

            for (idx = 0; idx < cnt; idx++)
            {
                if (view->frm_main.lst_ctrl_buddies.sel_get(idx))
                {
                    buddy = model->buddies_lst[(int) idx];
                    break;
                }
            }

            GLib.assert(buddy != null);

            // Do the buddy selection:
            //  - update GUI controls,
            //  - select last messages from database and append them to text
            //    control
            lst_ctrl_buddies_sel(buddy);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with buddies deselect callback.
        //
        // It is placed on the buddy list panel.
        private void *lst_ctrl_buddies_deselected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like row removing.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Do the buddy deselection.
            // It updates GUI controls.
            lst_ctrl_buddies_sel(null);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Status & buddy filter choice select callback.
        //
        // It is placed on the left panel.
        private void *choice_status_and_filter_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Clear text input control.
            // Get index of selected choice.
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_status_and_filter.clr();
            ctrl->ctrl_changed_by_user = true;
            uint idx = view->frm_main.choice_status_and_filter.sel_get();

            switch (idx)
            {
            // Status
            case View.FrmMain.CHOICE_IDX_STATUS:

                // Show, enable/disable "Apply" Button.
                // Refill buddy list.
                // Refill conference list.
                view->frm_main.btn_apply_status.show(true, // Show
                                                     true  // Update layout
                                                    );
                btn_apply_status_en_set();
                lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);
                lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);
                break;

            // Buddy filter
            case View.FrmMain.CHOICE_IDX_FILTER:

                // Hide "Apply" Button
                view->frm_main.btn_apply_status.show(false, // Hide
                                                     true   // Update layout
                                                    );
                break;

            // Something else
            default:
                GLib.assert(false);
                break;
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Status & buddy filter input text control changed callback.
        //
        // It is placed on the left panel.
        private void *txt_status_and_filter_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get index of selected choice
            uint idx = view->frm_main.choice_status_and_filter.sel_get();

            switch (idx)
            {
            // Status
            case View.FrmMain.CHOICE_IDX_STATUS:

                // Enable/disable "Apply" button
                btn_apply_status_en_set();
                break;

            // Buddy filter
            case View.FrmMain.CHOICE_IDX_FILTER:

                // Refill buddy list.
                // Refill conference list.
                lst_ctrl_buddies_change(Ctrl.action_t.UPD_LST);
                lst_ctrl_conferences_change(Ctrl.action_t.UPD_LST);
                break;

            // Something else
            default:
                GLib.assert(false);
                break;
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Apply" button click callback.
        //
        // It is placed on the left panel.
        private void *btn_apply_status_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get current typed status.
            // Update controls.
            model->self.status = view->frm_main.txt_status_and_filter.val_get();
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_status_and_filter.clr();
            ctrl->ctrl_changed_by_user = true;
            view->frm_main.lbl_status.lbl_set(model->self.status);
            btn_apply_status_en_set();

            // Set status to Tox profile
            global::ToxCore.ERR_SET_INFO err;
            model->tox.self_set_status_message(model->self.status.data, out err);
            GLib.assert(err == global::ToxCore.ERR_SET_INFO.OK);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Vertical splitter sash position changing.
        //
        // It is placed on the tab 1 panel.
        private void *splitter_vertical_tab1_sash_changing(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Set new width of member list columns.
            // Unlock mutex.
            model->mutex.lock();
            view->frm_main.lst_ctrl_members_tab1_width_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with members right click callback.
        //
        // It is placed on the right panel.
        private void *lst_ctrl_members_tab1_right_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex.
            // Pops up the context menu.
            // Unlock mutex.
            //model->mutex.lock(); // Can't lock here because popup window blocks
            view->frm_main.lst_ctrl_members_tab1.menu_popup(view->frm_main.menu_members_,
                                                            true // Use current mouse position
                                                           );
            //model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with members select callback.
        //
        // It is placed on the right panel.
        private void *lst_ctrl_members_tab1_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Find selected member
            uint idx;
            size_t cnt = view->frm_main.lst_ctrl_members_tab1.row_cnt_get();
            Model.Member? member = null;

            for (idx = 0; idx < cnt; idx++)
            {
                if (view->frm_main.lst_ctrl_members_tab1.sel_get(idx))
                {
                    member = model->members_lst[(int) idx];
                    break;
                }
            }

            GLib.assert(member != null);

            // Change menus
            bool flag;
            unowned string val;

            if (member.person == null)
            {
                flag = true;
                val  = _("&Add to Buddy List");
            }
            else if (member.person is Model.Profile)
            {
                flag = false;
                val  = _("&Add to Buddy List");
            }
            //else if (member.person is Model.Buddy)
            else
            {
                GLib.assert(member.person is Model.Buddy);

                flag = true;
                val  = _("&Open in Buddy List");
            }

            view->frm_main.menu_about_member.en(true);
            view->frm_main.menu_members_action_in_buddy_list.en(flag);
            view->frm_main.menu_members_action_in_buddy_list_.en(flag);

            view->frm_main.menu_about_member_.en(true);
            view->frm_main.menu_members_action_in_buddy_list.val_set(val + "\tAlt-L");
            view->frm_main.menu_members_action_in_buddy_list_.val_set(val);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with members deselect callback.
        //
        // It is placed on the right panel.
        private void *lst_ctrl_members_tab1_deselected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like row removing.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Change menus
            bool flag          = false;
            unowned string val = _("&Add to Buddy List");

            view->frm_main.menu_about_member.en(false);
            view->frm_main.menu_members_action_in_buddy_list.en(flag);
            view->frm_main.menu_members_action_in_buddy_list_.en(flag);

            view->frm_main.menu_about_member_.en(false);
            view->frm_main.menu_members_action_in_buddy_list.val_set(val + "\tAlt-L");
            view->frm_main.menu_members_action_in_buddy_list_.val_set(val);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Available" menu item click callback.
        //
        // It is placed in context menu for changing self user state.
        private void *menu_avail_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Do nothing if already in available user state
            if (model->self.state_user != Model.Person.state_user_t.AVAIL)
            {
                // Set the available user state
                model->self.state_user = Model.Person.state_user_t.AVAIL;
                model->tox.status      = global::ToxCore.UserStatus.NONE;

                GLib.debug(_("State: %s"), model->self.display_state_user_get(true));

                // Update member list,
                // set new state icon
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == model->self)
                        {
                            lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                         member,

                                                         false, // Don't set display name column
                                                         true   // Set user state column
                                                        );

                            break;
                        }

                // Update icon with self user state
                try
                {
                    view->frm_main.btn_bmp_state_user.img_set(
                        model->self.icon_filepath_state_user_get(model->self.cfg.get_integer("appearance", "avatar_size") == Model.Profile.cfg_appearance_avatar_size_t.SMALL));
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Away" menu item click callback.
        //
        // It is placed in context menu for changing self user state.
        private void *menu_away_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Do nothing if already in away user state
            if (model->self.state_user != Model.Person.state_user_t.AWAY)
            {
                // Set the available user state
                model->self.state_user = Model.Person.state_user_t.AWAY;
                model->tox.status      = global::ToxCore.UserStatus.AWAY;

                GLib.debug(_("State: %s"), model->self.display_state_user_get(true));

                // Update member list,
                // set new state icon
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == model->self)
                        {
                            lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                         member,

                                                         false, // Don't set display name column
                                                         true   // Set user state column
                                                        );

                            break;
                        }

                // Update icon with self user state
                try
                {
                    view->frm_main.btn_bmp_state_user.img_set(
                        model->self.icon_filepath_state_user_get(model->self.cfg.get_integer("appearance", "avatar_size") == Model.Profile.cfg_appearance_avatar_size_t.SMALL));
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Busy" menu item click callback.
        //
        // It is placed in context menu for changing self user state.
        private void *menu_busy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Do nothing if already in busy user state
            if (model->self.state_user != Model.Person.state_user_t.BUSY)
            {
                // Set the available user state
                model->self.state_user = Model.Person.state_user_t.BUSY;
                model->tox.status      = global::ToxCore.UserStatus.BUSY;

                GLib.debug(_("State: %s"), model->self.display_state_user_get(true));

                // Update member list,
                // set new state icon
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == model->self)
                        {
                            lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                         member,

                                                         false, // Don't set display name column
                                                         true   // Set user state column
                                                        );

                            break;
                        }

                // Update icon with self user state
                try
                {
                    view->frm_main.btn_bmp_state_user.img_set(
                        model->self.icon_filepath_state_user_get(model->self.cfg.get_integer("appearance", "avatar_size") == Model.Profile.cfg_appearance_avatar_size_t.SMALL));
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Contact list notebook page changed callback.
        //
        // It is placed on the left panel.
        private void *notebook_contacts_page_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like page selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update controls
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_msg_tab1.clr();
            ctrl->ctrl_changed_by_user = true;
            view->frm_main.btn_send_tab1.en(false);

            // What page is selected?
            uint page = view->frm_main.notebook_contacts.page_sel_get();

            switch (page)
            {
            case View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES:

                // Show/hide GUI widgets
                view->frm_main.splitter_vertical_tab1.show(false, // Hide
                                                           false  // Don't update layout
                                                          );
                view->frm_main.txt_conv_buddy_tab1.show(true, // Show
                                                        true  // Update layout
                                                       );

                view->frm_main.txt_msg_tab1.en(model->peer_buddy != null);

                break;

            case View.FrmMain.NOTEBOOK_CONTACTS_PAGE_CONFERENCES:

                // Show/hide GUI widgets
                view->frm_main.txt_conv_buddy_tab1.show(false, // Hide
                                                        false  // Don't update layout
                                                       );
                view->frm_main.splitter_vertical_tab1.show(true, // Show
                                                           true  // Update layout
                                                          );

                // Set new width of member list columns.
                //
                // (The code below is hack.
                // I should call this method when the program starts,
                // but it then works incorrectly.)
                if (this.page_conferences_opened_first_time)
                {
                    this.page_conferences_opened_first_time = false;
                    view->frm_main.lst_ctrl_members_tab1_width_set();
                }

                view->frm_main.txt_msg_tab1.en(model->peer_conference != null);

                break;

            default:

                GLib.assert(false);
                break;
            }

            // A/V call GUIs elements
            av_widgets_upd();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Contact list notebook page changing callback.
        //
        // It is placed on the left panel.
        private void *notebook_contacts_page_changing(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Nothing to do

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Members filter input text control changed callback.
        //
        // It is placed on the right panel.
        private void *txt_filter_members_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Refill member list.
            // Unlock mutex.
            model->mutex.lock();
            lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Input text control with current typing message changed
        // callback.
        //
        // It is placed on the tab 1 panel.
        private void *txt_msg_tab1_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Continue only if buddy list is opened and if someone is
            // selected
            uint page = view->frm_main.notebook_contacts.page_sel_get();

            if (page == View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES &&
                model->peer_buddy != null)
            {
                // Check whether we are already typing a message
                if ((model->peer_buddy.action_from_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                {
                    // Typing is already started.
                    // Stop timer that should turn it off. (We start it again below.)
                    GLib.Source.remove(model->peer_buddy.timer_typing_off);
                }

                // Set we are currently typing a message
                model->peer_buddy.typing_change(model,
                                                true // Start typing
                                               );

                // Remembed peer.
                // It will be captured by timer callback closure below.
                Model.Buddy peer = model->peer_buddy;

                // Setup timer that will turn off the typing in Tox profile
                // after some amount of time
                model->peer_buddy.timer_typing_off = GLib.Timeout.add(typing_tout_ms, () =>
                {
                    // Lock mutex.
                    // Set that we stop typing a message.
                    // Unlock mutex.
                    model->mutex.lock();
                    peer.typing_change(model,
                                       false // Stop typing
                                      );
                    model->mutex.unlock();

                    // Stop timer
                    return GLib.Source.REMOVE;
                });
            }

            // Enable/disable "Send" button
            ulong len = view->frm_main.txt_msg_tab1.len_get();
            view->frm_main.btn_send_tab1.en(len > 0);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Send" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_send_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Send message
            try
            {
                // Get current typed message
                string msg = view->frm_main.txt_msg_tab1.val_get();

                if (msg.length == 0)
                {
                    // Unlock mutex
                    model->mutex.unlock();
                    return null;
                }

                // What page is selected?
                uint page = view->frm_main.notebook_contacts.page_sel_get();

                switch (page)
                {
                case View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES:

                    // Send a text chat message to a current peer
                    Model.Buddy buddy = model->peer_buddy;

                    global::ToxCore.ERR_FRIEND_SEND_MESSAGE err;
                    model->tox.friend_send_message(buddy.uid,
                                                   global::ToxCore.MessageType.NORMAL,
                                                   msg.data,
                                                   out err);

                    switch (err)
                    {
                    // The function returned successfully
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.OK:
                        break;

                    // One of the arguments to the function was NULL
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.NULL:
                        GLib.assert(false);
                        break;

                    // The buddy number did not designate a valid buddy
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.FRIEND_NOT_FOUND:
                        GLib.assert(false);
                        break;

                    // This client is currently not connected to the buddy
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.FRIEND_NOT_CONNECTED:
                        throw new ErrFrmMainSendMsgBuddy.BUDDY_NOT_CONNECTED_ERR(_("This client is currently not connected to the buddy"));
                        //break;

                    // An allocation error occurred while increasing the send queue
                    // size
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.SENDQ:
                        throw new ErrFrmMainSendMsgBuddy.MEM_ALLOC(_("An allocation error occurred while increasing the send queue size"));
                        //break;

                    // Message length exceeded global::ToxCore.MAX_MESSAGE_LENGTH
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.TOO_LONG:
                        throw new ErrFrmMainSendMsgBuddy.TOO_LONG(_("Message length exceeded %d bytes").printf(global::ToxCore.MAX_MESSAGE_LENGTH));
                        //break;

                    // Attempted to send a zero-length message
                    case global::ToxCore.ERR_FRIEND_SEND_MESSAGE.EMPTY:
                        GLib.assert(false);
                        break;

                    // Another error
                    default:
                        GLib.assert(false);
                        break;
                    }

                    // Check whether we are typing a message
                    if ((buddy.action_from_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                    {
                        // Typing was started.
                        // Stop timer that should turn it off.
                        GLib.Source.remove(buddy.timer_typing_off);
                    }

                    // Set that we stop typing a message
                    buddy.typing_change(model,
                                        false // Stop typing
                                       );

                    // Append new message to text control with last exchanged
                    // messages
                    var    datetime     = new GLib.DateTime.now_local();
                    string display_name = model->self.display_name_get();

                    buddy.datetime_last_msg = datetime;
                    buddy.last_msg          = (int64) Model.Buddy.last_add_lst++;

                    GLib.debug(_("Outcoming message to buddy: %s: %d characters"), buddy.display_name_get(),
                                                                                   msg.char_count());

                    view->frm_main.txt_conv_buddy_tab1_append(display_name,
                                                              true, // Message from us (not peer)
                                                              msg,
                                                              datetime);

                    // Clear current typing message.
                    // Disable "Send" button.
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.txt_msg_tab1.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_main.btn_send_tab1.en(false);

                    // Insert new message into database table
                    // "conv_buddies"
                    try
                    {
                        int64 last_msg = model->self.db_conv_buddies_insert(display_name,
                                                                            buddy.uid,
                                                                            true, // Message from us (not peer)
                                                                            msg,
                                                                            datetime);

                        if (last_msg != -1 &&
                                last_msg != 0)
                        {
                            buddy.last_msg = last_msg;
                        }
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // The buddy position in buddy list may changed if sorting by
                    // last message is active.
                    //
                    // Update buddy list.
                    lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                            buddy,

                                            false, // Don't set display name column
                                            false, // Don't set status column
                                            false, // Don't set state column
                                            false  // Don't set user state column
                                           );

                    break;

                case View.FrmMain.NOTEBOOK_CONTACTS_PAGE_CONFERENCES:

                    // Send a text chat message to a current peer
                    Model.Conference conference = model->peer_conference;

                    global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE err;
                    bool res = model->tox.conference_send_message(conference.uid,
                                                                  global::ToxCore.MessageType.NORMAL,
                                                                  msg.data,
                                                                  out err);

                    switch (err)
                    {
                    // The function returned successfully
                    case global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.OK:
                        GLib.assert(res);
                        break;

                    // The conference number passed did not designate a valid
                    // conference
                    case global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.CONFERENCE_NOT_FOUND:
                        GLib.assert(!res);
                        GLib.assert(false);
                        break;

                    // The message is too long
                    case global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.TOO_LONG:
                        GLib.assert(!res);
                        GLib.assert(false);
                        break;

                    // The client is not connected to the conference
                    case global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.NO_CONNECTION:
                        GLib.assert(!res);
                        throw new ErrFrmMainSendMsgConference.NO_CONNECTION(_("The client is not connected to the conference"));
                        //break;

                    // The message packet failed to send
                    case global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.FAIL_SEND:
                        GLib.assert(!res);
                        throw new ErrFrmMainSendMsgConference.SEND_FAIL(_("The message packet failed to send"));
                        //break;

                    // Another error
                    default:
                        GLib.assert(false);
                        break;
                    }

                    // Find ourself as member of the conference
                    Model.Member? self = null;

                    foreach (Model.Member member in conference.members_hash.values)
                    {
                        if (member.person == model->self)
                        {
                            self = member;
                            break;
                        }
                    }

                    GLib.assert(self != null);

                    // Append new message to text control with last exchanged
                    // messages
                    var    datetime     = new GLib.DateTime.now_local();
                    string display_name = model->self.display_name_get();

                    conference.datetime_last_msg = datetime;
                    conference.last_msg          = (int64) Model.Conference.last_add_lst++;

                    GLib.debug(_("Outcoming message to conference: %s: %d characters"), conference.display_name_get(),
                                                                                        msg.char_count());

                    view->frm_main.txt_conv_conference_tab1_append(display_name,
                                                                   self.fg,
                                                                   self.bg,
                                                                   msg,
                                                                   datetime);

                    // Clear current typing message.
                    // Disable "Send" button.
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.txt_msg_tab1.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_main.btn_send_tab1.en(false);

                    // Insert new message into database table
                    // "conv_conferences"
                    try
                    {
                        int64 last_msg = model->self.db_conv_conferences_insert(display_name,
                                                                                conference.uid,
                                                                                self.fg,
                                                                                self.bg,
                                                                                msg,
                                                                                datetime);

                        if (last_msg != -1 &&
                                last_msg != 0)
                        {
                            conference.last_msg = last_msg;
                        }
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // The conference position may changed if sorting by last
                    // message is active.
                    //
                    // Update conference list.
                    lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                                conference,

                                                false, // Don't set display name column
                                                false, // Don't set count column
                                                false  // Don't set unread state column
                                               );

                    break;

                default:

                    GLib.assert(false);
                    break;
                }
            }
            catch (GLib.Error ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(ex.message);
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg.modal_show();

                return null;
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Audio Call" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_av_audio_call_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Do the A/V call
            av_call(model->peer_buddy,
                    true, // Do the call
                    false // Audio call
                   );

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Video Call" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_av_video_call_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Do the A/V call
            av_call(model->peer_buddy,
                    true, // Do the call
                    true  // Video call
                   );

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // A/V Call "Finish" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_av_finish_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Cancel the A/V call
            av_call_cancel(model->peer_buddy,
                           true,  // Hang up
                           true,  // Update GUI
                           false  // Mutex is not locked
                          );

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // A/V Call "Accept" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_av_accept_tab1_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Available multimedia
            Model.Buddy buddy = model->peer_buddy;
            bool video = (buddy.action_from_us & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0 ||
                         (buddy.action_to_us   & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0;

            // Answer the A/V call
            ctrl->frm_main.av_answer_answered(buddy,
                                              true, // Buddy calls, we answered
                                              video,
                                              false // Mutex is not locked
                                             );

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // A/V Call "Reject" button click callback.
        //
        // It is placed on the tab 1 panel.
        private void *btn_av_reject_tab1_clicked(void *fn, void *param, void *event)
        {
            // Almost the same behavior as for A/V Call "Finish" button
            return btn_av_finish_tab1_clicked(fn, param, event);
        }

        /*----------------------------------------------------------------------------*/

        // Check list box with private keys of buddy requests select
        // callback.
        //
        // It is placed on the tab 2 panel.
        private void *check_lst_pub_keys_tab2_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Update the buddy request message.
            // Unlock mutex.
            model->mutex.lock();
            txt_msg_tab2_upd();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List box with private keys of buddy requests toggle callback.
        //
        // It is placed on the tab 2 panel.
        private void *check_lst_pub_keys_tab2_toggled(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Iterate through the all private keys
            size_t cnt          = view->frm_main.check_lst_pub_keys_tab2.cnt_get();
            bool   checked_flag = false;

            for (uint idx = 0; idx < cnt; idx++)
            {
                // Get state of the private key
                checked_flag = view->frm_main.check_lst_pub_keys_tab2.is_checked(idx);
                if (checked_flag)
                    break;
            }

            // Update tab 2 buttons
            view->frm_main.btn_accept_tab2.en(checked_flag);
            view->frm_main.btn_reject_tab2.en(checked_flag);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Accept" button click callback.
        //
        // It is placed on the tab 2 panel.
        private void *btn_accept_tab2_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            string? err = null;

            // Lock mutex
            model->mutex.lock();

            // Iterate through the all private keys
            size_t cnt = view->frm_main.check_lst_pub_keys_tab2.cnt_get();
            GLib.assert(cnt > 0);

            for (int idx = (int) cnt - 1; idx >= 0; idx--)
            {
                // Get state of the private key.
                // Accept only checked private keys.
                bool checked_flag = view->frm_main.check_lst_pub_keys_tab2.is_checked(idx);

                if (checked_flag)
                {
                    // Add buddy
                    Model.Model.BuddyReq buddy_req = model->buddy_reqs[idx];
                    string dbg = _("Buddy accepting: %s: ").printf(buddy_req.key_str);

                    try
                    {
                        // Add buddy without sending a buddy request
                        uint32 uid = model->buddy_no_req_add(buddy_req.key);

                        // Add buddy to the buddy list
                        var buddy = new Model.Buddy(uid,
                                                    null, // Tox ID is unknown
                                                    null  // Alias is unused
                                                   );
                        model->buddy_init(buddy);
                        lst_ctrl_buddies_change(Ctrl.action_t.ADD_ELEM, buddy);

                        // Edit members in member lists.
                        //
                        // Find the buddy which has been added.
                        // His state icon and avatar now should be enabled, because the
                        // found member is our buddy now.
                        foreach (Model.Conference conference in model->conferences_hash.values)
                            foreach (Model.Member member in conference.members_hash.values)
                                if (member.person == null)
                                {
                                    uint8[]? key = member.key_get();
                                    GLib.assert(key != null);

                                    if (Posix.memcmp(key,
                                                     buddy.key,
                                                     key.length) == 0)
                                    {
                                        member.person = buddy;
                                        member.name   = null;
                                        member.key    = null;

                                        lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                     member,

                                                                     true, // Set display name column
                                                                     true  // Set user state column
                                                                    );

                                        break;
                                    }
                                }

                        // Remove buddy request.
                        // Update check list box.
                        model->buddy_reqs.remove_at(idx);
                        view->frm_main.check_lst_pub_keys_tab2.remove((uint) idx);

                        // Insert new buddy into database table "buddies".
                        // Delete buddy request with specified private key from database
                        // table "buddy_reqs".
                        try
                        {
                            int64 add_lst = model->self.db_buddies_insert(uid,
                                                                          null, // Tox ID is unknown
                                                                          null, // Alias is unused
                                                                          Model.Buddy.unread_t.FALSE,
                                                                          buddy.datetime_add_lst,
                                                                          null, // No last seen date and time
                                                                          null, // No avatar
                                                                          null, // No avatar
                                                                          null  // No avatar
                                                                         );

                            if (add_lst != -1 &&
                                    add_lst != 0)
                            {
                                buddy.add_lst = add_lst;
                            }

                            model->self.db_buddy_reqs_del(buddy_req.key);
                        }
                        catch (Model.ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }

                        // Update status bar
                        status_val_set(View.FrmMain.STATUS_COL_BUDDIES);

                        GLib.debug(dbg + _("OK"));
                    }
                    catch (Model.ErrModelAddBuddy ex)
                    {
                        GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                        if (err == null)
                            err = ex.message;
                    }
                }
            }

            // Disable the tab 2 buttons
            if (err == null)
            {
                view->frm_main.btn_accept_tab2.en(false);
                view->frm_main.btn_reject_tab2.en(false);
            }

            // Update the buddy request message
            txt_msg_tab2_upd();

            // Unlock mutex
            model->mutex.unlock();

            // Show the buddy accepting error if it occurs
            if (err != null)
            {
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, err, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Reject" button click callback.
        //
        // It is placed on the tab 2 panel.
        private void *btn_reject_tab2_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remove checked private keys from incoming buddy requests.
            // Iterate through the all private keys
            size_t cnt = view->frm_main.check_lst_pub_keys_tab2.cnt_get();
            GLib.assert(cnt > 0);

            for (int idx = (int) cnt - 1; idx >= 0; idx--)
            {
                // Get state of the private key.
                // Reject only checked private keys.
                bool flag = view->frm_main.check_lst_pub_keys_tab2.is_checked(idx);

                if (flag)
                {
                    Model.Model.BuddyReq buddy_req = model->buddy_reqs[idx];

                    // Remove buddy request.
                    // Also remove from check list box.
                    model->buddy_reqs.remove_at(idx);
                    view->frm_main.check_lst_pub_keys_tab2.remove((uint) idx);

                    // Delete buddy request with specified private key from database
                    // table "buddy_reqs"
                    try
                    {
                        model->self.db_buddy_reqs_del(buddy_req.key);
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }
                }
            }

            // Update the buddy request message
            txt_msg_tab2_upd();

            // Disable the tab 2 buttons
            view->frm_main.btn_accept_tab2.en(false);
            view->frm_main.btn_reject_tab2.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with invites select callback.
        //
        // It is placed on the tab 3 panel.
        private void *lst_ctrl_invites_tab3_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable the tab 3 buttons if there are the invite selections
            size_t sel = view->frm_main.lst_ctrl_invites_tab3.sel_cnt_get();

            if (sel == 1)
            {
                view->frm_main.btn_accept_tab3.en(true);
                view->frm_main.btn_reject_tab3.en(true);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List control with invites deselect callback.
        //
        // It is placed on the tab 3 panel.
        private void *lst_ctrl_invites_tab3_deselected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like row removing.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Disable the tab 3 buttons if there are the invite selections
            size_t sel = view->frm_main.lst_ctrl_invites_tab3.sel_cnt_get();

            if (sel == 0)
            {
                view->frm_main.btn_accept_tab3.en(false);
                view->frm_main.btn_reject_tab3.en(false);
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Accept" button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_accept_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            string? err = null;

            // Lock mutex
            model->mutex.lock();

            // Iterate through the all invites
            size_t cnt = view->frm_main.lst_ctrl_invites_tab3.row_cnt_get();
            GLib.assert(cnt > 0);

            for (int idx = (int) cnt - 1; idx >= 0; idx--)
            {
                // Get state of the invitation.
                // Accept only the selected invites.
                bool flag = view->frm_main.lst_ctrl_invites_tab3.sel_get(idx);

                if (flag)
                {
                    // Join a conference
                    Model.Model.ConferenceInvitation conference_invitation = model->conference_invitations[idx];
                    string dbg = _("Conference joining: %s: %s: ").printf(conference_invitation.buddy.display_name_get(),
                                                                          conference_invitation.cookie_str);

                    try
                    {
                        // Join a conference that the client has been invited to
                        global::ToxCore.ERR_CONFERENCE_JOIN err_;
                        uint32 uid = model->tox.conference_join(conference_invitation.buddy.uid,
                                                                conference_invitation.cookie,
                                                                out err_);

                        switch (err_)
                        {
                        // The function returned successfully
                        case global::ToxCore.ERR_CONFERENCE_JOIN.OK:
                            break;

                        // The cookie passed has an invalid length
                        case global::ToxCore.ERR_CONFERENCE_JOIN.INVALID_LENGTH:
                            throw new ErrFrmMainJoinConference.INVALID_LEN(_("The cookie passed has an invalid length"));
                            //break;

                        // The conference is not the expected type. This indicates an
                        // invalid cookie.
                        case global::ToxCore.ERR_CONFERENCE_JOIN.WRONG_TYPE:
                            throw new ErrFrmMainJoinConference.WRONG_TYPE(_("The conference is not the expected type. This indicates an invalid cookie."));
                            //break;

                        // The buddy number passed does not designate a valid friend
                        case global::ToxCore.ERR_CONFERENCE_JOIN.FRIEND_NOT_FOUND:
                            GLib.assert(false);
                            break;

                        // Client is already in this conference
                        case global::ToxCore.ERR_CONFERENCE_JOIN.DUPLICATE:
                            throw new ErrFrmMainJoinConference.DUPLICATE(_("Client is already in this conference"));
                            //break;

                        // Conference instance failed to initialize
                        case global::ToxCore.ERR_CONFERENCE_JOIN.INIT_FAIL:
                            GLib.assert(false);
                            break;

                        // The join packet failed to send
                        case global::ToxCore.ERR_CONFERENCE_JOIN.FAIL_SEND:
                            throw new ErrFrmMainJoinConference.SEND_FAIL(_("The join packet failed to send"));
                            //break;

                        // Another error
                        default:
                            GLib.assert(false);
                            break;
                        }

                        // Add conference to the conference list
                        var conference = new Model.Conference(uid,
                                                              null // Alias is unused
                                                             );
                        model->conference_init(conference);
                        lst_ctrl_conferences_change(Ctrl.action_t.ADD_ELEM, conference);

                        // Remove conference from the invite list
                        lst_ctrl_invites_tab3_change(Ctrl.action_t.REMOVE_ELEM, conference_invitation);

                        // Update status bar
                        status_val_set(View.FrmMain.STATUS_COL_CONFERENCES);

                        // Insert new conference into database table "conferences".
                        // Insert new member into database table "members".
                        // Delete conference invitation with specified cookie from
                        // database table "conference_invitations".
                        try
                        {
                            int64 add_lst = model->self.db_conferences_insert(uid,
                                                                              null, // Alias is unused
                                                                              Model.Conference.unread_t.FALSE,
                                                                              conference.datetime_add_lst);

                            if (add_lst != -1 &&
                                    add_lst != 0)
                            {
                                conference.add_lst = add_lst;
                            }

                            model->self.db_members_insert(model->self.key,
                                                          conference.member_fg++,
                                                          conference.member_bg++,
                                                          conference.uid);
                            model->self.db_conference_invitations_del(conference_invitation.cookie);
                        }
                        catch (Model.ErrProfileDB ex)
                        {
                            GLib.debug(ex.message);
                        }

                        GLib.debug(dbg + _("OK"));
                    }
                    catch (GLib.Error ex)
                    {
                        GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                        if (err == null)
                            err = ex.message;
                    }
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            // Show the conference joining error if it occurs
            if (err != null)
            {
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, err, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Reject" button click callback.
        //
        // It is placed on the tab 3 panel.
        private void *btn_reject_tab3_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Remove selections from the incoming conference invitations.
            // Iterate through the all invites.
            size_t cnt = view->frm_main.lst_ctrl_invites_tab3.row_cnt_get();
            GLib.assert(cnt > 0);

            for (int row = (int) cnt - 1; row >= 0; row--)
            {
                // Get state of the invitation.
                // Reject only the selected invites.
                bool flag = view->frm_main.lst_ctrl_invites_tab3.sel_get(row);

                if (flag)
                {
                    // Remove conference from the invite list
                    Model.Model.ConferenceInvitation conference_invitation = model->conference_invitations[row];
                    lst_ctrl_invites_tab3_change(Ctrl.action_t.REMOVE_ELEM, conference_invitation);

                    // Delete conference invitation with specified cookie from
                    // database table "conference_invitations"
                    try
                    {
                        model->self.db_conference_invitations_del(conference_invitation.cookie);
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }
                }
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Main window callback constructor.
         *
         * Connects signals to callback methods.
         */
        public FrmMain()
        {
            // GObject-style construction
            GLib.Object(txt_conv_buddy_tab1_typing_len     : 0,
                        page_conferences_opened_first_time : true);

            // Timer starting when answered an A/V call
            this.timer_status = 0;

            // Main window size
            this.width  = 0;
            this.height = 0;

            // Connect signals to callback methods
            this.idle_handle_sig.connect(idle_handle);
            this.closed_sig.connect(closed);
            this.move_sig.connect(move);
            this.menu_edit_profile_clicked_sig.connect(menu_edit_profile_clicked);
            this.menu_about_profile_clicked_sig.connect(menu_about_profile_clicked);
            this.menu_change_profile_clicked_sig.connect(menu_change_profile_clicked);
            this.menu_quit_clicked_sig.connect(menu_quit_clicked);
            this.menu_add_buddy_clicked_sig.connect(menu_add_buddy_clicked);
            this.menu_edit_buddy_clicked_sig.connect(menu_edit_buddy_clicked);
            this.menu_remove_buddy_clicked_sig.connect(menu_remove_buddy_clicked);
            this.menu_about_buddy_clicked_sig.connect(menu_about_buddy_clicked);
            this.menu_conv_buddy_clicked_sig.connect(menu_conv_buddy_clicked);
            this.menu_show_all_clicked_sig.connect(menu_show_all_clicked);
            this.menu_hide_offline_but_clicked_sig.connect(menu_hide_offline_but_clicked);
            this.menu_hide_offline_clicked_sig.connect(menu_hide_offline_clicked);
            this.menu_sort_add_lst_buddies_clicked_sig.connect(menu_sort_add_lst_buddies_clicked);
            this.menu_sort_display_name_buddies_clicked_sig.connect(menu_sort_display_name_buddies_clicked);
            this.menu_sort_last_msg_buddies_clicked_sig.connect(menu_sort_last_msg_buddies_clicked);
            this.menu_sort_state_buddies_clicked_sig.connect(menu_sort_state_buddies_clicked);
            this.menu_sort_asc_buddies_clicked_sig.connect(menu_sort_asc_buddies_clicked);
            this.menu_sort_desc_buddies_clicked_sig.connect(menu_sort_desc_buddies_clicked);
            this.menu_sort_unread_top_buddies_clicked_sig.connect(menu_sort_unread_top_buddies_clicked);
            this.menu_add_conference_clicked_sig.connect(menu_add_conference_clicked);
            this.menu_edit_conference_clicked_sig.connect(menu_edit_conference_clicked);
            this.menu_remove_conference_clicked_sig.connect(menu_remove_conference_clicked);
            this.menu_about_conference_clicked_sig.connect(menu_about_conference_clicked);
            this.menu_conv_conference_clicked_sig.connect(menu_conv_conference_clicked);
            this.menu_sort_add_lst_conferences_clicked_sig.connect(menu_sort_add_lst_conferences_clicked);
            this.menu_sort_display_name_conferences_clicked_sig.connect(menu_sort_display_name_conferences_clicked);
            this.menu_sort_last_msg_conferences_clicked_sig.connect(menu_sort_last_msg_conferences_clicked);
            this.menu_sort_asc_conferences_clicked_sig.connect(menu_sort_asc_conferences_clicked);
            this.menu_sort_desc_conferences_clicked_sig.connect(menu_sort_desc_conferences_clicked);
            this.menu_sort_unread_top_conferences_clicked_sig.connect(menu_sort_unread_top_conferences_clicked);
            this.menu_members_action_in_buddy_list_clicked_sig.connect(menu_members_action_in_buddy_list_clicked);
            this.menu_about_member_clicked_sig.connect(menu_about_member_clicked);
            this.menu_sort_add_lst_members_clicked_sig.connect(menu_sort_add_lst_members_clicked);
            this.menu_sort_display_name_members_clicked_sig.connect(menu_sort_display_name_members_clicked);
            this.menu_sort_asc_members_clicked_sig.connect(menu_sort_asc_members_clicked);
            this.menu_sort_desc_members_clicked_sig.connect(menu_sort_desc_members_clicked);
            this.lst_ctrl_conferences_right_clicked_sig.connect(lst_ctrl_conferences_right_clicked);
            this.lst_ctrl_conferences_selected_sig.connect(lst_ctrl_conferences_selected);
            this.lst_ctrl_conferences_deselected_sig.connect(lst_ctrl_conferences_deselected);
            this.menu_plugins_clicked_sig.connect(menu_plugins_clicked);
            this.menu_settings_clicked_sig.connect(menu_settings_clicked);
            this.menu_Vika_clicked_sig.connect(menu_Vika_clicked);
            this.menu_about_clicked_sig.connect(menu_about_clicked);
            this.btn_bmp_state_user_clicked_sig.connect(btn_bmp_state_user_clicked);
            this.splitter_vertical_sash_changing_sig.connect(splitter_vertical_sash_changing);
            this.lst_ctrl_buddies_right_clicked_sig.connect(lst_ctrl_buddies_right_clicked);
            this.lst_ctrl_buddies_selected_sig.connect(lst_ctrl_buddies_selected);
            this.lst_ctrl_buddies_deselected_sig.connect(lst_ctrl_buddies_deselected);
            this.choice_status_and_filter_selected_sig.connect(choice_status_and_filter_selected);
            this.txt_status_and_filter_changed_sig.connect(txt_status_and_filter_changed);
            this.btn_apply_status_clicked_sig.connect(btn_apply_status_clicked);
            this.splitter_vertical_tab1_sash_changing_sig.connect(splitter_vertical_tab1_sash_changing);
            this.lst_ctrl_members_tab1_right_clicked_sig.connect(lst_ctrl_members_tab1_right_clicked);
            this.lst_ctrl_members_tab1_selected_sig.connect(lst_ctrl_members_tab1_selected);
            this.lst_ctrl_members_tab1_deselected_sig.connect(lst_ctrl_members_tab1_deselected);
            this.menu_avail_clicked_sig.connect(menu_avail_clicked);
            this.menu_away_clicked_sig.connect(menu_away_clicked);
            this.menu_busy_clicked_sig.connect(menu_busy_clicked);
            this.notebook_contacts_page_changed_sig.connect(notebook_contacts_page_changed);
            this.notebook_contacts_page_changing_sig.connect(notebook_contacts_page_changing);
            this.txt_filter_members_changed_sig.connect(txt_filter_members_changed);
            this.txt_msg_tab1_changed_sig.connect(txt_msg_tab1_changed);
            this.btn_send_tab1_clicked_sig.connect(btn_send_tab1_clicked);
            this.btn_av_audio_call_tab1_clicked_sig.connect(btn_av_audio_call_tab1_clicked);
            this.btn_av_video_call_tab1_clicked_sig.connect(btn_av_video_call_tab1_clicked);
            this.btn_av_finish_tab1_clicked_sig.connect(btn_av_finish_tab1_clicked);
            this.btn_av_accept_tab1_clicked_sig.connect(btn_av_accept_tab1_clicked);
            this.btn_av_reject_tab1_clicked_sig.connect(btn_av_reject_tab1_clicked);
            this.check_lst_pub_keys_tab2_selected_sig.connect(check_lst_pub_keys_tab2_selected);
            this.check_lst_pub_keys_tab2_toggled_sig.connect(check_lst_pub_keys_tab2_toggled);
            this.btn_accept_tab2_clicked_sig.connect(btn_accept_tab2_clicked);
            this.btn_reject_tab2_clicked_sig.connect(btn_reject_tab2_clicked);
            this.lst_ctrl_invites_tab3_selected_sig.connect(lst_ctrl_invites_tab3_selected);
            this.lst_ctrl_invites_tab3_deselected_sig.connect(lst_ctrl_invites_tab3_deselected);
            this.btn_accept_tab3_clicked_sig.connect(btn_accept_tab3_clicked);
            this.btn_reject_tab3_clicked_sig.connect(btn_reject_tab3_clicked);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set self avatar
         */
        public void img_avatar_set()
        {
            try
            {
                if (model->self.cfg.get_integer("appearance", "avatar_size") ==
                        Model.Profile.cfg_appearance_avatar_size_t.SMALL)
                {
                    if (model->self.avatar_16x16 == null)
                    {
                        view->frm_main.img_avatar.img_set(
                            Model.Person.icon_filepath_default_avatar_get(true),
                            null // Avatar filepath is to be used, do not use avatar blob
                        );
                    }
                    else
                    {
                        view->frm_main.img_avatar.img_set(
                            null, // Avatar blob is to be used, do not use avatar filepath
                            model->self.avatar_16x16
                        );
                    }
                }
                else
                {
                    if (model->self.avatar_32x32 == null)
                    {
                        view->frm_main.img_avatar.img_set(
                            Model.Person.icon_filepath_default_avatar_get(false),
                            null // Avatar filepath is to be used, do not use avatar blob
                        );
                    }
                    else
                    {
                        view->frm_main.img_avatar.img_set(
                            null, // Avatar blob is to be used, do not use avatar filepath
                            model->self.avatar_32x32
                        );
                    }
                }
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * The method changes the buddy list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding objects, hash and array.
         *
         * @param action           Action
         * @param buddy            Buddy
         * @param col_display_name false: don't change display name column,
         *                         true:  change display name column
         * @param col_status       false: don't change status column,
         *                         true:  change status column
         * @param col_state        false: don't change network state column,
         *                         true:  change network state column
         * @param col_state_user   false: don't change user state column,
         *                         true:  change user state column
         */
        public void lst_ctrl_buddies_change(
            Ctrl.action_t action,

            Model.Buddy? buddy = null,

            bool col_display_name = false,
            bool col_status       = false,
            bool col_state        = false,
            bool col_state_user   = false)

            requires ((action == Ctrl.action_t.ADD_ELEM    && buddy != null) ||
                      (action == Ctrl.action_t.EDIT_ELEM   && buddy != null) ||
                      (action == Ctrl.action_t.REMOVE_ELEM && buddy != null) ||
                      (action == Ctrl.action_t.UPD_LST     && buddy == null))

            requires (action == Ctrl.action_t.EDIT_ELEM || (!col_display_name &&
                                                            !col_status       &&
                                                            !col_state        &&
                                                            !col_state_user))
        {
            // Get index of selected status & buddy filter choice
            uint idx_choice = view->frm_main.choice_status_and_filter.sel_get();
            bool use_filter = idx_choice == View.FrmMain.CHOICE_IDX_FILTER;

            // Use filter?
            GLib.Regex? pattern = null;

            if (use_filter)
            {
                // Get current typed buddy filter
                string filter = view->frm_main.txt_status_and_filter.val_get();

                try
                {
                    // This regular expression will be used for buddy filtering
                    pattern = new GLib.Regex("\\Q%s\\E".printf(filter), GLib.RegexCompileFlags.CASELESS);
                }
                catch (RegexError ex)
                {
                    GLib.assert(false);
                }
            }

            // Fill buddy list entirely or change one row only?
            bool removed_peer = false;

            switch (action)
            {
            case Ctrl.action_t.ADD_ELEM:

                // Add buddy to the buddy list
                model->buddies_hash[buddy.uid] = buddy;

                // Should buddy be showed in GUI?
                if (buddy.should_be_displayed(model, use_filter, pattern))
                {
                    // Buddy is to be shown in GUI.
                    // Find its place in the buddy list.
                    uint row = 0;
                    GLib.CompareDataFunc<Model.Buddy> cmp_fn = Model.Buddy.cmp_fn_get(model);

                    foreach (Model.Buddy buddy_ in model->buddies_lst)
                    {
                        if (cmp_fn(buddy_, buddy) != -1)
                            break;
                        row++;
                    }

                    // Add him to the buddy list.
                    // Also update list control.
                    model->buddies_lst.insert((int) row, buddy);
                    view->frm_main.lst_ctrl_buddies.row_add(row);
                    lst_ctrl_buddies_val_set(row,
                                             buddy,

                                             true, // Set display name column
                                             true, // Set status column
                                             true, // Set state column
                                             true  // Set user state column
                                            );
                }

                break;

            case Ctrl.action_t.EDIT_ELEM:

                // Should buddy be showed in GUI?
                int idx = model->buddies_lst.index_of(buddy);

                if (buddy.should_be_displayed(model, use_filter, pattern))
                {
                    // Buddy is to be shown in GUI.
                    // Check if it is already there.
                    if (idx == -1)
                    {
                        // Buddy is not in the buddy list yet.
                        // Find its place in the buddy list.
                        idx = 0;
                        GLib.CompareDataFunc<Model.Buddy> cmp_fn = Model.Buddy.cmp_fn_get(model);

                        foreach (Model.Buddy buddy_ in model->buddies_lst)
                        {
                            if (cmp_fn(buddy_, buddy) != -1)
                                break;
                            idx++;
                        }

                        // Add buddy to the buddy list.
                        // Also update list control.
                        model->buddies_lst.insert((int) idx, buddy);
                        view->frm_main.lst_ctrl_buddies.row_add(idx);
                        lst_ctrl_buddies_val_set(idx,
                                                 buddy,

                                                 true, // Set display name column
                                                 true, // Set status column
                                                 true, // Set state column
                                                 true  // Set user state column
                                                );
                    }
                    else
                    {
                        // Buddy is already in the buddy list.
                        // Find its place in the buddy list.
                        uint idx_new = 0;
                        GLib.CompareDataFunc<Model.Buddy> cmp_fn = Model.Buddy.cmp_fn_get(model);

                        foreach (Model.Buddy buddy_ in model->buddies_lst)
                        {
                            if (buddy_ == buddy)
                                continue;
                            if (cmp_fn(buddy_, buddy) != -1)
                                break;
                            idx_new++;
                        }

                        // The position may be the same or may differs
                        if (idx_new == idx)
                        {
                            // Position is the same.
                            // Just update list control.
                            lst_ctrl_buddies_val_set(idx,
                                                     buddy,

                                                     col_display_name,
                                                     col_status,
                                                     col_state,
                                                     col_state_user);
                        }
                        else
                        {
                            // Position is the new.
                            // Remove buddy from the buddy list.
                            model->buddies_lst.remove_at(idx);
                            ctrl->ctrl_changed_by_user = false;
                            view->frm_main.lst_ctrl_buddies.row_remove(idx);
                            ctrl->ctrl_changed_by_user = true;

                            // Add it again now to the new position
                            model->buddies_lst.insert((int) idx_new, buddy);
                            view->frm_main.lst_ctrl_buddies.row_add(idx_new);
                            lst_ctrl_buddies_val_set(idx_new,
                                                     buddy,

                                                     true, // Set display name column
                                                     true, // Set status column
                                                     true, // Set state column
                                                     true  // Set user state column
                                                    );

                            // Reselect list control row
                            if (model->peer_buddy == buddy)
                            {
                                ctrl->ctrl_changed_by_user = false;
                                view->frm_main.lst_ctrl_buddies.sel_set(idx_new, true);
                                ctrl->ctrl_changed_by_user = true;
                            }
                        }
                    }
                }
                else
                {
                    // Buddy is not to be shown in GUI.
                    // Remove buddy from the buddy list it is there.
                    // Also update list control.
                    if (idx != -1)
                    {
                        model->buddies_lst.remove_at(idx);
                        ctrl->ctrl_changed_by_user = false;
                        view->frm_main.lst_ctrl_buddies.row_remove(idx);
                        ctrl->ctrl_changed_by_user = true;

                        if (model->peer_buddy == buddy)
                            removed_peer = true;
                    }
                }

                break;

            case Ctrl.action_t.REMOVE_ELEM:

                // Remove buddy from the buddy list
                model->buddies_hash.unset(buddy.uid);

                // Is buddy showed in list control?
                int idx = model->buddies_lst.index_of(buddy);

                if (idx != -1)
                {
                    // Buddy is showed in GUI.
                    // Remove buddy from the buddy list.
                    // Also update list control.
                    model->buddies_lst.remove_at(idx);
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.lst_ctrl_buddies.row_remove(idx);
                    ctrl->ctrl_changed_by_user = true;

                    if (model->peer_buddy == buddy)
                        removed_peer = true;
                }

                break;

            case Ctrl.action_t.UPD_LST:

                // Filter buddy list.
                // Hide offline buddies if according setting was set.
                model->buddies_lst.clear();

                foreach (Model.Buddy buddy_ in model->buddies_hash.values)
                {
                    // Should buddy be showed in GUI?
                    if (buddy_.should_be_displayed(model, use_filter, pattern))
                    {
                        // Buddy is to be shown in GUI.
                        // Add him to the buddy list.
                        model->buddies_lst.add(buddy_);
                    }
                }

                // Sort buddies
                GLib.CompareDataFunc<Model.Buddy> fn = Model.Buddy.cmp_fn_get(model);
                model->buddies_lst.sort((owned) fn);

                // Fill list control
                view->frm_main.lst_ctrl_buddies.clr();

                for (uint row = 0; row < model->buddies_lst.size; row++)
                {
                    Model.Buddy buddy_ = model->buddies_lst[(int) row];

                    view->frm_main.lst_ctrl_buddies.row_add(row);
                    lst_ctrl_buddies_val_set(row,
                                             buddy_,

                                             true, // Set display name column
                                             true, // Set status column
                                             true, // Set state column
                                             true  // Set user state column
                                            );
                }

                // No peer anymore?
                // Or select previosly selected peer in list control.
                if (model->peer_buddy != null)
                {
                    int sel = model->buddies_lst.index_of(model->peer_buddy);

                    if (sel == -1)
                        removed_peer = true;
                    else
                    {
                        ctrl->ctrl_changed_by_user = false;
                        view->frm_main.lst_ctrl_buddies.sel_set(sel, true);
                        ctrl->ctrl_changed_by_user = true;
                    }
                }

                break;

            default:
                GLib.assert(false);
                break;
            }

            // No peer anymore?
            if (removed_peer)
            {
                // Update other GUI controls
                view->frm_main.menu_edit_buddy.en(false);
                view->frm_main.menu_remove_buddy.en(false);
                view->frm_main.menu_about_buddy.en(false);
                view->frm_main.menu_conv_buddy.en(false);
                view->frm_main.menu_edit_buddy_.en(false);
                view->frm_main.menu_remove_buddy_.en(false);
                view->frm_main.menu_about_buddy_.en(false);
                view->frm_main.menu_conv_buddy_.en(false);
                view->frm_main.txt_conv_buddy_tab1.clr();
                this.txt_conv_buddy_tab1_typing_len = 0;

                // What page is selected?
                uint page = view->frm_main.notebook_contacts.page_sel_get();

                if (page == View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES)
                {
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.txt_msg_tab1.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_main.txt_msg_tab1.en(false);
                    view->frm_main.btn_send_tab1.en(false);
                }

                // A/V call GUIs elements
                av_widgets_upd();

                // No peer anymore
                model->peer_buddy = null;
            }
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Set buddy to a list control
         *
         * @param row              Row
         * @param buddy            Buddy
         * @param col_display_name false: don't change display name column,
         *                         true:  change display name column
         * @param col_status       false: don't change status column,
         *                         true:  change status column
         * @param col_state        false: don't change network state column,
         *                         true:  change network state column
         * @param col_state_user   false: don't change user state column,
         *                         true:  change user state column
         */
        public void lst_ctrl_buddies_val_set(uint row,
                                             Model.Buddy buddy,

                                             bool col_display_name,
                                             bool col_status,
                                             bool col_state,
                                             bool col_state_user)
        {
            bool is_small;

            try
            {
                is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                    Model.Profile.cfg_appearance_avatar_size_t.SMALL;
            }
            catch (GLib.KeyFileError ex)
            {
                is_small = false;
                GLib.assert(false);
            }

            if (col_display_name)
            {
                unowned uint8[]? avatar = is_small ? buddy.avatar_16x16 :
                                                     buddy.avatar_32x32;
                View.Wrapper.Ctrl.colour_t colour_bg;

                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AUDIO_CALL) > 0 ||
                    (buddy.action_to_us   & Model.Buddy.state_t.MASK_AUDIO_CALL) > 0)
                {
                    colour_bg = View.Wrapper.Ctrl.colour_t.GREEN;
                }
                else if ((buddy.action_from_us & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0 ||
                         (buddy.action_to_us   & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0)
                {
                    colour_bg = View.Wrapper.Ctrl.colour_t.CYAN;
                }
                else
                {
                    colour_bg = (buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0 ?
                        View.Wrapper.Ctrl.colour_t.GREY :
                        View.Wrapper.Ctrl.colour_t.WHITE;
                }

                view->frm_main.lst_ctrl_buddies.val_set(row,
                                                        View.FrmMain.LST_CTRL_BUDDIES_COL_DISPLAY_NAME,
                                                        buddy.display_name_get(),
                                                        avatar == null ? Model.Person.icon_filepath_default_avatar_get(is_small) : null,
                                                        avatar,
                                                        null, // Hash for blob isn't used
                                                        buddy.Alias == null ? View.Wrapper.Ctrl.font_t.NORMAL : View.Wrapper.Ctrl.font_t.ITALIC,
                                                        View.Wrapper.Ctrl.colour_t.NONE,
                                                        colour_bg);
            }

            if (col_status)
                view->frm_main.lst_ctrl_buddies.val_set(row,
                                                        View.FrmMain.LST_CTRL_BUDDIES_COL_STATUS,
                                                        buddy.status);

            if (col_state)
                view->frm_main.lst_ctrl_buddies.val_set(row,
                                                        View.FrmMain.LST_CTRL_BUDDIES_COL_STATE,
                                                        null,
                                                        buddy.icon_filepath_state_get(is_small),
                                                        null, // Icon blob isn't used, icon filepath is used above
                                                        null  // Hash for blob isn't used
                                                       );

            if (col_state_user)
                view->frm_main.lst_ctrl_buddies.val_set(row,
                                                        View.FrmMain.LST_CTRL_BUDDIES_COL_STATE_USER,
                                                        null,
                                                        buddy.icon_filepath_state_user_get(is_small, buddy.have_unread_msg == Model.Buddy.unread_t.TRUE),
                                                        null, // Icon blob isn't used, icon filepath is used above
                                                        null  // Hash for blob isn't used
                                                       );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Do the buddy selection:
         * - update GUI controls,
         * - select last messages from database and append them to text
         * control
         *
         * @param buddy Buddy or null if no one in list control should
         *              be selected
         */
        public void lst_ctrl_buddies_sel(Model.Buddy? buddy)
        {
            // Should we select buddy in list control?
            int  sel      = buddy == null ? -1 : model->buddies_lst.index_of(buddy);
            bool have_sel = sel != -1;

            // Open tab 1 in main notebook
            view->frm_main.notebook_main.page_sel_set(View.FrmMain.NOTEBOOK_MAIN_PAGE_CONVERSATIONS);

            // Open buddy list in contacts
            uint page = view->frm_main.notebook_contacts.page_sel_get();

            if (page != View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES)
            {
                // Show/hide GUI widgets
                ctrl->ctrl_changed_by_user = false;
                view->frm_main.notebook_contacts.page_sel_set(View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES);
                ctrl->ctrl_changed_by_user = true;
                view->frm_main.splitter_vertical_tab1.show(false, // Hide
                                                           false  // Don't update layout
                                                          );
                view->frm_main.txt_conv_buddy_tab1.show(true, // Show
                                                        true  // Update layout
                                                       );
            }

            // Set focus
            if (have_sel)
                view->frm_main.txt_msg_tab1.focus_set();
            else
            {
                ctrl->ctrl_changed_by_user = false;
                view->frm_main.lst_ctrl_buddies.focus_set();
                ctrl->ctrl_changed_by_user = true;
            }

            // Exit if this buddy is already selected
            if (buddy == model->peer_buddy)
                return;

            // Select the buddy in list control
            ctrl->ctrl_changed_by_user = false;

            if (have_sel)
            {
                // Buddy is shown in buddy list.
                // Set selection.
                view->frm_main.lst_ctrl_buddies.sel_set(sel, true);
            }
            else
            {
                // Buddy is not shown in buddy list.
                // Clear selection.
                size_t cnt = view->frm_main.lst_ctrl_buddies.row_cnt_get();
                for (uint row = 0; row < cnt; row++)
                    view->frm_main.lst_ctrl_buddies.sel_set(row, false);
            }

            ctrl->ctrl_changed_by_user = true;

            // Determine new peer
            model->peer_buddy = have_sel ? buddy :
                                           null;

            // A/V call GUIs elements
            av_widgets_upd();

            // Check if there are unread messages from the buddy
            if (buddy != null &&
                buddy.have_unread_msg == Model.Buddy.unread_t.TRUE)
            {
                // Have unread messages from him was before.
                // Set there are no unread messages.
                buddy.have_unread_msg = Model.Buddy.unread_t.FALSE;

                // Update buddy list,
                // set normal icon in buddy list (not unread)
                if (have_sel)
                {
                    lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                            buddy,

                                            false, // Don't set display name column
                                            false, // Don't set status column
                                            false, // Don't set state column
                                            true   // Set user state column
                                           );
                }
            }

            // Update other controls
            view->frm_main.menu_edit_buddy.en(have_sel);
            view->frm_main.menu_remove_buddy.en(have_sel);
            view->frm_main.menu_about_buddy.en(have_sel);
            view->frm_main.menu_conv_buddy.en(have_sel);
            view->frm_main.menu_edit_buddy_.en(have_sel);
            view->frm_main.menu_remove_buddy_.en(have_sel);
            view->frm_main.menu_about_buddy_.en(have_sel);
            view->frm_main.menu_conv_buddy_.en(have_sel);
            view->frm_main.txt_conv_buddy_tab1.clr();
            this.txt_conv_buddy_tab1_typing_len = 0;
            ctrl->ctrl_changed_by_user = false;
            view->frm_main.txt_msg_tab1.clr();
            ctrl->ctrl_changed_by_user = true;
            view->frm_main.txt_msg_tab1.en(have_sel);
            view->frm_main.btn_send_tab1.en(false);

            // Select last messages from database and append them to text
            // control
            if (have_sel)
            {
                try
                {
                    // Select next message from database table "conv_buddies"
                    // written by/for peer with specific uid
                    string? name;
                    bool    am_i;
                    string? msg;
                    GLib.DateTime? datetime;

                    Sqlite.Statement? stmt = null;

                    while (model->self.db_conv_buddies_sel(ref stmt,

                                                           out name,
                                                           buddy.uid,
                                                           out am_i,
                                                           out msg,
                                                           out datetime,

                                                           null, // No date & time limit, just limit to number of outputted messages below
                                                           null, // No date & time limit, just limit to number of outputted messages below

                                                           View.FrmMain.TXT_CONV_TAB1_LIMIT))
                    {
                        // Append next message to text control with last exchanged
                        // messages from/to the buddy
                        view->frm_main.txt_conv_buddy_tab1_append(name,
                                                                  am_i,
                                                                  msg,
                                                                  datetime);
                    }
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }

                // Warning!
                // I disabled code below because it's buggy.
                // It may freeze the app.

                // Scroll to end of text control with last exchanged messages
                // from/to the buddy
                //ulong len = view->frm_main.txt_conv_buddy_tab1.len_get();
                //view->frm_main.txt_conv_buddy_tab1.pos_show(len);

                // Append typing information to input text control with last
                // exchanged messages
                if ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                    this.txt_conv_buddy_tab1_typing_len = view->frm_main.txt_conv_buddy_tab1_append_typing(model->peer_buddy.display_name_get());
            }

            //view->frm_main.btn_send_tab1.show(true, // Show
            //                                  true  // Update layout
            //                                 );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set conference to a list control
         *
         * @param row              Row
         * @param conference       Conference
         * @param col_display_name false: don't change display name
         *                                column,
         *                         true:  change display name column
         * @param col_cnt          false: don't change count column,
         *                         true:  change count column
         * @param col_unread_state false: don't change unread state
         *                                column,
         *                         true:  change unread state column
         */
        public void lst_ctrl_conferences_val_set(uint row,
                                                 Model.Conference conference,

                                                 bool col_display_name,
                                                 bool col_cnt,
                                                 bool col_unread_state)
        {
            bool is_small;

            try
            {
                is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                    Model.Profile.cfg_appearance_avatar_size_t.SMALL;
            }
            catch (GLib.KeyFileError ex)
            {
                is_small = false;
                GLib.assert(false);
            }

            if (col_display_name)
                view->frm_main.lst_ctrl_conferences.val_set(row,
                                                            View.FrmMain.LST_CTRL_CONFERENCES_COL_DISPLAY_NAME,
                                                            conference.display_name_get(),
                                                            Model.Conference.icon_filepath_default_avatar_get(is_small),
                                                            null, // Icon blob isn't used, icon filepath is used above
                                                            null, // Hash for blob isn't used
                                                            conference.Alias == null ? View.Wrapper.Ctrl.font_t.NORMAL : View.Wrapper.Ctrl.font_t.ITALIC);

            if (col_cnt)
            {
                view->frm_main.lst_ctrl_conferences.val_set(row,
                                                            View.FrmMain.LST_CTRL_CONFERENCES_COL_CNT,
                                                            conference.display_cnt_get());
            }

            if (col_unread_state)
                view->frm_main.lst_ctrl_conferences.val_set(row,
                                                            View.FrmMain.LST_CTRL_CONFERENCES_COL_UNREAD_STATE,
                                                            null,
                                                            conference.have_unread_msg == Model.Conference.unread_t.TRUE ?
                                                                Model.Conference.icon_filepath_unread_get(is_small) :
                                                                null,
                                                            null, // Icon blob isn't used, icon filepath is used above
                                                            null  // Hash for blob isn't used
                                                           );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * The method changes the conference list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding objects, hash and array.
         *
         * @param action           Action
         * @param conference       Conference
         * @param col_display_name false: don't change display name
         *                                column,
         *                         true:  change display name column
         * @param col_cnt          false: don't change count column,
         *                         true:  change count column
         * @param col_unread_state false: don't change unread state
         *                                column,
         *                         true:  change unread state column
         */
        public void lst_ctrl_conferences_change(
            Ctrl.action_t action,

            Model.Conference? conference = null,

            bool col_display_name = false,
            bool col_cnt          = false,
            bool col_unread_state = false)

            requires ((action == Ctrl.action_t.ADD_ELEM    && conference != null) ||
                      (action == Ctrl.action_t.EDIT_ELEM   && conference != null) ||
                      (action == Ctrl.action_t.REMOVE_ELEM && conference != null) ||
                      (action == Ctrl.action_t.UPD_LST     && conference == null))

            requires (action == Ctrl.action_t.EDIT_ELEM || (!col_display_name &&
                                                            !col_cnt          &&
                                                            !col_unread_state))
        {
            // Get index of selected status & conference filter choice
            uint idx_choice = view->frm_main.choice_status_and_filter.sel_get();
            bool use_filter = idx_choice == View.FrmMain.CHOICE_IDX_FILTER;

            // Use filter?
            GLib.Regex? pattern = null;

            if (use_filter)
            {
                // Get current typed conference filter
                string filter = view->frm_main.txt_status_and_filter.val_get();

                try
                {
                    // This regular expression will be used for conference filtering
                    pattern = new GLib.Regex("\\Q%s\\E".printf(filter), GLib.RegexCompileFlags.CASELESS);
                }
                catch (RegexError ex)
                {
                    GLib.assert(false);
                }
            }

            // Fill conference list entirely or change one row only?
            bool removed_peer = false;

            switch (action)
            {
            case Ctrl.action_t.ADD_ELEM:

                // Add conference to the conference list
                model->conferences_hash[conference.uid] = conference;

                // Should conference be showed in GUI?
                if (conference.should_be_displayed(model, use_filter, pattern))
                {
                    // Conference is to be shown in GUI.
                    // Find its place in the conference list.
                    uint row = 0;
                    GLib.CompareDataFunc<Model.Conference> cmp_fn = Model.Conference.cmp_fn_get(model);

                    foreach (Model.Conference conference_ in model->conferences_lst)
                    {
                        if (cmp_fn(conference_, conference) != -1)
                            break;
                        row++;
                    }

                    // Add him to the conference list.
                    // Also update list control.
                    model->conferences_lst.insert((int) row, conference);
                    view->frm_main.lst_ctrl_conferences.row_add(row);
                    lst_ctrl_conferences_val_set(row,
                                                 conference,

                                                 true, // Set display name column
                                                 true, // Set count column
                                                 true  // Set unread state column
                                                );
                }

                break;

            case Ctrl.action_t.EDIT_ELEM:

                // Should conference be showed in GUI?
                int idx = model->conferences_lst.index_of(conference);

                if (conference.should_be_displayed(model, use_filter, pattern))
                {
                    // Conference is to be shown in GUI.
                    // Check if it is already there.
                    if (idx == -1)
                    {
                        // Conference is not in the conference list yet.
                        // Find its place in the conference list.
                        idx = 0;
                        GLib.CompareDataFunc<Model.Conference> cmp_fn = Model.Conference.cmp_fn_get(model);

                        foreach (Model.Conference conference_ in model->conferences_lst)
                        {
                            if (cmp_fn(conference_, conference) != -1)
                                break;
                            idx++;
                        }

                        // Add conference to the conference list.
                        // Also update list control.
                        model->conferences_lst.insert((int) idx, conference);
                        view->frm_main.lst_ctrl_conferences.row_add(idx);
                        lst_ctrl_conferences_val_set(idx,
                                                     conference,

                                                     true, // Set display name column
                                                     true, // Set count column
                                                     true  // Set unread state column
                                                    );
                    }
                    else
                    {
                        // Conference is already in the conference list.
                        // Find its place in the conference list.
                        uint idx_new = 0;
                        GLib.CompareDataFunc<Model.Conference> cmp_fn = Model.Conference.cmp_fn_get(model);

                        foreach (Model.Conference conference_ in model->conferences_lst)
                        {
                            if (conference_ == conference)
                                continue;
                            if (cmp_fn(conference_, conference) != -1)
                                break;
                            idx_new++;
                        }

                        // The position may be the same or may differs
                        if (idx_new == idx)
                        {
                            // Position is the same.
                            // Just update list control.
                            lst_ctrl_conferences_val_set(idx,
                                                         conference,

                                                         col_display_name,
                                                         col_cnt,
                                                         col_unread_state);
                        }
                        else
                        {
                            // Position is the new.
                            // Remove conference from the conference list.
                            model->conferences_lst.remove_at(idx);
                            ctrl->ctrl_changed_by_user = false;
                            view->frm_main.lst_ctrl_conferences.row_remove(idx);
                            ctrl->ctrl_changed_by_user = true;

                            // Add it again now to the new position
                            model->conferences_lst.insert((int) idx_new, conference);
                            view->frm_main.lst_ctrl_conferences.row_add(idx_new);
                            lst_ctrl_conferences_val_set(idx_new,
                                                         conference,

                                                         true, // Set display name column
                                                         true, // Set count column
                                                         true  // Set unread state column
                                                        );

                            // Reselect list control row
                            if (model->peer_conference == conference)
                            {
                                ctrl->ctrl_changed_by_user = false;
                                view->frm_main.lst_ctrl_conferences.sel_set(idx_new, true);
                                ctrl->ctrl_changed_by_user = true;
                            }
                        }
                    }
                }
                else
                {
                    // Conference is not to be shown in GUI.
                    // Remove conference from the conference list it is there.
                    // Also update list control.
                    // No peer anymore if it was this conference.
                    if (idx != -1)
                    {
                        model->conferences_lst.remove_at(idx);
                        ctrl->ctrl_changed_by_user = false;
                        view->frm_main.lst_ctrl_conferences.row_remove(idx);
                        ctrl->ctrl_changed_by_user = true;

                        if (model->peer_conference == conference)
                        {
                            model->peer_conference = null;
                            removed_peer = true;
                        }
                    }
                }

                break;

            case Ctrl.action_t.REMOVE_ELEM:

                // Remove conference from the conference list
                model->conferences_hash.unset(conference.uid);

                // Is conference showed in list control?
                int idx = model->conferences_lst.index_of(conference);

                if (idx != -1)
                {
                    // Conference is showed in GUI.
                    // Remove conference from the conference list.
                    // Also update list control.
                    // No peer anymore if it was this conference.
                    model->conferences_lst.remove_at(idx);
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.lst_ctrl_conferences.row_remove(idx);
                    ctrl->ctrl_changed_by_user = true;

                    if (model->peer_conference == conference)
                    {
                        model->peer_conference = null;
                        removed_peer = true;
                    }
                }

                break;

            case Ctrl.action_t.UPD_LST:

                // Filter conference list.
                // Hide offline conferences if according setting was set.
                model->conferences_lst.clear();

                foreach (Model.Conference conference_ in model->conferences_hash.values)
                {
                    // Should conference be showed in GUI?
                    if (conference_.should_be_displayed(model, use_filter, pattern))
                    {
                        // Conference is to be shown in GUI.
                        // Add him to the conference list.
                        model->conferences_lst.add(conference_);
                    }
                }

                // Sort conferences
                GLib.CompareDataFunc<Model.Conference> fn = Model.Conference.cmp_fn_get(model);
                model->conferences_lst.sort((owned) fn);

                // Fill list control
                view->frm_main.lst_ctrl_conferences.clr();

                for (uint row = 0; row < model->conferences_lst.size; row++)
                {
                    Model.Conference conference_ = model->conferences_lst[(int) row];

                    view->frm_main.lst_ctrl_conferences.row_add(row);
                    lst_ctrl_conferences_val_set(row,
                                                 conference_,

                                                 true, // Set display name column
                                                 true, // Set count column
                                                 true  // Set unread state column
                                                );
                }

                // No peer anymore?
                // Or select previosly selected peer in list control.
                if (model->peer_conference != null)
                {
                    int sel = model->conferences_lst.index_of(model->peer_conference);

                    if (sel == -1)
                    {
                        model->peer_conference = null;
                        removed_peer = true;
                    }
                    else
                    {
                        ctrl->ctrl_changed_by_user = false;
                        view->frm_main.lst_ctrl_conferences.sel_set(sel, true);
                        ctrl->ctrl_changed_by_user = true;
                    }
                }

                break;

            default:
                GLib.assert(false);
                break;
            }

            // Update other GUI controls
            if (removed_peer)
            {
                view->frm_main.menu_edit_conference.en(false);
                view->frm_main.menu_remove_conference.en(false);
                view->frm_main.menu_about_conference.en(false);
                view->frm_main.menu_conv_conference.en(false);
                view->frm_main.menu_edit_conference_.en(false);
                view->frm_main.menu_remove_conference_.en(false);
                view->frm_main.menu_about_conference_.en(false);
                view->frm_main.menu_conv_conference_.en(false);
                view->frm_main.txt_conv_conference_tab1.clr();

                uint page = view->frm_main.notebook_contacts.page_sel_get();

                if (page == View.FrmMain.NOTEBOOK_CONTACTS_PAGE_CONFERENCES)
                {
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.txt_msg_tab1.clr();
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_main.txt_msg_tab1.en(false);
                    view->frm_main.btn_send_tab1.en(false);
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set member to a list control
         *
         * @param row              Row
         * @param member           Member
         * @param col_display_name false: don't change display name column,
         *                         true:  change display name column
         * @param col_state_user   false: don't change user state column,
         *                         true:  change user state column
         */
        public void lst_ctrl_members_tab1_val_set(uint row,
                                                  Model.Member member,

                                                  bool col_display_name,
                                                  bool col_state_user)
        {
            bool is_small;

            try
            {
                is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                    Model.Profile.cfg_appearance_avatar_size_t.SMALL;
            }
            catch (GLib.KeyFileError ex)
            {
                is_small = false;
                GLib.assert(false);
            }

            if (col_display_name)
            {
                if (member.person == null)
                {
                    view->frm_main.lst_ctrl_members_tab1.val_set(row,
                                                                 View.FrmMain.LST_CTRL_BUDDIES_COL_DISPLAY_NAME,
                                                                 member.display_name_get(),
                                                                 null, // Filepath to icon isn't used
                                                                 null, // Icon blob isn't used, icon filepath is used above
                                                                 null, // Hash for blob isn't used
                                                                 View.Wrapper.Ctrl.font_t.NORMAL,
                                                                 view->db_colour_convert(member.fg),
                                                                 View.Wrapper.Ctrl.colour_t.NONE);
                }
                else
                {
                    unowned uint8[]? avatar = is_small ? member.person.avatar_16x16 :
                                                         member.person.avatar_32x32;

                    if (member.person is Model.Profile)
                    {
                        //GLib.assert(member.person is Model.Profile);
                        var profile = member.person as Model.Profile;

                        view->frm_main.lst_ctrl_members_tab1.val_set(row,
                                                                     View.FrmMain.LST_CTRL_BUDDIES_COL_DISPLAY_NAME,
                                                                     profile.display_name_get(),
                                                                     avatar == null ? Model.Person.icon_filepath_default_avatar_get(is_small) : null,
                                                                     avatar,
                                                                     null, // Hash for blob isn't used
                                                                     View.Wrapper.Ctrl.font_t.NORMAL,
                                                                     view->db_colour_convert(member.fg),
                                                                     View.Wrapper.Ctrl.colour_t.NONE);
                    }
                    else
                    {
                        GLib.assert(member.person is Model.Buddy);
                        var buddy = member.person as Model.Buddy;

                        view->frm_main.lst_ctrl_members_tab1.val_set(row,
                                                                     View.FrmMain.LST_CTRL_BUDDIES_COL_DISPLAY_NAME,
                                                                     buddy.display_name_get(),
                                                                     avatar == null ? Model.Person.icon_filepath_default_avatar_get(is_small) : null,
                                                                     avatar,
                                                                     null, // Hash for blob isn't used
                                                                     buddy.Alias == null ? View.Wrapper.Ctrl.font_t.NORMAL : View.Wrapper.Ctrl.font_t.ITALIC,
                                                                     view->db_colour_convert(member.fg),
                                                                     View.Wrapper.Ctrl.colour_t.NONE);
                    }
                }
            }

            if (col_state_user)
            {
                if (member.person == null)
                {
                    view->frm_main.lst_ctrl_members_tab1.val_set(row,
                                                                 View.FrmMain.LST_CTRL_MEMBERS_COL_STATE_USER,
                                                                 null, // No text value in this cell
                                                                 null, // Filepath to icon isn't used
                                                                 null, // Icon blob isn't used, icon filepath is used above
                                                                 null  // Hash for blob isn't used
                                                                );
                }
                else
                {
                    GLib.assert(member.person is Model.Buddy ||
                                member.person is Model.Profile);

                    view->frm_main.lst_ctrl_members_tab1.val_set(row,
                                                                 View.FrmMain.LST_CTRL_MEMBERS_COL_STATE_USER,
                                                                 null, // No text value in this cell
                                                                 member.person.icon_filepath_state_user_get(is_small),
                                                                 null, // Icon blob isn't used, icon filepath is used above
                                                                 null  // Hash for blob isn't used
                                                                );
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * The method changes the member list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding hash.
         *
         * @param action           Action
         * @param member           Member
         * @param col_display_name false: don't change display name column,
         *                         true:  change display name column
         * @param col_state_user   false: don't change user state column,
         *                         true:  change user state column
         */
        public void lst_ctrl_members_tab1_change(
            Ctrl.action_t action,

            Model.Member? member = null,

            bool col_display_name = false,
            bool col_state_user   = false)

            requires ((action == Ctrl.action_t.ADD_ELEM    && member != null) ||
                      (action == Ctrl.action_t.EDIT_ELEM   && member != null) ||
                      (action == Ctrl.action_t.REMOVE_ELEM && member != null) ||
                      (action == Ctrl.action_t.UPD_LST     && member == null))

            requires (action == Ctrl.action_t.EDIT_ELEM || (!col_display_name &&
                                                            !col_state_user))
        {
            // Get current typed member filter
            GLib.Regex? pattern;
            string filter = view->frm_main.txt_filter_members.val_get();

            try
            {
                // This regular expression will be used for member filtering
                pattern = new GLib.Regex("\\Q%s\\E".printf(filter), GLib.RegexCompileFlags.CASELESS);
            }
            catch (RegexError ex)
            {
                pattern = null;
                GLib.assert(false);
            }

            // Fill member list entirely or change one row only?
            bool removed_sel = false;

            switch (action)
            {
            case Ctrl.action_t.ADD_ELEM:

                // Add member to the member list
                member.conference.members_hash[member.uid] = member;

                // Continue only if specified conference is selected.
                // Should member be showed in GUI?
                if (model->peer_conference == member.conference &&
                    member.should_be_displayed(model, pattern))
                {
                    // Member is to be shown in GUI.
                    // Find its place in the member list.
                    uint row = 0;
                    GLib.CompareDataFunc<Model.Member> cmp_fn = Model.Member.cmp_fn_get(model);

                    foreach (Model.Member member_ in model->members_lst)
                    {
                        if (cmp_fn(member_, member) != -1)
                            break;
                        row++;
                    }

                    // Add him to the member list.
                    // Also update list control.
                    model->members_lst.insert((int) row, member);
                    view->frm_main.lst_ctrl_members_tab1.row_add(row);
                    lst_ctrl_members_tab1_val_set(row,
                                                  member,

                                                  true, // Set display name column
                                                  true  // Set user state column
                                                 );
                }

                break;

            case Ctrl.action_t.EDIT_ELEM:

                // Continue only if specified conference is selected.
                if (model->peer_conference == member.conference)
                {
                    // Should member be showed in GUI?
                    int idx = model->members_lst.index_of(member);

                    if (member.should_be_displayed(model, pattern))
                    {
                        // Member is to be shown in GUI.
                        // Check if it is already there.
                        if (idx == -1)
                        {
                            // Member is not in the member list yet.
                            // Find its place in the member list.
                            idx = 0;
                            GLib.CompareDataFunc<Model.Member> cmp_fn = Model.Member.cmp_fn_get(model);

                            foreach (Model.Member member_ in model->members_lst)
                            {
                                if (cmp_fn(member_, member) != -1)
                                    break;
                                idx++;
                            }

                            // Add member to the member list.
                            // Also update list control.
                            model->members_lst.insert((int) idx, member);
                            view->frm_main.lst_ctrl_members_tab1.row_add(idx);
                            lst_ctrl_members_tab1_val_set(idx,
                                                          member,

                                                          true, // Set display name column
                                                          true  // Set user state column
                                                         );
                        }
                        else
                        {
                            // Member is already in the member list.
                            // Find its place in the member list.
                            uint idx_new = 0;
                            GLib.CompareDataFunc<Model.Member> cmp_fn = Model.Member.cmp_fn_get(model);

                            foreach (Model.Member member_ in model->members_lst)
                            {
                                if (member_ == member)
                                    continue;
                                if (cmp_fn(member_, member) != -1)
                                    break;
                                idx_new++;
                            }

                            // The position may be the same or may differs
                            if (idx_new == idx)
                            {
                                // Position is the same.
                                // Just update list control.
                                lst_ctrl_members_tab1_val_set(idx,
                                                              member,

                                                              col_display_name,
                                                              col_state_user);
                            }
                            else
                            {
                                // Position is the new.
                                // Is the member selected?
                                bool sel = view->frm_main.lst_ctrl_members_tab1.sel_get(idx);

                                // Remove member from the member list
                                model->members_lst.remove_at(idx);
                                ctrl->ctrl_changed_by_user = false;
                                view->frm_main.lst_ctrl_members_tab1.row_remove(idx);
                                ctrl->ctrl_changed_by_user = true;

                                // Add it again now to the new position
                                model->members_lst.insert((int) idx_new, member);
                                view->frm_main.lst_ctrl_members_tab1.row_add(idx_new);
                                lst_ctrl_members_tab1_val_set(idx_new,
                                                              member,

                                                              true, // Set display name column
                                                              true  // Set user state column
                                                             );

                                // Reselect list control row
                                if (sel)
                                {
                                    ctrl->ctrl_changed_by_user = false;
                                    view->frm_main.lst_ctrl_members_tab1.sel_set(idx_new, true);
                                    ctrl->ctrl_changed_by_user = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Member is not to be shown in GUI.
                        // Remove member from the member list it is there.
                        // Also update list control.
                        if (idx != -1)
                        {
                            model->members_lst.remove_at(idx);
                            removed_sel = view->frm_main.lst_ctrl_members_tab1.sel_get(idx);
                            ctrl->ctrl_changed_by_user = false;
                            view->frm_main.lst_ctrl_members_tab1.row_remove(idx);
                            ctrl->ctrl_changed_by_user = true;
                        }
                    }
                }

                break;

            case Ctrl.action_t.REMOVE_ELEM:

                // Remove member from the member list
                member.conference.members_hash.unset(member.uid);

                // Continue only if specified conference is selected
                if (model->peer_conference == member.conference)
                {
                    // Is buddy showed in list control?
                    int idx = model->members_lst.index_of(member);

                    // Should member be showed in GUI?
                    if (idx != -1)
                    {
                        // Member is showed in GUI.
                        // Remove member from the member list.
                        // Also update list control.
                        model->members_lst.remove_at(idx);
                        removed_sel = view->frm_main.lst_ctrl_members_tab1.sel_get(idx);
                        ctrl->ctrl_changed_by_user = false;
                        view->frm_main.lst_ctrl_members_tab1.row_remove(idx);
                        ctrl->ctrl_changed_by_user = true;
                    }
                }

                break;

            case Ctrl.action_t.UPD_LST:

                // Continue only if any conference is selected
                Model.Conference? conference = model->peer_conference;

                if (conference == null)
                {
                    // Clear member list.
                    // Also clear list control.
                    model->members_lst.clear();
                    view->frm_main.lst_ctrl_members_tab1.clr();
                }
                else
                {
                    // Find currently selected member in list control if any
                    Model.Member? sel_member = null;

                    for (uint row = 0; row < model->members_lst.size; row++)
                    {
                        bool sel_flag = view->frm_main.lst_ctrl_members_tab1.sel_get(row);

                        if (sel_flag)
                        {
                            sel_member = model->members_lst[(int) row];
                            break;
                        }
                    }

                    // Filter member list
                    model->members_lst.clear();

                    foreach (Model.Member member_ in conference.members_hash.values)
                    {
                        // Should member be showed in GUI?
                        if (member_.should_be_displayed(model, pattern))
                        {
                            // Member is to be shown in GUI.
                            // Add him to the member list.
                            model->members_lst.add(member_);
                        }
                    }

                    // Sort members
                    GLib.CompareDataFunc<Model.Member> fn = Model.Member.cmp_fn_get(model);
                    model->members_lst.sort((owned) fn);

                    // Fill list control
                    view->frm_main.lst_ctrl_members_tab1.clr();

                    for (uint row = 0; row < model->members_lst.size; row++)
                    {
                        Model.Member member_ = model->members_lst[(int) row];

                        view->frm_main.lst_ctrl_members_tab1.row_add(row);
                        lst_ctrl_members_tab1_val_set(row,
                                                      member_,

                                                      true, // Set display name column
                                                      true  // Set user state column
                                                     );
                    }

                    // Select previosly selected member in list control if any
                    if (sel_member != null)
                    {
                        int sel_row = model->members_lst.index_of(sel_member);

                        if (sel_row == -1)
                            removed_sel = true;
                        else
                        {
                            ctrl->ctrl_changed_by_user = false;
                            view->frm_main.lst_ctrl_members_tab1.sel_set(sel_row, true);
                            ctrl->ctrl_changed_by_user = true;
                        }
                    }
                }

                break;

            default:
                GLib.assert(false);
                break;
            }

            // Update other GUI controls
            if (removed_sel)
            {
                // Change menus
                bool flag          = false;
                unowned string val = _("&Add to Buddy List");

                view->frm_main.menu_about_member.en(false);
                view->frm_main.menu_members_action_in_buddy_list.en(flag);
                view->frm_main.menu_members_action_in_buddy_list_.en(flag);

                view->frm_main.menu_about_member_.en(false);
                view->frm_main.menu_members_action_in_buddy_list.val_set(val + "\tAlt-L");
                view->frm_main.menu_members_action_in_buddy_list_.val_set(val);
            }
        }

        /*----------------------------------------------------------------------------*/

        // Set invitation from a buddy to a list control

        /**
         * The method changes the invite list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding array.
         *
         * @param row                   Row
         * @param conference_invitation Conference invitation
         * @param col_display_name      false: don't change display name column,
         *                              true:  change display name column
         * @param col_cookie            false: don't change cookie column,
         *                              true:  change cookie column
         * @param col_datetime          false: don't change date and time column,
         *                              true:  change date and time column
         */
        public void lst_ctrl_invites_tab3_val_set(uint row,
                                                  Model.Model.ConferenceInvitation? conference_invitation,

                                                  bool col_display_name,
                                                  bool col_cookie,
                                                  bool col_datetime)
        {
            if (col_display_name)
            {
                Model.Buddy buddy = conference_invitation.buddy;
                bool is_small;

                try
                {
                    is_small = model->self.cfg.get_integer("appearance", "avatar_size") ==
                        Model.Profile.cfg_appearance_avatar_size_t.SMALL;
                }
                catch (GLib.KeyFileError ex)
                {
                    is_small = false;
                    GLib.assert(false);
                }

                unowned uint8[]? avatar = is_small ? buddy.avatar_16x16 :
                                                     buddy.avatar_32x32;

                view->frm_main.lst_ctrl_invites_tab3.val_set(row,
                                                             View.FrmMain.LST_CTRL_INVITES_COL_DISPLAY_NAME,
                                                             buddy.display_name_get(),
                                                             avatar == null ? Model.Person.icon_filepath_default_avatar_get(is_small) : null,
                                                             avatar,
                                                             null, // Hash for blob isn't used
                                                             buddy.Alias == null ? View.Wrapper.Ctrl.font_t.NORMAL : View.Wrapper.Ctrl.font_t.ITALIC
                                                            );
            }

            if (col_cookie)
                view->frm_main.lst_ctrl_invites_tab3.val_set(row,
                                                             View.FrmMain.LST_CTRL_INVITES_COL_COOKIE,
                                                             conference_invitation.cookie_str);

            if (col_datetime)
                view->frm_main.lst_ctrl_invites_tab3.val_set(row,
                                                             View.FrmMain.LST_CTRL_INVITES_COL_DATETIME,
                                                             conference_invitation.datetime.format("%A, %e %B %Y %H:%M:%S"));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * The method changes the invite list.
         * This means it changes a corresponding list control:
         * add/edit/remove a single row or refill it entirely.
         * It also changes corresponding array.
         *
         * @param action                Action
         * @param conference_invitation Conference invitation
         * @param col_display_name      false: don't change display name column,
         *                              true:  change display name column
         * @param col_cookie            false: don't change cookie column,
         *                              true:  change cookie column
         */
        public void lst_ctrl_invites_tab3_change(
            Ctrl.action_t action,

            Model.Model.ConferenceInvitation? conference_invitation = null,

            bool col_display_name = false,
            bool col_cookie       = false)

            requires ((action == Ctrl.action_t.ADD_ELEM    && conference_invitation != null) ||
                      (action == Ctrl.action_t.REMOVE_ELEM && conference_invitation != null) ||
                      (action == Ctrl.action_t.UPD_LST     && conference_invitation == null))

            requires (action == Ctrl.action_t.EDIT_ELEM || (!col_display_name &&
                                                            !col_cookie))
        {
            switch (action)
            {
            case Ctrl.action_t.ADD_ELEM:

                // Add conference to the invite list.
                // Also update list control.
                model->conference_invitations.insert(0, conference_invitation);

                view->frm_main.lst_ctrl_invites_tab3.row_add(0);
                lst_ctrl_invites_tab3_val_set(0,
                                              conference_invitation,

                                              true, // Set display name column
                                              true, // Set cookie column
                                              true  // Set date and time column
                                             );

                break;

            case Ctrl.action_t.REMOVE_ELEM:

                // Remove conference from the conference list.
                // Also update list control.
                int idx = model->conference_invitations.index_of(conference_invitation);

                model->conference_invitations.remove_at(idx);
                ctrl->ctrl_changed_by_user = false;
                view->frm_main.lst_ctrl_invites_tab3.row_remove(idx);
                ctrl->ctrl_changed_by_user = true;

                // Have at least one conference selected?
                size_t cnt = view->frm_main.lst_ctrl_invites_tab3.row_cnt_get();
                bool   sel = false;

                for (int row = 0; row < (int) cnt; row++)
                {
                    sel = view->frm_main.lst_ctrl_invites_tab3.sel_get(row);
                    if (sel)
                        break;
                }

                // If not, disable the tab 3 buttons
                if (!sel)
                {
                    view->frm_main.btn_accept_tab3.en(false);
                    view->frm_main.btn_reject_tab3.en(false);
                }

                break;

            case Ctrl.action_t.UPD_LST:

                // Get selected invites in list control
                size_t cnt = view->frm_main.lst_ctrl_invites_tab3.row_cnt_get();
                uint[] sel = {};

                for (uint row = 0; row < cnt; row++)
                    if (view->frm_main.lst_ctrl_invites_tab3.sel_get(row))
                        sel += row;

                // Fill list control
                view->frm_main.lst_ctrl_invites_tab3.clr();

                for (uint row = 0; row < cnt; row++)
                {
                    Model.Model.ConferenceInvitation conference_invitation_ = model->conference_invitations[(int) row];

                    view->frm_main.lst_ctrl_invites_tab3.row_add(row);
                    lst_ctrl_invites_tab3_val_set(row,
                                                  conference_invitation_,

                                                  true, // Set display name column
                                                  true, // Set cookie column
                                                  true  // Set date and time column
                                                 );
                }

                // Select previosly selected invites in list control
                foreach (uint row in sel)
                {
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.lst_ctrl_invites_tab3.sel_set(row, true);
                    ctrl->ctrl_changed_by_user = true;
                }

                break;

            default:
                GLib.assert(false);
                break;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Enable/disable "Apply" button
         */
        public void btn_apply_status_en_set()
        {
            string status = view->frm_main.txt_status_and_filter.val_get();
            view->frm_main.btn_apply_status.en(status != model->self.status &&
                                               status.length <= Model.Person.status_max_len);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update tab 2 message control
         */
        public void txt_msg_tab2_upd()
        {
            // Get selected public key
            int  idx      = view->frm_main.check_lst_pub_keys_tab2.sel_elem_get();
            bool have_sel = idx != View.Wrapper.Win.FOUND_NOT;

            // Get corresponding buddy request message.
            // Change this message.
            ctrl->ctrl_changed_by_user = false;

            if (have_sel)
            {
                Model.Model.BuddyReq buddy_req = model->buddy_reqs[idx];
                view->frm_main.txt_msg_tab2.val_set(buddy_req.msg);
            }
            else
                view->frm_main.txt_msg_tab2.clr();

            ctrl->ctrl_changed_by_user = true;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Start/stop timer updating the call length on status bar
         *
         * @param flag false: stop timer,
         *             true:  start timer
         */
        public void status_timer_en(bool flag)
        {
            GLib.assert(( flag && this.timer_status == 0) ||
                        (!flag && this.timer_status != 0));

            // Check flag
            if (flag)
            {
                // Start timer
                this.timer_status = GLib.Timeout.add(1000, () =>
                {
                    // Lock mutex.
                    // Update status bar.
                    // Unlock mutex.
                    model->mutex.lock();
                    status_val_set(View.FrmMain.STATUS_COL_EVT);
                    model->mutex.unlock();

                    // Continue timer
                    return GLib.Source.CONTINUE;
                });
            }
            else
            {
                // Stop timer
                GLib.Source.remove(this.timer_status);
                this.timer_status = 0;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update status bar
         *
         * @param col Column
         */
        public void status_val_set(uint col)
        {
            string val;

            // Determine the new value of a column in status bar
            switch (col)
            {
            case View.FrmMain.STATUS_COL_BUDDIES:

                // Total number of buddies and the number of currently online
                // buddies
                val = _("Buddies: %u/%d").printf((uint) (Model.Buddy.online_tcp +
                                                         Model.Buddy.online_udp), model->buddies_hash.size);
                break;

            case View.FrmMain.STATUS_COL_CONFERENCES:

                // Total number of conferences
                val = _("Conferences: %d").printf(model->conferences_hash.size);
                break;

            case View.FrmMain.STATUS_COL_EVT:

                // A/V calls
                Model.Buddy? most_important = null;
                size_t       av_peers = 0;

                foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                {
                    av_peers++;
                    if ((peer.action_to_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                {
                    if ((peer.action_from_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                {
                    if ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                foreach (Model.Buddy peer in model->peer_buddies_video.values)
                {
                    av_peers++;
                    if ((peer.action_to_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                foreach (Model.Buddy peer in model->peer_buddies_video.values)
                {
                    if ((peer.action_from_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                foreach (Model.Buddy peer in model->peer_buddies_video.values)
                {
                    if ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_VIDEO_CALL) > 0)
                        if (most_important == null)
                            most_important = peer;
                }

                if (most_important != null)
                {
                    if ((most_important.action_to_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0)
                        val = _("Incoming audio call...");
                    else if ((most_important.action_from_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0)
                        val = _("Outcoming audio call...");
                    else if ((most_important.action_to_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0)
                        val = _("Incoming video call...");
                    else if ((most_important.action_from_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0)
                        val = _("Outcoming video call...");
                    else if (((most_important.action_from_us & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0) ||
                             ((most_important.action_to_us   & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0))
                    {
                        var           now     = new GLib.DateTime.now_local();
                        GLib.TimeSpan diff_us = now.difference(most_important.datetime_av_answer);
                        var           tz      = new GLib.TimeZone.local();
                        var           empty   = new GLib.DateTime(tz, 1, 1, 1, 0, 0, 0);
                        GLib.DateTime len     = empty.add(diff_us);

                        val = _("%s (%s)").printf(
                            ((most_important.action_from_us & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0 ||
                                 (most_important.action_to_us & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0) ?
                            _("Audio call is active") :
                            _("Video call is active"), len.format("%H:%M:%S"));
                    }
                    else
                        val = "";
                }
                else
                    val = "";

                if (av_peers > 1)
                    val += _(" (And %u A/V sessions more)").printf((uint) av_peers - 1);

                break;

            default:

                // Can't go here
                val = "";
                GLib.assert(false);
                break;
            }

            // Update control with the value
            view->frm_main.status.val_set(col, val);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Do the A/V call
         *
         * @param buddy Buddy
         * @param video false: no video,
         *              true:  do video call
         */
        public void av_call(Model.Buddy buddy,
                            bool        do_call,
                            bool        video)
        {
            string display_name = buddy.display_name_get();
            string dbg          = _("%s: %s: ").printf(
                video ? _("Outcoming video call to buddy") : _("Outcoming audio call to buddy"),
                display_name);

            // Lock mutex
            model->mutex.lock();

            // Handle the outcoming call
            try
            {
                GLib.assert((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                            (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) == 0);

                // Start the A/V call
                buddy.av_call(model, video);

                // Fill the call information
                buddy.action_from_us |= video ? Model.Buddy.state_t.DOING_VIDEO_CALL :
                                                Model.Buddy.state_t.DOING_AUDIO_CALL;

                // Add buddy to A/V peers
                if (video) model->peer_buddies_video[buddy.uid] = buddy;
                else       model->peer_buddies_audio[buddy.uid] = buddy;

                // Update buddy list,
                // new row background should be set
                int idx = model->buddies_lst.index_of(buddy);

                if (idx != -1)
                {
                    lst_ctrl_buddies_val_set(idx,
                                             buddy,

                                             true,  // Set display name column
                                             false, // Don't set status column
                                             false, // Don't state column
                                             false  // Don't user state column
                                            );
                }

                // A/V call GUIs elements.
                // Update status bar.
                av_widgets_upd();
                status_val_set(View.FrmMain.STATUS_COL_EVT);

                GLib.debug(dbg + _("OK"));
            }
            catch (Model.ErrCall ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg.modal_show();

                return;
            }

            // Unlock mutex
            model->mutex.unlock();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Cancel the A/V call
         *
         * @param buddy           Buddy
         * @param cancel_call     false: do not hang up,
         *                        true:  hang up,
         * @param update_gui      false: do not update GUI,
         *                        true:  update GUI
         * @param is_locked_mutex false: mutex had not locked,
         *                        true:  mutex had locked
         */
        public void av_call_cancel(Model.Buddy buddy,
                                   bool        cancel_call,
                                   bool        update_gui,
                                   bool        is_locked_mutex)
        {
            string display_name = buddy.display_name_get();

            // Available multimedia
            bool video = (buddy.action_from_us & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0 ||
                         (buddy.action_to_us   & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0;

            string dbg = (buddy.action_to_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0 ?
                _("%s: %s: ").printf(video ? _("Rejected video call from buddy") : _("Rejected audio call from buddy"), display_name) :
                _("%s: %s: ").printf(video ? _("Finished video call with buddy") : _("Finished audio call with buddy"), display_name);

            // Lock mutex
            if (!is_locked_mutex)
                model->mutex.lock();

            // Handle the call rejecting/finishing
            try
            {
                // Cancel the A/V call
                if (cancel_call)
                    buddy.av_call_cancel(model);

                // Clear the call information
                bool av_active = (buddy.action_from_us & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0 ||
                                 (buddy.action_to_us   & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0;

                buddy.action_from_us &= ~Model.Buddy.state_t.MASK_AV_CALL;
                buddy.action_to_us   &= ~Model.Buddy.state_t.MASK_AV_CALL;

                buddy.datetime_av_answer = null;

                // Remove buddy from A/V peers
                model->peer_buddies_audio.unset(buddy.uid);
                model->peer_buddies_video.unset(buddy.uid);

                // Any more video calls?
                bool have_more_active_video_peers = false;
                foreach (Model.Buddy peer in model->peer_buddies_video.values)
                    if ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_VIDEO_CALL) > 0 &&
                            peer != buddy)
                        have_more_active_video_peers = true;

                // Disable microphone / webcam.
                // Stop timer updating the call length on status bar.
                if (av_active && !have_more_active_video_peers)
                {
                    // Any more audio calls?
                    bool have_more_active_audio_peers = false;
                    foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                        if ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0 &&
                                peer != buddy)
                            have_more_active_audio_peers = true;

                    // Unlock mutex.
                    //
                    // Note. We should unlock mutex to prevent threads from blocking
                    // each other.
                    // pipeline1_new_sample_handle() and this thread could wait
                    // each other infinitely.
                    //
                    // It is bad practice in general, but it is safe here, because
                    // enabling/disabling microphone/webcam could be made only in
                    // one our main thread.
                    model->mutex.unlock();

                    // Disable microphone
                    if (!have_more_active_audio_peers)
                    {
                        try
                        {
                            model->audio.mic_dis(model);
                        }
                        catch (Model.ErrAudio ex)
                        {
                            GLib.debug(ex.message);
                            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                            msg_dlg.modal_show();
                        }
                    }

                    // Disable webcam
                    if (video)
                    {
                        try
                        {
                            model->video.webcam_dis(model);
                        }
                        catch (Model.ErrVideo ex)
                        {
                            GLib.debug(ex.message);
                            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                            msg_dlg.modal_show();
                        }
                    }

                    // Lock mutex
                    model->mutex.lock();
                }

                // Stop timer updating the call length on status bar
                if (av_active)
                    status_timer_en(false);

                // Do we need to update GUI?
                if (update_gui)
                {
                    // Update buddy list,
                    // new row background should be set
                    int idx = model->buddies_lst.index_of(buddy);

                    if (idx != -1)
                    {
                        lst_ctrl_buddies_val_set(idx,
                                                 buddy,

                                                 true,  // Set display name column
                                                 false, // Don't set display name column
                                                 false, // Don't set status column
                                                 false  // Don't user state column
                                                );
                    }

                    // A/V call GUIs elements.
                    // Update status bar.
                    av_widgets_upd();
                    status_val_set(View.FrmMain.STATUS_COL_EVT);
                }

                if (cancel_call)
                    GLib.debug(dbg + _("OK"));
            }
            catch (Model.ErrCallCancel ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg.modal_show();

                return;
            }

            // Unlock mutex
            if (!is_locked_mutex)
                model->mutex.unlock();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Answer the A/V call.
         * Also use this method as callback when your A/V call
         * got answered by someone else.
         *
         * @param buddy           Buddy
         * @param answer_call     false: we call, buddy answered,
         *                        true:  buddy calls, we answered,
         * @param video           false: no video,
         *                        true:  do video call
         * @param is_locked_mutex false: mutex had not locked,
         *                        true:  mutex had locked
         */
        public void av_answer_answered(Model.Buddy buddy,
                                       bool        answer_call,
                                       bool        video,
                                       bool        is_locked_mutex)
        {
            string display_name = buddy.display_name_get();
            string dbg          = _("%s: %s: ").printf(video ?
                    _("Accepted video call from buddy") :
                    _("Accepted audio call from buddy"), display_name);

            // Lock mutex
            if (!is_locked_mutex)
                model->mutex.lock();

            // Handle the incoming call accepting
            try
            {
                GLib.assert(((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                             (buddy.action_to_us   & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0)
                            ||
                            ((buddy.action_from_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0 &&
                             (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) == 0));

                // Answer the call
                if (answer_call)
                    buddy.av_answer(model, video);

                // Fill the call information
                buddy.action_from_us &= ~Model.Buddy.state_t.MASK_AV_CALL;
                buddy.action_to_us   &= ~Model.Buddy.state_t.MASK_AV_CALL;

                if (video)
                {
                    buddy.action_from_us |= Model.Buddy.state_t.ACTIVE_VIDEO_CALL;
                    buddy.action_to_us   |= Model.Buddy.state_t.ACTIVE_VIDEO_CALL;
                }
                else
                {
                    buddy.action_from_us |= Model.Buddy.state_t.ACTIVE_AUDIO_CALL;
                    buddy.action_to_us   |= Model.Buddy.state_t.ACTIVE_AUDIO_CALL;
                }

                buddy.datetime_av_answer = new GLib.DateTime.now_local();

                // Enable microphone
                if (model->peer_buddies_audio.size == 1 ^
                    model->peer_buddies_video.size == 1)
                {
                    try
                    {
                        model->audio.mic_en(model);
                    }
                    catch (Model.ErrAudio ex)
                    {
                        // Unlock mutex
                        model->mutex.unlock();

                        GLib.debug(ex.message);
                        var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                        msg_dlg.modal_show();

                        // Lock mutex
                        model->mutex.lock();
                    }

                    // Enable webcam
                    if (video && model->peer_buddies_video.size == 1)
                    {
                        try
                        {
                            model->video.webcam_en(model);
                        }
                        catch (Model.ErrVideo ex)
                        {
                            // Unlock mutex
                            model->mutex.unlock();

                            GLib.debug(ex.message);
                            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                            msg_dlg.modal_show();

                            // Lock mutex
                            model->mutex.lock();
                        }
                    }
                }

                // Start timer updating the call length on status bar
                status_timer_en(true);

                // Update buddy list,
                // new row background should be set
                int idx = model->buddies_lst.index_of(buddy);

                if (idx != -1)
                {
                    lst_ctrl_buddies_val_set(idx,
                                             buddy,

                                             true,  // Set display name column
                                             false, // Don't set status column
                                             false, // Don't set state column
                                             false  // Don't set user state column
                                            );
                }

                // A/V call GUIs elements.
                // Update status bar.
                av_widgets_upd();
                status_val_set(View.FrmMain.STATUS_COL_EVT);

                if (answer_call)
                    GLib.debug(dbg + _("OK"));
            }
            catch (Model.ErrCallAnswer ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, ex.message, _("Error"));
                msg_dlg.modal_show();

                return;
            }

            // Unlock mutex
            if (!is_locked_mutex)
                model->mutex.unlock();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update GUI widgets related to A/V
         */
        public void av_widgets_upd()
        {
            // What page is selected?
            uint page = view->frm_main.notebook_contacts.page_sel_get();

            if (page == View.FrmMain.NOTEBOOK_CONTACTS_PAGE_BUDDIES)
            {
                Model.Buddy peer_av = null;
                bool        have_av = model->peer_buddies_audio.size > 0 ||
                                      model->peer_buddies_video.size > 0;

                // Any A/V sessions?
                foreach (Model.Buddy peer in model->peer_buddies_audio.values)
                {
                    if (peer == model->peer_buddy)
                    {
                        peer_av = model->peer_buddy;
                        break;
                    }
                }

                if (peer_av == null)
                {
                    foreach (Model.Buddy peer in model->peer_buddies_video.values)
                    {
                        if (peer == model->peer_buddy)
                        {
                            peer_av = model->peer_buddy;
                            break;
                        }
                    }
                }

                if (have_av)
                {
                    // Do we have any active A/V sessions?
                    // Does the active A/V session currently opened in buddy list?
                    if (peer_av == null)
                    {
                        view->frm_main.btn_av_audio_call_tab1.en(false);
                        view->frm_main.btn_av_audio_call_tab1.show(true);
                        view->frm_main.btn_av_video_call_tab1.en(false);
                        view->frm_main.btn_av_video_call_tab1.show(true);
                        view->frm_main.btn_av_finish_tab1.show(false);
                        view->frm_main.btn_av_accept_tab1.show(false);
                        view->frm_main.btn_av_reject_tab1.show(false);
                    }
                    else
                    {
                        if ((peer_av.action_from_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0 ||

                            ((peer_av.action_from_us & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0 &&
                             (peer_av.action_to_us   & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0))
                        {
                            view->frm_main.btn_av_audio_call_tab1.show(false);
                            view->frm_main.btn_av_video_call_tab1.show(false);
                            view->frm_main.btn_av_finish_tab1.show(true);
                            view->frm_main.btn_av_accept_tab1.show(false);
                            view->frm_main.btn_av_reject_tab1.show(false);

                            av_widgets_lbl_upd();
                        }
                        else if ((peer_av.action_to_us & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0)
                        {
                            view->frm_main.btn_av_audio_call_tab1.show(false);
                            view->frm_main.btn_av_video_call_tab1.show(false);
                            view->frm_main.btn_av_finish_tab1.show(false);
                            view->frm_main.btn_av_accept_tab1.show(true);
                            view->frm_main.btn_av_reject_tab1.show(true);

                            av_widgets_lbl_upd();
                        }
                        else
                        {
                            GLib.assert(false);
                        }
                    }
                }
                else
                {
                    if (model->peer_buddy == null)
                    {
                        view->frm_main.btn_av_audio_call_tab1.en(false);
                        view->frm_main.btn_av_audio_call_tab1.show(true);
                        view->frm_main.btn_av_video_call_tab1.en(false);
                        view->frm_main.btn_av_video_call_tab1.show(true);
                        view->frm_main.btn_av_finish_tab1.show(false);
                        view->frm_main.btn_av_accept_tab1.show(false);
                        view->frm_main.btn_av_reject_tab1.show(false);
                    }
                    else
                    {
                        view->frm_main.btn_av_audio_call_tab1.en(true);
                        view->frm_main.btn_av_audio_call_tab1.show(true);
                        view->frm_main.btn_av_video_call_tab1.en(true);
                        view->frm_main.btn_av_video_call_tab1.show(true);
                        view->frm_main.btn_av_finish_tab1.show(false);
                        view->frm_main.btn_av_accept_tab1.show(false);
                        view->frm_main.btn_av_reject_tab1.show(false);
                    }
                }
            }
            else
            {
                view->frm_main.btn_av_audio_call_tab1.show(false);
                view->frm_main.btn_av_video_call_tab1.show(false);
                view->frm_main.btn_av_finish_tab1.show(false);
                view->frm_main.btn_av_accept_tab1.show(false);
                view->frm_main.btn_av_reject_tab1.show(false);
            }

            view->frm_main.btn_send_tab1.show(true, // Show
                                              true  // Update layout
                                             );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update GUI labels related to A/V
         */
        public void av_widgets_lbl_upd()
        {
            Model.Buddy? peer = model->peer_buddy;

            if (peer != null)
            {
                if ((peer.action_to_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0)
                {
                    view->frm_main.btn_av_accept_tab1.lbl_set(_("Audio Call &Accept"));
                    view->frm_main.btn_av_reject_tab1.lbl_set(_("Audio Call &Reject"));
                }
                else if ((peer.action_from_us & Model.Buddy.state_t.DOING_AUDIO_CALL) > 0 ||

                        ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0 &&
                         (peer.action_to_us   & Model.Buddy.state_t.ACTIVE_AUDIO_CALL) > 0))
                {
                    view->frm_main.btn_av_finish_tab1.lbl_set(_("Audio Call &Finish"));
                }
                else if ((peer.action_to_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0)
                {
                    view->frm_main.btn_av_accept_tab1.lbl_set(_("Video Call &Accept"));
                    view->frm_main.btn_av_reject_tab1.lbl_set(_("Video Call &Reject"));
                }
                else if ((peer.action_from_us & Model.Buddy.state_t.DOING_VIDEO_CALL) > 0 ||

                        ((peer.action_from_us & Model.Buddy.state_t.ACTIVE_VIDEO_CALL) > 0 &&
                         (peer.action_to_us   & Model.Buddy.state_t.ACTIVE_VIDEO_CALL) > 0))
                {
                    view->frm_main.btn_av_finish_tab1.lbl_set(_("Video Call &Finish"));
                }
                else
                {
                }
            }
        }
    }
}

