/*
 *    dlg_edit_profile.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Edit Profile" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgEditProfile : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Name input text control changed signal
         */
        public signal void *txt_name_changed_sig(void *fn, void *param, void *event);

        /**
         * "Browse" button click signal
         */
        public signal void *btn_browse_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Delete" button click signal
         */
        public signal void *btn_del_clicked_sig(void *fn, void *param, void *event);

        /**
         * "Edit" button click signal
         */
        public signal void *btn_edit_profile_clicked_sig(void *fn, void *param, void *event);

        /**
         * Self avatar preview.
         *
         * false: avatar has been changed at least one time during
         * session,
         * true:  avatar hasn't been changed.
         */
        public bool changed_avatar { public get; public set construct; }

        /**
         * Self avatar preview.
         *
         * Usually it is 200x200 image, but it can be smaller.
         */
        public Gdk.Pixbuf? avatar { public get; public set; }

        /**
         * Self avatar preview.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make array a property of Object
         * class. So there are wrapper method allowing to control it
         * manually: avatar_16x16_get(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_16x16;

        /**
         * Self avatar preview.
         *
         * Large 32x32 icon.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make array a property of Object
         * class. So there are wrapper method allowing to control it
         * manually: avatar_32x32_get(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_32x32;

        /**
         * Self avatar preview.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * Warning! It is forbidden to make array a property of Object
         * class. So there are wrapper method allowing to control it
         * manually: avatar_normal_get(). You may need it to use in
         * script GIR-based plugins.
         */
        public uint8[]? avatar_normal;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Name input text control changed callback
        private void *txt_name_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // "Edit" button is already enabled if avatar is to be changed
            if (this.changed_avatar)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Edit" button
            string name = view->dlg_edit_profile.txt_name.val_get();
            view->dlg_edit_profile.btn_edit_profile.en(name != model->self.name &&
                                                       name.length <= Model.Person.name_max_len);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Browse" button click callback
        private void *btn_browse_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

#if __WXMSW__
            // Attention! The modal dialog below changes current working
            // directory under M$ Windows!
            //
            // Lock mutex.
            model->mutex.lock();
#endif

            // Ask user for avatar filepath
            var file_dlg = new View.Wrapper.FileDlg(view->dlg_edit_profile,
                                                    _("Select an avatar"),
                                                    GLib.Environment.get_home_dir(),
                                                    "",
                                                    _("All files (*)|*|GIF images (*.gif)|*.gif|JPEG images (*.jpeg; *.jpg)|*.jpeg;*.jpg|PNG images (*.png)|*.png"),
                                                    View.Wrapper.FileDlg.style_t.OPEN_FILE | View.Wrapper.FileDlg.style_t.FILE_EXIST_MUST | View.Wrapper.FileDlg.style_t.PREVIEW);
            View.Wrapper.Dlg.answer_t answer = file_dlg.modal_show();

            switch (answer)
            {
            case View.Wrapper.Dlg.answer_t.OK:
                break;

            case View.Wrapper.Dlg.answer_t.CANCEL:

#if __WXMSW__
                // Unlock mutex
                model->mutex.unlock();
#endif
                return null;
                //break;

            default:
                GLib.assert(false);
                break;
            }

#if __WXMSW__
            // Unlock mutex
            model->mutex.unlock();
#endif

            // Change avatar
            string filepath = file_dlg.path_get();
            GLib.File file  = GLib.File.new_for_path(filepath);

            try
            {
                // Create three copies of chosen avatar:
                //  - 16x16,
                //  - 32x32,
                //  - 200x200.
                // Original sized avatar can be deleted.
                GLib.FileInputStream stream = file.read();
                this.avatar                 = new Gdk.Pixbuf.from_stream(stream);
                this.changed_avatar         = true;

                Gdk.Pixbuf rescaled_pix;

                rescaled_pix = model->image_rescale(this.avatar, 16, 16, true);
                rescaled_pix.save_to_buffer(out this.avatar_16x16, "png");
                rescaled_pix = model->image_rescale(this.avatar, 32, 32, true);
                rescaled_pix.save_to_buffer(out this.avatar_32x32, "png");
                rescaled_pix = model->image_rescale(this.avatar, 200, 200, false);
                rescaled_pix.save_to_buffer(out this.avatar_normal, "png");

                // Lock mutex.
                // Update GUI.
                // Unlock mutex.
                model->mutex.lock();
                view->dlg_edit_profile.img_avatar.img_set(null, // Avatar blob is to be used, do not use avatar filepath
                                                          this.avatar_normal);
                view->dlg_edit_profile.btn_del.en(true);
                view->dlg_edit_profile.btn_edit_profile.en(true);
                view->dlg_edit_profile.fit();
                view->dlg_edit_profile.refresh();
                model->mutex.unlock();
            }
            catch (GLib.Error ex)
            {
                GLib.debug(ex.message);
                var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_edit_profile, ex.message, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Delete" button click callback
        private void *btn_del_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Deallocate preview avatar
            this.changed_avatar = true;

            this.avatar        = null;
            this.avatar_16x16  = null;
            this.avatar_32x32  = null;
            this.avatar_normal = null;

            // Lock mutex.
            // Update GUI.
            // Unlock mutex.
            model->mutex.lock();
            view->dlg_edit_profile.img_avatar.img_set(Model.Person.img_filepath_default_avatar_get(),
                                                      null // Avatar filepath is to be used, do not use avatar blob
                                                     );
            view->dlg_edit_profile.btn_del.en(false);
            view->dlg_edit_profile.btn_edit_profile.en(true);
            view->dlg_edit_profile.fit();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Edit" button click callback
        private void *btn_edit_profile_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Name is changed?
            string name = view->dlg_edit_profile.txt_name.val_get();

            if (model->self.name != name)
            {
                // Set new name
                model->self.name = name;

                // Set new self name to Tox profile
                global::ToxCore.ERR_SET_INFO err;
                bool res = model->tox.self_set_name(model->self.name.data, out err);
                GLib.assert(res);
                GLib.assert(err == global::ToxCore.ERR_SET_INFO.OK);

                // Update main window widget
                view->frm_main.lbl_name.lbl_set(model->self.display_name_get());

                // Update member list
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == model->self)
                        {
                            ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                        member,

                                                                        true, // Set display name column
                                                                        false // Don't set user state column
                                                                       );
                            break;
                        }
            }

            // Has avatar been changed?
            if (this.changed_avatar)
            {
                // Set self avatar
                model->self.changed_avatar = true;

                model->self.avatar        = this.avatar;
                model->self.avatar_16x16  = this.avatar_16x16;
                model->self.avatar_32x32  = this.avatar_32x32;
                model->self.avatar_normal = this.avatar_normal;

                // Deallocate preview avatar
                this.avatar        = null;
                this.avatar_16x16  = null;
                this.avatar_32x32  = null;
                this.avatar_normal = null;

                // Send new avatar to online buddies
                foreach (Model.Buddy buddy in model->buddies_hash.values)
                {
                    if (buddy.state == Model.Person.state_t.ONLINE_TCP ||
                        buddy.state == Model.Person.state_t.ONLINE_UDP)
                    {
                        if (model->self.avatar_normal != null)
                            buddy.avatar_send(model);
                    }
                }

                // Update member list,
                // new avatar should be set
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == model->self)
                        {
                            ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                        member,

                                                                        true, // Set display name column
                                                                        false // Don't set user state column
                                                                       );

                            break;
                        }

                // Set self avatar
                ctrl->frm_main.img_avatar_set();
            }

            // Close "Edit Profile" dialog
            view->dlg_edit_profile.show(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Edit Profile" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgEditProfile()
        {
            // Connect signals to callback methods
            this.txt_name_changed_sig.connect(txt_name_changed);
            this.btn_browse_clicked_sig.connect(btn_browse_clicked);
            this.btn_del_clicked_sig.connect(btn_del_clicked);
            this.btn_edit_profile_clicked_sig.connect(btn_edit_profile_clicked);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_16x16_get()
        {
            return this.avatar_16x16;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Small 16x16 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_16x16_set(uint8[]? avatar_16x16)
        {
            this.avatar_16x16 = avatar_16x16;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Small 32x32 icon.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_32x32_get()
        {
            return this.avatar_32x32;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_32x32_set(uint8[]? avatar_32x32)
        {
            this.avatar_32x32 = avatar_32x32;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public uint8[]? avatar_normal_get()
        {
            return this.avatar_normal;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set avatar.
         *
         * Usually it is 200x200 image, but it can be smaller.
         * Currently, the PNG format is used.
         *
         * It is impossible to access the array directly from GIR-based
         * script plugins, because it is not property.
         * This method may be helpful is such cases.
         */
        public void avatar_normal_set(uint8[]? avatar_normal)
        {
            this.avatar_normal = avatar_normal;
        }
    }
}

