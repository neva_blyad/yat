/*
 *    dlg_add_buddy.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Add Buddy" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgAddBuddy : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox ID input text control changed signal
         */
        public signal void *txt_addr_changed_sig(void *fn, void *param, void *event);

        /**
         * Message input text control changed signal
         */
        public signal void *txt_msg_changed_sig(void *fn, void *param, void *event);

        /**
         * "Add" button click signal
         */
        public signal void *btn_add_buddy_clicked_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Tox ID input text control changed callback
        private void *txt_addr_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Enable/disable "Add" button.
            // Unlock mutex.
            model->mutex.lock();
            btn_add_buddy_en_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Message input text control changed callback
        private void *txt_msg_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex.
            // Enable/disable "Add" button.
            // Unlock mutex.
            model->mutex.lock();
            btn_add_buddy_en_set();
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Enable/disable "Add" button
        private void btn_add_buddy_en_set()
        {
            int    len_addr    = view->dlg_add_buddy.txt_addr.val_get().length;
            size_t addr_size   = Model.Person.addr_size;
            int    len_msg     = view->dlg_add_buddy.txt_msg.val_get().length;
            uint32 max_len_msg = global::ToxCore.max_friend_request_length();

            view->dlg_add_buddy.btn_add_buddy.en(len_addr == addr_size &&
                                                 0 < len_msg <= max_len_msg);
        }

        /*----------------------------------------------------------------------------*/

        // "Add" button click callback
        private void *btn_add_buddy_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get entered data from text input controls
            string addr  = view->dlg_add_buddy.txt_addr.val_get();
            string Alias = view->dlg_add_buddy.txt_Alias.val_get();
            string msg   = view->dlg_add_buddy.txt_msg.val_get();

            string dbg = _("Adding buddy: %s: ").printf(Alias.char_count() > 0 ? Alias : addr);

            // Add buddy
            try
            {
                // Send buddy request.
                // Add a buddy to Tox profile.
                uint8[] addr_bin = Model.Model.hex2bin_convert(addr);
                uint32  uid      = model->buddy_add(addr_bin, msg.data);

                // Add buddy to the buddy list
                string? Alias_ = Alias.char_count() > 0 ? (owned) Alias : null;
                var     buddy  = new Model.Buddy(uid,
                                                 addr,
                                                 Alias_);
                model->buddy_init(buddy);
                ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.ADD_ELEM, buddy);

                // Edit members in member lists.
                //
                // Find the buddy which has been added.
                // His state icon and avatar now should be enabled, because the
                // found member is our buddy now.
                foreach (Model.Conference conference in model->conferences_hash.values)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == null)
                        {
                            uint8[]? key = member.key_get();
                            GLib.assert(key != null);

                            if (Posix.memcmp(key,
                                             buddy.key,
                                             key.length) == 0)
                            {
                                member.person = buddy;
                                member.name   = null;
                                member.key    = null;

                                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                            member,

                                                                            true, // Set display name column
                                                                            true  // Set user state column
                                                                           );

                                break;
                            }
                        }

                // Insert new buddy into database table "buddies"
                try
                {
                    int64 add_lst = model->self.db_buddies_insert(uid,
                                                                  addr,
                                                                  Alias_,
                                                                  Model.Buddy.unread_t.FALSE,
                                                                  buddy.datetime_add_lst,
                                                                  null, // No last seen date and time
                                                                  null, // No avatar
                                                                  null, // No avatar
                                                                  null  // No avatar
                                                                 );

                    if (add_lst != -1 &&
                            add_lst != 0)
                    {
                        buddy.add_lst = add_lst;
                    }
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }

                // Close "Add Buddy" dialog.
                // Update status bar.
                view->dlg_add_buddy.show(false);
                ctrl->frm_main.status_val_set(View.FrmMain.STATUS_COL_BUDDIES);

                GLib.debug(dbg + _("OK"));
            }
            catch (GLib.Error ex)
            {
                // Unlock mutex
                model->mutex.unlock();

                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
                var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_add_buddy, ex.message, _("Error"));
                msg_dlg.modal_show();

                return null;
            }

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Add Buddy" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgAddBuddy()
        {
            // Connect signals to callback methods
            this.txt_addr_changed_sig.connect(txt_addr_changed);
            this.txt_msg_changed_sig.connect(txt_msg_changed);
            this.btn_add_buddy_clicked_sig.connect(btn_add_buddy_clicked);
        }
    }
}

