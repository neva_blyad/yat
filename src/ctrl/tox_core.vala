/*
 *    tox_core.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * File transfer errors
     */
    public errordomain ErrToxCoreFileTransfer
    {
        /**
         * Download is already active
         */
        DL_IS_ACTIVE,

        /**
         * Download is not active yet
         */
        DL_IS_NOT_ACTIVE,

        /**
         * Packet queue is full
         */
        QUEUE_FULL,

        /**
         * File is too large
         */
        TOO_LARGE,

        /**
         * Bad length or offset
         */
        BAD_LEN_OR_OFFSET,

        /**
         * Avatar is not set
         */
        SET_NOT,
    }

    /**
     * Tox class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    [SingleInstance]
    public class ToxCore : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered whenever there is a change in the
         * DHT connection state
         *
         * @param tox   Tox instance
         * @param state Network state
         * @param param Not used currently
         */
        public signal void self_state_changed_sig(global::ToxCore.Tox tox,
                                                  global::ToxCore.ConnectionStatus state,
                                                  void *param);

        /**
         * This callback is triggered when a message from a buddy is
         * received
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param type  Message type
         * @param msg   Message
         * @param param Not used currently
         */
        public signal void buddy_rx_sig(global::ToxCore.Tox          tox,
                                        uint32                       uid,
                                        global::ToxCore.MessageType  type,
                                        uint8[]                      msg,
                                        void                        *param);

        /**
         * This callback is triggered when a buddy changes their name
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param name  Name
         * @param param Not used currently
         */
        public signal void buddy_name_changed_sig(global::ToxCore.Tox  tox,
                                                  uint32               uid,
                                                  uint8[]              name,
                                                  void                *param);

        /**
         * This event is triggered when a buddy goes offline after
         * having been online, or when a buddy goes online
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param state Network state
         * @param param Not used currently
         */
        public signal void buddy_state_changed_sig(global::ToxCore.Tox               tox,
                                                   uint32                            uid,
                                                   global::ToxCore.ConnectionStatus  state,
                                                   void                             *param);

        /**
         * This callback is triggered when a buddy changes their user
         * state
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param state User state
         * @param param Not used currently
         */
        public signal void buddy_state_user_changed_sig(global::ToxCore.Tox         tox,
                                                        uint32                      uid,
                                                        global::ToxCore.UserStatus  state,
                                                        void                       *param);

        /**
         * This callback is triggered when a buddy changes their status
         * message
         *
         * @param tox    Tox instance
         * @param uid    Buddy unique identifier
         * @param status Status
         * @param param  Not used currently
         */
        public signal void buddy_status_changed_sig(global::ToxCore.Tox  tox,
                                                    uint32               uid,
                                                    uint8[]              status,
                                                    void                *param);

        /**
         * This callback is triggered when a buddy request is received
         *
         * @param tox   Tox instance
         * @param key   Public key
         * @param msg   Message
         * @param param Not used currently
         */
        public signal void buddy_req_rx_sig(global::ToxCore.Tox  tox,
                                            uint8[]              key,
                                            uint8[]              msg,
                                            void                *param);

        /**
         * This callback is triggered when a buddy request is received
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param flag  false: buddy is typing,
         *              true:  buddy is not typing
         * @param param Not used currently
         */
        public signal void buddy_typing_sig(global::ToxCore.Tox  tox,
                                            uint32               uid,
                                            bool                 flag,
                                            void                *param);

        /**
         * This callback is triggered when a file transfer request is
         * received
         *
         * @param tox      Tox instance
         * @param uid      Buddy unique identifier
         * @param file_num The friend-specific file number the data
         *                 received is associated with
         * @param kind     The meaning of the file to be sent
         * @param len      Size in bytes of the file the client wants to
         *                 send, UINT64_MAX if unknown or streaming
         * @param filename Name of the file. Does not need to be the
         *                 actual name. This name will be sent along
         *                 with the file send request.
         * @param param    Not used currently
         */
        public signal void file_req_rx_sig(global::ToxCore.Tox       tox,
                                           uint32                    uid,
                                           uint32                    file_num,
                                           global::ToxCore.FileKind  kind,
                                           uint64                    len,
                                           uint8[]                   filename,
                                           void                     *param);

        /**
         * This callback is triggered when a a file transfer request is
         * received
         *
         * @param tox      Tox instance
         * @param uid      Buddy unique identifier
         * @param file_num The friend-specific file number the data
         *                 received is associated with
         * @param offset   The file position to receive
         * @param buf      A byte array containing the received chunk
         * @param param    Not used currently
         */
        public signal void file_chunk_rx_sig(global::ToxCore.Tox  tox,
                                             uint32               uid,
                                             uint32               file_num,
                                             uint64               offset,
                                             uint8[]              buf,
                                             void                *param);

        /**
         * This callback is triggered when a Core is ready to send more
         * file data
         *
         * @param tox      Tox instance
         * @param uid      Buddy unique identifier
         * @param file_num The friend-specific file number the data
         *                 transmit is associated with
         * @param offset   The file position to transmit
         * @param len      The number of bytes requested for the current
         *                 chunk
         * @param param    Not used currently
         */
        public signal void file_chunk_tx_sig(global::ToxCore.Tox  tox,
                                             uint32               uid,
                                             uint32               file_num,
                                             uint64               offset,
                                             size_t               len,
                                             void                *param);

        /**
         * This callback is triggered when the client receives a
         * conference message
         *
         * @param tox            Tox instance
         * @param uid_conference Conference unique identifier
         * @param uid_member     Member unique identifier.
         *                       Note that it differs from buddy's uid!
         *                       They are different.
         * @param type           Message type
         * @param msg            Message
         * @param param          Not used currently
         */
        public signal void conference_rx_sig(global::ToxCore.Tox          tox,
                                             uint32                       uid_conference,
                                             uint32                       uid_member,
                                             global::ToxCore.MessageType  type,
                                             uint8[]                      msg,
                                             void                        *param);

        /**
         * This callback is triggered when somebody joins conference
         *
         * @param conference Conference
         * @param member     Member
         */
        public signal void member_joins_sig(Model.Conference conference, Model.Member member);

        /**
         * This callback is triggered when somebody leaves conference
         *
         * @param conference Conference
         * @param member     Member
         */
        public signal void member_leaves_sig(Model.Conference conference, Model.Member member);

        /**
         * This callback is triggered when when a peer changes their
         * name
         *
         * @param tox            Tox instance
         * @param uid_conference Conference unique identifier
         * @param uid_member     Member unique identifier.
         *                       Note that it differs from buddy's uid.
         *                       They are different.
         * @param name           Name
         * @param param          Not used currently
         */
        public signal void member_name_changed_sig(global::ToxCore.Tox  tox,
                                                   uint32               uid_conference,
                                                   uint32               uid_member,
                                                   uint8[]              name,
                                                   void                *param);

        /**
         * This callback is triggered when a peer changes the conference
         * title.
         *
         * @param tox            Tox instance
         * @param uid_conference Conference unique identifier
         * @param uid_member     Member unique identifier.
         *                       Note that it differs from buddy's uid!
         *                       They are different.
         * @param title          Title,
         * @param param          Not used currently.
         */
        public signal void title_changed_sig(global::ToxCore.Tox  tox,
                                             uint32               uid_conference,
                                             uint32               uid_member,
                                             uint8[]              title,
                                             void                *param);
 
        /**
         * This callback is triggered when the client is invited to join
         * a conference
         *
         * @param tox    Tox instance
         * @param uid    Buddy unique identifier
         * @param type   The conference type (text only or
         *               audio/video)
         * @param cookie A piece of data of variable length required
         *               to join the conference
         * @param param  Not used currently
         */
        public signal void conference_invited_sig(global::ToxCore.Tox  tox,
                                                  uint32               uid,
                                                  //global::ToxCore.CONFERENCE_TYPE type, // valac generates wrong code, so I disabled it
                                                  uint32               type,
                                                  uint8[]              cookie,
                                                  void                *param);

        /**
         * This callback is triggered when the client successfully
         * connects to a conference after joining it
         *
         * @param tox   Tox instance
         * @param uid   Conference unique identifier
         * @param param Not used currently
         */
        public signal void conference_connected_sig(global::ToxCore.Tox  tox,
                                                    uint32               uid,
                                                    void                *param);

        /**
         * This callback is triggered when a peer joins or leaves the
         * conference
         *
         * @param tox   Tox instance
         * @param uid   Conference unique identifier
         * @param param Not used currently
         */
        public signal void conference_members_changed_sig(global::ToxCore.Tox  tox,
                                                          uint32               uid,
                                                          void                *param);

        /*
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // This callback is triggered whenever there is a change in the
        // DHT connection state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — state Network state
        //  — param Not used currently
        private void self_state_changed(global::ToxCore.Tox tox,
                                        global::ToxCore.ConnectionStatus state,
                                        void *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            bool changed_flag = false;

            // Set self new user state
            switch (state)
            {
            // There is no connection
            case global::ToxCore.ConnectionStatus.NONE:
                if (model->self.state != Model.Person.state_t.OFFLINE)
                    changed_flag = true;
                model->self.state = Model.Person.state_t.OFFLINE;
                break;

            // A TCP connection has been established
            case global::ToxCore.ConnectionStatus.TCP:
                if (model->self.state != Model.Person.state_t.ONLINE_TCP)
                    changed_flag = true;
                model->self.state = Model.Person.state_t.ONLINE_TCP;
                break;

            // A UDP connection has been established
            case global::ToxCore.ConnectionStatus.UDP:
                if (model->self.state != Model.Person.state_t.ONLINE_UDP)
                    changed_flag = true;
                model->self.state = Model.Person.state_t.ONLINE_UDP;
                break;

            // Another state
            default:
                model->self.state = Model.Person.state_t.NONE;
                GLib.assert(false);
                break;
            }

            // Do something only if typing value has been changed
            //GLib.assert(changed_flag);

            if (changed_flag)
            {
                GLib.debug(_("Network state: ") + model->self.display_state_get(true));

                // Update icon with self network state
                try
                {
                    view->frm_main.img_state.img_set(
                        model->self.icon_filepath_state_get(model->self.cfg.get_integer("appearance", "avatar_size") == Model.Profile.cfg_appearance_avatar_size_t.SMALL),
                        null // Avatar filepath is to be used, do not use avatar blob
                    );
                }
                catch (GLib.KeyFileError ex)
                {
                    GLib.assert(false);
                }

                // Inform Model bootstrap thread that Tox network state has
                // been changed.
                // Asynchronous queue is used for communication with that
                // thread.
                var cmd = new Model.Threads.ThreadBootstrap.Cmd(Model.Threads.ThreadBootstrap.Cmd.cmd_t.HANDLE_NEW_STATE,
                                                                model->self.state);
                model->thread_bootstrap.queue_cmd.push(cmd);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a message from a buddy is
        // received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — type  Message type
        //  — msg   Message
        //  — param Not used currently
        private void buddy_rx(global::ToxCore.Tox          tox,
                              uint32                       uid,
                              global::ToxCore.MessageType  type,
                              uint8[]                      msg,
                              void                        *param)
            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            Model.Buddy buddy        = model->buddies_hash[uid];
            var         datetime     = new GLib.DateTime.now_local();
            string      msg_str      = Model.Model.arr2str_convert(msg);
            string      display_name = buddy.display_name_get();

            buddy.datetime_last_msg = datetime;
            buddy.last_msg          = (int64) Model.Buddy.last_add_lst++;

            GLib.debug(_("Incoming message from buddy: %s: %d characters"), display_name, msg_str.char_count());

            // Is chat with this buddy currently opened?
            bool upd_unread_state = false;

            if (model->peer_buddy == buddy)
            {
                // Chat with this buddy is currently opened.
                // Remove typing information.
                if ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                {
                    ulong len = view->frm_main.txt_conv_buddy_tab1.len_get();
                    view->frm_main.txt_conv_buddy_tab1.remove(len - ctrl->frm_main.txt_conv_buddy_tab1_typing_len, len);
                    ctrl->frm_main.txt_conv_buddy_tab1_typing_len = 0;
                }

                // Append new message to text control with last exchanged
                // messages
                view->frm_main.txt_conv_buddy_tab1_append(display_name,
                                                          false, // Message from buddy
                                                          msg_str,
                                                          datetime);

                // Append typing information
                if ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                    ctrl->frm_main.txt_conv_buddy_tab1_typing_len = view->frm_main.txt_conv_buddy_tab1_append_typing(buddy.display_name_get());
            }
            // Check if there are unread messages from the buddy
            else if (buddy.have_unread_msg == Model.Buddy.unread_t.FALSE)
            {
                // Chat with this buddy is not currently opened.
                // No unread messages from him was before.
                // Set there are unread messages from the buddy.
                buddy.have_unread_msg = Model.Buddy.unread_t.TRUE;
                upd_unread_state = true;
            }

            // Insert new message into database table "conv_buddies"
            try
            {
                int64 last_msg = model->self.db_conv_buddies_insert(display_name,
                                                                    buddy.uid,
                                                                    false, // Message from buddy
                                                                    msg_str,
                                                                    datetime);

                if (last_msg != -1 &&
                        last_msg != 0)
                {
                    buddy.last_msg = last_msg;
                }
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // The buddy position in buddy list may changed if sorting by
            // last message is active.
            // Also set unread icon if needed.
            //
            // Update buddy list.
            ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                                   buddy,

                                                   false, // Don't set display name column
                                                   false, // Don't set status column
                                                   false, // Don't set state column
                                                   upd_unread_state // Set user state column
                                                  );
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their name.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — name  Name
        //  — param Not used currently
        private void buddy_name_changed(global::ToxCore.Tox  tox,
                                        uint32               uid,
                                        uint8[]              name,
                                        void                *param)
            requires (param == null)
        {
            // Set buddy new name
            Model.Buddy buddy = model->buddies_hash[uid];
            string old = buddy.name;
            buddy.name = Model.Model.arr2str_convert(name);
            bool changed_flag = buddy.name != old;

            //GLib.assert(changed_flag); // Name can be the same

            // Do something only if buddy name has been changed
            if (changed_flag)
            {
                GLib.debug(_("Buddy name: %s => %s"), old == null ? _("unknown") : old,
                                                      buddy.name);

                // Continue if the buddy has no Alias
                if (buddy.Alias == null)
                {
                    // Update buddy list
                    ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                                           buddy,

                                                           true,  // Set display name column
                                                           false, // Don't set status column
                                                           false, // Don't state column
                                                           false  // Don't set user state column
                                                          );

                    // Update member list
                    Model.Conference? conference = model->peer_conference;

                    if (conference != null)
                        foreach (Model.Member member in conference.members_hash.values)
                            if (member.person == buddy)
                            {
                                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                            member,

                                                                            true, // Set display name column
                                                                            false // Don't set user state column
                                                                           );
                                break;
                            }
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This event is triggered when a buddy goes offline after
        // having been online, or when a buddy goes online.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state Network state
        //  — param Not used currently
        private void buddy_state_changed(global::ToxCore.Tox  tox,
                                         uint32               uid,
                                         global::ToxCore.ConnectionStatus state,
                                         void                *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            // Unset buddy old network state
            Model.Buddy buddy = model->buddies_hash[uid];
            bool changed_flag = false;

            switch (buddy.state)
            {
            // State is not set yet
            case Model.Person.state_t.NONE:
                GLib.assert(false);
                break;

            // User is offline
            case Model.Person.state_t.OFFLINE:

                if (state != global::ToxCore.ConnectionStatus.NONE)
                {
                    changed_flag = true;
                    Model.Buddy.offline--;
                }

                break;

            // User is online and available (TCP connection)
            case Model.Person.state_t.ONLINE_TCP:

                if (state != global::ToxCore.ConnectionStatus.TCP)
                {
                    changed_flag = true;
                    Model.Buddy.online_tcp--;
                }

                break;

            // User is online and available (UDP connection)
            case Model.Person.state_t.ONLINE_UDP:

                if (state != global::ToxCore.ConnectionStatus.UDP)
                {
                    changed_flag = true;
                    Model.Buddy.online_udp--;
                }

                break;

            default:
                GLib.assert(false);
                break;
            }

            // Do something only if typing value has been changed.
            // Send avatar.
            //GLib.assert(changed_flag);

            if (changed_flag)
            {
                // Set buddy new network state
                switch (state)
                {
                // There is no connection
                case global::ToxCore.ConnectionStatus.NONE:

                    buddy.state = Model.Person.state_t.OFFLINE;
                    buddy.datetime_seen_last = new GLib.DateTime.now_local();
                    Model.Buddy.offline++;

                    if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) > 0 || // Does we have an A/V call with the buddy whom has gone offline?
                        (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) > 0)
                    {
                        // Cancel the A/V call
                        ctrl->frm_main.av_call_cancel(buddy,
                                                      false, // Do not hang up
                                                      true,  // Update GUI
                                                      true   // Mutex is locked
                                                     );
                    }

                    break;

                // A TCP connection has been established
                case global::ToxCore.ConnectionStatus.TCP:

                    if ((buddy.state == Model.Person.state_t.NONE || buddy.state == Model.Person.state_t.OFFLINE) &&
                            model->self.avatar_normal != null)
                        buddy.avatar_send(model);
                    buddy.state = Model.Person.state_t.ONLINE_TCP;
                    Model.Buddy.online_tcp++;

                    break;

                // A UDP connection has been established
                case global::ToxCore.ConnectionStatus.UDP:

                    if ((buddy.state == Model.Person.state_t.NONE || buddy.state == Model.Person.state_t.OFFLINE) &&
                            model->self.avatar_normal != null)
                        buddy.avatar_send(model);
                    buddy.state = Model.Person.state_t.ONLINE_UDP;
                    Model.Buddy.online_udp++;

                    break;

                // Another state
                default:
                    buddy.state = Model.Person.state_t.NONE;
                    GLib.assert(false);
                    break;
                }

                GLib.debug(_("Buddy network state: %s: %s"), buddy.display_name_get(), buddy.display_state_get(true));

                // Update buddy list
                ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                                       buddy,

                                                       false, // Don't set display name column
                                                       false, // Don't set status column
                                                       true,  // Set state column
                                                       false  // Don't set user state column
                                                      );

                // Update status bar
                ctrl->frm_main.status_val_set(View.FrmMain.STATUS_COL_BUDDIES);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their user
        // state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state User state
        //  — param Not used currently
        private void buddy_state_user_changed(global::ToxCore.Tox         tox,
                                              uint32                      uid,
                                              global::ToxCore.UserStatus  state,
                                              void                       *param)

            requires (state == global::ToxCore.UserStatus.NONE ||
                      state == global::ToxCore.UserStatus.AWAY ||
                      state == global::ToxCore.UserStatus.BUSY)
            requires (param == null)
        {
            // Set buddy new user state
            Model.Buddy buddy = model->buddies_hash[uid];
            bool changed_flag = false;

            switch (state)
            {
            // User is online and available
            case global::ToxCore.UserStatus.NONE:
                if (buddy.state_user != Model.Person.state_user_t.AVAIL)
                    changed_flag = true;
                buddy.state_user = Model.Person.state_user_t.AVAIL;
                break;

            // User is away
            case global::ToxCore.UserStatus.AWAY:
                if (buddy.state_user != Model.Person.state_user_t.AWAY)
                    changed_flag = true;
                buddy.state_user = Model.Person.state_user_t.AWAY;
                break;

            // User is busy
            case global::ToxCore.UserStatus.BUSY:
                if (buddy.state_user != Model.Person.state_user_t.BUSY)
                    changed_flag = true;
                buddy.state_user = Model.Person.state_user_t.BUSY;
                break;

            // Another state
            default:
                buddy.state_user = Model.Person.state_user_t.NONE;
                GLib.assert(false);
                break;
            }

            // Do something only if typing value has been changed
            //GLib.assert(changed_flag);

            if (changed_flag)
            {
                GLib.debug(_("Buddy state: %s: %s"), buddy.display_name_get(), buddy.display_state_user_get(true));

                // Update buddy list,
                // set new state icon
                int idx = model->buddies_lst.index_of(buddy);

                if (idx != -1)
                {
                    ctrl->frm_main.lst_ctrl_buddies_val_set(idx,
                                                            buddy,

                                                            false, // Don't set display name column
                                                            false, // Don't set status column
                                                            false, // Don't state column
                                                            true   // Set user state column
                                                           );
                }

                // Update member list,
                // set new state icon
                Model.Conference? conference = model->peer_conference;

                if (conference != null)
                    foreach (Model.Member member in conference.members_hash.values)
                        if (member.person == buddy)
                        {
                            ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                        member,

                                                                        false, // Don't display name column
                                                                        true   // Set user state column
                                                                       );
                            break;
                        }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their status
        // message.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — status Status
        //  — param  Not used currently
        private void buddy_status_changed(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          uint8[]              status,
                                          void                *param)
            requires (param == null)
        {
            // Set buddy status from Tox profile
            Model.Buddy buddy = model->buddies_hash[uid];
            string status_old = buddy.status;
            buddy.status = Model.Model.arr2str_convert(status);
            bool changed_flag = buddy.status != status_old;

            // Do something only if typing value has been changed
            //GLib.assert(changed_flag);

            if (changed_flag)
            {
                GLib.debug(_("Buddy status: %s: %s"), buddy.display_name_get(), buddy.status);

                // Update buddy list
                ctrl->frm_main.lst_ctrl_buddies_change(Ctrl.action_t.EDIT_ELEM,

                                                       buddy,

                                                       false, // Don't set display name column
                                                       true,  // Set status column
                                                       false, // Don't state column
                                                       false  // Don't set user state column
                                                      );
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy request is received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — key   Public key
        //  — msg   Message
        //  — param Not used currently
        private void buddy_req_rx(global::ToxCore.Tox  tox,
                                  uint8[]              key,
                                  uint8[]              msg,
                                  void                *param)
            requires (param == null)
        {
            // Check for duplicates
            bool found_key = false;

            foreach (Model.Model.BuddyReq buddy_req_ in model->buddy_reqs)
            {
                found_key = Posix.memcmp(buddy_req_.key,
                                         key,
                                         buddy_req_.key.length) == 0;

                // Duplicate key is possible only if Tox session restarts
                if (buddy_req_.added_in_this_tox_session)
                    GLib.assert(!found_key);
                else if (found_key)
                    break;
            }

            // Convert private key from array to string
            size_t len = Model.Person.key_size;

            string key_str = "";
            for (size_t byte = 0; byte < len; byte++)
                key_str += key[byte].to_string("%02X");

            // Debug output
            string msg_str     = Model.Model.arr2str_convert(msg);
            unowned string res = found_key ? _("already added, skipping") :
                                             _("OK");

            GLib.debug(_("Buddy request: %s: %d characters: %s"), key_str,
                                                                  msg_str.char_count(),
                                                                  res);

            // Continue if no such requests are already present
            if (!found_key)
            {
                // Prepare the new buddy request
                var buddy_req = new Model.Model.BuddyReq();

                buddy_req.key      = new uint8[len];
                buddy_req.key_str  = (owned) key_str;
                buddy_req.msg      = (owned) msg_str;
                buddy_req.added_in_this_tox_session = true;
                buddy_req.datetime = new GLib.DateTime.now_local();

                Posix.memcpy(buddy_req.key,
                             key,
                             buddy_req.key.length);

                // Add buddy request.
                // Update check list box.
                model->buddy_reqs.insert(0, buddy_req);
                view->frm_main.check_lst_pub_keys_tab2_insert(buddy_req.key_str, buddy_req.datetime);

                // Insert buddy request into database table "buddy_reqs"
                try
                {
                    model->self.db_buddy_reqs_insert(buddy_req.key,
                                                     buddy_req.msg,
                                                     buddy_req.datetime);
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy starts/stops to type
        // us.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — flag  false: buddy is typing,
        //          true:  buddy is not typing
        //  — param Not used currently
        private void buddy_typing(global::ToxCore.Tox  tox,
                                  uint32               uid,
                                  bool                 flag,
                                  void                *param)
            requires (param == null)
        {
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            bool        changed_flag = ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) >  0 && !flag) ||
                                       ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) == 0 &&  flag);

            // Do something only if typing value has been changed
            //GLib.assert(changed_flag);

            if (changed_flag)
            {
                if (flag) GLib.debug(_("Buddy started typing message: %s").printf(display_name));
                else      GLib.debug(_("Buddy stopped typing message: %s").printf(display_name));

                // Append typing information to input text control with last
                // exchanged messages
                if (model->peer_buddy == buddy)
                {
                    // Remove typing information
                    if ((buddy.action_to_us & Model.Buddy.state_t.TYPING_TXT) > 0)
                    {
                        ulong len = view->frm_main.txt_conv_buddy_tab1.len_get();
                        view->frm_main.txt_conv_buddy_tab1.remove(len - ctrl->frm_main.txt_conv_buddy_tab1_typing_len, len);
                        ctrl->frm_main.txt_conv_buddy_tab1_typing_len = 0;
                    }

                    // Append typing information
                    if (flag)
                        ctrl->frm_main.txt_conv_buddy_tab1_typing_len = view->frm_main.txt_conv_buddy_tab1_append_typing(display_name);
                }

                // Set the buddy is typing or not
                if (flag)
                    buddy.action_to_us |= Model.Buddy.state_t.TYPING_KNOWN | Model.Buddy.state_t.TYPING_TXT;
                else
                {
                    buddy.action_to_us |=  Model.Buddy.state_t.TYPING_KNOWN;
                    buddy.action_to_us &= ~Model.Buddy.state_t.TYPING_TXT;
                }

                // Update buddy list,
                // new row background should be set
                int idx = model->buddies_lst.index_of(buddy);

                if (idx != -1)
                {
                    ctrl->frm_main.lst_ctrl_buddies_val_set(idx,
                                                            buddy,

                                                            true,  // Set display name column
                                                            false, // Don't set status column
                                                            false, // Don't set state column
                                                            false  // Don't set user state column
                                                           );
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — kind     The meaning of the file to be sent
        //  — len      Size in bytes of the file the client wants to
        //             send, UINT64_MAX if unknown or streaming
        //  — filename Name of the file. Does not need to be the
        //             actual name. This name will be sent along
        //             with the file send request.
        //  — param    Not used currently
        private void file_req_rx(global::ToxCore.Tox       tox,
                                 uint32                    uid,
                                 uint32                    file_num,
                                 global::ToxCore.FileKind  kind,
                                 uint64                    len,
                                 uint8[]                   filename,
                                 void                     *param)
            requires (kind == global::ToxCore.FileKind.DATA ||
                      kind == global::ToxCore.FileKind.AVATAR)
            requires (param == null)
        {
            Model.Buddy  buddy        = model->buddies_hash[uid];
            string       display_name = buddy.display_name_get();
            string       filename_str = Model.Model.arr2str_convert(filename);
            string       dbg          = _("Incoming file: %s: #%lu, %s, %llu bytes: ").printf(display_name, (ulong) file_num, filename_str, (ulong) len);
            const uint64 max_len      = 1 * 1000000L; // 1 Mb is file maximum length

            // TODO.
            //
            // Only files with number 65536 are supported by now.
            // Only avatars are supported by now.
            if (file_num != 65536)
                return;
            if (kind != global::ToxCore.FileKind.AVATAR)
                return;

            // Allow or disallow file request
            try
            {
                // Can't start new transfer if previous is not finished
                if (buddy.dl != null)
                    throw new ErrToxCoreFileTransfer.DL_IS_ACTIVE(_("Download is already active"));

                // Send a file control command to a friend for a given file
                // transfer
                global::ToxCore.ERR_FILE_CONTROL err;
                bool res = tox.file_control(uid,
                                            file_num,
                                            len <= max_len ? global::ToxCore.FileControl.RESUME :
                                                             global::ToxCore.FileControl.CANCEL,
                                            out err);

                switch (err)
                {
                // The function returned successfully
                case global::ToxCore.ERR_FILE_CONTROL.OK:
                    GLib.assert(res);
                    break;

                // The friend_number passed did not designate a valid friend
                case global::ToxCore.ERR_FILE_CONTROL.FRIEND_NOT_FOUND:
                    GLib.assert(false);
                    break;

                // This client is currently not connected to the buddy
                case global::ToxCore.ERR_FILE_CONTROL.FRIEND_NOT_CONNECTED:
                    GLib.assert(false);
                    break;

                // No file transfer with the given file number was found for
                // the given friend
                case global::ToxCore.ERR_FILE_CONTROL.NOT_FOUND:
                    GLib.assert(false);
                    break;

                // A RESUME control was sent, but the file transfer is running
                // normally
                case global::ToxCore.ERR_FILE_CONTROL.NOT_PAUSED:
                    GLib.assert(false);
                    break;

                // A RESUME control was sent, but the file transfer was paused
                // by the other party. Only the party that paused the transfer
                // can resume it.
                case global::ToxCore.ERR_FILE_CONTROL.DENIED:
                    GLib.assert(false);
                    break;

                // A PAUSE control was sent, but the file transfer was already
                // paused
                case global::ToxCore.ERR_FILE_CONTROL.ALREADY_PAUSED:
                    GLib.assert(false);
                    break;

                // Packet queue is full
                case global::ToxCore.ERR_FILE_CONTROL.SENDQ:
                    GLib.assert(!res);
                    throw new ErrToxCoreFileTransfer.QUEUE_FULL(_("Packet queue is full"));
                    //break;

                // Another state
                default:
                    GLib.assert(false);
                    break;
                }

                // Ensure file is not too large
                if (len > max_len)
                    throw new ErrToxCoreFileTransfer.TOO_LARGE(_("Too large"));

                // Everything is OK.
                // Allocate buffer and wait for the file receiving.
                GLib.debug(dbg + _("OK"));

                buddy.dl = new uint8[len];
            }
            catch (ErrToxCoreFileTransfer ex)
            {
                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — offset   The file position to receive
        //  — buf      A byte array containing the received chunk
        //  — param    Not used currently
        private void file_chunk_rx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   uint8[]              buf,
                                   void                *param)
            requires (param == null)
        {
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      dbg          = _("Incoming chunk: %s: #%lu: ").printf(display_name, (ulong) file_num);

            // TODO.
            //
            // Only files with number 65536 are supported by now.
            if (file_num != 65536)
                return;

            // Receive next chunk of a file
            try
            {
                // Can't continue a transfer if is not started
                if (buddy.dl == null)
                    throw new ErrToxCoreFileTransfer.DL_IS_NOT_ACTIVE(_("Download is not active yet"));

                // Ensure chunk length and offset are OK
                uint64 last = offset + buf.length - 1;
                if (last >= buddy.dl.length)
                    throw new ErrToxCoreFileTransfer.BAD_LEN_OR_OFFSET(_("Bad length or offset"));

                // Last chunk?
                if (buf.length == 0)
                {
                    // Last chunk.
                    // Create three copies of downloaded avatar:
                    //  - 16x16,
                    //  - 32x32,
                    //  - 200x200.
                    // Original sized avatar can be deleted.
                    var stream           = new MemoryInputStream.from_data(buddy.dl);
                    buddy.avatar         = new Gdk.Pixbuf.from_stream(stream);
                    buddy.changed_avatar = true;

                    Gdk.Pixbuf rescaled_pix;

                    rescaled_pix = model->image_rescale(buddy.avatar, 16, 16, true);
                    rescaled_pix.save_to_buffer(out buddy.avatar_16x16, "png");
                    rescaled_pix = model->image_rescale(buddy.avatar, 32, 32, true);
                    rescaled_pix.save_to_buffer(out buddy.avatar_32x32, "png");
                    rescaled_pix = model->image_rescale(buddy.avatar, 200, 200, false);
                    rescaled_pix.save_to_buffer(out buddy.avatar_normal, "png");

                    // Free buffer with downloaded chunks
                    buddy.dl = null;

                    // Update buddy list,
                    // new avatar should be set
                    int idx = model->buddies_lst.index_of(buddy);

                    if (idx != -1)
                    {
                        ctrl->frm_main.lst_ctrl_buddies_val_set(idx,
                                                                buddy,

                                                                true,  // Set display name column
                                                                false, // Don't set status column
                                                                false, // Don't set state column
                                                                false  // Don't set user state column
                                                               );
                    }

                    // Also update member list,
                    // new avatar should be set
                    Model.Conference? conference = model->peer_conference;

                    if (conference != null)
                        foreach (Model.Member member in conference.members_hash.values)
                            if (member.person == buddy)
                            {
                                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                            member,

                                                                            true, // Set display name column
                                                                            false // Don't set user state column
                                                                           );

                                break;
                            }

                    // Also update the invite list,
                    // new avatars should be set.
                    //
                    // Note that the invite list may contain multiple invitations
                    // from the same buddy.
                    for (uint idx_ = 0; idx_ < model->conference_invitations.size; idx_++)
                    {
                        Model.Model.ConferenceInvitation conference_invitation = model->conference_invitations[(int) idx_];

                        if (conference_invitation.buddy == buddy)
                        {
                            ctrl->frm_main.lst_ctrl_invites_tab3_val_set(idx_,
                                                                         conference_invitation,

                                                                         true,  // Set display name column
                                                                         false, // Don't set cookie column
                                                                         false  // Don't set date and time column
                                                                        );
                        }
                    }

                    dbg += _("finished");
                }
                else
                {
                    // Not last chunk
                    dbg += _("%llu bytes (from %llu to %llu)").printf((ulong) buf.length, (ulong) offset + 1, (ulong) last + 1);

                    Posix.memcpy(&buddy.dl[offset],
                                 buf,
                                 buf.length);
                }

                GLib.debug(dbg);
            }
            catch (ErrToxCoreFileTransfer ex)
            {
                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));

                // Free buffer with downloaded chunks
                buddy.dl = null;

                // Send a file control command to a friend for a given file
                // transfer
                tox.file_control(uid,
                                 file_num,
                                 global::ToxCore.FileControl.CANCEL,
                                 null // Ignore error
                                );
            }
            catch (GLib.Error ex)
            {
                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));

                // Free buffer with downloaded chunks
                buddy.dl = null;
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when core is ready to send more
        // file data.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             transmit is associated with
        //  — offset   The file position to transmit
        //  — len      The number of bytes requested for the current
        //             chunk
        //  — param    Not used currently
        private void file_chunk_tx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   size_t               len,
                                   void                *param)
            requires (param == null)
        {
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      dbg          = _("Outcoming chunk: %s: #%lu: ").printf(display_name, (ulong) file_num);

            // TODO.
            //
            // Only files with number 0 are supported by now.
            if (file_num != 0)
                return;

            // Send next chunk of a file
            try
            {
                // Ensure avatar is set
                if (model->self.avatar_normal == null)
                    throw new ErrToxCoreFileTransfer.SET_NOT(_("Avatar is not set"));

                // Ensure chunk length and offset are OK
                uint64 last = offset + len - 1;
                if (last >= model->self.avatar_normal.length)
                    throw new ErrToxCoreFileTransfer.BAD_LEN_OR_OFFSET(_("Bad length or offset"));

                // Last chunk?
                if (len == 0)
                    dbg += _("finished");
                else
                {
                    // Not last chunk.
                    // Continue to send avatar.
                    global::ToxCore.ERR_FILE_SEND_CHUNK err;
                    bool res = model->tox.file_send_chunk(uid,
                                                          file_num,
                                                          offset,
                                                          model->self.avatar_normal[offset:last + 1],
                                                          out err);

                    switch (err)
                    {
                    // The function returned successfully
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.OK:
                        GLib.assert(res);
                        break;

                    // The length parameter was non-zero, but data was NULL
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.NULL:
                        GLib.assert(false);
                        break;

                    // There was no buddy with the given buddy number
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.FRIEND_NOT_FOUND:
                        GLib.assert(false);
                        break;

                    // This client is currently not connected to the buddy
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.FRIEND_NOT_CONNECTED:
                        GLib.assert(false);
                        break;

                    // No file transfer with the given file number was found for the
                    // given buddy
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.NOT_FOUND:
                        GLib.assert(false);
                        break;

                    // File transfer was found but isn't in a transferring state:
                    // (paused, done, broken, etc...) (Happens only when not called
                    // from the request chunk callback).
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.NOT_TRANSFERRING:
                        GLib.assert(false);
                        break;

                    // Attempted to send more or less data than requested. The
                    // requested data size is adjusted according to maximum
                    // transmission unit and the expected end of the file. Trying to
                    // send less or more than requested will return this error.
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.INVALID_LENGTH:
                        GLib.assert(false);
                        break;

                    // Packet queue is full
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.SENDQ:
                        GLib.assert(!res);
                        throw new ErrToxCoreFileTransfer.QUEUE_FULL(_("Packet queue is full"));
                        //break;

                    // Position parameter was wrong
                    case global::ToxCore.ERR_FILE_SEND_CHUNK.WRONG_POSITION:
                        GLib.assert(false);
                        break;

                    // Another state
                    default:
                        GLib.assert(false);
                        break;
                    }

                    dbg += _("%u bytes (from %llu to %llu)").printf((uint) len, (ulong) offset + 1, (ulong) last + 1);
                }

                GLib.debug(dbg);
            }
            catch (ErrToxCoreFileTransfer ex)
            {
                GLib.debug(dbg + Model.Model.first_ch_lowercase_make(ex.message));
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client receives a
        // conference message.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — type           Message type
        //  — msg            Message
        //  — param          Not used currently
        private void conference_rx(global::ToxCore.Tox          tox,
                                   uint32                       uid_conference,
                                   uint32                       uid_member,
                                   global::ToxCore.MessageType  type,
                                   uint8[]                      msg,
                                   void                        *param)
            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            // Ignore our own messages
            global::ToxCore.ERR_CONFERENCE_PEER_QUERY err;
            bool is_ours = model->tox.conference_peer_number_is_ours(uid_conference,
                                                                     uid_member,
                                                                     out err);
            GLib.assert(err == global::ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);
            if (is_ours)
                return;

            // Some information about the message
            Model.Conference conference   = model->conferences_hash[uid_conference];
            Model.Member     member       = conference.members_hash[uid_member];
            var              datetime     = new GLib.DateTime.now_local();
            string           msg_str      = Model.Model.arr2str_convert(msg);
            string           display_name = member.display_name_get();

            conference.datetime_last_msg = datetime;
            conference.last_msg          = (int64) Model.Conference.last_add_lst++;

            // Is chat with this conference currently opened?
            bool upd_unread_state = false;

            if (model->peer_conference == conference)
            {
                // Append new message to text control with last exchanged
                // messages from/to the conference
                view->frm_main.txt_conv_conference_tab1_append(display_name,
                                                               member.fg,
                                                               member.bg,
                                                               msg_str,
                                                               datetime);
            }
            // Check if there are unread messages from the conference
            else if (conference.have_unread_msg == Model.Conference.unread_t.FALSE)
            {
                // Chat with this conference is not currently opened.
                // No unread messages from it was before.
                // Set there are unread messages from the conference.
                conference.have_unread_msg = Model.Conference.unread_t.TRUE;
                upd_unread_state = true;
            }

            GLib.debug(_("Incoming message from conference: %s: %s: %d characters"), conference.display_name_get(),
                                                                                     display_name,
                                                                                     msg_str.char_count());

            // Insert new message into database table "conv_conferences"
            try
            {
                int64 last_msg = model->self.db_conv_conferences_insert(display_name,
                                                                        uid_conference,
                                                                        member.fg,
                                                                        member.bg,
                                                                        msg_str,
                                                                        datetime);

                if (last_msg != -1 &&
                        last_msg != 0)
                {
                    conference.last_msg = last_msg;
                }
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // The conference position may changed if sorting by last
            // message is active.
            // Also set unread icon if needed.
            //
            // Update conference list
            ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                                       conference,

                                                       false, // Don't set display name column
                                                       false, // Don't set count column
                                                       upd_unread_state // Set unread state column
                                                      );
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody joins conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_joins(Model.Conference conference, Model.Member member)
        {
            GLib.debug(_("Member joined to the conference: %s: %s"), conference.display_name_get(),
                                                                     member.display_name_get());
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody leaves conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_leaves(Model.Conference conference, Model.Member member)
        {
            GLib.debug(_("Member leaved the conference: %s: %s"), conference.display_name_get(),
                                                                  member.display_name_get());
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when when a peer changes their
        // name.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid.
        //                   They are different.
        //  — name           Name
        //  — param          Not used currently
        private void member_name_changed(global::ToxCore.Tox  tox,
                                         uint32               uid_conference,
                                         uint32               uid_member,
                                         uint8[]              name,
                                         void                *param)
            requires (param == null)
        {
            Model.Conference conference = model->conferences_hash[uid_conference];
            Model.Member     member     = conference.members_hash[uid_member];

            // Do something only if the member is not ourself and is not our
            // buddy.
            // If he is our buddy, different callback is used:
            // buddy_name_changed().
            if (member.person == null)
            {
                // Set member new name
                string old = member.name;
                member.name = Model.Model.arr2str_convert(name);
                bool changed_flag = member.name != old;

                //GLib.assert(changed_flag); // Name can be the same

                // Do something only if member name has been changed
                if (changed_flag)
                {
                    GLib.debug(_("Member name in conference: %s: %s => %s"), conference.display_name_get(),
                                                                             old == null ? _("unknown") : old,
                                                                             member.name);

                    // Update member list
                    ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.EDIT_ELEM,

                                                                member,

                                                                true, // Set display name column
                                                                false // Don't set user state column
                                                               );
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer changes the conference
        // title.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — title          Title,
        //  — param          Not used currently.
        private void title_changed(global::ToxCore.Tox  tox,
                                   uint32               uid_conference,
                                   uint32               uid_member,
                                   uint8[]              title,
                                   void                *param)
            requires (param == null)
        {
            // Set new conference title
            Model.Conference conference = model->conferences_hash[uid_conference];
            string? title_old = conference.title;
            conference.title = Model.Model.arr2str_convert(title);
            bool changed_flag = title_old == null || title_old != conference.title;

            //GLib.assert(changed_flag); // Title can be the same

            // Do something only if conference title has been changed
            if (changed_flag)
            {
                // Debug otput
                string dbg;

                if (uid_member == uint32.MAX)
                {
                    // Author is unknown
                    dbg = _("Conference title: %s").printf(conference.title);
                }
                else
                {
                    // Author is known
                    Model.Member member       = conference.members_hash[uid_member];
                    string       display_name = member.display_name_get();

                    dbg = _("Conference title: %s => %s: %s").printf(title_old == null ? _("unknown") : title_old,
                                                                     conference.title,
                                                                     display_name);
                }

                GLib.debug(dbg);

                // Update conference list
                ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                                           conference,

                                                           true,  // Don't set display name column
                                                           false, // Don't set count column
                                                           false  // Don't set unread state column
                                                          );
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client is invited to join
        // a conference.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — type   The conference type (text only or audio/video)
        //  — cookie A piece of data of variable length required to join
        //           the conference
        //  — param  Not used currently
        private void conference_invited(global::ToxCore.Tox  tox,
                                        uint32               uid,
                                        uint32               type,
                                        //global::ToxCore.CONFERENCE_TYPE type, // valac generates wrong code, so I disabled it
                                        uint8[]              cookie,
                                        void                *param)
            requires (type == global::ToxCore.CONFERENCE_TYPE.TEXT ||
                      type == global::ToxCore.CONFERENCE_TYPE.AV)
            requires (param == null)
        {
            // TODO.
            //
            // A/V conferences are not supported yet.
            if (type == global::ToxCore.CONFERENCE_TYPE.AV)
            {
                GLib.debug(_("A/V conferences are not supported yet, skipping an invitation"));
                return;
            }

            // Prepare the conference type variables
            string type_str;
            Model.Profile.conference_invitation_t type_;

            switch (type)
            {
            case global::ToxCore.CONFERENCE_TYPE.TEXT:
                type_str = _("text");
                type_    = Model.Profile.conference_invitation_t.TEXT;
                break;
            //case global::ToxCore.CONFERENCE_TYPE.AV:
            //    type_str = _("A/V");
            //    type_    = Model.Profile.conference_invitation_t.AUDIO_VIDEO;
            //    break;
            default:
                type_str = "";
                type_    = Model.Profile.conference_invitation_t.NONE;
                GLib.assert(false);
                break;
            }

            // Convert cookie from array to string.
            // Ensure no such invitations are present.
            string cookie_str = Model.Model.bin2hex_convert(cookie);
            bool found_cookie = false;

            foreach (Model.Model.ConferenceInvitation conference_invitation_ in model->conference_invitations)
            {
                if (conference_invitation_.cookie.length == cookie.length)
                {
                    found_cookie = Posix.memcmp(conference_invitation_.cookie,
                                                cookie,
                                                conference_invitation_.cookie.length) == 0;
                    if (found_cookie)
                        break;
                }
            }

            Model.Buddy    buddy = model->buddies_hash[uid];
            unowned string dbg   = found_cookie ? _("already added, skipping") :
                                                  _("OK");

            GLib.debug(_("Conference invite: %s: %s: %s: %s"), buddy.display_name_get(),
                                                               type_str,
                                                               cookie_str,
                                                               dbg);

            // Prepare the new conference invitation.
            // Add it to the invite list.
            if (!found_cookie)
            {
                // Prepare the new conference invitation
                var conference_invitation = new Model.Model.ConferenceInvitation();

                conference_invitation.buddy      = buddy;
                conference_invitation.type       = (global::ToxCore.CONFERENCE_TYPE) type;
                conference_invitation.cookie     = new uint8[cookie.length];
                conference_invitation.cookie_str = (owned) cookie_str;
                conference_invitation.datetime   = new GLib.DateTime.now_local();

                Posix.memcpy(conference_invitation.cookie,
                             cookie,
                             conference_invitation.cookie.length);

                // Add it to the invite list
                ctrl->frm_main.lst_ctrl_invites_tab3_change(Ctrl.action_t.ADD_ELEM, conference_invitation);

                // Insert conference invitation into database table
                // "conference_invitations"
                try
                {
                    model->self.db_conference_invitations_insert(type_,
                                                                 conference_invitation.cookie,
                                                                 conference_invitation.datetime,
                                                                 uid);
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client successfully
        // connects to a conference after joining it.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_connected(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          void                *param)
            requires (param == null)
        {
            Model.Conference conference = model->conferences_hash[uid];

            GLib.debug(_("Conference connected: %s"), conference.display_name_get());
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer joins or leaves the
        // conference.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_members_changed(global::ToxCore.Tox  tox,
                                                uint32               uid,
                                                void                *param)
            requires (param == null)
        {
            Gee.HashMap<uint32, Model.Member> del_hash;
            Gee.HashMap<uint32, Model.Member> upd_hash;
            Gee.ArrayList<uint32>             add_lst;

            Model.Conference conference = model->conferences_hash[uid];

            // Prepare the members of the conference to delete, update and
            // to add
            del_hash = new Gee.HashMap<uint32, Model.Member>();
            upd_hash = new Gee.HashMap<uint32, Model.Member>();
            add_lst  = new Gee.ArrayList<uint32>();

            foreach (Model.Member member in conference.members_hash.values)
                del_hash[member.uid] = member;

            // Recalculate the number of offline members.
            // Recalculate the number of online members.
            conference.recalc(model, Model.Conference.recalc_kind_t.OFFLINE);
            conference.recalc(model, Model.Conference.recalc_kind_t.ONLINE);

            // Get the number of online members
            int    online = conference.online;
            uint32 cnt    = online == -1 ? 0 : online;

            // Handle all members of this conference.
            // All their UIDs should be updated!
            size_t  len = Model.Person.key_size;
            uint8[] key = new uint8[len];

            for (uint32 uid_member = 0; uid_member < cnt; uid_member++)
            {
                // Get the member public key
                global::ToxCore.ERR_CONFERENCE_PEER_QUERY err;
                bool res = model->tox.conference_peer_get_public_key(uid,
                                                                     uid_member,
                                                                     key,
                                                                     out err);
                GLib.assert(res);
                GLib.assert(err == global::ToxCore.ERR_CONFERENCE_PEER_QUERY.OK);

                // Find the member of the conference.
                // Is he already here?
                bool have_member = false;

                foreach (Model.Member member in conference.members_hash.values)
                {
                    // Compare public keys
                    uint8[]? key_ = member.key_get();
                    GLib.assert(key_ != null);

                    if (Posix.memcmp(key_,
                                     key,
                                     key_.length) == 0)
                    {
                        // Member has been found
                        have_member = true;

                        // This member doesn't leave the conference.
                        // Mark it should not be deleted.
                        // Mark his UID should be updated.
                        del_hash.unset(member.uid);
                        upd_hash[uid_member] = member;

                        break;
                    }
                }

                // Not found?
                // Mark this UID should be added to members.
                if (!have_member)
                    add_lst.add(uid_member);
            }

            // Had anyone leaved this conference?
            // Remove the old members.
            foreach (Model.Member member in del_hash.values)
            {
                // Emit signal
                this.member_leaves_sig(conference, member);

                // Remove member from the member list
                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.REMOVE_ELEM, member);
            }

            // Update old UIDs
            foreach (var entry in upd_hash.entries)
            {
                uint32       uid_member = entry.key; // Vala 0.48.9 produces warning, why?
                Model.Member member     = entry.value;

                // Update the member UID
                conference.members_hash.unset(member.uid);
                conference.members_hash[uid_member] = member;

                member.uid = uid_member;
            }

            // Had anyone joined this conference?
            // Add the new members.
            foreach (uint32 uid_member in add_lst)
            {
                // Find out if the member is ourself.
                //
                // conference_peer_number_is_ours() doesn't properly work here.
                // It can return
                // global::ToxCore.ERR_CONFERENCE_PEER_QUERY.NO_CONNECTION
                // error.
                Model.Person? person;

                if (Posix.memcmp(model->self.key,
                                 key,
                                 model->self.key.length) == 0)
                {
                    // The member is ourself
                    person = model->self;
                }
                else
                {
                    // The member is not ourself
                    person = null;

                    // Find out if he is buddy.
                    // Compare the public keys to know it.
                    foreach (Model.Buddy buddy in model->buddies_hash.values)
                    {
                        GLib.assert(buddy.key != null);

                        if (Posix.memcmp(buddy.key,
                                         key,
                                         buddy.key.length) == 0)
                        {
                            // The member is our buddy
                            person = buddy;
                            break;
                        }
                    }
                }

                // Select member from database table "members" if any.
                // Insert new member into database table "members" if not.
                uint fg;
                uint bg;

                try
                {
                    Sqlite.Statement? stmt = null;

                    bool res = model->self.db_members_sel(ref stmt,

                                                          ref key,
                                                          out fg,
                                                          out bg,
                                                              uid);

                    if (!res)
                    {
                        fg = conference.member_fg++;
                        bg = conference.member_bg++;

                        model->self.db_members_insert(key,
                                                      fg,
                                                      bg,
                                                      uid);
                    }
                }
                catch (Model.ErrProfileDB ex)
                {
                    GLib.debug(ex.message);
                }

                // Add member to the member list
                var member = new Model.Member(uid_member,
                                              person,
                                              conference,
                                              fg,
                                              bg);
                model->member_init(member,
                                   true // Set added to member list number
                                  );
                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.ADD_ELEM, member);

                // Emit signal
                this.member_joins_sig(conference, member);
            }

            // Is conference showed in list control?
            int idx = model->conferences_lst.index_of(conference);

            if (idx != -1)
            {
                // Conference is showed in GUI.
                // Update list control,
                // recalculate column with counters.
                ctrl->frm_main.lst_ctrl_conferences_val_set(idx,
                                                            conference,

                                                            false, // Don't set display name column
                                                            true,  // Set count column
                                                            false  // Don't set unread state column
                                                           );
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCore()
        {
            // Connect signals to callback methods
            this.self_state_changed_sig.connect(self_state_changed);
            this.buddy_rx_sig.connect(buddy_rx);
            this.buddy_name_changed_sig.connect(buddy_name_changed);
            this.buddy_state_changed_sig.connect(buddy_state_changed);
            this.buddy_state_user_changed_sig.connect(buddy_state_user_changed);
            this.buddy_status_changed_sig.connect(buddy_status_changed);
            this.buddy_req_rx_sig.connect(buddy_req_rx);
            this.buddy_typing_sig.connect(buddy_typing);
            this.file_req_rx_sig.connect(file_req_rx);
            this.file_chunk_rx_sig.connect(file_chunk_rx);
            this.file_chunk_tx_sig.connect(file_chunk_tx);
            this.conference_rx_sig.connect(conference_rx);
            this.member_joins_sig.connect(member_joins);
            this.member_leaves_sig.connect(member_leaves);
            this.member_name_changed_sig.connect(member_name_changed);
            this.title_changed_sig.connect(title_changed);
            this.conference_invited_sig.connect(conference_invited);
            this.conference_connected_sig.connect(conference_connected);
            this.conference_members_changed_sig.connect(conference_members_changed);
        }
    }
}

