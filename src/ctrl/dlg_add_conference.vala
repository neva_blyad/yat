/*
 *    dlg_add_conference.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the "Add Conference" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    [SingleInstance]
    public class DlgAddConference : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Title input text control changed signal
         */
        public signal void *txt_title_changed_sig(void *fn, void *param, void *event);

        /**
         * "Add" button click signal
         */
        public signal void *btn_add_conference_clicked_sig(void *fn, void *param, void *event);

        /**
         * List box with buddies select signal
         */
        public signal void *lst_box_buddies_selected_sig(void *fn, void *param, void *event);

        /**
         * Right button click signal
         */
        public signal void *btn_right_clicked_sig(void *fn, void *param, void *event);

        /**
         * Left button click signal
         */
        public signal void *btn_left_clicked_sig(void *fn, void *param, void *event);

        /**
         * List box with invited buddies select signal
         */
        public signal void *lst_box_buddies_invites_selected_sig(void *fn, void *param, void *event);

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Title input text control changed callback
        private void *txt_title_changed(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like input
            // set/clear.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable "Add" button
            int    len     = view->dlg_add_conference.txt_title.val_get().length;
            size_t max_len = Model.Person.name_max_len;

            view->dlg_add_conference.btn_add_conference.en(1 <= len <= max_len);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // "Add" button click callback
        private void *btn_add_conference_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get entered data from text input controls
            string title = view->dlg_add_conference.txt_title.val_get();
            string Alias = view->dlg_add_conference.txt_Alias.val_get();

            // Create a new conference
            global::ToxCore.ERR_CONFERENCE_NEW err;
            uint32 uid = model->tox.conference_new(out err);
            GLib.assert(err == global::ToxCore.ERR_CONFERENCE_NEW.OK);

            string? Alias_     = Alias.char_count() > 0 ? (owned) Alias : null;
            var     conference = new Model.Conference(uid, Alias_);

            model->conference_init(conference);

            // Set conference title
            conference.title = title;

            // Set conference title to Tox profile
            global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE err_;
            model->tox.conference_set_title(conference.uid,
                                            title.data,
                                            out err_);
            GLib.assert(err_ == global::ToxCore.ERR_CONFERENCE_SEND_MESSAGE.OK);

            // For each invited buddy...
            size_t cnt = view->dlg_add_conference.lst_box_buddies_invites.cnt_get();
            string? dbg_gui = null;

            for (uint idx = 0; idx < cnt; idx++)
            {
                // Find him and send invite
                string display_name = view->dlg_add_conference.lst_box_buddies_invites.val_get(idx);
                string dbg_console  = _("Invitating buddy: %s to %s").printf(display_name,
                                                                             conference.display_name_get());

                try
                {
                    // Find the buddy in buddy list
                    Model.Buddy buddy = null;

                    foreach (Model.Buddy tmp in model->buddies_hash.values)
                        if (tmp.display_name_get() == display_name)
                        {
                            buddy = tmp;
                            break;
                        }

                    if (buddy == null)
                        throw new ErrGeneral.BUDDY_NOT_FOUND_ERR(_("Buddy not found: %s").printf(display_name));

                    // Invite the buddy to the conference
                    conference.invite(model, buddy);

                    GLib.debug("%s: OK", dbg_console);
                }
                catch (ErrGeneral ex)
                {
                    GLib.debug(ex.message);
                }
                catch (Model.ErrConferenceInviteBuddy ex)
                {
                    GLib.debug("%s: %s", dbg_console, Model.Model.first_ch_lowercase_make(ex.message));
                    if (dbg_gui == null)
                        dbg_gui = _("Inviting the %s: %s").printf(display_name, Model.Model.first_ch_lowercase_make(ex.message));
                }
            }

            // Add conference to the conference list
            ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.action_t.ADD_ELEM, conference);

            // Add member to the member list
            var member = new Model.Member(0, // UID
                                          model->self,
                                          conference,
                                          conference.member_fg++,
                                          conference.member_bg++);
            model->member_init(member,
                               true // Set added to member list number
                              );
            ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.ADD_ELEM, member);

            // Update status bar
            ctrl->frm_main.status_val_set(View.FrmMain.STATUS_COL_CONFERENCES);

            // Insert new conference into database table "conferences".
            // Insert new member into database table "members".
            try
            {
                int64 add_lst = model->self.db_conferences_insert(uid,
                                                                  Alias_,
                                                                  Model.Conference.unread_t.FALSE,
                                                                  conference.datetime_add_lst
                                                                 );

                if (add_lst != -1 &&
                        add_lst != 0)
                {
                    conference.add_lst = add_lst;
                }

                uint8[]? key = member.key_get();
                GLib.assert(key != null);

                model->self.db_members_insert(key,
                                              member.fg,
                                              member.bg,
                                              conference.uid);
            }
            catch (Model.ErrProfileDB ex)
            {
                GLib.debug(ex.message);
            }

            // Close "Add Conference" dialog
            view->dlg_add_conference.show(false);

            GLib.debug(_("Adding conference: %s"), conference.display_name_get());

            // Unlock mutex
            model->mutex.unlock();

            // Show a buddy invitation error if it occurs
            if (dbg_gui != null)
            {
                var msg_dlg = new View.Wrapper.MsgDlg(view->dlg_add_conference, dbg_gui, _("Error"));
                msg_dlg.modal_show();
            }

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List box with buddies select callback
        private void *lst_box_buddies_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable right button
            size_t cnt      = view->dlg_add_conference.lst_box_buddies.cnt_get();
            bool   have_sel = false;

            for (uint idx = 0; idx < cnt; idx++)
            {
                if (view->dlg_add_conference.lst_box_buddies.sel_get(idx))
                {
                    have_sel = true;
                    break;
                }
            }

            view->dlg_add_conference.btn_right.en(have_sel);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Right button click callback
        private void *btn_right_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get selections from the right list box
            size_t cnt     = view->dlg_add_conference.lst_box_buddies.cnt_get();
            var    sel     = new uint[cnt];
            size_t cnt_sel = view->dlg_add_conference.lst_box_buddies.selections_elem_get(sel);

            GLib.assert(0 < cnt_sel <= cnt);

            // Add selected buddies to the right list box
            for (uint idx = 0; idx < cnt_sel; idx++)
            {
                string display_name = view->dlg_add_conference.lst_box_buddies.val_get(sel[idx]);
                view->dlg_add_conference.lst_box_buddies_invites.append(display_name);
            }

            // Remove selected buddies from the left list box
            ctrl->ctrl_changed_by_user = false;
            for (int idx = (int) cnt_sel - 1; idx >= 0; idx--)
                view->dlg_add_conference.lst_box_buddies.remove(sel[idx]);
            ctrl->ctrl_changed_by_user = true;

            // Disable right button if needed
            cnt_sel = view->dlg_add_conference.lst_box_buddies.selections_elem_get(sel);
            if (cnt_sel == 0)
                view->dlg_add_conference.btn_right.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // Left button click callback
        private void *btn_left_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Get selections from the left list box
            size_t cnt     = view->dlg_add_conference.lst_box_buddies_invites.cnt_get();
            var    sel     = new uint[cnt];
            size_t cnt_sel = view->dlg_add_conference.lst_box_buddies_invites.selections_elem_get(sel);

            GLib.assert(0 < cnt_sel <= cnt);

            // Add selected buddies to the left list box
            for (uint idx = 0; idx < cnt_sel; idx++)
            {
                string display_name = view->dlg_add_conference.lst_box_buddies_invites.val_get(sel[idx]);
                view->dlg_add_conference.lst_box_buddies.append(display_name);
            }

            // Remove selected buddies from the right list box
            ctrl->ctrl_changed_by_user = false;
            for (int idx = (int) cnt_sel - 1; idx >= 0; idx--)
                view->dlg_add_conference.lst_box_buddies_invites.remove(sel[idx]);
            ctrl->ctrl_changed_by_user = true;

            // Disable left button if needed
            cnt_sel = view->dlg_add_conference.lst_box_buddies_invites.selections_elem_get(sel);
            if (cnt_sel == 0)
                view->dlg_add_conference.btn_left.en(false);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/

        // List box with invited buddies select callback.
        private void *lst_box_buddies_invites_selected(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // This method is also called by GUI methods like selection
            // set.
            // No actions in this case.
            if (!ctrl->ctrl_changed_by_user)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Enable/disable left button
            size_t cnt      = view->dlg_add_conference.lst_box_buddies_invites.cnt_get();
            bool   have_sel = false;

            for (uint idx = 0; idx < cnt; idx++)
            {
                if (view->dlg_add_conference.lst_box_buddies_invites.sel_get(idx))
                {
                    have_sel = true;
                    break;
                }
            }

            view->dlg_add_conference.btn_left.en(have_sel);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Add Conference" dialog callback constructor.
         *
         * Connects signals to callback methods.
         */
        public DlgAddConference()
        {
            // Connect signals to callback methods
            this.txt_title_changed_sig.connect(txt_title_changed);
            this.btn_add_conference_clicked_sig.connect(btn_add_conference_clicked);
            this.lst_box_buddies_selected_sig.connect(lst_box_buddies_selected);
            this.btn_right_clicked_sig.connect(btn_right_clicked);
            this.btn_left_clicked_sig.connect(btn_left_clicked);
            this.lst_box_buddies_invites_selected_sig.connect(lst_box_buddies_invites_selected);
        }
    }
}

