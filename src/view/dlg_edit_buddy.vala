/*
 *    dlg_edit_buddy.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Edit Buddy" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgEditBuddy : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Name label
         */
        public Wrapper.StaticTxt lbl_name { public get; public set; }

        /**
         * Name label. (Contains buddy's name.)
         */
        public Wrapper.StaticTxt lbl_name_ { public get; public set; }

        /**
         * Alias label
         */
        public Wrapper.StaticTxt lbl_Alias { public get; public set; }

        /**
         * Alias input text control
         */
        public Wrapper.TxtCtrl txt_Alias { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * "Edit" button
         */
        public Wrapper.Btn btn_edit_buddy { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.lbl_name       = new Wrapper.StaticTxt();
            this.lbl_name_      = new Wrapper.StaticTxt();
            this.lbl_Alias      = new Wrapper.StaticTxt();
            this.txt_Alias      = new Wrapper.TxtCtrl();
            this.btn_cancel     = new Wrapper.Btn();
            this.btn_edit_buddy = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                = xml.dlg_load(null, new Wx.Str("dlg_edit_buddy"));
            GLib.assert(this.win                != null);
            this.lbl_name.win       = this.win.win_find(new Wx.Str("lbl_name"));
            GLib.assert(this.lbl_name.win       != null);
            this.lbl_name_.win      = this.win.win_find(new Wx.Str("lbl_name_"));
            GLib.assert(this.lbl_name_.win      != null);
            this.lbl_Alias.win      = this.win.win_find(new Wx.Str("lbl_Alias"));
            GLib.assert(this.lbl_Alias.win      != null);
            this.txt_Alias.win      = this.win.win_find(new Wx.Str("txt_Alias"));
            GLib.assert(this.txt_Alias.win      != null);
            this.btn_cancel.win     = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win     != null);
            this.btn_edit_buddy.win = this.win.win_find(new Wx.Str("btn_edit_buddy"));
            GLib.assert(this.btn_edit_buddy.win != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.txt_Alias.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_buddy.txt_Alias_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.btn_edit_buddy.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_buddy.btn_edit_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Edit Buddy" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgEditBuddy(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

