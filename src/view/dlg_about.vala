/*
 *    dlg_about.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "About Profile" / "About Buddy" / "About
     * Conference" / "About Member" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgAbout : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Self/buddy avatar
         */
        public Wrapper.StaticBMP img_avatar { public get; public set; }

        /**
         * About input text control
         */
        public Wrapper.TxtCtrl txt_about { public get; public set; }

        /**
         * "Close" button
         */
        public Wrapper.Btn btn_close_about { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.img_avatar      = new Wrapper.StaticBMP();
            this.txt_about       = new Wrapper.TxtCtrl();
            this.btn_close_about = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                 = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_about"));
            GLib.assert(this.win                 != null);
            this.img_avatar.win      = this.win.win_find(new Wx.Str("img_avatar"));
            GLib.assert(this.img_avatar          != null);
            this.txt_about.win       = this.win.win_find(new Wx.Str("txt_about"));
            GLib.assert(this.txt_about.win       != null);
            this.btn_close_about.win = this.win.win_find(new Wx.Str("btn_close_about"));
            GLib.assert(this.btn_close_about.win != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_close_about.win.id_set(Wx.Win.id_t.CANCEL);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "About Profile" / "About Buddy" / "About Conference" / "About
         * Member" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgAbout(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set self/buddy information to about input text control
         *
         * @param profile_name       Profile name
         * @param name               Name
         * @param Alias              Local Alias
         * @param addr               Tox ID
         * @param state              Network state
         * @param state_user         User state
         * @param status             Status
         * @param datetime_add_lst   Date and time of the adding to
         *                           buddy list
         * @param datetime_seen_last Last seen date and time
         */
        public void txt_about_val_person_set(string? profile_name,
                                             string  name,
                                             string? Alias,
                                             string  addr,
                                             string  state,
                                             string  state_user,
                                             string  status,
                                             string? datetime_add_lst,
                                             string? datetime_seen_last)
        {
            this.txt_about.clr();

            if (profile_name != null)       this.txt_about.append(_("Profile: ") + profile_name + "\n");
                                            this.txt_about.append(_("Name: ") + name + "\n");
            if (Alias != null)              this.txt_about.append(_("Local Alias: ") + Alias + "\n");
                                            this.txt_about.append(_("Tox ID: ") + addr + "\n");
                                            this.txt_about.append(_("Network: ") + state + "\n");
                                            this.txt_about.append(_("State: ") + state_user + "\n");
                                            this.txt_about.append(_("Status: ") + status + (datetime_add_lst == null && datetime_seen_last == null ? "" : "\n"));
            if (datetime_add_lst != null)   this.txt_about.append(_("Added: ") + datetime_add_lst + (datetime_seen_last == null ? "" : "\n"));
            if (datetime_seen_last != null) this.txt_about.append(_("Last seen: ") + datetime_seen_last);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set member information to about input text control
         *
         * @param name     Name
         * @param is_buddy Does this member present in buddy list?
         */
        public void txt_about_val_member_set(string name,
                                             string is_buddy)
        {
            this.txt_about.clr();
            this.txt_about.append(_("Name: ") + name + "\n");
            this.txt_about.append(_("Have in buddy list: ") + is_buddy);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set conference information to about input text control
         *
         * @param title            Title
         * @param Alias            Local Alias
         * @param cnt              Number of the currently online and
         *                         total members, use slash (/) between
         *                         these numbers
         * @param all              Total number of members
         * @param datetime_add_lst Date and time of the adding to buddy
         *                         list
         */
        public void txt_about_val_conference_set(string title,
                                                 string Alias,
                                                 string cnt,
                                                 size_t all,
                                                 string datetime_add_lst)
        {
            this.txt_about.clr();
            this.txt_about.append(_("Title: ") + title + "\n");
            this.txt_about.append(_("Local Alias: ") + Alias + "\n");
            this.txt_about.append(GLib.dngettext(Cfg.DOMAIN, "Online/Total: %s member\n", "Online/Total: %s members\n", all).printf(cnt));
            this.txt_about.append(_("Added: ") + datetime_add_lst);
        }
    }
}

