/*
 *    dlg_settings.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Settings" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgSettings : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Notebook
         */
        public Wrapper.Notebook notebook { public get; public set; }

        /*
         * "General" page in notebook
         */
        public const uint NOTEBOOK_PAGE_GENERAL    = 0;

        /*
         * "Appearance" page in notebook
         */
        public const uint NOTEBOOK_PAGE_APPEARANCE = 1;

        /*
         * "Network" page in notebook
         */
        public const uint NOTEBOOK_PAGE_NET        = 2;

        /*
         * "Privacy" page in notebook
         */
        public const uint NOTEBOOK_PAGE_PRIVACY    = 3;

        /**
         * Tab 1 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab1 { public get; public set; }

        /**
         * "Show 'Choose Profile' dialog at start" check box.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.CheckBox check_show_profile_tab1 { public get; public set; }

        /**
         * Tab 2 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab2 { public get; public set; }

        /**
         * Language label.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.StaticTxt lbl1_tab2 { public get; public set; }

        /**
         * Language choice.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Choice choice_lang_tab2 { public get; public set; }

        /**
         * Line.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.StaticLine line1_tab2 { public get; public set; }

        /**
         * Avatar sizes label.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.StaticTxt lbl2_tab2 { public get; public set; }

        /**
         * Small 16x16 avatars radio button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.RadioBtn radio_avatar_size_16x16_tab2 { public get; public set; }

        /**
         * Big 32x32 avatars radio button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.RadioBtn radio_avatar_size_32x32_tab2 { public get; public set; }

        /**
         * Tab 3 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab3 { public get; public set; }

        /**
         * Bootstrap nodes label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl1_tab3 { public get; public set; }

        /**
         * Node source choice.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Choice choice_node_tab3 { public get; public set; }

        /**
         * URL index in node choice
         */
        public const uint CHOICE_NODE_IDX_URL   = 0;

        /**
         * File index in node choice
         */
        public const uint CHOICE_NODE_IDX_FILE  = 1;

        /**
         * Buddy index in node choice
         */
        public const uint CHOICE_NODE_IDX_BUDDY = 2;

        /**
         * URL input text control.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.TxtCtrl txt_url_tab3 { public get; public set; }

        /**
         * File picker.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.FilePickerCtrl file_tab3 { public get; public set; }

        /**
         * Buddy choice.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Choice choice_buddy_tab3 { public get; public set; }

        /**
         * Buddy host label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl_buddy_host_tab3 { public get; public set; }

        /**
         * Buddy host input text control.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.TxtCtrl txt_buddy_host_tab3 { public get; public set; }

        /**
         * Buddy port label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl_buddy_port_tab3 { public get; public set; }

        /**
         * Buddy port spin control.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.SpinCtrl spin_buddy_port_tab3 { public get; public set; }

        /**
         * "Add" button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_add_tab3 { public get; public set; }

        /**
         * "Remove" button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_remove_tab3 { public get; public set; }

        /**
         * Up button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_up_tab3 { public get; public set; }

        /**
         * Down button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_down_tab3 { public get; public set; }

        /**
         * Check list box with nodes.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.CheckLstBox check_lst_node_tab3 { public get; public set; }

        /**
         * Line.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticLine line1_tab3 { public get; public set; }

        /**
         * Connection settings label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl2_tab3 { public get; public set; }

        /**
         * "Enable the use of UDP communication when available" check
         * box
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.CheckBox check_udp_tab3 { public get; public set; }

        /**
         * Line.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticLine line2_tab3 { public get; public set; }

        /**
         * Proxy label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl3_tab3 { public get; public set; }

        /**
         * Proxy type label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl_proxy_tab3 { public get; public set; }

        /**
         * Proxy type choice.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Choice choice_proxy_tab3 { public get; public set; }

        /**
         * None index in proxy type choice
         */
        public const uint CHOICE_PROXY_IDX_NONE   = 0;

        /**
         * HTTP index in proxy type choice
         */
        public const uint CHOICE_PROXY_IDX_HTTP   = 1;

        /**
         * SOCKS5 index in proxy type choice
         */
        public const uint CHOICE_PROXY_IDX_SOCKS5 = 2;

        /**
         * Proxy host label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl_proxy_host_tab3 { public get; public set; }

        /**
         * Proxy host input text control.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.TxtCtrl txt_proxy_host_tab3 { public get; public set; }

        /**
         * Proxy port label.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.StaticTxt lbl_proxy_port_tab3 { public get; public set; }

        /**
         * Proxy port spin control.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.SpinCtrl spin_proxy_port_tab3 { public get; public set; }

        /**
         * Tab 4 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab4 { public get; public set; }

        /**
         * "Send typing notifications" check box.
         *
         * It is placed on the tab 4 panel.
         */
        public Wrapper.CheckBox check_send_typing_tab4 { public get; public set; }

        /**
         * Tab 5 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab5 { public get; public set; }

        /**
         * Input/Output label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl1_tab5 { public get; public set; }

        /**
         * Audio subsystem label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl_sys_tab5 { public get; public set; }

        /**
         * Audio subsystem choice.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.Choice choice_sys_tab5 { public get; public set; }

        /**
         * Input device label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl2_tab5 { public get; public set; }

        /**
         * Capture device label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl_cap_tab5 { public get; public set; }

        /**
         * Capture device choice.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.Choice choice_cap_tab5 { public get; public set; }

        /**
         * Detect automatically index in capture device choice
         */
        public const uint CHOICE_CAP_IDX_AUTO = 0;

        /**
         * Gain label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl_gain_tab5 { public get; public set; }

        /**
         * Gain slider label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.Slider slider_gain_tab5 { public get; public set; }

        /**
         * Output device label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl3_tab5 { public get; public set; }

        /**
         * Playback device label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl_playback_tab5 { public get; public set; }

        /**
         * Playback device choice.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Choice choice_playback_tab5 { public get; public set; }

        /**
         * Detect automatically in node choice
         */
        public const uint CHOICE_PLAYBACK_IDX_AUTO = 0;

        /**
         * Volume label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.StaticTxt lbl_vol_tab5 { public get; public set; }

        /**
         * Volume slider label.
         *
         * It is placed on the tab 5 panel.
         */
        public Wrapper.Slider slider_vol_tab5 { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * "Reset" button
         */
        public Wrapper.Btn btn_reset { public get; public set; }

        /**
         * "Apply" button
         */
        public Wrapper.Btn btn_apply_settings { public get; public set; }

        /**
         * Context menu for "Reset" button
         */
        public Wrapper.Menu menu_reset { public get; public set; }

        /**
         * "Reset to last saved settings" menu item.
         *
         * It is placed in context menu for "Reset" button.
         */
        public Wrapper.MenuItem menu_reset_last { public get; public set; }

        /**
         * "Reset to program default settings" menu item.
         *
         * It is placed in context menu for "Reset" button.
         */
        public Wrapper.MenuItem menu_reset_default { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        // Setups the widgets.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.notebook                     = new Wrapper.Notebook();
            this.panel_tab1                   = new Wrapper.Panel();
            this.check_show_profile_tab1      = new Wrapper.CheckBox();
            this.panel_tab2                   = new Wrapper.Panel();
            this.lbl1_tab2                    = new Wrapper.StaticTxt();
            this.choice_lang_tab2             = new Wrapper.Choice();
            this.line1_tab2                   = new Wrapper.StaticLine();
            this.lbl2_tab2                    = new Wrapper.StaticTxt();
            this.radio_avatar_size_16x16_tab2 = new Wrapper.RadioBtn();
            this.radio_avatar_size_32x32_tab2 = new Wrapper.RadioBtn();
            this.panel_tab3                   = new Wrapper.Panel();
            this.lbl1_tab3                    = new Wrapper.StaticTxt();
            this.choice_node_tab3             = new Wrapper.Choice();
            this.txt_url_tab3                 = new Wrapper.TxtCtrl();
            this.file_tab3                    = new Wrapper.FilePickerCtrl();
            this.choice_buddy_tab3            = new Wrapper.Choice();
            this.lbl_buddy_host_tab3          = new Wrapper.StaticTxt();
            this.txt_buddy_host_tab3          = new Wrapper.TxtCtrl();
            this.lbl_buddy_port_tab3          = new Wrapper.StaticTxt();
            this.spin_buddy_port_tab3         = new Wrapper.SpinCtrl();
            this.btn_add_tab3                 = new Wrapper.Btn();
            this.btn_remove_tab3              = new Wrapper.Btn();
            this.btn_up_tab3                  = new Wrapper.Btn();
            this.btn_down_tab3                = new Wrapper.Btn();
            this.check_lst_node_tab3          = new Wrapper.CheckLstBox();
            this.line1_tab3                   = new Wrapper.StaticLine();
            this.lbl2_tab3                    = new Wrapper.StaticTxt();
            this.check_udp_tab3               = new Wrapper.CheckBox();
            this.line2_tab3                   = new Wrapper.StaticLine();
            this.lbl3_tab3                    = new Wrapper.StaticTxt();
            this.lbl_proxy_tab3               = new Wrapper.StaticTxt();
            this.choice_proxy_tab3            = new Wrapper.Choice();
            this.lbl_proxy_host_tab3          = new Wrapper.StaticTxt();
            this.txt_proxy_host_tab3          = new Wrapper.TxtCtrl();
            this.lbl_proxy_port_tab3          = new Wrapper.StaticTxt();
            this.spin_proxy_port_tab3         = new Wrapper.SpinCtrl();
            this.panel_tab4                   = new Wrapper.Panel();
            this.check_send_typing_tab4       = new Wrapper.CheckBox();
            this.btn_cancel                   = new Wrapper.Btn();
            this.btn_reset                    = new Wrapper.Btn();
            this.btn_apply_settings           = new Wrapper.Btn();
            this.panel_tab5                   = new Wrapper.Panel();
            this.lbl1_tab5                    = new Wrapper.StaticTxt();
            this.lbl_sys_tab5                 = new Wrapper.StaticTxt();
            this.choice_sys_tab5              = new Wrapper.Choice();
            this.lbl2_tab5                    = new Wrapper.StaticTxt();
            this.lbl_cap_tab5                 = new Wrapper.StaticTxt();
            this.choice_cap_tab5              = new Wrapper.Choice();
            this.lbl_gain_tab5                = new Wrapper.StaticTxt();
            this.slider_gain_tab5             = new Wrapper.Slider();
            this.lbl3_tab5                    = new Wrapper.StaticTxt();
            this.lbl_playback_tab5            = new Wrapper.StaticTxt();
            this.choice_playback_tab5         = new Wrapper.Choice();
            this.lbl_vol_tab5                 = new Wrapper.StaticTxt();
            this.slider_vol_tab5              = new Wrapper.Slider();

            this.menu_reset                   = new Wrapper.Menu();
            this.menu_reset_last              = new Wrapper.MenuItem();
            this.menu_reset_default           = new Wrapper.MenuItem();

            // Find/load the widgets in XRC file
            this.win                               = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_settings"));
            GLib.assert(this.win                              != null);
            this.notebook.win                      = this.win.win_find(new Wx.Str("notebook"));
            GLib.assert(this.notebook.win                     != null);
            this.panel_tab1.win                    = this.notebook.win.win_find(new Wx.Str("panel_tab1"));
            GLib.assert(this.panel_tab1.win                   != null);
            this.check_show_profile_tab1.win       = this.panel_tab1.win.win_find(new Wx.Str("check_show_profile_tab1"));
            GLib.assert(this.check_show_profile_tab1.win      != null);
            this.panel_tab2.win                    = this.notebook.win.win_find(new Wx.Str("panel_tab2"));
            GLib.assert(this.panel_tab2.win                   != null);
            this.lbl1_tab2.win                     = this.panel_tab2.win.win_find(new Wx.Str("lbl1_tab2"));
            GLib.assert(this.lbl1_tab2.win                    != null);
            this.choice_lang_tab2.win              = this.panel_tab2.win.win_find(new Wx.Str("choice_lang_tab2"));
            GLib.assert(this.choice_lang_tab2.win             != null);
            this.line1_tab2.win                    = this.panel_tab2.win.win_find(new Wx.Str("line1_tab2"));
            GLib.assert(this.line1_tab2.win                   != null);
            this.lbl2_tab2.win                     = this.panel_tab2.win.win_find(new Wx.Str("lbl2_tab2"));
            GLib.assert(this.lbl2_tab2.win                    != null);
            this.radio_avatar_size_16x16_tab2.win  = this.panel_tab2.win.win_find(new Wx.Str("radio_avatar_size_16x16_tab2"));
            GLib.assert(this.radio_avatar_size_16x16_tab2.win != null);
            this.radio_avatar_size_32x32_tab2.win  = this.panel_tab2.win.win_find(new Wx.Str("radio_avatar_size_32x32_tab2"));
            GLib.assert(this.radio_avatar_size_32x32_tab2.win != null);
            this.panel_tab3.win                    = this.notebook.win.win_find(new Wx.Str("panel_tab3"));
            GLib.assert(this.panel_tab3.win                   != null);
            this.lbl1_tab3.win                     = this.panel_tab3.win.win_find(new Wx.Str("lbl1_tab3"));
            GLib.assert(this.lbl1_tab3.win                    != null);
            this.choice_node_tab3.win              = this.panel_tab3.win.win_find(new Wx.Str("choice_node_tab3"));
            GLib.assert(this.choice_node_tab3.win             != null);
            this.txt_url_tab3.win                  = this.panel_tab3.win.win_find(new Wx.Str("txt_url_tab3"));
            GLib.assert(this.txt_url_tab3.win                 != null);
            this.file_tab3.win                     = this.panel_tab3.win.win_find(new Wx.Str("file_tab3"));
            GLib.assert(this.file_tab3.win                    != null);
            this.choice_buddy_tab3.win             = this.panel_tab3.win.win_find(new Wx.Str("choice_buddy_tab3"));
            GLib.assert(this.choice_buddy_tab3.win            != null);
            this.lbl_buddy_host_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("lbl_buddy_host_tab3"));
            GLib.assert(this.lbl_buddy_host_tab3.win          != null);
            this.txt_buddy_host_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("txt_buddy_host_tab3"));
            GLib.assert(this.txt_buddy_host_tab3.win          != null);
            this.lbl_buddy_port_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("lbl_buddy_port_tab3"));
            GLib.assert(this.lbl_buddy_port_tab3.win          != null);
            this.spin_buddy_port_tab3.win          = this.panel_tab3.win.win_find(new Wx.Str("spin_buddy_port_tab3"));
            GLib.assert(this.spin_buddy_port_tab3.win         != null);
            this.btn_add_tab3.win                  = this.panel_tab3.win.win_find(new Wx.Str("btn_add_tab3"));
            GLib.assert(this.btn_add_tab3.win                 != null);
            this.btn_remove_tab3.win               = this.panel_tab3.win.win_find(new Wx.Str("btn_remove_tab3"));
            GLib.assert(this.btn_remove_tab3.win              != null);
            this.btn_up_tab3.win                   = this.panel_tab3.win.win_find(new Wx.Str("btn_up_tab3"));
            GLib.assert(this.btn_up_tab3.win                  != null);
            this.btn_down_tab3.win                 = this.panel_tab3.win.win_find(new Wx.Str("btn_down_tab3"));
            GLib.assert(this.btn_down_tab3.win                != null);
            this.check_lst_node_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("check_lst_node_tab3"));
            GLib.assert(this.check_lst_node_tab3.win          != null);
            this.line1_tab3.win                    = this.panel_tab3.win.win_find(new Wx.Str("line1_tab3"));
            GLib.assert(this.line1_tab3.win                   != null);
            this.lbl2_tab3.win                     = this.panel_tab3.win.win_find(new Wx.Str("lbl2_tab3"));
            GLib.assert(this.lbl2_tab3.win                    != null);
            this.check_udp_tab3.win                = this.panel_tab3.win.win_find(new Wx.Str("check_udp_tab3"));
            GLib.assert(this.check_udp_tab3.win               != null);
            this.line2_tab3.win                    = this.panel_tab3.win.win_find(new Wx.Str("line2_tab3"));
            GLib.assert(this.line2_tab3.win                   != null);
            this.lbl3_tab3.win                     = this.panel_tab3.win.win_find(new Wx.Str("lbl3_tab3"));
            GLib.assert(this.lbl3_tab3.win                    != null);
            this.lbl_proxy_tab3.win                = this.panel_tab3.win.win_find(new Wx.Str("lbl_proxy_tab3"));
            GLib.assert(this.lbl_proxy_tab3.win               != null);
            this.choice_proxy_tab3.win             = this.panel_tab3.win.win_find(new Wx.Str("choice_proxy_tab3"));
            GLib.assert(this.choice_proxy_tab3.win            != null);
            this.lbl_proxy_host_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("lbl_proxy_host_tab3"));
            GLib.assert(this.lbl_proxy_host_tab3.win          != null);
            this.txt_proxy_host_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("txt_proxy_host_tab3"));
            GLib.assert(this.txt_proxy_host_tab3.win          != null);
            this.lbl_proxy_port_tab3.win           = this.panel_tab3.win.win_find(new Wx.Str("lbl_proxy_port_tab3"));
            GLib.assert(this.lbl_proxy_port_tab3.win          != null);
            this.spin_proxy_port_tab3.win          = this.panel_tab3.win.win_find(new Wx.Str("spin_proxy_port_tab3"));
            GLib.assert(this.spin_proxy_port_tab3.win         != null);
            this.panel_tab4.win                    = this.notebook.win.win_find(new Wx.Str("panel_tab4"));
            GLib.assert(this.panel_tab4.win                   != null);
            this.check_send_typing_tab4.win        = this.panel_tab4.win.win_find(new Wx.Str("check_send_typing_tab4"));
            GLib.assert(this.check_send_typing_tab4.win       != null);
            this.panel_tab5.win                    = this.notebook.win.win_find(new Wx.Str("panel_tab5"));
            GLib.assert(this.panel_tab5.win                   != null);
            this.lbl1_tab5.win                     = this.panel_tab5.win.win_find(new Wx.Str("lbl1_tab5"));
            GLib.assert(this.lbl1_tab5.win                    != null);
            this.lbl_sys_tab5.win                  = this.panel_tab5.win.win_find(new Wx.Str("lbl_sys_tab5"));
            GLib.assert(this.lbl_sys_tab5.win                 != null);
            this.choice_sys_tab5.win               = this.panel_tab5.win.win_find(new Wx.Str("choice_sys_tab5"));
            GLib.assert(this.choice_sys_tab5.win              != null);
            this.lbl2_tab5.win                     = this.panel_tab5.win.win_find(new Wx.Str("lbl2_tab5"));
            GLib.assert(this.lbl2_tab5.win                    != null);
            this.lbl_cap_tab5.win                  = this.panel_tab5.win.win_find(new Wx.Str("lbl_cap_tab5"));
            GLib.assert(this.lbl_cap_tab5.win                 != null);
            this.choice_cap_tab5.win               = this.panel_tab5.win.win_find(new Wx.Str("choice_cap_tab5"));
            GLib.assert(this.choice_cap_tab5.win              != null);
            this.lbl_gain_tab5.win                 = this.panel_tab5.win.win_find(new Wx.Str("lbl_gain_tab5"));
            GLib.assert(this.lbl_gain_tab5.win                != null);
            this.slider_gain_tab5.win              = this.panel_tab5.win.win_find(new Wx.Str("slider_gain_tab5"));
            GLib.assert(this.slider_gain_tab5.win             != null);
            this.lbl3_tab5.win                     = this.panel_tab5.win.win_find(new Wx.Str("lbl3_tab5"));
            GLib.assert(this.lbl3_tab5.win                    != null);
            this.lbl_playback_tab5.win             = this.panel_tab5.win.win_find(new Wx.Str("lbl_playback_tab5"));
            GLib.assert(this.lbl_playback_tab5.win            != null);
            this.choice_playback_tab5.win          = this.panel_tab5.win.win_find(new Wx.Str("choice_playback_tab5"));
            GLib.assert(this.choice_playback_tab5.win         != null);
            this.lbl_vol_tab5.win                  = this.panel_tab5.win.win_find(new Wx.Str("lbl_vol_tab5"));
            GLib.assert(this.lbl_vol_tab5.win                 != null);
            this.slider_vol_tab5.win               = this.panel_tab5.win.win_find(new Wx.Str("slider_vol_tab5"));
            GLib.assert(this.slider_vol_tab5.win              != null);
            this.btn_cancel.win                    = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win                   != null);
            this.btn_reset.win                     = this.win.win_find(new Wx.Str("btn_reset"));
            GLib.assert(this.btn_reset.win                    != null);
            this.btn_apply_settings.win            = this.win.win_find(new Wx.Str("btn_apply_settings"));
            GLib.assert(this.btn_apply_settings.win           != null);

            this.menu_reset.menu                   = xml.menu_load(new Wx.Str("menu_reset"));
            GLib.assert(this.menu_reset.menu                  != null);
            this.menu_reset_last.menu              = this.menu_reset.menu.item_find(xml.id_get(new Wx.Str("menu_reset_last")));
            GLib.assert(this.menu_reset_last.menu             != null);
            this.menu_reset_default.menu           = this.menu_reset.menu.item_find(xml.id_get(new Wx.Str("menu_reset_default")));
            GLib.assert(this.menu_reset_default               != null);

            // Setup widgets
            this.lbl1_tab2.lbl_markup_set(_("<b>Language</b>"));
            this.lbl2_tab2.lbl_markup_set(_("<b>Avatar Sizes</b>"));
            this.lbl1_tab3.lbl_markup_set(_("<b>Bootstrap Nodes</b>"));
            this.lbl2_tab3.lbl_markup_set(_("<b>Connection Settings</b>"));
            this.lbl3_tab3.lbl_markup_set(_("<b>Proxy</b>"));
            this.lbl1_tab5.lbl_markup_set(_("<b>General</b>"));
            this.lbl2_tab5.lbl_markup_set(_("<b>Input Device</b>"));
            this.lbl3_tab5.lbl_markup_set(_("<b>Output Device</b>"));

#if __WXGTK__
            this.choice_sys_tab5.append(_("Jack"));
            this.choice_sys_tab5.append(_("PipeWire"));
            this.choice_sys_tab5.append(_("PulseAudio"));
            this.choice_sys_tab5.append(_("OSS"));
#elif __WXMSW__
#endif
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.choice_node_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.choice_node_tab3_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.txt_url_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.txt_url_tab3_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.file_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.file_tab3_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_file_picker_changed(), closure);

            id      = this.txt_buddy_host_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.txt_buddy_host_tab3_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.btn_add_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_add_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_remove_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_remove_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_up_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_up_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_down_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_down_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.check_lst_node_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.check_lst_node_tab3_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_lst_box_sel(), closure);

            id      = this.choice_proxy_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.choice_proxy_tab3_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.txt_proxy_host_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.txt_proxy_host_tab3_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.choice_sys_tab5.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.choice_sys_tab5_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab5.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.choice_cap_tab5.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.choice_cap_tab5_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab5.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.choice_playback_tab5.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.choice_playback_tab5_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab5.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.btn_reset.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_reset_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_apply_settings.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.btn_apply_settings_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.menu_reset_last.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.menu_reset_last_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_reset.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_reset_default.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_settings.menu_reset_def_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_reset.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "About Profile" / "About Buddy" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgSettings(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

