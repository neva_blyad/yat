/*
 *    dlg_edit_profile.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Edit Profile" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgEditProfile : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Name label
         */
        public Wrapper.StaticTxt lbl_name { public get; public set; }

        /**
         * Name input text control
         */
        public Wrapper.TxtCtrl txt_name { public get; public set; }

        /**
         * Avatar label
         */
        public Wrapper.StaticTxt lbl_avatar { public get; public set; }

        /**
         * Avatar
         */
        public Wrapper.StaticBMP img_avatar { public get; public set; }

        /**
         * "Browse..." button
         */
        public Wrapper.Btn btn_browse { public get; public set; }

        /**
         * "Delete" button
         */
        public Wrapper.Btn btn_del { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * "Edit" button
         */
        public Wrapper.Btn btn_edit_profile { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.lbl_name         = new Wrapper.StaticTxt();
            this.txt_name         = new Wrapper.TxtCtrl();
            this.lbl_avatar       = new Wrapper.StaticTxt();
            this.img_avatar       = new Wrapper.StaticBMP();
            this.btn_browse       = new Wrapper.Btn();
            this.btn_del          = new Wrapper.Btn();
            this.btn_cancel       = new Wrapper.Btn();
            this.btn_edit_profile = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                  = xml.dlg_load(null, new Wx.Str("dlg_edit_profile"));
            GLib.assert(this.win                  != null);
            this.lbl_name.win         = this.win.win_find(new Wx.Str("lbl_name"));
            GLib.assert(this.lbl_name.win         != null);
            this.txt_name.win         = this.win.win_find(new Wx.Str("txt_name"));
            GLib.assert(this.txt_name.win         != null);
            this.lbl_avatar.win       = this.win.win_find(new Wx.Str("lbl_avatar"));
            GLib.assert(this.lbl_avatar.win       != null);
            this.img_avatar.win       = this.win.win_find(new Wx.Str("img_avatar"));
            GLib.assert(this.img_avatar.win       != null);
            this.btn_browse.win       = this.win.win_find(new Wx.Str("btn_browse"));
            GLib.assert(this.btn_browse.win       != null);
            this.btn_del.win          = this.win.win_find(new Wx.Str("btn_del"));
            GLib.assert(this.btn_del.win          != null);
            this.btn_cancel.win       = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win       != null);
            this.btn_edit_profile.win = this.win.win_find(new Wx.Str("btn_edit_profile"));
            GLib.assert(this.btn_edit_profile.win != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.txt_name.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_profile.txt_name_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.btn_browse.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_profile.btn_browse_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_del.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_profile.btn_del_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_edit_profile.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_edit_profile.btn_edit_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Edit Profile" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgEditProfile(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

