/*
 *    frm_conv.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Conversation History" frame class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class FrmConv : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Panel
         */
        public Wrapper.Panel panel { public get; public set; }

        /**
         * Tree control with conversation date & times.
         *
         * It is placed on the panel.
         */
        public Wrapper.TreeCtrl tree_datetime { public get; public set; }

        /**
         * Text control with exchanged messages.
         *
         * It is placed on the panel.
         */
        public Wrapper.TxtCtrl txt_conv { public get; public set; }

        /**
         * Conversation search input text control.
         *
         * It is placed on the panel.
         */
        public Wrapper.SrchCtrl srch_conv { public get; public set; }

        /**
         * Find previous button.
         *
         * It is placed on the panel.
         */
        public Wrapper.Btn btn_find_prev { public get; public set; }

        /**
         * Find next button.
         *
         * It is placed on the panel.
         */
        public Wrapper.Btn btn_find_next { public get; public set; }

        /**
         * "Delete" button.
         *
         * It is placed on the panel.
         */
        public Wrapper.Btn btn_del { public get; public set; }

        /**
         * "Delete All" button.
         *
         * It is placed on the panel.
         */
        public Wrapper.Btn btn_del_all { public get; public set; }

        /**
         * "Close" button.
         *
         * It is placed on the panel.
         */
        public Wrapper.Btn btn_close_conv { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Has the search input text control any text or not
        // Its default caption colour if not.
        private bool srch_conv_def;
        Wx.Colour    srch_conv_def_colour;

        // Colour constants from wxFormBuilder.
        //
        // I hope those colours have some system-wide meaning.
        // (At least, for M$ Windows.)
        private const Wx.Uint8 gray_txt_red   = 0x9A; // GreyText
        private const Wx.Uint8 gray_txt_green = 0x9A; // GreyText
        private const Wx.Uint8 gray_txt_blue  = 0x9A; // GreyText

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        // Setups the widgets.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.panel          = new Wrapper.Panel();
            this.tree_datetime  = new Wrapper.TreeCtrl();
            this.txt_conv       = new Wrapper.TxtCtrl();
            this.srch_conv      = new Wrapper.SrchCtrl();
            this.btn_find_prev  = new Wrapper.Btn();
            this.btn_find_next  = new Wrapper.Btn();
            this.btn_del        = new Wrapper.Btn();
            this.btn_del_all    = new Wrapper.Btn();
            this.btn_close_conv = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                = (Wx.Win) xml.frm_load(null, new Wx.Str("frm_conv"));
            GLib.assert(this.win                != null);
            this.panel.win          = this.win.win_find(new Wx.Str("panel"));
            GLib.assert(this.panel.win          != null);
            this.tree_datetime.win  = this.panel.win.win_find(new Wx.Str("tree_datetime"));
            GLib.assert(this.tree_datetime      != null);
            this.txt_conv.win       = this.panel.win.win_find(new Wx.Str("txt_conv"));
            GLib.assert(this.txt_conv.win       != null);
            this.srch_conv.win      = this.panel.win.win_find(new Wx.Str("srch_conv"));
            GLib.assert(this.srch_conv.win      != null);
            this.btn_find_prev.win  = this.panel.win.win_find(new Wx.Str("btn_find_prev"));
            GLib.assert(this.btn_find_prev.win  != null);
            this.btn_find_next.win  = this.panel.win.win_find(new Wx.Str("btn_find_next"));
            GLib.assert(this.btn_find_next.win  != null);
            this.btn_del.win        = this.panel.win.win_find(new Wx.Str("btn_del"));
            GLib.assert(this.btn_del.win        != null);
            this.btn_del_all.win    = this.panel.win.win_find(new Wx.Str("btn_del_all"));
            GLib.assert(this.btn_del_all.win    != null);
            this.btn_close_conv.win = this.panel.win.win_find(new Wx.Str("btn_close_conv"));
            GLib.assert(this.btn_close_conv.win != null);

            // Setup widgets
            this.srch_conv.btn_cancel_show(true);
            this.srch_conv.btn_srch_show(true);

            this.srch_conv_def = true; // Search input text control has no text
            this.srch_conv_def_colour = new Wx.Colour.rgb_create(gray_txt_red, gray_txt_green, gray_txt_blue, 0xFF);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_close_conv.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.focus_set_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, 0, 2147483647, Wx.EventHandler.focus_child_set(), closure);

            //id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.closed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.win_close(), closure);

            id      = this.tree_datetime.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.tree_datetime_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_tree_sel_changed(), closure);

            id      = this.srch_conv.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.srch_conv_btn_cancel_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.srch_ctrl_btn_cancel(), closure);

            //id      = this.srch_conv.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.srch_conv_btn_srch_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.srch_ctrl_btn_srch(), closure);

            //id      = this.srch_conv.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.srch_conv_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            //id      = this.srch_conv.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.srch_conv_btn_srch_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_txt_enter(), closure);

            id      = this.btn_find_prev.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.btn_find_prev_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_find_next.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.btn_find_next_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_del.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.btn_del_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_del_all.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.btn_del_all_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_close_conv.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_conv.btn_close_conv_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Conversation History" frame constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public FrmConv(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to input text control with service
         * message
         *
         * @param msg Message
         */
        public void txt_conv_append(string msg, GLib.DateTime datetime)
        {
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NORMAL, Wrapper.Ctrl.colour_t.CYAN);
            this.txt_conv.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + msg + "\n");
#if __WXMSW__
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to input text control with last exchanged
         * messages in 1v1 buddy chat
         *
         * @param name     Message author
         * @param am_i     false: message is issued from peer side,
         *                 true:  message is issued from our side
         * @param msg      Message
         * @param datetime Date and time
         */
        public void txt_conv_buddies_append(string name,
                                            bool   am_i,
                                            string msg,
                                            GLib.DateTime datetime)
        {
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NORMAL, am_i ? Wrapper.Ctrl.colour_t.BLUE : Wrapper.Ctrl.colour_t.RED);
            this.txt_conv.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + name + ":");
#if __WXMSW__
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
            this.txt_conv.append(" " + msg + "\n");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to input text control with last exchanged
         * messages in conference
         *
         * @param name     Message author
         * @param fg       Author foreground colour
         * @param bg       Author background colour.
         *                 Not used by now.
         * @param msg      Message
         * @param datetime Date and time
         */
        public void txt_conv_conferences_append(string name,
                                                uint   fg,
                                                uint   bg,
                                                string msg,
                                                GLib.DateTime datetime)
        {
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NORMAL, view->db_colour_convert(fg));
            this.txt_conv.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + name + ":");
#if __WXMSW__
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
            this.txt_conv.append(" " + msg + "\n");
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Set the conversation search input text control default value
         */
        public void srch_conv_val_def_set()
        {
            this.srch_conv.win.fg_colour_set(this.srch_conv_def_colour);
            this.srch_conv.val_change(_("Search..."));
            this.srch_conv_def = true;
        }

        /*----------------------------------------------------------------------------*/

        /*
         * Set our own helper tip string to search widget when field is
         * empty
         */
        public void srch_conv_val_def_upd()
        {
            // Dirty quirk, set our own helper tip string when field is
            // empty, not the widget default non-translated "Search"
            // caption.
            //
            // 1. Clear the our own helper tip string in search control if
            //    focused.
            // 2. Set it again on the control leaving.
            //
            // wxEVT_SET_FOCUS and wxEVT_KILL_FOCUS events don't work
            // with wxSearchCtrl, so I have to use wxEVT_CHILD_FOCUS.
            // (Possibly fixed now wxWidgets bug.)
            if (view->frm_conv.srch_conv.focus_has())
            {
                // The control has catched focus
                if (this.srch_conv_def)
                {
                    this.srch_conv.clr();
#if __WXMSW__
                    this.srch_conv.fg_colour_set(Wrapper.Ctrl.colour_t.BLACK);
#else
                    this.srch_conv.fg_colour_set(Wrapper.Ctrl.colour_t.NONE);
#endif
                    this.srch_conv_def = false;

                    // Another dirty hack. This fixes bug that the widget shows its
                    // value and length are non-zero at this place (due to
                    // fg_colour_set() calling).
                    //
                    // So we've cleared it twice.
                    this.srch_conv.clr();
                }
            }
            else
            {
                // The control has lost focus
                if (this.srch_conv_def)
                {
                }
                else
                {
                    ulong len = srch_conv.len_get();
                    if (len == 0)
                        srch_conv_val_def_set();
                }
            }
        }
    }
}

