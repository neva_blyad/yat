/*
 *    dlg_Vika.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "In Loving Memory of" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgVika : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Vika's avatar
         */
        public Wrapper.StaticBMP img_avatar { public get; public set; }

        /**
         * Vika's label
         */
        public Wrapper.StaticTxt lbl_Vika { public get; public set; }

        /**
         * "Close" button
         */
        public Wrapper.Btn btn_close_Vika { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.img_avatar     = new Wrapper.StaticBMP();
            this.lbl_Vika       = new Wrapper.StaticTxt();
            this.btn_close_Vika = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_Vika"));
            GLib.assert(this.win                != null);
            this.img_avatar.win     = this.win.win_find(new Wx.Str("img_avatar"));
            GLib.assert(this.img_avatar         != null);
            this.lbl_Vika.win       = this.win.win_find(new Wx.Str("lbl_Vika"));
            GLib.assert(this.lbl_Vika.win       != null);
            this.btn_close_Vika.win = this.win.win_find(new Wx.Str("btn_close_Vika"));
            GLib.assert(this.btn_close_Vika.win != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_close_Vika.win.id_set(Wx.Win.id_t.CANCEL);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "In Loving Memory of" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgVika(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

