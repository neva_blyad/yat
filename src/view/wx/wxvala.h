/*
 *    wxvala.h
 *    Copyright (C) 2021-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <time.h>

#define wxACCEL_NORMAL 0        // No modifiers
#define wxACCEL_ALT    (1 << 0) // Hold Alt key down
#define wxACCEL_CTRL   (1 << 1) // Hold Ctrl key down
#define wxACCEL_SHIFT  (1 << 2) // Hold Shift key down

#define wxBITMAP_TYPE_BMP   1
#define wxBITMAP_TYPE_ICO   3
#define wxBITMAP_TYPE_GIF  13
#define wxBITMAP_TYPE_PNG  15
#define wxBITMAP_TYPE_JPEG 17

#define wxIMAGE_LIST_NORMAL 0
#define wxIMAGE_LIST_SMALL  1
#define wxIMAGE_LIST_STATE  2

#define wxFD_OPEN             (1 << 0)
#define wxFD_SAVE             (1 << 1)
#define wxFD_OVERWRITE_PROMPT (1 << 2)
#define wxFD_FILE_MUST_EXIST  (1 << 4)
#define wxFD_MULTIPLE         (1 << 5)
#define wxFD_CHANGE_DIR       (1 << 7)
#define wxFD_PREVIEW          (1 << 8)

#define wxFONTWEIGHT_NORMAL 90
#define wxFONTWEIGHT_LIGHT  91
#define wxFONTWEIGHT_BOLD   92

extern void *wxString_CreateUTF8(char *);
extern void *wxString_GetUtf8(void *);
extern void *wxCharBuffer_DataUtf8(void *);
extern void wxCharBuffer_Delete(void *);

