/*
 *    wxvala.vapi
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

[CCode (cheader_filename = "wxvala.h,wx/defs.h,wx/language.h,wxc.h")]
namespace Wx
{
    namespace Version
    {
        [CCode (cname = "wxMAJOR_VERSION")]
        public const uint MAJOR;

        [CCode (cname = "wxMINOR_VERSION")]
        public const uint MINOR;

        [CCode (cname = "wxRELEASE_NUMBER")]
        public const uint RELEASE_NUM;

        [CCode (cname = "wxSUBRELEASE_NUMBER")]
        public const uint SUBRELEASE_NUM;

        [CCode (cname = "wxVersionNumber", has_target = false)]
        public int num_get();

        [CCode (cname = "wxVersionString", has_target = false)]
        public Str str_get();
    }

    namespace App
    {
        [CCode (cname = "ELJApp_InitAllImageHandlers", has_target = false)]
        public void all_img_handlers_init();

        [CCode (cname = "ELJApp_InitializeC", has_target = false)]
        public void c_init(Closure closure, int _argc, [CCode (array_length = false)] string[] _args);

        [CCode (cname = "ELJApp_Dispatch", has_target = false)]
        public void dispatch();

        [CCode (cname = "ELJApp_Exit", has_target = false)]
        public void exit();

        [CCode (cname = "ELJApp_ExitMainLoop", has_target = false)]
        public void main_loop_exit();

        [CCode (cname = "ELJApp_Pending", has_target = false)]
        public int pending();

        [CCode (cname = "ELJApp_SetExitOnFrameDelete", has_target = false)]
        public void exit_on_frm_del(bool flag);

        [CCode (cname = "ELJApp_MousePosition", has_target = false)]
        public Point mouse_pos_get();
    }

    [CCode (cname = "wxDefaultCoord")]
    public const int DEF_COORD;

    [CCode (cname = "wxID_ANY")]
    public const int ID_ANY;

    [CCode (cname = "wxNOT_FOUND")]
    public const int FOUND_NOT;

    [CCode (cname = "wxBitmapType", has_type_id = false)]
    public enum bmp_type_t
    {
        [CCode (cname = "wxBITMAP_TYPE_BMP", has_type_id = false)]
        BMP,
        [CCode (cname = "wxBITMAP_TYPE_GIF", has_type_id = false)]
        GIF,
        [CCode (cname = "wxBITMAP_TYPE_ICO", has_type_id = false)]
        ICO,
        [CCode (cname = "wxBITMAP_TYPE_PNG", has_type_id = false)]
        PNG,
        [CCode (cname = "wxBITMAP_TYPE_JPEG", has_type_id = false)]
        JPEG,
        [CCode (cname = "wxBITMAP_TYPE_ANY", has_type_id = false)]
        ANY,
    }

    [Flags]
    public enum border_t
    {
        [CCode (cname = "wxDEFAULT", has_type_id = false)]
        DEF = 0,
        [CCode (cname = "wxNONE", has_type_id = false)]
        NONE = 0x00200000UL,
        [CCode (cname = "wxSTATIC", has_type_id = false)]
        STATIC = 0x01000000UL,
        [CCode (cname = "wxSIMPLE", has_type_id = false)]
        SIMPLE = 0x02000000UL,
        [CCode (cname = "wxRAISED", has_type_id = false)]
        RAISED_STYLE = 0x04000000UL,
        [CCode (cname = "wxSUNKEN", has_type_id = false)]
        SUNKEN = 0x08000000UL,
        [CCode (cname = "wxDOUBLE", has_type_id = false)]
        DOUBLE = 0x10000000UL,
        [CCode (cname = "wxTHEME", has_type_id = false)]
        THEME = DOUBLE,
        [CCode (cname = "wxMASK", has_type_id = false)]
        MASK = 0x1F200000UL,
    }

    [CCode (cname = "wxGetTextFromUser", has_target = false)]
    public int txt_from_user_get([CCode (array_length = false)] Ch[]? msg,
                                 [CCode (array_length = false)] Ch[]? caption,
                                 [CCode (array_length = false)] Ch[]? def_txt,
                                 Win? parent,
                                 int x,
                                 int y,
                                 int centr,
                                 [CCode (array_length = false)] Ch[]? buf);

    [CCode (cname = "wxGetPasswordFromUser", has_target = false)]
    public int pwd_from_user_get([CCode (array_length = false)] Ch[]? msg,
                                 [CCode (array_length = false)] Ch[]? caption,
                                 [CCode (array_length = false)] Ch[]? def_txt,
                                 Win? parent,
                                 [CCode (array_length = false)] Ch[]? buf);

    [CCode (cname = "wxcWakeUpIdle", has_target = false)]
    public void idle_wake_up();

    [CCode (cname = "void", free_function = "wxAboutDialogInfo_Delete", has_type_id = false)]
    [Compact]
    public class AboutDlgInfo
    {
        [CCode (cname = "wxAboutBox", has_target = false)]
        public static void box_show(AboutDlgInfo info, Win parent);

        [CCode (cname = "wxAboutDialogInfo_Create", has_target = false)]
        public AboutDlgInfo();

        [CCode (cname = "wxAboutDialogInfo_AddDeveloper", has_target = false)]
        public void dev_add(Str dev);

        [CCode (cname = "wxAboutDialogInfo_SetArtists", has_target = false)]
        public void artists_set(ArrStr str);

        [CCode (cname = "wxAboutDialogInfo_SetCopyright", has_target = false)]
        public void copyright_set(Str copyright);

        [CCode (cname = "wxAboutDialogInfo_SetDescription", has_target = false)]
        public void desc_set(Str desc);

        [CCode (cname = "wxAboutDialogInfo_SetIcon", has_target = false)]
        public void icon_set(Icon icon);

        [CCode (cname = "wxAboutDialogInfo_SetLicense", has_target = false)]
        public void license_set(Str license);

        [CCode (cname = "wxAboutDialogInfo_SetName", has_target = false)]
        public void name_set(Str name);

        [CCode (cname = "wxAboutDialogInfo_SetTranslators", has_target = false)]
        public void translators_set(ArrStr str);

        [CCode (cname = "wxAboutDialogInfo_SetVersion", has_target = false)]
        public void ver_set(Str ver);

        [CCode (cname = "wxAboutDialogInfo_SetWebSite", has_target = false)]
        public void web_site_set(Str url);
    }

    [CCode (cname = "void", free_function = "wxAcceleratorEntry_Delete", has_type_id = false)]
    [Compact]
    public class AclEntry
    {
        [Flags]
        public enum flag_t
        {
            [CCode (cname = "wxACCEL_NORMAL", has_type_id = false)]
            NORMAL,

            [CCode (cname = "wxACCEL_ALT", has_type_id = false)]
            ALT,

            [CCode (cname = "wxACCEL_CTRL", has_type_id = false)]
            CTRL,

            [CCode (cname = "wxACCEL_SHIFT", has_type_id = false)]
            SHIFT,
        }

        public enum key_code_t
        {
            [CCode (cname = "WXK_CONTROL_M", has_type_id = false)]
            CTRL_M,

            [CCode (cname = "WXK_DELETE", has_type_id = false)]
            DEL_CODE,

            [CCode (cname = "WXK_RETURN", has_type_id = false)]
            RETURN,

            [CCode (cname = "WXK_NUMPAD_ENTER", has_type_id = false)]
            NUMPAD_ENTER,
        }

        [CCode (cname = "wxAcceleratorEntry_Create", has_target = false)]
        public AclEntry(flag_t flags, key_code_t key_code, int cmd);

        [CCode (cname = "wxAcceleratorEntry_Set", has_target = false)]
        public void @set(flag_t flags, key_code_t key_code, int cmd);

        [CCode (cname = "wxAcceleratorEntry_GetFlags", has_target = false)]
        public int flags_get();

        [CCode (cname = "wxAcceleratorEntry_GetKeyCode", has_target = false)]
        public int key_code_get();

        [CCode (cname = "wxAcceleratorEntry_GetCommand", has_target = false)]
        public int cmd_get();
    }

    [CCode (cname = "void", free_function = "wxAcceleratorTable_Delete", has_type_id = false)]
    [Compact]
    public class AclTbl
    {
        [CCode (cname = "wxAcceleratorTable_Create", has_target = false)]
        public AclTbl(int num, void *entries);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class AnyBtn : Ctrl
    {
        [CCode (cname = "wxBitmapButton_SetBitmapFocus", has_target = false)]
        public void bmp_focus_set(BMP sel);

        [CCode (cname = "wxBitmapButton_SetBitmapDisabled", has_target = false)]
        public void bmp_dis_set(BMP sel);

        [CCode (cname = "wxBitmapButton_SetBitmapLabel", has_target = false)]
        public void bmp_lbl_set(BMP sel);

        [CCode (cname = "wxBitmapButton_SetBitmapSelected", has_target = false)]
        public void bmp_sel_set(BMP sel);
    }

    [CCode (cname = "void", free_function = "wxArrayString_Delete", has_type_id = false)]
    [Compact]
    public class ArrStr
    {
        [CCode (cname = "wxArrayString_Create", has_target = false)]
        public ArrStr();

        [CCode (cname = "wxArrayString_Add", has_target = false)]
        public void add(Str str);
    }

    [CCode (cname = "void", free_function = "wxBitmap_Delete", has_type_id = false)]
    [Compact]
    public class BMP
    {
        [CCode (cname = "wxBitmap_CreateLoad", has_target = false)]
        public BMP(Str name, bmp_type_t type);

        [CCode (cname = "Null_Bitmap", has_target = false)]
        public static unowned BMP? null_get();
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class BMPBtn : Btn
    {
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Btn : AnyBtn
    {
        [CCode (cname = "wxButton_Create", has_target = false)]
        public Btn(Win parent, int _id, Str _txt, int _left, int _top, int _width, int _height, Win.style_t _style);
    }

    [CCode (cname = "void", free_function = "NULL", has_type_id = false)]
    [Compact]
    public class Ch
    {
    }

    [CCode (cname = "void", free_function = "wxCharBuffer_Delete", has_type_id = false)]
    [Compact]
    public class ChBuf
    {
        [CCode (cname = "wxCharBuffer_DataUtf8", has_target = false)]
        public unowned string data_utf8_get();
    }

    [CCode (cname = "void", free_function = "wxCheckBox_Delete", has_type_id = false)]
    [Compact]
    public class CheckBox : Ctrl
    {
        [CCode (cname = "wxCheckBox_GetValue", has_target = false)]
        public bool val_get();

        [CCode (cname = "wxCheckBox_SetValue", has_target = false)]
        public void val_set(bool val);
    }

    [CCode (cname = "void", free_function = "wxCheckListBox_Delete", has_type_id = false)]
    [Compact]
    public class CheckLstBox : LstBox
    {
        [CCode (cname = "wxCheckListBox_IsChecked", has_target = false)]
        public bool is_checked(int item);

        [CCode (cname = "wxCheckListBox_Check", has_target = false)]
        public void check(int item, bool check);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Choice : Ctrl
    {
        [CCode (cname = "wxChoice_Append", has_target = false)]
        public void append(Str item);

        [CCode (cname = "wxChoice_Clear", has_target = false)]
        public void clr();

        [CCode (cname = "wxChoice_Delete", has_target = false)]
        public void del(int num);

        [CCode (cname = "wxChoice_GetCount", has_target = false)]
        public int cnt_get();

        [CCode (cname = "wxChoice_GetSelection", has_target = false)]
        public int sel_get();

        [CCode (cname = "wxChoice_GetString", has_target = false)]
        public Str str_get(int num);

        [CCode (cname = "wxChoice_SetSelection", has_target = false)]
        public int sel_set(int num);

        [CCode (cname = "wxChoice_SetString", has_target = false)]
        public void str_set(int num, Wx.Str str);
    }

    [CCode (cname = "TClosureFun", free_function = "", has_type_id = false)]
    [Compact]
    public class Closure
    {
        [CCode (cname = "TClosureFun", has_target = false)]
        public delegate void *fn_t(void *fn, void *param, void *event);

        [CCode (cname = "wxClosure_Create", has_target = false)]
        public Closure(fn_t fn, void *data);

        [CCode (cname = "wxClosure_GetData", has_target = false)]
        public void *data_get();
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class Colour
    {
        /*
         * Get colour by its RGB
         */
        [CCode (cname = "wxColour_CreateRGB", has_target = false)]
        public Colour.rgb_create(Uint8 _red, Uint8 _green, Uint8 _blue, Uint8 _alpha);

        [CCode (cname = "Null_Colour", has_target = false)]
        public static unowned Colour? null_get();

        /*
         * Get predefined colour
         *
         * @param id 0 = black,
         *           1 = white,
         *           2 = red,
         *           3 = blue,
         *           4 = green,
         *           5 = cyan,
         *           6 = light grey
         */
        [CCode (cname = "wxColour_CreateFromStock", has_target = false)]
        public static unowned Colour? stock_get(int _id);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Ctrl : Win
    {
        [CCode (cname = "wxControl_SetLabel", has_target = false)]
        public void lbl_set(Str txt);

        [CCode (cname = "wxControl_SetLabelMarkup", has_target = false)]
        public void lbl_markup_set(Str txt);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Dlg : TopLvlWin
    {
        public enum answer_t
        {
            [CCode (cname = "wxID_YES", has_type_id = false)]
            YES,
            [CCode (cname = "wxID_OK", has_type_id = false)]
            OK,
            [CCode (cname = "wxID_NO", has_type_id = false)]
            NO,
            [CCode (cname = "wxID_CANCEL", has_type_id = false)]
            CANCEL,
        }

        [CCode (cname = "wxDialog_EndModal", has_target = false)]
        public void modal_close(int code);

        [CCode (cname = "wxDialog_ShowModal", has_target = false)]
        public void modal_show();
    }

    [CCode (cname = "void", free_function = "wxCommandEvent_Delete", has_type_id = false)]
    [Compact]
    public class Event
    {
        [CCode (cname = "wxCommandEvent_Create", has_target = false)]
        public Event(int _type, int _id);

        [CCode (cname = "wxEvent_NewEventType", has_target = false)]
        public static int new_event_type_create();

        [CCode (cname = "wxEvent_SetEventType", has_target = false)]
        public static int event_type_set(int type);

        [CCode (cname = "wxEvent_Skip", has_target = false)]
        public void skip();
    }

    [CCode (cname = "void", free_function = "wxEvtHandler_Delete", has_type_id = false)]
    [Compact]
    public class EventHandler
    {
        [CCode (cname = "wxEvtHandler_Create", has_target = false)]
        public EventHandler();

        [CCode (cname = "expEVT_CHILD_FOCUS", has_target = false)]
        public static int focus_child_set();

        [CCode (cname = "expEVT_CLOSE_WINDOW", has_target = false)]
        public static int win_close();

        [CCode (cname = "expEVT_COMMAND_BUTTON_CLICKED", has_target = false)]
        public static int cmd_btn_clicked();

        [CCode (cname = "expEVT_COMMAND_CHECKBOX_CLICKED", has_target = false)]
        public static int cmd_checkbox_clicked();

        [CCode (cname = "expEVT_COMMAND_CHECKLISTBOX_TOGGLED", has_target = false)]
        public static int cmd_check_lst_box_toggled();

        [CCode (cname = "expEVT_COMMAND_CHOICE_SELECTED", has_target = false)]
        public static int cmd_choice_sel();

        [CCode (cname = "expEVT_COMMAND_FILEPICKER_CHANGED", has_target = false)]
        public static int cmd_file_picker_changed();

        [CCode (cname = "expEVT_COMMAND_LIST_ITEM_DESELECTED", has_target = false)]
        public static int cmd_lst_item_desel();

        [CCode (cname = "expEVT_COMMAND_LIST_ITEM_RIGHT_CLICK", has_target = false)]
        public static int cmd_lst_item_right_click();

        [CCode (cname = "expEVT_COMMAND_LIST_ITEM_SELECTED", has_target = false)]
        public static int cmd_lst_item_sel();

        [CCode (cname = "expEVT_COMMAND_LISTBOX_SELECTED", has_target = false)]
        public static int cmd_lst_box_sel();

        [CCode (cname = "expEVT_COMMAND_MENU_SELECTED", has_target = false)]
        public static int cmd_menu_sel();

        [CCode (cname = "expEVT_COMMAND_NOTEBOOK_PAGE_CHANGED", has_target = false)]
        public static int cmd_notebook_page_changed();

        [CCode (cname = "expEVT_COMMAND_NOTEBOOK_PAGE_CHANGING", has_target = false)]
        public static int cmd_notebook_page_changing();

        [CCode (cname = "expEVT_COMMAND_SEARCHCTRL_CANCEL_BTN", has_target = false)]
        public static int srch_ctrl_btn_cancel();

        [CCode (cname = "expEVT_COMMAND_SEARCHCTRL_SEARCH_BTN", has_target = false)]
        public static int srch_ctrl_btn_srch();

        [CCode (cname = "expEVT_COMMAND_SPLITTER_SASH_POS_CHANGED", has_target = false)]
        public static int cmd_splitter_sash_pos_changed();

        [CCode (cname = "expEVT_COMMAND_SPLITTER_SASH_POS_CHANGING", has_target = false)]
        public static int cmd_splitter_sash_pos_changing();

        [CCode (cname = "expEVT_COMMAND_TEXT_ENTER", has_target = false)]
        public static int cmd_txt_enter();

        [CCode (cname = "expEVT_COMMAND_TEXT_UPDATED", has_target = false)]
        public static int cmd_txt_upd();

        [CCode (cname = "expEVT_COMMAND_TREE_SEL_CHANGED", has_target = false)]
        public static int cmd_tree_sel_changed();

        [CCode (cname = "expEVT_ENTER_WINDOW", has_target = false)]
        public static int win_enter();

        [CCode (cname = "expEVT_IDLE", has_target = false)]
        public static int idle_handle();

        [CCode (cname = "expEVT_INIT_DIALOG", has_target = false)]
        public static int dlg_init();

        [CCode (cname = "expEVT_KILL_FOCUS", has_target = false)]
        public static int focus_kill();

        [CCode (cname = "expEVT_LEFT_UP", has_target = false)]
        public static int left_clicked();

        [CCode (cname = "expEVT_MOVE", has_target = false)]
        public static int move();

        [CCode (cname = "expEVT_MOVE_START", has_target = false)]
        public static int move_start();

        [CCode (cname = "expEVT_MOVE_END", has_target = false)]
        public static int move_end();

        [CCode (cname = "expEVT_MOVING", has_target = false)]
        public static int moving();

        [CCode (cname = "expEVT_PAINT", has_target = false)]
        public static int paint();

        [CCode (cname = "expEVT_SET_FOCUS", has_target = false)]
        public static int focus_set();

        [CCode (cname = "expEVT_SIZE", has_target = false)]
        public static int size();

        [CCode (cname = "expEVT_SIZING", has_target = false)]
        public static int sizing();

        [CCode (cname = "expEVT_SHOW", has_target = false)]
        public static int show();

        [CCode (cname = "expEVT_WINDOW_MODAL_DIALOG_CLOSED", has_target = false)]
        public static int win_modal_dlg_closed();

        [CCode (cname = "wxEvtHandler_AddPendingEvent", has_target = false)]
        public void pending_event_add(Event event);

        [CCode (cname = "wxEvtHandler_Connect", has_target = false)]
        public static int connect(void *widget, int first, int last, int type, Closure closure);

        [CCode (cname = "wxEvtHandler_Delete", has_target = false)]
        public static void del(void *widget);

        [CCode (cname = "wxEvtHandler_Disconnect", has_target = false)]
        public static void disconnect(void *widget, int first, int last, int type, int id);

        [CCode (cname = "wxEvtHandler_SetEvtHandlerEnabled", has_target = false)]
        public static void set_evt_handler_en(void *widget, bool en_flag);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class FileDlg : Dlg
    {
        [Flags]
        public enum style_t
        {
            [CCode (cname = "wxFD_OPEN", has_type_id = false)]
            OPEN_FILE,
            [CCode (cname = "wxFD_SAVE", has_type_id = false)]
            SAVE_FILE,
            [CCode (cname = "wxFD_OVERWRITE_PROMPT", has_type_id = false)]
            OVERWRITE_PROMPT,
            [CCode (cname = "wxFD_FILE_MUST_EXIST", has_type_id = false)]
            FILE_EXIST_MUST,
            [CCode (cname = "wxFD_MULTIPLE", has_type_id = false)]
            MULTIPLE,
            [CCode (cname = "wxFD_CHANGE_DIR", has_type_id = false)]
            DIR_CHANGE,
            [CCode (cname = "wxFD_PREVIEW", has_type_id = false)]
            PREVIEW,
        }

        [CCode (cname = "wxFileDialog_Create", has_target = false)]
        public FileDlg(Win? parent, Str _msg, Str _dir, Str _file, Str wildcard, int _left, int _top, int _stl);

        [CCode (cname = "wxFileDialog_GetPath", has_target = false)]
        public Str path_get();

        [CCode (cname = "wxFileDialog_ShowModal", has_target = false)]
        public int modal_show();
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class FilePicker : Ctrl
    {
        [CCode (cname = "wxFilePickerCtrl_GetPath", has_target = false)]
        public Str path_get();

        [CCode (cname = "wxFilePickerCtrl_SetPath", has_target = false)]
        public void path_set(Str filename);
    }

    [CCode (cname = "void", free_function = "wxFont_Delete", has_type_id = false)]
    [Compact]
    public class Font
    {
        public enum weight_t
        {
            [CCode (cname = "wxFONTWEIGHT_NORMAL", has_type_id = false)]
            NORMAL,
            [CCode (cname = "wxFONTWEIGHT_LIGHT", has_type_id = false)]
            LIGHT,
            [CCode (cname = "wxFONTWEIGHT_BOLD", has_type_id = false)]
            BOLD,
        }

        [CCode (cname = "wxFont_CreateDefault", has_target = false)]
        public Font.def_create();

        [CCode (cname = "wxFont_MakeBold", has_target = false)]
        public void bold_make();

        [CCode (cname = "Null_Font", has_target = false)]
        public static unowned Font? null_get();

        /*
         * Get predefined font
         *
         * @param id 0 = italic,
         *           1 = normal,
         *           2 = small,
         *           3 = swiss
         */
        [CCode (cname = "wxFont_CreateFromStock", has_target = false)]
        public static unowned Font? stock_get(int id);

        [CCode (cname = "wxFont_SetWeight", has_target = false)]
        public void weight_set(weight_t weight);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Frm : TopLvlWin
    {
        [CCode (cname = "wxFrame_GetMenuBar", has_target = false)]
        public unowned MenuBar? menu_bar_get();

        [CCode (cname = "wxFrame_SetIcon", has_target = false)]
        public void icon_set(Icon icon);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Gauge : Ctrl
    {
        [CCode (cname = "wxGauge_SetValue", has_target = false)]
        public bool val_set(int pos);
    }

    [CCode (cname = "void", free_function = "wxIcon_Delete", has_type_id = false)]
    [Compact]
    public class Icon
    {
        [CCode (cname = "wxIcon_CreateDefault", has_target = false)]
        public Icon();

        [CCode (cname = "wxIcon_CreateLoad", has_target = false)]
        public Icon.create_load(Str name, bmp_type_t type, int width, int height);

        [CCode (cname = "wxIcon_FromRaw", has_target = false)]
        public Icon.from_raw_init(void *data, int width, int height);

        [CCode (cname = "wxIcon_CopyFromBitmap", has_target = false)]
        public void bmp_from_copy(BMP bmp);

        [CCode (cname = "wxIcon_IsOk", has_target = false)]
        public bool ok_is();
    }

    [CCode (cname = "void", free_function = "wxIconBundle_Delete", has_type_id = false)]
    [Compact]
    public class IconBundle
    {
        [CCode (cname = "wxIconBundle_CreateFromFile", has_target = false)]
        public IconBundle(Str file, bmp_type_t type);
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class IdleEvent
    {
        [CCode (cname = "wxIdleEvent_MoreRequested", has_target = false)]
        public bool more_req_check();

        [CCode (cname = "wxIdleEvent_RequestMore", has_target = false)]
        public void more_req(bool need_more);
    }

    [CCode (cname = "void", free_function = "wxImage_Destroy", has_type_id = false)]
    [Compact]
    public class Img
    {
        [CCode (cname = "wxImage_CreateDefault", has_target = false)]
        public Img();

        [CCode (cname = "wxImage_CreateFromData", has_target = false)]
        public Img.from_data_create(int width, int height, void *data);

        [CCode (cname = "wxImage_ConvertToBitmap", has_target = false)]
        public void to_bmp_convert(out BMP bmp);

        [CCode (cname = "wxImage_LoadStream", has_target = false)]
        public bool stream_load(InputStream stream, bmp_type_t type, int idx);

        [CCode (cname = "wxImage_Rescale", has_target = false)]
        public void rescale(int width, int height);
    }

    [CCode (cname = "void", free_function = "wxImageList_Delete", has_type_id = false)]
    [Compact]
    public class ImgLst
    {
        [CCode (cname = "wxImageList_Create", has_target = false)]
        public ImgLst(int width, int height, bool mask, int initial_cnt);

        [CCode (cname = "wxImageList_AddBitmap", has_target = false)]
        public int bmp_add(BMP bmp, BMP mask);

        [CCode (cname = "wxImageList_AddIcon", has_target = false)]
        public int icon_add(Icon icon);

        [CCode (cname = "wxImageList_AddMasked", has_target = false)]
        public int masked_add(BMP bmp, Colour colour);

        [CCode (cname = "wxImageList_Replace", has_target = false)]
        public bool replace(int idx, BMP bmp, BMP mask);

        [CCode (cname = "wxImageList_GetImageCount", has_target = false)]
        public int img_cnt_get();

        [CCode (cname = "wxImageList_ReplaceIcon", has_target = false)]
        public bool icon_replace(int idx, Icon icon);

        [CCode (cname = "wxImageList_Remove", has_target = false)]
        public bool remove(int idx);

        [CCode (cname = "wxImageList_RemoveAll", has_target = false)]
        public bool remove_all();
    }

    [CCode (cname = "void", free_function = "wxInputStream_Delete", has_type_id = false)]
    [Compact]
    public class InputStream
    {
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class KeyEvt
    {
    }

    [CCode (cname = "void", free_function = "wxLocale_Delete", has_type_id = false)]
    [Compact]
    public class Locale
    {
        [CCode (cname = "int")]
        public enum lang_t
        {
            [CCode (cname = "wxLANGUAGE_DEFAULT", has_type_id = false)]
            DEF,
            [CCode (cname = "wxLANGUAGE_ENGLISH", has_type_id = false)]
            EN,
            [CCode (cname = "wxLANGUAGE_ENGLISH_UK", has_type_id = false)]
            EN_UK,
            [CCode (cname = "wxLANGUAGE_ENGLISH_US", has_type_id = false)]
            EN_US,
            [CCode (cname = "wxLANGUAGE_RUSSIAN", has_type_id = false)]
            RU,
        }

        [CCode (cname = "wxLocale_Create", has_target = false)]
        public Locale(lang_t _name, int _flags);

        [CCode (cname = "wxLocale_AddCatalogLookupPathPrefix", has_target = false)]
        public void catalog_lookup_path_prefix_add([CCode (array_length = false)] Ch[] prefix);

        [CCode (cname = "wxLocale_AddCatalog", has_target = false)]
        public bool catalog_add([CCode (array_length = false)] Ch[] domain);

        [CCode (cname = "wxLocale_IsOk", has_target = false)]
        public bool ok_is();

        [CCode (cname = "wxLocale_GetName", has_target = false)]
        public Str name_get();
    }

    [CCode (cname = "void", free_function = "wxMemoryInputStream_Delete", has_type_id = false)]
    [Compact]
    public class MemInputStream
    {
        [CCode (cname = "wxMemoryInputStream_Create", has_target = false)]
        public MemInputStream([CCode (array_length_type="int")] uint8[] data);
    }

    [CCode (cname = "wxGetELJLocale", has_target = false)]
    public unowned Locale locale_get();

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class LstBox : Ctrl
    {
        [CCode (cname = "wxListBox_Append", has_target = false)]
        public void append(Str item);

        [CCode (cname = "wxListBox_Clear", has_target = false)]
        public void clr();

        [CCode (cname = "wxListBox_Delete", has_target = false)]
        public void del(int num);

        [CCode (cname = "wxListBox_GetClientData", has_target = false)]
        public void *client_data_get(int n);

        [CCode (cname = "wxListBox_GetCount", has_target = false)]
        public int cnt_get();

        [CCode (cname = "wxListBox_GetString", has_target = false)]
        public Str str_get(int num);

        [CCode (cname = "wxListBox_Insert", has_target = false)]
        public void insert(Str item, int pos);

        [CCode (cname = "wxListBox_InsertItems", has_target = false)]
        public void items_insert(void *items, int pos, int cnt);

        [CCode (cname = "wxListBox_IsSelected", has_target = false)]
        public bool sel_is(int num);

        [CCode (cname = "wxListBox_SetString", has_target = false)]
        public void str_set(int num, Str str);

        [CCode (cname = "wxListBox_GetSelection", has_target = false)]
        public int sel_get();

        [CCode (cname = "wxListBox_GetSelections", has_target = false)]
        public int selections_get([CCode (array_length_type="int")] int[] alloc_selections);

        [CCode (cname = "wxListBox_SetClientData", has_target = false)]
        public void client_data_set(int n, void *client_data);

        [CCode (cname = "wxListBox_SetSelection", has_target = false)]
        public void sel_set(int n, bool sel);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class LstCtrl : Ctrl
    {
        public enum img_lst
        {
            [CCode (cname = "wxIMAGE_LIST_NORMAL", has_type_id = false)]
            NORMAL,
            [CCode (cname = "wxIMAGE_LIST_SMALL", has_type_id = false)]
            SMALL,
            [CCode (cname = "wxIMAGE_LIST_STATE", has_type_id = false)]
            STATE
        }

        [CCode (cname = "wxListCtrl_DeleteItem", has_target = false)]
        public bool item_del(int item);

        [CCode (cname = "wxListCtrl_GetColumnCount", has_target = false)]
        public int col_cnt_get();

        [CCode (cname = "wxListCtrl_DeleteAllItems", has_target = false)]
        public bool all_items_del();

        [CCode (cname = "wxListCtrl_DeleteColumn", has_target = false)]
        public bool col_del(int col);

        [CCode (cname = "wxListCtrl_GetColumnWidth", has_target = false)]
        public int col_width_get(int col);

        [CCode (cname = "wxListCtrl_GetItemCount", has_target = false)]
        public int item_cnt_get();

        [CCode (cname = "wxListCtrl_GetItem", has_target = false)]
        public int item_get(LstItem info);

        [CCode (cname = "wxListCtrl_GetItemState", has_target = false)]
        public int item_state_get(int item, int state_mask);

        [CCode (cname = "wxListCtrl_GetItemText", has_target = false)]
        public Str item_txt_get(int item);

        [CCode (cname = "wxListCtrl_GetSelectedItemCount", has_target = false)]
        public int sel_item_cnt_get();

        [CCode (cname = "wxListCtrl_InsertColumn", has_target = false)]
        public int col_insert(int col, Str heading, int fmt, int width);

        [CCode (cname = "wxListCtrl_InsertItemWithData", has_target = false)]
        public int item_with_data_insert(int idx, Str lbl);

        [CCode (cname = "wxListCtrl_SetColumnWidth", has_target = false)]
        public bool col_width_set(int col, int width);

        [CCode (cname = "wxListCtrl_SetItemFromInfo", has_target = false)]
        public bool item_from_info_set(LstItem info);

        [CCode (cname = "wxListCtrl_SetImageList", has_target = false)]
        public bool img_lst_set(ImgLst img_lst, int which);

        [CCode (cname = "wxListCtrl_SetItem", has_target = false)]
        public bool item_set(int idx, int col, Str lbl, int img_id);

        [CCode (cname = "wxListCtrl_SetItemState", has_target = false)]
        public bool item_state_set(int item, int state, int state_mask);
    }

    [CCode (cname = "void", free_function = "wxListItem_Delete", has_type_id = false)]
    [Compact]
    public class LstItem
    {
        [CCode (cname = "wxListItem_Create", has_target = false)]
        public LstItem();

        [CCode (cname = "wxListItem_GetData", has_target = false)]
        public int data_get();

        [CCode (cname = "wxListItem_GetImage", has_target = false)]
        public int img_get();

        [CCode (cname = "wxListItem_SetColumn", has_target = false)]
        public void col_set(int col);

        [CCode (cname = "wxListItem_SetData", has_target = false)]
        public void data_set(int data);

        [CCode (cname = "wxListItem_SetFont", has_target = false)]
        public void font_set(Font font);

        [CCode (cname = "wxListItem_SetId", has_target = false)]
        public void id_set(int id);

        [CCode (cname = "wxListItem_SetImage", has_target = false)]
        public void img_set(int img);

        [CCode (cname = "wxListItem_SetMask", has_target = false)]
        public void mask_set(int mask);

        [CCode (cname = "wxListItem_SetBackgroundColour", has_target = false)]
        public void bg_colour_set(Colour col_bg);

        [CCode (cname = "wxListItem_SetTextColour", has_target = false)]
        public void txt_colour_set(Colour col_txt);

        [CCode (cname = "wxListItem_SetText", has_target = false)]
        public void txt_set(Str txt);
    }

    [CCode (cname = "void", free_function = "wxMenu_DeletePointer", has_type_id = false)]
    [Compact]
    public class Menu
    {
        [CCode (cname = "wxMenu_FindItem", has_target = false)]
        public unowned MenuItem? item_find(int id);

        [CCode (cname = "wxMenu_FindItemByLabel", has_target = false)]
        public int item_by_lbl_find(Str item_str);
    }

    [CCode (cname = "void", free_function = "wxMenuBar_DeletePointer", has_type_id = false)]
    [Compact]
    public class MenuBar : Win
    {
        [CCode (cname = "wxMenuBar_GetMenu", has_target = false)]
        public unowned Menu? menu_get(int pos);
    }

    [CCode (cname = "void", free_function = "wxMenuItem_Delete", has_type_id = false)]
    [Compact]
    public class MenuItem
    {
        [CCode (cname = "wxMenuItem_Check", has_target = false)]
        public void check(bool check_flag);

        [CCode (cname = "wxMenuItem_Enable", has_target = false)]
        public void en(bool en_flag);

        [CCode (cname = "wxMenuItem_GetId", has_target = false)]
        public int id_get();

        [CCode (cname = "wxMenuItem_GetMenu", has_target = false)]
        public unowned Menu? menu_get();

        [CCode (cname = "wxMenuItem_GetSubMenu", has_target = false)]
        public unowned Menu? submenu_get();

        [CCode (cname = "wxMenuItem_IsChecked", has_target = false)]
        public bool is_checked();

        [CCode (cname = "wxMenuItem_SetId", has_target = false)]
        public void id_set(int id);

        [CCode (cname = "wxMenuItem_SetItemLabel", has_target = false)]
        public void item_lbl_set(Str str);
    }

    [CCode (cname = "void", free_function = "wxMessageDialog_Delete", has_type_id = false)]
    [Compact]
    public class MsgDlg : Dlg
    {
        [Flags]
        public enum style_t
        {
            [CCode (cname = "wxCENTER", has_type_id = false)]
            CENTR,
            [CCode (cname = "wxYES", has_type_id = false)]
            YES,
            [CCode (cname = "wxOK", has_type_id = false)]
            OK,
            [CCode (cname = "wxNO", has_type_id = false)]
            NO,
            [CCode (cname = "wxCANCEL", has_type_id = false)]
            CANCEL
        }

        [CCode (cname = "wxMessageDialog_Create", has_target = false)]
        public MsgDlg(Win? parent, Str _msg, Str _cap, int _stl);

        [CCode (cname = "wxMessageDialog_ShowModal", has_target = false)]
        public int modal_show();
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Notebook : Ctrl
    {
        [CCode (cname = "wxNotebook_GetSelection", has_target = false)]
        public int sel_get();

        [CCode (cname = "wxNotebook_SetSelection", has_target = false)]
        public int sel_set(int num_page);

        [CCode (cname = "wxNotebook_AddPage", has_target = false)]
        public int page_add(Win page, Str str_txt, bool sel, int img_id);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Panel : Win
    {
        [CCode (cname = "wxPanel_Create", has_target = false)]
        public Panel(Win _parent, int _id, int _left, int _top, int _width, int _height, Win.style_t _style);
    }

    [CCode (cname = "void", free_function = "wxPoint_Delete", has_type_id = false)]
    [Compact]
    public class Point
    {
        [CCode (cname = "wxPoint_Create", has_target = false)]
        public Point(int x, int y);

        [CCode (cname = "wxPoint_GetX", has_target = false)]
        public int x_get();

        [CCode (cname = "wxPoint_GetY", has_target = false)]
        public int y_get();

        [CCode (cname = "wxPoint_SetX", has_target = false)]
        public void x_set(int x);

        [CCode (cname = "wxPoint_SetY", has_target = false)]
        public void y_set(int y);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class RadioBox : Ctrl
    {
        [CCode (cname = "wxRadioBox_Create", has_target = false)]
        public RadioBox(Win parent, int _id, Str _txt, int _left, int _top, int _width, int _height, int _num, void *_str, int _dim, Win.style_t _style);

        [CCode (cname = "wxRadioBox_GetSelection", has_target = false)]
        public int sel_get();

        [CCode (cname = "wxRadioBox_SetSelection", has_target = false)]
        public void sel_set(int _num);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class RadioBtn : Ctrl
    {
        [CCode (cname = "wxRadioButton_Create", has_target = false)]
        public RadioBtn(Win _parent, int _id, Str _txt, int _left, int _top, int _width, int _height, Win.style_t _style);

        [CCode (cname = "wxRadioButton_GetValue", has_target = false)]
        public bool sel_get();

        [CCode (cname = "wxRadioButton_SetValue", has_target = false)]
        public void sel_set(bool val);
    }

    [CCode (cname = "void", free_function = "wxSize_Delete", has_type_id = false)]
    [Compact]
    public class Size
    {
        [CCode (cname = "wxSize_Create", has_target = false)]
        public Size(int width, int height);

        [CCode (cname = "wxSize_GetHeight", has_target = false)]
        public int height_get();

        [CCode (cname = "wxSize_GetWidth", has_target = false)]
        public int width_get();
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class Sizer
    {
        [CCode (cname = "wxSizer_Fit", has_target = false)]
        public void fit(Win win);

        [CCode (cname = "wxSizer_Layout", has_target = false)]
        public void layout_set();

        [CCode (cname = "wxSizer_InsertWindow", has_target = false)]
        public void win_insert(int before, Win win, int opt, int flag, int border, void *user_data);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Slider : Ctrl
    {
        [CCode (cname = "wxSlider_Create", has_target = false)]
        public Slider(int _id, int _init, int _min, int _max, int _lft, int _top, int _wdt, int _hgt, long _stl);

        [CCode (cname = "wxSlider_ClearSel", has_target = false)]
        public void sel_clr();

        [CCode (cname = "wxSlider_ClearTicks", has_target = false)]
        public void ticks_clr();

        [CCode (cname = "wxSlider_GetLineSize", has_target = false)]
        public int line_size_get();

        [CCode (cname = "wxSlider_GetMax", has_target = false)]
        public int max_get();

        [CCode (cname = "wxSlider_GetMin", has_target = false)]
        public int min_get();

        [CCode (cname = "wxSlider_GetPageSize", has_target = false)]
        public int page_size_get();

        [CCode (cname = "wxSlider_GetSelEnd", has_target = false)]
        public int sel_end_get();

        [CCode (cname = "wxSlider_GetSelStart", has_target = false)]
        public int sel_start_get();

        [CCode (cname = "wxSlider_GetThumbLength", has_target = false)]
        public int thumb_len_get();

        [CCode (cname = "wxSlider_GetTickFreq", has_target = false)]
        public int tick_freq_get();

        [CCode (cname = "wxSlider_GetValue", has_target = false)]
        public int val_get();

        [CCode (cname = "wxSlider_SetLineSize", has_target = false)]
        public void line_size_set(int line_size);

        [CCode (cname = "wxSlider_SetPageSize", has_target = false)]
        public void page_size_set(int page_size);

        [CCode (cname = "wxSlider_SetRange", has_target = false)]
        public void range_set(int min_val, int max_val);

        [CCode (cname = "wxSlider_SetSelection", has_target = false)]
        public void sel_set(int min_pos, int max_pos);

        [CCode (cname = "wxSlider_SetThumbLength", has_target = false)]
        public void thumb_len_set(int len);

        [CCode (cname = "wxSlider_SetTick", has_target = false)]
        public void tick_set(int tick_pos);

        [CCode (cname = "wxSlider_SetValue", has_target = false)]
        public void val_set(int val);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class SpinBtn : Ctrl
    {
        [CCode (cname = "wxSpinButton_Create", has_target = false)]
        public SpinBtn(Win _parent, int _id, int _left, int _top, int _width, int _height, Win.style_t _style);

        [CCode (cname = "wxSpinButton_SetValue", has_target = false)]
        public void val_set(int val);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class SpinCtrl : Ctrl
    {
        [CCode (cname = "wxSpinCtrl_GetValue", has_target = false)]
        public int val_get();

        [CCode (cname = "wxSpinCtrl_SetValue", has_target = false)]
        public void val_set(int val);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class SplitterWin : Win
    {
        [CCode (cname = "wxSplitterWindow_GetSashPosition", has_target = false)]
        public int sash_pos_get();

        [CCode (cname = "wxSplitterWindow_SetSashPosition", has_target = false)]
        public void sash_pos_set(int pos, bool redraw_flag);
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
#if __WXMAC
    public class SrchCtrl : TxtCtrl
#else
    public class SrchCtrl : Ctrl
#endif
    {
        [CCode (cname = "wxSearchCtrl_CanCopy", has_target = false)]
        public bool can_copy();

        [CCode (cname = "wxSearchCtrl_CanCut", has_target = false)]
        public bool can_cut();

        [CCode (cname = "wxSearchCtrl_CanPaste", has_target = false)]
        public bool can_paste();

        [CCode (cname = "wxSearchCtrl_ShowCancelButton", has_target = false)]
        public void cancel_btn_show(bool show_flag);

        [CCode (cname = "wxSearchCtrl_IsCancelButtonVisible", has_target = false)]
        public bool cancel_btn_vis_is();

        [CCode (cname = "wxSearchCtrl_Clear", has_target = false)]
        public void clr();

        [CCode (cname = "wxSearchCtrl_Copy", has_target = false)]
        public void copy();

        [CCode (cname = "wxSearchCtrl_Cut", has_target = false)]
        public void cut();

        [CCode (cname = "wxSearchCtrl_GetDefaultStyle", has_target = false)]
        public unowned TxtAttr def_style_get();

        [CCode (cname = "wxSearchCtrl_SetDefaultStyle", has_target = false)]
        public bool def_style_set(TxtAttr style);

        [CCode (cname = "wxSearchCtrl_GetDescriptiveText", has_target = false)]
        public Str desc_txt_get();

        [CCode (cname = "wxSearchCtrl_SetDescriptiveText", has_target = false)]
        public void desc_txt_set(Str txt);

        [CCode (cname = "wxSearchCtrl_IsEditable", has_target = false)]
        public bool editable_is();

        [CCode (cname = "wxSearchCtrl_SetEditable", has_target = false)]
        public void editable_set(bool editable);

        [CCode (cname = "wxSearchCtrl_DiscardEdits", has_target = false)]
        public void edits_discard();

        [CCode (cname = "wxSearchCtrl_LoadFile", has_target = false)]
        public bool file_load(Str file);

        [CCode (cname = "wxSearchCtrl_SaveFile", has_target = false)]
        public bool file_save(Str file);

        [CCode (cname = "wxSearchCtrl_GetInsertionPoint", has_target = false)]
        public long ins_point_get();

        [CCode (cname = "wxSearchCtrl_EmulateKeyPress", has_target = false)]
        public int key_press_emulate(KeyEvt *key_evt);

        [CCode (cname = "wxSearchCtrl_GetLineLength", has_target = false)]
        public int line_len_get(long line_num);

        [CCode (cname = "wxSearchCtrl_GetLineText", has_target = false)]
        public Str line_txt_get(long line_num);

        [CCode (cname = "wxSearchCtrl_SetMaxLength", has_target = false)]
        public void max_len_set(long len);

        [CCode (cname = "wxSearchCtrl_IsMultiLine", has_target = false)]
        public bool multiline_is();

        [CCode (cname = "wxSearchCtrl_GetMenu", has_target = false)]
        public unowned Menu? menu_get();

        [CCode (cname = "wxSearchCtrl_SetMenu", has_target = false)]
        public void menu_set(Menu menu);

        [CCode (cname = "wxSearchCtrl_IsModified", has_target = false)]
        public bool modified_is();

        [CCode (cname = "wxSearchCtrl_Paste", has_target = false)]
        public void paste();

        [CCode (cname = "wxSearchCtrl_GetLastPosition", has_target = false)]
        public long pos_last_get();

        [CCode (cname = "wxSearchCtrl_ShowPosition", has_target = false)]
        public void pos_show(long pos);

        [CCode (cname = "wxSearchCtrl_PositionToXY", has_target = false)]
        public int pos_to_xy_convert(long pos, long *x, long *y);

        [CCode (cname = "wxSearchCtrl_GetRange", has_target = false)]
        public Str range_get(long from, long to);

        [CCode (cname = "wxSearchCtrl_Redo", has_target = false)]
        public void redo();

        [CCode (cname = "wxSearchCtrl_CanRedo", has_target = false)]
        public bool redo_can();

        [CCode (cname = "wxSearchCtrl_GetSelection", has_target = false)]
        public void sel_get(void *from, void *to);

        [CCode (cname = "wxSearchCtrl_IsSingleLine", has_target = false)]
        public bool single_line_is();

        [CCode (cname = "wxSearchCtrl_ShowSearchButton", has_target = false)]
        public void srch_btn_show(bool show_flag);

        [CCode (cname = "wxSearchCtrl_IsSearchButtonVisible", has_target = false)]
        public bool srch_btn_vis_is();

        [CCode (cname = "wxSearchCtrl_GetStringSelection", has_target = false)]
        public Str str_sel_get();

        [CCode (cname = "wxSearchCtrl_SetStyle", has_target = false)]
        public bool style_set(long start, long end, TxtAttr style);

        [CCode (cname = "wxSearchCtrl_SetInsertionPointEnd", has_target = false)]
        public void insert_point_end_set();

        [CCode (cname = "wxSearchCtrl_SetInsertionPoint", has_target = false)]
        public void insert_point_set(long pos);

        [CCode (cname = "wxSearchCtrl_GetNumberOfLines", has_target = false)]
        public int num_of_lines_get();

        [CCode (cname = "wxSearchCtrl_Remove", has_target = false)]
        public void remove(long from, long to);

        [CCode (cname = "wxSearchCtrl_Replace", has_target = false)]
        public void replace(long from, long to, Str val);

        [CCode (cname = "wxSearchCtrl_SetSelection", has_target = false)]
        public void sel_set(long from, long to);

        [CCode (cname = "wxSearchCtrl_AppendText", has_target = false)]
        public void txt_append(Str txt);

        [CCode (cname = "wxSearchCtrl_WriteText", has_target = false)]
        public void txt_write(Str txt);

        [CCode (cname = "wxSearchCtrl_Undo", has_target = false)]
        public void undo();

        [CCode (cname = "wxSearchCtrl_CanUndo", has_target = false)]
        public bool undo_can();

        [CCode (cname = "wxSearchCtrl_ChangeValue", has_target = false)]
        public void val_change(Str val);

        [CCode (cname = "wxSearchCtrl_GetValue", has_target = false)]
        public Str val_get();

        [CCode (cname = "wxSearchCtrl_SetValue", has_target = false)]
        public bool val_set(Str val);

        [CCode (cname = "wxSearchCtrl_XYToPosition", has_target = false)]
        public long xy_to_pos_convert(long x, long y);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class StaticLine : Ctrl
    {
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class StaticTxt : Ctrl
    {
    }

    [CCode (cname = "void", free_function = "wxString_Delete", has_type_id = false)]
    [Compact]
    public class Str
    {
        [CCode (cname = "wxString_CreateUTF8", has_target = false)]
        public Str(string buf);

        [CCode (cname = "wxString_Create", has_target = false)]
        public Str.from_arr_init([CCode (array_length = false)] Ch[] buf);

        [CCode (cname = "wxString_GetString", has_target = false)]
        public int str_get([CCode (array_length = false)] Ch[] buf);

        [CCode (cname = "wxString_GetUtf8", has_target = false)]
        public ChBuf utf8_get();

        [CCode (cname = "wxString_Length", has_target = false)]
        public uint len_get();
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class StaticBMP : Win
    {
        [CCode (cname = "wxStaticBitmap_SetBitmap", has_target = false)]
        public void bmp_set(BMP bmp);

        [CCode (cname = "wxStaticBitmap_SetIcon", has_target = false)]
        public void icon_set(Icon icon);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class StatusBar : Ctrl
    {
        [CCode (cname = "wxStatusBar_SetFieldsCount", has_target = false)]
        public void fields_cnt_set([CCode (array_length_type="int", array_length_pos = 0.9)] int[] widths);

        [CCode (cname = "wxStatusBar_SetStatusText", has_target = false)]
        public void status_txt_set(Str txt, int num);
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class TreeCtrl : Ctrl
    {
        [CCode (cname = "wxTreeCtrl_AddRoot", has_target = false)]
        public void root_add(Str txt, int img, int sel_img, Closure closure, TreeItemId _item);

        [CCode (cname = "wxTreeCtrl_AppendItem", has_target = false)]
        public void item_append(TreeItemId parent, Str txt, int img, int sel_img, Closure data, TreeItemId _item);

        [CCode (cname = "wxTreeCtrl_Delete", has_target = false)]
        public void del(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_DeleteAllItems", has_target = false)]
        public void all_items_del();

        [CCode (cname = "wxTreeCtrl_Expand", has_target = false)]
        public void expand(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_GetItemClientClosure", has_target = false)]
        public unowned Closure client_closure_get(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_GetItemText", has_target = false)]
        public Str item_txt_get(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_GetCount", has_target = false)]
        public int cnt_get();

        [CCode (cname = "wxTreeCtrl_GetSelection", has_target = false)]
        public void sel_get(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_GetRootItem", has_target = false)]
        public void root_item_get(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_ItemHasChildren", has_target = false)]
        public bool item_child_has(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_SelectItem", has_target = false)]
        public void item_sel(TreeItemId item);

        [CCode (cname = "wxTreeCtrl_Unselect", has_target = false)]
        public void unsel();
    }

    [CCode (cname = "void", free_function = "wxTreeItemId_Delete", has_type_id = false)]
    [Compact]
    public class TreeItemId
    {
        [CCode (cname = "wxTreeItemId_Create", has_target = false)]
        public TreeItemId();

        [CCode (cname = "wxTreeItemId_GetValue", has_target = false)]
        public intptr val_get();

        [CCode (cname = "wxTreeItemId_IsOk", has_target = false)]
        public bool ok_is();
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class TopLvlWin : Win
    {
        [CCode (cname = "wxTopLevelWindow_SetIcon", has_target = false)]
        public void icon_set(Icon icon);

        [CCode (cname = "wxTopLevelWindow_SetIcons", has_target = false)]
        public void icons_set(IconBundle icons);
    }

    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class TxtCtrl : Ctrl
    {
        [CCode (cname = "wxTextCtrl_AppendText", has_target = false)]
        public bool append(Str text);

        [CCode (cname = "wxTextCtrl_Clear", has_target = false)]
        public void clr();

        [CCode (cname = "wxTextCtrl_GetDefaultStyle", has_target = false)]
        public unowned TxtAttr def_style_get();

        [CCode (cname = "wxTextCtrl_GetLineLength", has_target = false)]
        public int line_len_get(long line_num);

        [CCode (cname = "wxTextCtrl_GetNumberOfLines", has_target = false)]
        public int num_of_lines_get();

        [CCode (cname = "wxTextCtrl_GetValue", has_target = false)]
        public Str val_get();

        [CCode (cname = "wxTextCtrl_Remove", has_target = false)]
        public void remove(long from, long to);

        [CCode (cname = "wxTextCtrl_Replace", has_target = false)]
        public void replace(long from, long to, Str val);

        [CCode (cname = "wxTextCtrl_SetDefaultStyle", has_target = false)]
        public bool def_style_set(TxtAttr style);

        [CCode (cname = "wxTextCtrl_SetInsertionPoint", has_target = false)]
        public void insert_point_set(long pos);

        [CCode (cname = "wxTextCtrl_SetInsertionPointEnd", has_target = false)]
        public void insert_point_end_set();

        [CCode (cname = "wxTextCtrl_SetSelection", has_target = false)]
        public void sel_set(long from, long to);

        [CCode (cname = "wxTextCtrl_SetMaxLength", has_target = false)]
        public void max_len_set(long len);

        [CCode (cname = "wxTextCtrl_ShowPosition", has_target = false)]
        public void pos_show(long pos);

        [CCode (cname = "wxTextCtrl_SetStyle", has_target = false)]
        public bool style_set(long start, long end, TxtAttr style);

        [CCode (cname = "wxTextCtrl_SetValue", has_target = false)]
        public bool val_set(Str val);
    }

    [CCode (cname = "void", free_function = "wxTextAttr_Delete", has_type_id = false)]
    [Compact]
    public class TxtAttr
    {
        [CCode (cname = "wxTextAttr_Create", has_target = false)]
        public TxtAttr(Colour col_txt, Colour col_bg, Font font);

        [CCode (cname = "wxTextAttr_CreateFromAnotherTextAttr", has_target = false)]
        public TxtAttr.from_another_txt_attr_create(TxtAttr attr);

        [CCode (cname = "wxTextAttr_GetFont", has_target = false)]
        public void font_get(Font font);

        [CCode (cname = "wxTextAttr_SetFont", has_target = false)]
        public void font_set(Font font);
    }

    [CCode (cname = "wxUint8", has_type_id = false)]
    [SimpleType]
    public struct Uint8 : uint8
    {
    }

    [CCode (cname = "void", free_function = "wxWindow_Destroy", has_type_id = false)]
    [Compact]
    public class Win
    {
        public enum id_t
        {
            [CCode (cname = "wxID_CANCEL", has_type_id = false)]
            CANCEL,
        }

        [Flags]
        public enum style_t
        {
            [CCode (cname = "wxVSCROLL", has_type_id = false)]
            VSCROLL = 0x80000000UL,
            [CCode (cname = "wxHSCROLL", has_type_id = false)]
            HSCROLL = 0x40000000UL,
            [CCode (cname = "wxCAPTION", has_type_id = false)]
            CAPTION = 0x20000000UL,
            [CCode (cname = "wxDOUBLE_BORDER", has_type_id = false)]
            DOUBLE_BORDER = border_t.DOUBLE,
            [CCode (cname = "wxSUNKEN_BORDER", has_type_id = false)]
            SUNKEN_BORDER = border_t.SUNKEN,
            [CCode (cname = "wxRAISED_BORDER", has_type_id = false)]
            RAISED_BORDER = border_t.RAISED_STYLE,
            [CCode (cname = "wxBORDER", has_type_id = false)]
            BORDER = border_t.SIMPLE,
            [CCode (cname = "wxSIMPLE_BORDER", has_type_id = false)]
            SIMPLE_BORDER = border_t.SIMPLE,
            [CCode (cname = "wxSTATIC_BORDER", has_type_id = false)]
            STATIC_BORDER = border_t.STATIC,
            [CCode (cname = "wxNO_BORDER", has_type_id = false)]
            NO_BORDER = border_t.NONE,
            [CCode (cname = "wxALWAYS_SHOW_SB", has_type_id = false)]
            ALWAYS_SHOW_SB = 0x00800000UL,
            [CCode (cname = "wxCLIP_CHILDREN", has_type_id = false)]
            CLIP_CHILDREN = 0x00400000UL,
            [CCode (cname = "wxCLIP_SIBLINGS", has_type_id = false)]
            CLIP_SIBLINGS = 0x20000000UL,
            [CCode (cname = "wxTRANSPARENT_WINDOW", has_type_id = false)]
            TRANSPARENT_WIN = 0x00100000UL,
            [CCode (cname = "wxTAB_TRAVERSAL", has_type_id = false)]
            TAB_TRAVERSAL = 0x00080000UL,
            [CCode (cname = "wxWANTS_CHARS", has_type_id = false)]
            WANTS_CHARS = 0x00040000UL,
            [CCode (cname = "wxBACKINGSTORE", has_type_id = false)]
            BACKINGSTORE = RETAINED,
            [CCode (cname = "wxPOPUP_WINDOW", has_type_id = false)]
            POPUP_WIN = 0x00020000UL,
            [CCode (cname = "wxFULL_REPAINT_ON_RESIZE", has_type_id = false)]
            ON_RESIZE_FULL_REPAINT = 0x00010000UL,
            [CCode (cname = "wxNO_FULL_REPAINT_ON_RESIZE", has_type_id = false)]
            ON_RESIZE_NO_FULL_REPAINT = 0,
            [CCode (cname = "wxRETAINED", has_type_id = false)]
            RETAINED = 0,
        }

        [CCode (cname = "wxWindow_Close", has_target = false)]
        public void close(bool force);

        [CCode (cname = "wxWindow_GetContainingSizer", has_target = false)]
        public unowned Sizer? containing_sizer_get();

        [CCode (cname = "wxWindow_Create", has_target = false)]
        public Win(Win? _parent, int _id, int _x, int _y, int width, int height, Win.style_t style);

        [CCode (cname = "wxWindow_Destroy", has_target = false)]
        [DestroysInstance]
        public void destroy();

        [CCode (cname = "wxWindow_Disable", has_target = false)]
        public bool dis();

        [CCode (cname = "wxWindow_Enable", has_target = false)]
        public bool en();

        [CCode (cname = "wxWindow_FindWindow", has_target = false)]
        public unowned Win? win_find(Str name);

        [CCode (cname = "wxWindow_Fit", has_target = false)]
        public void fit();

        [CCode (cname = "wxWindow_SetForegroundColour", has_target = false)]
        public void fg_colour_set(Colour col);

        [CCode (cname = "wxWindow_FitInside", has_target = false)]
        public void inside_fit();

        [CCode (cname = "wxWindow_GetId", has_target = false)]
        public int id_get();

        [CCode (cname = "wxWindow_GetParent", has_target = false)]
        public unowned Win parent_get();

        [CCode (cname = "wxWindow_GetPosition", has_target = false)]
        public Point pos_get();

        [CCode (cname = "wxWindow_GetSize", has_target = false)]
        public Size size_get();

        [CCode (cname = "wxWindow_GetLabel", has_target = false)]
        public Str lbl_get();

        [CCode (cname = "wxWindow_Hide", has_target = false)]
        public void hide();

        [CCode (cname = "wxWindow_IsShown", has_target = false)]
        public bool is_shown();

        [CCode (cname = "wxWindow_Move", has_target = false)]
        public void move(int x, int y);

        [CCode (cname = "wxWindow_PopupMenu", has_target = false)]
        public bool menu_popup(Menu menu, int x, int y);

        [CCode (cname = "wxWindow_PopEventHandler", has_target = false)]
        public void *event_handler_pop(bool del_handler);

        [CCode (cname = "wxWindow_PushEventHandler", has_target = false)]
        public void event_handler_push(EventHandler handler);

        [CCode (cname = "wxWindow_HasFocus", has_target = false)]
        public bool focus_has();

        [CCode (cname = "wxWindow_Refresh", has_target = false)]
        public void refresh(bool erase_bg);

        [CCode (cname = "wxWindow_ScreenToClient", has_target = false)]
        public Point screen_to_client_convert(int x, int y);

        [CCode (cname = "wxWindow_SetAcceleratorTable", has_target = false)]
        public void acl_tbl_set(AclTbl acl);

        [CCode (cname = "wxWindow_SetAutoLayout", has_target = false)]
        public void autolayout_set(bool autolayout);

        [CCode (cname = "wxWindow_SetFocus", has_target = false)]
        public void focus_set();

        [CCode (cname = "wxWindow_SetScrollPos", has_target = false)]
        public void scroll_pos_set(int orient, int pos, bool refresh_flag);

        [CCode (cname = "wxWindow_SetExtraStyle", has_target = false)]
        public void extra_style_set(long extra_style);

        [CCode (cname = "wxWindow_SetId", has_target = false)]
        public bool id_set(int _id);

        [CCode (cname = "wxWindow_SetLabel", has_target = false)]
        public bool lbl_set(Str _title);

        [CCode (cname = "wxWindow_SetSize", has_target = false)]
        public void size_set(int x, int y, int width, int height, int size_flags);

        [CCode (cname = "wxWindow_SetSizer", has_target = false)]
        public void sizer_set(Sizer sizer);

        [CCode (cname = "wxWindow_SetWindowStyleFlag", has_target = false)]
        public void style_win_set(long style);

        [CCode (cname = "wxWindow_Show", has_target = false)]
        public void show();

        [CCode (cname = "wxWindow_UpdateWindowUI", has_target = false)]
        public void win_ui_upd();
    }

    //[CCode (cname = "void", free_function = "wxXmlResource_Delete", has_type_id = false)] // wxXmlResource_Delete() is not defined, only declaration
    [CCode (cname = "void", free_function = "", has_type_id = false)]
    [Compact]
    public class XMLResource
    {
        [CCode (cname = "wxXmlResource_Create", has_target = false)]
        public XMLResource(int flags);

        [CCode (cname = "wxXmlResource_GetXRCID", has_target = false)]
        public int id_get(Str str_id);

        [CCode (cname = "wxXmlResource_InitAllHandlers", has_target = false)]
        public void all_handlers_init();

        [CCode (cname = "wxXmlResource_Load", has_target = false)]
        public bool load(Str filemask);

        [CCode (cname = "wxXmlResource_LoadFrame", has_target = false)]
        public unowned Frm? frm_load(Win? parent, Str name);

        [CCode (cname = "wxXmlResource_LoadDialog", has_target = false)]
        public unowned Dlg? dlg_load(Win? parent, Str name);

        [CCode (cname = "wxXmlResource_LoadMenu", has_target = false)]
        public unowned Menu? menu_load(Str name);

        [CCode (cname = "wxXmlResource_LoadMenuBar", has_target = false)]
        public unowned MenuBar? menu_bar_load(Win? parent, Str name);
    }
}

