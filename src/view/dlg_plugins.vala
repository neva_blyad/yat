/*
 *    dlg_plugins.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Plugins" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgPlugins : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Check list box with plugins
         */
        public Wrapper.CheckLstBox check_lst_plugins { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * "About" button
         */
        public Wrapper.Btn btn_about { public get; public set; }

        /**
         * "Preferences" button
         */
        public Wrapper.Btn btn_preferences { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.check_lst_plugins = new Wrapper.CheckLstBox();
            this.btn_cancel        = new Wrapper.Btn();
            this.btn_about         = new Wrapper.Btn();
            this.btn_preferences   = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                   = xml.dlg_load(null, new Wx.Str("dlg_plugins"));
            GLib.assert(this.win                   != null);
            this.check_lst_plugins.win = this.win.win_find(new Wx.Str("check_lst_plugins"));
            GLib.assert(this.check_lst_plugins.win != null);
            this.btn_cancel.win        = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win        != null);
            this.btn_about.win         = this.win.win_find(new Wx.Str("btn_about"));
            GLib.assert(this.btn_about.win         != null);
            this.btn_preferences.win   = this.win.win_find(new Wx.Str("btn_preferences"));
            GLib.assert(this.btn_preferences.win   != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_plugins.closed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.win_close(), closure);

            id      = this.check_lst_plugins.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_plugins.check_lst_plugins_toggled_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_check_lst_box_toggled(), closure);

            //id      = this.check_lst_plugins.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_plugins.check_lst_plugins_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_lst_box_sel(), closure);

            id      = this.btn_about.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_plugins.btn_about_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_preferences.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_plugins.btn_preferences_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Plugins" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgPlugins(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

