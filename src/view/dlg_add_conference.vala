/*
 *    dlg_add_conference.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Add Conference" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgAddConference : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Title label
         */
        public Wrapper.StaticTxt lbl_title { public get; public set; }

        /**
         * Title input text control
         */
        public Wrapper.TxtCtrl txt_title { public get; public set; }

        /**
         * Alias label
         */
        public Wrapper.StaticTxt lbl_Alias { public get; public set; }

        /**
         * Alias input text control
         */
        public Wrapper.TxtCtrl txt_Alias { public get; public set; }

        /**
         * Invites label
         */
        public Wrapper.StaticTxt lbl_invites { public get; public set; }

        /**
         * List box with buddies
         */
        public Wrapper.LstBox lst_box_buddies { public get; public set; }

        /**
         * Right button
         */
        public Wrapper.Btn btn_right { public get; public set; }

        /**
         * Left button
         */
        public Wrapper.Btn btn_left { public get; public set; }

        /**
         * List box with invited buddies
         */
        public Wrapper.LstBox lst_box_buddies_invites { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * "Add" button
         */
        public Wrapper.Btn btn_add_conference { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.lbl_title               = new Wrapper.StaticTxt();
            this.txt_title               = new Wrapper.TxtCtrl();
            this.lbl_Alias               = new Wrapper.StaticTxt();
            this.txt_Alias               = new Wrapper.TxtCtrl();
            this.lbl_invites             = new Wrapper.StaticTxt();
            this.lst_box_buddies         = new Wrapper.LstBox();
            this.btn_right               = new Wrapper.Btn();
            this.btn_left                = new Wrapper.Btn();
            this.lst_box_buddies_invites = new Wrapper.LstBox();
            this.btn_cancel              = new Wrapper.Btn();
            this.btn_add_conference      = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                         = xml.dlg_load(null, new Wx.Str("dlg_add_conference"));
            GLib.assert(this.win                         != null);
            this.lbl_title.win               = this.win.win_find(new Wx.Str("lbl_title"));
            GLib.assert(this.lbl_title.win               != null);
            this.txt_title.win               = this.win.win_find(new Wx.Str("txt_title"));
            GLib.assert(this.txt_title.win               != null);
            this.lbl_Alias.win               = this.win.win_find(new Wx.Str("lbl_Alias"));
            GLib.assert(this.lbl_Alias.win               != null);
            this.txt_Alias.win               = this.win.win_find(new Wx.Str("txt_Alias"));
            GLib.assert(this.txt_Alias.win               != null);
            this.lbl_invites.win             = this.win.win_find(new Wx.Str("lbl_invites"));
            GLib.assert(this.lbl_invites.win             != null);
            this.lst_box_buddies.win         = this.win.win_find(new Wx.Str("lst_box_buddies"));
            GLib.assert(this.lst_box_buddies.win         != null);
            this.btn_right.win               = this.win.win_find(new Wx.Str("btn_right"));
            GLib.assert(this.btn_right.win               != null);
            this.btn_left.win                = this.win.win_find(new Wx.Str("btn_left"));
            GLib.assert(this.btn_left.win                != null);
            this.lst_box_buddies_invites.win = this.win.win_find(new Wx.Str("lst_box_buddies_invites"));
            GLib.assert(this.lst_box_buddies_invites.win != null);
            this.btn_cancel.win              = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win              != null);
            this.btn_add_conference.win      = this.win.win_find(new Wx.Str("btn_add_conference"));
            GLib.assert(this.btn_add_conference.win      != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.txt_title.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.txt_title_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.lst_box_buddies.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.lst_box_buddies_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_lst_box_sel(), closure);

            id      = this.btn_right.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.btn_right_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_left.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.btn_left_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.lst_box_buddies_invites.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.lst_box_buddies_invites_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_lst_box_sel(), closure);

            id      = this.btn_add_conference.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_add_conference.btn_add_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Add Conference" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgAddConference(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }
    }
}

