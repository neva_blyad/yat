/*
 *    view.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View object
 */
View.View *view;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the View class
     */
    [SingleInstance]
    public class View : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Properties
        private Wx.Locale locale_;
        private Wx.XMLResource xml_;

        // GUI start closure
        private Wx.Closure closure;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
          * CLI arguments
          */
        public string[] args { public get; public set; }

        /**
         * Language-dependent settings
         */
        public Wx.Locale locale
        {
            public get
            {
                return this.locale_;
            }

            public owned set // valac 0.56.3 warning if no explicit setter for this property
            {
                this.locale_ = (owned) value;
            }
        }

        /**
         * XML Based Resource System (XRC)
         */
        public Wx.XMLResource xml
        {
            public get
            {
                return this.xml_;
            }

            public owned set // valac 0.56.3 warning if no explicit setter for this property
            {
                this.xml_ = (owned) value;
            }
        }

        /**
         * "Choose Profile" dialog
         */
        public DlgProfile dlg_profile { public get; public set; }

        /**
         * Main window frame
         */
        public FrmMain frm_main { public get; public set; }

        /**
         * "About Profile" / "About Buddy" dialog
         */
        public DlgAbout dlg_about { public get; public set; }

        /**
         * "Edit Profile" dialog
         */
        public DlgEditProfile dlg_edit_profile { public get; public set; }

        /**
         * "Add Buddy" dialog
         */
        public DlgAddBuddy dlg_add_buddy { public get; public set; }

        /**
         * "Edit Buddy" dialog
         */
        public DlgEditBuddy dlg_edit_buddy { public get; public set; }

        /**
         * "Conversation History" frame
         */
        public FrmConv frm_conv { public get; public set; }

        /**
         * "Add Conference" dialog
         */
        public DlgAddConference dlg_add_conference { public get; public set; }

        /**
         * "Edit Conference" dialog
         */
        public DlgEditConference dlg_edit_conference { public get; public set; }

        /**
         * "Plugins" dialog
         */
        public DlgPlugins dlg_plugins { public get; public set; }

        /**
         * "Settings" dialog
         */
        public DlgSettings dlg_settings { public get; public set; }

        /**
         * "In Loving Memory of" dialog
         */
        public DlgVika dlg_Vika { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        private Wx.Locale.lang_t locale_get()
        {
            Wx.Locale.lang_t locale;

            try
            {
                Model.Model.cfg_appearance_lang_t lang =
                    (Model.Model.cfg_appearance_lang_t) model->cfg.get_integer("appearance", "lang");

                switch (lang)
                {
                case Model.Model.cfg_appearance_lang_t.DEF:
                    locale = Wx.Locale.lang_t.DEF;
                    break;
                case Model.Model.cfg_appearance_lang_t.EN_UK:
                    locale = Wx.Locale.lang_t.EN_UK;
                    break;
                case Model.Model.cfg_appearance_lang_t.EN_US:
                    locale = Wx.Locale.lang_t.EN_US;
                    break;
                case Model.Model.cfg_appearance_lang_t.RU:
                    locale = Wx.Locale.lang_t.RU;
                    break;
                default:
                    locale = Wx.Locale.lang_t.DEF;
                    break;
                }
            }
            catch (GLib.KeyFileError ex)
            {
                locale = Wx.Locale.lang_t.DEF;
                GLib.assert(false);
            }

            return locale;
        }

        /*----------------------------------------------------------------------------*/

        // Initialize application
        private void app_init()
        {
            // Initializes all available image handlers
            Wx.App.all_img_handlers_init();
        }

        /*----------------------------------------------------------------------------*/

        // Initialize locale
        private void locale_init()
        {
            // Get locale
            Wx.Locale.lang_t locale = locale_get();

            // Initialize locale
            this.locale = new Wx.Locale(locale,
                                        1 // wxXRC_USE_LOCALE, translatable strings will be translated via _()
                                       );

            // Prepare strings for locale methods
            Wx.Str locale_str         = new Wx.Str(model->locale);
            Wx.Str locale_plugins_str = new Wx.Str(model->locale_plugins);

            var locale_arr         = new Wx.Ch[locale_str.len_get()];
            var locale_plugins_arr = new Wx.Ch[locale_plugins_str.len_get()];

            locale_str.str_get(locale_arr);
            locale_plugins_str.str_get(locale_plugins_arr);

            // The following code will set the appropriate variables so that
            // the import of XRC code can be translated
            this.locale.catalog_lookup_path_prefix_add(locale_arr);
            this.locale.catalog_lookup_path_prefix_add(locale_plugins_arr);

            // Bind domain to the directory with translations
            domain_add(Cfg.DOMAIN);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize XRC.
        //
        // Loads the XRC file.
        private void xrc_init()
        {
            // XRC filename.
            // This file contains GUI description in XML format.
            const string FILENAME = "yat.xrc";

            // Load the XRC file
            this.xml = new Wx.XMLResource(1);
            var res = this.xml.load(new Wx.Str(FILENAME));
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets.
        private void widgets_init()
        {
            // Allocate widgets
            this.dlg_profile         = new DlgProfile       (this.xml);
            this.frm_main            = new FrmMain          (this.xml);
            this.dlg_about           = new DlgAbout         (this.xml);
            this.dlg_edit_profile    = new DlgEditProfile   (this.xml);
            this.dlg_add_buddy       = new DlgAddBuddy      (this.xml);
            this.dlg_edit_buddy      = new DlgEditBuddy     (this.xml);
            this.frm_conv            = new FrmConv          (this.xml);
            this.dlg_add_conference  = new DlgAddConference (this.xml);
            this.dlg_edit_conference = new DlgEditConference(this.xml);
            this.dlg_plugins         = new DlgPlugins       (this.xml);
            this.dlg_settings        = new DlgSettings      (this.xml);
            this.dlg_Vika            = new DlgVika          (this.xml);
        }

        /*----------------------------------------------------------------------------*/

#if __WXMSW__
        // Initialize icons.
        //
        // Sets icon to all frames and dialogs.
        private void icon_init()
        {
            // Constants
            const string FILENAME = "yat.ico";

            // Filepath to icon
            string filepath = GLib.Path.build_filename(model->prefix, "img", FILENAME);

            // Set icon to frames and dialogs
            this.dlg_profile.icon_set        (filepath);
            this.frm_main.icon_set           (filepath);
            this.dlg_about.icon_set          (filepath);
            this.dlg_edit_profile.icon_set   (filepath);
            this.dlg_add_buddy.icon_set      (filepath);
            this.dlg_edit_buddy.icon_set     (filepath);
            this.frm_conv.icon_set           (filepath);
            this.dlg_plugins.icon_set        (filepath);
            this.dlg_settings.icon_set       (filepath);
            this.dlg_add_conference.icon_set (filepath);
            this.dlg_edit_conference.icon_set(filepath);
            this.dlg_Vika.icon_set           (filepath);
        }
#endif

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * View constructor.
         *
         * Initializes application.
         * Initializes locale.
         * Initializes XRC.
         * Initializes widgets.
         * Initializes icons.
         * Asks Controller to finish GUI setup.
         *
         * @param args CLI arguments
         */
        public View(string[] args)
        {
            // GUI start closure
            Wx.Closure.fn_t start = (fn, param, event) =>
            {
                view->app_init();
                view->locale_init();
                view->xrc_init();
                view->widgets_init();
#if __WXMSW__
                view->icon_init();
#endif
                ctrl->gui_setup();

                return null;
            };

            // Remember the program arguments
            this.args    = args;
            this.closure = new Wx.Closure(start, null);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * View destructor
         */
        ~View()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Execute GUI loop.
         * This method blocks.
         *
         * Initializes wxWidgets application, invokes GUI start closure,
         * enters main loop.
         */
        public void loop()
        {
            Wx.App.c_init(this.closure,
                          this.args.length,
                          this.args);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Exit from GUI loop.
         *
         * Destroys all windows. They will be deleted in the loop.
         * It exits from the loop and possibly terminates application.
         */
        public void exit()
        {
            this.dlg_profile.win.destroy();
            this.frm_main.win.destroy();
            this.dlg_about.win.destroy();
            this.dlg_edit_profile.win.destroy();
            this.dlg_add_buddy.win.destroy();
            this.dlg_edit_buddy.win.destroy();
            this.frm_conv.win.destroy();
            this.dlg_add_conference.win.destroy();
            this.dlg_edit_conference.win.destroy();
            this.dlg_plugins.win.destroy();
            this.dlg_settings.win.destroy();
            this.dlg_Vika.win.destroy();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Wake up idle
          */
        public void idle_wake_up()
        {
            Wx.idle_wake_up();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Bind domain to the directory with translations
          *
          * @param domain Localization domain
          */
        public void domain_add(string domain)
        {
            // Prepare strings for locale methods
            Wx.Str domain_str = new Wx.Str(domain);
            var domain_arr = new Wx.Ch[domain_str.len_get()];
            domain_str.str_get(domain_arr);

            // Bind domain to the directory with translations
            bool res = this.locale.catalog_add(domain_arr);
            GLib.assert(res);
            GLib.assert(this.locale.ok_is());
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Convert database colour number to View internal presentation
          *
          * @param colour Database colour number
          *
          * @return View colour
          */
        public Wrapper.Ctrl.colour_t db_colour_convert(uint colour)
        {
            Wrapper.Ctrl.colour_t colour_;

            colour = colour % 4;

            switch (colour)
            {
            case 0:  colour_ = Wrapper.Ctrl.colour_t.BLUE;
                     break;
            case 1:  colour_ = Wrapper.Ctrl.colour_t.RED;
                     break;
            case 2:  colour_ = Wrapper.Ctrl.colour_t.GREEN;
                     break;
            case 3:  colour_ = Wrapper.Ctrl.colour_t.CYAN;
                     break;
            //case 4:
            default: colour_ = Wrapper.Ctrl.colour_t.GREY;
                     break;
            }

            return colour_;
        }
    }
}

