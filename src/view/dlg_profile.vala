/*
 *    dlg_profile.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the "Choose Profile" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class DlgProfile : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Notebook
         */
        public Wrapper.Notebook notebook { public get; public set; }

        /**
         * Tab 1 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab1 { public get; public set; }

        /**
         * Profile name label.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.StaticTxt lbl_name_tab1 { public get; public set; }

        /**
         * Profile name input text control.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.TxtCtrl txt_name_tab1 { public get; public set; }

        /**
         * Password label.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.StaticTxt lbl_pwd_tab1 { public get; public set; }

        /**
         * Password input text control
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.TxtCtrl txt_pwd_tab1 { public get; public set; }

        /**
         * Password confirmation label.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.StaticTxt lbl_confirm_tab1 { public get; public set; }

        /**
         * Password confirmation input text control.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.TxtCtrl txt_confirm_tab1 { public get; public set; }

        /**
         * "Use password" check box.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.CheckBox check_pwd_tab1 { public get; public set; }

        /**
         * "Don't save on disk" check box.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.CheckBox check_mem_only_tab1 { public get; public set; }

        /**
         * Password strength label.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.StaticTxt lbl_pwd_strength_tab1 { public get; public set; }

        /**
         * Password strength gauge.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Gauge gauge_pwd_strength_tab1 { public get; public set; }

        /**
         * "Create" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_create_tab1 { public get; public set; }

        /**
         * Tab 2 panel.
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tab2 { public get; public set; }

        /**
         * Profile name label.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.StaticTxt lbl_name_tab2 { public get; public set; }

        /**
         * Choice of profile name.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Choice choice_name_tab2 { public get; public set; }

        /**
         * Password label.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.StaticTxt lbl_pwd_tab2 { public get; public set; }

        /**
         * Password input text control.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.TxtCtrl txt_pwd_tab2 { public get; public set; }

        /**
         * "Action" button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Btn btn_action_tab2 { public get; public set; }

        /**
         * "Load" button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Btn btn_load_tab2 { public get; public set; }

        /**
         * "Load automatically" check box
         */
        public Wrapper.CheckBox check_autoload { public get; public set; }

        /**
         * Context menu for "Action" button
         */
        public Wrapper.Menu menu_action { public get; public set; }

        /**
         * "Rename..." menu item.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.MenuItem menu_rename_profile { public get; public set; }

        /**
         * "Delete" menu item.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.MenuItem menu_del_profile { public get; public set; }

        /**
         * "Import" menu item.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.MenuItem menu_import { public get; public set; }

        /**
         * "Export..." submenu.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.Menu menu_export { public get; public set; }

        /**
         * "Export..." -> "Export for yat..." menu item.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.MenuItem menu_export_yat { public get; public set; }

        /**
         * "Export..." -> "Export for another Tox client..." menu item.
         *
         * It is placed in context menu for "Action" button.
         */
        public Wrapper.MenuItem menu_export_another { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.notebook                = new Wrapper.Notebook();
            this.panel_tab1              = new Wrapper.Panel();
            this.lbl_name_tab1           = new Wrapper.StaticTxt();
            this.txt_name_tab1           = new Wrapper.TxtCtrl();
            this.lbl_pwd_tab1            = new Wrapper.StaticTxt();
            this.txt_pwd_tab1            = new Wrapper.TxtCtrl();
            this.lbl_confirm_tab1        = new Wrapper.StaticTxt();
            this.txt_confirm_tab1        = new Wrapper.TxtCtrl();
            this.check_pwd_tab1          = new Wrapper.CheckBox();
            this.check_mem_only_tab1     = new Wrapper.CheckBox();
            this.lbl_pwd_strength_tab1   = new Wrapper.StaticTxt();
            this.gauge_pwd_strength_tab1 = new Wrapper.Gauge();
            this.btn_create_tab1         = new Wrapper.Btn();
            this.panel_tab2              = new Wrapper.Panel();
            this.lbl_name_tab2           = new Wrapper.StaticTxt();
            this.choice_name_tab2        = new Wrapper.Choice();
            this.lbl_pwd_tab2            = new Wrapper.StaticTxt();
            this.txt_pwd_tab2            = new Wrapper.TxtCtrl();
            this.btn_action_tab2         = new Wrapper.Btn();
            this.btn_load_tab2           = new Wrapper.Btn();
            this.check_autoload          = new Wrapper.CheckBox();

            this.menu_action             = new Wrapper.Menu();
            this.menu_rename_profile     = new Wrapper.MenuItem();
            this.menu_del_profile        = new Wrapper.MenuItem();
            this.menu_import             = new Wrapper.MenuItem();
            this.menu_export             = new Wrapper.Menu();
            this.menu_export_yat         = new Wrapper.MenuItem();
            this.menu_export_another     = new Wrapper.MenuItem();

            // Find/load the widgets in XRC file
            this.win                         = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_profile"));
            GLib.assert(this.win                         != null);
            this.notebook.win                = this.win.win_find(new Wx.Str("notebook"));
            GLib.assert(this.notebook.win                != null);
            this.panel_tab1.win              = this.notebook.win.win_find(new Wx.Str("panel_tab1"));
            GLib.assert(this.panel_tab1.win              != null);
            this.lbl_name_tab1.win           = this.panel_tab1.win.win_find(new Wx.Str("lbl_name_tab1"));
            GLib.assert(this.lbl_name_tab1.win           != null);
            this.txt_name_tab1.win           = this.panel_tab1.win.win_find(new Wx.Str("txt_name_tab1"));
            GLib.assert(this.txt_name_tab1.win           != null);
            this.lbl_pwd_tab1.win            = this.panel_tab1.win.win_find(new Wx.Str("lbl_pwd_tab1"));
            GLib.assert(this.lbl_pwd_tab1.win            != null);
            this.txt_pwd_tab1.win            = this.panel_tab1.win.win_find(new Wx.Str("txt_pwd_tab1"));
            GLib.assert(this.txt_pwd_tab1.win            != null);
            this.lbl_confirm_tab1.win        = this.panel_tab1.win.win_find(new Wx.Str("lbl_confirm_tab1"));
            GLib.assert(this.lbl_confirm_tab1.win        != null);
            this.txt_confirm_tab1.win        = this.panel_tab1.win.win_find(new Wx.Str("txt_confirm_tab1"));
            GLib.assert(this.txt_confirm_tab1.win        != null);
            this.check_pwd_tab1.win          = this.panel_tab1.win.win_find(new Wx.Str("check_pwd_tab1"));
            GLib.assert(this.check_pwd_tab1.win          != null);
            this.check_mem_only_tab1.win     = this.panel_tab1.win.win_find(new Wx.Str("check_mem_only_tab1"));
            GLib.assert(this.check_mem_only_tab1.win     != null);
            this.lbl_pwd_strength_tab1.win   = this.panel_tab1.win.win_find(new Wx.Str("lbl_pwd_strength_tab1"));
            GLib.assert(this.lbl_pwd_strength_tab1.win   != null);
            this.gauge_pwd_strength_tab1.win = this.panel_tab1.win.win_find(new Wx.Str("gauge_pwd_strength_tab1"));
            GLib.assert(this.gauge_pwd_strength_tab1.win != null);
            this.btn_create_tab1.win         = this.panel_tab1.win.win_find(new Wx.Str("btn_create_tab1"));
            GLib.assert(this.btn_create_tab1.win         != null);
            this.panel_tab2.win              = this.notebook.win.win_find(new Wx.Str("panel_tab2"));
            GLib.assert(this.panel_tab2.win              != null);
            this.lbl_name_tab2.win           = this.panel_tab2.win.win_find(new Wx.Str("lbl_name_tab2"));
            GLib.assert(this.lbl_name_tab2.win           != null);
            this.choice_name_tab2.win        = this.panel_tab2.win.win_find(new Wx.Str("choice_name_tab2"));
            GLib.assert(this.choice_name_tab2.win        != null);
            this.lbl_pwd_tab2.win            = this.panel_tab2.win.win_find(new Wx.Str("lbl_pwd_tab2"));
            GLib.assert(this.lbl_pwd_tab2.win            != null);
            this.txt_pwd_tab2.win            = this.panel_tab2.win.win_find(new Wx.Str("txt_pwd_tab2"));
            GLib.assert(this.txt_pwd_tab2.win            != null);
            this.btn_action_tab2.win         = this.panel_tab2.win.win_find(new Wx.Str("btn_action_tab2"));
            GLib.assert(this.btn_action_tab2.win         != null);
            this.btn_load_tab2.win           = this.panel_tab2.win.win_find(new Wx.Str("btn_load_tab2"));
            GLib.assert(this.btn_load_tab2.win           != null);
            this.check_autoload.win          = this.win.win_find(new Wx.Str("check_autoload"));
            GLib.assert(this.check_autoload.win          != null);

            this.menu_action.menu         = xml.menu_load(new Wx.Str("menu_action"));
            GLib.assert(this.menu_action.menu         != null);
            this.menu_rename_profile.menu = this.menu_action.menu.item_find(xml.id_get(new Wx.Str("menu_rename_profile")));
            GLib.assert(this.menu_rename_profile.menu != null);
            this.menu_del_profile.menu    = this.menu_action.menu.item_find(xml.id_get(new Wx.Str("menu_del_profile")));
            GLib.assert(this.menu_del_profile.menu    != null);
            this.menu_import.menu         = this.menu_action.menu.item_find(xml.id_get(new Wx.Str("menu_import")));
            GLib.assert(this.menu_import.menu         != null);
            this.menu_export_yat.menu     = this.menu_action.menu.item_find(xml.id_get(new Wx.Str("menu_export_yat")));
            GLib.assert(this.menu_export_yat.menu     != null);
            this.menu_export_another.menu = this.menu_action.menu.item_find(xml.id_get(new Wx.Str("menu_export_another")));
            GLib.assert(this.menu_export_another.menu != null);
            this.menu_export.menu         = this.menu_export_yat.menu.menu_get();
            GLib.assert(this.menu_export.menu         != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            int        id;
            Wx.Closure closure;

            id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.closed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.win_close(), closure);

            id      = this.txt_name_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.txt_name_tab1_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.txt_pwd_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.txt_pwd_tab1_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.txt_confirm_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.txt_confirm_tab1_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.check_pwd_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.check_pwd_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_checkbox_clicked(), closure);

            id      = this.check_mem_only_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.check_mem_only_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_checkbox_clicked(), closure);

            id      = this.btn_create_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.btn_create_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.choice_name_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.choice_name_tab2_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.btn_action_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.btn_action_tab2_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_load_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.btn_load_tab2_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.check_autoload.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.check_autoload_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_checkbox_clicked(), closure);

            id      = this.menu_rename_profile.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.menu_rename_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_action.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_del_profile.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.menu_del_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_action.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_import.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.menu_import_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_action.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_export_yat.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.menu_export_yat_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_export.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);
            Wx.EventHandler.connect((void *) this.menu_action.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_export_another.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->dlg_profile.menu_export_another_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_export.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);
            Wx.EventHandler.connect((void *) this.menu_action.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Choose Profile" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgProfile(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set password strength gauge and label values.
         *
         * They are placed on the tab 1 panel.
         *
         * @param score_percent Password strength in percent
         */
        public void pwd_strength_tab1_set(uint score_percent)
            requires (0 <= score_percent <= 100)
        {
            // Set label
            string str = _("Password strength: %u%%").printf(score_percent);
            this.lbl_pwd_strength_tab1.lbl_set(str);

            // Set gauge
            this.gauge_pwd_strength_tab1.val_set(score_percent);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide password strength gauge and label.
         *
         * They are placed on the tab 1 panel.
         *
         * @param flag false: hide,
         *             true:  show
         */
        public void pwd_strength_tab1_show(bool flag)
        {
            this.lbl_pwd_strength_tab1.show(flag);
            this.gauge_pwd_strength_tab1.show(flag);
        }
    }
}

