/*
 *    frm_main.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the main window frame class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    [SingleInstance]
    public class FrmMain : Wrapper.Frm
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Width
         */
        public const size_t WIDTH  = 1440;

        /**
         * Height
         */
        public const size_t HEIGHT = 540;

        /**
         * Menu bar
         */
        public Wrapper.MenuBar menu { public get; public set; }

        /**
         * "Profile" menu
         */
        public Wrapper.Menu menu_profile { public get; public set; }

        /**
         * "Profile" -> "Edit Profile..." menu item
         */
        public Wrapper.MenuItem menu_edit_profile { public get; public set; }

        /**
         * "Profile" -> "About Profile..." menu item
         */
        public Wrapper.MenuItem menu_about_profile { public get; public set; }

        /**
         * "Profile" -> "Change Profile..." menu item
         */
        public Wrapper.MenuItem menu_change_profile { public get; public set; }

        /**
         * "Profile" -> "Quit" menu item
         */
        public Wrapper.MenuItem menu_quit { public get; public set; }

        /**
         * "Buddies" menu
         */
        public Wrapper.Menu menu_buddies { public get; public set; }

        /**
         * "Buddies" -> "Add Buddy..." menu item
         */
        public Wrapper.MenuItem menu_add_buddy { public get; public set; }

        /**
         * "Buddies" -> "Edit Buddy..." menu item
         */
        public Wrapper.MenuItem menu_edit_buddy { public get; public set; }

        /**
         * "Buddies" -> "Remove Buddy" menu item
         */
        public Wrapper.MenuItem menu_remove_buddy { public get; public set; }

        /**
         * "Buddies" -> "About Buddy..." menu item
         */
        public Wrapper.MenuItem menu_about_buddy { public get; public set; }

        /**
         * "Buddies" -> "Conversation History..." menu item
         */
        public Wrapper.MenuItem menu_conv_buddy { public get; public set; }

        /**
         * "Buddies" -> "Show" submenu
         */
        public Wrapper.Menu menu_show_buddies { public get; public set; }

        /**
         * "Buddies" -> "Show" -> "Show all" menu item
         */
        public Wrapper.MenuItem menu_show_all { public get; public set; }

        /**
         * "Buddies" -> "Show" -> "Hide offline but show unread" menu
         * item
         */
        public Wrapper.MenuItem menu_hide_offline_but { public get; public set; }

        /**
         * "Buddies" -> "Show" -> "Hide offline" menu item
         */
        public Wrapper.MenuItem menu_hide_offline { public get; public set; }

        /**
         * "Buddies" -> "Sort By" submenu
         */
        public Wrapper.Menu menu_sort_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Adding to buddy list" menu item
         */
        public Wrapper.MenuItem menu_sort_add_lst_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Display name" menu item
         */
        public Wrapper.MenuItem menu_sort_display_name_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Last message" menu item
         */
        public Wrapper.MenuItem menu_sort_last_msg_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Network state" menu item
         */
        public Wrapper.MenuItem menu_sort_state_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Ascending" menu item
         */
        public Wrapper.MenuItem menu_sort_asc_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Descending" menu item
         */
        public Wrapper.MenuItem menu_sort_desc_buddies { public get; public set; }

        /**
         * "Buddies" -> "Sort By" -> "Unread are always top" menu item
         */
        public Wrapper.MenuItem menu_sort_unread_top_buddies { public get; public set; }

        /**
         * "Conferences" menu
         */
        public Wrapper.Menu menu_conferences { public get; public set; }

        /**
         * "Conferences" -> "Add Conference..." menu item
         */
        public Wrapper.MenuItem menu_add_conference { public get; public set; }

        /**
         * "Conferences" -> "Edit Conference..." menu item
         */
        public Wrapper.MenuItem menu_edit_conference { public get; public set; }

        /**
         * "Conferences" -> "Remove Conference" menu item
         */
        public Wrapper.MenuItem menu_remove_conference { public get; public set; }

        /**
         * "Conferences" -> "About Conference..." menu item
         */
        public Wrapper.MenuItem menu_about_conference { public get; public set; }

        /**
         * "Conferences" -> "Conversation History..." menu item
         */
        public Wrapper.MenuItem menu_conv_conference { public get; public set; }

        /**
         * "Conferences" -> "Sort By" submenu
         */
        public Wrapper.Menu menu_sort_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Adding to conference list" menu
         * item
         */
        public Wrapper.MenuItem menu_sort_add_lst_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Display name" menu item
         */
        public Wrapper.MenuItem menu_sort_display_name_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Last message" menu item
         */
        public Wrapper.MenuItem menu_sort_last_msg_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Ascending" menu item
         */
        public Wrapper.MenuItem menu_sort_asc_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Descending" menu item
         */
        public Wrapper.MenuItem menu_sort_desc_conferences { public get; public set; }

        /**
         * "Conferences" -> "Sort By" -> "Unread are always top" menu item
         */
        public Wrapper.MenuItem menu_sort_unread_top_conferences { public get; public set; }

        /**
         * "Conferences" -> "Members" submenu
         */
        public Wrapper.Menu menu_members { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Add to Buddy List" / "Find in
         * Buddy List" menu item
         */
        public Wrapper.MenuItem menu_members_action_in_buddy_list { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "About Member..." menu item
         */
        public Wrapper.MenuItem menu_about_member { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Sort By" submenu
         */
        public Wrapper.Menu menu_sort_members { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Sort By" -> "Adding to member
         * list" menu item
         */
        public Wrapper.MenuItem menu_sort_add_lst_members { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Sort By" -> "Display name"
         * menu item
         */
        public Wrapper.MenuItem menu_sort_display_name_members { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Sort By" -> "Ascending" menu
         * item
         */
        public Wrapper.MenuItem menu_sort_asc_members { public get; public set; }

        /**
         * "Conferences" -> "Members" -> "Sort By" -> "Descending" menu
         * item
         */
        public Wrapper.MenuItem menu_sort_desc_members { public get; public set; }

        /**
         * "Tools" menu
         */
        public Wrapper.Menu menu_tools { public get; public set; }

        /**
         * "Tools" -> "Plugins" menu item
         */
        public Wrapper.MenuItem menu_plugins { public get; public set; }

        /**
         * "Tools" -> "Settings" menu item
         */
        public Wrapper.MenuItem menu_settings { public get; public set; }

        /**
         * "Help" menu
         */
        public Wrapper.Menu menu_help { public get; public set; }

        /**
         * "Help" -> "In Loving Memory of..." menu item
         */
        public Wrapper.MenuItem menu_Vika { public get; public set; }

        /**
         * "Help" -> "About..." menu item
         */
        public Wrapper.MenuItem menu_about { public get; public set; }

        /**
         * Vertical splitter
         */
        public Wrapper.SplitterWin splitter_vertical { public get; public set; }

        /**
         * Sash position
         */
        public const size_t
            SPLITTER_VERTICAL_SASH_POS = 480;

        /**
         * Left panel.
         *
         * It is placed on the vertical splitter.
         */
        public Wrapper.Panel panel_left { public get; public set; }

        /**
         * Self avatar.
         *
         * It is placed on the left panel.
         */
        public Wrapper.StaticBMP img_avatar { public get; public set; }

        /**
         * Self name.
         *
         * It is placed on the left panel.
         */
        public Wrapper.StaticTxt lbl_name { public get; public set; }

        /**
         * Self status.
         *
         * It is placed on the left panel.
         */
        public Wrapper.StaticTxt lbl_status { public get; public set; }

        /**
         * Icon with self network state.
         *
         * It is placed on the left panel.
         */
        public Wrapper.StaticBMP img_state { public get; public set; }

        /**
         * Button for changing self user state.
         *
         * It is placed on the left panel.
         */
        public Wrapper.BtnBMP btn_bmp_state_user { public get; public set; }

        /**
         * Context menu for changing self user state
         */
        public Wrapper.Menu menu_state_user { public get; public set; }

        /**
         * "Available" menu item.
         *
         * It is placed in context menu for changing self user state.
         */
        public Wrapper.MenuItem menu_avail { public get; public set; }

        /**
         * "Away" menu item.
         *
         * It is placed in context menu for changing self user state.
         */
        public Wrapper.MenuItem menu_away { public get; public set; }

        /**
         * "Busy" menu item.
         *
         * It is placed in context menu for changing self user state.
         */
        public Wrapper.MenuItem menu_busy { public get; public set; }

        /**
         * Contact list notebook.
         *
         * It is placed on the left panel.
         */
        public Wrapper.Notebook notebook_contacts { public get; public set; }

        /*
         * "Buddies" page in notebook with contacts
         */
        public const uint NOTEBOOK_CONTACTS_PAGE_BUDDIES     = 0;

        /*
         * "Conferences" page in notebook with contacts
         */
        public const uint NOTEBOOK_CONTACTS_PAGE_CONFERENCES = 1;

        /**
         * Buddy list panel.
         *
         * It is placed on the contact list notebook.
         */
        public Wrapper.Panel panel_buddies { public get; public set; }

        /**
         * List control with buddies.
         *
         * It is placed on the buddy list panel.
         */
        public Wrapper.LstCtrl lst_ctrl_buddies { public get; public set; }

        /*
         * Display name column in list control with buddies
         */
        public const uint LST_CTRL_BUDDIES_COL_DISPLAY_NAME = 0;

        /*
         * Status column in list control with buddies
         */
        public const uint LST_CTRL_BUDDIES_COL_STATUS       = 1;

        /*
         * Network state column in list control with buddies
         */
        public const uint LST_CTRL_BUDDIES_COL_STATE        = 2;

        /*
         * User state column in list control with buddies
         */
        public const uint LST_CTRL_BUDDIES_COL_STATE_USER   = 3;

        /**
         * Context menu for list control with buddies (right click)
         */
        public Wrapper.Menu menu_buddies_ { public get; public set; }

        /**
         * "Add Buddy..." menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_add_buddy_ { public get; public set; }

        /**
         * "Edit Buddy..." menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_edit_buddy_ { public get; public set; }

        /**
         * "Remove Buddy" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_remove_buddy_ { public get; public set; }

        /**
         * "About Buddy..." menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_about_buddy_ { public get; public set; }

        /**
         * "Conversation History..." menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_conv_buddy_ { public get; public set; }

        /**
         * "Show" submenu.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.Menu menu_show_buddies_ { public get; public set; }

        /**
         * "Show" -> "Show all" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_show_all_ { public get; public set; }

        /**
         * "Show" -> "Hide offline but show unread" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_hide_offline_but_ { public get; public set; }

        /**
         * "Show" -> "Hide offline" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_hide_offline_ { public get; public set; }

        /**
         * "Sort By" submenu.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.Menu menu_sort_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Adding to buddy list" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_add_lst_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Display name" menu item.
         *
         * It is plased in context menu for list control with buddies.
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_display_name_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Last message" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_last_msg_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Network state" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_state_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Ascending" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_asc_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Descending" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_desc_buddies_ { public get; public set; }

        /**
         * "Sort By" -> "Unread are always top" menu item.
         *
         * It is plased in context menu for list control with buddies
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_unread_top_buddies_ { public get; public set; }

        /**
         * Conference list panel.
         *
         * It is placed on the contact list notebook.
         */
        public Wrapper.Panel panel_conferences { public get; public set; }

        /**
         * List control with conferences.
         *
         * It is placed on the conference list panel.
         */
        public Wrapper.LstCtrl lst_ctrl_conferences { public get; public set; }

        /*
         * Display name column in list control with conferences
         */
        public const uint LST_CTRL_CONFERENCES_COL_DISPLAY_NAME = 0;

        /*
         * Count column in list control with conferences
         */
        public const uint LST_CTRL_CONFERENCES_COL_CNT          = 1;

        /*
         * Unread state column in list control with conferences
         */
        public const uint LST_CTRL_CONFERENCES_COL_UNREAD_STATE = 2;

        /**
         * Context menu for list control with conferences (right click)
         */
        public Wrapper.Menu menu_conferences_ { public get; public set; }

        /**
         * "Add Conference..." menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_add_conference_ { public get; public set; }

        /**
         * "Edit Conference..." menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_edit_conference_ { public get; public set; }

        /**
         * "Remove Conference" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_remove_conference_ { public get; public set; }

        /**
         * "About Conference..." menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_about_conference_ { public get; public set; }

        /**
         * "Conversation History..." menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_conv_conference_ { public get; public set; }

        /**
         * "Sort By" submenu.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.Menu menu_sort_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Adding to conference list" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_add_lst_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Display name" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_display_name_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Last message" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_last_msg_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Ascending" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_asc_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Descending" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_desc_conferences_ { public get; public set; }

        /**
         * "Sort By" -> "Unread are always top" menu item.
         *
         * It is plased in context menu for list control with
         * conferences (right click).
         */
        public Wrapper.MenuItem menu_sort_unread_top_conferences_ { public get; public set; }

        /**
         * Status & buddy filter choice.
         *
         * It is placed on the left panel.
         */
        public Wrapper.Choice choice_status_and_filter { public get; public set; }

        /**
         * Status index in choice
         */
        public const uint CHOICE_IDX_STATUS = 0;

        /**
         * Buddy filter index in choice
         */
        public const uint CHOICE_IDX_FILTER = 1;

        /**
         * Status & buddy filter input text control.
         *
         * It is placed on the left panel.
         */
        public Wrapper.TxtCtrl txt_status_and_filter { public get; public set; }

        /**
         * "Apply" button.
         *
         * It is placed on the left panel.
         */
        public Wrapper.Btn btn_apply_status { public get; public set; }

        /**
         * Right panel.
         *
         * It is placed on the vertical splitter.
         */
        public Wrapper.Panel panel_right { public get; public set; }

        /**
         * Main notebook.
         *
         * It is placed on the right panel.
         */
        public Wrapper.Notebook notebook_main { public get; public set; }

        /*
         * "Conversation" page in main notebook
         */
        public const uint NOTEBOOK_MAIN_PAGE_CONVERSATIONS      = 0;

        /*
         * "Buddy Requests" page in main notebook
         */
        public const uint NOTEBOOK_MAIN_PAGE_BUDDY_REQS         = 1;

        /*
         * "Conference Invites" page in main notebook
         */
        public const uint NOTEBOOK_MAIN_PAGE_CONFERENCE_INVITES = 2;

        /**
         * Tab 1 panel.
         *
         * It is placed on the main notebook.
         */
        public Wrapper.Panel panel_tab1 { public get; public set; }

        /**
         * Text control with last exchanged messages from/to a
         * buddy.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.TxtCtrl txt_conv_buddy_tab1 { public get; public set; }

        /**
          * Maximum number of messages to be appeared onto the text
          * control
          */
        public const int TXT_CONV_TAB1_LIMIT = 88;

        /**
         * Vertical splitter.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.SplitterWin splitter_vertical_tab1 { public get; public set; }

        /**
         * Sash position.
         *
         * It is placed on the tab 1 panel.
         */
        public const size_t
            SPLITTER_VERTICAL_TAB1_SASH_POS = 600;

        /**
         * Left panel.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Panel panel_left_tab1 { public get; public set; }

        /**
         * Text control with last exchanged messages from/to a
         * conference.
         *
         * It is placed on the left panel.
         */
        public Wrapper.TxtCtrl txt_conv_conference_tab1 { public get; public set; }

        /**
         * Right panel.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Panel panel_right_tab1 { public get; public set; }

        /**
         * List control with members.
         *
         * It is placed on the right panel.
         */
        public Wrapper.LstCtrl lst_ctrl_members_tab1 { public get; public set; }

        /**
         * Context menu for list control with members (right click)
         */
        public Wrapper.Menu menu_members_ { public get; public set; }

        /**
         * "Add to Buddy List" / "Find in Buddy List" menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_members_action_in_buddy_list_ { public get; public set; }

        /**
         * "About Member..." menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_about_member_ { public get; public set; }

        /**
         * "Sort By" submenu.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.Menu menu_sort_members_ { public get; public set; }

        /**
         * "Sort By" -> "Adding to member list" menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_add_lst_members_ { public get; public set; }

        /**
         * "Sort By" -> "Display name" menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_display_name_members_ { public get; public set; }

        /**
         * "Sort By" -> "Ascending" menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_asc_members_ { public get; public set; }

        /**
         * "Sort By" -> "Descending" menu item.
         *
         * It is plased in context menu for list control with members
         * (right click).
         */
        public Wrapper.MenuItem menu_sort_desc_members_ { public get; public set; }

        /*
         * Display name column in list control with members
         */
        public const uint LST_CTRL_MEMBERS_COL_DISPLAY_NAME = 0;

        /*
         * User state column in list control with members
         */
        public const uint LST_CTRL_MEMBERS_COL_STATE_USER   = 1;

        /**
         * Members filter input text control.
         *
         * It is placed on the right panel.
         */
        public Wrapper.TxtCtrl txt_filter_members { public get; public set; }

        /**
         * Input text control with current typing message.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.TxtCtrl txt_msg_tab1 { public get; public set; }

        /**
         * "Send" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_send_tab1 { public get; public set; }

        /**
         * "Audio Call" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_av_audio_call_tab1 { public get; public set; }

        /**
         * Audio Call "Finish" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_av_finish_tab1 { public get; public set; }

        /**
         * A/V Call "Accept" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_av_accept_tab1 { public get; public set; }

        /**
         * Audio Call "Reject" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_av_reject_tab1 { public get; public set; }

        /**
         * "Video Call" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_av_video_call_tab1 { public get; public set; }

        /**
         * Video Call "Finish" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_video_call_finish_tab1 { public get; public set; }

        /**
         * Video Call "Accept" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_video_call_accept_tab1 { public get; public set; }

        /**
         * Video Call "Reject" button.
         *
         * It is placed on the tab 1 panel.
         */
        public Wrapper.Btn btn_video_call_reject_tab1 { public get; public set; }

        /**
         * Tab 2 panel.
         *
         * It is placed on the main notebook.
         */
        public Wrapper.Panel panel_tab2 { public get; public set; }

        /**
         * Check list box with public keys of buddy requests.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.CheckLstBox check_lst_pub_keys_tab2 { public get; public set; }

        /**
         * Message input text control.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.TxtCtrl txt_msg_tab2 { public get; public set; }

        /**
         * "Reject" button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Btn btn_reject_tab2 { public get; public set; }

        /**
         * "Accept" button.
         *
         * It is placed on the tab 2 panel.
         */
        public Wrapper.Btn btn_accept_tab2 { public get; public set; }

        /**
         * Status bar
         */
        public Wrapper.StatusBar status { public get; public set; }

        /*
         * Width of dummy column in status bar
         */
        public const uint STATUS_WIDTH_DUMMY       =   5;

        /*
         * Width of column with number of total and currently online
         * buddies in status bar
         */
        public const uint STATUS_WIDTH_BUDDIES     = 150;

        /*
         * Width of column with number of conferences in status bar
         */
        public const uint STATUS_WIDTH_CONFERENCES = 150;

        /*
         * Width of event column in status bar
         */
        public const uint STATUS_WIDTH_EVT         = 450;

        /*
         * Column with number of total and currently online buddies in
         * status bar
         */
        public const uint STATUS_COL_BUDDIES     = 1;

        /*
         * Column with number of conferences in status bar
         */
        public const uint STATUS_COL_CONFERENCES = 2;

        /*
         * Event column in status bar
         */
        public const uint STATUS_COL_EVT         = 3;

        /**
         * Tab 3 panel.
         *
         * It is placed on the main notebook.
         */
        public Wrapper.Panel panel_tab3 { public get; public set; }

        /**
         * List control with invites.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.LstCtrl lst_ctrl_invites_tab3 { public get; public set; }

        /*
         * Display name column in list control with invites
         */
        public const uint LST_CTRL_INVITES_COL_DISPLAY_NAME = 0;

        /*
         * Cookie column in list control with invites
         */
        public const uint LST_CTRL_INVITES_COL_COOKIE       = 1;

        /*
         * Date and time column in list control with invites
         */
        public const uint LST_CTRL_INVITES_COL_DATETIME     = 2;

        /**
         * "Reject" button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_reject_tab3 { public get; public set; }

        /**
         * "Accept" button.
         *
         * It is placed on the tab 3 panel.
         */
        public Wrapper.Btn btn_accept_tab3 { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.menu                               = new Wrapper.MenuBar();
            this.menu_profile                       = new Wrapper.Menu();
            this.menu_edit_profile                  = new Wrapper.MenuItem();
            this.menu_about_profile                 = new Wrapper.MenuItem();
            this.menu_change_profile                = new Wrapper.MenuItem();
            this.menu_quit                          = new Wrapper.MenuItem();
            this.menu_buddies                       = new Wrapper.Menu();
            this.menu_add_buddy                     = new Wrapper.MenuItem();
            this.menu_edit_buddy                    = new Wrapper.MenuItem();
            this.menu_remove_buddy                  = new Wrapper.MenuItem();
            this.menu_about_buddy                   = new Wrapper.MenuItem();
            this.menu_conv_buddy                    = new Wrapper.MenuItem();
            this.menu_show_buddies                  = new Wrapper.Menu();
            this.menu_show_all                      = new Wrapper.MenuItem();
            this.menu_hide_offline_but              = new Wrapper.MenuItem();
            this.menu_hide_offline                  = new Wrapper.MenuItem();
            this.menu_sort_buddies                  = new Wrapper.Menu();
            this.menu_sort_add_lst_buddies          = new Wrapper.MenuItem();
            this.menu_sort_display_name_buddies     = new Wrapper.MenuItem();
            this.menu_sort_last_msg_buddies         = new Wrapper.MenuItem();
            this.menu_sort_state_buddies            = new Wrapper.MenuItem();
            this.menu_sort_asc_buddies              = new Wrapper.MenuItem();
            this.menu_sort_desc_buddies             = new Wrapper.MenuItem();
            this.menu_sort_unread_top_buddies       = new Wrapper.MenuItem();
            this.menu_conferences                   = new Wrapper.Menu();
            this.menu_add_conference                = new Wrapper.MenuItem();
            this.menu_edit_conference               = new Wrapper.MenuItem();
            this.menu_remove_conference             = new Wrapper.MenuItem();
            this.menu_about_conference              = new Wrapper.MenuItem();
            this.menu_conv_conference               = new Wrapper.MenuItem();
            this.menu_sort_conferences              = new Wrapper.Menu();
            this.menu_sort_add_lst_conferences      = new Wrapper.MenuItem();
            this.menu_sort_display_name_conferences = new Wrapper.MenuItem();
            this.menu_sort_last_msg_conferences     = new Wrapper.MenuItem();
            this.menu_sort_asc_conferences          = new Wrapper.MenuItem();
            this.menu_sort_desc_conferences         = new Wrapper.MenuItem();
            this.menu_sort_unread_top_conferences   = new Wrapper.MenuItem();
            this.menu_members                       = new Wrapper.Menu();
            this.menu_members_action_in_buddy_list  = new Wrapper.MenuItem();
            this.menu_about_member                  = new Wrapper.MenuItem();
            this.menu_sort_members                  = new Wrapper.Menu();
            this.menu_sort_add_lst_members          = new Wrapper.MenuItem();
            this.menu_sort_display_name_members     = new Wrapper.MenuItem();
            this.menu_sort_asc_members              = new Wrapper.MenuItem();
            this.menu_sort_desc_members             = new Wrapper.MenuItem();
            this.menu_tools                         = new Wrapper.Menu();
            this.menu_plugins                       = new Wrapper.MenuItem();
            this.menu_settings                      = new Wrapper.MenuItem();
            this.menu_help                          = new Wrapper.Menu();
            this.menu_Vika                          = new Wrapper.MenuItem();
            this.menu_about                         = new Wrapper.MenuItem();

            this.splitter_vertical                   = new Wrapper.SplitterWin();
            this.panel_left                          = new Wrapper.Panel();
            this.img_avatar                          = new Wrapper.StaticBMP();
            this.lbl_name                            = new Wrapper.StaticTxt();
            this.lbl_status                          = new Wrapper.StaticTxt();
            this.img_state                           = new Wrapper.StaticBMP();
            this.btn_bmp_state_user                  = new Wrapper.BtnBMP();
            this.menu_state_user                     = new Wrapper.Menu();
            this.menu_avail                          = new Wrapper.MenuItem();
            this.menu_away                           = new Wrapper.MenuItem();
            this.menu_busy                           = new Wrapper.MenuItem();
            this.notebook_contacts                   = new Wrapper.Notebook();
            this.panel_buddies                       = new Wrapper.Panel();
            this.lst_ctrl_buddies                    = new Wrapper.LstCtrl(Wrapper.LstCtrl.mode_t.OPTIMIZE_FOR_SPEED, 0, 0);
            this.menu_buddies_                       = new Wrapper.Menu();
            this.menu_add_buddy_                     = new Wrapper.MenuItem();
            this.menu_edit_buddy_                    = new Wrapper.MenuItem();
            this.menu_remove_buddy_                  = new Wrapper.MenuItem();
            this.menu_about_buddy_                   = new Wrapper.MenuItem();
            this.menu_conv_buddy_                    = new Wrapper.MenuItem();
            this.menu_show_buddies_                  = new Wrapper.Menu();
            this.menu_show_all_                      = new Wrapper.MenuItem();
            this.menu_hide_offline_but_              = new Wrapper.MenuItem();
            this.menu_hide_offline_                  = new Wrapper.MenuItem();
            this.menu_sort_buddies_                  = new Wrapper.Menu();
            this.menu_sort_add_lst_buddies_          = new Wrapper.MenuItem();
            this.menu_sort_display_name_buddies_     = new Wrapper.MenuItem();
            this.menu_sort_last_msg_buddies_         = new Wrapper.MenuItem();
            this.menu_sort_state_buddies_            = new Wrapper.MenuItem();
            this.menu_sort_asc_buddies_              = new Wrapper.MenuItem();
            this.menu_sort_desc_buddies_             = new Wrapper.MenuItem();
            this.menu_sort_unread_top_buddies_       = new Wrapper.MenuItem();
            this.panel_conferences                   = new Wrapper.Panel();
            this.lst_ctrl_conferences                = new Wrapper.LstCtrl(Wrapper.LstCtrl.mode_t.OPTIMIZE_FOR_SPEED, 0, 0);
            this.menu_conferences_                   = new Wrapper.Menu();
            this.menu_add_conference_                = new Wrapper.MenuItem();
            this.menu_edit_conference_               = new Wrapper.MenuItem();
            this.menu_remove_conference_             = new Wrapper.MenuItem();
            this.menu_about_conference_              = new Wrapper.MenuItem();
            this.menu_conv_conference_               = new Wrapper.MenuItem();
            this.menu_sort_conferences_              = new Wrapper.Menu();
            this.menu_sort_add_lst_conferences_      = new Wrapper.MenuItem();
            this.menu_sort_display_name_conferences_ = new Wrapper.MenuItem();
            this.menu_sort_last_msg_conferences_     = new Wrapper.MenuItem();
            this.menu_sort_asc_conferences_          = new Wrapper.MenuItem();
            this.menu_sort_desc_conferences_         = new Wrapper.MenuItem();
            this.menu_sort_unread_top_conferences_   = new Wrapper.MenuItem();
            this.choice_status_and_filter            = new Wrapper.Choice();
            this.txt_status_and_filter               = new Wrapper.TxtCtrl();
            this.btn_apply_status                    = new Wrapper.Btn();
            this.panel_right                         = new Wrapper.Panel();
            this.notebook_main                       = new Wrapper.Notebook();
            this.panel_tab1                          = new Wrapper.Panel();
            this.txt_conv_buddy_tab1                 = new Wrapper.TxtCtrl();
            this.splitter_vertical_tab1              = new Wrapper.SplitterWin();
            this.panel_left_tab1                     = new Wrapper.Panel();
            this.txt_conv_conference_tab1            = new Wrapper.TxtCtrl();
            this.panel_right_tab1                    = new Wrapper.Panel();
            this.lst_ctrl_members_tab1               = new Wrapper.LstCtrl(Wrapper.LstCtrl.mode_t.OPTIMIZE_FOR_SPEED, 0, 0);
            this.menu_members_                       = new Wrapper.Menu();
            this.menu_members_action_in_buddy_list_  = new Wrapper.MenuItem();
            this.menu_about_member_                  = new Wrapper.MenuItem();
            this.menu_sort_members_                  = new Wrapper.Menu();
            this.menu_sort_add_lst_members_          = new Wrapper.MenuItem();
            this.menu_sort_display_name_members_     = new Wrapper.MenuItem();
            this.menu_sort_asc_members_              = new Wrapper.MenuItem();
            this.menu_sort_desc_members_             = new Wrapper.MenuItem();
            this.txt_filter_members                  = new Wrapper.TxtCtrl();
            this.txt_msg_tab1                        = new Wrapper.TxtCtrl();
            this.btn_send_tab1                       = new Wrapper.Btn();
            this.btn_av_audio_call_tab1              = new Wrapper.Btn();
            this.btn_av_video_call_tab1              = new Wrapper.Btn();
            this.btn_av_finish_tab1                  = new Wrapper.Btn();
            this.btn_av_accept_tab1                  = new Wrapper.Btn();
            this.btn_av_reject_tab1                  = new Wrapper.Btn();
            this.panel_tab2                          = new Wrapper.Panel();
            this.check_lst_pub_keys_tab2             = new Wrapper.CheckLstBox();
            this.txt_msg_tab2                        = new Wrapper.TxtCtrl();
            this.btn_reject_tab2                     = new Wrapper.Btn();
            this.btn_accept_tab2                     = new Wrapper.Btn();
            this.panel_tab3                          = new Wrapper.Panel();
            this.lst_ctrl_invites_tab3               = new Wrapper.LstCtrl(Wrapper.LstCtrl.mode_t.OPTIMIZE_FOR_SPEED, 0, 0);
            this.btn_reject_tab3                     = new Wrapper.Btn();
            this.btn_accept_tab3                     = new Wrapper.Btn();

            this.status = new Wrapper.StatusBar();

            // Find/load the widgets in XRC file
            this.win                                     = (Wx.Win) xml.frm_load(null, new Wx.Str("frm_main"));
            GLib.assert(this.win                                     != null);
            //this.menu.win                                = this.win.win_find(new Wx.Str("menu")); // It doesn't work
            //this.menu.win                                = xml.menu_bar_load(this.win, new Wx.Str("menu")); // It doesn't work
            this.menu.win                                = ((Wx.Frm) this.win).menu_bar_get();
            GLib.assert(this.menu.win                                != null);
            //this.menu_profile.menu                       = xml.menu_load(new Wx.Str("menu")); // It doesn't work
            this.menu_profile.menu                       = ((Wx.MenuBar) this.menu.win).menu_get(0);
            GLib.assert(this.menu_profile.menu                       != null);
            this.menu_edit_profile.menu                  = this.menu_profile.menu.item_find(xml.id_get(new Wx.Str("menu_edit_profile")));
            GLib.assert(this.menu_edit_profile.menu                  != null);
            this.menu_about_profile.menu                 = this.menu_profile.menu.item_find(xml.id_get(new Wx.Str("menu_about_profile")));
            GLib.assert(this.menu_about_profile.menu                 != null);
            this.menu_change_profile.menu                = this.menu_profile.menu.item_find(xml.id_get(new Wx.Str("menu_change_profile")));
            GLib.assert(this.menu_change_profile.menu                != null);
            this.menu_quit.menu                          = this.menu_profile.menu.item_find(xml.id_get(new Wx.Str("menu_quit")));
            GLib.assert(this.menu_quit.menu                          != null);
            this.menu_buddies.menu                       = ((Wx.MenuBar) this.menu.win).menu_get(1);
            GLib.assert(this.menu_buddies.menu                       != null);
            this.menu_add_buddy.menu                     = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_add_buddy")));
            GLib.assert(this.menu_add_buddy.menu                     != null);
            this.menu_edit_buddy.menu                    = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_edit_buddy")));
            GLib.assert(this.menu_edit_buddy                         != null);
            this.menu_remove_buddy.menu                  = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_remove_buddy")));
            GLib.assert(this.menu_remove_buddy.menu                  != null);
            this.menu_about_buddy.menu                   = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_about_buddy")));
            GLib.assert(this.menu_about_buddy.menu                   != null);
            this.menu_conv_buddy.menu                    = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_conv_buddy")));
            GLib.assert(this.menu_conv_buddy.menu                    != null);
            this.menu_show_all.menu                      = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_show_all")));
            GLib.assert(this.menu_show_all.menu                      != null);
            this.menu_hide_offline_but.menu              = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_hide_offline_but")));
            GLib.assert(this.menu_hide_offline_but.menu              != null);
            this.menu_hide_offline.menu                  = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_hide_offline")));
            GLib.assert(this.menu_hide_offline.menu                  != null);
            this.menu_show_buddies.menu                  = this.menu_show_all.menu.menu_get();
            GLib.assert(this.menu_show_buddies.menu                  != null);
            this.menu_sort_add_lst_buddies.menu          = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_buddies")));
            GLib.assert(this.menu_sort_add_lst_buddies.menu          != null);
            this.menu_sort_display_name_buddies.menu     = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_buddies")));
            GLib.assert(this.menu_sort_display_name_buddies.menu     != null);
            this.menu_sort_last_msg_buddies.menu         = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_last_msg_buddies")));
            GLib.assert(this.menu_sort_last_msg_buddies.menu         != null);
            this.menu_sort_state_buddies.menu            = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_state_buddies")));
            GLib.assert(this.menu_sort_state_buddies.menu            != null);
            this.menu_sort_asc_buddies.menu              = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_buddies")));
            GLib.assert(this.menu_sort_asc_buddies.menu              != null);
            this.menu_sort_desc_buddies.menu             = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_buddies")));
            GLib.assert(this.menu_sort_desc_buddies.menu             != null);
            this.menu_sort_unread_top_buddies.menu       = this.menu_buddies.menu.item_find(xml.id_get(new Wx.Str("menu_sort_unread_top_buddies")));
            GLib.assert(this.menu_sort_unread_top_buddies.menu       != null);
            this.menu_sort_buddies.menu                  = this.menu_sort_add_lst_buddies.menu.menu_get();
            GLib.assert(this.menu_sort_buddies.menu                  != null);
            this.menu_conferences.menu                   = ((Wx.MenuBar) this.menu.win).menu_get(2);
            GLib.assert(this.menu_conferences.menu                   != null);
            this.menu_add_conference.menu                = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_add_conference")));
            GLib.assert(this.menu_add_conference.menu                != null);
            this.menu_edit_conference.menu               = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_edit_conference")));
            GLib.assert(this.menu_edit_conference                    != null);
            this.menu_remove_conference.menu             = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_remove_conference")));
            GLib.assert(this.menu_remove_conference.menu             != null);
            this.menu_about_conference.menu              = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_about_conference")));
            GLib.assert(this.menu_about_conference.menu              != null);
            this.menu_conv_conference.menu               = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_conv_conference")));
            GLib.assert(this.menu_conv_conference.menu               != null);
            this.menu_sort_add_lst_conferences.menu      = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_conferences")));
            GLib.assert(this.menu_sort_add_lst_conferences.menu      != null);
            this.menu_sort_display_name_conferences.menu = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_conferences")));
            GLib.assert(this.menu_sort_display_name_conferences.menu != null);
            this.menu_sort_last_msg_conferences.menu     = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_last_msg_conferences")));
            GLib.assert(this.menu_sort_last_msg_conferences.menu     != null);
            this.menu_sort_asc_conferences.menu          = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_conferences")));
            GLib.assert(this.menu_sort_asc_conferences.menu          != null);
            this.menu_sort_desc_conferences.menu         = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_conferences")));
            GLib.assert(this.menu_sort_desc_conferences.menu         != null);
            this.menu_sort_unread_top_conferences.menu   = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_unread_top_conferences")));
            GLib.assert(this.menu_sort_unread_top_conferences.menu   != null);
            this.menu_sort_conferences.menu              = this.menu_sort_add_lst_conferences.menu.menu_get();
            GLib.assert(this.menu_sort_conferences.menu              != null);
            this.menu_members_action_in_buddy_list.menu  = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_members_action_in_buddy_list")));
            GLib.assert(this.menu_members_action_in_buddy_list.menu  != null);
            this.menu_members.menu                       = this.menu_members_action_in_buddy_list.menu.menu_get();
            GLib.assert(this.menu_members.menu                       != null);
            this.menu_about_member.menu                  = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_about_member")));
            GLib.assert(this.menu_about_member.menu                  != null);
            this.menu_sort_add_lst_members.menu          = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_members")));
            GLib.assert(this.menu_sort_add_lst_members.menu          != null);
            this.menu_sort_display_name_members.menu     = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_members")));
            GLib.assert(this.menu_sort_display_name_members.menu     != null);
            this.menu_sort_asc_members.menu              = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_members")));
            GLib.assert(this.menu_sort_asc_members.menu              != null);
            this.menu_sort_desc_members.menu             = this.menu_conferences.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_members")));
            GLib.assert(this.menu_sort_desc_members.menu             != null);
            this.menu_sort_members.menu                  = this.menu_sort_add_lst_members.menu.menu_get();
            GLib.assert(this.menu_sort_members.menu                  != null);
            this.menu_tools.menu                         = ((Wx.MenuBar) this.menu.win).menu_get(3);
            GLib.assert(this.menu_tools.menu                         != null);
            this.menu_plugins.menu                       = this.menu_tools.menu.item_find(xml.id_get(new Wx.Str("menu_plugins")));
            GLib.assert(this.menu_plugins.menu                       != null);
            this.menu_settings.menu                      = this.menu_tools.menu.item_find(xml.id_get(new Wx.Str("menu_settings")));
            GLib.assert(this.menu_settings.menu                      != null);
            this.menu_help.menu                          = ((Wx.MenuBar) this.menu.win).menu_get(4);
            GLib.assert(this.menu_help.menu                          != null);
            this.menu_Vika.menu                          = this.menu_help.menu.item_find(xml.id_get(new Wx.Str("menu_Vika")));
            GLib.assert(this.menu_Vika.menu                          != null);
            this.menu_about.menu                         = this.menu_help.menu.item_find(xml.id_get(new Wx.Str("menu_about")));
            GLib.assert(this.menu_about.menu                         != null);

            this.splitter_vertical.win                    = this.win.win_find(new Wx.Str("splitter_vertical"));
            GLib.assert(this.splitter_vertical.win                    != null);
            this.panel_left.win                           = this.splitter_vertical.win.win_find(new Wx.Str("panel_left"));
            GLib.assert(this.panel_left.win                           != null);
            this.img_avatar.win                           = this.panel_left.win.win_find(new Wx.Str("img_avatar"));
            GLib.assert(this.img_avatar.win                           != null);
            this.lbl_name.win                             = this.panel_left.win.win_find(new Wx.Str("lbl_name"));
            GLib.assert(this.lbl_name.win                             != null);
            this.lbl_status.win                           = this.panel_left.win.win_find(new Wx.Str("lbl_status"));
            GLib.assert(this.lbl_status.win                           != null);
            this.img_state.win                            = this.panel_left.win.win_find(new Wx.Str("img_state"));
            GLib.assert(this.img_state.win                            != null);
            this.btn_bmp_state_user.win                   = this.panel_left.win.win_find(new Wx.Str("btn_bmp_state_user"));
            GLib.assert(this.btn_bmp_state_user.win                   != null);
            this.menu_state_user.menu                     = xml.menu_load(new Wx.Str("menu_state_user"));
            GLib.assert(this.menu_state_user.menu                     != null);
            this.menu_avail.menu                          = this.menu_state_user.menu.item_find(xml.id_get(new Wx.Str("menu_avail")));
            GLib.assert(this.menu_avail.menu                          != null);
            this.menu_away.menu                           = this.menu_state_user.menu.item_find(xml.id_get(new Wx.Str("menu_away")));
            GLib.assert(this.menu_away.menu                           != null);
            this.menu_busy.menu                           = this.menu_state_user.menu.item_find(xml.id_get(new Wx.Str("menu_busy")));
            GLib.assert(this.menu_busy.menu                           != null);
            this.notebook_contacts.win                    = this.panel_left.win.win_find(new Wx.Str("notebook_contacts"));
            GLib.assert(this.notebook_contacts.win                    != null);
            this.panel_buddies.win                        = this.notebook_contacts.win.win_find(new Wx.Str("panel_buddies"));
            GLib.assert(this.panel_buddies.win                        != null);
            this.lst_ctrl_buddies.win                     = this.panel_buddies.win.win_find(new Wx.Str("lst_ctrl_buddies"));
            GLib.assert(this.lst_ctrl_buddies.win                     != null);
            this.menu_buddies_.menu                       = xml.menu_load(new Wx.Str("menu_buddies_"));
            GLib.assert(this.menu_buddies_.menu                       != null);
            this.menu_add_buddy_.menu                     = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_add_buddy_")));
            GLib.assert(this.menu_add_buddy_.menu                     != null);
            this.menu_edit_buddy_.menu                    = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_edit_buddy_")));
            GLib.assert(this.menu_edit_buddy_                         != null);
            this.menu_remove_buddy_.menu                  = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_remove_buddy_")));
            GLib.assert(this.menu_remove_buddy_.menu                  != null);
            this.menu_about_buddy_.menu                   = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_about_buddy_")));
            GLib.assert(this.menu_about_buddy_.menu                   != null);
            this.menu_conv_buddy_.menu                    = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_conv_buddy_")));
            GLib.assert(this.menu_conv_buddy_.menu                    != null);
            this.menu_show_all_.menu                      = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_show_all_")));
            GLib.assert(this.menu_show_all_.menu                      != null);
            this.menu_hide_offline_but_.menu              = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_hide_offline_but_")));
            GLib.assert(this.menu_hide_offline_but_.menu              != null);
            this.menu_hide_offline_.menu                  = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_hide_offline_")));
            GLib.assert(this.menu_hide_offline_.menu                  != null);
            this.menu_show_buddies_.menu                  = this.menu_show_all_.menu.menu_get();
            GLib.assert(this.menu_show_buddies_.menu                  != null);
            this.menu_sort_add_lst_buddies_.menu          = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_buddies_")));
            GLib.assert(this.menu_sort_add_lst_buddies_.menu          != null);
            this.menu_sort_display_name_buddies_.menu     = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_buddies_")));
            GLib.assert(this.menu_sort_display_name_buddies_.menu     != null);
            this.menu_sort_last_msg_buddies_.menu         = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_last_msg_buddies_")));
            GLib.assert(this.menu_sort_last_msg_buddies_.menu         != null);
            this.menu_sort_state_buddies_.menu            = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_state_buddies_")));
            GLib.assert(this.menu_sort_state_buddies_.menu            != null);
            this.menu_sort_asc_buddies_.menu              = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_buddies_")));
            GLib.assert(this.menu_sort_asc_buddies_.menu              != null);
            this.menu_sort_desc_buddies_.menu             = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_buddies_")));
            GLib.assert(this.menu_sort_desc_buddies_.menu             != null);
            this.menu_sort_unread_top_buddies_.menu       = this.menu_buddies_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_unread_top_buddies_")));
            GLib.assert(this.menu_sort_unread_top_buddies_.menu       != null);
            this.menu_sort_buddies_.menu                  = this.menu_sort_add_lst_buddies_.menu.menu_get();
            GLib.assert(this.menu_sort_buddies_.menu                  != null);
            this.panel_conferences.win                    = this.notebook_contacts.win.win_find(new Wx.Str("panel_conferences"));
            GLib.assert(this.panel_conferences.win                    != null);
            this.lst_ctrl_conferences.win                 = this.panel_conferences.win.win_find(new Wx.Str("lst_ctrl_conferences"));
            GLib.assert(this.lst_ctrl_conferences.win                 != null);
            this.menu_conferences_.menu                   = xml.menu_load(new Wx.Str("menu_conferences_"));
            GLib.assert(this.menu_conferences_.menu                   != null);
            this.menu_add_conference_.menu                = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_add_conference_")));
            GLib.assert(this.menu_add_conference_.menu                != null);
            this.menu_edit_conference_.menu               = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_edit_conference_")));
            GLib.assert(this.menu_edit_conference_                    != null);
            this.menu_remove_conference_.menu             = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_remove_conference_")));
            GLib.assert(this.menu_remove_conference_.menu             != null);
            this.menu_about_conference_.menu              = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_about_conference_")));
            GLib.assert(this.menu_about_conference_.menu              != null);
            this.menu_conv_conference_.menu               = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_conv_conference_")));
            GLib.assert(this.menu_conv_conference_.menu               != null);
            this.menu_sort_add_lst_conferences_.menu      = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_conferences_")));
            GLib.assert(this.menu_sort_add_lst_conferences_.menu      != null);
            this.menu_sort_display_name_conferences_.menu = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_conferences_")));
            GLib.assert(this.menu_sort_display_name_conferences_.menu != null);
            this.menu_sort_last_msg_conferences_.menu     = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_last_msg_conferences_")));
            GLib.assert(this.menu_sort_last_msg_conferences_.menu     != null);
            this.menu_sort_asc_conferences_.menu          = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_conferences_")));
            GLib.assert(this.menu_sort_asc_conferences_.menu          != null);
            this.menu_sort_desc_conferences_.menu         = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_conferences_")));
            GLib.assert(this.menu_sort_desc_conferences_.menu         != null);
            this.menu_sort_unread_top_conferences_.menu   = this.menu_conferences_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_unread_top_conferences_")));
            GLib.assert(this.menu_sort_unread_top_conferences_.menu   != null);
            this.menu_sort_conferences_.menu              = this.menu_sort_add_lst_conferences_.menu.menu_get();
            GLib.assert(this.menu_sort_conferences_.menu              != null);
            this.choice_status_and_filter.win             = this.panel_left.win.win_find(new Wx.Str("choice_status_and_filter"));
            GLib.assert(this.choice_status_and_filter.win             != null);
            this.txt_status_and_filter.win                = this.panel_left.win.win_find(new Wx.Str("txt_status_and_filter"));
            GLib.assert(this.txt_status_and_filter.win                != null);
            this.btn_apply_status.win                     = this.panel_left.win.win_find(new Wx.Str("btn_apply_status"));
            GLib.assert(this.btn_apply_status.win                     != null);
            this.panel_right.win                          = this.splitter_vertical.win.win_find(new Wx.Str("panel_right"));
            GLib.assert(this.panel_right.win                          != null);
            this.notebook_main.win                        = this.panel_right.win.win_find(new Wx.Str("notebook_main"));
            GLib.assert(this.notebook_main.win                        != null);
            this.panel_tab1.win                           = this.notebook_main.win.win_find(new Wx.Str("panel_tab1"));
            GLib.assert(this.panel_tab1.win                           != null);
            this.txt_conv_buddy_tab1.win                  = this.panel_tab1.win.win_find(new Wx.Str("txt_conv_buddy_tab1"));
            GLib.assert(this.txt_conv_buddy_tab1.win                  != null);
            this.splitter_vertical_tab1.win               = this.panel_tab1.win.win_find(new Wx.Str("splitter_vertical_tab1"));
            GLib.assert(this.splitter_vertical_tab1.win               != null);
            this.panel_left_tab1.win                      = this.splitter_vertical_tab1.win.win_find(new Wx.Str("panel_left_tab1"));
            GLib.assert(this.panel_left_tab1.win                      != null);
            this.txt_conv_conference_tab1.win             = this.panel_left_tab1.win.win_find(new Wx.Str("txt_conv_conference_tab1"));
            GLib.assert(this.txt_conv_conference_tab1.win             != null);
            this.panel_right_tab1.win                     = this.splitter_vertical_tab1.win.win_find(new Wx.Str("panel_right_tab1"));
            GLib.assert(this.panel_right_tab1.win                     != null);
            this.lst_ctrl_members_tab1.win                = this.panel_right_tab1.win.win_find(new Wx.Str("lst_ctrl_members_tab1"));
            GLib.assert(this.lst_ctrl_members_tab1.win                != null);
            this.menu_members_.menu                       = xml.menu_load(new Wx.Str("menu_members_"));
            GLib.assert(this.menu_members_.menu                       != null);
            this.menu_members_action_in_buddy_list_.menu  = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_members_action_in_buddy_list_")));
            GLib.assert(this.menu_members_action_in_buddy_list_.menu  != null);
            this.menu_about_member_.menu                  = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_about_member_")));
            GLib.assert(this.menu_about_member_.menu                  != null);
            this.menu_sort_add_lst_members_.menu          = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_add_lst_members_")));
            GLib.assert(this.menu_sort_add_lst_members_.menu          != null);
            this.menu_sort_display_name_members_.menu     = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_display_name_members_")));
            GLib.assert(this.menu_sort_display_name_members_.menu     != null);
            this.menu_sort_asc_members_.menu              = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_asc_members_")));
            GLib.assert(this.menu_sort_asc_members_.menu              != null);
            this.menu_sort_desc_members_.menu             = this.menu_members_.menu.item_find(xml.id_get(new Wx.Str("menu_sort_desc_members_")));
            GLib.assert(this.menu_sort_desc_members_.menu             != null);
            this.menu_sort_members_.menu                  = this.menu_sort_add_lst_members_.menu.menu_get();
            GLib.assert(this.menu_sort_members_.menu                  != null);
            this.txt_filter_members.win                   = this.panel_right_tab1.win.win_find(new Wx.Str("txt_filter_member"));
            GLib.assert(this.txt_filter_members.win                   != null);
            this.txt_msg_tab1.win                         = this.panel_tab1.win.win_find(new Wx.Str("txt_msg_tab1"));
            GLib.assert(this.txt_msg_tab1.win                         != null);
            this.btn_send_tab1.win                        = this.panel_tab1.win.win_find(new Wx.Str("btn_send_tab1"));
            GLib.assert(this.btn_send_tab1.win                        != null);
            this.btn_av_audio_call_tab1.win               = this.panel_tab1.win.win_find(new Wx.Str("btn_av_audio_call_tab1"));
            GLib.assert(this.btn_av_audio_call_tab1.win               != null);
            this.btn_av_video_call_tab1.win               = this.panel_tab1.win.win_find(new Wx.Str("btn_av_video_call_tab1"));
            GLib.assert(this.btn_av_video_call_tab1.win               != null);
            this.btn_av_finish_tab1.win                   = this.panel_tab1.win.win_find(new Wx.Str("btn_av_finish_tab1"));
            GLib.assert(this.btn_av_finish_tab1.win                   != null);
            this.btn_av_accept_tab1.win                   = this.panel_tab1.win.win_find(new Wx.Str("btn_av_accept_tab1"));
            GLib.assert(this.btn_av_accept_tab1.win                   != null);
            this.btn_av_reject_tab1.win                   = this.panel_tab1.win.win_find(new Wx.Str("btn_av_reject_tab1"));
            GLib.assert(this.btn_av_reject_tab1.win                   != null);
            this.panel_tab2.win                           = this.notebook_main.win.win_find(new Wx.Str("panel_tab2"));
            GLib.assert(this.panel_tab2.win                           != null);
            this.check_lst_pub_keys_tab2.win              = this.panel_tab2.win.win_find(new Wx.Str("check_lst_pub_keys_tab2"));
            GLib.assert(check_lst_pub_keys_tab2.win                   != null);
            this.txt_msg_tab2.win                         = this.panel_tab2.win.win_find(new Wx.Str("txt_msg_tab2"));
            GLib.assert(txt_msg_tab2.win                              != null);
            this.btn_reject_tab2.win                      = this.panel_tab2.win.win_find(new Wx.Str("btn_reject_tab2"));
            GLib.assert(this.btn_reject_tab2.win                      != null);
            this.btn_accept_tab2.win                      = this.panel_tab2.win.win_find(new Wx.Str("btn_accept_tab2"));
            GLib.assert(this.btn_accept_tab2.win                      != null);
            this.panel_tab3.win                           = this.notebook_main.win.win_find(new Wx.Str("panel_tab3"));
            GLib.assert(this.panel_tab3.win                           != null);
            this.lst_ctrl_invites_tab3.win                = this.panel_tab3.win.win_find(new Wx.Str("lst_ctrl_invites_tab3"));
            GLib.assert(lst_ctrl_invites_tab3.win                     != null);
            this.btn_reject_tab3.win                      = this.panel_tab3.win.win_find(new Wx.Str("btn_reject_tab3"));
            GLib.assert(this.btn_reject_tab3.win                      != null);
            this.btn_accept_tab3.win                      = this.panel_tab3.win.win_find(new Wx.Str("btn_accept_tab3"));
            GLib.assert(this.btn_accept_tab3.win                      != null);

            this.status.win = this.win.win_find(new Wx.Str("status"));
            GLib.assert(this.status.win != null);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            int        id;
            Wx.Closure closure;

            id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.idle_handle_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.idle_handle(), closure);
            Wx.EventHandler.connect((void *) this.win, 0, 2147483647, Wx.EventHandler.idle_handle(), closure);

            //id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.closed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.win_close(), closure);

            //id      = this.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.move_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.move(), closure);

            id      = this.menu_edit_profile.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_edit_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_profile.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_profile.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_profile.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_change_profile.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_change_profile_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_profile.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_quit.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_quit_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_profile.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_add_buddy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_add_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_edit_buddy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_edit_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_remove_buddy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_remove_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_buddy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_conv_buddy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_conv_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_show_all.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_show_all_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_hide_offline_but.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_hide_offline_but_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_hide_offline.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_hide_offline_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_add_lst_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_display_name_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_last_msg_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_last_msg_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_state_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_state_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_asc_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_desc_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_unread_top_buddies.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_unread_top_buddies_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_buddies.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_add_conference.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_add_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_edit_conference.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_edit_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_remove_conference.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_remove_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_conference.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_conv_conference.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_conv_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_add_lst_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_display_name_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_last_msg_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_last_msg_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_asc_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_desc_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_unread_top_conferences.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_unread_top_conferences_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_members_action_in_buddy_list.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_members_action_in_buddy_list_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_conferences.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_member.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_member_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_sort_members.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_add_lst_members.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_members_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_sort_members.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_display_name_members.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_members_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_sort_members.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_asc_members.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_members_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_sort_members.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_desc_members.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_members_clicked_sig(fn, param, event); }, null);

            //Wx.EventHandler.connect((void *) this.menu_sort_members.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // It doesn't work
            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_plugins.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_plugins_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_tools.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_settings.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_settings_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_tools.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_Vika.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_Vika_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_help.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_help.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.btn_bmp_state_user.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_bmp_state_user_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.menu_avail.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_avail_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_state_user.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_away.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_away_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_state_user.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_busy.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_busy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_state_user.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.notebook_contacts.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.notebook_contacts_page_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_notebook_page_changed(), closure);

            //id      = this.notebook_contacts.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.notebook_contacts_page_changing_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_notebook_page_changing(), closure);

            id      = this.splitter_vertical.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.splitter_vertical_sash_changing_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_splitter_sash_pos_changing(), closure);

            id      = this.lst_ctrl_buddies.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_buddies_right_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_right_click(), closure);

            ///id      = this.lst_ctrl_buddies.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_buddies_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_sel(), closure);

            //id      = this.lst_ctrl_buddies.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_buddies_deselected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_desel(), closure);

            id      = this.menu_add_buddy_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_add_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_edit_buddy_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_edit_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_remove_buddy_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_remove_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_buddy_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_conv_buddy_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_conv_buddy_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_show_all_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_show_all_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_show_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_hide_offline_but_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_hide_offline_but_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_show_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_hide_offline_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_hide_offline_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_show_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_add_lst_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_display_name_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_last_msg_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_last_msg_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_state_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_state_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_asc_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_desc_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_unread_top_buddies_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_unread_top_buddies_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_buddies_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_buddies_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.lst_ctrl_conferences.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_conferences_right_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_right_click(), closure);

            ///id      = this.lst_ctrl_conferences.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_conferences_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_sel(), closure);

            //id      = this.lst_ctrl_conferences.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_conferences_deselected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_lst_item_desel(), closure);

            id      = this.menu_add_conference_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_add_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_edit_conference_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_edit_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_remove_conference_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_remove_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_conference_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_conv_conference_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_conv_conference_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.menu_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_add_lst_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_display_name_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_last_msg_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_last_msg_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_asc_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_desc_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_unread_top_conferences_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_unread_top_conferences_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_conferences_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_conferences_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.choice_status_and_filter.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.choice_status_and_filter_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_choice_sel(), closure);

            id      = this.txt_status_and_filter.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.txt_status_and_filter_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.btn_apply_status.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_apply_status_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_left.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.splitter_vertical_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.splitter_vertical_tab1_sash_changing_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_splitter_sash_pos_changing(), closure);

            id      = this.lst_ctrl_members_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_members_tab1_right_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_lst_item_right_click(), closure);

            //id      = this.lst_ctrl_members_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_members_tab1_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_lst_item_sel(), closure);

            //id      = this.lst_ctrl_members_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_members_tab1_deselected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_lst_item_desel(), closure);

            id      = this.menu_members_action_in_buddy_list_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_members_action_in_buddy_list_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_about_member_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_about_member_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure);

            id      = this.menu_sort_add_lst_members_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_add_lst_members_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_members_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_display_name_members_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_display_name_members_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_members_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_asc_members_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_asc_members_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_members_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.menu_sort_desc_members_.menu.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.menu_sort_desc_members_clicked_sig(fn, param, event); }, (void *) 1);

            Wx.EventHandler.connect((void *) this.menu_sort_members_.menu, id, id, Wx.EventHandler.cmd_menu_sel(), closure); // GNU/Linux
            Wx.EventHandler.connect((void *) this.menu_members_.menu,      id, id, Wx.EventHandler.cmd_menu_sel(), closure); // M$ Windows

            id      = this.txt_filter_members.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.txt_filter_members_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_right_tab1.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.txt_msg_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.txt_msg_tab1_changed_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_txt_upd(), closure);

            id      = this.btn_send_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_send_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_av_audio_call_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_av_audio_call_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_av_video_call_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_av_video_call_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_av_finish_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_av_finish_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_av_accept_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_av_accept_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_av_reject_tab1.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_av_reject_tab1_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab1.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.check_lst_pub_keys_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.check_lst_pub_keys_tab2_toggled_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_check_lst_box_toggled(), closure);

            //id      = this.check_lst_pub_keys_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.check_lst_pub_keys_tab2_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_lst_box_sel(), closure);

            id      = this.btn_reject_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_reject_tab2_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_accept_tab2.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_accept_tab2_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab2.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.lst_ctrl_invites_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_invites_tab3_selected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_lst_item_sel(), closure);

            //id      = this.lst_ctrl_invites_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.lst_ctrl_invites_tab3_deselected_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_lst_item_desel(), closure);

            id      = this.btn_reject_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_reject_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);

            id      = this.btn_accept_tab3.win.id_get();
            closure = new Wx.Closure((fn, param, event) => { return ctrl->frm_main.btn_accept_tab3_clicked_sig(fn, param, event); }, null);

            Wx.EventHandler.connect((void *) this.panel_tab3.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize keybindings.
        //
        // Setups keybindings for the widgets.
        private void keybindings_init(Wx.XMLResource xml)
        {
            // Emulate "Send" button click event with Ctrl-Enter
            int id    = this.btn_send_tab1.win.id_get();
            var entry = new Wx.AclEntry(Wx.AclEntry.flag_t.CTRL, Wx.AclEntry.key_code_t.RETURN, id);
            var tbl   = new Wx.AclTbl(1, &entry);

            this.txt_msg_tab1.win.acl_tbl_set(tbl);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Main window frame constructor.
         *
         * Initializes widgets.
         * Initializes events.
         * Initializes keybindings.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public FrmMain(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
            keybindings_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set new width of buddy list columns.
         *
         * It is placed on the left panel.
         */
        public void lst_ctrl_buddies_width_set()
        {
            /*
            // The code below doesn't work.
            // size_get() method gets wrong widget dimensions when program
            // starts =(

            size_t width;
            size_t height;

            this.lst_ctrl_buddies.size_get(out width, out height);

            size_t width_state      = this.lst_ctrl_buddies.width_get(LST_CTRL_BUDDIES_COL_STATE);
            size_t width_state_user = this.lst_ctrl_buddies.width_get(LST_CTRL_BUDDIES_COL_STATE_USER);

            int tmp = (int) width       -
                      (int) width_state -
                      (int) width_state_user;
            size_t width_display_name_and_status = tmp >= 0 ? tmp : 0;

            size_t width_display_name = width_display_name_and_status * 2 / 3;
            size_t width_status       = width_display_name_and_status - width_display_name;

            this.lst_ctrl_buddies.width_set(LST_CTRL_BUDDIES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_buddies.width_set(LST_CTRL_BUDDIES_COL_STATUS,       width_status);
            */

            size_t pos = this.splitter_vertical.sash_get();

            size_t width_state      = this.lst_ctrl_buddies.width_get(LST_CTRL_BUDDIES_COL_STATE);
            size_t width_state_user = this.lst_ctrl_buddies.width_get(LST_CTRL_BUDDIES_COL_STATE_USER);

#if __WXMSW__
            const size_t WIDTH_ERR = 32;
#else
            const size_t WIDTH_ERR = 20;
#endif

            int tmp = (int) pos              -
                      (int) width_state      -
                      (int) width_state_user -
                      (int) WIDTH_ERR;
            size_t width_display_name_and_status = tmp >= 0 ? tmp : 0;

            size_t width_display_name = width_display_name_and_status * 2 / 3;
            size_t width_status       = width_display_name_and_status - width_display_name;

            this.lst_ctrl_buddies.width_set(LST_CTRL_BUDDIES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_buddies.width_set(LST_CTRL_BUDDIES_COL_STATUS,       width_status);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set new width of conference list columns.
         *
         * It is placed on the left panel.
         */
        public void lst_ctrl_conferences_width_set()
        {
            /*
            // The code below doesn't work.
            // size_get() method gets wrong widget dimensions when program
            // starts =(

            size_t width;
            size_t height;

            this.lst_ctrl_conferences.size_get(out width, out height);

            size_t width_unread_state = this.lst_ctrl_conferences.width_get(LST_CTRL_CONFERENCES_COL_UNREAD_STATE);

            int tmp = (int) width - (int) width_unread_state;
            size_t width_display_name_and_cnt = tmp >= 0 ? tmp : 0;

            size_t width_display_name = width_display_name_and_cnt * 7 / 8;
            size_t width_cnt          = width_display_name_and_cnt - width_display_name;

            this.lst_ctrl_conferences.width_set(LST_CTRL_CONFERENCES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_conferences.width_set(LST_CTRL_CONFERENCES_COL_CNT,          width_cnt);
            */

            size_t pos = this.splitter_vertical.sash_get();

            size_t width_unread_state = this.lst_ctrl_conferences.width_get(LST_CTRL_CONFERENCES_COL_UNREAD_STATE);

#if __WXMSW__
            const size_t WIDTH_ERR = 32;
#else
            const size_t WIDTH_ERR = 20;
#endif

            int tmp = (int) pos                -
                      (int) width_unread_state -
                      (int) WIDTH_ERR;
            size_t width_display_name_and_cnt = tmp >= 0 ? tmp : 0;

            size_t width_display_name = width_display_name_and_cnt * 7 / 8;
            size_t width_cnt          = width_display_name_and_cnt - width_display_name;

            this.lst_ctrl_conferences.width_set(LST_CTRL_CONFERENCES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_conferences.width_set(LST_CTRL_CONFERENCES_COL_CNT,          width_cnt);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set new width of member list columns.
         *
         * It is placed on the tab 1 panel.
         */
        public void lst_ctrl_members_tab1_width_set()
        {
            size_t width;
            size_t height;

            this.lst_ctrl_members_tab1.size_get(out width, out height);

            size_t width_state = this.lst_ctrl_members_tab1.width_get(LST_CTRL_MEMBERS_COL_STATE_USER);

#if __WXMSW__
            const size_t WIDTH_ERR = 4;
#else
            const size_t WIDTH_ERR = 0;
#endif

            int tmp = (int) width       -
                      (int) width_state -
                      (int) WIDTH_ERR;
            size_t width_display_name = tmp >= 0 ? tmp : 0;

            this.lst_ctrl_members_tab1.width_set(LST_CTRL_MEMBERS_COL_DISPLAY_NAME, width_display_name);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to text control with service
         * message.
         *
         * It is placed on the tab 1 panel.
         *
         * @param msg      Message
         * @param datetime Date and time
         */
        public void txt_conv_tab1_append(string msg, GLib.DateTime datetime)
        {
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NORMAL, Wrapper.Ctrl.colour_t.GREY);
            this.txt_conv_buddy_tab1.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + msg + "\n");
#if __WXMSW__
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to text control with last exchanged
         * messages from/to a buddy.
         *
         * It is placed on the tab 1 panel.
         *
         * @param name     Message author
         * @param am_i     false: message is issued from peer side,
         *                 true:  message is issued from our side
         * @param msg      Message
         * @param datetime Date and time
         */
        public void txt_conv_buddy_tab1_append(string name,
                                               bool   am_i,
                                               string msg,
                                               GLib.DateTime datetime)
        {
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NORMAL, am_i ? Wrapper.Ctrl.colour_t.BLUE : Wrapper.Ctrl.colour_t.RED);
            this.txt_conv_buddy_tab1.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + name + ":");
#if __WXMSW__
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
            this.txt_conv_buddy_tab1.append(" " + msg + "\n");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append new message to text control with last exchanged
         * messages from/to a conference.
         *
         * It is placed on the left panel.
         *
         * @param name     Message author
         * @param fg       Author foreground colour
         * @param bg       Author background colour.
         *                 Not used by now.
         * @param msg      Message
         * @param datetime Date and time
         */
        public void txt_conv_conference_tab1_append(string name,
                                                    uint   fg,
                                                    uint   bg,
                                                    string msg,
                                                    GLib.DateTime datetime)
        {
            this.txt_conv_conference_tab1.style_def_set(Wrapper.Ctrl.font_t.NORMAL, view->db_colour_convert(fg));
            this.txt_conv_conference_tab1.append("(" + datetime.format("%Y-%m-%d %H:%M:%S") + ") " + name + ":");
#if __WXMSW__
            this.txt_conv_conference_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.BLACK);
#else
            this.txt_conv_conference_tab1.style_def_set(Wrapper.Ctrl.font_t.NONE, Wrapper.Ctrl.colour_t.NONE);
#endif
            this.txt_conv_conference_tab1.append(" " + msg + "\n");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append typing information to input text control with last
         * exchanged messages from/to a buddy.
         *
         * It is placed on the tab 1 panel.
         *
         * @param name Message author
         *
         * @return Length of appended typing information in characters
         */
        public size_t txt_conv_buddy_tab1_append_typing(string name)
        {
            string typing = name + _(" is typing a message...");

            this.txt_conv_buddy_tab1.style_def_set(Wrapper.Ctrl.font_t.SMALL, Wrapper.Ctrl.colour_t.GREY);
            this.txt_conv_buddy_tab1.append(typing);

            return typing.char_count();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert information to check list box with public keys of
         * buddy requests
         *
         * It is placed on the tab 2 panel.
         *
         * @param key      Public key
         * @param datetime Date and time
         */
        public void check_lst_pub_keys_tab2_insert(string key, GLib.DateTime datetime)
        {
#if __WXGTK__
            // Can't simply insert a key.
            //
            // Without this hack the callback for selecting element in
            // this check list control won't be called sometimes.
            //string val = key + " (" + datetime.format("%A, %e %B %Y %H:%M:%S") + ")";
            //view->frm_main.check_lst_pub_keys_tab2.insert(0, val);

            view->frm_main.check_lst_pub_keys_tab2.append("");
            size_t cnt = view->frm_main.check_lst_pub_keys_tab2.cnt_get();

            for (int idx = (int) cnt - 2; idx >= 0; idx--)
            {
                string val = view->frm_main.check_lst_pub_keys_tab2.val_get(idx);
                view->frm_main.check_lst_pub_keys_tab2.val_set(idx + 1, val);
            }

            string val = key + " (" + datetime.format("%A, %e %B %Y %H:%M:%S") + ")";
            view->frm_main.check_lst_pub_keys_tab2.val_set(0, val);
            ctrl->frm_main.txt_msg_tab2_upd();
#else
            ERROR
#endif
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set new width of invite list columns.
         *
         * It is placed on the tab 3 panel.
         */
        public void lst_ctrl_invites_tab3_width_set()
        {
            /*
            // The code below doesn't work.
            // size_get() method gets wrong widget dimensions when program
            // starts =(

            size_t width;
            size_t height;

            this.lst_ctrl_invites_tab3.size_get(out width, out height);

            size_t width_display_name_and_cookie = width;

            size_t width_display_name = width_display_name_and_cookie * 1 / 3;
            size_t width_cookie       = width_display_name_and_cookie - width_display_name;

            this.lst_ctrl_invites_tab3.width_set(LST_CTRL_INVITES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_invites_tab3.width_set(LST_CTRL_INVITES_COL_COOKIE,       width_cookie);
            */

            size_t width;
            size_t height;

            this.size_get(out width, out height);

            size_t pos = this.splitter_vertical.sash_get();

#if __WXMSW__
            const size_t WIDTH_ERR = 52;
#else
            const size_t WIDTH_ERR = 25;
#endif

            int tmp = (int) width - (int) pos - (int) WIDTH_ERR;
            size_t width_all_cols = tmp >= 0 ? tmp : 0;

            size_t width_display_name = width_all_cols * 1 / 3;
            size_t width_datetime     = width_all_cols * 1 / 3;
            size_t width_cookie       = width_all_cols - width_display_name - width_datetime;

            this.lst_ctrl_invites_tab3.width_set(LST_CTRL_INVITES_COL_DISPLAY_NAME, width_display_name);
            this.lst_ctrl_invites_tab3.width_set(LST_CTRL_INVITES_COL_COOKIE,       width_cookie);
            this.lst_ctrl_invites_tab3.width_set(LST_CTRL_INVITES_COL_DATETIME,     width_datetime);
        }
    }
}


