/*
 *    txt_ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.TxtCtrl class of
     * wxVala/wxWidgets.
     * It is input text control.
     */
    public class TxtCtrl : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // This event occurs when GUI is ready to call
        // Wx.TxtCtrl.pos_show() method.
        //
        // Can't call Wx.TxtCtrl.pos_show() in this.pos_show()
        // straightaway, it doesn't work, but call it in the end of the
        // GUI loop. For this reason this event handler is created.
        private int rdy_to_show_pos;
        private Wx.EventHandler? event_handler = null;
        private Wx.Event event;
        private ulong pos;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Select the text starting at the first position up to (but
        // not including) the character at the last position.
        // If both parameters are equal to -1 all text in the control is
        // selected.
        private static void *pos_show_(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            unowned TxtCtrl    txt_   = (TxtCtrl)    param;
            unowned Wx.TxtCtrl txt    = (Wx.TxtCtrl) txt_.win;
            unowned Wx.Win     parent = txt.parent_get();

            // Scroll control to this position
            txt.pos_show((long) txt_.pos);
            parent.event_handler_pop(false // Do not delete handler, it may be reused later
                                    );

            return null;
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Input text control constructor.
         *
         * It does nothing by now.
         */
        public TxtCtrl()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set maximum length
         *
         * @param max_len Maximum length
         */
        public void max_len_set(size_t max_len)
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.max_len_set((long) max_len);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get content length
         *
         * @return Length in characters
         */
        public ulong len_get()
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;
                    uint       cnt = txt.num_of_lines_get();
                    ulong      len = cnt - 1;

            for (uint line = 0; line < cnt; line++)
                len += txt.line_len_get(line);

            return len;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get value
         *
         * @return Value
         */
        public string val_get()
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;
                    Wx.Str     str = txt.val_get();

            return str.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value
         *
         * @param val Value
         */
        public void val_set(string val)
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.val_set(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append value
         *
         * @param val Value
         */
        public void append(string val)
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.append(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove value
         *
         * @param from First character
         * @param to   Last character (not including)
         */
        public void remove(ulong from, ulong to)
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.remove((long) from, (long) to);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Clear value
         */
        public void clr()
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.clr();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select the text starting at the first position up to (but
         * not including) the character at the last position.
         *
         * If both parameters are equal to -1 all text in the control is
         * selected.
         *
         * @param from First position
         * @param to   Last position
         */
        public void sel_set(long from, long to)
        {
            unowned Wx.TxtCtrl txt = (Wx.TxtCtrl) this.win;

            txt.sel_set(from, to);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Make the line containing the given position visible
         *
         * @param pos Position
         */
        public void pos_show(ulong pos)
        {
            // Can't call this method right now, because it doesn't always
            // work.
            // (For example, no scrolling if text was just appended.)
            //txt.pos_show(pos);

            // Instead we generate event which will set position in callback.
            // Initialize event handler.
            // It's ugly hack, read above, why it's needed.
            if (this.event_handler == null)
            {
                int id      = this.win.id_get();
                var closure = new Wx.Closure(pos_show_, this);

                this.rdy_to_show_pos = Wx.Event.new_event_type_create();
                this.event_handler   = new Wx.EventHandler();
                this.event           = new Wx.Event(this.rdy_to_show_pos, id);

                Wx.EventHandler.connect((void *) this.win.parent_get(), id, id, this.rdy_to_show_pos, closure);
            }

            unowned Wx.Win parent = this.win.parent_get();
            parent.event_handler_push(this.event_handler);
            this.pos = pos;
            event_handler.pending_event_add(this.event);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set style
         *
         * @param from   Begin position
         * @param to     End position
         * @param font   Font
         * @param colour Colour
         */
        public void style_set(ulong from,
                              ulong to,
                              Ctrl.font_t font,
                              Ctrl.colour_t colour)

            requires (font == Ctrl.font_t.NONE   ||
                      font == Ctrl.font_t.ITALIC ||
                      font == Ctrl.font_t.NORMAL ||
                      font == Ctrl.font_t.SMALL  ||
                      font == Ctrl.font_t.SWISS)

            requires (colour == Ctrl.colour_t.NONE  ||
                      colour == Ctrl.colour_t.BLACK ||
                      colour == Ctrl.colour_t.WHITE ||
                      colour == Ctrl.colour_t.RED   ||
                      colour == Ctrl.colour_t.BLUE  ||
                      colour == Ctrl.colour_t.GREEN ||
                      colour == Ctrl.colour_t.CYAN  ||
                      colour == Ctrl.colour_t.GREY)
        {
            unowned Wx.Font    font_   = font == Ctrl.font_t.NONE ? Wx.Font.null_get() :
                                                                    Wx.Font.stock_get(font);
            unowned Wx.Colour  colour_ = colour == Ctrl.colour_t.NONE ? Wx.Colour.null_get() :
                                                                        Wx.Colour.stock_get(colour);
            unowned Wx.TxtCtrl txt     = (Wx.TxtCtrl) this.win;

            var attr = new Wx.TxtAttr(colour_,
                                      Wx.Colour.null_get(),
                                      font_);

            txt.style_set((long) from,
                          (long) to,
                          attr);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set default style
         *
         * @param font   Font
         * @param colour Colour
         */
        public void style_def_set(Ctrl.font_t font, Ctrl.colour_t colour)

            requires (font == Ctrl.font_t.NONE   ||
                      font == Ctrl.font_t.ITALIC ||
                      font == Ctrl.font_t.NORMAL ||
                      font == Ctrl.font_t.SMALL  ||
                      font == Ctrl.font_t.SWISS)

            requires (colour == Ctrl.colour_t.NONE  ||
                      colour == Ctrl.colour_t.BLACK ||
                      colour == Ctrl.colour_t.WHITE ||
                      colour == Ctrl.colour_t.RED   ||
                      colour == Ctrl.colour_t.BLUE  ||
                      colour == Ctrl.colour_t.GREEN ||
                      colour == Ctrl.colour_t.CYAN  ||
                      colour == Ctrl.colour_t.GREY)
        {
            unowned Wx.Font    font_   = font == Ctrl.font_t.NONE ? Wx.Font.null_get() :
                                                                    Wx.Font.stock_get(font);
            unowned Wx.Colour  colour_ = colour == Ctrl.colour_t.NONE ? Wx.Colour.null_get() :
                                                                        Wx.Colour.stock_get(colour);
            unowned Wx.TxtCtrl txt     = (Wx.TxtCtrl) this.win;

            var attr = new Wx.TxtAttr(colour_,
                                      Wx.Colour.null_get(),
                                      font_);

            txt.def_style_set(attr);
        }
    }
}

