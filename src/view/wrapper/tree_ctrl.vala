/*
 *    tree_ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.TreeCtrl class of
     * wxVala/wxWidgets.
     * It is tree control.
     */
    public class TreeCtrl : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Dummy function
        private static Wx.Closure.fn_t dummy = (fn, param, event) =>
        {
            return null;
        };

        // Root element
        private Item root = null;

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /**
         * Element item
         */
        [Compact]
        public class Item : Wx.TreeItemId
        {
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tree control constructor.
         *
         * It does nothing by now.
         */
        public TreeCtrl()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get value
         *
         * @param item Element item
         *
         * @return Value
         */
        public string val_get(Item item)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;
                    Wx.Str      val  = tree.item_txt_get(item);

            return val.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append value
         *
         * @param parent Parent element item or null to append to root
         *               item
         * @param val    Value
         * @param meta   Meta tag
         *
         * @return New element item
         */
        public Item append(Item? parent, string val, void *meta)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            // Create root element
            if (this.root == null)
            {
                Wx.Closure *closure = new Wx.Closure(dummy, null); 
                this.root           = new Item();

                tree.root_add(new Wx.Str(""),
                              -1, // Image is not used
                              -1, // Image is not used
                              closure,
                              this.root);
            }

            if (parent == null)
                parent = this.root;

            // Append value
            Wx.Closure *closure = new Wx.Closure(dummy, meta); 
            Item       item     = new Item();

            tree.item_append((Wx.TreeItemId) parent,
                             new Wx.Str(val),
                             -1, // Image is not used
                             -1, // Image is not used
                             closure,
                             (Wx.TreeItemId) item);

            return item;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove value
         *
         * @param item Element item
         */
        public void remove(Item item)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            tree.del(item);

            if (cnt_get() == 1)
            {
                tree.all_items_del();
                this.root = null;
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of elements
         *
         * @return Number of elements
         */
        public size_t cnt_get()
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            return (size_t) tree.cnt_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get element item of selected value
         *
         * @return Element item or null
         */
        public Item? sel_get()
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            Item item = new Item();
            Item root = root_get();

            tree.sel_get(item);

            bool is_root = item.val_get() == root.val_get();
            if (is_root)
                return null;

            return item;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select item
         */
        public void sel(Item item)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            tree.item_sel(item);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Unselect item
         */
        public void unsel()
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            tree.unsel();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get root element
         *
         * @return Element item
         */
        public Item root_get()
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            Item item = new Item();
            tree.root_item_get(item);

            return item;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get meta tag of specified item
         *
         * @param item Element item
         *
         * @return Meta tag
         */
        public void *meta_get(Item item)
        {
            unowned Wx.TreeCtrl tree    = (Wx.TreeCtrl) this.win;
            unowned Wx.Closure  closure = tree.client_closure_get(item);

            return closure.data_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Expand item
         *
         * @param item Element item
         */
        public void expand(Item item)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            tree.expand(item);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove all values
         */
        public void clr()
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            tree.all_items_del();
            this.root = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Check whether item has children
         *
         * @param item Element item
         *
         * @return false: item has no children,
         *         true:  item has children
         */
        public bool item_child_has(Item item)
        {
            unowned Wx.TreeCtrl tree = (Wx.TreeCtrl) this.win;

            return tree.item_child_has(item);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Check whether item is valid
         *
         * @param item Element item
         *
         * @return false: item is valid,
         *         true:  item is invalid
         */
        public static bool valid_is(Item item)
        {
            return item.ok_is();
        }
    }
}

