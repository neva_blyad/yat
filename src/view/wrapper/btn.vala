/*
 *    btn.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.Btn class of wxVala/wxWidgets.
     * It is button.
     */
    public class Btn : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Button constructor.
         *
         * It does nothing by now.
         */
        public Btn()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Create and show button
         *
         * @param parent Parent window
         * @param val    Value
         */
        public void create(Win parent, string val)
        {
            Wx.Btn *btn = new Wx.Btn(parent.win,
                                     Wx.ID_ANY,
                                     new Wx.Str(val),
                                     0,  // x coordinate
                                     0,  // y coordinate
                                     -1, // Width
                                     -1, // Height
                                     0   // Default style
                                    );
            this.win = btn;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Delete button
          */
        public void del()
        {
            Wx.Btn *btn = this.win;

            delete btn;
        }
    }
}

