/*
 *    ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.Ctrl class of wxVala/wxWidgets.
     * It is basic control.
     */
    public abstract class Ctrl : Win
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * Font
         */
        public enum font_t
        {
            NONE   = -1,
            ITALIC =  0,
            NORMAL =  1,
            SMALL  =  2,
            SWISS  =  3,
        }

        /*
         * Colour
         */
        public enum colour_t
        {
            NONE  = -1,
            BLACK =  0,
            WHITE =  1,
            RED   =  2,
            BLUE  =  3,
            GREEN =  4,
            CYAN  =  5,
            GREY  =  6,
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Set label
         *
         * @param val Value
         */
        public override void lbl_set(string val)
        {
            unowned Wx.Ctrl ctrl = (Wx.Ctrl) this.win;

            ctrl.lbl_set(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set label using markup
         *
         * @param val Value
         */
        public void lbl_markup_set(string val)
        {
            unowned Wx.Ctrl ctrl = (Wx.Ctrl) this.win;

            ctrl.lbl_markup_set(new Wx.Str(val));
        }
    }
}

