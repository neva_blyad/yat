/*
 *    choice.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.Choice class of
     * wxVala/wxWidgets.
     * It is choice.
     */
    public class Choice : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Choice constructor.
         *
         * It does nothing by now.
         */
        public Choice()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append value
         *
         * @param val Value
         */
        public void append(string val)
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            choice.append(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove value
         *
         * @param idx Index
         */
        public void remove(uint idx)
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            choice.del((int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of values
         *
         * @return Number of values
         */
        public size_t cnt_get()
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            return (size_t) choice.cnt_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get value
         *
         * @param idx Index
         *
         * @return Value
         */
        public string val_get(uint idx)
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;
                    Wx.Str    val     = choice.str_get((int) idx);

            return val.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value
         *
         * @param idx Index
         * @param val Value
         */
        public void val_set(uint idx, string val)
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            choice.str_set((int) idx, new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Clear values
         */
        public void clr()
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            choice.clr();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get index of selected choice
         *
         * @return idx Index or Win.FOUND_NOT if no value selected
         */
        public int sel_get()
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            return choice.sel_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select choice using its index
         *
         * @param idx Index
         */
        public void sel_set(uint idx)
        {
            unowned Wx.Choice choice = (Wx.Choice) this.win;

            choice.sel_set((int) idx);
        }
    }
}

