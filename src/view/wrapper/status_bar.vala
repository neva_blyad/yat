/*
 *    status_bar.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.StatusBar class of
     * wxVala/wxWidgets.
     * It is status bar.
     */
    public class StatusBar : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * List control constructor.
         *
         * It does nothing by now.
         */
        public StatusBar()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set field columns and their widths
         *
         * @param widths Column widths
         */
        public void fields_set(size_t[] widths)
        {
            unowned Wx.StatusBar status = (Wx.StatusBar) this.win;

            var widths_int = new int[widths.length];
            for (int col = 0; col < widths_int.length; col++)
                widths_int[col] = (int) widths[col];

            status.fields_cnt_set(widths_int);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value
         *
         * @param col Column
         * @param val Value
         */
        public void val_set(uint col, string val)
        {
            unowned Wx.StatusBar status = (Wx.StatusBar) this.win;

            status.status_txt_set(new Wx.Str(val), (int) col);
        }
    }
}

