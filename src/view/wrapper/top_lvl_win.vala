/*
 *    top_lvl_win.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.TopLvlWin class of
     * wxVala/wxWidgets.
     * It is top level window.
     */
    public class TopLvlWin : Win
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Top level window constructor.
         *
         * It does nothing by now.
         */
        public TopLvlWin()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set icon
         *
         * @param filepath Filepath
         */
        public void icon_set(string filepath)
        {
            unowned Wx.TopLvlWin top_lvl_win = (Wx.TopLvlWin) this.win;

            // Load the icon bundle from a file
            var icons = new Wx.IconBundle(new Wx.Str(filepath), Wx.bmp_type_t.ICO);

            // Set it to the window
            top_lvl_win.icons_set(icons);
        }
    }
}

