/*
 *    static_bmp.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.StaticBMP class of
     * wxVala/wxWidgets.
     * It is static bitmap.
     */
    public class StaticBMP : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Static bitmap constructor.
         *
         * It does nothing by now.
         */
        public StaticBMP()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set image.
         *
         * Do not use filepath and blob simultaneously.
         *
         * @param filepath Filepath to image
         * @param blob     Image blob
         */
        public void img_set(string? filepath, uint8[]? blob)
            requires ((filepath == null || blob != null) ||
                      (filepath != null || blob == null))
        {
            Wx.BMP bmp;

            unowned Wx.StaticBMP static_bmp = (Wx.StaticBMP) this.win;

            // Load the bitmap from a file or a blob
            if (filepath == null)
            {
                // Load the bitmap from a blob.
                //
                // Can't use Wx.BMP constructor directly, because it can load
                // only monochrome icons from memory according to wxWidgets
                // docs. So do several steps.
                //
                // First, create memory input stream.
                var stream = new Wx.MemInputStream(blob);

                // Second, load it to image
                var  img = new Wx.Img();
                bool res = img.stream_load((Wx.InputStream) stream,
                                           Wx.bmp_type_t.PNG,
                                           -1 // Choose the default image in the case that the image file contains multiple images
                                          );
                GLib.assert(res);

                // Then convert it to bitmap
                img.to_bmp_convert(out bmp);
            }
            else
            {
                // Load the bitmap from a file
                bmp = new Wx.BMP(new Wx.Str(filepath), Wx.bmp_type_t.GIF);
            }

            // Set it to control
            static_bmp.bmp_set(bmp);

            // Update sizer layout
            unowned Wx.Sizer sizer = static_bmp.containing_sizer_get();
            if (sizer != null)
                sizer.layout_set();
        }
    }
}

