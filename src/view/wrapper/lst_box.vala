/*
 *    lst_box.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.LstBox class of
     * wxVala/wxWidgets.
     * It is list box.
     */
    public class LstBox : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * List box constructor.
         *
         * It does nothing by now.
         */
        public LstBox()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of elements
         *
         * @return Number of elements
         */
        public size_t cnt_get()
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            return (size_t) lst_box.cnt_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get value
         *
         * @param idx Index
         *
         * @return Value
         */
        public string val_get(uint idx)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;
                    Wx.Str    val     = lst_box.str_get((int) idx);

            return val.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value
         *
         * @param idx Index
         * @param val Value
         */
        public void val_set(uint idx, string val)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.str_set((int) idx, new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Insert element
         *
         * @param idx Index
         * @param val Value
         */
        public void insert(uint idx, string val)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.insert(new Wx.Str(val), (int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Append element
         *
         * @param val Value
         */
        public void append(string val)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.append(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove element
         *
         * @param idx Index
         */
        public void remove(uint idx)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.del((int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove all elements
         */
        public void clr()
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.clr();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get index of selected item
         *
         * @return Index or Win.FOUND_NOT if no value selected
         */
        public int sel_elem_get()
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            return lst_box.sel_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get index of selected items
         *
         * @return The number of selections
         */
        public size_t selections_elem_get(uint[] sel)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            return lst_box.selections_get((int[]) sel);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get state of element
         *
         * @param idx Index
         *
         * @return false: element is deselected,
         *         true:  element is selected
         */
        public bool sel_get(uint idx)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            return lst_box.sel_is((int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set state to element
         *
         * @param idx  Index
         * @param flag false: deselect the element,
         *             true:  select the element
         */
        public void sel_set(uint idx, bool flag)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.sel_set((int) idx, flag);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get meta data associated with element
         *
         * @param idx Index
         *
         * @return Meta data
         */
        public void *meta_get(uint idx)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            return lst_box.client_data_get((int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set meta data associated with element
         *
         * @param idx  Index
         * @param meta Meta data
         */
        public void meta_set(uint idx, void *meta)
        {
            unowned Wx.LstBox lst_box = (Wx.LstBox) this.win;

            lst_box.client_data_set((int) idx, meta);
        }
    }
}

