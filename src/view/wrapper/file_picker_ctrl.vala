/*
 *    file_picker_ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.FilePickerCtrl class of
     * wxVala/wxWidgets.
     * It is file picker.
     */
    public class FilePickerCtrl : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * FilePickerCtrl constructor.
         *
         * It does nothing by now.
         */
        public FilePickerCtrl()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
          * The full path (directory and filename) of the selected file
          *
          * @return Path
          */
        public string path_get()
        {
            unowned Wx.FilePicker file_picker = (Wx.FilePicker) this.win;

            return file_picker.path_get().utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Set the absolute path of the currently selected file
          *
          * @param path Path
          */
        public void path_set(string path)
        {
            unowned Wx.FilePicker file_picker = (Wx.FilePicker) this.win;

            file_picker.path_set(new Wx.Str(path));
        }
    }
}

