/*
 *    lst_ctrl.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.LstCtrl class of
     * wxVala/wxWidgets.
     * It is list control.
     */
    public class LstCtrl : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Optimization mode
        private mode_t mode;

        // Icon size
        private size_t icon_width;
        private size_t icon_height;

        // Image list
        private Wx.ImgLst *img_lst { private get; private set; }

        // Icons.
        //
        // This array list stores image indexes of used in list control
        // icons.
        //
        // Element of this array list is image index in image list.
        // Wx.FOUND_NOT means the cell doesn't contain image.
        private Gee.ArrayList<Gee.ArrayList<int>> icons_idx;

        // Icons.
        //
        // This array list stores hashes and filenames of used in list
        // control icons.
        //
        // Element of this array list is hash/filename.
        private Gee.ArrayList<string> icon_hash_or_filepath;

        /*----------------------------------------------------------------------------*/
        /*                               Private Types                                */
        /*----------------------------------------------------------------------------*/

        // State of row
        [Flags]
        private enum state_t
        {
            NONE = 0,      // Unselected
            SEL  = 1 << 2, // Selected
        }

        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * Optimization mode
         */
        public enum mode_t
        {
            OPTIMIZE_FOR_SPEED,  // Use as many copies of icons as needed
            OPTIMIZE_FOR_MEMORY, // Use only one copy of each icon in memory. TODO: this mode has not been tested.
        }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Check if it possible to delete icon from cell.
        // No other cells should use it.
        private bool icon_del_check(uint   row,        uint   col,
                                    bool   remove_row, bool   remove_col,
                                    size_t row_cnt,    size_t col_cnt)
        {
            bool del_permission;

            // Get icon's index in image list.
            // Do nothing if cell doesn't contain icon.
            int idx = this.icons_idx[(int) row][(int) col];

            if (idx == Wx.FOUND_NOT)
                del_permission = false;
            else
            {
                // Scan the entire list control.
                // Find the cells with the same icon.
                // Thus we can decide can we delete this icon or not if it used
                // in some other place.
                del_permission = true;

                for (uint row_old = 0; row_old < row_cnt; row_old++)
                {
                    // Ignore removed row
                    if (remove_row && row_old == row)
                        continue;

                    // Walk through each column
                    for (uint col_old = 0; col_old < col_cnt; col_old++)
                    {
                        // Ignore removed column
                        if (remove_col && col_old == col)
                            continue;

                        // Ignore the cell which is to be deleted
                        if (row_old == row &&
                                col_old == col)
                            continue;

                        // Get icon's index of the next cell
                        int idx_old = this.icons_idx[(int) row_old][(int) col_old];

                        // Mark not to remove icon if it's used in other cell.
                        // Stop scanning in this case.
                        if (idx_old == idx)
                        {
                            del_permission = false;
                            break;
                        }
                    }

                    // Stop scanning if cell using this icon found
                    if (!del_permission)
                        break;
                }
            }

            return del_permission;
        }

        /*----------------------------------------------------------------------------*/

        // Delete icon from cell
        private void icon_del(uint   row,        uint   col,
                              bool   remove_row, bool   remove_col,
                              size_t row_cnt,    size_t col_cnt)
        {
            // Get icon's index in image list.
            // Do nothing if cell doesn't contain icon.
            int idx = this.icons_idx[(int) row][(int) col];

            if (idx != Wx.FOUND_NOT)
            {
                // Scan the entire list control.
                // Shift icon indexes in all cells.
                for (uint row_old = 0; row_old < row_cnt; row_old++)
                {
                    // Ignore removed row
                    //if (remove_row && row_old == row)
                    //    continue;

                    // Walk through each column
                    for (uint col_old = 0; col_old < col_cnt; col_old++)
                    {
                        // Ignore removed column
                        //if (remove_col && col_old == col)
                        //    continue;

                        // Ignore the cell which is to be deleted
                        //if (row_old == row &&
                        //        col_old == col)
                        //    continue;

                        // Get content of the next cell
                        var lst_item = new Wx.LstItem();
                        unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

                        lst_item.id_set((int) row_old);
                        lst_item.col_set((int) col_old);
                        lst_ctrl.item_get(lst_item);

                        // Get icon's index of the next cell
                        int idx_old = this.icons_idx[(int) row_old][(int) col_old];

                        if (idx_old > idx)
                        {
                            // Set new index to it
                            idx_old--;
                            this.icons_idx[(int) row_old][(int) col_old] = idx_old;

                            // Update cell content with new image index
                            lst_item.img_set((int) idx_old);
                            lst_ctrl.item_from_info_set(lst_item);
                        }
                        else if (idx_old == idx)
                        {
                            // Update cell content with new image index
                            lst_item.img_set(Wx.FOUND_NOT);
                            lst_ctrl.item_from_info_set(lst_item);
                        }
                    }
                }

                // Remove the icon from image list.
                // Remove hash/filepath.
                bool res = this.img_lst->remove(idx);
                GLib.assert(res);
                if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
                    this.icon_hash_or_filepath.remove_at(idx);

            }
        }

        /*----------------------------------------------------------------------------*/

        // Load the icon from a file or a blob
        private Wx.Icon? icon_get(string? icon_filepath, uint8[]? icon_blob)
        {
            // This icon is not used yet.
            // Load the icon from a file or a blob.
            Wx.Icon icon;

            if (icon_filepath == null)
            {
                // Load the icon from a blob.
                //
                // Can't use Wx.Icon constructor directly, because it can load
                // only monochrome icons from memory according to wxWidgets
                // docs. So do several steps.
                //
                // First, create memory input stream.
                var stream = new Wx.MemInputStream(icon_blob);

                // Second, load it to image
                var  img = new Wx.Img();
                bool res = img.stream_load((Wx.InputStream) stream,
                                           Wx.bmp_type_t.PNG,
                                           -1 // Choose the default image in the case that the image file contains multiple images
                                          );
                //GLib.assert(res);
                if (!res)
                    return null;
                //img.rescale((int) this.icon_width, (int) this.icon_height);

                // Then convert it to bitmap
                Wx.BMP bmp;
                img.to_bmp_convert(out bmp);

                // Finally, we can convert it to icon
                icon = new Wx.Icon();
                icon.bmp_from_copy(bmp);
            }
            else
            {
                // Load the icon from a file
                icon = new Wx.Icon.create_load(new Wx.Str(icon_filepath),
                                               Wx.bmp_type_t.GIF,
                                               (int) this.icon_width,
                                               (int) this.icon_height);
            }

            if (!icon.ok_is())
                return null;

            return icon;
        }

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        // Default column width
        public const size_t DEFAULT_WIDTH = 240;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * List box constructor
         *
         * @param mode        Optimization mode
         * @param icon_width  Icon width
         * @param icon_height Icon height
         */
        public LstCtrl(mode_t mode, size_t icon_width, size_t icon_height)
            requires (mode == mode_t.OPTIMIZE_FOR_SPEED ||
                      mode == mode_t.OPTIMIZE_FOR_MEMORY)
        {
            // Initialize optimization mode
            this.mode = mode;

            // Initialize icon size
            this.icon_width  = icon_width;
            this.icon_height = icon_height;

            // Initialize image list
            this.img_lst = null;

            // Initialize array lists
            this.icons_idx = new Gee.ArrayList<Gee.ArrayList<int>>();
            if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
                this.icon_hash_or_filepath = new Gee.ArrayList<string>();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set icon size
         *
         * @param icon_width  Icon width
         * @param icon_height Icon height
         */
        public void icon_size_set(size_t icon_width, size_t icon_height)
        {
            // Set icon size
            this.icon_width  = icon_width;
            this.icon_height = icon_height;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * List box deconstructor
         *
         */
        ~LstCtrl()
        {
            // Deinitialize image list
            if (this.img_lst != null)
                delete this.img_lst;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Add row
         *
         * @param row Row
         */
        public void row_add(uint row)
        {
            // Add row
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            lst_ctrl.item_with_data_insert((int) row, new Wx.Str(""));

            // Add row to array with image indexes
            var icons_row  = new Gee.ArrayList<int>();
            size_t col_cnt = col_cnt_get();
            for (uint col = 0; col < col_cnt; col++)
                icons_row.add(Wx.FOUND_NOT);
            this.icons_idx.insert((int) row, icons_row);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove row
         *
         * @param row Row
         */
        public void row_remove(uint row)
        {
            // Optimize for memory?
            // Delete icons in row.
            size_t row_cnt = row_cnt_get();
            size_t col_cnt = col_cnt_get();

            if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
            {
                // Memory saving.
                // Delete icons.
                for (uint col = 0; col < col_cnt; col++)
                {
                    if (icon_del_check(row,
                                       col,
                                       true,  // Row will be removed
                                       false, // Column will not be removed
                                       row_cnt,
                                       col_cnt))
                        icon_del(row, col, true, false, row_cnt, col_cnt);
                }
            }
            else
            {
                // CPU saving.
                // Delete icons.
                for (uint col = 0; col < col_cnt; col++)
                    icon_del(row, col, true, false, row_cnt, col_cnt);
            }

            // Remove row
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            bool res = lst_ctrl.item_del((int) row);
            GLib.assert(res);

            // Remove row from array with image indexes
            this.icons_idx.remove_at((int) row);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Add column
         *
         * @param col Column
         */
        public void col_add(uint col, size_t width = DEFAULT_WIDTH)
        {
            // Add column
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            lst_ctrl.col_insert((int) col,
                                new Wx.Str(""),
                                0,
                                (int) width);

            // Add column to array with image indexes
            foreach (Gee.ArrayList<int> icons_row in this.icons_idx)
                icons_row.insert((int) col, Wx.FOUND_NOT);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove column
         *
         * @param col Column
         */
        public void col_remove(uint col)
        {
            // Optimize for memory?
            // Delete icons in column.
            size_t row_cnt = row_cnt_get();
            size_t col_cnt = col_cnt_get();

            if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
            {
                // Memory saving.
                // Delete icons.
                for (uint row = 0; row < row_cnt; row++)
                {
                    if (icon_del_check(row,
                                       col,
                                       false, // Row will not be removed
                                       true,  // Column will be removed
                                       row_cnt,
                                       col_cnt))
                        icon_del(row, col, false, true, row_cnt, col_cnt);
                }
            }
            else
            {
                // CPU saving.
                // Delete icons.
                for (uint row = 0; row < row_cnt; row++)
                    icon_del(row, col, false, true, row_cnt, col_cnt);
            }

            // Remove column
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            bool res = lst_ctrl.col_del((int) col);
            GLib.assert(res);

            // Remove column from array with image indexes
            if (col_cnt == 1)
                this.icons_idx.clear();
            else
                foreach (Gee.ArrayList<int> icons_row in this.icons_idx)
                    icons_row.remove_at((int) col);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of columns
         *
         * @return Number of columns
         */
        public size_t col_cnt_get()
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            return (size_t) lst_ctrl.col_cnt_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of rows
         *
         * @return Number of rows
         */
        public size_t row_cnt_get()
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            return (size_t) lst_ctrl.item_cnt_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value to cell.
         *
         * How to use icon with cell:
         * do not use icon_filepath and icon_blob simultaneously,
         * use icon_hash for icon_blob (calculate it by hash function
         * which you like).
         *
         * @param row           Row
         * @param col           Column
         * @param val           Value or
         *                      null if it not used
         * @param icon_filepath Filepath to icon or null if icon should
         *                      not be used for this cell.
         * @param icon_blob     Icon blob or null if icon should not be
         *                      used for this cell
         * @param icon_hash     Hash for icon blob or null if icon blob
         *                      is not used
         *                      used for this cell
         * @param font          Font
         * @param colour_fg     Foreground colour
         * @param colour_bg     Background colour
         */
        public void val_set(uint    row,
                            uint    col,
                            string? val,

                            string?  icon_filepath = null,
                            uint8[]? icon_blob     = null,
                            string?  icon_hash     = null,

                            Ctrl.font_t   font      = Ctrl.font_t.NONE,
                            Ctrl.colour_t colour_fg = Ctrl.colour_t.NONE,
                            Ctrl.colour_t colour_bg = Ctrl.colour_t.NONE)

            requires ((icon_filepath == null && icon_blob == null && icon_hash == null) ||
                      (icon_filepath == null && icon_blob != null)                      ||
                      (icon_filepath != null && icon_blob == null && icon_hash == null))

            requires (font == Ctrl.font_t.NONE   ||
                      font == Ctrl.font_t.ITALIC ||
                      font == Ctrl.font_t.NORMAL ||
                      font == Ctrl.font_t.SMALL  ||
                      font == Ctrl.font_t.SWISS)

            requires (colour_fg == Ctrl.colour_t.NONE  ||
                      colour_fg == Ctrl.colour_t.WHITE ||
                      colour_fg == Ctrl.colour_t.RED   ||
                      colour_fg == Ctrl.colour_t.BLUE  ||
                      colour_fg == Ctrl.colour_t.GREEN ||
                      colour_fg == Ctrl.colour_t.CYAN  ||
                      colour_fg == Ctrl.colour_t.GREY)

            requires (colour_bg == Ctrl.colour_t.NONE  ||
                      colour_bg == Ctrl.colour_t.WHITE ||
                      colour_bg == Ctrl.colour_t.RED   ||
                      colour_bg == Ctrl.colour_t.BLUE  ||
                      colour_bg == Ctrl.colour_t.GREEN ||
                      colour_bg == Ctrl.colour_t.CYAN  ||
                      colour_bg == Ctrl.colour_t.GREY)
        {
            unowned Wx.Font    font_      = font == Ctrl.font_t.NONE ? Wx.Font.null_get() :
                                                                       Wx.Font.stock_get(font);
            unowned Wx.Colour  colour_fg_ = colour_fg == Ctrl.colour_t.NONE ? Wx.Colour.null_get() :
                                                                              Wx.Colour.stock_get(colour_fg);
            unowned Wx.Colour  colour_bg_ = colour_bg == Ctrl.colour_t.NONE ? Wx.Colour.null_get() :
                                                                              Wx.Colour.stock_get(colour_bg);
            unowned Wx.LstCtrl lst_ctrl   = (Wx.LstCtrl) this.win;

            size_t row_cnt = row_cnt_get();
            size_t col_cnt = col_cnt_get();

            GLib.assert(this.mode == mode_t.OPTIMIZE_FOR_MEMORY || icon_hash == null);

            // Fill cell information
            var lst_item = new Wx.LstItem();

            lst_item.id_set((int) row);
            lst_item.col_set((int) col);
            if (val != null)
                lst_item.txt_set(new Wx.Str(val));
            lst_item.font_set(font_);
            lst_item.bg_colour_set(colour_bg_);
            lst_item.txt_colour_set(colour_fg_);

            // Set icon to cell if required
            int idx;

            if (icon_filepath == null && icon_blob == null)
            {
                // Delete icon from old cell
                if (this.mode == mode_t.OPTIMIZE_FOR_SPEED ||
                    icon_del_check(row,
                                   col,
                                   false, // Row will not be removed
                                   false, // Column will not be removed
                                   row_cnt,
                                   col_cnt))
                {
                    icon_del(row, col, false, false, row_cnt, col_cnt);
                }

                // No icon
                idx = Wx.FOUND_NOT;
            }
            else
            {
                // Initialize icon list
                if (this.img_lst == null)
                {
                    // Initialize image list
                    this.img_lst = new Wx.ImgLst((int) this.icon_width,
                                                 (int) this.icon_height,
                                                 true, // Masks should be created for all images
                                                 0     // The initial size of the list
                                                );

                    // Assign image list to list control
                    lst_ctrl.img_lst_set(this.img_lst, Wx.LstCtrl.img_lst.SMALL);
                }

                // Optimize for memory?
                if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
                {
                    // Memory saving.
                    // Check whether this icon is used already in another cell.
                    unowned string icon_hash_or_filepath = icon_filepath == null ? icon_hash : icon_filepath;
                    idx = this.icon_hash_or_filepath.index_of(icon_hash_or_filepath);

                    if (idx == -1)
                    {
                        // This icon is not used yet.
                        // Load the icon from a file or a blob.
                        Wx.Icon? icon = icon_get(icon_filepath, icon_blob);

                        // Ensure the icon is correct
                        if (icon != null)
                        {
                            // Check if old cell icon can be replaced.
                            // No other cells should use it.
                            if (icon_del_check(row,
                                               col,
                                               false, // Row will not be removed
                                               false, // Column will not be removed
                                               row_cnt_get(),
                                               col_cnt_get()))
                            {
                                // Other cells don't use old icon.
                                // Replace it in image list.
                                // Replace it array with hashes/filepaths.
                                int idx_old = this.icons_idx[(int) row][(int) col];

                                bool res = this.img_lst->icon_replace(idx_old, icon);
                                GLib.assert(res);
                                this.icon_hash_or_filepath[idx_old] = icon_hash_or_filepath;

                                // Icon index in image list is not to be changed
                                idx = idx_old;
                            }
                            else
                            {
                                // Other cells use old icon so we can't replace it.
                                // We also get in this branch if there was no icon in old cell
                                // at all.
                                // Add new icon to image list.
                                // Add new hash/filepath.
                                this.img_lst->icon_add(icon);
                                this.icon_hash_or_filepath.add(icon_hash_or_filepath);

                                // Get its index in image list
                                idx = this.icon_hash_or_filepath.size - 1;
                            }
                        }
                        else
                        {
                            // Bad icon
                            idx = Wx.FOUND_NOT;
                        }
                    }
                    else
                    {
                        // This icon is used already.
                        // Clean up.
                        int idx_old = this.icons_idx[(int) row][(int) col];

                        if (idx != idx_old)
                        {
                            // Delete icon from old cell.
                            // Get its index in image list again.
                            if (icon_del_check(row,
                                               col,
                                               false, // Row will not be removed
                                               false, // Column will not be removed
                                               row_cnt,
                                               col_cnt))
                            {
                                icon_del(row, col, false, false, row_cnt, col_cnt);
                                idx = this.icon_hash_or_filepath.index_of(icon_hash_or_filepath);
                            }
                        }
                    }
                }
                else
                {
                    // This icon is not used yet.
                    // Load the icon from a file or a blob.
                    Wx.Icon? icon = icon_get(icon_filepath, icon_blob);

                    // Ensure the icon is correct
                    if (icon != null)
                    {
                        // Check if old cell icon should be replaced
                        int idx_old = this.icons_idx[(int) row][(int) col];

                        if (idx_old == Wx.FOUND_NOT)
                        {
                            // There was no icon in old cell at all.
                            // Add new icon to image list. Get its index.
                            idx = this.img_lst->icon_add(icon);
                        }
                        else
                        {
                            // Old cell use icon.
                            // Replace it in image list.
                            bool res = this.img_lst->icon_replace(idx_old, icon);
                            GLib.assert(res);

                            // Icon index in image list is not to be changed
                            idx = idx_old;
                        }
                    }
                    else
                    {
                        // Bad icon
                        idx = Wx.FOUND_NOT;
                    }
                }

                // Set icon
                if (idx != Wx.FOUND_NOT)
                    lst_item.img_set((int) idx);
            }

            // Set new index.
            // Set cell.
            this.icons_idx[(int) row][(int) col] = idx;
            lst_ctrl.item_from_info_set(lst_item);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get column width
         *
         * @param col Column
         *
         * @return Width
         */
        public size_t width_get(uint col)
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            return lst_ctrl.col_width_get((int) col);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set column width
         *
         * @param col   Column
         * @param width Width
         */
        public void width_set(uint col, size_t width)
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            bool res = lst_ctrl.col_width_set((int) col, (int) width);
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Remove all rows
         */
        public void clr()
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            // Remove all rows
            bool res = lst_ctrl.all_items_del();
            GLib.assert(res);

            // Clear and deinitialize image list
            if (this.img_lst != null)
            {
                res = this.img_lst->remove_all();
                GLib.assert(res);
                this.img_lst = null;
            }

            // Clear array with hashes/filepaths
            if (this.mode == mode_t.OPTIMIZE_FOR_MEMORY)
                this.icon_hash_or_filepath.clear();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get state of row
         *
         * @param row Row
         *
         * @return false: row is deselected,
         *         true:  row is selected
         */
        public bool sel_get(uint row)
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            bool sel = lst_ctrl.item_state_get((int) row, state_t.SEL) == state_t.SEL;

            return sel;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set state to row
         *
         * @param row Row
         * @param sel false: deselect the row,
         *            true:  select the row
         */
        public void sel_set(uint row, bool sel)
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            lst_ctrl.item_state_set((int) row,
                                    sel ? state_t.SEL : state_t.NONE,
                                    state_t.SEL);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get number of selected rows
         *
         * @return Number of selected rows
         */
        public size_t sel_cnt_get()
        {
            unowned Wx.LstCtrl lst_ctrl = (Wx.LstCtrl) this.win;

            return (size_t) lst_ctrl.sel_item_cnt_get();
        }
    }
}

