/*
 *    dlg.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.Dlg class of wxVala/wxWidgets.
     * It is dialog window.
     */
    public class Dlg : TopLvlWin
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * User answer
         */
        public enum answer_t
        {
            YES    = Wx.Dlg.answer_t.YES,
            OK     = Wx.Dlg.answer_t.OK,
            NO     = Wx.Dlg.answer_t.NO,
            CANCEL = Wx.Dlg.answer_t.CANCEL
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Dialog window constructor.
         *
         * It does nothing by now.
         */
        public Dlg()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide application-modal dialog
         *
         * @param flag false: show dialog,
         *             true:  hide dialog
         */
        public void modal_show(bool flag)
        {
            unowned Wx.Dlg dlg = (Wx.Dlg) this.win;

            if (flag) dlg.modal_show();
            else      dlg.modal_close(0);
        }
    }
}

