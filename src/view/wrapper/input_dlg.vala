/*
 *    input_dlg.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.InputDlg class of
     * wxVala/wxWidgets.
     * It is input dialog window.
     */
    public class InputDlg : Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Pop up a dialog box with input from user.
        // The user may type in text and press "OK" to return this text,
        // or press "Cancel" to return the empty string.
        private static string input(Win?   parent,
                                    string txt,
                                    string title,
                                    string inp_txt,
                                    bool   pwd)
        {
            // Convert strings to internal wxWidgets format wxChar[]
            Wx.Str txt_str     = new Wx.Str(txt);
            Wx.Str title_str   = new Wx.Str(title);
            Wx.Str inp_txt_str = new Wx.Str(inp_txt);

            var txt_arr     = new Wx.Ch[txt_str.len_get()];
            var title_arr   = new Wx.Ch[title_str.len_get()];
            var inp_txt_arr = new Wx.Ch[inp_txt_str.len_get()];

            txt_str.str_get(txt_arr);
            title_str.str_get(title_arr);
            inp_txt_str.str_get(inp_txt_arr);

            // Show input dialog.
            // Get entered text/password length.
            // Length is needed to allocate buffer for storing
            // text/password.
            int len;

            if (pwd)
            {
                len = Wx.pwd_from_user_get(txt_arr,
                                           title_arr,
                                           inp_txt_arr,
                                           parent == null ? null : parent.win,
                                           null);
            }
            else
            {
                len = Wx.txt_from_user_get(txt_arr,
                                           title_arr,
                                           inp_txt_arr,
                                           parent == null ? null : parent.win,
                                           0, // x coordinate
                                           0, // y coordinate
                                           1, // Centering is on
                                           null);
            }

            // Call this function the second time to retrieve entered
            // text/password
            var inp_arr = new Wx.Ch[len];

            if (pwd)
            {
                Wx.pwd_from_user_get(null,
                                     null,
                                     null,
                                     null,
                                     inp_arr);
            }
            else
            {
                Wx.txt_from_user_get(null,
                                     null,
                                     null,
                                     null,
                                     0, // x coordinate
                                     0, // y coordinate
                                     1, // Centering is on
                                     inp_arr);
            }

            var inp_str = new Wx.Str.from_arr_init(inp_arr);

            return inp_str.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Pop up a dialog box with input from user.
         * The user may type in text and press "OK" to return this text,
         * or press "Cancel" to return the empty string.
         * Text entered in the dialog is shown on screen and unmasked.
         *
         * @param parent Parent window
         * @param txt    Text
         * @param title  Title
         * @param title  Title
         */
        public static string txt_inp(Win?   parent,
                                     string txt,
                                     string title,
                                     string inp_txt)
        {
            // Ask user for a password using input dialog
            return input(parent,
                         txt,
                         title,
                         inp_txt,
                         false // Get plain text (unmasked)
                        );
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Pop up a dialog box with input from user.
         * The user may type in text and press "OK" to return this text,
         * or press "Cancel" to return the empty string.
         * Text entered in the dialog is not shown on screen but
         * replaced with stars.
         *
         * @param parent Parent window
         * @param txt    Text
         * @param title  Title
         * @param title  Title
         */
        public static string pwd_inp(Win?   parent,
                                     string txt,
                                     string title,
                                     string inp_txt)
        {
            // Ask user for a password using input dialog
            return input(parent,
                         txt,
                         title,
                         inp_txt,
                         true // Get password (masked text by asterisks)
                        );
        }
    }
}

