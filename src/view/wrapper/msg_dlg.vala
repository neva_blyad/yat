/*
 *    msg_dlg.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.MsgDlg class of
     * wxVala/wxWidgets.
     * It is message dialog window.
     */
    public class MsgDlg : Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * Style
         */
        [Flags]
        public enum style_t
        {
            CENTR  = Wx.MsgDlg.style_t.CENTR,
            YES    = Wx.MsgDlg.style_t.YES,
            OK     = Wx.MsgDlg.style_t.OK,
            NO     = Wx.MsgDlg.style_t.NO,
            CANCEL = Wx.MsgDlg.style_t.CANCEL
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
          * Message dialog window constructor
          *
          * @param parent Parent window
          * @param txt    Text
          * @param title  Title
          * @param style  Style
          */
        public MsgDlg(Win     parent,
                      string  txt,
                      string  title,
                      style_t style = style_t.CENTR | style_t.OK)

            requires ((style & ~(style_t.CENTR |
                                 style_t.YES   |
                                 style_t.OK    |
                                 style_t.NO    |
                                 style_t.CANCEL)) == 0)
        {
            Wx.MsgDlg *msg_dlg = new Wx.MsgDlg(parent.win,
                                               new Wx.Str(txt),
                                               new Wx.Str(title),
                                               (int) style);
            this.win = msg_dlg;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Message dialog window destructor
          */
        ~MsgDlg()
        {
            Wx.MsgDlg *msg_dlg = this.win;

            delete msg_dlg;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide application-modal dialog
         *
         * @return Clicked button
         */
        public new Dlg.answer_t modal_show()
            ensures (result == Dlg.answer_t.YES ||
                     result == Dlg.answer_t.OK  ||
                     result == Dlg.answer_t.NO  ||
                     result == Dlg.answer_t.CANCEL)
        {
            unowned Wx.MsgDlg msg_dlg = (Wx.MsgDlg) this.win;

            return (Dlg.answer_t) msg_dlg.modal_show();
        }
    }
}

