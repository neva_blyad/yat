/*
 *    file_dlg.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.FileDlg class of
     * wxVala/wxWidgets.
     * It is file dialog window.
     */
    public class FileDlg : Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Types                                */
        /*----------------------------------------------------------------------------*/

        /*
         * Style
         */
        [Flags]
        public enum style_t
        {
            OPEN_FILE        = Wx.FileDlg.style_t.OPEN_FILE,
            SAVE_FILE        = Wx.FileDlg.style_t.SAVE_FILE,
            OVERWRITE_PROMPT = Wx.FileDlg.style_t.OVERWRITE_PROMPT,
            FILE_EXIST_MUST  = Wx.FileDlg.style_t.FILE_EXIST_MUST,
            MULTIPLE         = Wx.FileDlg.style_t.MULTIPLE,
            DIR_CHANGE       = Wx.FileDlg.style_t.DIR_CHANGE,
            PREVIEW          = Wx.FileDlg.style_t.PREVIEW,
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
          * File dialog window constructor
          *
          * @param parent   Parent window
          * @param title    Title
          * @param dir      Default directory
          * @param filename Default filename
          * @param wildcart A wildcard, such as "*.*" or "BMP files
          *                 (*.bmp)|*.bmp|GIF files (*.gif)|*.gif"
          * @param style    Style
          */
        public FileDlg(Win     parent,
                       string  title,
                       string  dir,
                       string  filename,
                       string  wildcart,
                       style_t style = style_t.OPEN_FILE | style_t.PREVIEW)

            requires ((style & ~(style_t.OPEN_FILE        |
                                 style_t.SAVE_FILE        |
                                 style_t.OVERWRITE_PROMPT |
                                 style_t.FILE_EXIST_MUST  |
                                 style_t.MULTIPLE         |
                                 style_t.DIR_CHANGE       |
                                 style_t.PREVIEW)) == 0)
        {
            Wx.FileDlg *file_dlg = new Wx.FileDlg(parent.win,
                                                  new Wx.Str(title),
                                                  new Wx.Str(dir),
                                                  new Wx.Str(filename),
                                                  new Wx.Str(wildcart),
                                                  0, // x coordinate
                                                  0, // y coordinate
                                                  (int) style);
            this.win = file_dlg;
        }

        /*----------------------------------------------------------------------------*/

        /**
          * File dialog window destructor
          *
          */
        ~FileDlg()
        {
            Wx.FileDlg *file_dlg = this.win;

            delete file_dlg;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide application-modal dialog
         *
         * @return Clicked button
         */
        public new Dlg.answer_t modal_show()
            ensures (result == Dlg.answer_t.OK ||
                     result == Dlg.answer_t.CANCEL)
        {
            unowned Wx.FileDlg file_dlg = (Wx.FileDlg) this.win;

            return (Dlg.answer_t) file_dlg.modal_show();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * The full path (directory and filename) of the selected file
          *
          * @return Path
          */
        public string path_get()
        {
            unowned Wx.FileDlg file_dlg = (Wx.FileDlg) this.win;

            return file_dlg.path_get().utf8_get().data_utf8_get();
        }
    }
}

