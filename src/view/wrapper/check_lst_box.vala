/*
 *    check_lst_box.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.CheckLstBox class of
     * wxVala/wxWidgets.
     * It is check list box.
     */
    public class CheckLstBox : LstBox
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Check list box constructor.
         *
         * It does nothing by now.
         */
        public CheckLstBox()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get state of element
         *
         * @param idx Index
         *
         * @return false: element is not checked,
         *         true:  element is checked
         */
        public bool is_checked(uint idx)
        {
            unowned Wx.CheckLstBox check_lst = (Wx.CheckLstBox) this.win;

            return check_lst.is_checked((int) idx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set state to element
         *
         * @param idx  Index
         * @param flag false: uncheck element,
         *             true:  check element
         */
        public void check(uint idx, bool flag)
        {
            unowned Wx.CheckLstBox check_lst = (Wx.CheckLstBox) this.win;

            check_lst.check((int) idx, flag);
        }
    }
}

