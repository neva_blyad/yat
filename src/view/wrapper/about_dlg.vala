/*
 *    about_dlg.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.AboutDlgInfo class of
     * wxVala/wxWidgets.
     * It is message dialog window.
     */
    public class AboutDlg : Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Window
        private Wx.AboutDlgInfo about_dlg = new Wx.AboutDlgInfo();

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
          * About dialog window constructor
          *
          * @param parent        Parent window
          * @param icon_filepath Filepath to icon
          * @param icon_width    Icon width
          * @param icon_height   Icon height
          * @param name          Name of the program
          * @param ver           Version
          * @param desc          Description
          * @param license       License
          * @param url           Web site
          * @param copyright     Copyright
          * @param devs          Developers
          * @param artists       Artists
          * @param translators   Translators
          */
        public AboutDlg(Win parent,

                        string? icon_filepath,
                        size_t  icon_width,
                        size_t  icon_height,
                        
                        string    name,
                        string?   ver,
                        string?   desc,
                        string?   license,
                        string?   url,
                        string?   copyright,
                        string[]? devs,
                        string[]? artists,
                        string[]? translators)
        {
            // Use window object as window parent
            this.win = parent.win;

            // Initialize about dialog window:
            //  - icon,
            if (icon_filepath != null)
            {
                // Load the icon from a file
                Wx.Icon icon = new Wx.Icon.create_load(new Wx.Str(icon_filepath),
                                                       Wx.bmp_type_t.PNG,
                                                       (int) icon_width,
                                                       (int) icon_height);
                about_dlg.icon_set(icon);
            }

            //  - program name,
            about_dlg.name_set(new Wx.Str(name));

            //  - version,
            if (ver != null)
                about_dlg.ver_set(new Wx.Str(ver));

            //  - description,
            if (desc != null)
                about_dlg.desc_set(new Wx.Str(desc));

            //  - license,
            if (license != null)
                about_dlg.license_set(new Wx.Str(license));

            //  - URL,
            if (url != null)
                about_dlg.web_site_set(new Wx.Str(url));

            //  - copyright,
            if (copyright != null)
                about_dlg.copyright_set(new Wx.Str(copyright));

            //  - developers,
            if (devs != null)
                foreach (string dev in devs)
                    about_dlg.dev_add(new Wx.Str(dev));

            //  - artists,
            if (artists != null)
            {
                var arr = new Wx.ArrStr();
                foreach (string artist in artists)
                    arr.add(new Wx.Str(artist));
                about_dlg.artists_set(arr);
            }

            //  - translators,
            if (translators != null)
            {
                var arr = new Wx.ArrStr();
                foreach (string translator in translators)
                    arr.add(new Wx.Str(translator));
                about_dlg.translators_set(arr);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide application-modal dialog
         */
        public new void modal_show()
        {
            Wx.AboutDlgInfo.box_show(this.about_dlg, this.win);
        }
    }
}

