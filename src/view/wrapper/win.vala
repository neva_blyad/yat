/*
 *    win.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * Wrapper around Wx.Win class of wxVala/Widgets.
     * It is basic window.
     */
    public abstract class Win : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Item in window is not found
         */
        public const int FOUND_NOT = Wx.FOUND_NOT;

        /*----------------------------------------------------------------------------*/

        /**
         * Window
         */
        public unowned Wx.Win win { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Show/hide
         *
         * @param flag       false: hide,
         *                   true:  show
         * @param upd_layout false: do not update layout,
         *                   true:  force layout of the parent sizer anew
         */
        public void show(bool flag, bool upd_layout = false)
        {
            // Show/hide control
            if (flag) this.win.show();
            else      this.win.hide();

            // Update sizer layout
            if (upd_layout)
            {
                unowned Wx.Sizer? sizer = this.win.containing_sizer_get();
                if (sizer != null)
                    sizer.layout_set();
            }

            // Refresh
            if (flag)
                this.win.refresh(true);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Visibility
         *
         * @return false: the window is shown,
         *         true:  it has been hidden
         */
        public bool is_shown()
        {
            return this.win.is_shown();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Enable/disable
         *
         * @param flag false: disable,
         *             true:  enable
         */
        public void en(bool flag)
        {
            if (flag) this.win.en();
            else      this.win.dis();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get label
         *
         * @return Value
         */
        public virtual string lbl_get()
        {
            Wx.Str val = this.win.lbl_get();

            return val.utf8_get().data_utf8_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set label
         *
         * @param val Value
         */
        public virtual void lbl_set(string val)
        {
            this.win.lbl_set(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Pops up the given menu below this window
         *
         * @param menu          Menu
         * @param use_mouse_pos Use current mouse position
         */
        public void menu_popup(Menu menu, bool use_mouse_pos = false)
        {
            int x, y;

            if (use_mouse_pos)
            {
                Wx.Point pos_mouse = Wx.App.mouse_pos_get();
                Wx.Point pos_win   = this.win.screen_to_client_convert(pos_mouse.x_get(),
                                                                       pos_mouse.y_get());

                x = pos_win.x_get();
                y = pos_win.y_get();
            }
            else
            {
                Wx.Size size = this.win.size_get();

                x = 0;
                y = size.height_get();
            }

            this.win.menu_popup(menu.menu, x, y);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This gets the position of the window
         *
         * @param x Horizontal position
         * @param y Vertical position
         */
        public void pos_get(out int x, out int y)
        {
            Wx.Point point = this.win.pos_get();

            x = point.x_get();
            y = point.y_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Moves the window to the specified position
         *
         * @param x Horizontal position
         * @param y Vertical position
         */
        public void pos_set(int x, int y)
        {
            this.win.move(x, y);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Returns the size of the entire window in pixels, including
         * title bar, border, scrollbars, etc.
         *
         * @param width  Width
         * @param height Height
         */
        public void size_get(out size_t width, out size_t height)
        {
            Wx.Size size = this.win.size_get();

            width  = (size_t) size.width_get();
            height = (size_t) size.height_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Sets the size of the window
         *
         * @param width  Width
         * @param height Height
         */
        public void size_set(size_t width, size_t height)
        {
            this.win.size_set(Wx.DEF_COORD,
                              Wx.DEF_COORD,
                              (int) width,
                              (int) height,
                              0);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Causes this window and all of its children recursively to be
         * repainted
         */
        public void refresh()
        {
            this.win.refresh(true);
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Sizes the window so that it fits around its subwindows
          */
        public void fit()
        {
            this.win.fit();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Similar to Fit(), but sizes the interior (virtual) size of a
          * window
          */
        public void inside_fit()
        {
            this.win.inside_fit();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Has the window focus?
          *
          * @return false: the window has no focus,
          *         true:  otherwise
          */
        public bool focus_has()
        {
            return this.win.focus_has();
        }

        /*----------------------------------------------------------------------------*/

        /**
          * This sets the window to receive keyboard input
          */
        public void focus_set()
        {
            this.win.focus_set();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set foreground colour
         *
         * @param colour Colour
         */
        public void fg_colour_set(Ctrl.colour_t colour)

            requires (colour == Ctrl.colour_t.NONE  ||
                      colour == Ctrl.colour_t.BLACK ||
                      colour == Ctrl.colour_t.WHITE ||
                      colour == Ctrl.colour_t.RED   ||
                      colour == Ctrl.colour_t.BLUE  ||
                      colour == Ctrl.colour_t.GREEN ||
                      colour == Ctrl.colour_t.CYAN  ||
                      colour == Ctrl.colour_t.GREY)
        {
            unowned Wx.Colour colour_ = colour == Ctrl.colour_t.NONE ? Wx.Colour.null_get() :
                                                                       Wx.Colour.stock_get(colour);

            this.win.fg_colour_set(colour_);
        }
    }
}

