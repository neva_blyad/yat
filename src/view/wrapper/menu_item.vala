/*
 *    menu_item.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.MenuItem class of
     * wxVala/wxWidgets.
     * It is menu item.
     */
    public class MenuItem : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Menu item
         */
        public unowned Wx.MenuItem menu;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Menu item constructor.
         *
         * It does nothing by now.
         */
        public MenuItem()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Check/uncheck
         *
         * @param flag false: check,
         *             true:  uncheck
         */
        public void check(bool flag)
        {
            this.menu.check(flag);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Enable/disable
         *
         * @param flag false: disable,
         *             true:  enable
         */
        public void en(bool flag)
        {
            this.menu.en(flag);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set value
         *
         * @param val Value
         */
        public void val_set(string val)
        {
            this.menu.item_lbl_set(new Wx.Str(val));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Checked/unchecked
         *
         * @return false: not checked
         *         true:  checked
         */
        public bool is_checked()
        {
            return this.menu.is_checked();
        }
    }
}

