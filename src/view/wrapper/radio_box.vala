/*
 *    radio_box.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.RadioBox class of
     * wxVala/wxWidgets.
     * It is radio box — number of mutually exclusive choices.
     */
    public class RadioBox : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Radio box constructor.
         *
         * It does nothing by now.
         */
        public RadioBox()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get index of selected radio button
         *
         * @return idx Index or Win.FOUND_NOT if no value selected
         */
        public int sel_get()
        {
            unowned Wx.RadioBox radio_box = (Wx.RadioBox) this.win;

            return radio_box.sel_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Select radio button using its index
         *
         * @param idx Index
         */
        public void sel_set(uint idx)
        {
            unowned Wx.RadioBox radio_box = (Wx.RadioBox) this.win;

            radio_box.sel_set((int) idx);
        }
    }
}

