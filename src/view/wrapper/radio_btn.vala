/*
 *    radio_btn.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.RadioBtn class of
     * wxVala/wxWidgets.
     * It is radio button — exclusive choice.
     */
    public class RadioBtn : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Radio button constructor.
         *
         * It does nothing by now.
         */
        public RadioBtn()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get state of radio button
         *
         * @return false: radio button is not selected,
         *         true:  radio button is selected
         */
        public bool sel_get()
        {
            unowned Wx.RadioBtn radio_btn = (Wx.RadioBtn) this.win;

            return radio_btn.sel_get();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set state to radio button
         *
         * @param val false: deselect the radio button,
         *            true:  select the radio button
         */
        public void sel_set(bool val)
        {
            unowned Wx.RadioBtn radio_btn = (Wx.RadioBtn) this.win;

            radio_btn.sel_set(val);
        }
    }
}

