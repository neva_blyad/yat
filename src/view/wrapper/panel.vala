/*
 *    notebook.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.Panel class of wxVala/wxWidgets.
     * It is panel.
     */
    public class Panel : Ctrl
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Panel constructor.
         *
         * Doesn't allocate a control, assuming you've already done it.
         * (E. g. imported from XRC.)
         * You have to assign this.win object of Wx.Win class then.
         *
         * It does nothing by now.
         */
        public Panel()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Panel constructor.
         *
         * Dynamically allocates and creates a new control.
         *
         * @param notebook Notebook to attach to
         */
        public Panel.create(Notebook notebook)
        {
            var panel = new Wx.Panel(notebook.win,
                                     Wx.ID_ANY,
                                     0, // Left
                                     0, // Top
                                     0, // Width
                                     0, // Height
                                     Wx.Win.style_t.TAB_TRAVERSAL);
            this.win = panel;
        }
    }
}

