/*
 *    btn_bmp.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

/**
 * Wrapper around Wx namespace of wxVala/wxWidgets.
 *
 * It is necessary to prevent Controller from directly
 * accessing wxWidgets. Controller should not know that View
 * is based on wxWidgets (or some other GUI toolkit library).
 */
namespace yat.View.Wrapper
{
    /**
     * This is the extension for Wx.BMPBtn class of
     * wxVala/wxWidgets.
     * It is button with image.
     */
    public class BtnBMP : Btn
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Bitmap button constructor.
         *
         * It does nothing by now.
         */
        public BtnBMP()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Set image
         *
         * @param filepath Filepath to image
         */
        public void img_set(string filepath)
        {
            unowned Wx.BMPBtn btn_bmp = (Wx.BMPBtn) this.win;

            // Load the bitmap from a file
            var bmp = new Wx.BMP(new Wx.Str(filepath), Wx.bmp_type_t.GIF);

            // Set it to control
            btn_bmp.bmp_lbl_set(bmp);

            // Update sizer layout
            unowned Wx.Sizer sizer = btn_bmp.containing_sizer_get();
            if (sizer != null)
                sizer.layout_set();
        }
    }
}

