/*
 *    cfg.vapi
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Program configuration.
 *
 * These macro are defined in SConstruct file.
 */
[CCode]
namespace Cfg
{
    /*
     * Program name
     */
    [CCode (cname = "CFG_NAME")]
    const string NAME;

    /*
     * Program version
     */
    namespace Ver
    {
        /*
         * Version: major
         */
        [CCode (cname = "CFG_VER_MAJOR")]
        const uint MAJOR;

        /*
         * Version: minor
         */
        [CCode (cname = "CFG_VER_MINOR")]
        const uint MINOR;

        /*
         * Version: micro
         */
        [CCode (cname = "CFG_VER_MICRO")]
        const uint MICRO;

        /*
         * Version: name.
         *
         * Only important releases should have name
         * (i. e. if major/minor changes).
         */
        [CCode (cname = "CFG_VER_NAME")]
        const string NAME;
    }

    /*
     * i18n symbol
     */
    [CCode (cname = "GETTEXT_PACKAGE")]
    const string DOMAIN;
}

