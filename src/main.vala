/*
 *    main.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// i18n
static string prefix;
static bool   use_prefix;

// CLI arguments
static bool ver;
static uint verbose;
static uint warn;

// Message log data variables:
// standard descriptors to output, preformatted log levels
static Gee.HashMap<GLib.LogLevelFlags, unowned GLib.FileStream> desc_hash;
static Gee.HashMap<GLib.LogLevelFlags, string>                  fmt_lvl_hash;

/*----------------------------------------------------------------------------*/

// Singletons
extern yat.Model.Model *model;
extern yat.View.View   *view;
extern yat.Ctrl.Ctrl   *ctrl;

// Main process
GLib.Thread<int> main_proc;

/*----------------------------------------------------------------------------*/

// Initialize application name.
//
// Sets environment variables.
static void app_init()
{
    GLib.Environment.set_prgname(Cfg.NAME);
    GLib.Environment.set_application_name(Cfg.NAME); // No translation for the program name
}

/*----------------------------------------------------------------------------*/

// Initialize prefix.
//
// Finds the program installation prefix if it is installed.
private void prefix_init()
{
    // Find path to currently running executable
    int len = WhereAmI.exec_path_get(null, null);
    var filepath = new char[len];
    int len_dir;
    WhereAmI.exec_path_get(filepath, out len_dir);

    // Cut the directory name
    string path = "";
    for (int byte = 0; byte < len_dir; byte++)
        path += filepath[byte].to_string();

    // Find the program installation prefix if any
    try
    {
        var regex = new GLib.Regex(Path.DIR_SEPARATOR_S + "bin\\z");

        prefix = regex.replace_literal(path, -1, 0, "");
        use_prefix  = path != prefix;
    }
    catch (RegexError ex)
    {
        GLib.assert(false);
    }
}

/*----------------------------------------------------------------------------*/

// Initialize i18n.
//
// Installs the specified system locale.
// Binds domain to the directory with translations.
//
// Note. This function is needed just to translate several
// console help messages right now.
// We'll do full l14n & i18n setup later by Model() constructor,
// e. g. we should read yat's config file with user's preferred
// langyage, etc..
static void i18n_init()
{
    // i18n constants
    const string CHARSET = "UTF-8"; // Character set of the translation domain

    // Directory with translations
    string filepath = use_prefix ? GLib.Path.build_filename(prefix, "share", "locale") :
                                   "locale";

    // Install the specified system locale
    GLib.Intl.setlocale(GLib.LocaleCategory.ALL, "");
    GLib.Intl.setlocale(GLib.LocaleCategory.COLLATE, "C");

    // Bind domain to the directory with translations
    GLib.Intl.textdomain(Cfg.DOMAIN);
    GLib.Intl.bindtextdomain(Cfg.DOMAIN, filepath);
    GLib.Intl.bind_textdomain_codeset(Cfg.DOMAIN, CHARSET);
}

/*----------------------------------------------------------------------------*/

// Handle and count the found CLI flag
static bool cli_flag_handle(string name, string? val, void *data)
{
    GLib.assert(val  == null);
    GLib.assert(data == null);

    // Found a new CLI short-sized argument.
    // Increment its counter.
    bool res = true;

    switch (name)
    {
    case "-v":
        verbose++;
        break;
    case "-V":
        warn++;
        break;
    case "":
        res = false;
        break;
    default:
        GLib.assert(false);
        break;
    }

    return res;
}

/*----------------------------------------------------------------------------*/

// Parse CLI arguments
static void cli_parse(string[] args)
{
    // Now this is time for GLib CLI parser.
    //
    // This struct defines which options are accepted by the command
    // line option parser.
    string ver_info = _("Output version information");

    GLib.OptionEntry[] opts =
    {
        // --version.
        //
        // If appears, a boolean variable will be set.
        { "version", '\0', OptionFlags.NONE,   OptionArg.NONE,     &ver,                     ver_info, null },

        // -v, -V.
        //
        // We use callbacks to count repeating arguments which we can
        // meet multiple times.
        { "",        'v',  OptionFlags.NO_ARG, OptionArg.CALLBACK, (void *) cli_flag_handle, "",       null },
        { "",        'V',  OptionFlags.NO_ARG, OptionArg.CALLBACK, (void *) cli_flag_handle, "",       null },

        // Assume --help, -h arguments will be added by GLib parser
        // internally.
        // List terminator.
        { null }
    };

    // Run CLI option parser.
    //
    // Set help messages for v, -V flags explicitly, because GLib
    // parser doesn't show help for short-only arguments without
    // long variants by default.
    try
    {
        // GStreamer arguments
        GLib.OptionGroup? gst_group = Gst.init_get_option_group();
        GLib.assert(gst_group != null);

        // Copy argument array, because parser will destroy it
        var tmp = new string[args.length];
        for (size_t idx = 0; idx < args.length; idx++)
            tmp[idx] = args[idx];
        unowned string[] args_ = tmp;

        // CLI parser context
        var opt_context = new GLib.OptionContext(_("— instant messaging Tox client"));

        opt_context.add_main_entries(opts, null);
        opt_context.add_group(gst_group);
        opt_context.set_help_enabled(true);
        opt_context.set_description(_("""Log Options:
  -v                                Enable debug messages. You also can pass
                                    this flag two or three times to increase
                                    verbose level:
                                           do not print debug messages at all,
                                      -v   prints only yat's debug messages,
                                      -vv  prints debug messages from yat and
                                           its plugins,
                                      -vvv prints debug messages also from
                                           external libraries such as GStreamer,
                                           GLib and others.
  -V                                Enable warnings:
                                           do not print warnings at all,
                                      -V   prints only yat's warnings,
                                      -VV  prints warnings from yat and its
                                           plugins,
                                      -VVV prints warnings also from external
                                           libraries such as GStreamer, GLib and
                                           others"""));

        bool res = opt_context.parse(ref args_);
        GLib.assert(res);
    }
    catch (GLib.OptionError ex)
    {
        stdout.printf(_("Error: [%s]\n"), ex.message);
        stdout.printf(_("Run '%s --help' to see a full list of available command line options.\n"), args[0]);

        Posix.exit(Posix.EXIT_FAILURE);
    }
}

/*----------------------------------------------------------------------------*/

// Show application and dynamic linked library versions.
//
// Checks the shared object version mismatches.
// Handles --version flag.
static void ver_show()
{
    // Ensure version of the running GLib is compatible with
    // compiled time version
    unowned string? dbg = GLib.Version.check(GLib.Version.major,
                                             GLib.Version.minor,
                                             GLib.Version.micro);

    if (dbg != null)
    {
        stdout.printf(dbg);
        Posix.exit(Posix.EXIT_FAILURE);
    }

    // Handle --version flag
    if (ver)
    {
        stdout.printf(_("%s %u.%u.%u\n"), Cfg.NAME,
            Cfg.Ver.MAJOR,
            Cfg.Ver.MINOR,
            Cfg.Ver.MICRO);
        stdout.printf(_("GLib %u.%u.%u (compiled time version; compatible with the current)\n"),
            GLib.Version.major,
            GLib.Version.minor,
            GLib.Version.micro);
        stdout.printf(_("libtoxcore %u.%u.%u\n"),
            ToxCore.Version.lib_major(),
            ToxCore.Version.lib_minor(),
            ToxCore.Version.lib_patch());
        stdout.printf(_("libsqlite3 %s\n"), Sqlite.libversion());
        stdout.printf(_("%s\n"), Gst.version_string());

        Posix.exit(Posix.EXIT_SUCCESS);
    }
}

/*----------------------------------------------------------------------------*/

// Setup log messages.
//
// Handles -v, -V flags.
static void log_liberate()
{
    // Setup debug messages.
    // Setup warnings.
    //
    // First, define log format.
    desc_hash    = new Gee.HashMap<GLib.LogLevelFlags, unowned GLib.FileStream>();
    fmt_lvl_hash = new Gee.HashMap<GLib.LogLevelFlags, string>();

    desc_hash[GLib.LogLevelFlags.LEVEL_CRITICAL] = GLib.stderr;
    desc_hash[GLib.LogLevelFlags.LEVEL_DEBUG   ] = GLib.stdout;
    desc_hash[GLib.LogLevelFlags.LEVEL_ERROR   ] = GLib.stderr;
    desc_hash[GLib.LogLevelFlags.LEVEL_INFO    ] = GLib.stdout;
    desc_hash[GLib.LogLevelFlags.LEVEL_MESSAGE ] = GLib.stdout;
    desc_hash[GLib.LogLevelFlags.LEVEL_WARNING ] = GLib.stderr;

    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_CRITICAL] = "\x1B[38;5;196m[%s]\x1B[0m".printf(_("CRITICAL"));
    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_DEBUG   ] = "\x1B[38;5;154m[%s]\x1B[0m".printf(_("DEBUG"));
    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_ERROR   ] = "\x1B[38;5;196m[%s]\x1B[0m".printf(_("ERROR"));
    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_INFO    ] = "\x1B[38;5;163m[%s]\x1B[0m".printf(_("INFO"));
    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_MESSAGE ] = "\x1B[38;5;163m[%s]\x1B[0m".printf(_("MESSAGE"));
    fmt_lvl_hash[GLib.LogLevelFlags.LEVEL_WARNING ] = "\x1B[38;5;199m[%s]\x1B[0m".printf(_("WARNING"));

    main_proc = GLib.Thread.self<int>();

    GLib.LogFunc log_fn = (domain, flag, msg) =>
    {
        bool res;

        switch (domain)
        {
        case null:
        case "yat-plugins":
            res = !((verbose < 2 && flag == GLib.LogLevelFlags.LEVEL_DEBUG) ||
                    (warn    < 2 && flag == GLib.LogLevelFlags.LEVEL_WARNING));
            break;
        case "yat":
            res = !((verbose < 1 && flag == GLib.LogLevelFlags.LEVEL_DEBUG) ||
                    (warn    < 1 && flag == GLib.LogLevelFlags.LEVEL_WARNING));
            break;
        default:
            res = !((verbose < 3 && flag == GLib.LogLevelFlags.LEVEL_DEBUG) ||
                    (warn    < 3 && flag == GLib.LogLevelFlags.LEVEL_WARNING));
            break;
        }

        if (res)
        {
            var thread = GLib.Thread.self<void>();

            string fmt_thread = "\x1B[38;5;22m[";
            if ((void *) thread == main_proc)
                fmt_thread += _("MAIN PROCESS");
            else if (model != null && model->threads.has_key(thread))
                fmt_thread += model->threads[thread].name;
            else
                fmt_thread += "%p".printf(thread);
            fmt_thread += "]\x1B[0m";

            var    datetime = new GLib.DateTime.now_local();
            string fmt_time = "\x1B[38;5;56m[%s]\x1B[0m".printf(datetime.format("%H:%M:%S:%f"));

            desc_hash[flag].printf("%s %s %s %s\n", fmt_time,
                                                    fmt_lvl_hash[flag],
                                                    fmt_thread,
                                                    msg);
        }
    };

    // Set the message levels which are always fatal, in any log domain.
    // Setup default log handler.
    //GLib.Log.set_always_fatal(GLib.LogLevelFlags.LEVEL_CRITICAL |
    //                          GLib.LogLevelFlags.LEVEL_ERROR); // This suppressed errors at all, I disabled it
    GLib.Log.set_default_handler(log_fn);

#if SET_SPECIFIC_LOG_HANDLERS
    // Setup specific log handlers
    GLib.LogLevelFlags flags = 
        GLib.LogLevelFlags.LEVEL_CRITICAL |
        GLib.LogLevelFlags.LEVEL_ERROR    |
        GLib.LogLevelFlags.LEVEL_INFO     |
        //GLib.LogLevelFlags.LEVEL_MASK     |
        GLib.LogLevelFlags.LEVEL_MESSAGE;

    if (warn    > 2) flags |= GLib.LogLevelFlags.LEVEL_WARNING;
    if (verbose > 2) flags |= GLib.LogLevelFlags.LEVEL_DEBUG;

    GLib.Log.set_handler("GLib",                     flags, log_fn);
    GLib.Log.set_handler("GLib-GObject",             flags, log_fn);
    GLib.Log.set_handler("GLib-IO",                  flags, log_fn);
    GLib.Log.set_handler("GVFS-RemoteVolumeMonitor", flags, log_fn);
    GLib.Log.set_handler("GdkPixbuf",                flags, log_fn);
    GLib.Log.set_handler("Gtk",                      flags, log_fn);

    // TODO: list of domain handlers should be continued...

    if (warn    > 1) flags |= GLib.LogLevelFlags.LEVEL_WARNING;
    if (verbose > 1) flags |= GLib.LogLevelFlags.LEVEL_DEBUG;

    GLib.Log.set_handler("yat-plugins", flags, log_fn);

    if (warn    > 0) flags |= GLib.LogLevelFlags.LEVEL_WARNING;
    if (verbose > 0) flags |= GLib.LogLevelFlags.LEVEL_DEBUG;

    GLib.Log.set_handler(null,  flags, log_fn);
    GLib.Log.set_handler("yat", flags, log_fn);
#endif

    // Set environment variables.
    //
    // (If user set them already in OS, do not overwrite.)
    string domains;

    if (verbose > 2)
        domains = "all";
    else
    {
                         domains  = "";
        if (verbose > 0) domains += "yat";
        if (verbose > 1) domains += ",yat-plugins";
    }

    GLib.Environment.set_variable("G_MESSAGES_PREFIXED", "",      false);
    GLib.Environment.set_variable("G_MESSAGES_DEBUG",    domains, false);
}

/*----------------------------------------------------------------------------*/

// Initialize command line interface (CLI).
// Handle CLI arguments.
static void cli_init(string[] args)
{
    // Initialize command line interface (CLI).
    cli_parse(args);

    // Now CLI arguments should be handled.
    //
    // Show help (if user asked).
    // Setup log messages.
    // Show application and dynamic linked library versions (if user
    // asked).
    log_liberate();
    ver_show();
}

/*----------------------------------------------------------------------------*/

// Program entry point
static int main(string[] args)
{
    // The main program.
    //
    // Initialize application name.
    // Initialize prefix.
    // Initialize i18n. Needed to translate everything.
    app_init();
    prefix_init();
    i18n_init();

    // Initialize command line interface (CLI).
    // Handle CLI arguments:
    //  --version, -v,
    //  --help,
    //  -v, -V.
    cli_init(args);

    GLib.debug(_("Start %s %u.%u.%u"), Cfg.NAME, Cfg.Ver.MAJOR,
                                                 Cfg.Ver.MINOR,
                                                 Cfg.Ver.MICRO);

    // This application uses a Model-View-Controller architecture.
    // Initialize MVC singleton objects.
    // This creates and starts new threads.
    model = new yat.Model.Model(args);
    view  = new yat.View.View  (args);
    ctrl  = new yat.Ctrl.Ctrl  (args);

    // Let Controller do all the application job
    ctrl->execute();

    GLib.debug(_("Stop"));

    // Deinitialize MVC singleton objects.
    // Vala doesn't destroy global data so we need to do it manually
    // using pointers instead of references.
    delete model;
    delete view;
    delete ctrl;

    // Terminate application with successful exit code
    return Posix.EXIT_SUCCESS;
}

