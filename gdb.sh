#!/bin/sh

# Find libyat.so and yat-*.gir in the specified directories
export LD_LIBRARY_PATH=./
export GI_TYPELIB_PATH=./:src/model/external/libtoxcore/

# Find sources in plugin/ directory.
# We use find instead of gdb's directory internal directive here because find do
# it recursively.
PLUGIN_DIRS=$(find plugins -type d | xargs printf ' -d %s')

# Now it is time for GNU Debugger seSSion
gdb --command gdbinit \
    $PLUGIN_DIRS      \
    --args ./yat "$@"
