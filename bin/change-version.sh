#!/usr/bin/env bash
#
# Upgrades the program version in all files in repository.

# Note. Web links for downloading in README.md are not to be updated, do it
# manually.

function use
{
    echo "Usage: $0 <MAJOR> <MINOR> <MICRO> [NAME]"
    exit 1
}

if [ $# -ne 3 ] &&
   [ $# -ne 4 ]
then
    use
fi

MAJOR="$1"
MINOR="$2"
MICRO="$3"
[ $# -eq 4 ] && NAME="$4" || NAME=

perl -e 'my $name;
         local $/;
         $_ = <STDIN>;
         if ($ARGV[3] eq "") { $name = ""; }
         else                { $name = qq( "$ARGV[3]"); }
         s|# VERSION\n\n\K\d+\.\d+\.\d+( "[^"]+")?|$ARGV[0].$ARGV[1].$ARGV[2]$name|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < README.md > README.md.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|MAJOR=\K\d+|$ARGV[0]|;
         s|MINOR=\K\d+|$ARGV[1]|;
         s|MICRO=\K\d+|$ARGV[2]|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < INSTALL > INSTALL.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|major = \K\d+|$ARGV[0]|;
         s|minor = \K\d+|$ARGV[1]|;
         s|micro = \K\d+|$ARGV[2]|;
         s|name  = "\K[^"]*|$ARGV[3]|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < SConstruct > SConstruct.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+-\d+|$ARGV[0].$ARGV[1].$ARGV[2]-1|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < debian/DEBIAN/control > debian/DEBIAN/control.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+-\d+|$ARGV[0].$ARGV[1].$ARGV[2]-1|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < plugins/debian/DEBIAN/control > plugins/debian/DEBIAN/control.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|major = \K\d+|$ARGV[0]|;
         s|minor = \K\d+|$ARGV[1]|;
         s|micro = \K\d+|$ARGV[2]|;
         s|name  = "\K[^"]*|$ARGV[3]|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < plugins/SConstruct > plugins/SConstruct.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|ProductVersion="\K\d+\.\d+\.\d+|$ARGV[0].$ARGV[1].$ARGV[2]|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < yat.wxs > yat.wxs.new

mv debian/DEBIAN/control.new         debian/DEBIAN/control
mv INSTALL.new                       INSTALL
mv README.md.new                     README.md
mv SConstruct.new                    SConstruct
mv plugins/debian/DEBIAN/control.new plugins/debian/DEBIAN/control
mv plugins/SConstruct.new            plugins/SConstruct
mv yat.wxs.new                       yat.wxs

for FILEPATH in locale/yat.pot locale/*/LC_MESSAGES/*.po
do
    perl -e 'local $/;
             $_ = <STDIN>;
             s|Project-Id-Version: yat \K\d+.\d+.\d+|$ARGV[0].$ARGV[1].$ARGV[2]|;
             print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

for FILEPATH in plugins/locale/yat-plugins-*.pot plugins/locale/*/LC_MESSAGES/*.po
do
    perl -e 'local $/;
             $_ = <STDIN>;
             s|Project-Id-Version: yat-plugins \K\d+.\d+.\d+|$ARGV[0].$ARGV[1].$ARGV[2]|;
             print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

for FILEPATH in plugins/*/*.plugin
do
    perl -e 'local $/;
             $_ = <STDIN>;
             s|Version=\K\d+.\d+.\d+|$ARGV[0].$ARGV[1].$ARGV[2]|;
             print' "$MAJOR" "$MINOR" "$MICRO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

for FILEPATH in plugins/*/*.pm
do
    perl -e "local $/;
             \$_ = <STDIN>;
             s|version[ ]*=>[ ]*'\K\d+.\d+'|\$ARGV[0].\$ARGV[1]'|;
             print" "$MAJOR" "$MINOR" "$MICRO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

echo Done
