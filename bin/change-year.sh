#!/usr/bin/env bash
#
# Upgrades the copyright years in headers of all source files.

shopt -s globstar

function use
{
    echo "Usage: $0 <year_from> <year_to> <name>"
    exit 1
}

if [ $# -ne 3 ]
then
    use
fi

YEAR_FROM="$1"
YEAR_TO="$2"
YEAR_NAME="$3"

# TODO: add README.md, SConstruct, plugins/SConstruct, .gitignore something
#       more?

for FILEPATH in plugins/*/*.vala src/**/*.vala src/**/*.vapi
do
    perl -e 'local $/;
             $_ = <STDIN>;

             s|(?<=Copyright \(C\) )\d\d\d\d(-\d\d\d\d)?(?= $ARGV[2])|$ARGV[0]-$ARGV[1]|;
             s| \*\s+<([a-zA-Z0-9._-]+\@[a-zA-Z0-9._-]+)>| *                                            <\1>|;

             print' "$YEAR_FROM" "$YEAR_TO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

# Uncomment if plugins should be updated too
exit

for FILEPATH in plugins/*/*.plugin
do
    perl -e 'local $/;
             $_ = <STDIN>;

             s|© 2022(?= $ARGV[2])|© $ARGV[0]-$ARGV[1]|g;

             print' "$YEAR_FROM" "$YEAR_TO" "$NAME" < "$FILEPATH" > "$FILEPATH.new"
    mv "$FILEPATH.new" "$FILEPATH"
done

echo Done
