#!/bin/sh

# Find libyat.so and yat-*.typelib in the current directory
export LD_LIBRARY_PATH=./
export GI_TYPELIB_PATH=./

# Start Tox messaging with yat
./yat "$@"
