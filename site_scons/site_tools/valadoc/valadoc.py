# -*- coding:utf-8; -*-
#
#    valadoc.py
#    Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                       <neva_blyad@lovecri.es>
#
#    This file is part of yat.
#
#    yat is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    yat is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with yat.  If not, see <https://www.gnu.org/licenses/>.

"""SCons.Tool.valadoc

Tool-specific initialization for Valadoc documentation generator.

There normally shouldn't be any need to import this module directly.
It will usually be imported through the generic SCons.Tool.Tool()
selection method.

"""

import SCons

def generate(env) :
    env['VALADOC'] = env.Detect('valadoc') or 'valadoc'
    env['VALADOCFLAGS'] = SCons.Util.CLVar('')
    env['VALADOCPACKAGES'] = SCons.Util.CLVar('')
    env['VALADOCPACKAGEPREFIX'] = '--pkg='
    env['_VALADOCPACKAGES'] = '${ _defines(VALACPACKAGEPREFIX, VALACPACKAGES, None, __env__) }'
    env['VALADOCPACKAGEPATHS'] = SCons.Util.CLVar('')
    env['VALADOCPACKAGEPATHPREFIX'] = '--vapidir='
    env['_VALADOCPACKAGEPATHS'] = '${ _defines(VALADOCPACKAGEPATHPREFIX, VALADOCPACKAGEPATHS, None, __env__) }'
    baseCommandString = '-o $TARGET $VALADOCFLAGS $_VALADOCPACKAGEPATHS $_VALADOCPACKAGES $SOURCES'
    valaDocAction = SCons.Action.Action('$VALADOC ' + baseCommandString, '$VALAPROGRAMCOMSTR')
    valaDocBuilder = SCons.Builder.Builder (
        action = valaDocAction,
        src_suffix = '.vala',
        suffix = '',
        target_factory=env.fs.Dir
       )
    env.Append(BUILDERS = {
        'Valadoc': valaDocBuilder
        })

def exists(env) :
    return env.Detect(["valadoc"])
