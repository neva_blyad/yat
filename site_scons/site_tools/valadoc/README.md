# Valadoc Tool for SCons

## Introduction

[SCons](http://www.scons.org) is a build framework originally for C, C++, Fortran, and D builds. It has
though a tools (think plugin) architecture that allows tools to be built for other language builds. This
repository contains a tool for for generating API documentation in HTML format from Vala source code.

## Licence

GPL-3.0-or-later.
