# -*- coding:utf-8; -*-
#
#    __init__.py
#    Copyright (C) 2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                       <neva_blyad@lovecri.es>
#
#    This file is part of yat.
#
#    yat is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    yat is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with yat.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import absolute_import
from .valadoc import generate, exists
