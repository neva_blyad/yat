#    SConstruct
#    Copyright (C) 2021-2024 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#
#    This file is part of yat.
#
#    yat is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    yat is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with yat.  If not, see <https://www.gnu.org/licenses/>.

# Imports
import SCons
import glob
import os
import re
import subprocess
import sys

# Construct the environment
dict = {}
if 'PATH' in os.environ:
    dict['PATH'] = os.environ['PATH']
if 'PKG_CONFIG_PATH' in os.environ:
    dict['PKG_CONFIG_PATH'] = os.environ['PKG_CONFIG_PATH']
env = Environment(ENV=dict)

# Software version
major = 0
minor = 11
micro = 0
name  = "Daria Videophone Edition"

ver = str(major) + '.' + str(minor) + '.' + str(micro)

# Parse SCons command line arguments
#  - port,
#  - debug flag,
AddOption('--port', dest    = 'port',
                    nargs   = 1,
                    type    = 'string',
                    action  = 'store',
                    metavar = 'PORT',
                    help    = 'Port (leave empty usually)',
                    default = '')
AddOption('--dbg' , dest    = 'dbg',
                    nargs   = 1,
                    type    = 'string',
                    action  = 'store',
                    metavar = 'VAL [on/off]',
                    help    = 'Build with debugging',
                    default = 'off')

port = str(GetOption('port'))
dbg  = str(GetOption('dbg'))

#  - show all audio hardware in "Settings" dialog, even hidden
AddOption('--show-hidden-audio-hw', dest    = 'show_hidden_audio_hw',
                                    nargs   = 1,
                                    type    = 'string',
                                    action  = 'store',
                                    metavar = 'VAL [on/off]',
                                    help    = 'Show all audio devices, even those from hidden providers',
                                    default = 'off')
show_hidden_audio_hw = str(GetOption('show_hidden_audio_hw'))

# C compiler flags:
#
#  - disable some warnings,
env.Append(CCFLAGS = '-Wno-deprecated-declarations')
env.Append(CCFLAGS = '-Wno-discarded-qualifiers')
env.Append(CCFLAGS = '-Wno-implicit-function-declaration')
env.Append(CCFLAGS = '-Wno-incompatible-pointer-types')
#env.Append(CCFLAGS = '-Wno-int-conversion')
#env.Append(CCFLAGS = '-Wno-pointer-to-int-cast')

#  - hide the console for Windows application,
if sys.platform == 'win32' or sys.platform == 'cygwin':
    env.Append(CCFLAGS = '-mwindows')

#  - change optimization level
if dbg == 'on':
    env.Append(CCFLAGS = '-O0')
    env.Append(CCFLAGS = '-g -ggdb')
else:
    env.Append(CCFLAGS = '-O2')
    env.Append(CCFLAGS = '-g0')

# Macro definitions:
#  - general,
env.Append(CPPDEFINES = [('CFG_NAME',        r'\"yat\"')])
env.Append(CPPDEFINES = [('CFG_VER_MAJOR',   major)])
env.Append(CPPDEFINES = [('CFG_VER_MINOR',   minor)])
env.Append(CPPDEFINES = [('CFG_VER_MICRO',   micro)])
env.Append(CPPDEFINES = [('CFG_VER_NAME',    r'\"' + name + r'\"')])
env.Append(CPPDEFINES = [('GETTEXT_PACKAGE', r'\"yat\"')])
env.Append(CPPDEFINES = [('G_LOG_DOMAIN',    r'\"yat\"')])

#  - port,
if port == 'librem5':
    env.Append(CPPDEFINES = 'PORT_LIBREM5')

#  - logic
if show_hidden_audio_hw == 'on':
    env.Append(CPPDEFINES = 'SHOW_HIDDEN_AUDIO_HW')
env.Append(CPPDEFINES = 'GSTREAMER_INITIALIZED_BY_AUDIO')
#env.Append(CPPDEFINES = 'GSTREAMER_INITIALIZED_BY_VIDEO')

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons resource                                                               #
################################################################################

# Windows only
if sys.platform == 'win32' or sys.platform == 'cygwin':

    # Add GNU windres builder
    builder_windres = SCons.Builder.Builder(action     = '`wx-config --rescomp` $SOURCE -o $TARGET',
                                            src_suffix = '.rc',
                                            suffix     = '.o')
    env.Append(BUILDERS = {'builder_resource' : builder_windres})

    # Add resource target
    target_resource = env.builder_resource('resource.o', 'resource.rc')
    env.Alias('resource', target_resource)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons lib-static                                                             #
################################################################################

# External macro definitions
#env.Append(CPPDEFINES = 'SQLITE_OMIT_DISKIO')         # No writing database to disk
#env.Append(CPPDEFINES = 'SQLITE_OMIT_LOAD_EXTENSION') # No dynamic libraries support

# External includes/libraries
env.Append(CPPPATH = [])
env.Append(LIBPATH = [])
env.Append(LIBS    = [])

env.Append(CPPPATH = 'src/model/sqlite3/external/')
env.Append(LIBPATH = 'src/model/sqlite3/external/')
env.Append(LIBS    = 'sqlite3-SSE')

env.Append(CPPPATH = 'src/model/whereami/external/src/')
env.Append(LIBPATH = 'src/model/whereami/external/src/')
env.Append(LIBS    = 'whereami')

# Special environment for static libraries.
#
# C compiler flags:
#  - position-independent code (PIC).
env_static = env.Clone()
env_static.Append(CCFLAGS = '-fPIC')

# Add static library target
target_lib_static = [env_static.Library('src/model/sqlite3/external/libsqlite3-SSE.a',
                                        'src/model/sqlite3/external/sqlite3.c'),
                     env_static.Library('src/model/whereami/external/src/libwhereami.a',
                                        'src/model/whereami/external/src/whereami.c')]
env.Alias('lib-static', target_lib_static)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons lib-shared                                                             #
################################################################################

# Load the custom Vala builder
env.Tool('vala')

# Find yat.h in root directory
env.Append(CPPPATH = './')

# Determine wxWidgets dependencies.
# Determine wxC dependencies.
env.Append(CPPPATH = 'src/view/wx/')

env.ParseConfig('wx-config --version=3.0 --cppflags --libs all')
env.ParseConfig('pkg-config --cflags --libs wxc')

# Determine other package dependencies
env.ParseConfig('pkg-config --cflags --libs libsodium')
env.ParseConfig('pkg-config --cflags --libs toxcore')
#env.ParseConfig('pkg-config --cflags --libs libnotify')

# Vala builder doesn't use all the headers and libraries from
# the our Environment which we had parsed recently.
# Lets add them to Vala compiler strings manually.
define = ''

for define_ in env['CPPDEFINES']:

    # gcc macro definitions
    define += '-X "-D'
    if isinstance(define_, list) or \
       isinstance(define_, tuple):
        define += '='.join('{0}'.format(ch) for ch in define_)
    else:
        define += define_
    define += '" '

    # Generic port definitions macro are needed not only as gcc
    # parameters but also as valac parameters
    if define_ == 'PORT_LIBREM5':
        define += '-D PORT_LIBREM5 '

    # wxWidgets port definitions macro are needed not only as gcc
    # parameters but also as valac parameters.
    #
    # Also, general OS macro definitions not related to graphic
    # toolkits are needed.
    if define_ == '__WXGTK__':
        define += '-D __WXGTK__ '
        define += '-D __UNIX '
    elif define_ == '__WXMSW__':
        define += '-D __WXMSW__ '
        define += '-D __WIN '

flags    = "-X '"   +   "' -X '".join(env['CCFLAGS']) + "'"
include  = "-X '-I" + "' -X '-I".join(env['CPPPATH']) + "'"
lib_path = "-X '-L" + "' -X '-L".join(env['LIBPATH']) + "'"
lib      = "-X '-l" + "' -X '-l".join(env['LIBS'   ]) + "'"

# System-wide includes/libraries without .pc.
# Also, they have no Vala VAPIs.
# So no pkg-config and Vala packages here.
env.Append(LIBS = 'notify')

# Configure Vala compiler
vala_flags = flags    + ' ' + \
             define   + ' ' + \
             include  + ' ' + \
             lib_path + ' ' + \
             lib
vala_pkg = ['SSE',
            'cfg',
            'gdk-pixbuf-2.0',
            'gee-0.8',
            'gio-2.0',
            'gmodule-2.0',
            'gstreamer-1.0',
            'gstreamer-app-1.0',
            'gstreamer-audio-1.0',
            'gstreamer-video-1.0',
            'json-glib-1.0',
            'libarchive',
            'libnotify',
            'libpeas-1.0',
            'libtoxav',
            'libtoxcore',
            'libtoxencryptsave',
            'libxml-2.0',
            'posix',
            'sqlite3-SSE',
            'whereami',
            'wxvala']
vala_pkg_path = ['src/',
                 'src/model/sqlite3/',
                 'src/model/sqlite3/external/',
                 'src/model/external/libtoxcore/',
                 'src/model/whereami/',
                 'src/view/wx/']
vala_src_shared = glob.glob('src/model/**/*.vala', recursive = True) + \
                  glob.glob('src/ctrl/**/*.vala',  recursive = True) + \
                  glob.glob('src/view/**/*.vala',  recursive = True)

# Configure Vala builder.
#
# It should build libyat.so.
# Also it generates API for C plugins (yat.h);
# generates API for Vala plugins (yat.vapi);
# generates API for Perl, Python, Lua plugins (yat-X.X.gir).
env['VALACFLAGS'] = vala_flags                                            + ' ' + \
                    "-X '-shared'"                                        + ' ' + \
                    "-X '-fPIC'"                                          + ' ' + \
                    ('', '--save-temps') [dbg == 'on']                    + ' ' + \
                    ('', '-g')           [dbg == 'on']                    + ' ' + \
                    '-H yat.h'                                            + ' ' + \
                    '--library yat'                                       + ' ' + \
                    '--gir yat-' + str(major) + '.' + str(minor) + '.gir' + ' ' + \
                    '--enable-version-header'
                    #'--enable-experimental-non-null'
env['VALACPACKAGES']         = vala_pkg
env['VALACPACKAGEPATHS']     = vala_pkg_path
env['VALAPACKAGESPREFIX']    = '--pkg='
env['_VALACPACKAGES']        = '${ _defines(VALAPACKAGESPREFIX, VALACPACKAGES, None, __env__) }'
env['VALAPACKAGEPATHPREFIX'] = '--vapidir='
env['_VALACPACKAGEPATHS']    = '${ _defines(VALACPACKAGEPATHPREFIX, VALACPACKAGEPATHS, None, __env__) }'

# Special environment for dynamic libraries.
#
# C compiler flags:
#  - position-independent code (PIC).
env_shared = env.Clone()

# Add Vala builder for dynamic libraries
builder_vala_dyn_lib = SCons.Builder.Builder(action     = 'valac ' + '-o $TARGET'                     + ' ' + \
                                                                     env_shared['VALACFLAGS']         + ' ' + \
                                                                     env_shared['_VALACPACKAGEPATHS'] + ' ' + \
                                                                     env_shared['_VALACPACKAGES']     + ' ' + \
                                                                     "$SOURCES",
                                             src_suffix = '.vala',
                                             prefix     = 'lib',
                                             suffix     = '.so')
env_shared.Append(BUILDERS = {'builder_vala_so' : builder_vala_dyn_lib})

# Add GIR compiler builder.
# 
# TODO: allow pass $INC as list to add multiple
# --includedir keys.
builder_gir_compiler = SCons.Builder.Builder(action     = 'g-ir-compiler -o $TARGET'  + ' ' + \
                                                          '--shared-library $DYN_LIB' + ' ' + \
                                                          '--includedir $INC'         + ' ' + \
                                                          "$SOURCE",
                                             src_suffix = '.gir',
                                             suffix     = '.typelib')
env_shared.Append(BUILDERS = {'builder_typelib' : builder_gir_compiler})

# Add shared library target
if sys.platform == 'linux' or sys.platform == 'linux2':
    ext = 'so'
elif sys.platform == 'win32' or sys.platform == 'cygwin':
    ext = 'dll'
else:
    ext = '' # TODO

gir = 'yat-' + str(major) + '.' + str(minor)

target_lib_shared = env_shared.builder_vala_so('libyat.' + ext, vala_src_shared)
target_lib_shared_typelib = env_shared.builder_typelib('src/model/external/libtoxcore/ToxCore-0.10.typelib', \
                                                       'src/model/external/libtoxcore/ToxCore-0.10.gir',     \
                                                       DYN_LIB = 'libtoxcore',
                                                       INC = '.') + \
                            env_shared.builder_typelib(gir + '.typelib',   \
                                                       gir + '.gir',       \
                                                       DYN_LIB = 'libyat', \
                                                       INC = 'src/model/external/libtoxcore/')
env.Alias('lib-shared',  target_lib_shared)
env.Alias('lib-shared-typelib', target_lib_shared_typelib)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons exec                                                                   #
################################################################################

# Vala variables
lib_path = "-X '-L=./'"
lib      = "-X '-lyat'"

vala_flags_exec  = vala_flags
vala_flags_exec += ' ' + lib_path + ' ' + lib

# Configure Vala compiler
vala_src_exec = 'src/main.vala'

# Special environment for executable
env_exec = env.Clone()

# Configure Vala builder
env_exec['VALACFLAGS'] = vala_flags_exec                    + ' ' + \
                         ('', '--save-temps') [dbg == 'on'] + ' ' + \
                         ('', '-g')           [dbg == 'on'] + ' ' + \
                         '--enable-version-header'
                         #"-X '-rdynamic'"
                         #'--enable-experimental-non-null'
if sys.platform == 'win32' or sys.platform == 'cygwin':
    env_exec['VALACFLAGS'] += " -X '" + str(target_resource[0]) + "'"
env_exec['VALACPACKAGES']     += ['yat']
env_exec['VALACPACKAGEPATHS'] += ['./']

# Add executable target
target_exec = env_exec.ValaProgram('yat', vala_src_exec)
if sys.platform == 'win32' or sys.platform == 'cygwin':
    env.Depends(target_exec, target_resource)       # Should build object resource before the program executable
env.Depends(target_exec, target_lib_static)         # ... static libraries too
env.Depends(target_exec, target_lib_shared)         # ... and dynamic libraries too
env.Depends(target_exec, target_lib_shared_typelib) # ... and its GIR typelibs
env.Alias('exec', target_exec)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons xrc                                                                    #
################################################################################

# Add wxFormBuilder builder
builder_wxformbuilder = SCons.Builder.Builder(action     = 'wxformbuilder -g $SOURCE',
                                              ENV        = os.environ, # It avoids error "Error: Unable to initialize GTK+, is DISPLAY set properly?"
                                              src_suffix = '.fbp',
                                              suffix     = '.xrc')
env.Append(BUILDERS = {'builder_xrc' : builder_wxformbuilder})

# Add XRC target
target_xrc = env.builder_xrc('yat.xrc', 'yat.fbp')
env.Alias('xrc', target_xrc)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons pot                                                                    #
################################################################################

# Load the xgettext
env.Tool('xgettext')

# Add wxrc builder
builder_wxrc = SCons.Builder.Builder(action     = 'wxrc --gettext $SOURCE > $TARGET',
                                     src_suffix = '.xrc',
                                     suffix     = '.xrc.tmp')
env.Append(BUILDERS = {'builder_wxrc' : builder_wxrc})

# Configure xgettext builder
env.Append(XGETTEXTFLAGS = '--language=Vala')
env.Append(XGETTEXTFLAGS = '--from-code=UTF-8')
env.Append(XGETTEXTFLAGS = '--join-existing')
env.Append(XGETTEXTFLAGS = '--add-comments')
env.Append(XGETTEXTFLAGS = '--keyword=_')
env.Append(XGETTEXTFLAGS = '--keyword=C_:1c,2') # Support for Vala's GLib.C_() method
env.Append(XGETTEXTFLAGS = '--sort-by-file')
env.Append(XGETTEXTFLAGS = '--package-name=yat')
env.Append(XGETTEXTFLAGS = '--package-version=' + ver)

# Add POT target
target_xrc_tmp = env.builder_wxrc('yat.xrc.tmp', 'yat.xrc')
target_pot = env.POTUpdate('locale/yat.pot', ['yat.xrc.tmp', vala_src_shared, vala_src_exec])
env.Depends(target_xrc_tmp, target_xrc) # Should build XRC before the POT file
env.Depends(target_pot, target_xrc_tmp)
env.Alias('pot', target_pot)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons po_init                                                                #
################################################################################

# Warning! The target silently overwrites existing files, which
# edited manually by translators, but anyway it works only in
# interactive mode.
# Use it only for generating new language PO for the first time.

# Load the msginit
env.Tool('msginit')

# Parse language command line argument
AddOption('--lang', dest    = 'lang',
                    nargs   = 1,
                    type    = 'string',
                    action  = 'store',
                    metavar = 'LOCALE',
                    help    = 'Language to initialize',
                    default = 'ru_RU')
lang = str(GetOption('lang'))

# Configure msginit builder
env.Append(MSGINITFLAGS = '--locale=' + lang + '.UTF-8')

# Add PO generation target
pot = 'locale/yat.pot'
po  = 'locale/' + lang + '/LC_MESSAGES/yat.po'

target_po_init = env.POInit(po, pot)
env.Depends(target_po_init, target_pot) # Should generate POT before the PO files
env.Alias('po_init', target_po_init)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons po_upd                                                                 #
################################################################################

# Load the msgmerge
env.Tool('msgmerge')

# Configure msgmerge builder
env.Append(MSGMERGEFLAGS = '--update')
env.Append(MSGMERGEFLAGS = '--backup=off')
env.Append(MSGMERGEFLAGS = '--sort-by-file')

# Find all PO files
po = []
for file in glob.glob('locale/*/LC_MESSAGES/yat.po'):
    if re.search('\.UTF-8', file):
        continue
    po.append(file)

# Add PO updating target
target_po_upd = []
for po_ in po:
    target_po_upd += env.POUpdate(po_, pot)
env.Depends(target_po_upd, target_pot) # Should generate POT before the PO files
env.Alias('po_upd', target_po_upd)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons mo                                                                     #
################################################################################

# Load the msgfmt
env.Tool('msgfmt')

# Add PO generation target
target_mo = []
for po_ in po:
    mo = re.sub('yat.po$', 'yat.mo', po_)
    target_mo += env.MOFiles(mo, po_)
env.Depends(target_mo, target_po_upd) # Should update PO before the MO files
env.Alias('mo', target_mo)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons valadoc                                                                #
################################################################################

# Load the custom Valadoc builder
env.Tool('valadoc')

# Configure Valadoc builder
env['VALADOCFLAGS'       ] = vala_flags + ' ' + \
                             '--force'
env['VALADOCPACKAGES'    ] = vala_pkg
env['VALADOCPACKAGEPATHS'] = vala_pkg_path

# Add documentation target
target_valadoc = env.Valadoc('valadoc/', vala_src_shared)
env.AlwaysBuild(target_valadoc)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons                                                                        #
# scons all                                                                    #
################################################################################

# Add all target
target_all = [target_exec, target_xrc, target_mo]
env.Alias('all', target_all)
env.Default(target_all) # Invoking scons without arguments will produce executable, XRC, MO i18n

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons install                                                                #
################################################################################

# Configure installation logic
AddOption('--prefix', dest    = 'prefix',
                      nargs   = 1,
                      type    = 'string',
                      action  = 'store',
                      metavar = 'DIR',
                      help    = 'Installation prefix',
                      default = '/usr/local/')
prefix = str(GetOption('prefix'))

# Prepare installation paths
(code, deb_arch) = subprocess.getstatusoutput('dpkg-architecture -qDEB_BUILD_GNU_TYPE')
deb_arch = deb_arch.rstrip() if code == 0 else '.'

dir_bin           = prefix        + 'bin/'
dir_lib           = prefix        + 'lib/' + deb_arch
dir_lib_yat       = dir_lib       + 'yat/'
dir_share         = prefix        + 'share/'
dir_share_apps    = dir_share     + 'applications/'
dir_share_doc     = dir_share     + 'doc/'
dir_share_doc_yat = dir_share_doc + 'yat/'
dir_share_pixmaps = dir_share     + 'pixmaps/'
dir_share_yat     = dir_share     + 'yat/'

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Find all MO i18n files and their directories
    target_install_all = []

    for mo in Glob('locale/*/*/*.mo'):
        mo  = str(mo)
        dir = os.path.dirname(mo)

        src = mo
        dst = dir_share + '/' + dir

        target_install_all += env.Install(dst, src)

    # Make symbolic link for yat icon
    def yat_svg_sym_link(target, source, env):
        os.symlink('../yat/img/yat.svg', str(target[0]))
    def yat_svg_symbolic_sym_link(target, source, env):
        os.symlink('../yat/img/yat-symbolic.svg', str(target[0]))

    # Add install target
    target_install_all += [env.Install(dir_bin,       target_exec),
                           env.Install(dir_lib,       target_lib_shared),
                           env.Install(dir_share_yat, target_lib_shared_typelib),
                           env.Install(dir_share_yat, target_xrc),

                           env.Install(dir_share_apps, 'yat.desktop'),
                           env.Install(dir_share_doc_yat, ['AUTHORS',
                                                           'COPYING',
                                                           'LICENSE',
                                                           'README.md']),
                           env.Install(dir_share_yat, 'tox-nodes.json'),
                           env.Install(dir_share_yat, 'img/'),

                           env.Command(target = dir_share_pixmaps + '/yat.svg',
                                       source = 'img/yat.svg',
                                       action = [yat_svg_sym_link]),
                           env.Command(target = dir_share_pixmaps + '/yat-symbolic.svg',
                                       source = 'img/yat-symbolic.svg',
                                       action = [yat_svg_symbolic_sym_link])]
    dst = env.Alias('install', target_install_all)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons deinstall                                                              #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Configure deinstallation logic
    def deinstall(nodes):
        deletes = []
        for node in nodes:
            if node.__class__ == dst[0].__class__:
                deletes.append(deinstall(node.sources))
            else:
                deletes.append(Delete(str(node)))
        return deletes

    # Add deinstall target
    target_deinstall_all = env.Command('deinstall', '', Flatten(deinstall(dst)) or '')
    env.AlwaysBuild(target_deinstall_all)
    env.Precious(target_deinstall_all)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons linux                                                                  #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Package names
    deb = 'yat_' + ver + '-1_amd64.deb'
    rpm = 'yat-' + ver + '-2.x86_64.rpm'
    tgz = 'yat-' + ver + '.tgz'

    # Add dpkg builder
    builder_dpkg = SCons.Builder.Builder(action         = 'fakeroot dpkg --build $SOURCE $TARGET',
                                         src_suffix     = '',
                                         suffix         = '.deb',
                                         source_factory = env.fs.Dir)
    env.Append(BUILDERS = {'builder_deb' : builder_dpkg})

    # Add alien builder
    builder_alien = SCons.Builder.Builder(action     = 'fakeroot alien $type $SOURCE',
                                          ENV        = os.environ, # It avoids error "Permission denied"
                                          src_suffix = '.deb')
    env.Append(BUILDERS = {'builder_rpm_or_tgz' : builder_alien})

    # Add linux target
    target_deb = env.builder_deb       (target = deb, source = 'debian/')
    target_rpm = env.builder_rpm_or_tgz(target = rpm, source = deb, type = '--to-rpm')
    target_tgz = env.builder_rpm_or_tgz(target = tgz, source = deb, type = '--to-tgz')

    env.AlwaysBuild(target_deb)

    target_linux = [target_deb, target_rpm, target_tgz]
    env.Alias('linux', target_linux)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons must_die                                                               #
################################################################################

# Windows only
if sys.platform == 'win32' or sys.platform == 'cygwin':

    # Configure candle/light builder
    env['WIXCANDLEFLAGS'].append('-arch')
    env['WIXCANDLEFLAGS'].append('x64')
    env['WIXCANDLEFLAGS'].append('-ext')
    env['WIXCANDLEFLAGS'].append('WixUIExtension')

    env['WIXLIGHTFLAGS'].append('-b')
    env['WIXLIGHTFLAGS'].append('win/')
    env['WIXLIGHTFLAGS'].append('-cultures:ru-RU') # TODO: add multi-language support
    env['WIXLIGHTFLAGS'].append('-ext')
    env['WIXLIGHTFLAGS'].append('WixUIExtension')

    # Find all required DLLs
    dll = subprocess.getoutput('ldd yat.exe | grep /mingw64 | sed "s/.*\=> \(.*\) (.*/\\1/"')
    dll = dll.splitlines()

    target_install_win = []
    for dll_ in dll:
        target_install_win += env.Install('win/', dll_)

    # Find all MO i18n files and their directories
    for mo in Glob('locale/*/*/*.mo'):
        mo  = str(mo)
        dir = os.path.dirname(mo)

        src = mo
        dst = 'win/' + dir

        target_install_win += env.Install(dst, src)

    # Do local installation
    target_install_win += [env.Install('win/', 'yat.exe'),
                           env.Install('win/', target_xrc),
                           env.Install('win/', ['AUTHORS',
                                                'COPYING',
                                                'LICENSE',
                                                'README.md']),
                           env.Install('win/', 'img/')]

    # Add heat builder.
    # Add candle builder. (We can't use standard tool from SCons,
    # because build for multiple sources is required.)
    # Add light builder. (The same.)
    builder_heat   = SCons.Builder.Builder(action     = 'heat dir $SOURCE -cg Product_ -dr Product -gg -platform x64 -nologo -sfrag -srd -sw5150 -out $TARGET',
                                           src_suffix = '',
                                           suffix     = '.wxs',
                                           source_factory = env.fs.Dir)
    builder_candle = SCons.Builder.Builder(action     = '$WIXCANDLECOM',
                                           suffix     = '$WIXOBJSUF',
                                           src_suffix = '$WIXSRCSUF')
    builder_light  = SCons.Builder.Builder(action     = '$WIXLIGHTCOM',
                                           suffix     = '.msi',
                                           src_suffix = '$WIXOBJSUF')

    env.Append(BUILDERS = {'builder_wxs'    : builder_heat})
    env.Append(BUILDERS = {'builder_wixobj' : builder_candle})
    env.Append(BUILDERS = {'builder_msi'    : builder_light})

    # Add must die target
    target_wxs        = env.builder_wxs('dir.wxs', 'win/')
    target_wixobj_yat = env.builder_wixobj('yat.wxobj', 'yat.wxs')
    target_wixobj_dir = env.builder_wixobj('dir.wxobj', 'dir.wxs')
    target_die_must   = env.builder_msi(target = 'yat_' + ver + '_amd64.msi',
                                        source = ['yat.wxobj', 'dir.wxobj'])

    env.AlwaysBuild(target_wxs)

    env.Depends(target_wxs,        target_install_win)
    env.Depends(target_wixobj_yat, target_install_win)
    env.Depends(target_wixobj_dir, target_install_win)
    env.Depends(target_wixobj_dir, target_wxs)
    env.Depends(target_die_must,   target_install_win)
    env.Depends(target_die_must,   target_wixobj_yat)
    env.Depends(target_die_must,   target_wixobj_dir)

    env.Alias('must_die', target_die_must)

################################################################################
# scons --clean                                                                #
# scons all --clean                                                            #
################################################################################

# Configure --clean flag
if dbg == 'on':
    env.Clean(target_exec, list(set(glob.glob('src/**/*.c', recursive = True))   -   # Clean all *.c sources
                                set(glob.glob('src/model/sqlite3/external/*.c')) -   # ... excluding static libraries, which are not produded by valac,
                                set(['src/model/whereami/external/src/whereami.c'])) # ...
             )
env.Clean(target_exec, ['yat.h', 'yat.vapi', Glob('yat-*.gir')]) # Clean yat API
env.Clean(target_pot, 'locale/yat.pot')
env.Clean(target_valadoc, Glob('valadoc/*'))
env.Clean(target_all, 'site_scons/site_tools/vala/__pycache__/')
if sys.platform == 'win32' or sys.platform == 'cygwin':
    env.Clean(target_die_must, 'yat_' + ver + '_amd64.wixpdb')

# vim: ts=4:sw=4:sts=4:et
