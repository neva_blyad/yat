# NAME

yat

# VERSION

0.11.0 "Daria Videophone Edition"

# DESCRIPTION

yat is Yet Another Tox.

Also yat (яд) in Russian means poison.

So it is the Tox client.

Tox is a peer-to-peer (P2P) instant-messaging and video-calling protocol that
offers end-to-end encryption (E2EE).

Tox uses decentralized network (DHT). It means there are no central servers that
can be raided or shutdowned by police or government. None of existing today IMs
use decentralized architecture.

One day Tox replaces Skype, WhatsApp, Viber, Telegram, Zoom, Matrix and all
other proprietary and/or centralized IM applications.

Fight corporations! Fight government! Fight for your freedom!

![Alt Text](doc/acab.png "ACAB")

*Booklet from CD: The Prodigy - More Music For The Jilted Generation*

# FEATURES

- [x] Usable, **truly native**, bug-free and user-friendly interface
- [x] 1v1 messages
- [x] Conferences (groupchats)
- [x] Typing notifications. (And you can disable the issuing them from your
      side!)
- [x] GUI, audio and native desktop notifications. (May be disabled.)
- [x] Conversation history
- [x] User states, status messages, avatars
- [x] Avatars are really big! 200x200 pixels, more than all other clients have.
- [x] Buddy/Conference local Aliases
- [ ] Pseudo-offline messaging
- [ ] File transfers, file resuming
- [x] Multilingual
- [x] Multiprofile
- [x] **Full** profile password encryption
- [x] Import profiles from other Tox clients. (uTox, qTox are tested.)
- [x] Proxy/SOCKS support
- [ ] Spell check
- [x] Audio calls
- [x] Video calls
- [ ] Desktop sharing
- [x] Plugin support
- [x] One-time seSSions support. (All information exists only in memory, no
      disk storage, so cops can't catch you. Paranoid mode.)

# HIGHLIGHTS AND BACKGROUND

The program is Free/Libre and Open Source Software (FLOSS). There is only one
right software license in the world. You are completely free to both use and
modify yat.

The program comes without any advertising. yat will never require you to pay
for features.

The program is cross-platform: GNU/Linux, UNIX, M$ Windows, Apple macOS are
all supported. It is based on wxWidgets library, so the GUI is native for each
OS, for example, it uses GTK+ for GNU, MFC for Windows, Cocoa for macOS. No
fucking control emulation like Qt or Java does.

The program is based and fully supports the toxcore2 library, currently
developed Tox protocol reference implementation.

The program is the only Tox client in the world which has GTK interface under
Unix-like systems. (Ricin/Konv.im also have but they are dead.)

The program fully encrypts all your data, not only which sent but which is
stored on PC too. Other Tox clients do not. Tox ID, buddy list, even
conversation history and program settings are all encrypted. There are no
unencrypted files stored on PC! (qTox, for example, doesn't encrypt friend list
and history.)

The program is written in Vala. It is fast and simple as C. It also powerful but
has no speed, memory, syntax overhead like other high-level languages, e. g.
C++, Java or C#.

The program uses fast & cross-platform underneath libraries: GLib/GObject
backend, GStreamer multimedia framework, SQLite 3 database engine, libpeas
plugin support.

# SCREENSHOTS

![Alt Text](doc/screenshot-linux.png "yat screenshot under GNU/Linux")
*yat 0.10.1 under GNU/Linux*

![Alt Text](doc/screenshot-win.png "yat screenshot under M$ Windows")
*yat 0.5.8 under M$ Windows*

![Alt Text](doc/screenshot-macos.png "yat screenshot under Apple macOS")
*yat 0.5.8 under Apple macOS*

# SCREENCAST

![Alt Text](doc/screencast.webm "yat screencast")

# PORTS

![Alt Text](doc/screenshot-librem5.png "yat screenshot under Purism Librem 5")
*yat 0.10.1-videocalls under Purism Librem 5 (buggy and experimental!)*

# INSTALL

INSTALL file describes how to install the yat from scratch.

You do not need it to use yat.

Download binaries and skip all the INSTALL instructions:

+ **Deb-based GNU/Linux distro**

  ![Alt Text](doc/logo-debian.png "Logo Debian")

  http://www.lovecry.pt/dl/yat_0.11.0-1_amd64.deb |
  http://www.lovecry.pt/dl/yat-plugins_0.11.0-1_amd64.deb

  http://www.lovecry.pt/dl/yat_0.11.0-1_arm64.deb |
  http://www.lovecry.pt/dl/yat-plugins_0.11.0-1_arm64.deb

  Dependencies can be downloaded from official repository. Also install wxC:

  http://www.lovecry.pt/dl/libwxc_0.93.0.0-8-1_amd64.deb

  http://www.lovecry.pt/dl/libwxc_0.93.0.0-8-1_arm64.deb

+ **RPM-based GNU/Linux distro**
  *(Not tested!)*

  ![Alt Text](doc/logo-redhat.png "Logo Red Hat")

  http://www.lovecry.pt/dl/yat-0.11.0-2.x86_64.rpm |
  http://www.lovecry.pt/dl/yat-plugins-0.11.0-2.x86_64.rpm

  http://www.lovecry.pt/dl/yat-0.11.0-2.arm64.rpm |
  http://www.lovecry.pt/dl/yat-plugins-0.11.0-2.arm64.rpm

  Dependencies can be downloaded from official repository. Also install wxC:

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8-2.x86_64.rpm

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8-2.arm64.rpm

+ **Slackware and source-based GNU/Linux distros**
  *(Not tested!)*

  ![Alt Text](doc/logo-slackware.png "Logo Slackware")

  http://www.lovecry.pt/dl/yat-0.11.0.amd64.tgz |
  http://www.lovecry.pt/dl/yat-plugins-0.11.0.amd64.tgz

  http://www.lovecry.pt/dl/yat-0.11.0.arm64.tgz |
  http://www.lovecry.pt/dl/yat-plugins-0.11.0.arm64.tgz

  Dependencies can be downloaded from official repository. Also install wxC:

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8.amd64.tgz

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8.arm64.tgz

+ **General GNU/Linux distro / Flatpak**

  ![Alt Text](doc/logo-flatpak.png "Logo Flatpak")

  http://www.lovecry.pt/dl/yat-0.11.0_amd64.flatpak

+ **M$ Windows**

  ![Alt Text](doc/logo-win.png "Logo Windows") http://www.lovecry.pt/dl/yat_0.5.8_amd64.msi

+ **Apple macOS**

  TODO

# FAQ

+ Why wxWidgets?

See above. It's native for each platform.

+ Why SCons?

Because.

SCons in Russian sounds like skunk (скунс).
yat is Tox, skunk is toxic too.

+ Why libpeas?

Because.

libpeas sounds like pee.
In Russian it is similar to peeing too (писать).
We enjoy to do peeing.

+ Why using own fork of libpeas?

Because mainline libpeas only supports Python and Lua scripting.
Our build, on the other hand, is patched version with Perl plugin support.
One day we hope to merge it with upstream version.

+ Why using own static embedded build of SQLite 3?

Original SQLite doesn't support read and write operations with raw data in RAM.
You could open SQLite database stored in your filesystem as file and perform
some actions on it, but it is not option for us. All our files stored on disk
are encrypted, it is requirement. We read database from encrypted file, decrypt
it and then use special SQLite functions for working with it directly in RAM.
Only our modified version of SQLite supports these functions. So we can't use
dynamic library of SQLite shipped with GNU/Linux distributions, MSYS builds,
etc.

+ Why some buddies have no Tox ID?

We don't know Tox IDs of those buddies whom we didn't add ourselves. The Tox IDs
of those buddies whom we added upon the incoming request are unknown. Only their
public keys are known. It is restriction of the Tox protocol. All other Tox
clients work the same shitty way.

+ Does Tox support short nice-looking names?

There were Tox resolution servers in past which could convert 76-hexacedimal
character Tox ID to AT-separated string similar to email addresses (or vice
versa?), e. g. toxer@toxme.io.

It is adviced not to use such servers because it violates Tox philisophy and
idea of DHT/P2P messaging.

However, try to use our Web-based Tox name resolution service:
http://tox.lovecry.pt:8080

SSL and maybe integration with yat via plugin are coming soon.

+ P2P means my buddies can see my IP address. How to hide my IP?

Use Tor. For example, start Tor browser (or, better, install separate Tor
daemon) and setup proxy in yat's "Settings" dialog:

Type: SOCKS5
Host: localhost or 127.0.0.1
Port: 9150

All yat traffic will go through Tor onion network then.

+ What about VPN?

Yes. It it another solution to hide your IP.
But don't use free VPNs from freely available lists you'd found in Web.

+ How to synchronize multiple yat clients (logs, buddy lists, etc), e. g. mobile
and desktop?

Try to use Unison.
It is possible that one day we would implement our own (optional)
synchronization method.

+ English Wikipedia says you'd worked for Russian police department. WTF? Is it
  true?

It is bullshit.

# FOR HACKERS

+ Plugins are small programs. They definitely have soul. Plugins have good mood
  and feel happy when they are together and when there are many of them. Sadly,
  by now we have just few plugins.

  Write plugins.

  yat's plugin support is based on libpeas. Five programming languages are
  available for you to write plugins.

  There are two types of plugins.
  There are demo examples for each.

  1. Vala and pure C plugins are fully supported.
     These plugins are compiled into dynamic libraries and loaded into
     application memory.

     yat API you have to use in this case is C header or Vala's VAPI.
     (yat.h for C plugins, yat.vapi for Vala plugins.)

  2. Plugins written in script interpreted languages are supported due to
     GObject introspection: Perl, Python 3, Lua.

     (Possibly, loaders for other script languages would be implemented in
     future.)

     yat presents its API as GIR's typelib file you have to use for writing
     script plugins.
     (yat-*.*.gir, yat-*.*.typelib)

  Subscribe to yat's events using GObject signals.

  (See plugins/.
  Also see libpeas demo examples:
  https://gitlab.com/neva_blyad/libpeas/-/tree/libpeas-perl)

+ For using gdb you should build executable with debug symbols:

      $ scons --dbg=on
      $ cd plugins/
      $ scons --dbg=on
      $ cd ../
      $ ./gdb.sh -vvvVVV

+ Catch warnings and errors with gdb:

      $ G_DEBUG=fatal-criticals ./gdb.sh
      $ G_DEBUG=fatal-warnings  ./gdb.sh --gst-fatal-warnings

+ yat knows how to pass arguments from CLI directly to GStreamer multimedia
  framework. Ask it to show you all of the GStreamer options:

      ./yat.sh --help-gst

+ Build and read documentation for programmers:

      $ sudo apt install valadoc
      $ scons valadoc
      $ firefox valadoc/valadoc/index.htm

+ Show all audio hardware in "Settings" dialog, even hidden:

      $ scons --show-hidden-audio-hw=on

# FOR TRANSLATORS

1. Build the program.
   See INSTALL for details how to do it.
2. Generate a system locale for the language you want to translate to:

       $ sudo dpkg-reconfigure locales

   For example, add de_DE.UTF-8.
3. Generate new PO files:

       $ scons po_init --lang=de_DE # It's German. Replace language by yours.

   At first it generates XRC and POT files.
4. It may ask you to invoke msginit command manually. Do it.
5. Edit generated *locale/de/LC_MESSAGES/yat.po* with your favourite text
   editor or special software like Poedit.
6. Generate MO files:

       $ scons mo

7. Test the results:

       $ LANG=de_DE.UTF-8 ./yat.sh -vvVV # Again, replace language by yours

8. Send me your PO file.
   I'll include the translation in next version of yat.

9. Now let's imagine you want to translate some plugin.
   Do steps 1-2. Then:

        $ cd plugins/
        $ scons pot
        $ scons po_init --lang=de_DE --plugin=demo # Replace plugin name and language by yours

   Now do steps 4-6. Then:

        $ cd ../

   Now do steps 7-8.
   That's all.

# WANTED

We are highly interested in software developers and engineers, package
maintainers and distro enthusiasts, QA engineers, hackers and plugin punks,
translators, icon drawers or simply advanced users who are ready to give us his
hand and want to help.

As you see, Windows build is old enough, macOS variant is not even started, BSD
and OpenSolaris ports are desired. (Though codebase we do is fully
platform-independent and libraries we are using are cross-platform friendly.)

Librem 5 port (aarch64 / arm64) needs attention, care, love, patience. Builds
for all OSes need to be tested. Language translations and plugins are very very
needed.

Just mail us.

Let's do free and libre A/V messenger together!

# COPYRIGHT AND LICENSE

    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# IN LOVING MEMORY OF

![Alt Text](doc/VikaBeseda.jpg "Вика Беседа")

*Вика Беседа*

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
