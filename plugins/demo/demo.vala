/*
 *    demo.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

// Singletons
extern Model.Model *model;
extern View.View   *view;
extern Ctrl.Ctrl   *ctrl;

Model.ModelDemo *model_demo;
View.ViewDemo   *view_demo;
Ctrl.CtrlDemo   *ctrl_demo;

/*----------------------------------------------------------------------------*/

// Register types used by the plugin.
//
// Gets the reference to the plugin object as parameter.
//
// Note: ModuleInit attribute marks that this function should
// register the plugin's types with GObject type system.
[ModuleInit]
public void peas_register_types(GLib.TypeModule module)
{
    // Types to be registered
    var obj = module as Peas.ObjectModule;
    obj.register_extension_type(typeof(Model.Plugin), typeof(Ctrl.CtrlDemo));
}

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * This is the Model class for Demo plugin
     */
    public class ModelDemo : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Information about this plugin.
        //
        // Warning! We don't use properties here because they are buggy
        // when used dynamically.
        private Peas.PluginInfo info;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Demo Model constructor
         */
        public ModelDemo()
        {
            // Save reference to plugin information.
            // We will need this in the future.
            this.info = model->plugins_ext.engine.get_plugin_info("demo");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Demo Model destructor
         */
        ~ModelDemo()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get reference to this plugin
         *
         * @return The plugin information
         */
        public Peas.PluginInfo info_get()
        {
            return this.info;
        }
    }
}

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the View class for Demo plugin
     */
    public class ViewDemo : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        // XML Based Resource System (XRC)
        public Wx.XMLResource xml;

        // "Preferences" dialog
        public DlgPreferencesDemo dlg_preferences;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize XRC.
        //
        // Loads the XRC file.
        private void xrc_init()
        {
            // XRC filename.
            // This file contains GUI description in XML format.
            Peas.PluginInfo info = model_demo->info_get();
            unowned string name  = info.get_module_name();
            string filepath      = GLib.Path.build_filename(model->path_plugins,
                                                            name,
                                                            name + ".xrc");

            // Load the XRC file
            this.xml = new Wx.XMLResource(1);
            var res = this.xml.load(new Wx.Str(filepath));
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets.
        private void widgets_init()
        {
            // Allocate widgets
            this.dlg_preferences = new DlgPreferencesDemo(this.xml);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Demo View constructor.
         *
         * Initializes XRC.
         * Initializes widgets.
         */
        public ViewDemo()
        {
            xrc_init();
            widgets_init();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Demo View destructor
         */
        ~ViewDemo()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
          * Set/clear demo message in the status bar
          *
          * @param flag false: clear status bar,
          *             true:  set status bar
          */
        public void frm_main_status_val_set(bool flag)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = ctrl_demo->name_get(model, info);
            string          val  = flag ? _("Set by %s plugin").printf(name) : "";

            // Set/clear the message in the status bar
            view->frm_main.status.val_set(FrmMain.STATUS_COL_EVT, val);
        }
    }

    /**
     * This is the "Preferences" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    public class DlgPreferencesDemo : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * "Set demo message in the status bar" check box
         */
        public Wrapper.CheckBox check_status;

        /**
         * "Show message dialog on incoming message" check box
         */
        public Wrapper.CheckBox check_msg_dlg;

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel;

        /**
         * "Apply" button
         */
        public Wrapper.Btn btn_apply_settings;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        // Setups the widgets.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.check_status       = new Wrapper.CheckBox();
            this.check_msg_dlg      = new Wrapper.CheckBox();
            this.btn_cancel         = new Wrapper.Btn();
            this.btn_apply_settings = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                    = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_preferences_demo"));
            GLib.assert(this.win                    != null);
            this.check_status.win       = this.win.win_find(new Wx.Str("check_status"));
            GLib.assert(this.check_status.win       != null);
            this.check_msg_dlg.win      = this.win.win_find(new Wx.Str("check_msg_dlg"));
            GLib.assert(this.check_msg_dlg.win      != null);
            this.btn_cancel.win         = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win         != null);
            this.btn_apply_settings.win = this.win.win_find(new Wx.Str("btn_apply_settings"));
            GLib.assert(this.btn_apply_settings.win != null);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Destroys all windows. They will be deleted in the loop.
         */
        private void win_deinit()
        {
            // Destroy all windows
            this.win.destroy();
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.btn_apply_settings.win.id_get();
            closure = new Wx.Closure(Ctrl.DlgPreferencesDemo.btn_apply_settings_clicked, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize events.
        //
        // Deletes events for the widgets.
        private void events_deinit()
        {
            // Delete events for the widgets
            int id;

            id = this.btn_apply_settings.win.id_get();

            Wx.EventHandler.disconnect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), 0);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgPreferencesDemo(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog destructor.
         *
         * Destroys all windows. They will be deleted in the loop.
         * Deinitializes events.
         */
        ~DlgPreferencesDemo()
        {
            events_deinit();
            win_deinit();
        }
    }
}

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the Controller class for the Demo plugin
     */
    public class CtrlDemo : GLib.Object, Model.Plugin
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Model.
         *
         * Not used, direct access to model reference is available.
         */
        public unowned Model.Model           model_ { public get; public construct; } 

        /**
         * View.
         *
         * Not used, direct access to view reference is available.
         */
        public unowned View.View             view_  { public get; public construct; } 

        /**
         * Controller.
         *
         * Not used, direct access to ctrl reference is available.
         */
        public unowned global::yat.Ctrl.Ctrl ctrl_  { public get; public construct; } 

        /**
         * Tox callbacks
         */
        public ToxCoreDemo tox_core;

        /**
         * "Preferences" dialog callbacks
         */
        public DlgPreferencesDemo dlg_preferences;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Plugin entry point
         */
        public void plugin_load()
        {
            // This plugin uses a Model-View-Controller architecture.
            // Initialize MVC singleton objects.
            // Controller is already instantiated by libpeas.
            model_demo = new Model.ModelDemo();
            view_demo  = new View.ViewDemo();
            ctrl_demo  = this;

            // Initialize Controller (instead of constructor)
            ctrl_demo->init();

            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = ctrl_demo->name_get(model, info);

            GLib.debug(_("Start %s %s").printf(name, info.get_version()));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin exit point
         */
        public void plugin_unload()
        {
            GLib.debug(_("Stop"));

            // Deinitialize Controller (instead of destructor)
            ctrl_demo->deinit();

            // Deinitialize MVC singleton objects.
            // Controller will be deleted by libpeas later.
            delete model_demo;
            delete view_demo;
            ctrl_demo = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration availability
         *
         * @return false: the plugin isn't configurable,
         *         true:  the plugin is configurable
         */
        public bool plugin_cfg_check()
        {
            // This plugin is configurable
            GLib.debug(_("Configuration is available"));
            return true;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration entry point
         */
        public void plugin_cfg_exec()
        {
            // Show the "Preferences" dialog
            GLib.debug(_("Execute configuration"));
            view_demo->dlg_preferences.show(true);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize the Demo Controller.
         *
         * Checks/unchecks check boxes if needed.
         * Sets/clears demo message in the status bar.
         * Initializes callbacks.
         *
         * Warning! We don't use constructor because of the constraint
         * of the GObject type system. Constructor is emulated.
         */
        public void init()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = info.get_module_name();

            // Should we set the status bar message?
            bool set_status;

            try
            {
                set_status = model->self.cfg.get_boolean(name, "set_status");
            }
            catch (GLib.KeyFileError ex)
            {
                model->self.cfg.set_boolean(name, "set_status", true);
                set_status = true;
            };

            // Uncheck "Set demo message in the status bar" check box if
            // needed.
            // Set/clear demo message in the status bar.
            if (!set_status)
                view_demo->dlg_preferences.check_status.val_set(false);
            view_demo->frm_main_status_val_set(set_status);

            // Should we show dialog on incoming message?
            bool show_dlg;

            try
            {
                show_dlg = model->self.cfg.get_boolean(name, "show_dlg_on_incoming_msg");
            }
            catch (GLib.KeyFileError ex)
            {
                model->self.cfg.set_boolean(name, "show_dlg_on_incoming_msg", true);
                show_dlg = true;
            };

            // Uncheck "Show message dialog on incoming message" check box
            // if needed
            if (!show_dlg)
                view_demo->dlg_preferences.check_msg_dlg.val_set(false);

            // Tox callbacks.
            // Dialog UI callbacks.
            this.tox_core = new ToxCoreDemo();
            this.dlg_preferences = new DlgPreferencesDemo();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Deinitialize the Demo Controller.
         *
         * Clears demo message in the status bar.
         *
         * Warning! We don't use destructor because of the constraint
         * of the GObject type system. Destructor is emulated.
         */
        public void deinit()
        {
            // Clear demo message in the status bar
            view_demo->frm_main_status_val_set(false);

            // Disconnect signals from callback methods.
            //
            // Can't move this call to ~ToxCoreDemo(), because that
            // destructor would never been called then.
            this.tox_core.signals_disconnect();

            // Tox callbacks.
            // Frame and dialog UI callbacks.
            //
            // Should delete manually, because there will be no destructor
            // call ~CtrlDemo(), which should call destructors of its
            // members subsequently.
            this.tox_core = null;
            this.dlg_preferences = null;
        }
    }

    /**
     * Tox class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    public class ToxCoreDemo : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreDemo()
        {
            signals_connect();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback destructor.
         *
         * Disconnects signals from callback methods.
         */
        ~ToxCoreDemo()
        {
            //signals_disconnect(); // Should do it manually, otherwise destructor would not been ever called
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Connect signals to callback methods.
         *
         * This plugin uses signals which are located in main
         * application.
         */
        public void signals_connect()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = info.get_module_name();

            // Should we show dialog on incoming message?
            bool show_dlg;

            try
            {
                show_dlg = model->self.cfg.get_boolean(name, "show_dlg_on_incoming_msg");
            }
            catch (GLib.KeyFileError ex)
            {
                show_dlg = true;
                GLib.assert(false);
            };

            // Connect signals to callback methods.
            //
            // This plugin uses signal which is the handler for Tox incoming
            // messages. It is located in main application.
            if (show_dlg)
                ctrl->tox_core.buddy_rx_sig.connect(buddy_rx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disconnects signals from callback methods
         */
        public void signals_disconnect()
        {
            ctrl->tox_core.buddy_rx_sig.disconnect(buddy_rx);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when a message from a buddy is
         * received.
         *
         * @param tox   Tox instance
         * @param uid   Buddy unique identifier
         * @param type  Message type
         * @param msg   Message
         * @param param Not used currently
         */
        public void buddy_rx(global::ToxCore.Tox          tox,
                             uint32                       uid,
                             global::ToxCore.MessageType  type,
                             uint8[]                      msg,
                             void                        *param)
            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            Model.Buddy  buddy        = model->buddies_hash[uid];
            string       display_name = buddy.display_name_get();
            string       msg_str      = Model.Model.arr2str_convert(msg);
            string       txt          = _("Incoming message from buddy: %s: %d characters").printf(display_name, msg_str.char_count());

            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = ctrl_demo->name_get(model, info);

            // Unlock mutex.
            // Show message dialog.
            // Lock mutex.
            model->mutex.unlock();
            var msg_dlg = new View.Wrapper.MsgDlg(view->frm_main, txt, name);
            msg_dlg.modal_show();
            model->mutex.lock();
        }
    }

    /**
     * This is the "Preferences" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    public class DlgPreferencesDemo : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog callback destructor
         */
        public DlgPreferencesDemo()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog callback destructor
         */
        ~DlgPreferencesDemo()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Apply" button click callback
         *
         * @param fn    Not used currently
         * @param param Not used currently
         * @param event Not used currently
         *
         * @return null
         */
        public static void *btn_apply_settings_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Plugin description fields
            Peas.PluginInfo info = model_demo->info_get();
            unowned string  name = info.get_module_name();

            // Get check box values
            bool check_status_flag  = view_demo->dlg_preferences.check_status.val_get();
            bool check_msg_dlg_flag = view_demo->dlg_preferences.check_msg_dlg.val_get();

            // Connect/disconnect signals to/from callback method
            try
            {
                if (check_msg_dlg_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "show_dlg_on_incoming_msg"))
                        ctrl->tox_core.buddy_rx_sig.connect(ctrl_demo->tox_core.buddy_rx);
                }
                else
                    ctrl->tox_core.buddy_rx_sig.disconnect(ctrl_demo->tox_core.buddy_rx);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }

            // Update per-profile config with value from GUI
            model->self.cfg.set_boolean(name, "set_status",               check_status_flag);
            model->self.cfg.set_boolean(name, "show_dlg_on_incoming_msg", check_msg_dlg_flag);

            // Set/clear demo message in the status bar
            view_demo->frm_main_status_val_set(check_status_flag);

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }
    }
}

