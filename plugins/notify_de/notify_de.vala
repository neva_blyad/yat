/*
 *    notify_de.vala
 *    Copyright (C) 2021-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

// Singletons
extern Model.Model *model;
extern View.View   *view;
extern Ctrl.Ctrl   *ctrl;

yat.Model.ModelNotifyDE *model_notify_de;
yat.View.ViewNotifyDE   *view_notify_de;
yat.Ctrl.CtrlNotifyDE   *ctrl_notify_de;

/*----------------------------------------------------------------------------*/

// Register types used by the plugin.
//
// Gets the reference to the plugin object as parameter.
//
// Note: ModuleInit attribute marks that this function should
// register the plugin's types with GObject type system.
[ModuleInit]
public void peas_register_types(GLib.TypeModule module)
{
    // Types to be registered
    var obj = module as Peas.ObjectModule;
    obj.register_extension_type(typeof(Model.Plugin), typeof(Ctrl.CtrlNotifyDE));
}

/*----------------------------------------------------------------------------*/

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * This is the Model class for DE Notifications plugin
     */
    public class ModelNotifyDE : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Information about this plugin.
        //
        // Warning! We don't use properties here because they are buggy
        // when used dynamically.
        private Peas.PluginInfo info;

        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Active notifications.
         *
         * Hash key is notification ID.
         */
        public Gee.HashMap<int, Notify.Notification> notifications;

        /**
         * Current notification ID
         */
        public int id;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * DE Notifications Model constructor
         */
        public ModelNotifyDE()
        {
            // Save reference to plugin information.
            // We will need this in the future.
            this.info = model->plugins_ext.engine.get_plugin_info("notify_de");

            // Active notifications
            this.notifications = new Gee.HashMap<int, Notify.Notification>();

            // Current notification ID
            this.id = 0;

            // Initialize notifications
            bool res = Notify.init(Cfg.NAME);
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * DE Notifications Model destructor
         */
        ~ModelNotifyDE()
        {
            // Deinitialize notifications
            Notify.uninit();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get reference to this plugin
         *
         * @return The plugin information
         */
        public Peas.PluginInfo info_get()
        {
            return this.info;
        }
    }
}

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the View class for DE Notifications plugin
     */
    public class ViewNotifyDE : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * DE Notifications View constructor
         */
        public ViewNotifyDE()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * DE Notifications View destructor
         */
        ~ViewNotifyDE()
        {
        }
    }
}

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the Controller class for the DE Notifications
     * plugin
     */
    public class CtrlNotifyDE : GLib.Object, Model.Plugin
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Model.
         *
         * Not used, direct access to model reference is available.
         */
        public unowned Model.Model           model_ { public get; public construct; }

        /**
         * View.
         *
         * Not used, direct access to view reference is available.
         */
        public unowned View.View             view_  { public get; public construct; }

        /**
         * Controller.
         *
         * Not used, direct access to ctrl reference is available.
         */
        public unowned global::yat.Ctrl.Ctrl ctrl_  { public get; public construct; }

        /**
         * Tox callbacks
         */
        public ToxCoreNotifyDE tox_core;

        /**
         * Tox AV callbacks
         */
        public ToxCoreAVNotifyDE tox_core_av;

        /**
         * Notification button callbacks.
         *
         * Open buddy in buddy list.
         */
        public Notify.ActionCallback open_buddy_clicked_cb;

        /**
         * Notification button callbacks.
         *
         * Accept audio call.
         */
        public Notify.ActionCallback accept_clicked_cb;

        /**
         * Notification button callbacks.
         *
         * Reject/Finish audio call.
         */
        public Notify.ActionCallback reject_finish_clicked_cb;

        /**
         * Notification button callbacks.
         *
         * Open "Buddy Requests" tab.
         */
        public Notify.ActionCallback open_buddy_req_clicked_cb;

        /**
         * Notification button callbacks.
         *
         * Open conference in conference list.
         */
        public Notify.ActionCallback open_conference_clicked_cb;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Plugin entry point
         */
        public void plugin_load()
        {
            // This plugin uses a Model-View-Controller architecture.
            // Initialize MVC singleton objects.
            // Controller is already instantiated by libpeas.
            model_notify_de = new Model.ModelNotifyDE();
            view_notify_de  = new View.ViewNotifyDE();
            ctrl_notify_de  = this;

            // Initialize Controller (instead of constructor)
            ctrl_notify_de->init();

            // Plugin description fields
            Peas.PluginInfo info = model_notify_de->info_get();
            unowned string  name = ctrl_notify_de->name_get(model, info);

            GLib.debug(_("Start %s %s").printf(name, info.get_version()));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin exit point
         */
        public void plugin_unload()
        {
            GLib.debug(_("Stop"));

            // Deinitialize Controller (instead of destructor)
            ctrl_notify_de->deinit();

            // Deinitialize MVC singleton objects.
            // Controller will be deleted by libpeas later.
            delete model_notify_de;
            delete view_notify_de;
            ctrl_notify_de = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration availability
         *
         * @return false: the plugin isn't configurable,
         *         true:  the plugin is configurable
         */
        public bool plugin_cfg_check()
        {
            // This plugin is not configurable
            GLib.debug(_("Configuration is not available"));
            return false;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration entry point
         */
        public void plugin_cfg_exec()
        {
            GLib.assert(false);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize the DE Notifications Controller.
         *
         * Initializes callbacks.
         *
         * Warning! We don't use constructor because of the constraint
         * of the GObject type system. Constructor is emulated.
         */
        public void init()
        {
            // Tox callbacks
            this.tox_core    = new ToxCoreNotifyDE();
            this.tox_core_av = new ToxCoreAVNotifyDE();

            // Notification button callbacks.
            //
            // Open buddy in buddy list.
            this.open_buddy_clicked_cb = (notification, action) =>
            {
                // Lock mutex
                model->mutex.lock();

                // Ensure the buddy hadn't been removed to this time
                unowned Model.Buddy? buddy = notification.get_data<Model.Buddy>("buddy");

                if (!model->buddies_hash.has_key(buddy.uid))
                    buddy = null;

                // Do the buddy selection:
                //  - update GUI controls,
                //  - select last messages from database and append them to text
                //    control
                ctrl->frm_main.lst_ctrl_buddies_sel(buddy);

                // Close notification
                try                   { notification.close();   }
                catch (GLib.Error ex) { GLib.debug(ex.message); }

                // Unlock mutex
                model->mutex.unlock();
            };

            // Accept audio call
            this.accept_clicked_cb = (notification, action) =>
            {
                // Lock mutex
                model->mutex.lock();

                // Ensure the buddy hadn't been removed to this time
                unowned Model.Buddy? buddy = notification.get_data<Model.Buddy>("buddy");

                if (!model->buddies_hash.has_key(buddy.uid))
                    buddy = null;

                // Do the buddy selection:
                //  - update GUI controls,
                //  - select last messages from database and append them to text
                //    control
                ctrl->frm_main.lst_ctrl_buddies_sel(buddy);

                // Ignore if outdated
                if (buddy == null || !((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                                       (buddy.action_to_us   & Model.Buddy.state_t.MASK_DOING_AV_CALL) > 0 &&
                                       (buddy.action_to_us   & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) == 0))
                {
                    GLib.debug(_("Notification outdated, ignoring the clicking"));

                    // Close notification
                    try                   { notification.close();   }
                    catch (GLib.Error ex) { GLib.debug(ex.message); }

                    // Unlock mutex
                    model->mutex.unlock();
                    return;
                }

                // Available multimedia
                bool video = (buddy.action_from_us & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0 ||
                             (buddy.action_to_us   & Model.Buddy.state_t.MASK_VIDEO_CALL) > 0;

                // Answer the A/V call
                ctrl->frm_main.av_answer_answered(buddy,
                                                  true, // Answer the A/V call
                                                  video,
                                                  true  // Mutex is locked
                                                 );

                // Close notification
                try                   { notification.close();   }
                catch (GLib.Error ex) { GLib.debug(ex.message); }

                // Unlock mutex
                model->mutex.unlock();
            };

            // Reject audio call
            this.reject_finish_clicked_cb = (notification, action) =>
            {
                // Lock mutex
                model->mutex.lock();

                // Ensure the buddy hadn't been removed to this time
                unowned Model.Buddy? buddy = notification.get_data<Model.Buddy>("buddy");

                if (!model->buddies_hash.has_key(buddy.uid))
                    buddy = null;

                // Ignore if outdated
                if (buddy == null || !((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) > 0 &&
                                       (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) > 0))
                {
                    GLib.debug(_("Notification outdated, ignoring the clicking"));

                    // Close notification
                    try                   { notification.close();   }
                    catch (GLib.Error ex) { GLib.debug(ex.message); }

                    // Unlock mutex
                    model->mutex.unlock();
                    return;
                }

                // Cancel the A/V call
                ctrl->frm_main.av_call_cancel(buddy,
                                              true, // Hang up
                                              true, // Update GUI
                                              true  // Mutex is locked
                                             );

                // Close notification
                try                   { notification.close();   }
                catch (GLib.Error ex) { GLib.debug(ex.message); }

                // Unlock mutex
                model->mutex.unlock();
            };

            // Open "Buddy Requests" tab
            this.open_buddy_req_clicked_cb = (notification, action) =>
            {
                // Lock mutex
                model->mutex.lock();

                // Open tab 2 in main notebook
                ctrl->ctrl_changed_by_user = false;

                view->frm_main.notebook_main.page_sel_set(View.FrmMain.NOTEBOOK_MAIN_PAGE_BUDDY_REQS);

                // Iterate through the all private keys.
                // Ensure the key hadn't been accepted/rejected to this time.
                unowned uint8[] key = notification.get_data<uint8[]>("key");
                bool found_key = false;
                uint idx;

                for (idx = 0; idx < model->buddy_reqs.size; idx++)
                {
                    Model.Model.BuddyReq tmp = model->buddy_reqs[(int) idx];

                    found_key = Posix.memcmp(tmp.key,
                                             key,
                                             tmp.key.length) == 0;
                    if (found_key)
                        break;
                }

                // Select key from notification in the check list control if
                // any
                if (found_key)
                {
                    // The key from notification has been found.
                    // Set selection.
                    view->frm_main.check_lst_pub_keys_tab2.sel_set(idx, true);
                    view->frm_main.btn_accept_tab2.focus_set();
                }
                else
                {
                    // The key from notification has not been found.
                    // Clear selection.
                    idx = view->frm_main.check_lst_pub_keys_tab2.sel_elem_get();
                    if (idx != View.Wrapper.Win.FOUND_NOT)
                        view->frm_main.check_lst_pub_keys_tab2.sel_set(idx, false);
                    view->frm_main.check_lst_pub_keys_tab2.focus_set();
                }

                ctrl->ctrl_changed_by_user = true;

                // Update the buddy request message
                ctrl->frm_main.txt_msg_tab2_upd();

                // Close notification
                try                   { notification.close();   }
                catch (GLib.Error ex) { GLib.debug(ex.message); }

                // Unlock mutex
                model->mutex.unlock();
            };

            // Open conference in buddy list
            this.open_conference_clicked_cb = (notification, action) =>
            {
                // Lock mutex
                model->mutex.lock();

                // Ensure the conference hadn't been removed to this time
                unowned Model.Conference? conference = notification.get_data<Model.Conference>("conference");

                if (!model->conferences_hash.has_key(conference.uid))
                    conference = null;

                // Should we select conference in list control?
                int  sel      = conference == null ? -1 : model->conferences_lst.index_of(conference);
                bool have_sel = sel != -1;

                // Open tab 1 in main notebook
                view->frm_main.notebook_main.page_sel_set(View.FrmMain.NOTEBOOK_MAIN_PAGE_CONVERSATIONS);

                // Open conference list in contacts
                uint page = view->frm_main.notebook_contacts.page_sel_get();

                if (page != View.FrmMain.NOTEBOOK_CONTACTS_PAGE_CONFERENCES)
                {
                    // Show/hide GUI widgets
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.notebook_contacts.page_sel_set(View.FrmMain.NOTEBOOK_CONTACTS_PAGE_CONFERENCES);
                    ctrl->ctrl_changed_by_user = true;
                    view->frm_main.txt_conv_buddy_tab1.show(false, // Hide
                                                            false  // Don't update layout
                                                           );
                    view->frm_main.splitter_vertical_tab1.show(true, // Show
                                                               true  // Update layout
                                                              );

                    // Set new width of member list columns.
                    //
                    // (The code below is hack.
                    // I should call this method when the program starts,
                    // but it then works incorrectly.)
                    if (ctrl->frm_main.page_conferences_opened_first_time)
                    {
                        ctrl->frm_main.page_conferences_opened_first_time = false;
                        view->frm_main.lst_ctrl_members_tab1_width_set();
                    }

                    // A/V call GUIs elements
                    ctrl->frm_main.av_widgets_upd();
                }

                // Set focus
                if (have_sel)
                    view->frm_main.txt_msg_tab1.focus_set();
                else
                {
                    ctrl->ctrl_changed_by_user = false;
                    view->frm_main.lst_ctrl_conferences.focus_set();
                    ctrl->ctrl_changed_by_user = true;
                }

                // Exit if this conference is already selected
                if (conference == model->peer_conference)
                    return;

                // Select the conference in list control
                ctrl->ctrl_changed_by_user = false;

                if (have_sel)
                {
                    // Conference is shown in conference list.
                    // Set selection.
                    view->frm_main.lst_ctrl_conferences.sel_set(sel, true);
                }
                else
                {
                    // Conference is not shown in conference list.
                    // Clear selection.
                    size_t cnt = view->frm_main.lst_ctrl_conferences.row_cnt_get();
                    for (uint row = 0; row < cnt; row++)
                        view->frm_main.lst_ctrl_conferences.sel_set(row, false);
                }

                ctrl->ctrl_changed_by_user = true;

                // Determine new peer
                model->peer_conference = have_sel ? conference :
                                                    null;

                // Check if there are unread messages from the conference
                if (conference != null &&
                    conference.have_unread_msg == Model.Conference.unread_t.TRUE)
                {
                    // Have unread messages from him was before.
                    // Set there are no unread messages.
                    conference.have_unread_msg = Model.Conference.unread_t.FALSE;

                    // Update conference list,
                    // set normal icon in conference list (not unread)
                    if (have_sel)
                    {
                        ctrl->frm_main.lst_ctrl_conferences_change(Ctrl.action_t.EDIT_ELEM,

                                                                   conference,

                                                                   false, // Don't set display name column
                                                                   false, // Don't set count column
                                                                   true   // Set unread state column
                                                                  );
                    }
                }

                // Refill members list
                ctrl->frm_main.lst_ctrl_members_tab1_change(Ctrl.action_t.UPD_LST);

                // Update other controls
                view->frm_main.menu_edit_conference.en(have_sel);
                view->frm_main.menu_remove_conference.en(have_sel);
                view->frm_main.menu_about_conference.en(have_sel);
                view->frm_main.menu_conv_conference.en(have_sel);
                view->frm_main.menu_edit_conference_.en(have_sel);
                view->frm_main.menu_remove_conference_.en(have_sel);
                view->frm_main.menu_about_conference_.en(have_sel);
                view->frm_main.menu_conv_conference_.en(have_sel);
                view->frm_main.txt_conv_conference_tab1.clr();
                ctrl->ctrl_changed_by_user = false;
                view->frm_main.txt_msg_tab1.clr();
                ctrl->ctrl_changed_by_user = true;
                view->frm_main.txt_msg_tab1.en(have_sel);
                view->frm_main.btn_send_tab1.en(false);

                // Select last messages from database and append them to text
                // control
                if (have_sel)
                {
                    try
                    {
                        // Select next message from database table "conv_conferences"
                        // written by/for peer with specific uid
                        string? name;
                        uint    fg;
                        uint    bg;
                        string? msg;
                        GLib.DateTime? datetime;

                        Sqlite.Statement? stmt = null;

                        while (model->self.db_conv_conferences_sel(ref stmt,

                                                                   out name,
                                                                   conference.uid,
                                                                   out fg,
                                                                   out bg,
                                                                   out msg,
                                                                   out datetime,

                                                                   null, // No date & time limit, just limit to number of outputted messages below
                                                                   null, // No date & time limit, just limit to number of outputted messages below

                                                                   View.FrmMain.TXT_CONV_TAB1_LIMIT))
                        {
                            // Append next message to text control with last exchanged
                            // messages from/to the conference
                            view->frm_main.txt_conv_conference_tab1_append(name,
                                                                           fg,
                                                                           bg,
                                                                           msg,
                                                                           datetime);
                        }
                    }
                    catch (Model.ErrProfileDB ex)
                    {
                        GLib.debug(ex.message);
                    }

                    // Warning!
                    // I disabled code below because it's buggy.
                    // It may freeze the app.

                    // Scroll to end of text control with last exchanged messages
                    // from/to the conference
                    //ulong len = view->frm_main.txt_conv_conference_tab1.len_get();
                    //view->frm_main.txt_conv_conference_tab1.pos_show(len);
                }

                // Close notification
                try                   { notification.close();   }
                catch (GLib.Error ex) { GLib.debug(ex.message); }

                // Unlock mutex
                model->mutex.unlock();
            };
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Deinitialize the NotifyDE Controller.
         *
         * Warning! We don't use destructor because of the constraint
         * of the GObject type system. Destructor is emulated.
         */
        public void deinit()
        {
            // Disconnect signals from callback methods.
            //
            // Can't move these calls to ~ToxCoreNotifyDE() and
            // ~ToxCoreAVNotifyDE(), because those destructors would never
            // been called then.
            this.tox_core.signals_disconnect();
            this.tox_core_av.signals_disconnect();

            // Tox callbacks.
            //
            // Should delete manually, because there will be no destructor
            // call ~CtrlNotifyDE(), which should call destructors of its
            // members subsequently.
            this.tox_core    = null;
            this.tox_core_av = null;
        }
    }

    /**
     * Tox class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    public class ToxCoreNotifyDE : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // This callback is triggered whenever there is a change in the
        // DHT connection state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — state Network state
        //  — param Not used currently
        private void self_state_changed(global::ToxCore.Tox tox,
                                        global::ToxCore.ConnectionStatus state,
                                        void *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            // Prepare to send notification
            Model.Profile self    = model->self;
            string        summary = _("Your network state was changed");
            string        body    = self.display_state_get(false);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = self.avatar == null ?
                Model.Person.no_avatar :
                self.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a message from a buddy is
        // received.
        //
        // Parameters:
        //  - tox   Tox instance
        //  - uid   Buddy unique identifier
        //  - type  Message type
        //  - msg   Message
        //  - param Not used currently
        private void buddy_rx(global::ToxCore.Tox          tox,
                              uint32                       uid,
                              global::ToxCore.MessageType  type,
                              uint8[]                      msg,
                              void                        *param)

            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            // Prepare to send notification
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      summary      = _("%s wrote").printf(display_name == "" ? _("Somebody") :
                                                                                 display_name);
            string      body         = Model.Model.arr2str_convert(msg);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their name.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — name  Name
        //  — param Not used currently
        private void buddy_name_changed(global::ToxCore.Tox  tox,
                                        uint32               uid,
                                        uint8[]              name,
                                        void                *param)
            requires (param == null)
        {
            Model.Buddy buddy = model->buddies_hash[uid];

            // Ignore the first incoming name from each buddy.
            // This name is not real.
            //
            // (Dirty hack.)
            bool flag = buddy.get_data<bool>("ignored_first_name");

            if (!flag)
            {
                buddy.set_data<bool>("ignored_first_name", true);
                return;
            }

            // Prepare to send notification
            string summary = _("New name %s").printf(buddy.name == "" ? C_("buddy", "is empty") :
                                                                        buddy.name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This event is triggered when a buddy goes offline after
        // having been online, or when a buddy goes online.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state Network state
        //  — param Not used currently
        private void buddy_state_changed(global::ToxCore.Tox               tox,
                                         uint32                            uid,
                                         global::ToxCore.ConnectionStatus  state,
                                         void                             *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            Model.Buddy buddy = model->buddies_hash[uid];

            // Ignore the first incoming state from each buddy.
            // This state is not real.
            //
            // (Dirty hack.)
            bool flag = buddy.get_data<bool>("ignored_first_state");

            if (!flag)
            {
                buddy.set_data<bool>("ignored_first_state", true);
                return;
            }

            // Prepare to send notification
            string display_name = buddy.display_name_get();
            string summary      = _("There is changed network state for %s").printf(display_name == "" ? _("somebody") :
                                                                                                         display_name);
            string body         = buddy.display_state_get(false);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their user
        // state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state User state
        //  — param Not used currently
        private void buddy_state_user_changed(global::ToxCore.Tox         tox,
                                              uint32                      uid,
                                              global::ToxCore.UserStatus  state,
                                              void                       *param)

            requires (state == global::ToxCore.UserStatus.NONE ||
                      state == global::ToxCore.UserStatus.AWAY ||
                      state == global::ToxCore.UserStatus.BUSY)
            requires (param == null)
        {
            Model.Buddy buddy = model->buddies_hash[uid];

            // Prepare to send notification
            string display_name = buddy.display_name_get();
            string summary      = _("%s changed state").printf(display_name == "" ? _("Somebody") :
                                                                                    display_name);
            string body         = buddy.display_state_user_get(false);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their status
        // message.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — status Status
        //  — param  Not used currently
        private void buddy_status_changed(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          uint8[]              status,
                                          void                *param)
            requires (param == null)
        {
            Model.Buddy buddy = model->buddies_hash[uid];

            // Ignore the first incoming status from each buddy.
            // This status is not real.
            //
            // (Dirty hack.)
            bool flag = buddy.get_data<bool>("ignored_first_status");

            if (!flag)
            {
                buddy.set_data<bool>("ignored_first_status", true);
                return;
            }

            // Prepare to send notification
                    string display_name = buddy.display_name_get();
                    string summary      = _("%s changed status").printf(display_name == "" ? _("Somebody") :
                                                                                             display_name);
            unowned string body         = buddy.status;

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy request is received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — key   Public key
        //  — msg   Message
        //  — param Not used currently
        private void buddy_req_rx(global::ToxCore.Tox  tox,
                                  uint8[]              key,
                                  uint8[]              msg,
                                  void                *param)
            requires (param == null)
        {
            // Prepare to send notification
            const string ICON = "yat";

            string summary = _("Buddy request");
            string body    = Model.Model.arr2str_convert(msg);
            size_t len     = Model.Person.key_size;

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       ICON);

            // Add notification action
            notification.add_action("action-open-buddy-req",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_req_clicked_cb);

            // Remember information about this notification
            var tmp = new uint8[len];
            Posix.memcpy(tmp,
                         key,
                         tmp.length);

            notification.set_data<uint8[]>("key", tmp);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy request is received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — flag  false: buddy is typing,
        //          true:  buddy is not typing
        //  — param Not used currently
        private void buddy_typing(global::ToxCore.Tox  tox,
                                  uint32               uid,
                                  bool                 flag,
                                  void                *param)
            requires (param == null)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — kind     The meaning of the file to be sent
        //  — len      Size in bytes of the file the client wants to
        //             send, UINT64_MAX if unknown or streaming
        //  — filename Name of the file. Does not need to be the
        //             actual name. This name will be sent along
        //             with the file send request.
        //  — param    Not used currently
        private void file_req_rx(global::ToxCore.Tox       tox,
                                 uint32                    uid,
                                 uint32                    file_num,
                                 global::ToxCore.FileKind  kind,
                                 uint64                    len,
                                 uint8[]                   filename,
                                 void                     *param)
            requires (kind == global::ToxCore.FileKind.DATA ||
                      kind == global::ToxCore.FileKind.AVATAR)
            requires (param == null)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — offset   The file position to receive
        //  — buf      A byte array containing the received chunk
        //  — param    Not used currently
        private void file_chunk_rx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   uint8[]              buf,
                                   void                *param)
            requires (param == null)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when core is ready to send more
        // file data.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             transmit is associated with
        //  — offset   The file position to transmit
        //  — len      The number of bytes requested for the current
        //             chunk
        //  — param    Not used currently
        private void file_chunk_tx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   size_t               len,
                                   void                *param)
            requires (param == null)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client receives a
        // conference message.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — type           Message type
        //  — msg            Message
        //  — param          Not used currently
        private void conference_rx(global::ToxCore.Tox          tox,
                                   uint32                       uid_conference,
                                   uint32                       uid_member,
                                   global::ToxCore.MessageType  type,
                                   uint8[]                      msg,
                                   void                        *param)
            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            // Prepare to send notification
            Model.Conference conference   = model->conferences_hash[uid_conference];
            Model.Member     member       = conference.members_hash[uid_member];

            if (member.person is Model.Profile) // Ignore our own messages
                return;

            Model.Buddy?     buddy        = member.person as Model.Buddy;
            string           display_name = member.display_name_get();
            string           summary      = _("%s wrote").printf(display_name == "" ? _("Somebody") :
                                                                                      display_name);
            string           body         = Model.Model.arr2str_convert(msg);

            // This assumes the case that conference has name but we haven't
            // receive it
            display_name  = conference.display_name_get();
            summary      += display_name == "" ? C_("rx", " in conference") :
                                                 C_("rx", " in %s").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            GLib.assert(buddy != null);

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy == null || buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            if (buddy == null)
                notification.add_action("action-open-conference", _("Open"), ctrl_notify_de->open_conference_clicked_cb);
            else
            {
                notification.add_action("action-open-buddy",      _("Open Buddy"),      ctrl_notify_de->open_buddy_clicked_cb);
                notification.add_action("action-open-conference", _("Open Conference"), ctrl_notify_de->open_conference_clicked_cb);
            }

            // Remember information about this notification
            if (buddy != null)
                notification.set_data<Model.Buddy>("buddy", buddy);
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody joins conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_joins(Model.Conference conference, Model.Member member)
        {
            // Ignore our own joining
            if (member.person is Model.Profile)
                return;

            // Prepare to send notification
            Model.Buddy? buddy        = member.person as Model.Buddy;
            string       display_name = member.display_name_get();
            string       summary      = _("%s joined").printf(display_name == "" ? _("Somebody") :
                                                                                   display_name);

            // This assumes the case that conference has name but we haven't
            // receive it
            display_name  = conference.display_name_get();
            summary      += display_name == "" ? C_("join", " conference") :
                                                 C_("join", " %s").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy == null || buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            if (buddy == null)
                notification.add_action("action-open-conference", _("Open"), ctrl_notify_de->open_conference_clicked_cb);
            else
            {
                notification.add_action("action-open-buddy",      _("Open Buddy"),      ctrl_notify_de->open_buddy_clicked_cb);
                notification.add_action("action-open-conference", _("Open Conference"), ctrl_notify_de->open_conference_clicked_cb);
            }

            // Remember information about this notification
            if (buddy != null)
                notification.set_data<Model.Buddy>("buddy", buddy);
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody leaves conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_leaves(Model.Conference conference, Model.Member member)
        {
            // Ignore our own joining
            if (member.person is Model.Profile)
                return;

            // Prepare to send notification
            Model.Buddy? buddy        = member.person as Model.Buddy;
            string       display_name = member.display_name_get();
            string       summary      = _("%s leaved").printf(display_name == "" ? _("Somebody") :
                                                                                   display_name);

            // This assumes the case that conference has name but we haven't
            // receive it
            display_name  = conference.display_name_get();
            summary      += display_name == "" ? C_("leave", " conference") :
                                                 C_("leave", " %s").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy == null || buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            if (buddy == null)
                notification.add_action("action-open-conference", _("Open"), ctrl_notify_de->open_conference_clicked_cb);
            else
            {
                notification.add_action("action-open-buddy",      _("Open Buddy"),      ctrl_notify_de->open_buddy_clicked_cb);
                notification.add_action("action-open-conference", _("Open Conference"), ctrl_notify_de->open_conference_clicked_cb);
            }

            // Remember information about this notification
            if (buddy != null)
                notification.set_data<Model.Buddy>("buddy", buddy);
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when when a peer changes their
        // name.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid.
        //                   They are different.
        //  — name           Name
        //  — param          Not used currently
        private void member_name_changed(global::ToxCore.Tox  tox,
                                         uint32               uid_conference,
                                         uint32               uid_member,
                                         uint8[]              name,
                                         void                *param)
            requires (param == null)
        {
            // Prepare to send notification
            Model.Conference conference = model->conferences_hash[uid_conference];
            Model.Member     member     = conference.members_hash[uid_member];

            if (member.person != null) // Ignore our buddies and ourselves
                return;

            string summary = _("New name %s").printf(member.person.name == "" ? C_("member", "is empty") :
                                                                                member.person.name);

            // This assumes the case that conference has name but we haven't
            // receive it
            string display_name  = conference.display_name_get();
                   summary      += display_name == "" ? C_("change", " in conference") :
                                                        C_("change", " in %s").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = Model.Person.no_avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-conference",
                                    _("Open"),
                                    ctrl_notify_de->open_conference_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer changes the conference
        // title.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — title          Title,
        //  — param          Not used currently.
        private void title_changed(global::ToxCore.Tox  tox,
                                   uint32               uid_conference,
                                   uint32               uid_member,
                                   uint8[]              title,
                                   void                *param)

            requires (param == null)
        {
            // Prepare to send notification
            const string ICON = "yat";

            Model.Conference    conference = model->conferences_hash[uid_conference];
            Model.Buddy?        buddy;
            string              summary;
            unowned Gdk.Pixbuf? icon;

            if (uid_member == uint32.MAX)
            {
                // Author is unknown (e.g. initial joining the conference)
                buddy   = null;
                summary = _("Conference title %s").printf(conference.title == "" ? C_("conference", "is empty") :
                                                                                   conference.title);
                icon    = null;
            }
            else
            {
                // Author is known
                Model.Member member       = conference.members_hash[uid_member];
                string       display_name = member.display_name_get();

                buddy   = (member.person is Model.Buddy) ? member.person as Model.Buddy : null;
                summary = _("%s renamed conference as %s").printf(display_name == "" ? _("Somebody") : display_name, conference.title == "" ? _("empty") : conference.title);
                icon    = member.person == null || member.person.avatar == null ? Model.Person.no_avatar : member.person.avatar;
            }

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       icon == null ? ICON : null
                                                      );

            // Set notification icon
            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            if (buddy == null)
                notification.add_action("action-open-conference", _("Open"), ctrl_notify_de->open_conference_clicked_cb);
            else
            {
                notification.add_action("action-open-buddy",      _("Open Buddy"),      ctrl_notify_de->open_buddy_clicked_cb);
                notification.add_action("action-open-conference", _("Open Conference"), ctrl_notify_de->open_conference_clicked_cb);
            }

            // Remember information about this notification
            if (buddy != null)
                notification.set_data<Model.Buddy>("buddy", buddy);
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client is invited to join
        // a conference.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — type   The conference type (text only or audio/video)
        //  — cookie A piece of data of variable length required to join
        //           the conference
        //  — param  Not used currently
        private void conference_invited(global::ToxCore.Tox              tox,
                                        uint32                           uid,
                                        uint32                           type,
                                        //global::ToxCore.CONFERENCE_TYPE  type, // valac generates wrong code, so I disabled it
                                        uint8[]                          cookie,
                                        void                            *param)
            requires (type == global::ToxCore.CONFERENCE_TYPE.TEXT ||
                      type == global::ToxCore.CONFERENCE_TYPE.AV)
            requires (param == null)
        {
            // Convert cookie from array to string
            string cookie_str = "";
            foreach (uint8 byte in cookie)
                cookie_str += byte.to_string("%02X");

            // Prepare to send notification
                    Model.Buddy buddy        = model->buddies_hash[uid];
                    string      display_name = buddy.display_name_get();
                    string      summary      = _("%s sent you invite").printf(display_name == "" ? _("Somebody") :
                                                                                                   display_name);
            unowned string      body         = cookie_str;

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       body,
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-buddy",
                                    _("Open"),
                                    ctrl_notify_de->open_buddy_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client successfully
        // connects to a conference after joining it.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_connected(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          void                *param)
            requires (param == null)
        {
            // Prepare to send notification
            Model.Conference conference   = model->conferences_hash[uid];
            Model.Profile    self         = model->self;
            string           display_name = conference.display_name_get();
            string           summary      = _("You connected");

            // This assumes the case that conference has name but we haven't
            // receive it
            if (display_name == "") summary += C_("connect", " conference");
            else                    summary += C_("connect", " %s").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = self.avatar == null ?
                Model.Person.no_avatar :
                self.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification action
            notification.add_action("action-open-conference",
                                    _("Open"),
                                    ctrl_notify_de->open_conference_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Conference>("conference", conference);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer joins or leaves the
        // conference.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_members_changed(global::ToxCore.Tox  tox,
                                                uint32               uid,
                                                void                *param)
            requires (param == null)
        {
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreNotifyDE()
        {
            signals_connect();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback destructor.
         *
         * Disconnects signals from callback methods.
         */
        ~ToxCoreNotifyDE()
        {
            //signals_disconnect(); // Should do it manually, otherwise destructor would not been ever called
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Connect signals to callback methods.
         *
         * This plugin uses signals which are located in main
         * application.
         */
        public void signals_connect()
        {
            ctrl->tox_core.self_state_changed_sig.connect(self_state_changed);
            ctrl->tox_core.buddy_rx_sig.connect(buddy_rx);
            ctrl->tox_core.buddy_name_changed_sig.connect(buddy_name_changed);
            ctrl->tox_core.buddy_state_changed_sig.connect(buddy_state_changed);
            ctrl->tox_core.buddy_state_user_changed_sig.connect(buddy_state_user_changed);
            ctrl->tox_core.buddy_status_changed_sig.connect(buddy_status_changed);
            ctrl->tox_core.buddy_req_rx_sig.connect(buddy_req_rx);
            ctrl->tox_core.buddy_typing_sig.connect(buddy_typing);
            ctrl->tox_core.file_req_rx_sig.connect(file_req_rx);
            ctrl->tox_core.file_chunk_rx_sig.connect(file_chunk_rx);
            ctrl->tox_core.file_chunk_tx_sig.connect(file_chunk_tx);
            ctrl->tox_core.conference_rx_sig.connect(conference_rx);
            ctrl->tox_core.member_joins_sig.connect(member_joins);
            ctrl->tox_core.member_leaves_sig.connect(member_leaves);
            ctrl->tox_core.member_name_changed_sig.connect(member_name_changed);
            ctrl->tox_core.title_changed_sig.connect(title_changed);
            ctrl->tox_core.conference_invited_sig.connect(conference_invited);
            ctrl->tox_core.conference_connected_sig.connect(conference_connected);
            ctrl->tox_core.conference_members_changed_sig.connect(conference_members_changed);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disconnects signals from callback methods
         */
        public void signals_disconnect()
        {
            ctrl->tox_core.self_state_changed_sig.disconnect(self_state_changed);
            ctrl->tox_core.buddy_rx_sig.disconnect(buddy_rx);
            ctrl->tox_core.buddy_name_changed_sig.disconnect(buddy_name_changed);
            ctrl->tox_core.buddy_state_changed_sig.disconnect(buddy_state_changed);
            ctrl->tox_core.buddy_state_user_changed_sig.disconnect(buddy_state_user_changed);
            ctrl->tox_core.buddy_status_changed_sig.disconnect(buddy_status_changed);
            ctrl->tox_core.buddy_req_rx_sig.disconnect(buddy_req_rx);
            ctrl->tox_core.buddy_typing_sig.disconnect(buddy_typing);
            ctrl->tox_core.file_req_rx_sig.disconnect(file_req_rx);
            ctrl->tox_core.file_chunk_rx_sig.disconnect(file_chunk_rx);
            ctrl->tox_core.file_chunk_tx_sig.disconnect(file_chunk_tx);
            ctrl->tox_core.conference_rx_sig.disconnect(conference_rx);
            ctrl->tox_core.member_joins_sig.disconnect(member_joins);
            ctrl->tox_core.member_leaves_sig.disconnect(member_leaves);
            ctrl->tox_core.member_name_changed_sig.disconnect(member_name_changed);
            ctrl->tox_core.title_changed_sig.disconnect(title_changed);
            ctrl->tox_core.conference_invited_sig.disconnect(conference_invited);
            ctrl->tox_core.conference_connected_sig.disconnect(conference_connected);
            ctrl->tox_core.conference_members_changed_sig.disconnect(conference_members_changed);
        }
    }

    /**
     * Tox AV class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    public class ToxCoreAVNotifyDE : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when a peer calls us
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param audio  true:  the buddy is sending audio,
         *               false: otherwise,
         * @param video  true:  the buddy is sending video,
         *               false: otherwise
         */
        private void called(ToxAV.ToxAV tox_av,
                            uint32      uid,
                            bool        audio,
                            bool        video)
        {
            // Show notification
            Model.Buddy buddy        = model->buddies_hash[uid];
            string      display_name = buddy.display_name_get();
            string      summary      = _("%s is calling").printf(display_name);

            // Create notification
            var notification = new Notify.Notification(summary,
                                                       "",  // Empty body
                                                       null // No icon. (Set below.)
                                                      );

            // Set notification icon
            unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                Model.Person.no_avatar :
                buddy.avatar;

            if (icon != null)
                notification.set_image_from_pixbuf(icon);

            // Add notification actions
            notification.add_action("action-open-buddy", _("Open"),   ctrl_notify_de->open_buddy_clicked_cb);
            notification.add_action("action-reject",     _("Reject"), ctrl_notify_de->reject_finish_clicked_cb);
            notification.add_action("action-accept",     _("Accept"), ctrl_notify_de->accept_clicked_cb);

            // Remember information about this notification
            notification.set_data<Model.Buddy>("buddy", buddy);

            // Remember this notification in memory.
            // Don't forget to free this memory when notification has been
            // closed.
            notification.id = model_notify_de->id++;
            model_notify_de->notifications[notification.id] = notification;

            notification.closed.connect(() =>
            {
                model_notify_de->notifications.unset(notification.id);
            });

            // Send notification
            try                   { notification.show();    }
            catch (GLib.Error ex) { GLib.debug(ex.message); }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when the network becomes too
         * saturated for current bit rates at which point core suggests
         * new bitrates.
         *
         * @param uid           Conference unique identifier
         * @param audio_bitrate Suggested maximum audio bitrate in
         *                      Kb/s
         * @param video_bitrate Suggested maximum video bitrate in
         *                      Kb/s
         */
        private void bitrate_suggest(uint32 uid,
                                     uint32 audio_bitrate,
                                     uint32 video_bitrate)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when an audio frame is coming
         *
         * @param tox_av       Tox A/V instance
         * @param uid          Conference unique identifier
         * @param pcm          An array of audio samples
         *                     (cnt_samples * cnt_channels elements)
         * @param cnt_samples  The number of audio samples per channel
         *                     in the PCM array
         * @param cnt_channels Number of audio channels
         * @param bitrate      Sampling rate used in this frame
         */
        private void audio_rx(ToxAV.ToxAV tox_av,
                              uint32      uid,
                              int16[]     pcm,
                              size_t      cnt_samples,
                              uint8       cnt_channels,
                              uint32      bitrate)
        {
            // Do nothing
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when an event is issued
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param state  Bitmask of ToxAV.FriendCallState enums
         */
        private void buddy_av_state_changed(ToxAV.ToxAV tox_av,
                                            uint32      uid,
                                            uint32      state)
        {
            // Has the call been rejectred or finished?
            if ((state & ToxAV.FriendCallState.FINISHED) > 0)
            {
                // Continue only if we really talk with this buddy
                Model.Buddy buddy = model->buddies_hash[uid];

                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_AV_CALL) == 0 &&
                    (buddy.action_to_us   & Model.Buddy.state_t.MASK_AV_CALL) == 0)
                {
                    // Prepare to send notification
                    string display_name = buddy.display_name_get();
                    string summary      = _("%s rejected/finished audio call").printf(display_name);

                    // Create notification
                    var notification = new Notify.Notification(summary,
                                                               "",  // Empty body
                                                               null // No icon. (Set below.)
                                                              );

                    // Set notification icon
                    unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                        Model.Person.no_avatar :
                        buddy.avatar;

                    if (icon != null)
                        notification.set_image_from_pixbuf(icon);

                    // Add notification action
                    notification.add_action("action-open-buddy", _("Open"), ctrl_notify_de->open_buddy_clicked_cb);

                    // Remember information about this notification
                    notification.set_data<Model.Buddy>("buddy", buddy);

                    // Remember this notification in memory.
                    // Don't forget to free this memory when notification has been
                    // closed.
                    notification.id = model_notify_de->id++;
                    model_notify_de->notifications[notification.id] = notification;

                    notification.closed.connect(() =>
                    {
                        model_notify_de->notifications.unset(notification.id);
                    });

                    // Send notification
                    try                   { notification.show();    }
                    catch (GLib.Error ex) { GLib.debug(ex.message); }
                }
            }
            // Has the call been answered?
            else if ((state & ToxAV.FriendCallState.SENDING_A)   > 0 ||
                     (state & ToxAV.FriendCallState.ACCEPTING_A) > 0 ||
                     (state & ToxAV.FriendCallState.SENDING_V)   > 0 ||
                     (state & ToxAV.FriendCallState.ACCEPTING_V) > 0)
            {
                // Continue only if we are calling to this buddy
                Model.Buddy buddy = model->buddies_hash[uid];

                if ((buddy.action_from_us & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0 ||
                    (buddy.action_to_us   & Model.Buddy.state_t.MASK_ACTIVE_AV_CALL) > 0)
                {
                    // Prepare to send notification
                    string display_name = buddy.display_name_get();
                    string summary      = _("%s answered audio call").printf(display_name);

                    // Create notification
                    var notification = new Notify.Notification(summary,
                                                               "",  // Empty body
                                                               null // No icon. (Set below.)
                                                              );

                    // Set notification icon
                    unowned Gdk.Pixbuf? icon = buddy.avatar == null ?
                        Model.Person.no_avatar :
                        buddy.avatar;

                    if (icon != null)
                        notification.set_image_from_pixbuf(icon);

                    // Add notification actions
                    notification.add_action("action-open-buddy", _("Open"),   ctrl_notify_de->open_buddy_clicked_cb);
                    notification.add_action("action-finish",     _("Finish"), ctrl_notify_de->reject_finish_clicked_cb);

                    // Remember information about this notification
                    notification.set_data<Model.Buddy>("buddy", buddy);

                    // Remember this notification in memory.
                    // Don't forget to free this memory when notification has been
                    // closed.
                    notification.id = model_notify_de->id++;
                    model_notify_de->notifications[notification.id] = notification;

                    notification.closed.connect(() =>
                    {
                        model_notify_de->notifications.unset(notification.id);
                    });

                    // Send notification
                    try                   { notification.show();    }
                    catch (GLib.Error ex) { GLib.debug(ex.message); }
                }
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox AV callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreAVNotifyDE()
        {
            signals_connect();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Tox AV callback destructor.
         *
         * Disconnects signals from callback methods.
         */
        ~ToxCoreAVNotifyDE()
        {
            //signals_disconnect(); // Should do it manually, otherwise destructor would not been ever called
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Connect signals to callback methods.
         *
         * This plugin uses signals which are located in main
         * application.
         */
        public void signals_connect()
        {
            ctrl->tox_core_av.called_sig.connect(called);
            ctrl->tox_core_av.bitrate_suggest_sig.connect(bitrate_suggest);
            ctrl->tox_core_av.audio_rx_sig.connect(audio_rx);
            ctrl->tox_core_av.buddy_av_state_changed_sig.connect(buddy_av_state_changed);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disconnects signals from callback methods
         */
        public void signals_disconnect()
        {
            ctrl->tox_core_av.called_sig.disconnect(called);
            ctrl->tox_core_av.bitrate_suggest_sig.disconnect(bitrate_suggest);
            ctrl->tox_core_av.audio_rx_sig.disconnect(audio_rx);
            ctrl->tox_core_av.buddy_av_state_changed_sig.disconnect(buddy_av_state_changed);
        }
    }
}

