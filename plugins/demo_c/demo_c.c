/*
 *    demo_c.c
 *    Copyright (C) 2022-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Headers
#include <glib-object.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gmodule.h>
#include <libpeas/peas.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <yat.h>

#include "demo_c.h"

/*----------------------------------------------------------------------------*/

// Singletons
extern yatModelModel *model;
extern yatViewView   *view;
extern yatCtrlCtrl   *ctrl;

/*----------------------------------------------------------------------------*/

// Properties.
//
// Note. We do not use properties in this example,
// so implemented them just for demonstration purpose.
enum
{
    PROP_0,
    PROP_VIEW,
    PROP_MODEL,
    PROP_CTRL,
};

/*----------------------------------------------------------------------------*/

// Register the Demo type.
// It is derived from yatModelModel interface.
static void yat_model_plugin_init(yatModelPluginIface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED(Demo,
                               demo,
                               PEAS_TYPE_EXTENSION_BASE,
                               0, //G_TYPE_FLAG_NONE,
                               G_IMPLEMENT_INTERFACE_DYNAMIC(YAT_MODEL_TYPE_PLUGIN,
                                                             yat_model_plugin_init))

/*----------------------------------------------------------------------------*/

// Implemented methods of yatModelPlugin interface
static void     plugin_load     (yatModelPlugin *self);
static void     plugin_unload   (yatModelPlugin *self);
static gboolean plugin_cfg_check(yatModelPlugin *self);

static void yat_model_plugin_init(yatModelPluginIface *iface)
{
    //g_debug("%s", G_STRFUNC);

    iface->plugin_load      = plugin_load;
    iface->plugin_unload    = plugin_unload;
    iface->plugin_cfg_check = plugin_cfg_check;
    iface->plugin_cfg_exec  = NULL; // Change if needed
}

/*----------------------------------------------------------------------------*/

// GObject helper methods.
// They initialize Demo object.
static void demo_init(Demo *plugin)
{
    //g_debug("%s", G_STRFUNC);
}

static void demo_get_property(GObject    *obj,
                              guint       prop_id,
                              GValue     *val,
                              GParamSpec *param)
{
    Demo *plugin = DEMO(obj);

    //g_debug("%s", G_STRFUNC);

    switch (prop_id)
    {
    case PROP_MODEL:
        g_value_set_object(val, plugin->model_);
        break;
    case PROP_VIEW:
        g_value_set_object(val, plugin->view_);
        break;
    case PROP_CTRL:
        g_value_set_object(val, plugin->ctrl_);
        break;
    case PROP_0:
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, param);
        break;
    }
}

static void demo_set_property(GObject      *obj,
                              guint         prop_id,
                              const GValue *val,
                              GParamSpec   *param)
{
    Demo *plugin = DEMO(obj);

    //g_debug("%s", G_STRFUNC);

    switch (prop_id)
    {
    case PROP_MODEL:
        plugin->model_ = g_value_get_object(val);
        break;
    case PROP_VIEW:
        plugin->view_  = g_value_get_object(val);
        break;
    case PROP_CTRL:
        plugin->ctrl_  = g_value_get_object(val);
        break;
    case PROP_0:
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, param);
        break;
    }
}

static void demo_finalize(GObject *obj)
{
    GObjectClass *obj_class = G_OBJECT_CLASS(demo_parent_class);

    //g_debug("%s", G_STRFUNC);
    obj_class->finalize(obj);
}

static void demo_class_init(DemoClass *klass)
{
    GObjectClass *obj_class = G_OBJECT_CLASS(klass);

    //g_debug("%s", G_STRFUNC);
    obj_class->set_property = demo_set_property;
    obj_class->get_property = demo_get_property;
    obj_class->finalize     = demo_finalize;

    g_object_class_override_property(obj_class, PROP_MODEL, "model-");
    g_object_class_override_property(obj_class, PROP_VIEW,  "view-");
    g_object_class_override_property(obj_class, PROP_CTRL,  "ctrl-");
}

static void demo_class_finalize(DemoClass *klass)
{
    //g_debug("%s", G_STRFUNC);
}

/*----------------------------------------------------------------------------*/

// Plugin entry point.
//
// Gets the pointer to the plugin object as parameter.
static void plugin_load(yatModelPlugin *self)
{
    Demo           *plugin = DEMO(self);
    PeasPluginInfo *info   = peas_extension_base_get_plugin_info(&plugin->parent_instance);

	gchar *filename = peas_plugin_info_get_module_name(info);
	gchar *domain   = g_strconcat("yat-plugins-", filename, NULL);
	gchar *name     = dgettext(domain, peas_plugin_info_get_name(info));
	gchar *ver      = peas_plugin_info_get_version(info);
	gchar *dbg      = g_strdup_printf(_("Start %s %s"), name, ver);

    g_debug(dbg);

    g_free(domain);
    g_free(dbg);
}

/*----------------------------------------------------------------------------*/

// Plugin exit point
static void plugin_unload(yatModelPlugin *self)
{
    g_debug(_("Stop"));
}

/*----------------------------------------------------------------------------*/

// Plugin configuration availability
static gboolean plugin_cfg_check(yatModelPlugin *self)
{
    g_debug(_("Configuration is not available"));
    return FALSE;
}

/*----------------------------------------------------------------------------*/

// Register types used by the plugin.
//
// Gets the pointer to the plugin object module as parameter.
G_MODULE_EXPORT void peas_register_types(PeasObjectModule *module)
{
    //g_debug("%s", G_STRFUNC);
    demo_register_type(G_TYPE_MODULE(module));
    peas_object_module_register_extension_type(module,
                                               YAT_MODEL_TYPE_PLUGIN,
                                               TYPE_DEMO);
}

