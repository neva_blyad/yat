/*
 *    demo_c.h
 *    Copyright (C) 2022-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                            <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __DEMO_H__
#define __DEMO_H__

// Headers
#include <libpeas/peas.h>

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define TYPE_DEMO               (demo_get_type())
#define DEMO(o)                 (G_TYPE_CHECK_INSTANCE_CAST((o), TYPE_DEMO, Demo))
#define DEMO_CLASS(k)           (G_TYPE_CHECK_CLASS_CAST((k), TYPE_DEMO, Demo))
#define IS_DEMO_PLUGIN(o)       (G_TYPE_CHECK_INSTANCE_TYPE((o), TYPE_DEMO))
#define IS_DEMO_PLUGIN_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE((k), TYPE_DEMO))
#define DEMO_GET_CLASS(o)       (G_TYPE_INSTANCE_GET_CLASS((o), TYPE_DEMO, DemoClass))

/*----------------------------------------------------------------------------*/

typedef struct _Demo      Demo;
typedef struct _DemoClass DemoClass;

struct _Demo
{
    PeasExtensionBase parent_instance;

    yatModelModel *model_; // Not used, direct access to model reference is available
    yatViewView   *view_;  // Not used, direct access to view reference is available
    yatCtrlCtrl   *ctrl_;  // Not used, direct access to ctrl reference is available
};

struct _DemoClass
{
    PeasExtensionBaseClass parent_class;
};

/*----------------------------------------------------------------------------*/

GType                demo_get_type      (void) G_GNUC_CONST;
G_MODULE_EXPORT void peas_register_types(PeasObjectModule *module);

G_END_DECLS

#endif /* __DEMO_H__ */

