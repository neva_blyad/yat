/*
 *    scriptbox.vala
 *    Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                       <neva_blyad@lovecri.es>
 *
 *    This file is part of yat.
 *
 *    yat is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    yat is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with yat.  If not, see <https://www.gnu.org/licenses/>.
 */

// Allow non-qualified names
using yat;

// Singletons
extern Model.Model *model;
//extern View.View   *view; // Not used
extern Ctrl.Ctrl   *ctrl;

yat.Model.ModelScriptBox *model_scriptbox;
yat.View.ViewScriptBox   *view_scriptbox;
yat.Ctrl.CtrlScriptBox   *ctrl_scriptbox;

/*----------------------------------------------------------------------------*/

// Register types used by the plugin.
//
// Gets the reference to the plugin object as parameter.
//
// Note: ModuleInit attribute marks that this function should
// register the plugin's types with GObject type system.
[ModuleInit]
public void peas_register_types(GLib.TypeModule module)
{
    // Types to be registered
    var obj = module as Peas.ObjectModule;
    obj.register_extension_type(typeof(Model.Plugin), typeof(Ctrl.CtrlScriptBox));
}

/*----------------------------------------------------------------------------*/

/**
 * This is the Model namespace.
 *
 * This is where the data and logic is.
 * Nothing GUI oriented here.
 */
namespace yat.Model
{
    /**
     * This is the Model class for Script Box plugin
     */
    public class ModelScriptBox : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Private Data                                */
        /*----------------------------------------------------------------------------*/

        // Information about this plugin.
        //
        // Warning! We don't use properties here because they are buggy
        // when used dynamically.
        private Peas.PluginInfo info;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Script Box Model constructor
         */
        public ModelScriptBox()
        {
            // Save reference to plugin information.
            // We will need this in the future.
            this.info = model->plugins_ext.engine.get_plugin_info("scriptbox");
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Script Box Model destructor
         */
        ~ModelScriptBox()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Get reference to this plugin
         *
         * @return The plugin information
         */
        public Peas.PluginInfo info_get()
        {
            return this.info;
        }
    }
}

/**
 * This is the View namespace.
 *
 * All GUI should go here.
 * Now it is implemented by the wxWidgets/wxVala widget toolkit.
 * Qt interface is planned.
 */
namespace yat.View
{
    /**
     * This is the View class for Script Box plugin
     */
    public class ViewScriptBox : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        // XML Based Resource System (XRC)
        public Wx.XMLResource xml;

        // "Preferences" dialog
        public DlgPreferencesScriptBox dlg_preferences;

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize XRC.
        //
        // Loads the XRC file.
        private void xrc_init()
        {
            // XRC filename.
            // This file contains GUI description in XML format.
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string name  = info.get_module_name();
            string filepath      = GLib.Path.build_filename(model->path_plugins,
                                                            name,
                                                            name + ".xrc");

            // Load the XRC file
            this.xml = new Wx.XMLResource(1);
            var res = this.xml.load(new Wx.Str(filepath));
            GLib.assert(res);
        }

        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets.
        private void widgets_init()
        {
            // Allocate widgets
            this.dlg_preferences = new DlgPreferencesScriptBox(this.xml);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Script Box View constructor.
         *
         * Initializes XRC.
         * Initializes widgets.
         */
        public ViewScriptBox()
        {
            xrc_init();
            widgets_init();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Script Box View destructor
         */
        ~ViewScriptBox()
        {
        }
    }

    /**
     * This is the "Preferences" dialog class.
     * It belongs to the View part.
     *
     * View is GUI. Currently it is implemented by wxWidgets/wxVala.
     */
    public class DlgPreferencesScriptBox : Wrapper.Dlg
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Notebook
         */
        public Wrapper.Notebook notebook { public get; public set; }

        /*
         * "Tox related callbacks" page in notebook
         */
        public const uint NOTEBOOK_PAGE_TOX    = 0;

        /*
         * "Tox A/V callbacks" page in notebook
         */
        public const uint NOTEBOOK_PAGE_TOX_AV = 1;

        /**
         * Panel "Tox related callbacks".
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tox { public get; public set; }

        /**
         * Label for "Self stated is changed".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_self_changed_state { public get; public set; }

        /**
         * Input text control for "Self stated is changed".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_self_changed_state { public get; public set; }

        /**
         * Label for event "Message is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_rx_buddy { public get; public set; }

        /**
         * Input text control for event "Message is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_rx_buddy { public get; public set; }

        /**
         * Label for event "Buddy starts/stops to type us".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_typing_buddy { public get; public set; }

        /**
         * Input text control for event "Buddy starts/stops to type us".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_typing_buddy { public get; public set; }

        /**
         * Label for event "Buddy changes name".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_buddy_changed_name { public get; public set; }

        /**
         * Input text control for event "Buddy changes name".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_buddy_changed_name { public get; public set; }

        /**
         * Label for event "Buddy goes online or offline".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_buddy_changed_state { public get; public set; }

        /**
         * Input text control for event "Buddy goes online or offline".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_buddy_changed_state { public get; public set; }

        /**
         * Label for event "Buddy changes his user state".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_buddy_changed_user_state { public get; public set; }

        /**
         * Input text control for event "Buddy changes his user state".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_buddy_changed_user_state { public get; public set; }

        /**
         * Label for event "Buddy changes status".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_buddy_changed_status { public get; public set; }

        /**
         * Input text control for event "Buddy changes status".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_buddy_changed_status { public get; public set; }

        /**
         * Label for event "Buddy request is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_buddy_rx_req { public get; public set; }

        /**
         * Input text control for event "Buddy request is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_buddy_rx_req { public get; public set; }

        /**
         * Label for event "File request is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_file_rx_req { public get; public set; }

        /**
         * Input text control for event "File request is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_file_rx_req { public get; public set; }

        /**
         * Label for event "Incoming file chunk".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_file_rx_chunk { public get; public set; }

        /**
         * Input text control for event "Incoming file chunk".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_file_rx_chunk { public get; public set; }

        /**
         * Label for event "Ready to send more file data".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_file_tx_chunk { public get; public set; }

        /**
         * Input text control for event "Ready to send more file data".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_file_tx_chunk { public get; public set; }

        /**
         * Label for event "Conference message is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_conference_rx_message { public get; public set; }

        /**
         * Input text control for event "Conference message is received".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_conference_rx_message { public get; public set; }

        /**
         * Label for event "Somebody joins conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_joins_member { public get; public set; }

        /**
         * Input text control for event "Somebody joins conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_joins_member { public get; public set; }

        /**
         * Label for event "Member leaves".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_leaves_member { public get; public set; }

        /**
         * Input text control for event "Member leaves".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_leaves_member { public get; public set; }

        /**
         * Label for event "Peer joins or leaves the conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_member_changed_name { public get; public set; }

        /**
         * Input text control for event "Peer joins or leaves the
         * conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_member_changed_name { public get; public set; }

        /**
         * Label for event "Client is invited to join a conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_changed_title { public get; public set; }

        /**
         * Input text control for event "Client is invited to join a
         * conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_changed_title { public get; public set; }

        /**
         * Label for event "Client is invited to join a conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_invited_conference { public get; public set; }

        /**
         * Input text control for event "Client is invited to join a
         * conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_invited_conference { public get; public set; }

        /**
         * Label for event "Client successfully connects to conference
         * after joining it".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_connected_conference { public get; public set; }

        /**
         * Input text control for event "Client successfully connects to
         * conference after joining it".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_connected_conference { public get; public set; }

        /**
         * Label for event "Peer joins or leaves the conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.CheckBox check_changed_conference_members { public get; public set; }

        /**
         * Input text control for event "Peer joins or leaves the
         * conference".
         *
         * It is placed on the tab panel "Tox related callbacks".
         */
        public Wrapper.TxtCtrl txt_changed_conference_members { public get; public set; }

        /**
         * Panel "Tox A/V callbacks".
         *
         * It is placed on the notebook.
         */
        public Wrapper.Panel panel_tox_av { public get; public set; }

        /**
         * Label for event "Peer calls us".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.CheckBox check_called_by_peer { public get; public set; }

        /**
         * Input text control for event "Peer calls us".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.TxtCtrl txt_called_by_peer { public get; public set; }

        /**
         * Label for event "Suggest new bitrate".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.CheckBox check_suggest_bitrate { public get; public set; }

        /**
         * Input text control for event "Suggest new bitrate".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.TxtCtrl txt_suggest_bitrate { public get; public set; }

        /**
         * Label for event "Audio frame is coming".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.CheckBox check_rx_audio { public get; public set; }

        /**
         * Input text control for event "Audio frame is coming".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.TxtCtrl txt_rx_audio { public get; public set; }

        /**
         * Label for event "Event is issued".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.CheckBox check_changed_state { public get; public set; }

        /**
         * Input text control for event "Event is issued".
         *
         * It is placed on the tab panel "Tox A/V callbacks".
         */
        public Wrapper.TxtCtrl txt_changed_state { public get; public set; }

        /**
         * "Cancel" button
         */
        public Wrapper.Btn btn_cancel { public get; public set; }

        /**
         * Note
         */
        public Wrapper.StaticTxt lbl_note { public get; public set; }

        /**
         * "Apply" button
         */
        public Wrapper.Btn btn_apply_settings { public get; public set; }

        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize widgets.
        //
        // Allocates widgets. Finds/loads the widgets in XRC file.
        // Setups the widgets.
        private void widgets_init(Wx.XMLResource xml)
        {
            // Allocate widgets
            this.notebook                         = new Wrapper.Notebook();
            this.panel_tox                        = new Wrapper.Panel();
            this.check_self_changed_state         = new Wrapper.CheckBox();
            this.txt_self_changed_state           = new Wrapper.TxtCtrl();
            this.check_rx_buddy                   = new Wrapper.CheckBox();
            this.txt_rx_buddy                     = new Wrapper.TxtCtrl();
            this.check_buddy_changed_name         = new Wrapper.CheckBox();
            this.txt_buddy_changed_name           = new Wrapper.TxtCtrl();
            this.check_buddy_changed_state        = new Wrapper.CheckBox();
            this.txt_buddy_changed_state          = new Wrapper.TxtCtrl();
            this.check_buddy_changed_user_state   = new Wrapper.CheckBox();
            this.txt_buddy_changed_user_state     = new Wrapper.TxtCtrl();
            this.check_buddy_changed_status       = new Wrapper.CheckBox();
            this.txt_buddy_changed_status         = new Wrapper.TxtCtrl();
            this.check_buddy_rx_req               = new Wrapper.CheckBox();
            this.txt_buddy_rx_req                 = new Wrapper.TxtCtrl();
            this.check_typing_buddy               = new Wrapper.CheckBox();
            this.txt_typing_buddy                 = new Wrapper.TxtCtrl();
            this.check_file_rx_req                = new Wrapper.CheckBox();
            this.txt_file_rx_req                  = new Wrapper.TxtCtrl();
            this.check_file_rx_chunk              = new Wrapper.CheckBox();
            this.txt_file_rx_chunk                = new Wrapper.TxtCtrl();
            this.check_file_tx_chunk              = new Wrapper.CheckBox();
            this.txt_file_tx_chunk                = new Wrapper.TxtCtrl();
            this.check_conference_rx_message      = new Wrapper.CheckBox();
            this.txt_conference_rx_message        = new Wrapper.TxtCtrl();
            this.check_joins_member               = new Wrapper.CheckBox();
            this.txt_joins_member                 = new Wrapper.TxtCtrl();
            this.check_leaves_member              = new Wrapper.CheckBox();
            this.txt_leaves_member                = new Wrapper.TxtCtrl();
            this.check_member_changed_name        = new Wrapper.CheckBox();
            this.txt_member_changed_name          = new Wrapper.TxtCtrl();
            this.check_changed_title              = new Wrapper.CheckBox();
            this.txt_changed_title                = new Wrapper.TxtCtrl();
            this.check_invited_conference         = new Wrapper.CheckBox();
            this.txt_invited_conference           = new Wrapper.TxtCtrl();
            this.check_connected_conference       = new Wrapper.CheckBox();
            this.txt_connected_conference         = new Wrapper.TxtCtrl();
            this.check_changed_conference_members = new Wrapper.CheckBox();
            this.txt_changed_conference_members   = new Wrapper.TxtCtrl();
            this.panel_tox_av                     = new Wrapper.Panel();
            this.check_called_by_peer             = new Wrapper.CheckBox();
            this.txt_called_by_peer               = new Wrapper.TxtCtrl();
            this.check_suggest_bitrate            = new Wrapper.CheckBox();
            this.txt_suggest_bitrate              = new Wrapper.TxtCtrl();
            this.check_rx_audio                   = new Wrapper.CheckBox();
            this.txt_rx_audio                     = new Wrapper.TxtCtrl();
            this.check_changed_state              = new Wrapper.CheckBox();
            this.txt_changed_state                = new Wrapper.TxtCtrl();
            this.btn_cancel                       = new Wrapper.Btn();
            this.lbl_note                         = new Wrapper.StaticTxt();
            this.btn_apply_settings               = new Wrapper.Btn();

            // Find/load the widgets in XRC file
            this.win                                  = (Wx.Win) xml.dlg_load(null, new Wx.Str("dlg_preferences_scriptbox"));
            GLib.assert(this.win                                  != null);
            this.notebook.win                         = this.win.win_find(new Wx.Str("notebook"));
            GLib.assert(this.notebook.win                         != null);
            this.panel_tox.win                        = this.notebook.win.win_find(new Wx.Str("panel_tox"));
            GLib.assert(this.panel_tox.win                        != null);
            this.check_self_changed_state.win         = this.panel_tox.win.win_find(new Wx.Str("check_self_changed_state"));
            GLib.assert(this.check_self_changed_state.win         != null);
            this.txt_self_changed_state.win           = this.panel_tox.win.win_find(new Wx.Str("txt_self_changed_state"));
            GLib.assert(this.txt_self_changed_state.win           != null);
            this.check_rx_buddy.win                   = this.panel_tox.win.win_find(new Wx.Str("check_rx_buddy"));
            GLib.assert(this.check_rx_buddy.win                   != null);
            this.txt_rx_buddy.win                     = this.panel_tox.win.win_find(new Wx.Str("txt_rx_buddy"));
            GLib.assert(this.txt_rx_buddy.win                     != null);
            this.check_buddy_changed_name.win         = this.panel_tox.win.win_find(new Wx.Str("check_buddy_changed_name"));
            GLib.assert(this.check_buddy_changed_name.win         != null);
            this.txt_buddy_changed_name.win           = this.panel_tox.win.win_find(new Wx.Str("txt_buddy_changed_name"));
            GLib.assert(this.txt_buddy_changed_name.win           != null);
            this.check_buddy_changed_state.win        = this.panel_tox.win.win_find(new Wx.Str("check_buddy_changed_state"));
            GLib.assert(this.check_buddy_changed_state.win        != null);
            this.txt_buddy_changed_state.win          = this.panel_tox.win.win_find(new Wx.Str("txt_buddy_changed_state"));
            GLib.assert(this.txt_buddy_changed_state.win          != null);
            this.check_buddy_changed_user_state.win   = this.panel_tox.win.win_find(new Wx.Str("check_buddy_changed_user_state"));
            GLib.assert(this.check_buddy_changed_user_state.win   != null);
            this.txt_buddy_changed_user_state.win     = this.panel_tox.win.win_find(new Wx.Str("txt_buddy_changed_user_state"));
            GLib.assert(this.txt_buddy_changed_user_state.win     != null);
            this.check_buddy_changed_status.win       = this.panel_tox.win.win_find(new Wx.Str("check_buddy_changed_status"));
            GLib.assert(this.check_buddy_changed_status.win       != null);
            this.txt_buddy_changed_status.win         = this.panel_tox.win.win_find(new Wx.Str("txt_buddy_changed_status"));
            GLib.assert(this.txt_buddy_changed_status.win         != null);
            this.check_buddy_rx_req.win               = this.panel_tox.win.win_find(new Wx.Str("check_buddy_rx_req"));
            GLib.assert(this.check_buddy_rx_req.win               != null);
            this.txt_buddy_rx_req.win                 = this.panel_tox.win.win_find(new Wx.Str("txt_buddy_rx_req"));
            GLib.assert(this.txt_buddy_rx_req.win                 != null);
            this.check_typing_buddy.win               = this.panel_tox.win.win_find(new Wx.Str("check_typing_buddy"));
            GLib.assert(this.check_typing_buddy.win               != null);
            this.txt_typing_buddy.win                 = this.panel_tox.win.win_find(new Wx.Str("txt_typing_buddy"));
            GLib.assert(this.txt_typing_buddy.win                 != null);
            this.check_file_rx_req.win                = this.panel_tox.win.win_find(new Wx.Str("check_file_rx_req"));
            GLib.assert(this.check_file_rx_req.win                != null);
            this.txt_file_rx_req.win                  = this.panel_tox.win.win_find(new Wx.Str("txt_file_rx_req"));
            GLib.assert(this.txt_file_rx_req.win                  != null);
            this.check_file_rx_chunk.win              = this.panel_tox.win.win_find(new Wx.Str("check_file_rx_chunk"));
            GLib.assert(this.check_file_rx_chunk.win              != null);
            this.txt_file_rx_chunk.win                = this.panel_tox.win.win_find(new Wx.Str("txt_file_rx_chunk"));
            GLib.assert(this.txt_file_rx_chunk.win                != null);
            this.check_file_tx_chunk.win              = this.panel_tox.win.win_find(new Wx.Str("check_file_tx_chunk"));
            GLib.assert(this.check_file_tx_chunk.win              != null);
            this.txt_file_tx_chunk.win                = this.panel_tox.win.win_find(new Wx.Str("txt_file_tx_chunk"));
            GLib.assert(this.txt_file_tx_chunk.win                != null);
            this.check_conference_rx_message.win      = this.panel_tox.win.win_find(new Wx.Str("check_conference_rx_message"));
            GLib.assert(this.check_conference_rx_message.win      != null);
            this.txt_conference_rx_message.win        = this.panel_tox.win.win_find(new Wx.Str("txt_conference_rx_message"));
            GLib.assert(this.txt_conference_rx_message.win        != null);
            this.check_joins_member.win               = this.panel_tox.win.win_find(new Wx.Str("check_joins_member"));
            GLib.assert(this.check_joins_member.win               != null);
            this.txt_joins_member.win                 = this.panel_tox.win.win_find(new Wx.Str("txt_joins_member"));
            GLib.assert(this.txt_joins_member.win                 != null);
            this.check_leaves_member.win              = this.panel_tox.win.win_find(new Wx.Str("check_leaves_member"));
            GLib.assert(this.check_leaves_member.win              != null);
            this.txt_leaves_member.win                = this.panel_tox.win.win_find(new Wx.Str("txt_leaves_member"));
            GLib.assert(this.txt_leaves_member.win                != null);
            this.check_member_changed_name.win        = this.panel_tox.win.win_find(new Wx.Str("check_member_changed_name"));
            GLib.assert(this.check_member_changed_name.win        != null);
            this.txt_member_changed_name.win          = this.panel_tox.win.win_find(new Wx.Str("txt_member_changed_name"));
            GLib.assert(this.txt_member_changed_name.win          != null);
            this.check_changed_title.win              = this.panel_tox.win.win_find(new Wx.Str("check_changed_title"));
            GLib.assert(this.check_changed_title.win              != null);
            this.txt_changed_title.win                = this.panel_tox.win.win_find(new Wx.Str("txt_changed_title"));
            GLib.assert(this.txt_changed_title.win                != null);
            this.check_invited_conference.win         = this.panel_tox.win.win_find(new Wx.Str("check_invited_conference"));
            GLib.assert(this.check_invited_conference.win         != null);
            this.txt_invited_conference.win           = this.panel_tox.win.win_find(new Wx.Str("txt_invited_conference"));
            GLib.assert(this.txt_invited_conference.win           != null);
            this.check_connected_conference.win       = this.panel_tox.win.win_find(new Wx.Str("check_connected_conference"));
            GLib.assert(this.check_connected_conference.win       != null);
            this.txt_connected_conference.win         = this.panel_tox.win.win_find(new Wx.Str("txt_connected_conference"));
            GLib.assert(this.txt_connected_conference.win         != null);
            this.check_changed_conference_members.win = this.panel_tox.win.win_find(new Wx.Str("check_changed_conference_members"));
            GLib.assert(this.check_changed_conference_members.win != null);
            this.txt_changed_conference_members.win   = this.panel_tox.win.win_find(new Wx.Str("txt_changed_conference_members"));
            GLib.assert(this.txt_changed_conference_members.win   != null);
            this.panel_tox_av.win                     = this.notebook.win.win_find(new Wx.Str("panel_tox_av"));
            GLib.assert(this.panel_tox_av.win                     != null);
            this.check_called_by_peer.win             = this.panel_tox_av.win.win_find(new Wx.Str("check_called_by_peer"));
            GLib.assert(this.check_called_by_peer.win             != null);
            this.txt_called_by_peer.win               = this.panel_tox_av.win.win_find(new Wx.Str("txt_called_by_peer"));
            GLib.assert(this.txt_called_by_peer.win               != null);
            this.check_suggest_bitrate.win            = this.panel_tox_av.win.win_find(new Wx.Str("check_suggest_bitrate"));
            GLib.assert(this.check_suggest_bitrate.win            != null);
            this.txt_suggest_bitrate.win              = this.panel_tox_av.win.win_find(new Wx.Str("txt_suggest_bitrate"));
            GLib.assert(this.txt_suggest_bitrate.win              != null);
            this.check_rx_audio.win                   = this.panel_tox_av.win.win_find(new Wx.Str("check_rx_audio"));
            GLib.assert(this.check_rx_audio.win                   != null);
            this.txt_rx_audio.win                     = this.panel_tox_av.win.win_find(new Wx.Str("txt_rx_audio"));
            GLib.assert(this.txt_rx_audio.win                     != null);
            this.check_changed_state.win              = this.panel_tox_av.win.win_find(new Wx.Str("check_changed_state"));
            GLib.assert(this.check_changed_state.win              != null);
            this.txt_changed_state.win                = this.panel_tox_av.win.win_find(new Wx.Str("txt_changed_state"));
            GLib.assert(this.txt_changed_state.win                != null);
            this.btn_cancel.win                       = this.win.win_find(new Wx.Str("btn_cancel"));
            GLib.assert(this.btn_cancel.win                       != null);
            this.lbl_note.win                         = this.win.win_find(new Wx.Str("lbl_note"));
            GLib.assert(this.lbl_note.win                         != null);
            this.btn_apply_settings.win               = this.win.win_find(new Wx.Str("btn_apply_settings"));
            GLib.assert(this.btn_apply_settings.win               != null);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Destroys all windows. They will be deleted in the loop.
         */
        private void win_deinit()
        {
            // Destroy all windows
            this.win.destroy();
        }

        /*----------------------------------------------------------------------------*/

        // Initialize events.
        //
        // Sets widget IDs. Setups events for the widgets.
        private void events_init(Wx.XMLResource xml)
        {
            // Set widget IDs
            this.btn_cancel.win.id_set(Wx.Win.id_t.CANCEL);

            // Setup events for the widgets
            int        id;
            Wx.Closure closure;

            id      = this.btn_apply_settings.win.id_get();
            closure = new Wx.Closure(Ctrl.DlgPreferencesScriptBox.btn_apply_settings_clicked, null);

            Wx.EventHandler.connect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), closure);
        }

        /*----------------------------------------------------------------------------*/

        // Deinitialize events.
        //
        // Deletes events for the widgets.
        private void events_deinit()
        {
            // Delete events for the widgets
            int id;

            id = this.btn_apply_settings.win.id_get();

            Wx.EventHandler.disconnect((void *) this.win, id, id, Wx.EventHandler.cmd_btn_clicked(), 0);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog constructor.
         *
         * Initializes widgets.
         * Initializes events.
         *
         * @param xml XML Based Resource System (XRC) containing the
         *            application GUI
         */
        public DlgPreferencesScriptBox(Wx.XMLResource xml)
        {
            widgets_init(xml);
            events_init(xml);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog destructor.
         *
         * Destroys all windows. They will be deleted in the loop.
         * Deinitializes events.
         */
        ~DlgPreferencesScriptBox()
        {
            events_deinit();
            win_deinit();
        }
    }
}

/**
 * This is the Controller namespace.
 *
 * It is glue between View and Model.
 * Nothing GUI oriented here.
 * No business logic, just connecting things, i. e. UI callbacks.
 */
namespace yat.Ctrl
{
    /**
     * This is the Controller class for the Script Box
     * plugin
     */
    public class CtrlScriptBox : GLib.Object, Model.Plugin
    {
        /*----------------------------------------------------------------------------*/
        /*                                Public Data                                 */
        /*----------------------------------------------------------------------------*/

        /**
         * Model.
         *
         * Not used, direct access to model reference is available.
         */
        public unowned Model.Model           model_ { public get; public construct; } 

        /**
         * View.
         *
         * Not used, direct access to view reference is available.
         */
        public unowned View.View             view_  { public get; public construct; } 

        /**
         * Controller.
         *
         * Not used, direct access to ctrl reference is available.
         */
        public unowned global::yat.Ctrl.Ctrl ctrl_  { public get; public construct; } 

        /**
         * Tox callbacks
         */
        public ToxCoreScriptBox tox_core;

        /**
         * Tox AV callbacks
         */
        public ToxCoreAVScriptBox tox_core_av;

        /**
         * "Preferences" dialog callbacks
         */
        public DlgPreferencesScriptBox dlg_preferences;

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Plugin entry point
         */
        public void plugin_load()
        {
            // This plugin uses a Model-View-Controller architecture.
            // Initialize MVC singleton objects.
            // Controller is already instantiated by libpeas.
            model_scriptbox = new Model.ModelScriptBox();
            view_scriptbox  = new View.ViewScriptBox();
            ctrl_scriptbox  = this;

            // Initialize Controller (instead of constructor)
            ctrl_scriptbox->init();

            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = ctrl_scriptbox->name_get(model, info);

            GLib.debug(_("Start %s %s").printf(name, info.get_version()));
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin exit point
         */
        public void plugin_unload()
        {
            GLib.debug(_("Stop"));

            // Deinitialize Controller (instead of destructor)
            ctrl_scriptbox->deinit();

            // Deinitialize MVC singleton objects.
            // Controller will be deleted by libpeas later.
            delete model_scriptbox;
            delete view_scriptbox;
            ctrl_scriptbox = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration availability
         *
         * @return false: the plugin isn't configurable,
         *         true:  the plugin is configurable
         */
        public bool plugin_cfg_check()
        {
            // This plugin is configurable
            GLib.debug(_("Configuration is available"));
            return true;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Plugin configuration entry point
         */
        public void plugin_cfg_exec()
        {
            GLib.debug(_("Execute configuration"));

            // Reset tab selection.
            // Initialize "Preferences" dialog controls.
            // Show the dialog.
            view_scriptbox->dlg_preferences.notebook.page_sel_set(View.DlgPreferencesScriptBox.NOTEBOOK_PAGE_TOX);
            view_scriptbox->dlg_preferences.txt_self_changed_state.focus_set();
            ctrl_scriptbox->dlg_preferences.gui_reset();
            view_scriptbox->dlg_preferences.show(true);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Initialize the Script Box Controller.
         *
         * Initializes callbacks.
         *
         * Warning! We don't use constructor because of the constraint
         * of the GObject type system. Constructor is emulated.
         */
        public void init()
        {
            // Tox callbacks
            this.tox_core    = new ToxCoreScriptBox();
            this.tox_core_av = new ToxCoreAVScriptBox();

            // Dialog UI callbacks
            this.dlg_preferences = new DlgPreferencesScriptBox();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Deinitialize the ScriptBox Controller.
         *
         * Warning! We don't use destructor because of the constraint
         * of the GObject type system. Destructor is emulated.
         */
        public void deinit()
        {
            // Disconnect signals from callback methods.
            //
            // Can't move these calls to ~ToxCoreScriptBox() and
            // ~ToxCoreAVScriptBox(), because those destructors would never
            // been called then.
            this.tox_core.signals_disconnect();
            this.tox_core_av.signals_disconnect();

            // Tox callbacks.
            //
            // Should delete manually, because there will be no destructor
            // call ~CtrlScriptBox(), which should call destructors of its
            // members subsequently.
            this.tox_core    = null;
            this.tox_core_av = null;
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Execute command
         *
         * @param cmd Shell command to execute
         * @param ... Its arguments
         */
        public void cmd_exec(string cmd, ...)
        {
            // Command
            string  dbg    = _("Script");
            string  cmd_   = cmd;
            string? stdout = null;
            string? stderr = null;
            int     code   = 0;

            // Replace all %s by arguments
            var args = va_list();

            for (uint pos = 1;; pos++)
            {
                string? arg = args.arg();
                if (arg == null)
                    break;

                try
                {
                    var regex = new GLib.Regex("%s" + pos.to_string());
                    cmd_ = regex.replace_literal(cmd_, -1, 0, arg);
                }
                catch (RegexError ex)
                {
                    GLib.assert(false);
                }
            }

            dbg = _("%s: %s").printf(dbg, cmd_);

            // Callback handler
            try
            {
                // Execute command 
                GLib.Process.spawn_command_line_sync(cmd_,
                                                     out stdout,
                                                     out stderr,
                                                     out code
                                                    );
                //bool res = GLib.Process.check_wait_status(code);
                //GLib.assert(res);

                dbg = _("%s: code %d").printf(dbg, code);
            }
            catch (GLib.SpawnError ex)
            {
                dbg = _("%s: %s").printf(dbg, Model.Model.first_ch_lowercase_make(ex.message));
            }

            GLib.debug(dbg);
            if (stdout != null &&
                    stdout != "")
                GLib.debug(stdout);
            if (stderr != null &&
                    stderr != "")
                GLib.debug(stderr);
        }
    }

    /**
     * Tox class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    public class ToxCoreScriptBox : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // This callback is triggered whenever there is a change in the
        // DHT connection state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — state Network state
        //  — param Not used currently
        private void self_state_changed(global::ToxCore.Tox tox,
                                        global::ToxCore.ConnectionStatus state,
                                        void *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd       = model->self.cfg.get_string(name, "cmd_self_changed_state");
                string state_str = state.to_string();

                ctrl_scriptbox->cmd_exec(cmd, state_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a message from a buddy is
        // received.
        //
        // Parameters:
        //  - tox   Tox instance
        //  - uid   Buddy unique identifier
        //  - type  Message type
        //  - msg   Message
        //  - param Not used currently
        private void buddy_rx(global::ToxCore.Tox          tox,
                              uint32                       uid,
                              global::ToxCore.MessageType  type,
                              uint8[]                      msg,
                              void                        *param)

            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_rx_buddy");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      msg_str      = Model.Model.arr2str_convert(msg);
                string      display_name = buddy.display_name_get();

                ctrl_scriptbox->cmd_exec(cmd, display_name, msg_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their name.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — name  Name
        //  — param Not used currently
        private void buddy_name_changed(global::ToxCore.Tox  tox,
                                        uint32               uid,
                                        uint8[]              name,
                                        void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info  = model_scriptbox->info_get();
            unowned string  name_ = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd      = model->self.cfg.get_string(name_, "cmd_buddy_changed_name");
                string name_str = Model.Model.arr2str_convert(name);

                ctrl_scriptbox->cmd_exec(cmd, name_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This event is triggered when a buddy goes offline after
        // having been online, or when a buddy goes online.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state Network state
        //  — param Not used currently
        private void buddy_state_changed(global::ToxCore.Tox               tox,
                                         uint32                            uid,
                                         global::ToxCore.ConnectionStatus  state,
                                         void                             *param)

            requires (state == global::ToxCore.ConnectionStatus.NONE ||
                      state == global::ToxCore.ConnectionStatus.TCP  ||
                      state == global::ToxCore.ConnectionStatus.UDP)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_buddy_changed_state");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      state_str    = state.to_string();

                ctrl_scriptbox->cmd_exec(cmd, display_name, state_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their user
        // state.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — state User state
        //  — param Not used currently
        private void buddy_state_user_changed(global::ToxCore.Tox         tox,
                                              uint32                      uid,
                                              global::ToxCore.UserStatus  state,
                                              void                       *param)

            requires (state == global::ToxCore.UserStatus.NONE ||
                      state == global::ToxCore.UserStatus.AWAY ||
                      state == global::ToxCore.UserStatus.BUSY)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_buddy_changed_user_state");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      state_str    = state.to_string();

                ctrl_scriptbox->cmd_exec(cmd, display_name, state_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy changes their status
        // message.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — status Status
        //  — param  Not used currently
        private void buddy_status_changed(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          uint8[]              status,
                                          void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_buddy_changed_status");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      status_str   = Model.Model.arr2str_convert(status);

                ctrl_scriptbox->cmd_exec(cmd, display_name, status_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy request is received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — key   Public key
        //  — msg   Message
        //  — param Not used currently
        private void buddy_req_rx(global::ToxCore.Tox  tox,
                                  uint8[]              key,
                                  uint8[]              msg,
                                  void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd     = model->self.cfg.get_string(name, "cmd_buddy_rx_req");
                size_t len     = Model.Person.key_size;
                string key_str = "";
                string msg_str = Model.Model.arr2str_convert(msg);

                for (size_t byte = 0; byte < len; byte++)
                    key_str += key[byte].to_string("%02X");

                ctrl_scriptbox->cmd_exec(cmd, key_str, msg_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a buddy request is received.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Buddy unique identifier
        //  — flag  false: buddy is typing,
        //          true:  buddy is not typing
        //  — param Not used currently
        private void buddy_typing(global::ToxCore.Tox  tox,
                                  uint32               uid,
                                  bool                 flag,
                                  void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_typing_buddy");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      flag_str     = flag.to_string();

                ctrl_scriptbox->cmd_exec(cmd, display_name, flag_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — kind     The meaning of the file to be sent
        //  — len      Size in bytes of the file the client wants to
        //             send, UINT64_MAX if unknown or streaming
        //  — filename Name of the file. Does not need to be the
        //             actual name. This name will be sent along
        //             with the file send request.
        //  — param    Not used currently
        private void file_req_rx(global::ToxCore.Tox       tox,
                                 uint32                    uid,
                                 uint32                    file_num,
                                 global::ToxCore.FileKind  kind,
                                 uint64                    len,
                                 uint8[]                   filename,
                                 void                     *param)
            requires (kind == global::ToxCore.FileKind.DATA ||
                      kind == global::ToxCore.FileKind.AVATAR)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_file_rx_req");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      file_num_str = file_num.to_string();
                string      kind_str     = kind.to_string();
                string      len_str      = len.to_string();
                string      filename_str = Model.Model.arr2str_convert(filename);

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         file_num_str,
                                         kind_str,
                                         len_str,
                                         filename_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a file transfer request is
        // received.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             received is associated with
        //  — offset   The file position to receive
        //  — buf      A byte array containing the received chunk
        //  — param    Not used currently
        private void file_chunk_rx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   uint8[]              buf,
                                   void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_file_rx_chunk");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      file_num_str = file_num.to_string();
                string      offset_str   = offset.to_string();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         file_num_str,
                                         offset_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when core is ready to send more
        // file data.
        //
        // Parameters:
        //  — tox      Tox instance
        //  — uid      Buddy unique identifier
        //  — file_num The friend-specific file number the data
        //             transmit is associated with
        //  — offset   The file position to transmit
        //  — len      The number of bytes requested for the current
        //             chunk
        //  — param    Not used currently
        private void file_chunk_tx(global::ToxCore.Tox  tox,
                                   uint32               uid,
                                   uint32               file_num,
                                   uint64               offset,
                                   size_t               len,
                                   void                *param)
            requires (param == null)
        {
            //ctrl_scriptbox->cmd_exec("cmd_file_tx_chunk");
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_file_rx_chunk");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      file_num_str = file_num.to_string();
                string      offset_str   = offset.to_string();
                string      len_str      = len.to_string();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         file_num_str,
                                         offset_str,
                                         len_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client receives a
        // conference message.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — type           Message type
        //  — msg            Message
        //  — param          Not used currently
        private void conference_rx(global::ToxCore.Tox          tox,
                                   uint32                       uid_conference,
                                   uint32                       uid_member,
                                   global::ToxCore.MessageType  type,
                                   uint8[]                      msg,
                                   void                        *param)
            requires (type == global::ToxCore.MessageType.NORMAL ||
                      type == global::ToxCore.MessageType.ACTION)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string           cmd                     = model->self.cfg.get_string(name, "cmd_conference_rx_message");
                Model.Conference conference              = model->conferences_hash[uid_conference];
                Model.Member     member                  = conference.members_hash[uid_member];
                string           display_name_conference = conference.display_name_get();
                string           display_name_member     = member.display_name_get();
                string           type_str                = type.to_string();
                string           msg_str                 = Model.Model.arr2str_convert(msg);

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name_conference,
                                         display_name_member,
                                         type_str,
                                         msg_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody joins conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_joins(Model.Conference conference, Model.Member member)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd                     = model->self.cfg.get_string(name, "cmd_joins_member");
                string display_name_conference = conference.display_name_get();
                string display_name_member     = member.display_name_get();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name_conference,
                                         display_name_member);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when somebody leaves conference.
        //
        // Parameters:
        //  - conference Conference
        //  - member     Member
        private void member_leaves(Model.Conference conference, Model.Member member)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd                     = model->self.cfg.get_string(name, "cmd_leaves_member");
                string display_name_conference = conference.display_name_get();
                string display_name_member     = member.display_name_get();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name_conference,
                                         display_name_member);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when when a peer changes their
        // name.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid.
        //                   They are different.
        //  — name           Name
        //  — param          Not used currently
        private void member_name_changed(global::ToxCore.Tox  tox,
                                         uint32               uid_conference,
                                         uint32               uid_member,
                                         uint8[]              name,
                                         void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info  = model_scriptbox->info_get();
            unowned string  name_ = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string           cmd          = model->self.cfg.get_string(name_, "cmd_member_changed_name");
                Model.Conference conference   = model->conferences_hash[uid_conference];
                string           display_name = conference.display_name_get();
                string           name_str     = Model.Model.arr2str_convert(name);

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         name_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer changes the conference
        // title.
        //
        // Parameters:
        //  — tox            Tox instance
        //  — uid_conference Conference unique identifier
        //  — uid_member     Member unique identifier.
        //                   Note that it differs from buddy's uid!
        //                   They are different.
        //  — title          Title,
        //  — param          Not used currently.
        private void title_changed(global::ToxCore.Tox  tox,
                                   uint32               uid_conference,
                                   uint32               uid_member,
                                   uint8[]              title,
                                   void                *param)
 
            requires (param == null)
        {
            // Ignore initial joining
            if (uid_member == uint32.MAX)
                return;

            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string           cmd          = model->self.cfg.get_string(name, "cmd_changed_title");
                Model.Conference conference   = model->conferences_hash[uid_conference];
                Model.Member     member       = conference.members_hash[uid_member];
                string           display_name = member.display_name_get();
                string           title_str    = Model.Model.arr2str_convert(title);

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         title_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client is invited to join
        // a conference.
        //
        // Parameters:
        //  — tox    Tox instance
        //  — uid    Buddy unique identifier
        //  — type   The conference type (text only or audio/video)
        //  — cookie A piece of data of variable length required to join
        //           the conference
        //  — param  Not used currently
        private void conference_invited(global::ToxCore.Tox              tox,
                                        uint32                           uid,
                                        uint32                           type,
                                        //global::ToxCore.CONFERENCE_TYPE  type, // valac generates wrong code, so I disabled it
                                        uint8[]                          cookie,
                                        void                            *param)
            requires (type == global::ToxCore.CONFERENCE_TYPE.TEXT ||
                      type == global::ToxCore.CONFERENCE_TYPE.AV)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_invited_conference");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      type_str     = type.to_string();
                string      cookie_str   = Model.Model.bin2hex_convert(cookie);

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         type_str,
                                         cookie_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when the client successfully
        // connects to a conference after joining it.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_connected(global::ToxCore.Tox  tox,
                                          uint32               uid,
                                          void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string           cmd          = model->self.cfg.get_string(name, "cmd_connected_conference");
                Model.Conference conference   = model->conferences_hash[uid];
                string           display_name = conference.display_name_get();

                ctrl_scriptbox->cmd_exec(cmd, display_name);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        // This callback is triggered when a peer joins or leaves the
        // conference.
        //
        // Parameters:
        //  — tox   Tox instance
        //  — uid   Conference unique identifier
        //  — param Not used currently
        private void conference_members_changed(global::ToxCore.Tox  tox,
                                                uint32               uid,
                                                void                *param)
            requires (param == null)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string cmd          = model->self.cfg.get_string(name, "cmd_changed_conference_members");
                Model.Conference conference = model->conferences_hash[uid];
                string display_name = conference.display_name_get();

                ctrl_scriptbox->cmd_exec(cmd, display_name);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreScriptBox()
        {
            signals_connect();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Tox callback destructor.
         *
         * Disconnects signals from callback methods.
         */
        ~ToxCoreScriptBox()
        {
            //signals_disconnect(); // Should do it manually, otherwise destructor would not been ever called
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Connect signals to callback methods.
         *
         * This plugin uses signals which are located in main
         * application.
         */
        public void signals_connect()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Connect signals to callback method
            try                          { if (model->self.cfg.get_boolean(name, "check_self_changed_state"))         ctrl->tox_core.self_state_changed_sig.connect(self_state_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_self_changed_state",         false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_rx_buddy"))                   ctrl->tox_core.buddy_rx_sig.connect(buddy_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_rx_buddy",                   false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_buddy_changed_name"))         ctrl->tox_core.buddy_name_changed_sig.connect(buddy_name_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_buddy_changed_name",         false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_buddy_changed_state"))        ctrl->tox_core.buddy_state_changed_sig.connect(buddy_state_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_buddy_changed_state",        false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_buddy_changed_user_state"))   ctrl->tox_core.buddy_state_user_changed_sig.connect(buddy_state_user_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_buddy_changed_user_state",   false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_buddy_changed_status"))       ctrl->tox_core.buddy_status_changed_sig.connect(buddy_status_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_buddy_changed_status",       false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_buddy_rx_req"))               ctrl->tox_core.buddy_req_rx_sig.connect(buddy_req_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_buddy_rx_req",               false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_typing_buddy"))               ctrl->tox_core.buddy_typing_sig.connect(buddy_typing); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_typing_buddy",               false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_file_rx_req"))                ctrl->tox_core.file_req_rx_sig.connect(file_req_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_file_rx_req",                false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_file_rx_chunk"))              ctrl->tox_core.file_chunk_rx_sig.connect(file_chunk_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_file_rx_chunk",              false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_file_tx_chunk"))              ctrl->tox_core.file_chunk_tx_sig.connect(file_chunk_tx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_file_tx_chunk",              false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_conference_rx_message"))      ctrl->tox_core.conference_rx_sig.connect(conference_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_conference_rx_message",      false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_joins_member"))               ctrl->tox_core.member_joins_sig.connect(member_joins); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_joins_member",               false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_leaves_member"))              ctrl->tox_core.member_leaves_sig.connect(member_leaves); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_leaves_member",              false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_member_changed_name"))        ctrl->tox_core.member_name_changed_sig.connect(member_name_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_member_changed_name",        false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_changed_title"))              ctrl->tox_core.title_changed_sig.connect(title_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_changed_title",              false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_invited_conference"))         ctrl->tox_core.conference_invited_sig.connect(conference_invited); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_invited_conference",         false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_connected_conference"))       ctrl->tox_core.conference_connected_sig.connect(conference_connected); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_connected_conference",       false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_changed_conference_members")) ctrl->tox_core.conference_members_changed_sig.connect(conference_members_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_changed_conference_members", false); }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disconnects signals from callback methods
         */
        public void signals_disconnect()
        {
            ctrl->tox_core.self_state_changed_sig.disconnect(self_state_changed);
            ctrl->tox_core.buddy_rx_sig.disconnect(buddy_rx);
            ctrl->tox_core.buddy_name_changed_sig.disconnect(buddy_name_changed);
            ctrl->tox_core.buddy_state_changed_sig.disconnect(buddy_state_changed);
            ctrl->tox_core.buddy_state_user_changed_sig.disconnect(buddy_state_user_changed);
            ctrl->tox_core.buddy_status_changed_sig.disconnect(buddy_status_changed);
            ctrl->tox_core.buddy_req_rx_sig.disconnect(buddy_req_rx);
            ctrl->tox_core.buddy_typing_sig.disconnect(buddy_typing);
            ctrl->tox_core.file_req_rx_sig.disconnect(file_req_rx);
            ctrl->tox_core.file_chunk_rx_sig.disconnect(file_chunk_rx);
            ctrl->tox_core.file_chunk_tx_sig.disconnect(file_chunk_tx);
            ctrl->tox_core.conference_rx_sig.disconnect(conference_rx);
            ctrl->tox_core.member_joins_sig.disconnect(member_joins);
            ctrl->tox_core.member_leaves_sig.disconnect(member_leaves);
            ctrl->tox_core.member_name_changed_sig.disconnect(member_name_changed);
            ctrl->tox_core.title_changed_sig.disconnect(title_changed);
            ctrl->tox_core.conference_invited_sig.disconnect(conference_invited);
            ctrl->tox_core.conference_connected_sig.disconnect(conference_connected);
            ctrl->tox_core.conference_members_changed_sig.disconnect(conference_members_changed);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update signal connections. (Should connect/disconnect something?)
         *
         * It also updates config using current GUI control values.
         */
        public void signals_upd()
        {
            // Update signal connections
            try
            {
                // Plugin description fields
                Peas.PluginInfo info = model_scriptbox->info_get();
                unowned string  name = info.get_module_name();

                // Get check box values
                bool check_self_changed_state_flag         = view_scriptbox->dlg_preferences.check_self_changed_state.val_get();
                bool check_rx_buddy_flag                   = view_scriptbox->dlg_preferences.check_rx_buddy.val_get();
                bool check_buddy_changed_name_flag         = view_scriptbox->dlg_preferences.check_buddy_changed_name.val_get();
                bool check_buddy_changed_state_flag        = view_scriptbox->dlg_preferences.check_buddy_changed_state.val_get();
                bool check_buddy_changed_user_state_flag   = view_scriptbox->dlg_preferences.check_buddy_changed_user_state.val_get();
                bool check_buddy_changed_status_flag       = view_scriptbox->dlg_preferences.check_buddy_changed_status.val_get();
                bool check_buddy_rx_req_flag               = view_scriptbox->dlg_preferences.check_buddy_rx_req.val_get();
                bool check_typing_buddy_flag               = view_scriptbox->dlg_preferences.check_typing_buddy.val_get();
                bool check_file_rx_req_flag                = view_scriptbox->dlg_preferences.check_file_rx_req.val_get();
                bool check_file_rx_chunk_flag              = view_scriptbox->dlg_preferences.check_file_rx_chunk.val_get();
                bool check_file_tx_chunk_flag              = view_scriptbox->dlg_preferences.check_file_tx_chunk.val_get();
                bool check_conference_rx_message_flag      = view_scriptbox->dlg_preferences.check_conference_rx_message.val_get();
                bool check_joins_member_flag               = view_scriptbox->dlg_preferences.check_joins_member.val_get();
                bool check_leaves_member_flag              = view_scriptbox->dlg_preferences.check_leaves_member.val_get();
                bool check_member_changed_name_flag        = view_scriptbox->dlg_preferences.check_member_changed_name.val_get();
                bool check_changed_title_flag              = view_scriptbox->dlg_preferences.check_changed_title.val_get();
                bool check_invited_conference_flag         = view_scriptbox->dlg_preferences.check_invited_conference.val_get();
                bool check_connected_conference_flag       = view_scriptbox->dlg_preferences.check_connected_conference.val_get();
                bool check_changed_conference_members_flag = view_scriptbox->dlg_preferences.check_changed_conference_members.val_get();

                // Connect/disconnect signal to callback method
                if (check_self_changed_state_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_self_changed_state"))
                        ctrl->tox_core.self_state_changed_sig.connect(self_state_changed);
                }
                else
                    ctrl->tox_core.self_state_changed_sig.disconnect(self_state_changed);

                if (check_rx_buddy_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_rx_buddy"))
                        ctrl->tox_core.buddy_rx_sig.connect(buddy_rx);
                }
                else
                    ctrl->tox_core.buddy_rx_sig.disconnect(buddy_rx);

                if (check_buddy_changed_name_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_buddy_changed_name"))
                        ctrl->tox_core.buddy_name_changed_sig.connect(buddy_name_changed);
                }
                else
                    ctrl->tox_core.buddy_name_changed_sig.disconnect(buddy_name_changed);

                if (check_buddy_changed_state_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_buddy_changed_state"))
                        ctrl->tox_core.buddy_state_changed_sig.connect(buddy_state_changed);
                }
                else
                    ctrl->tox_core.buddy_state_changed_sig.disconnect(buddy_state_changed);

                if (check_buddy_changed_user_state_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_buddy_changed_user_state"))
                        ctrl->tox_core.buddy_state_user_changed_sig.connect(buddy_state_user_changed);
                }
                else
                    ctrl->tox_core.buddy_state_user_changed_sig.disconnect(buddy_state_user_changed);

                if (check_buddy_changed_status_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_buddy_changed_status"))
                        ctrl->tox_core.buddy_status_changed_sig.connect(buddy_status_changed);
                }
                else
                    ctrl->tox_core.buddy_status_changed_sig.disconnect(buddy_status_changed);

                if (check_buddy_rx_req_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_buddy_rx_req"))
                        ctrl->tox_core.buddy_req_rx_sig.connect(buddy_req_rx);
                }
                else
                    ctrl->tox_core.buddy_req_rx_sig.disconnect(buddy_req_rx);

                if (check_typing_buddy_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_typing_buddy"))
                        ctrl->tox_core.buddy_typing_sig.connect(buddy_typing);
                }
                else
                    ctrl->tox_core.buddy_typing_sig.disconnect(buddy_typing);

                if (check_file_rx_req_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_file_rx_req"))
                        ctrl->tox_core.file_req_rx_sig.connect(file_req_rx);
                }
                else
                    ctrl->tox_core.file_req_rx_sig.disconnect(file_req_rx);

                if (check_file_rx_chunk_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_file_rx_chunk"))
                        ctrl->tox_core.file_chunk_rx_sig.connect(file_chunk_rx);
                }
                else
                    ctrl->tox_core.file_chunk_rx_sig.disconnect(file_chunk_rx);

                if (check_file_tx_chunk_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_file_tx_chunk"))
                        ctrl->tox_core.file_chunk_tx_sig.connect(file_chunk_tx);
                }
                else
                    ctrl->tox_core.file_chunk_tx_sig.disconnect(file_chunk_tx);

                if (check_conference_rx_message_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_conference_rx_message"))
                        ctrl->tox_core.conference_rx_sig.connect(conference_rx);
                }
                else
                    ctrl->tox_core.conference_rx_sig.disconnect(conference_rx);

                if (check_joins_member_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_joins_member"))
                        ctrl->tox_core.member_joins_sig.connect(member_joins);
                }
                else
                    ctrl->tox_core.member_joins_sig.disconnect(member_joins);

                if (check_leaves_member_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_leaves_member"))
                        ctrl->tox_core.member_leaves_sig.connect(member_leaves);
                }
                else
                    ctrl->tox_core.member_leaves_sig.disconnect(member_leaves);

                if (check_member_changed_name_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_member_changed_name"))
                        ctrl->tox_core.member_name_changed_sig.connect(member_name_changed);
                }
                else
                    ctrl->tox_core.member_name_changed_sig.disconnect(member_name_changed);

                if (check_changed_title_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_changed_title"))
                        ctrl->tox_core.title_changed_sig.connect(title_changed);
                }
                else
                    ctrl->tox_core.title_changed_sig.disconnect(title_changed);

                if (check_invited_conference_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_invited_conference"))
                        ctrl->tox_core.conference_invited_sig.connect(conference_invited);
                }
                else
                    ctrl->tox_core.conference_invited_sig.disconnect(conference_invited);

                if (check_connected_conference_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_connected_conference"))
                        ctrl->tox_core.conference_connected_sig.connect(conference_connected);
                }
                else
                    ctrl->tox_core.conference_connected_sig.disconnect(conference_connected);

                if (check_changed_conference_members_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_changed_conference_members"))
                        ctrl->tox_core.conference_members_changed_sig.connect(conference_members_changed);
                }
                else
                    ctrl->tox_core.conference_members_changed_sig.disconnect(conference_members_changed);

                // Update per-profile config with value from GUI
                model->self.cfg.set_boolean(name, "check_self_changed_state",         check_self_changed_state_flag);
                model->self.cfg.set_boolean(name, "check_rx_buddy",                   check_rx_buddy_flag);
                model->self.cfg.set_boolean(name, "check_buddy_changed_name",         check_buddy_changed_name_flag);
                model->self.cfg.set_boolean(name, "check_buddy_changed_state",        check_buddy_changed_state_flag);
                model->self.cfg.set_boolean(name, "check_buddy_changed_user_state",   check_buddy_changed_user_state_flag);
                model->self.cfg.set_boolean(name, "check_buddy_changed_status",       check_buddy_changed_status_flag);
                model->self.cfg.set_boolean(name, "check_buddy_rx_req",               check_buddy_rx_req_flag);
                model->self.cfg.set_boolean(name, "check_typing_buddy",               check_typing_buddy_flag);
                model->self.cfg.set_boolean(name, "check_file_rx_req",                check_file_rx_req_flag);
                model->self.cfg.set_boolean(name, "check_file_rx_chunk",              check_file_rx_chunk_flag);
                model->self.cfg.set_boolean(name, "check_file_tx_chunk",              check_file_tx_chunk_flag);
                model->self.cfg.set_boolean(name, "check_conference_rx_message",      check_conference_rx_message_flag);
                model->self.cfg.set_boolean(name, "check_joins_member",               check_joins_member_flag);
                model->self.cfg.set_boolean(name, "check_leaves_member",              check_leaves_member_flag);
                model->self.cfg.set_boolean(name, "check_member_changed_name",        check_member_changed_name_flag);
                model->self.cfg.set_boolean(name, "check_changed_title",              check_changed_title_flag);
                model->self.cfg.set_boolean(name, "check_invited_conference",         check_invited_conference_flag);
                model->self.cfg.set_boolean(name, "check_connected_conference",       check_connected_conference_flag);
                model->self.cfg.set_boolean(name, "check_changed_conference_members", check_changed_conference_members_flag);
            }
            catch (GLib.KeyFileError ex) 
            {
                GLib.assert(false);
            }
        }
    }

    /**
     * Tox AV class for callbacks.
     *
     * It contains UI callbacks for the corresponding methods in
     * Model part.
     */
    public class ToxCoreAVScriptBox : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when a peer calls us
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param audio  true:  the buddy is sending audio,
         *               false: otherwise,
         * @param video  true:  the buddy is sending video,
         *               false: otherwise
         */
        private void called(ToxAV.ToxAV tox_av,
                            uint32      uid,
                            bool        audio,
                            bool        video)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_called_by_peer");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      audio_str    = audio.to_string();
                string      video_str    = video.to_string();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         audio_str,
                                         video_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when the network becomes too
         * saturated for current bit rates at which point core suggests
         * new bitrates.
         *
         * @param uid           Conference unique identifier
         * @param audio_bitrate Suggested maximum audio bitrate in
         *                      Kb/s
         * @param video_bitrate Suggested maximum video bitrate in
         *                      Kb/s
         */
        private void bitrate_suggest(uint32 uid,
                                     uint32 audio_bitrate,
                                     uint32 video_bitrate)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd               = model->self.cfg.get_string(name, "cmd_suggest_bitrate");
                Model.Buddy buddy             = model->buddies_hash[uid];
                string      display_name      = buddy.display_name_get();
                string      audio_bitrate_str = audio_bitrate.to_string();
                string      video_bitrate_str = video_bitrate.to_string();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         audio_bitrate_str,
                                         video_bitrate_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when an audio frame is coming
         *
         * @param tox_av       Tox A/V instance
         * @param uid          Conference unique identifier
         * @param pcm          An array of audio samples
         *                     (cnt_samples * cnt_channels elements)
         * @param cnt_samples  The number of audio samples per channel
         *                     in the PCM array
         * @param cnt_channels Number of audio channels
         * @param bitrate      Sampling rate used in this frame
         */
        private void audio_rx(ToxAV.ToxAV tox_av,
                              uint32      uid,
                              int16[]     pcm,
                              size_t      cnt_samples,
                              uint8       cnt_channels,
                              uint32      bitrate)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd              = model->self.cfg.get_string(name, "cmd_rx_audio");
                Model.Buddy buddy            = model->buddies_hash[uid];
                string      display_name     = buddy.display_name_get();
                string      cnt_samples_str  = cnt_samples.to_string();
                string      cnt_channels_str = cnt_channels.to_string();
                string      bitrate_str      = bitrate.to_string();

                ctrl_scriptbox->cmd_exec(cmd,
                                         display_name,
                                         cnt_samples_str,
                                         cnt_channels_str,
                                         bitrate_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This callback is triggered when an event is issued
         *
         * @param tox_av Tox A/V instance
         * @param uid    Conference unique identifier
         * @param state  Bitmask of ToxAV.FriendCallState enums
         */
        private void buddy_av_state_changed(ToxAV.ToxAV tox_av,
                                            uint32      uid,
                                            uint32      state)
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Prepare command and its arguments.
            // Execute it.
            try
            {
                string      cmd          = model->self.cfg.get_string(name, "cmd_changed_state");
                Model.Buddy buddy        = model->buddies_hash[uid];
                string      display_name = buddy.display_name_get();
                string      state_str    = state.to_string();

                ctrl_scriptbox->cmd_exec(cmd, display_name, state_str);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * Tox AV callback constructor.
         *
         * Connects signals to callback methods.
         */
        public ToxCoreAVScriptBox()
        {
            signals_connect();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Tox AV callback destructor.
         *
         * Disconnects signals from callback methods.
         */
        ~ToxCoreAVScriptBox()
        {
            //signals_disconnect(); // Should do it manually, otherwise destructor would not been ever called
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Connect signals to callback methods.
         *
         * This plugin uses signals which are located in main
         * application.
         */
        public void signals_connect()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Connect signals to callback method
            try                          { if (model->self.cfg.get_boolean(name, "check_called_by_peer"))  ctrl->tox_core_av.called_sig.connect(called); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_called_by_peer",  false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_suggest_bitrate")) ctrl->tox_core_av.bitrate_suggest_sig.connect(bitrate_suggest); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_suggest_bitrate", false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_rx_audio"))        ctrl->tox_core_av.audio_rx_sig.connect(audio_rx); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_rx_audio",        false); }
            try                          { if (model->self.cfg.get_boolean(name, "check_changed_state"))   ctrl->tox_core_av.buddy_av_state_changed_sig.connect(buddy_av_state_changed); }
            catch (GLib.KeyFileError ex) {     model->self.cfg.set_boolean(name, "check_changed_state",   false); }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Disconnects signals from callback methods
         */
        public void signals_disconnect()
        {
            ctrl->tox_core_av.called_sig.disconnect(called);
            ctrl->tox_core_av.bitrate_suggest_sig.disconnect(bitrate_suggest);
            ctrl->tox_core_av.audio_rx_sig.disconnect(audio_rx);
            ctrl->tox_core_av.buddy_av_state_changed_sig.disconnect(buddy_av_state_changed);
        }

        /*----------------------------------------------------------------------------*/

        /**
         * Update signal connections. (Should connect/disconnect something?)
         *
         * It also updates config using current GUI control values.
         */
        public void signals_upd()
        {
            // Update signal connections
            try
            {
                // Plugin description fields
                Peas.PluginInfo info = model_scriptbox->info_get();
                unowned string  name = info.get_module_name();

                // Get check box values
                bool check_called_by_peer_flag  = view_scriptbox->dlg_preferences.check_called_by_peer.val_get();
                bool check_suggest_bitrate_flag = view_scriptbox->dlg_preferences.check_suggest_bitrate.val_get();
                bool check_rx_audio_flag        = view_scriptbox->dlg_preferences.check_rx_audio.val_get();
                bool check_changed_state_flag   = view_scriptbox->dlg_preferences.check_changed_state.val_get();

                // Connect/disconnect signals to/from callback method
                if (check_called_by_peer_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_called_by_peer"))
                        ctrl->tox_core_av.called_sig.connect(called);
                }
                else
                    ctrl->tox_core_av.called_sig.disconnect(called);

                if (check_suggest_bitrate_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_suggest_bitrate"))
                        ctrl->tox_core_av.bitrate_suggest_sig.connect(bitrate_suggest);
                }
                else
                    ctrl->tox_core_av.bitrate_suggest_sig.disconnect(bitrate_suggest);

                if (check_rx_audio_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_rx_audio"))
                        ctrl->tox_core_av.audio_rx_sig.connect(audio_rx);
                }
                else
                    ctrl->tox_core_av.audio_rx_sig.disconnect(audio_rx);

                if (check_changed_state_flag)
                {
                    if (!model->self.cfg.get_boolean(name, "check_changed_state"))
                        ctrl->tox_core_av.buddy_av_state_changed_sig.connect(buddy_av_state_changed);
                }
                else
                    ctrl->tox_core_av.buddy_av_state_changed_sig.disconnect(buddy_av_state_changed);

                // Update per-profile config with value from GUI
                model->self.cfg.set_boolean(name, "check_called_by_peer",  check_called_by_peer_flag);
                model->self.cfg.set_boolean(name, "check_suggest_bitrate", check_suggest_bitrate_flag);
                model->self.cfg.set_boolean(name, "check_rx_audio",        check_rx_audio_flag);
                model->self.cfg.set_boolean(name, "check_changed_state",   check_changed_state_flag);
            }
            catch (GLib.KeyFileError ex) 
            {
                GLib.assert(false);
            }
        }
    }

    /**
     * This is the "Preferences" dialog class for callbacks.
     *
     * It contains UI callbacks for the corresponding class in View
     * part.
     */
    public class DlgPreferencesScriptBox : GLib.Object
    {
        /*----------------------------------------------------------------------------*/
        /*                              Private Methods                               */
        /*----------------------------------------------------------------------------*/

        // Initialize profile-dependent plugin settings
        private void cfg_init()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Set default values for keys which are not set
            try                          { model->self.cfg.get_string(name, "cmd_self_changed_state"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_self_changed_state", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_rx_buddy"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_rx_buddy", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_buddy_changed_name"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_buddy_changed_name", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_buddy_changed_state"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_buddy_changed_state", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_buddy_changed_user_state"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_buddy_changed_user_state", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_buddy_changed_status"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_buddy_changed_status", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_buddy_rx_req"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_buddy_rx_req", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_typing_buddy"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_typing_buddy", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_file_rx_req"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_file_rx_req", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_file_rx_chunk"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_file_rx_chunk", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_file_tx_chunk"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_file_tx_chunk", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_conference_rx_message"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_conference_rx_message", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_joins_member"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_joins_member", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_leaves_member"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_leaves_member", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_member_changed_name"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_member_changed_name", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_changed_title"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_changed_title", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_invited_conference"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_invited_conference", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_connected_conference"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_connected_conference", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_changed_conference_members"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_changed_conference_members", ""); }

            try                          { model->self.cfg.get_string(name, "cmd_called_by_peer"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_called_by_peer", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_suggest_bitrate"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_suggest_bitrate", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_rx_audio"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_rx_audio", ""); }
            try                          { model->self.cfg.get_string(name, "cmd_changed_state"); }
            catch (GLib.KeyFileError ex) { model->self.cfg.set_string(name, "cmd_changed_state", ""); }
        }

        /*----------------------------------------------------------------------------*/

        // Update profile-dependent plugin settings using current GUI
        // control values
        private void cfg_upd()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Update per-profile config with value from GUI
            string cmd_self_changed_state         = view_scriptbox->dlg_preferences.txt_self_changed_state.val_get();
            string cmd_rx_buddy                   = view_scriptbox->dlg_preferences.txt_rx_buddy.val_get();
            string cmd_buddy_changed_name         = view_scriptbox->dlg_preferences.txt_buddy_changed_name.val_get();
            string cmd_buddy_changed_state        = view_scriptbox->dlg_preferences.txt_buddy_changed_state.val_get();
            string cmd_buddy_changed_user_state   = view_scriptbox->dlg_preferences.txt_buddy_changed_user_state.val_get();
            string cmd_buddy_changed_status       = view_scriptbox->dlg_preferences.txt_buddy_changed_status.val_get();
            string cmd_buddy_rx_req               = view_scriptbox->dlg_preferences.txt_buddy_rx_req.val_get();
            string cmd_typing_buddy               = view_scriptbox->dlg_preferences.txt_typing_buddy.val_get();
            string cmd_file_rx_req                = view_scriptbox->dlg_preferences.txt_file_rx_req.val_get();
            string cmd_file_rx_chunk              = view_scriptbox->dlg_preferences.txt_file_rx_chunk.val_get();
            string cmd_file_tx_chunk              = view_scriptbox->dlg_preferences.txt_file_tx_chunk.val_get();
            string cmd_conference_rx_message      = view_scriptbox->dlg_preferences.txt_conference_rx_message.val_get();
            string cmd_joins_member               = view_scriptbox->dlg_preferences.txt_joins_member.val_get();
            string cmd_leaves_member              = view_scriptbox->dlg_preferences.txt_leaves_member.val_get();
            string cmd_member_changed_name        = view_scriptbox->dlg_preferences.txt_member_changed_name.val_get();
            string cmd_changed_title              = view_scriptbox->dlg_preferences.txt_changed_title.val_get();
            string cmd_invited_conference         = view_scriptbox->dlg_preferences.txt_invited_conference.val_get();
            string cmd_connected_conference       = view_scriptbox->dlg_preferences.txt_connected_conference.val_get();
            string cmd_changed_conference_members = view_scriptbox->dlg_preferences.txt_changed_conference_members.val_get();

            string cmd_called_by_peer             = view_scriptbox->dlg_preferences.txt_called_by_peer.val_get();
            string cmd_suggest_bitrate            = view_scriptbox->dlg_preferences.txt_suggest_bitrate.val_get();
            string cmd_rx_audio                   = view_scriptbox->dlg_preferences.txt_rx_audio.val_get();
            string cmd_changed_state              = view_scriptbox->dlg_preferences.txt_changed_state.val_get();

            model->self.cfg.set_string(name, "cmd_self_changed_state",         cmd_self_changed_state);
            model->self.cfg.set_string(name, "cmd_rx_buddy",                   cmd_rx_buddy);
            model->self.cfg.set_string(name, "cmd_buddy_changed_name",         cmd_buddy_changed_name);
            model->self.cfg.set_string(name, "cmd_buddy_changed_state",        cmd_buddy_changed_state);
            model->self.cfg.set_string(name, "cmd_buddy_changed_user_state",   cmd_buddy_changed_user_state);
            model->self.cfg.set_string(name, "cmd_buddy_changed_status",       cmd_buddy_changed_status);
            model->self.cfg.set_string(name, "cmd_buddy_rx_req",               cmd_buddy_rx_req);
            model->self.cfg.set_string(name, "cmd_typing_buddy",               cmd_typing_buddy);
            model->self.cfg.set_string(name, "cmd_file_rx_req",                cmd_file_rx_req);
            model->self.cfg.set_string(name, "cmd_file_rx_chunk",              cmd_file_rx_chunk);
            model->self.cfg.set_string(name, "cmd_file_tx_chunk",              cmd_file_tx_chunk);
            model->self.cfg.set_string(name, "cmd_conference_rx_message",      cmd_conference_rx_message);
            model->self.cfg.set_string(name, "cmd_joins_member",               cmd_joins_member);
            model->self.cfg.set_string(name, "cmd_leaves_member",              cmd_leaves_member);
            model->self.cfg.set_string(name, "cmd_member_changed_name",        cmd_member_changed_name);
            model->self.cfg.set_string(name, "cmd_changed_title",              cmd_changed_title);
            model->self.cfg.set_string(name, "cmd_invited_conference",         cmd_invited_conference);
            model->self.cfg.set_string(name, "cmd_connected_conference",       cmd_connected_conference);
            model->self.cfg.set_string(name, "cmd_changed_conference_members", cmd_changed_conference_members);

            model->self.cfg.set_string(name, "cmd_called_by_peer",             cmd_called_by_peer);
            model->self.cfg.set_string(name, "cmd_suggest_bitrate",            cmd_suggest_bitrate);
            model->self.cfg.set_string(name, "cmd_rx_audio",                   cmd_rx_audio);
            model->self.cfg.set_string(name, "cmd_changed_state",              cmd_changed_state);
        }

        /*----------------------------------------------------------------------------*/
        /*                               Public Methods                               */
        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog callback destructor
         */
        public DlgPreferencesScriptBox()
        {
            // Initialize profile-dependent plugin settings
            cfg_init();
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Preferences" dialog callback destructor
         */
        ~DlgPreferencesScriptBox()
        {
        }

        /*----------------------------------------------------------------------------*/

        /**
         * This method is to be called when all get loaded and starts
         */
        public void gui_reset()
        {
            // Plugin description fields
            Peas.PluginInfo info = model_scriptbox->info_get();
            unowned string  name = info.get_module_name();

            // Setup GUI widget initial states using values from per-profile
            // config:
            //
            //  - tab 1 check boxes,
            try
            {
                bool check_self_changed_state_flag         = model->self.cfg.get_boolean(name, "check_self_changed_state");
                bool check_rx_buddy_flag                   = model->self.cfg.get_boolean(name, "check_rx_buddy");
                bool check_buddy_changed_name_flag         = model->self.cfg.get_boolean(name, "check_buddy_changed_name");
                bool check_buddy_changed_state_flag        = model->self.cfg.get_boolean(name, "check_buddy_changed_state");
                bool check_buddy_changed_user_state_flag   = model->self.cfg.get_boolean(name, "check_buddy_changed_user_state");
                bool check_buddy_changed_status_flag       = model->self.cfg.get_boolean(name, "check_buddy_changed_status");
                bool check_buddy_rx_req_flag               = model->self.cfg.get_boolean(name, "check_buddy_rx_req");
                bool check_typing_buddy_flag               = model->self.cfg.get_boolean(name, "check_typing_buddy");
                bool check_file_rx_req_flag                = model->self.cfg.get_boolean(name, "check_file_rx_req");
                bool check_file_rx_chunk_flag              = model->self.cfg.get_boolean(name, "check_file_rx_chunk");
                bool check_file_tx_chunk_flag              = model->self.cfg.get_boolean(name, "check_file_tx_chunk");
                bool check_conference_rx_message_flag      = model->self.cfg.get_boolean(name, "check_conference_rx_message");
                bool check_joins_member_flag               = model->self.cfg.get_boolean(name, "check_joins_member");
                bool check_leaves_member_flag              = model->self.cfg.get_boolean(name, "check_leaves_member");
                bool check_member_changed_name_flag        = model->self.cfg.get_boolean(name, "check_member_changed_name");
                bool check_changed_title_flag              = model->self.cfg.get_boolean(name, "check_changed_title");
                bool check_invited_conference_flag         = model->self.cfg.get_boolean(name, "check_invited_conference");
                bool check_connected_conference_flag       = model->self.cfg.get_boolean(name, "check_connected_conference");
                bool check_changed_conference_members_flag = model->self.cfg.get_boolean(name, "check_changed_conference_members");

                view_scriptbox->dlg_preferences.check_self_changed_state.val_set(check_self_changed_state_flag);
                view_scriptbox->dlg_preferences.check_rx_buddy.val_set(check_rx_buddy_flag);
                view_scriptbox->dlg_preferences.check_buddy_changed_name.val_set(check_buddy_changed_name_flag);
                view_scriptbox->dlg_preferences.check_buddy_changed_state.val_set(check_buddy_changed_state_flag);
                view_scriptbox->dlg_preferences.check_buddy_changed_user_state.val_set(check_buddy_changed_user_state_flag);
                view_scriptbox->dlg_preferences.check_buddy_changed_status.val_set(check_buddy_changed_status_flag);
                view_scriptbox->dlg_preferences.check_buddy_rx_req.val_set(check_buddy_rx_req_flag);
                view_scriptbox->dlg_preferences.check_typing_buddy.val_set(check_typing_buddy_flag);
                view_scriptbox->dlg_preferences.check_file_rx_req.val_set(check_file_rx_req_flag);
                view_scriptbox->dlg_preferences.check_file_rx_chunk.val_set(check_file_rx_chunk_flag);
                view_scriptbox->dlg_preferences.check_file_tx_chunk.val_set(check_file_tx_chunk_flag);
                view_scriptbox->dlg_preferences.check_conference_rx_message.val_set(check_conference_rx_message_flag);
                view_scriptbox->dlg_preferences.check_joins_member.val_set(check_joins_member_flag);
                view_scriptbox->dlg_preferences.check_leaves_member.val_set(check_leaves_member_flag);
                view_scriptbox->dlg_preferences.check_member_changed_name.val_set(check_member_changed_name_flag);
                view_scriptbox->dlg_preferences.check_changed_title.val_set(check_changed_title_flag);
                view_scriptbox->dlg_preferences.check_invited_conference.val_set(check_invited_conference_flag);
                view_scriptbox->dlg_preferences.check_connected_conference.val_set(check_connected_conference_flag);
                view_scriptbox->dlg_preferences.check_changed_conference_members.val_set(check_changed_conference_members_flag);

                //  - tab 1 input text controls,
                string cmd_self_changed_state         = model->self.cfg.get_string(name, "cmd_self_changed_state");
                string cmd_rx_buddy                   = model->self.cfg.get_string(name, "cmd_rx_buddy");
                string cmd_buddy_changed_name         = model->self.cfg.get_string(name, "cmd_buddy_changed_name");
                string cmd_buddy_changed_state        = model->self.cfg.get_string(name, "cmd_buddy_changed_state");
                string cmd_buddy_changed_user_state   = model->self.cfg.get_string(name, "cmd_buddy_changed_user_state");
                string cmd_buddy_changed_status       = model->self.cfg.get_string(name, "cmd_buddy_changed_status");
                string cmd_buddy_rx_req               = model->self.cfg.get_string(name, "cmd_buddy_rx_req");
                string cmd_typing_buddy               = model->self.cfg.get_string(name, "cmd_typing_buddy");
                string cmd_file_rx_req                = model->self.cfg.get_string(name, "cmd_file_rx_req");
                string cmd_file_rx_chunk              = model->self.cfg.get_string(name, "cmd_file_rx_chunk");
                string cmd_file_tx_chunk              = model->self.cfg.get_string(name, "cmd_file_tx_chunk");
                string cmd_conference_rx_message      = model->self.cfg.get_string(name, "cmd_conference_rx_message");
                string cmd_joins_member               = model->self.cfg.get_string(name, "cmd_joins_member");
                string cmd_leaves_member              = model->self.cfg.get_string(name, "cmd_leaves_member");
                string cmd_member_changed_name        = model->self.cfg.get_string(name, "cmd_member_changed_name");
                string cmd_changed_title              = model->self.cfg.get_string(name, "cmd_changed_title");
                string cmd_invited_conference         = model->self.cfg.get_string(name, "cmd_invited_conference");
                string cmd_connected_conference       = model->self.cfg.get_string(name, "cmd_connected_conference");
                string cmd_changed_conference_members = model->self.cfg.get_string(name, "cmd_changed_conference_members");

                view_scriptbox->dlg_preferences.txt_self_changed_state.val_set(cmd_self_changed_state);
                view_scriptbox->dlg_preferences.txt_rx_buddy.val_set(cmd_rx_buddy);
                view_scriptbox->dlg_preferences.txt_buddy_changed_name.val_set(cmd_buddy_changed_name);
                view_scriptbox->dlg_preferences.txt_buddy_changed_state.val_set(cmd_buddy_changed_state);
                view_scriptbox->dlg_preferences.txt_buddy_changed_user_state.val_set(cmd_buddy_changed_user_state);
                view_scriptbox->dlg_preferences.txt_buddy_changed_status.val_set(cmd_buddy_changed_status);
                view_scriptbox->dlg_preferences.txt_buddy_rx_req.val_set(cmd_buddy_rx_req);
                view_scriptbox->dlg_preferences.txt_typing_buddy.val_set(cmd_typing_buddy);
                view_scriptbox->dlg_preferences.txt_file_rx_req.val_set(cmd_file_rx_req);
                view_scriptbox->dlg_preferences.txt_file_rx_chunk.val_set(cmd_file_rx_chunk);
                view_scriptbox->dlg_preferences.txt_file_tx_chunk.val_set(cmd_file_tx_chunk);
                view_scriptbox->dlg_preferences.txt_conference_rx_message.val_set(cmd_conference_rx_message);
                view_scriptbox->dlg_preferences.txt_joins_member.val_set(cmd_joins_member);
                view_scriptbox->dlg_preferences.txt_leaves_member.val_set(cmd_leaves_member);
                view_scriptbox->dlg_preferences.txt_member_changed_name.val_set(cmd_member_changed_name);
                view_scriptbox->dlg_preferences.txt_changed_title.val_set(cmd_changed_title);
                view_scriptbox->dlg_preferences.txt_invited_conference.val_set(cmd_invited_conference);
                view_scriptbox->dlg_preferences.txt_connected_conference.val_set(cmd_connected_conference);
                view_scriptbox->dlg_preferences.txt_changed_conference_members.val_set(cmd_changed_conference_members);

                //  - tab 2 check boxes,
                bool check_called_by_peer_flag  = model->self.cfg.get_boolean(name, "check_called_by_peer");
                bool check_suggest_bitrate_flag = model->self.cfg.get_boolean(name, "check_suggest_bitrate");
                bool check_rx_audio_flag        = model->self.cfg.get_boolean(name, "check_rx_audio");
                bool check_changed_state_flag   = model->self.cfg.get_boolean(name, "check_changed_state");

                view_scriptbox->dlg_preferences.check_called_by_peer.val_set(check_called_by_peer_flag);
                view_scriptbox->dlg_preferences.check_suggest_bitrate.val_set(check_suggest_bitrate_flag);
                view_scriptbox->dlg_preferences.check_rx_audio.val_set(check_rx_audio_flag);
                view_scriptbox->dlg_preferences.check_changed_state.val_set(check_changed_state_flag);

                //  - tab 2 input text controls
                string cmd_called_by_peer  = model->self.cfg.get_string(name, "cmd_called_by_peer");
                string cmd_suggest_bitrate = model->self.cfg.get_string(name, "cmd_suggest_bitrate");
                string cmd_rx_audio        = model->self.cfg.get_string(name, "cmd_rx_audio");
                string cmd_changed_state   = model->self.cfg.get_string(name, "cmd_changed_state");

                view_scriptbox->dlg_preferences.txt_called_by_peer.val_set(cmd_called_by_peer);
                view_scriptbox->dlg_preferences.txt_suggest_bitrate.val_set(cmd_suggest_bitrate);
                view_scriptbox->dlg_preferences.txt_rx_audio.val_set(cmd_rx_audio);
                view_scriptbox->dlg_preferences.txt_changed_state.val_set(cmd_changed_state);
            }
            catch (GLib.KeyFileError ex)
            {
                GLib.assert(false);
            }
        }

        /*----------------------------------------------------------------------------*/

        /**
         * "Apply" button click callback
         *
         * @param fn    Not used currently
         * @param param Not used currently
         * @param event Not used currently
         *
         * @return null
         */
        public static void *btn_apply_settings_clicked(void *fn, void *param, void *event)
        {
            // This method is called with zero event when program wants
            // to terminate.
            // No actions in this case.
            if (event == null)
                return null;

            // Lock mutex
            model->mutex.lock();

            // Update signal connections.
            // It also updates config using current GUI control values.
            ctrl_scriptbox->tox_core.signals_upd();
            ctrl_scriptbox->tox_core_av.signals_upd();

            // Update profile-dependent plugin settings using current GUI
            // control values
            ctrl_scriptbox->dlg_preferences.cfg_upd();

            // Unlock mutex
            model->mutex.unlock();

            return null;
        }
    }
}

