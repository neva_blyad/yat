#!/usr/bin/perl
#
# vim: ts=4:sw=4:sts=4:et

=pod

=encoding UTF-8

=head1 NAME

audio_status.pm

=head1 DESCRIPTION

Sets the currently playing music track from your audio player as yat IM status

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# Use experimental switch-case construct
use v5.14;
use feature qw(switch);
no warnings qw(experimental);

# Declare package.
# The class in this package describes ModelPlugin interface (Model.Plugin in
# Vala naming).
package audio_status::ModelPlugin;

# CPAN modules
use Cwd;
use Data::Dumper;
use DateTime;
use DateTime::Format::ISO8601;
use Encode;
use File::Basename;
use File::Find;
use File::Spec;
use File::Spec::Functions;
use Glib;
use Glib qw/TRUE FALSE/; 
use Glib::Object::Introspection;
use Glib::Object::Subclass;
use Locale::gettext;
use Net::DBus;
use Net::DBus::GLib;
use Regexp::Common;
use Term::ANSIColor;
use Tie::IxHash;
use Try::Tiny;
use URI::Escape;
use Wx;
use Wx::XRC;

# Path to binary
our $path;
our $prefix;
our $use_prefix;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    my $pattern;

    $path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; just remember it
    $path = File::Basename::dirname($path);

    $pattern    = File::Spec::Functions::catfile('share', 'yat', 'audio_status');
    $prefix     = $path;
    $use_prefix = 1 if $prefix =~ s/\Q$pattern\E\z//i;
}

# Module path
use lib $path;

# Setup GIR instrospection
BEGIN
{
    # Directories to find for typelibs
    tie our %dirs_sys, 'Tie::IxHash';
    tie our %dirs_cwd, 'Tie::IxHash';

    my $dir = `pkgconf --variable=typelibdir gobject-introspection-1.0`;
    chomp $dir;

    @dirs_sys{glob("$prefix/lib/girepository-*.*/"),
              "$prefix/lib/x86_64-linux-gnu/girepository-1.0",
              $dir} = 1;

    %dirs_cwd = %dirs_sys if $use_prefix;
    @dirs_cwd{'./'} = 1;
    my @dirs_env = exists $ENV{GI_TYPELIB_PATH} ? split ':', $ENV{GI_TYPELIB_PATH} : ();
    @dirs_sys{@dirs_env} = @dirs_cwd{@dirs_env} = 1;

    # Search and enjoy.
    #
    # We could do Glib::Object::Introspection->setup() only instead
    # of all of the shitty loops below, but we don't know the
    # library versions. We should find them manually.
    sub wanted
    {
        my $lib  = shift;
        my $dirs = shift;

OUTER:
        foreach my $dir (reverse keys %$dirs)
        {
            my @filepaths = glob "$dir/$lib-*.*.typelib";

INNER:
            foreach my $filepath (@filepaths)
            {
                $_ = File::Basename::basename($filepath);
                next unless /^($lib)-(\d+\.\d+)\.typelib$/;

                my $name = $1;
                my $ver  = $2;

                Glib::Object::Introspection->setup(
                    basename    => $name,
                    version     => $ver,
                    package     => $lib,
                    search_path => $dir,
                );

                last OUTER;
            }
        }
    }

    # libyat,
    # GLib
    wanted('yat',  \%dirs_cwd);
    wanted('GLib', \%dirs_sys);
}

# Setup inheritance
use Glib::Object::Subclass
    'Glib::Object', # Base class
    interfaces => [ qw(yat::ModelPlugin) ],
    properties => [
        Glib::ParamSpec->object(
            'model_',
            'model', # Alias
            'This is the Model object of the main program',
            'yat::ModelModel',
            [ qw/readable writable/ ]
        ),
        Glib::ParamSpec->object(
            'view_',
            'view', # Alias
            'This is the View object of the main program',
            'yat::ViewView',
            [ qw/readable writable/ ]
        ),
        Glib::ParamSpec->object(
            'ctrl_',
            'ctrl', # Alias
            'This is the Ctrl object of the main program',
            'yat::CtrlCtrl',
            [ qw/readable writable/ ]
        ),
    ];

# Constants:
#
#  - main,
use constant PLUGIN       => 'audio_status';             # Plugin name
use constant PREFIX       => 'audio_status:';            # Console output prefix
use constant COLOUR       => 'grey15';                   # Prefix colour
use constant LOG_DOMAIN   => 'yat-plugins';              # GLib log domain
use constant I18N_DOMAIN  => 'yat-plugins-audio_status'; # Translation domain

#  - list MPRIS-compliant players on start (method call),
use constant SVC_LST      => 'org.freedesktop.DBus';
use constant OBJ_LST      => '/org/freedesktop/DBus';
use constant IFACE_LST    => 'org.freedesktop.DBus';

#  - receive new tracks from such player (signal),
use constant SVC_PLAYER   => 'org.mpris.MediaPlayer2.'; # We'll automatically detect it
use constant OBJ_PLAYER   => '/org/mpris/MediaPlayer2';
use constant IFACE_PLAYER => 'org.freedesktop.DBus.Properties';
use constant SIG_PLAYER   => 'PropertiesChanged';

#  - monitor MPRIS-compliant players all the time (signal),
use constant SVC_MON      => 'org.freedesktop.DBus';
use constant OBJ_MON      => '/org/freedesktop/DBus';
use constant IFACE_MON    => 'org.freedesktop.DBus';
use constant SIG_MON      => 'NameOwnerChanged';

#  - set 1 if warnings should be fatal,
#  - set 1 if debugging,
use constant FATAL_WRN => 0;
use constant VERBOSE   => 0;

#  - string to be substituted if there is no tag/filepath
#    available, set undef to leave pattern as it is, e. g.
#    <Tracknumber>, empty string literal '' is an other good
#    choice,
use constant FILL_EMPTY_TAGS => '';
use constant FILL_EMPTY_FILE => '';

#  - <Date> default format,
use constant DATE_DEF_FMT    => '%Y-%m-%d';

#  - configuration key,
#    opt key in [audio_status] group,
use constant
{
    OPT_TAG  => 0,
    OPT_FILE => 1,
};

#  - configuration key,
#    pattern_file in [audio_status] group,
use constant
{
    PATTERN_FILE_LVL1 => 0,
    PATTERN_FILE_LVL2 => 1,
    PATTERN_FILE_LVL3 => 2,
    PATTERN_FILE_LVL4 => 3,
};

# Setup buffering for standard file descriptors
STDERR->autoflush(1);
STDOUT->autoflush(1);

# Setup console output
BEGIN
{
    # i18n & l10n
    sub _T
    {
        my $msg      = $_[0];
        my $i18n_msg = Locale::gettext::dgettext(I18N_DOMAIN, $msg);

        Encode::_utf8_on($i18n_msg);
        return $i18n_msg;
    }

    # GLib logger
    my $prefix =
        Term::ANSIColor::color(COLOUR) .
        PREFIX .
        Term::ANSIColor::color('reset') . ' ';

    sub dbg { Glib->debug  (LOG_DOMAIN, $prefix . $_[0]); };
    sub wrn { Glib->warning(LOG_DOMAIN, $prefix . $_[0]); };
}

# Singletons
our $model;
our $view;
our $ctrl;

# Plugin predefined settings
our $fatal_wrn       = FATAL_WRN;       # Warnings setting
our $verbose         = VERBOSE;         # Option parsing
our $fill_empty_tags = FILL_EMPTY_TAGS; # String to be substituted if there is no tag available

# Set __WARN__ handler.
#
# Do all warnings to be fatal if set.
$SIG{__WARN__} = sub { die @_; } if $fatal_wrn;

# DBus and its services and objects listed there
our $bus;

our $svc_lst;
our $obj_lst;

our $svc_mon;
our $obj_mon;

our %svc_players;
our %obj_players;

# GObject signal hook IDs
our $id_idle_handle_sig;
our $id_btn_apply_settings_sig;
our $id_menu_reset_last_clicked_sig;
our $id_menu_reset_def_clicked_sig;
our $id_menu_settings_clicked_sig;

# Audio status
our $last_status;

# Configuration keys stored in INI-like file.
# We use the same config file as yat.
our $cfg;

our $opt;
our $pattern_tag;
our $pattern_tag_i18n;
our $pattern_file;

# Tags and their translations
our %tags =
(
    Artist      => _T('Artist'),
    Album       => _T('Album'),
    Title       => _T('Title'),
    Tracknumber => _T('Tracknumber'),
    Date        => _T('Date'),
    Genre       => _T('Genre'),
    BPM         => _T('BPM'),
);

# GUI
our $page;

our $radio_audio_status_tag;
our $radio_audio_status_file;
our $combo_audio_status_tag;
our $choice_audio_status_file;

# Subscribe on signals from the specified MPRIS player
sub player_add
{
    my $svc = shift;

    # MPRIS player was added
    my $svc_player = $bus->get_service($svc);
    my $obj_player = $svc_player->get_object(OBJ_PLAYER, IFACE_PLAYER);

    # Show DBus parameters
    dbg _T('New MPRIS player has been revealed');
    dbg _T('Service: ')   . $svc;
    dbg _T('Object: ')    . OBJ_PLAYER;
    dbg _T('Interface: ') . IFACE_PLAYER;

    # Listen the player's signal and connect our handler to it
    my $sig_player = $obj_player->connect_to_signal(SIG_PLAYER, \&prop_changed);
    dbg _T('Signal: ') . SIG_PLAYER;
    dbg '';

    # Remember this player for future
    $svc_players{$svc} = [ $svc_player, $obj_player, $sig_player ];
}

# Cancel subscription on signals from the specified MPRIS player
sub player_remove
{
    my $svc = shift;

    # MPRIS player was removed
    my $svc_player = $svc_players{$svc}[0];
    my $obj_player = $svc_players{$svc}[1];
    my $sig_player = $svc_players{$svc}[2];

    # Show DBus parameters
    dbg _T('Old MPRIS player has been closed');
    dbg _T('Service: ')   . $svc;
    dbg _T('Object: ')    . OBJ_PLAYER;
    dbg _T('Interface: ') . IFACE_PLAYER;

    # Disconnect handler from the player's signal
    $obj_player->disconnect_from_signal(SIG_PLAYER, $sig_player);
    dbg _T('Signal: ') . SIG_PLAYER;
    dbg '';

    # Do not remember this player anymore
    delete $svc_players{$svc};
}

# Plugin entry point
sub PLUGIN_LOAD
{
    my $self = shift;

    # Singletons
    $model = $self->{'model_'};
    $view  = $self->{'view_'};
    $ctrl  = $self->{'ctrl_'};

    dbg _T('Start');

    # Connect to DBus session
    $bus = Net::DBus::GLib->session;
    dbg _T('DBus connection to Session bus is done');

    # We are interested in object that lists MPRIS compliant audio
    # players.
    # Such player should suggest us tracks it plays.
    $svc_lst = $bus->get_service(SVC_LST);
    $obj_lst = $svc_lst->get_object(OBJ_LST, IFACE_LST);

    # Show DBus parameters
    dbg _T('List available MPRIS players');
    dbg _T('Service: ')   . SVC_LST;
    dbg _T('Object: ')    . OBJ_LST;
    dbg _T('Interface: ') . IFACE_LST;
    dbg _T('Method: ListNames()');
    dbg '';

    # Poll DBus bus to get list with all of the available MPRIS
    # players in system
    try
    {
        my @names   = @{$obj_lst->ListNames()};
        my $pattern = SVC_PLAYER;
        my @players = grep {/^$pattern/} @names;

        foreach my $player (@players)
        {
            player_add($player);
        }
    }
    catch
    {
        # Something go wrong.
        # Let's the user know why.
        die "[0]: $_";
    };

    # Also we'll monitor MPRIS players
    $svc_mon = $bus->get_service(SVC_MON);
    $obj_mon = $svc_mon->get_object(OBJ_MON, IFACE_MON);

    # Show DBus parameters
    dbg _T('Wait events about added/removed MPRIS players');
    dbg _T('Service: ')   . SVC_MON;
    dbg _T('Object: ')    . OBJ_MON;
    dbg _T('Interface: ') . IFACE_MON;

    # Prepare status according to the tag-based pattern.
    # The pattern is set in config.
    sub tag_status_cook
    {
        my %meta =
        (
            Artist      => undef,
            Album       => undef,
            Title       => undef,
            Tracknumber => undef,
            Date        => undef,
            Genre       => undef,
            BPM         => undef,

            %{$_[0]}, # Override named parameters listed above if any
        );

        my $status = $pattern_tag_i18n;

        foreach my $tag (keys %tags)
        {
            my $tag_i18n = $tags{$tag};
            my $meta     = $meta{$tag};

            if ($tag eq 'Date')
            {
                my $date;

                if (defined $meta)
                {
                    try   { $date = DateTime::Format::ISO8601->parse_datetime($meta); }
                    catch {};
                }

                if (defined $date)
                {
                    $status =~ s/\Q<$tag_i18n\E:(.+?)>/$date->strftime($1)/eg;
                    $status =~ s/\Q<$tag_i18n\E>/$date->strftime(DATE_DEF_FMT)/eg;
                }
                elsif (defined $fill_empty_tags)
                {
                    $status =~ s/\Q<$tag_i18n\E(?::.+?)?>/$fill_empty_tags/g;
                }
            }
            else
            {
                my $repl = $meta // $fill_empty_tags;
                next unless defined $repl;

                $status =~ s/\Q<$tag_i18n\E>/$repl/g;
            }
        }

        return $status;
    }

    # Prepare status according to the file-based pattern.
    # The pattern is set in config.
    sub file_status_cook
    {
        my %meta =
        (
            URL => undef,

            %{$_[0]}, # Override named parameters listed above if any
        );

        return FILL_EMPTY_FILE unless $meta{URL};

        my $status =  $meta{URL};
           $status =~ s|^file://||;
           $status =  URI::Escape::uri_unescape($status);
           $status =  Encode::decode('UTF-8', $status);

        my @fs_parts = File::Spec->splitdir($status);

        my $idx_start = $#fs_parts > $pattern_file ? $#fs_parts - $pattern_file : 0;
        my $idx_end   = $#fs_parts;

        $status = File::Spec->catdir(@fs_parts[$idx_start..$idx_end]);

        return $status;
    }

    # Set status
    sub status_set
    {
        my $status = shift;

        # Properties
        my $ctrl_frm_main = $ctrl->get('frm_main');
        my $view_frm_main = $view->get('frm_main');
        my $self          = $model->get('self');
        my $lbl_status    = $view_frm_main->get('lbl_status');
        my $tox           = $model->get('tox');

        # We are currently in GLib thread, because GLib event loop
        # processes DBus signals.
        # All GUI methods should be called in UI thread,
        # so we don't call them directly here, but ask to do it
        # the idle handler.
        defined $id_idle_handle_sig &&
            $ctrl_frm_main->remove_emission_hook('idle_handle_sig', $id_idle_handle_sig);

        $id_idle_handle_sig = $ctrl_frm_main->signal_add_emission_hook('idle_handle_sig' => sub
        {
            # Lock mutex
            $model->mutex_lock();

            # Update label widget with the new status.
            # Disable/enable apply button.
            # Set status to Tox profile.
            $self->set('status', $status);
            $lbl_status->lbl_set($status);
            $ctrl_frm_main->btn_apply_status_en_set();

            Encode::_utf8_off($status); # To do length operator more nice
            c_status_set($tox, $status, length $status);
            Encode::_utf8_on($status);

            # No more calls.
            # Destroy hook.
            $id_idle_handle_sig = undef;

            # Unlock mutex
            $model->mutex_unlock();

            return FALSE;
        });
    }

    # Handle the signal that will be emitted each time thack is
    # changed
    sub prop_changed
    {
        # Pick up the message emitted by the signal
        my $iface = shift;
        my $prop  = shift;
        my $other = shift;

        # Print the messages to the terminal showing what we have
        # received from the signal
        if ($verbose)
        {
            dbg _T('Received message from ') . SIG_PLAYER . _T(' signal:') . $iface;
            dbg Data::Dumper::Dumper($prop);
            dbg Data::Dumper::Dumper($other);
        }

        # Try to extract track tags from the message
        try
        {
            # Check the interface parameter
            #die unless $iface eq IFACE;

            # Do we really need the checks below?
            die unless exists $prop->{'PlaybackStatus'} &&
                              $prop->{'PlaybackStatus'} eq 'Playing';
            die unless exists   $prop->{'Metadata'}           &&
                       ref      $prop->{'Metadata'} eq 'HASH' &&
                              %{$prop->{'Metadata'}} > 0;

            # Which audio status option is chosen in config?
            my $status;

            given ($opt)
            {
                # Tag-based status pattern
                when ($_ == OPT_TAG)
                {
                    # Do we really need the checks below?
                    #die unless ref      $prop->{'Metadata'}{'xesam:artist'} eq 'ARRAY' ||
                    #                        $prop->{'Metadata'}{'xesam:albumArtist'} eq 'ARRAY';
                    #die unless scalar @{$prop->{'Metadata'}{'xesam:artist'}} > 0 ||
                    #                      @{$prop->{'Metadata'}{'xesam:albumArtist'}} > 0;
                    #die unless exists   $prop->{'Metadata'}{'xesam:album'};
                    #die unless exists   $prop->{'Metadata'}{'xesam:title'};
                    #die unless exists   $prop->{'Metadata'}{'xesam:trackNumber'};
                    #die unless exists   $prop->{'Metadata'}{'xesam:contentCreated'} ||
                    #                        $prop->{'Metadata'}{'xesam:firstUsed'} ||
                    #                        $prop->{'Metadata'}{'xesam:lastUsed'}
                    #die unless ref      $prop->{'Metadata'}{'xesam:genre'} eq 'ARRAY';
                    #die unless scalar @{$prop->{'Metadata'}{'xesam:genre'}} > 0;
                    #die unless exists   $prop->{'Metadata'}{'xesam:bpm'};

                    # Get MPRIS metadata fields
                    my $artist      = $prop->{'Metadata'}{'xesam:artist'}[0] ||
                                          $prop->{'Metadata'}{'xesam:albumArtist'}[0];
                    my $album       = $prop->{'Metadata'}{'xesam:album'};
                    my $title       = $prop->{'Metadata'}{'xesam:title'};
                    my $tracknumber = $prop->{'Metadata'}{'xesam:trackNumber'};
                    my $date        = $prop->{'Metadata'}{'xesam:contentCreated'} ||
                                          $prop->{'Metadata'}{'xesam:firstUsed'}  ||
                                          $prop->{'Metadata'}{'xesam:lastUsed'};
                    my $genre       = $prop->{'Metadata'}{'xesam:genre'}[0];
                    my $bpm         = $prop->{'Metadata'}{'xesam:bpm'};

                    # Support non-ASCII characters
                    Encode::_utf8_on($artist);
                    Encode::_utf8_on($album);
                    Encode::_utf8_on($title);
                    #Encode::_utf8_on($tracknumber);
                    #Encode::_utf8_on($date);
                    Encode::_utf8_on($genre);
                    #Encode::_utf8_on($bpm);

                    # Now we have to cook a new tag-based status for yat
                    my %meta = ();

                    $meta{Artist     } = $artist      if $artist;
                    $meta{Album      } = $album       if $album;
                    $meta{Title      } = $title       if $title;
                    $meta{Tracknumber} = $tracknumber if defined $tracknumber && $tracknumber =~ /^$RE{num}{int}\z/;
                    $meta{Date       } = $date        if $date;
                    $meta{Genre      } = $genre       if $genre;
                    $meta{BPM        } = $bpm         if defined $bpm && $bpm =~ /^$RE{num}{int}\z/;

                    $status = tag_status_cook(\%meta);
                }

                # File-based status pattern
                when ($_ == OPT_FILE)
                {
                    # Do we really need the checks below?
                    #die unless exists $prop->{'Metadata'}{'xesam:url'};

                    # Get MPRIS metadata fields
                    my $url = $prop->{'Metadata'}{'xesam:url'};

                    # Support non-ASCII characters
                    Encode::_utf8_on($url);

                    # Now we have to cook a new file-based status for yat
                    my %meta = ();
                    $meta{URL} = $url if $url;
                    $status = file_status_cook(\%meta);
                }

                # Error
                default
                {
                    wrn _("Error. Can't go here.");
                    $status = '';
                }
            }

            !defined $last_status || $status ne $last_status
                or die _T('Status has not been changed, ignoring');
            $last_status = $status;

            # Status is ready.
            # Send it to buddies, set it to GUI.
            dbg _T('New status: ') . $status;
            status_set($status);
        }
        catch
        {
            # Debug
            wrn "[1]: $_" if $verbose;
        };
    }

    # Handle the signal that will be emitted when MPRIS player is
    # added/removed
    sub name_owner_changed
    {
        # Pick up the message emitted by the signal
        my $svc = shift;
        my $old = shift;
        my $new = shift;

        # Print the messages to the terminal showing what we have
        # received from the signal
        if ($verbose)
        {
            dbg _T('Received message from ') . SIG_MON . _T(' signal:') . $svc;
            dbg Data::Dumper::Dumper($old);
            dbg Data::Dumper::Dumper($new);
        }

        # Only added/removed MPRIS objects are interested
        try
        {
            # Check service name
            if ((index $svc, SVC_PLAYER) == 0)
            {
                # Something new about available MPRIS players
                if ($old eq '' &&
                    $new ne '')
                {
                    # MPRIS player was added
                    player_add($svc);
                }
                elsif ($old ne '' &&
                       $new eq '')
                {
                    # MPRIS player was removed
                    player_remove($svc);
                }
            }
        }
        catch
        {
            # Debug
            wrn "[2]: $_" if $verbose;
        };
    }

    # Listen this signal and connect our handler to it
    $obj_mon->connect_to_signal(SIG_MON, \&name_owner_changed);
    dbg _T('Signal: ') . SIG_MON;
    dbg '';

    # Initialize per-profile configuration stored in INI-like file.
    #
    # Loads keys.
    # Sets default if necessary.
    sub cfg_init
    {
        # Properties
        my $profile = $model->get('self');
           $cfg     = $profile->get('cfg');

        # Type of pattern to use
        try
        {
            $opt = $cfg->get_integer(PLUGIN, 'opt');
        }
        catch
        {
            $cfg->set_integer(PLUGIN, 'opt', OPT_TAG);
            $opt = OPT_TAG;
        };

        # Tag-based pattern
        try
        {
            $pattern_tag = $cfg->get_string(PLUGIN, 'pattern_tag');
        }
        catch
        {
            $cfg->set_string(PLUGIN, 'pattern_tag', '<Title>');
            $pattern_tag = '<Title>';
        };

        # File-based pattern
        try
        {
            $pattern_file = $cfg->get_integer(PLUGIN, 'pattern_file');
        }
        catch
        {
            $cfg->set_integer(PLUGIN, 'pattern_file', PATTERN_FILE_LVL1);
            $pattern_file = PATTERN_FILE_LVL1;
        };
    }

    # Initialize configuration keys
    cfg_init();

    # Setup GUI.
    #
    # There are two ways to use wxWidgets and control GUI from Perl
    # plugins:
    #
    # 1. wxPerl and CPAN module named Wx.
    # 2. wxVala/wxC methods available using GIR introspection and
    #    libyat.
    #
    # The both are demonstrated below.
    sub gui_setup
    {
        # Properties
        my $frm_main     = $ctrl->get('frm_main');
        my $dlg_settings = $view->get('dlg_settings');
        my $notebook     = $dlg_settings->get('notebook');

        # Prepare path to the XRC file
        my $xrc      = Wx::XmlResource->new();
        my $filepath = File::Spec::Functions::catfile(($use_prefix ? './' : 'plugins'), PLUGIN, PLUGIN . '.xrc');

        # Load the XRC
        $xrc->InitAllHandlers();
        $xrc->Load($filepath);

        # Wrap C widget instances into Perl objects
        my $notebook_ptr = $notebook->get('win');
        my $notebook_ref = bless do { \( my $o = $notebook_ptr ) }, 'Wx::Notebook';

        # Now plugin widgets. They are already Perl.
        my $dlg_settings_fake = $xrc->LoadDialog(undef, 'dlg_settings_audio_status');

        my $panel         = $dlg_settings_fake->FindWindow('panel_audio_status');
        my $notebook_fake = $dlg_settings_fake->FindWindow('notebook');
        my $lbl_pattern   = $dlg_settings_fake->FindWindow('lbl_audio_status_pattern');

        $radio_audio_status_tag   = $dlg_settings_fake->FindWindow('radio_audio_status_tag');
        $radio_audio_status_file  = $dlg_settings_fake->FindWindow('radio_audio_status_file');
        $combo_audio_status_tag   = $dlg_settings_fake->FindWindow('combo_audio_status_tag');
        $choice_audio_status_file = $dlg_settings_fake->FindWindow('choice_audio_status_file');

        # We will use Perl regex to substitute tags in pattern.
        # It produces a new pattern with translated tags.
        $pattern_tag_i18n = $pattern_tag;

        foreach my $tag (keys %tags)
        {
            my $tag_i18n = $tags{$tag};
            $pattern_tag_i18n =~ s/\Q<$tag\E>/<$tag_i18n>/g;
        }

        # Setup widget initial states according to config.
        #
        # Note to the related wxStaticText code below.
        # wxPerl doesn't implement SetLabelMarkup() from wxWidgets
        # library, but wxVala/wxC does. So we control our widget using
        # yat's method, not from Wx.
        my $lbl_pattern_ = yat::ViewWrapperStaticTxt->new();
        $lbl_pattern_->set('win', $$lbl_pattern);
        $lbl_pattern_->lbl_markup_set(_T("<b>Status Pattern</b>"));

        if ($opt)
        {
            $radio_audio_status_file->SetValue(1);
            #$combo_audio_status_tag->Enable(0);
        }

        $combo_audio_status_tag->SetStringSelection($pattern_tag_i18n) or
            $combo_audio_status_tag->SetValue($pattern_tag_i18n);
        $choice_audio_status_file->SetSelection($pattern_file);

        # Add new tab "Audio Status" to dialog "Settings"
        $notebook_fake->RemovePage(0);
        $dlg_settings_fake->Destroy();

        $panel->Reparent($notebook_ref);
        $page = $notebook_ref->GetPageCount();
        $notebook_ref->AddPage($panel,
                               _T('Audio Status'),
                               0, # Do not select
                               $page);
    }

    # Now GUI
    try
    {
        # Add "Audio Status" page to "Settings" dialog
        gui_setup();
    }
    catch
    {
        # Debug
        wrn "[4]: $_";
    };

    # Configure "Apply" button click callback
    sub btn_apply_settings_clicked_cfg
    {
        # Properties
        my $dlg_settings = $ctrl->get('dlg_settings');

        # Subscribe to GObject signal related to "Apply" button clicking.
        # The hook below would be called on this action.
        my $hook = sub
        {
            my $info  = shift;
            my $param = shift;

            # Signal parameters
            my $self   = $param->[0];
            my $fn     = $param->[1];
            my $param_ = $param->[2];
            my $event  = $param->[3];

            # Lock mutex
            $model->mutex_lock();

            # This method is called with zero event when program wants
            # to terminate.
            # No actions in this case.
            unless ($event)
            {
                # Unlock mutex
                #$model->mutex_unlock();
                #return TRUE;
            }

            # Get values from GUI controls
            $opt              = $radio_audio_status_file->GetValue();
            $pattern_tag_i18n = $combo_audio_status_tag->GetValue();
            $pattern_file     = $choice_audio_status_file->GetSelection();

            # Translate to English
            $pattern_tag = $pattern_tag_i18n;

            foreach my $tag (keys %tags)
            {
                my $tag_i18n = $tags{$tag};
                $pattern_tag =~ s/\Q<$tag_i18n\E>/<$tag>/g;
            }

            # Update per-profile config with values from GUI
            $cfg->set_integer(PLUGIN, 'opt',          $opt);
            $cfg->set_string (PLUGIN, 'pattern_tag',  $pattern_tag);
            $cfg->set_integer(PLUGIN, 'pattern_file', $pattern_file);

            # Unlock mutex
            $model->mutex_unlock();

            return TRUE;
        };

        $id_btn_apply_settings_sig =
            $dlg_settings->signal_add_emission_hook('btn_apply_settings_clicked_sig' => $hook);

        return $hook;
    }

    # Configure "Reset to last applied settings" menu item click
    # callback
    sub menu_reset_last_settings_clicked_cfg
    {
        # Properties
        my $dlg_settings = $ctrl->get('dlg_settings');

        # Subscribe to GObject signal related to "Reset to last applied
        # settings" menu item click.
        # The hook below would be called on this action.
        my $hook = sub
        {
            my $info  = shift;
            my $param = shift;

            # Signal parameters
            my $self   = $param->[0];
            my $fn     = $param->[1];
            my $param_ = $param->[2];
            my $event  = $param->[3];

            # Lock mutex
            $model->mutex_lock();

            # This method is called with zero event when program wants
            # to terminate.
            # No actions in this case.
            unless ($event)
            {
                # Unlock mutex
                #$model->mutex_unlock();
                #return TRUE;
            }

            # Reset all widgets to last saved
            if ($opt == OPT_TAG) { $radio_audio_status_tag->SetValue(1);  }
            else                 { $radio_audio_status_file->SetValue(1); }
            $combo_audio_status_tag->SetStringSelection($pattern_tag_i18n) or
                $combo_audio_status_tag->SetValue($pattern_tag_i18n);
            $choice_audio_status_file->SetSelection($pattern_file);

            # Unlock mutex
            $model->mutex_unlock();

            return TRUE;
        };

        $id_menu_reset_last_clicked_sig =
            $dlg_settings->signal_add_emission_hook('menu_reset_last_clicked_sig' => $hook);

        return $hook;
    }

    # Configure "Reset to program default settings" menu item click
    # callback
    sub menu_reset_def_settings_clicked_cfg
    {
        # Properties
        my $dlg_settings = $ctrl->get('dlg_settings');

        # Subscribe to GObject signal related to "Reset to program
        # default settings" menu item click.
        # The hook below would be called on this action.
        my $hook = sub
        {
            my $info  = shift;
            my $param = shift;

            # Signal parameters
            my $self   = $param->[0];
            my $fn     = $param->[1];
            my $param_ = $param->[2];
            my $event  = $param->[3];

            # Lock mutex
            $model->mutex_lock();

            # This method is called with zero event when program wants
            # to terminate.
            # No actions in this case.
            unless ($event)
            {
                # Unlock mutex
                #$model->mutex_unlock();
                #return TRUE;
            }

            # Reset all widgets to initial state
            my $pattern = '<' . _T('Title') . '>';

            $radio_audio_status_tag->SetValue(1);
            $combo_audio_status_tag->SetStringSelection($pattern) or
                $combo_audio_status_tag->SetValue($pattern);
            $choice_audio_status_file->SetSelection(PATTERN_FILE_LVL1);

            # Unlock mutex
            $model->mutex_unlock();

            return TRUE;
        };

        $id_menu_reset_def_clicked_sig =
            $dlg_settings->signal_add_emission_hook('menu_reset_def_clicked_sig' => $hook);

        return $hook;
    }

    # Configure "Tools" -> "Settings" menu item click callback
    sub menu_settings_clicked_cfg
    {
        # Properties
        my $frm_main = $ctrl->get('frm_main');

        # Subscribe to GObject signal related to "Reset to program
        # default settings" menu item click.
        # The hook below would be called on this action.
        my $hook = shift;
        $id_menu_settings_clicked_sig =
            $frm_main->signal_add_emission_hook('menu_settings_clicked_sig' => $hook);

        return $hook;
    }

    # Setup button callbacks
               btn_apply_settings_clicked_cfg(undef);
    my $hook = menu_reset_last_settings_clicked_cfg(undef);
               menu_reset_def_settings_clicked_cfg(undef);
               menu_settings_clicked_cfg($hook);

    # GLib loop is already started by yat.
    # It will service DBus signals as we'd configured above.
    #my $loop = Glib::MainLoop->new();
    #$loop->run();
}

# Plugin exit point
sub PLUGIN_UNLOAD
{
    my $self = shift;

    # Properties
    my $frm_main     = $ctrl->get('frm_main');
    my $dlg_settings = $ctrl->get('dlg_settings');

    # Remove all MPRIS players
    foreach my $svc (keys %svc_players)
    {
        player_remove($svc);
    }

    # Clean up GObject signal hooks
    defined $id_idle_handle_sig &&
        $frm_main->signal_remove_emission_hook('idle_handle_sig', $id_idle_handle_sig);

    $dlg_settings->signal_remove_emission_hook('btn_apply_settings_clicked_sig', $id_btn_apply_settings_sig);
    $dlg_settings->signal_remove_emission_hook('menu_reset_last_clicked_sig',    $id_menu_reset_last_clicked_sig);
    $dlg_settings->signal_remove_emission_hook('menu_reset_def_clicked_sig',     $id_menu_reset_def_clicked_sig);
    $frm_main->signal_remove_emission_hook    ('menu_settings_clicked_sig',      $id_menu_settings_clicked_sig);

    dbg _T('Stop');
}

# Plugin configuration availability
sub PLUGIN_CFG_CHECK
{
    my $self = shift;

    dbg _T('Configuration is not available');

    return 0;
}

# Plugin configuration entry point
sub PLUGIN_CFG_EXEC
{
    my $self = shift;

    # Nothing to do
}

# GIR-file for libtoxcore is not implemented yet.
# We still need Perl wrappers for those methods which have not
# GIRs.
# Use Inline::C or XS wrappers like below.
BEGIN
{
    my $inline = "$ENV{HOME}/.Inline/";
    mkdir $inline;
    $ENV{PERL_INLINE_DIRECTORY} = $inline;
}

use Inline 'C' => <<'__END_OF_C__',
void c_status_set(void *tox, unsigned char *msg, size_t len)
{
    int err;
    tox_self_set_status_message(tox, msg, len, &err);
}
__END_OF_C__
    name              => 'audio_status::ModelPlugin',
    clean_after_build => 0, # Optimize for speed
    clean_build_area  => 0; # Optimize for speed

1;

