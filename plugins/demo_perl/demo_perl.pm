#!/usr/bin/perl

=pod

=encoding UTF-8

=head1 NAME

demo_perl.pm

=head1 DESCRIPTION

Demonstration plugin written on Perl programming language

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# Declare package.
# The class in this package describes ModelPlugin interface (Model.Plugin in
# Vala naming).
package demo_perl::ModelPlugin;

# CPAN modules
use Cwd;
use File::Basename;
use File::Spec::Functions;
use Glib;
use Glib qw/TRUE FALSE/; 
use Glib::Object::Introspection;
use Glib::Object::Subclass;
use Locale::gettext;
use Term::ANSIColor;
use Tie::IxHash;

# Path to binary
our $path;
our $prefix;
our $use_prefix;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    my $pattern;

    $path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; just remember it
    $path = File::Basename::dirname($path);

    $pattern    = File::Spec::Functions::catfile('share', 'yat', 'demo_perl');
    $prefix     = $path;
    $use_prefix = 1 if $prefix =~ s/\Q$pattern\E\z//i;
}

# Module path
use lib $path;

# Setup GIR instrospection
BEGIN
{
    # Directories to find for typelibs
    tie our %dirs_sys, 'Tie::IxHash';
    tie our %dirs_cwd, 'Tie::IxHash';

    my $dir = `pkgconf --variable=typelibdir gobject-introspection-1.0`;
    chomp $dir;

    @dirs_sys{glob("$prefix/lib/girepository-*.*/"),
              "$prefix/lib/x86_64-linux-gnu/girepository-1.0",
              $dir} = 1;

    %dirs_cwd = %dirs_sys if $use_prefix;
    @dirs_cwd{'./'} = 1;
    my @dirs_env = exists $ENV{GI_TYPELIB_PATH} ? split ':', $ENV{GI_TYPELIB_PATH} : ();
    @dirs_sys{@dirs_env} = @dirs_cwd{@dirs_env} = 1;

    # Search and enjoy.
    #
    # We could do Glib::Object::Introspection->setup() only instead
    # of all of the shitty loops below, but we don't know the
    # library versions. We should find them manually.
    sub wanted
    {
        my $lib  = shift;
        my $dirs = shift;

OUTER:
        foreach my $dir (reverse keys %$dirs)
        {
            my @filepaths = glob "$dir/$lib-*.*.typelib";

INNER:
            foreach my $filepath (@filepaths)
            {
                $_ = File::Basename::basename($filepath);
                next unless /^($lib)-(\d+\.\d+)\.typelib$/;

                my $name = $1;
                my $ver  = $2;

                Glib::Object::Introspection->setup(
                    basename    => $name,
                    version     => $ver,
                    package     => $lib,
                    search_path => $dir,
                );

                last OUTER;
            }
        }
    }

    # libyat,
    # GLib
    wanted('yat',  \%dirs_cwd);
    wanted('GLib', \%dirs_sys);
}

# Setup inheritance
use Glib::Object::Subclass
    'Glib::Object', # Base class
    interfaces => [ qw(yat::ModelPlugin) ],
    properties => [
        Glib::ParamSpec->object(
            'model_',
            'model', # Alias
            'This is the Model object of the main program',
            'yat::ModelModel',
            [ qw/readable writable/ ]
        ),
        Glib::ParamSpec->object(
            'view_',
            'view', # Alias
            'This is the View object of the main program',
            'yat::ViewView',
            [ qw/readable writable/ ]
        ),
        Glib::ParamSpec->object(
            'ctrl_',
            'ctrl', # Alias
            'This is the Ctrl object of the main program',
            'yat::CtrlCtrl',
            [ qw/readable writable/ ]
        ),
    ];

# Constants
use constant PREFIX      => 'demo_perl:';            # Console output prefix
use constant COLOUR      => 'grey15';                # Prefix colour
use constant LOG_DOMAIN  => 'yat-plugins';           # GLib log domain
use constant I18N_DOMAIN => 'yat-plugins-demo_perl'; # Translation domain

# Setup buffering for standard file descriptors
STDERR->autoflush(1);
STDOUT->autoflush(1);

# Setup console output
BEGIN
{
    # i18n & l10n
    sub _T
    {
        my $msg      = $_[0];
        my $i18n_msg = Locale::gettext::dgettext(I18N_DOMAIN, $msg);

        Encode::_utf8_on($i18n_msg);
        return $i18n_msg;
    }

    # Glib logger
    my $prefix =
        Term::ANSIColor::color(COLOUR) .
        PREFIX .
        Term::ANSIColor::color('reset') . ' ';

    sub dbg { Glib->debug  (LOG_DOMAIN, $prefix . $_[0]); };
    sub wrn { Glib->warning(LOG_DOMAIN, $prefix . $_[0]); };
}

# Set __WARN__ handler.
#
# All warnings should be fatal.
$SIG{__WARN__} = sub { die @_; };

# Note that module variables should be declared as global
# variables with "our" keyword.
# It is guaranteed that libpeas cleans them when plugin unloads.

# Singletons
our $model;
our $view;
our $ctrl;

# Main window frame
our $frm_main;
our $title;

# Plugin entry point.
#
# Note that methods overrided from ModelPlugin execute in UI
# event loop thread. All GUI methods should be called only
# from UI thread. (Constraint of M$ port of wxWidgets.)
#
# Also note that mutex is already had been acquired when these
# methods execute.
sub PLUGIN_LOAD
{
    my $self = shift;

    # Singletons
    $model = $self->{'model_'};
    $view  = $self->{'view_'};
    $ctrl  = $self->{'ctrl_'};

    dbg _T('Start');

    # Set title to main window
    $frm_main = $view->get('frm_main');
    $title = $frm_main->lbl_get();
    $frm_main->lbl_set(_T('Hacking yat with Perl is fun! Try it and enjoy!'));

    # Get version of libtoxcore
    my ($major, $minor, $patch);

    $major = tox_ver_major_get();
    $minor = tox_ver_minor_get();
    $patch = tox_ver_patch_get();

    # Prepare message dialog
    my $msg     = (sprintf _T("You are using libtoxcore version %u.%u.%u.\n\n"), $major, $minor, $patch) .
                  _T("The most secure and reliable cypherpunk instant messaging library and protocol.");
    my $hdr     = _T("Be Happy!");
    my $style   = ["centr", "ok"];
    my $msg_dlg = yat::ViewWrapperMsgDlg->new($frm_main, $msg, $hdr, $style);

    # Unlock mutex.
    # Show message dialog.
    # Lock mutex.
    #
    # Not that modal windows block UI thread until user clicks "OK"
    # button on them. Temporarly give mutex to allow other threads
    # execute, then acquire it again.
    $model->mutex_unlock();
    $msg_dlg->modal_show();
    $model->mutex_lock();
}

# Plugin exit point
sub PLUGIN_UNLOAD
{
    my $self = shift;

    # Reset title of main window
    $frm_main->lbl_set($title);

    dbg _T('Stop');
}

# Plugin configuration availability
sub PLUGIN_CFG_CHECK
{
    my $self = shift;

    dbg _T('Configuration is not available');

    return 0;
}

# Plugin configuration entry point
sub PLUGIN_CFG_EXEC
{
    my $self = shift;

    # Nothing to do
}

# GIR-file for libtoxcore is not implemented yet.
# We still need Perl wrappers for those methods which have not
# GIRs.
# Use Inline::C or XS wrappers like below.
BEGIN
{
    my $inline = "$ENV{HOME}/.Inline/";
    mkdir $inline;
    $ENV{PERL_INLINE_DIRECTORY} = $inline;
}

# GIR-file for libtoxcore is not implemented yet.
# We still need Perl wrappers for those methods which have not
# GIRs.
# Use Inline::C or XS wrappers like below.
use Inline 'C' => <<'__END_OF_C__',
unsigned tox_ver_major_get() { return tox_version_major(); }
unsigned tox_ver_minor_get() { return tox_version_minor(); }
unsigned tox_ver_patch_get() { return tox_version_patch(); }
__END_OF_C__
    name              => 'demo_perl::ModelPlugin',
    clean_after_build => 0, # Optimize for speed
    clean_build_area  => 0; # Optimize for speed

1;

# vim: ts=4:sw=4:sts=4:et
